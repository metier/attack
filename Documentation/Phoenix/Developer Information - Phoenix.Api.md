﻿# Fallgruver

## Integrasjon mot Agresso

### Agresso Import-service

Import Web Service'en til Agresso aksepterer et ABWOrder XSD-skjema ved opprettelse av nye ordre. En C#-klasse representasjon av dette skjemaet er generert i API'et. Agresso sin Web Service er kresen på hvordan serialisert XML fra dette skjemaet kan se ut, og er også implementert slik at den krever XML som ikke samsvarer med skjemaet for å fungere. Det er derfor blitt nødvendig å håndkode i den genererte filen for ABWOrder, og man må være oppmerksom på at dette fikses dersom man genererer nye versjoner av skjemaet i fremtiden.

- For klassen ABWOrder er det genererte XmlRootAttribute-parameteret fjernet siden tjenesten ikke aksepterer at dette elementet er prefikset
- I klassen ServerProcessInput er det lagt til en XmlString-property for korrekt generering av CDATA-element til tjeneste