﻿# Oppsett av utviklingsmiljø Phoenix.Web

## Følgende må gjøres for å sette opp Phoenix web-applikasjonen

### IIS og database

- Sett opp virtual directory for web site og web api i IIS
- Opprett lokal kopi av Phoenix LMS database
	- Lag login for IIS-bruker på Web API'et til SQL Server databasen. Gi skriv- og lesrettigheter til denne

### Validering av Javascript (JSLint)

- Installer JSLint. Extension Manager - JSLint.VS2012

### Kompilering av Twitter Bootstrap LESS-filer

- Installer Node.js - http://nodejs.org/
- Installer recess fra cmd.exe: npm install recess -g

### Testing (JavaScript)

- Installer karma:
npm install -g karma
npm install -g karma-junit-reporter
npm install -g karma-ie-launcher
npm install -g karma-ng-scenario

- CHROME_BIN
Legg til environment variable: CHROME_BIN som skal linke til chrome.exe (C:\Program Files (x86)\Google\Chrome\Application\chrome.exe)