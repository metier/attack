﻿# Oppsett av utviklingsmiljø Phoenix.Api

## Følgende må gjøres for å sette opp Phoenix API'et

### Membership og roller

- Applikasjonen bruker SQL Membership provider for opprettelse av brukerkontoer. Disse featurene må aktiveres i SQL-databasen. Dette gjøres ved å kjøre følgende kommandolinje i Visual Studio prompt. (Angi riktig database. Statementet antar at pålogget Windowsbruker har rettighet til å kjøre kommandoen mot SQL-databasen)

aspnet_regsql.exe -E -S localhost -A mr -d DEV_MetierLms

- Application pool brukeren til API'et behøver Execute permission på Phoenix-databasen. Dette kan settes eksplisitt i SQL Management Studio