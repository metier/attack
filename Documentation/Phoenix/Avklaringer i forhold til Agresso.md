# Notater fra Agressoimplementasjon

Dette dokumentet inneholder notater fra begrensninger og beslutninger fra arbeidet med Phoenix-integrasjon mot Agresso

## Issues ved opprettelse av individkunder

- Web Service krever nå at Phoenix må angi kunde-ID når kunde opprettes i Agresso. Dette er problematisk
	- I øyeblikket benytter Phoenix en random integer mellom 0 og 100 000 som kunde-ID og håper at det ikke feiler
	- Web service bør endres slik at CustomerId ikke er et påkrevet felt **(Roger)**
- Det går ikke an å slette en opprettet kunde etter at den er laget. Trenger en metodikk på å matche opprettede kunder i Agresso mot egne kunder dersom en transaksjon skulle feile etter at kunden er opprettet i Agresso

## Begrensninger i Agresso Web Services

- Man får ikke opprettet nye arbeidsordre gjennom Web Service API'et i følge Roger. For å håndtere dette problemet legger vi opp til følgende muligheter i integrasjonen
	- Phoenix API'et skal kunne konfigureres til å sette en gitt arbeidsordrerelasjon for alle individkunder som opprettes fra API'et
	- Spørring etter LMS arbeidsordre i Agresso skal returnere en predefinert arbeidsordre for kunder som ikke er konfigurert opp med egne arbeidsordre i Agresso

## Konfigurasjon som må gjøres i Phoenix

All konfigurasjon legges i API'et sin web.config. Følgende må konfigureres

- Påloggingsinformasjon for Web Service bruker mot Agresso API'et
	- Brukernavn
	- Klient
	- Passord
- Template ID'er for queries Phoenix bruker i Agresso API'et
	- AgressoInvoiceQueryTemplateId: Liste over fakturaer for en gitt Agresso-kunde
	- Liste over LMS arbeidsordre for en gitt Agresso-kunde

## Oppsummeringsepost fra første workshop på invoices

Hei,
Jeg tenkte å sende en oppsummering rundt de problemene vi diskuterte for Agresso Web Service'en i går.

1. Vi behøver å angi en arbeidsordre når det opprettes en ordre fra Phoenix. 

Akkurat nå benytter vi en tilfeldig arbeidsordre (TEST) når vi oppretter ordre, men vi behøver en ny spørring i Agresso som returnerer hvilken arbeidsordre som skal benyttes gitt en Agresso-kunde. Følgende problem må håndteres for å få på plass dette

- Hvilken arbeidsordre skal returneres for en individkunde? **(Hans Ole)**
- Skal det finnes flere arbeidsordre per kunde (online bestilling osv.) Til rapporteringsformål. **(Hans Ole)**
- Hvilken arbeidsordre skal returneres for større kunder som benytter samme arbeidsordre for flere registrerte kunder i Agresso (Telenor, Forsvaret) **(Hans Ole)**
- Lage spørring som returnerer riktig arbeidsordre i Agresso, og som tar høyde for de avklaringene som gjøres over **(Roger)**

2. Må fjerne krav til at DIM5/Y09 (produkt) må angis i Web Service kall ved opprettelse av ordre. 

I følge Hans Ole kan dette avledes fra artikkelkode, og man skal derfor ikke måtte angi denne verdien for at kontering på ordren skal bli riktig. I øyeblikket angir Phoenix et tilfeldig produkt i tjenestekallet (ACADEMY)

- Nødvendige endringer må spesifiseres og bestilles i Agressomiljøet. Jeg husker ikke hva som krevdes her.. **(Hans Ole)**
- Web Service må endres slik at den aksepterer tjenestekall uten dette parameteret **(Roger)**

3. Det må legges til DIM7 for produktpakker. 

Jeg er usikker på hvem som må gjøre hva for å få dette på plass, men skjønner det slik at dette skal fungere slik at et sett med artikler omsettes til en enkelt fakturalinje på fakturaen? Tror ikke funksjonalitet for dette er på plass nå.

**- Hvem håndterer dette?**

4. Phoenix skal opprette individkunder i Agresso. Vi behøver avklaring på hvilke(n) arbeidsordre vi skal sette på disse individkundene når disse opprettes.

- Avklare hva som skal angis av arbeidsordre på individkunde **(Hans Ole)**
- Tilpasse/beskrive hvordan Phoenix kan opprette slik kunde gjennom Web Service ****

Dette er de punktene jeg fikk med meg fra diskusjonen i går. Gi lyd dersom dere er uenig i konklusjonene/ansvarsfordelingene rundt dette..

## Tips og triks

- I Agresso kan man trykke Ctrl + Shift + H i alle felter for å få tabellinformasjon (kolonnenavn). Nyttig i forbindelse med Queries
- Dersom det er ulikt hvem som skal faktureres og den som kjøper varen så skiller en mellom BuyerNo og MainBuyerNo

	