If exists(select * from sysobjects where name like 'v_Phoenix_SearchableRcoCourses' and xtype like 'V')
	drop view v_Phoenix_SearchableRcoCourses
GO

If exists(select * from sysobjects where name like 'v_Phoenix_SearchableRcos' and xtype like 'V')
	drop view v_Phoenix_SearchableRcos
GO

CREATE VIEW [dbo].[v_Phoenix_SearchableRcos]
AS

SELECT r.Id, r.Name, r.Version, r.Context, CASE WHEN lan.Name IS NULL THEN defaultLang.Name ELSE lan.Name END as Language, Created, r.ChildCount
FROM [dbo].[Rcos] r
LEFT OUTER JOIN Languages lan ON lan.Identifier =  r.Language
LEFT OUTER JOIN Languages defaultLang ON defaultLang.Id =  45

GO