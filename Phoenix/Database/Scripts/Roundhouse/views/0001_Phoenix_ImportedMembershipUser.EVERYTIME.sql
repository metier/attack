If exists(select * from sysobjects where name like 'Phoenix_ImportedMembershipUser' and xtype like 'V')
	drop view Phoenix_ImportedMembershipUser
GO

/****** Object:  View [dbo].[Phoenix_ImportedMembershipUser]    Script Date: 18-Feb-14 15:42:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Phoenix_ImportedMembershipUser]
AS
SELECT ROW_NUMBER() OVER(ORDER BY UserId ASC) as RowId, *
FROM (
	SELECT U.UserId, U.UserName, M.Password, M.PasswordSalt
	FROM [dbo].[aspnet_Users] AS U INNER JOIN [dbo].[aspnet_Membership] AS M 
		ON U.UserId = M.UserId
	WHERE CAST(M.Comment as NVARCHAR(MAX)) = 'OracleUserExport'
) AS MembershipUser
 
GO


