If exists(select * from sysobjects where name like 'v_Phoenix_Customers' and xtype like 'V')
	drop view v_Phoenix_Customers
GO

/****** Object:  View [dbo].[v_Phoenix_Customers]    Script Date: 18-Feb-14 15:46:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

Create view [dbo].[v_Phoenix_Customers]
as
select 
	c.*
	,ci.ourRef
	,isNull(ci.orderRefCustomText, '')		orderRefCustomText
	,isNull(ci.yourRefCustomText, '')		yourRefCustomText
	,isNull(ci.BundledBilling, 0)			BundledBilling
	,ci.OrderRefLeadingTextId
	,ci.YourRefLeadingTextId
	
	,lt1.[Text]					CustomField1LeadingText
	,lt1.isVisible				CustomField1IsVisible
	,lt1.isMandatory			CustomField1IsMandatory
	,lt1.isVisibleInvoiceLine	CustomField1IsVisibleInvoiceLine
	,lt1.isGroupByOnInvoice		CustomField1IsGroupByOnInvoice

	,lt2.[Text]					CustomField2LeadingText
	,lt2.isVisible				CustomField2IsVisible
	,lt2.isMandatory			CustomField2IsMandatory
	,lt2.isVisibleInvoiceLine	CustomField2IsVisibleInvoiceLine
	,lt2.isGroupByOnInvoice		CustomField2IsGroupByOnInvoice
		
	,lt3.[Text]					EmployeeIdLeadingText
	,lt3.isVisible				EmployeeIdIsVisible
	,lt3.isMandatory			EmployeeIdIsMandatory
	,lt3.isVisibleInvoiceLine	EmployeeIdIsVisibleInvoiceLine
	,lt3.isGroupByOnInvoice		EmployeeIdIsGroupByOnInvoice

	,lt4.[Text]					DepartmentLeadingText
	,lt4.isVisible				DepartmentIsVisible
	,lt4.isMandatory			DepartmentIsMandatory
	,lt4.isVisibleInvoiceLine	DepartmentIsVisibleInvoiceLine
	,lt4.isGroupByOnInvoice		DepartmentIsGroupByOnInvoice

	,lt5.[Text]					PositionLeadingText
	,lt5.isVisible				PositionIsVisible
	,lt5.isMandatory			PositionIsMandatory
	,lt5.isVisibleInvoiceLine	PositionIsVisibleInvoiceLine
	,lt5.isGroupByOnInvoice		PositionIsGroupByOnInvoice

	,lt6.[Text]					SupervisorLeadingText
	,lt6.isVisible				SupervisorIsVisible
	,lt6.isMandatory			SupervisorIsMandatory
	,lt6.isVisibleInvoiceLine	SupervisorIsVisibleInvoiceLine
	,lt6.isGroupByOnInvoice		SupervisorIsGroupByOnInvoice

	,lt7.[Text]					ConfirmationEmailLeadingText
	,lt7.isVisible				ConfirmationEmailIsVisible
	,lt7.isMandatory			ConfirmationEmailIsMandatory
	,lt7.isVisibleInvoiceLine	ConfirmationEmailIsVisibleInvoiceLine
	,lt7.isGroupByOnInvoice		ConfirmationEmailIsGroupByOnInvoice

	,u.Id ProgramCoordinatorId
	,u.FirstName + ' ' + u.LastName ProgramCoordinatorFullName
	
from Customers c
left join MetierContactPersons mcp
	on	c.Id = mcp.CustomerId and
		mcp.ContactType = 1			-- Program Coordinator
		and mcp.IsDeleted = 0
left join CustomerInvoicings ci
	on	c.CustomerInvoicingId = ci.Id
left join Users u
	on	mcp.UserId = u.Id

left join CustomLeadingTexts lt1
	on lt1.CustomerId = c.Id
	and lt1.LeadingTextId = 11		-- Custom field 1

left join CustomLeadingTexts lt2
	on lt2.CustomerId = c.Id
	and lt2.LeadingTextId = 12		-- Custom field 2

left join CustomLeadingTexts lt3
	on lt3.CustomerId = c.Id
	and lt3.LeadingTextId = 13		-- Employee ID

left join CustomLeadingTexts lt4
	on lt4.CustomerId = c.Id
	and lt4.LeadingTextId = 14		-- Department

left join CustomLeadingTexts lt5
	on lt5.CustomerId = c.Id
	and lt5.LeadingTextId = 15		-- Position

left join CustomLeadingTexts lt6
	on lt6.CustomerId = c.Id
	and lt6.LeadingTextId = 16		-- Supervisor

left join CustomLeadingTexts lt7
	on lt7.CustomerId = c.Id
	and lt7.LeadingTextId = 17		-- Confirmation e-mail


GO


