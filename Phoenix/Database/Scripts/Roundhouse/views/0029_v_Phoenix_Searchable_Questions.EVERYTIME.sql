If exists(select * from sysobjects where name like 'v_Phoenix_Searchable_Questions' and xtype like 'V')
	drop view v_Phoenix_Searchable_Questions
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_Phoenix_Searchable_Questions] 
AS 
SELECT	
q.Id,
q.Text,
c.Id as CustomerId,
c.Name as CustomerName,
lan.Id as LanguageId,
lan.Name as Language,
q.Author,
q.Type,
q.Status,
q.Difficulty,
q.Created,
q.CreatedById,
createdByUser.FirstName + ' ' + createdByUser.LastName as CreatedByName,
q.Modified,
q.ModifiedById,
q.LastUsed,
modifiedByUser.FirstName + ' ' + modifiedByUser.LastName as ModifiedByName

FROM dbo.Questions AS q
LEFT OUTER JOIN dbo.Customers c ON c.Id = q.CustomerId
INNER JOIN dbo.Languages lan ON lan.Id = q.LanguageId
LEFT OUTER JOIN dbo.Users createdByUser ON createdByUser.Id = q.CreatedById
LEFT OUTER JOIN dbo.Users modifiedByUser ON modifiedByUser.Id = q.ModifiedById

GO