If exists(select * from sysobjects where name like 'v_Phoenix_SearchableOrders' and xtype like 'V')
	drop view v_Phoenix_SearchableOrders
GO

/****** Object:  View [dbo].[v_Phoenix_SearchableOrders]    Script Date: 18-Feb-14 15:53:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[v_Phoenix_SearchableOrders]
as

SELECT 
	*,
	'' as OrderId,
	null as OrderNumber,
	null as InvoiceNumber,
	CAST(0 as bit) as IsCreditNote
FROM v_Phoenix_ProposedOrders po

UNION

SELECT 
	'' as GroupByKey,
	d.Id  as DistributorId,
	o.Title,
	c.Name,
	c.Id as CustomerId,
	o.CoordinatorId,
	uc.FirstName + ' ' + uc.LastName as CoordinatorFullName,
	o.YourOrder,
	o.YourRef,
	o.OurRef,
	case when o.[Status] = 1 and o.IsCreditNote = 0 then
		(select isnull(sum(p1.UnitPrice), 0) from  Participants p1, Orders_Participants op1 where o.Id = op1.OrderId and op1.ParticipantId = p1.Id) + 
		(select isnull(sum(a1.FixedPrice), 0) from  Activities a1, Orders_Activities oa1 where o.Id = oa1.OrderId and a1.Id = oa1.ActivityId)
	else
		(select isnull(sum(op1.Price), 0) from  Participants p1, Orders_Participants op1 where o.Id = op1.OrderId and op1.ParticipantId = p1.Id) + 
		(select isnull(sum(oa1.Price), 0) from  Activities a1, Orders_Activities oa1 where o.Id = oa1.OrderId and a1.Id = oa1.ActivityId)
	end as TotalAmount,
	currencyCode.Value as CurrencyCode,
	o.LastModified,
	o.[Status] as OrderStatus,
	c.BundledBilling,
	case when c.IsArbitrary = 1 then 0 else 1 end as CustomerType,
	o.Id as OrderId,
	o.OrderNumber,
	o.InvoiceNumber,
	o.IsCreditNote
FROM Orders o
INNER JOIN v_Phoenix_Customers c
	ON o.CustomerId = c.Id
INNER JOIN Distributors d
	ON c.DistributorId = d.Id
LEFT JOIN Users uc
	ON o.CoordinatorId = uc.Id
LEFT JOIN Orders_Activities oa
	ON o.Id = oa.OrderId
LEFT JOIN Activities a
	ON oa.ActivityId = a.Id
LEFT JOIN Orders_Participants op
	ON o.Id = op.OrderId
LEFT JOIN Participants p
	ON op.ParticipantId = p.Id
LEFT JOIN Codes currencyCode
	ON (
	case when o.[Status] = 0 then 
		case when a.CurrencyCodeId is not null then a.CurrencyCodeId else p.CurrencyCodeId end
	else
		case when a.CurrencyCodeId is not null then oa.CurrencyCodeId else op.CurrencyCodeId end
	end) = currencyCode.Id
GROUP BY
	o.Id,
	d.Id,
	o.Title,
	c.Name,
	o.CoordinatorId,
	uc.FirstName + ' ' + uc.LastName,
	o.YourOrder,
	o.YourRef,
	o.OurRef,
	o.LastModified,
	o.[Status],
	c.BundledBilling,
	currencyCode.Value,
	o.OrderNumber,
	o.InvoiceNumber,
	c.Id,
	c.IsArbitrary,
	o.IsCreditNote
GO


