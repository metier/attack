If exists(select * from sysobjects where name like 'v_Phoenix_CurrentParticipantOrders' and xtype like 'V')
	drop view v_Phoenix_CurrentParticipantOrders
GO

-- JSO, 27. aug. 2014: Don't need this view anymore...

/****** Object:  View [dbo].[v_Phoenix_CurrentParticipantOrders]    Script Date: 18-Feb-14 15:45:55 ******/
/* 
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[v_Phoenix_CurrentParticipantOrders] as 
with participantOrders as (
	select max(OrderId) maxOrderId, ParticipantId 
	from [Orders_Participants]
	group by ParticipantId
)
select o.*, po.ParticipantId
from participantOrders po
inner join [Orders] o
on po.maxOrderId = o.Id


GO


*/
