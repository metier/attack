If exists(select * from sysobjects where name like 'v_Phoenix_SearchableParticipants' and xtype like 'V')
	drop view v_Phoenix_SearchableParticipants
GO

/****** Object:  View [dbo].[v_Phoenix_SearchableParticipants]    Script Date: 18-Feb-14 15:53:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[v_Phoenix_SearchableParticipants]
as

SELECT 
	p.Id AS Id,
	c.Id AS CustomerId,
	u.Id AS UserId,
	aset.Id AS ActivitySetId,
	act.Id AS ActivityId,
	ca.CustomerId AS ActivityOwnerCustomerId,
	cac.Name AS ActivityOwnerCustomerName,
	ca.Id AS CustomerArticleId,
	a.Title AS CustomerArticleTitle,
	a.Id AS ArticleId,
	u.FirstName,
	u.LastName,
	c.Name as CustomerName,
	a.Path as ArticlePath,
	a.ArticleType as ArticleType,
	a.ArticleTypeId,
	p.UnitPrice,
	p.StatusCodeValue as ParticipantStatusCodeValue,
	p.CompletedDate as ParticipantCompletedDate,
	code.Value AS CurrencyCode,
	aset.Name AS ActivitySetName,
	act.Name AS ActivityName,
	act.MomentTimezoneName,
	act.HideActivityDates,
	(select min(aps.Start) from dbo.ActivityPeriods aps where aps.ActivityId=act.Id) as ActivityStart,
	(select max(aps.[End]) from dbo.ActivityPeriods aps where aps.ActivityId=act.Id) as ActivityEnd,	
	cast(CASE WHEN op.IsCreditNote IS NULL OR (op.IsCreditNote = 1) OR op.Status = 1 THEN 0 ELSE 1 END as bit) AS IsInvoiced,
	op.Id as OrderId,
	op.OrderNumber as OrderNumber,
	op.InvoiceNumber as InvoiceNumber,
	op.Status as OrderStatus,
	act.RcoCourseId,
	act.RcoExamContainerId,
	a.LanguageId,
	a.ProductDescriptionId,
	a.IsUserCompetenceInfoRequired,
	p.EnrollmentId,
	EnrolledActivitySet.Id as EnrolledActivitySetId,
	EnrolledActivitySet.Name as EnrolledActivitySetName,
	RcoCourseProgress.RcoLessonCount,
	RcoCourseProgress.RcoLessonCompletedCount,
	RcoExamProgress.RcoExamCount,
	RcoExamProgress.RcoExamCompletedCount
FROM Participants p
LEFT JOIN Codes code 
	ON p.CurrencyCodeId = code.Id
INNER JOIN Users u
	ON p.UserId = u.Id
INNER JOIN Customers c
	ON p.CustomerId = c.Id
INNER JOIN ActivitySets aset
	ON p.ActivitySetId = aset.Id
INNER JOIN Activities act
	ON p.ActivityId = act.Id
INNER JOIN CustomerArticles ca
	ON act.CustomerArticleId = ca.Id
INNER JOIN Customers cac
	ON ca.CustomerId = cac.Id
INNER JOIN v_Phoenix_Searchable_Articles_All a
	ON ca.ArticleCopyId = a.Id
CROSS APPLY(
	SELECT top 1 aset2.* FROM Enrollments E 
	INNER JOIN ActivitySets aset2
	ON aset2.id = e.ActivitySetId
	WHERE e.id = p.EnrollmentId
	) as EnrolledActivitySet
OUTER APPLY
	(SELECT TOP 1 o.*
		FROM Orders_Participants pop
		INNER JOIN Orders o
			ON pop.OrderId = o.Id
		WHERE pop.ParticipantId = p.Id
		ORDER BY pop.OrderId DESC) AS op
OUTER APPLY
	(SELECT COUNT(*) AS RcoLessonCount, ISNULL(SUM(CASE WHEN sp.Status='C' THEN 1 ELSE 0 END),0) AS RcoLessonCompletedCount
		FROM Rcos r
		LEFT JOIN ScormPerformances sp
			ON sp.RcoId = r.Id AND sp.UserId = u.Id
		WHERE r.ParentCourseId = act.RcoCourseId AND r.Context='lesson' and r.IsFinalTest=0
	) AS RcoCourseProgress
OUTER APPLY
	(SELECT COUNT(*) AS RcoExamCount, ISNULL(SUM(CASE WHEN sp.Status='C' THEN 1 ELSE 0 END),0) AS RcoExamCompletedCount
		FROM Rcos r
		LEFT JOIN ScormPerformances sp
			ON sp.RcoId = r.Id AND sp.UserId = u.Id
		WHERE r.ParentId = act.RcoExamContainerId 
	) AS RcoExamProgress
WHERE p.IsDeleted = 0

GO