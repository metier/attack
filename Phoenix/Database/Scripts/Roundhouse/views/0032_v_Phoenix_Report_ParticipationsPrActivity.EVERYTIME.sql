If exists(select * from sysobjects where name like 'v_Phoenix_Report_ParticipantsPrActivity' and xtype like 'V')
	drop view v_Phoenix_Report_ParticipantsPrActivity
GO

create view [dbo].v_Phoenix_Report_ParticipantsPrActivity 
as 
	select 
		p.id ParticipantId,
		d.Id DistributorId,
		d.Name Distributor,
		c.Id CustomerId,
		c.Name Customer,
		(select pc.name from customers pc where pc.id = c.ParentId) as ParentOrg,
		cart.productId,
		dbo.fn_product_path(cart.productId) AS ProductPath,
		dbo.fn_article_path(art.Id) as ArticleNumber,
		(select name from ArticleTypes at where at.Id = art.ArticleTypeId) as ContentType,
		elearning.CourseLink,
		art.Title as ArticleName,
		cart.Title as CustomerArticleName,
		(select l.Name from Languages l where id = cart.LanguageId) CustomerArticleLanguage,
		a.Name ActivityName,
		u.FirstName,
		u.LastName,
		u.MembershipUserId as UserName,
		am.Email, 
		u.PhoneNumber,
		u.ExpiryDate,
		am.LastLoginDate,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.Id and pie.LeadingTextId = 11) CustomField1,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.Id and pie.LeadingTextId = 12) CustomField2,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.Id and pie.LeadingTextId = 13) EmployeeId,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.Id and pie.LeadingTextId = 14) Department,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.Id and pie.LeadingTextId = 15) Position,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.Id and pie.LeadingTextId = 16) SuperVisor,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.Id and pie.LeadingTextId = 17) ConfirmationEmail,
		p.created EnrollmentDate,
		periods.startDate,
		periods.enddate,
		p.[StatusCodeValue] as [Status],
		isNull(elearning.NumberOfLessons, 0) NumberOfLessons,
		isNull(elearning.Points, 0) Points,
		isNull(elearning.NumberOfCompletedLessons, 0) NumberOfCompletedLessons,
		IsNull(Cast(accountManager.FirstName as varchar(max)) + ' ','') + IsNull(Cast(accountManager.LastName as varchar(max)),'') AS AccountManager,
		IsNull(Cast(coordinator.FirstName as varchar(max)) + ' ','') + IsNull(Cast(coordinator.LastName as varchar(max)),'') AS Coordinator,
		partInfo.IsAllowUsersToSelfEnrollToExams	AS SelfEnrollmentExams,
		partInfo.IsAllowUsersToSelfEnrollToCourses	AS SelfEnrollmentCourses,
		userroles.Roles as UserRole,
		c.IsArbitrary
	from users u 
	inner join aspnet_Users au on au.UserName = u.MembershipUserId
	inner join aspnet_Membership am on am.UserId = au.UserId
	inner join Participants p on p.UserId = u.Id
	inner join Customers c on c.id = p.CustomerId
	inner join Distributors d on d.Id = c.DistributorId
	inner join Activities a on a.id = p.ActivityId
	inner join CustomerArticles ca on ca.id = a.CustomerArticleId
	inner join Articles art on art.Id = ca.ArticleOriginalId
	inner join Articles cart on cart.Id = ca.ArticleCopyId
	left outer join ParticipantInfoDefinitions partInfo ON c.ParticipantInfoDefinitionId = partInfo.Id
	outer apply(
		select min(ap.Start) as startDate, max(ap.[End]) as enddate	from ActivityPeriods ap 
		where ap.ActivityId = p.ActivityId) as periods
	outer apply(
		select top 1 
			rco.[Version] as CourseLink,
			course.points,
			course.completedDate,
			(select count(distinct spf.RcoId) from ScormPerformances spf inner join rcos r on spf.RcoId = r.Id where r.ParentId = rco.Id and spf.UserId = p.userId and spf.[status] = 'C') as NumberOfCompletedLessons, 	 
			(select count(*) from rcos r where r.ParentId = rco.id) as NumberOfLessons
		from v_Phoenix_SearchableRcos rco 
		outer apply(
			select max(spf.Score) as Points, max(spf.CompletedDate) CompletedDate 
			from ScormPerformances spf where spf.RcoId = rco.id and spf.UserId = p.userId
			) as course
		where 
			rco.id = a.RcoCourseId
			and cart.ArticleTypeId = 1
		) elearning
	outer apply (
		select top 1 contper.CustomerId, contper_user.FirstName, contper_user.LastName
		from MetierContactPersons contper
		inner join Users contper_user ON contper.UserId = contper_user.Id
		where contper.ContactType = 0 and c.Id = contper.CustomerId
		) as accountManager
	outer apply (
		select top 1 contper.CustomerId, contper_user.FirstName, contper_user.LastName		--Don't really think I need the 'top 1', but just want to make absolutely sure we don't get duplicate rows
		from MetierContactPersons contper
		inner join Users contper_user ON contper.UserId = contper_user.Id
		where contper.ContactType = 1 and c.Id = contper.CustomerId
		) as coordinator 
	outer apply (
		select stuff(
			(select N', ' + asp_role.RoleName 
				from vw_aspnet_Users asp_user 
				left outer join vw_aspnet_UsersInRoles asp_userrole ON asp_user.UserId = asp_userrole.UserId
				left outer join vw_aspnet_Roles asp_role ON asp_userrole.RoleId = asp_role.RoleId
				where u.MembershipUserId = asp_user.UserName
				for xml path(''),type).value('text()[1]','nvarchar(max)'),1,2,N'') as Roles
		) as userroles
	where
		p.IsDeleted		= 0 and 
		a.IsDeleted		= 0 and
		ca.IsDeleted	= 0 and
		art.IsDeleted	= 0 and 
		cart.IsDeleted	= 0
