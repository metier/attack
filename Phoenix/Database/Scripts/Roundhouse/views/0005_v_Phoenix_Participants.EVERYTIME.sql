If exists(select * from sysobjects where name like 'v_Phoenix_Participants' and xtype like 'V')
	drop view v_Phoenix_Participants
GO

/****** Object:  View [dbo].[v_Phoenix_Participants]    Script Date: 18-Feb-14 15:47:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create view [dbo].[v_Phoenix_Participants]
as

SELECT 
	  p.id
	, p.UserId
	, u.FirstName
	, u.LastName
	, a.Name ActivityName
	, p.ActivitySetId
	, p.customerId
	, p.ActivityId
	, p.StatusCodeValue
	, p.CurrencyCodeId
	, p.UnitPrice
	, p.EarliestInvoiceDate
	, p.isNotBillable
	, p.OnlinePaymentReference

	, isNull(pst.CustomField1, '')		CustomField1InfoText
	, isNull(pst.CustomField2, '')		CustomField2InfoText
	, isNull(pst.EmployeeId, '')		EmployeeIdInfoText
	, isNull(pst.Department, '')		DepartmentInfoText
	, isNull(pst.Position, '')			PositionInfoText
	, isNull(pst.Supervisor, '')		SupervisorInfoText
	, isNull(pst.ConfirmationEmail, '') ConfirmationEmailInfoText

	,InvoiceStatus.isInvoiced


FROM 
	Participants p 
left join
	ParticipantInfoElementsShadowTable pst
	on pst.ParticipantId = p.Id
inner join
	Users u
	on u.Id = p.UserId
inner join Activities a
	on a.Id = p.ActivityId
outer apply(
	select top 1 op.*, o.IsCreditNote, o.[Status]
	from Orders_Participants op
	inner join Orders o
	on op.OrderId = o.Id
	where op.ParticipantId = p.Id
	order by o.Id desc
) CurrentParticipantOrder

cross apply
	(
	select case

	 when p.id not in(
		/*
		Returnerer alle deltagelser som har blitt fakturert (alts� hvor siste ordre er en faktura), 
		eller deltagelser som er tilknyttet en kreditnota med status 'Proposed' eller 'Draft'.
		(Det er f�rst n�r en kreditnota er sendt til Agresso at deltagelsen skal tilgjengeliggj�res for fakturering igjen)
		*/
		select CurrentParticipantOrder.ParticipantId
		where 
			  (CurrentParticipantOrder.IsCreditNote = 0 or
			  	  (CurrentParticipantOrder.IsCreditNote = 1 and CurrentParticipantOrder.[Status] < 2)
				)
		) then 0
		else 1
	  end isInvoiced
	  ) InvoiceStatus

WHERE
	a.isDeleted = 0	and p.IsDeleted = 0
	
GO


