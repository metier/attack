If exists(select * from sysobjects where name like 'v_Phoenix_Report_ActivityParticipants' and xtype like 'V')
	drop view v_Phoenix_Report_ActivityParticipants
GO


CREATE VIEW [dbo].[v_Phoenix_Report_ActivityParticipants] 
AS 
SELECT	
cus.Id as CustomerId,
d.Id as DistributorId,
usr.Id as UserId,
act.Id as ActivityId,
par.ActivitySetId as ActivitySetId,
aset.Name as ActivitySetTitle,
pro.Id as ProductId,
art.Id as ArticleId,
arttype.Id as ArticleTypeId,
cusart.Id as CustomerArticleId,
par.Id as ParticipantId,
act.Name as ActivityTitle,
cus.Name as Customer,
d.Name as Distributor,
dbo.fn_product_path(pro.Id) as ProductNumber,
dbo.fn_article_path(art.Id) as ArticleNumber,
sp.Start as StartDate,
ep.[End] as EndDate,
par.Created as EnrollmentDate,
usr.FirstName as FirstName,
usr.LastName as LastName,
aspmbr.Email as Email,
code.Description as Status,
case when par.Result = 'PASSED' THEN 'Passed' ELSE CASE WHEN par.Result = 'FAILED' THEN 'Failed' ELSE null END END as Result,
cus.IsArbitrary

FROM 
Customers cus
INNER JOIN Distributors d ON d.Id = cus.DistributorId
INNER JOIN Participants par ON par.CustomerId = cus.Id
INNER JOIN Codes code ON code.Value = par.StatusCodeValue
INNER JOIN CodeTypes ct ON ct.Id = code.CodeTypeId
INNER JOIN Activities act ON act.Id = par.ActivityId
INNER JOIN ActivitySets aset ON par.ActivitySetId = aset.Id
INNER JOIN Users usr ON par.UserId = usr.Id
INNER JOIN CustomerArticles cusart ON cusart.Id = act.CustomerArticleId
INNER JOIN Articles art ON art.Id = cusart.ArticleCopyId
INNER JOIN ArticleTypes arttype ON art.ArticleTypeId = arttype.Id
INNER JOIN Products pro ON pro.Id = art.ProductId
INNER JOIN aspnet_Users aspusr ON usr.MembershipUserId = aspusr.UserName
INNER JOIN aspnet_Membership aspmbr ON aspmbr.UserId = aspusr.UserId

LEFT OUTER JOIN (SELECT sp2.Start, sp2.ActivityId 
FROM dbo.ActivityPeriods as sp2
	JOIN (SELECT ActivityId, MIN(Start) MinStart 
		FROM dbo.ActivityPeriods
		GROUP BY ActivityId) as sp3 ON sp2.ActivityId = sp3.ActivityId AND sp2.Start = sp3.MinStart
GROUP BY sp2.ActivityId, sp2.Start) as sp ON act.Id = sp.ActivityId

LEFT OUTER JOIN (SELECT ep2.[End], ep2.ActivityId 
FROM dbo.ActivityPeriods as ep2
	JOIN (SELECT ActivityId, MAX([End]) MaxEnd 
		FROM dbo.ActivityPeriods
		GROUP BY ActivityId) as ep3 ON ep2.ActivityId = ep3.ActivityId AND ep2.[End] = ep3.[MaxEnd]
GROUP BY ep2.ActivityId, ep2.[End]) as ep ON act.Id = ep.ActivityId

WHERE 
act.IsDeleted = 0 
AND par.IsDeleted = 0
AND cusart.IsDeleted = 0
AND ct.Name = 'ParticipantStatuses'

GROUP BY

cus.Id,
d.Id,
usr.Id,
act.Id,
par.ActivitySetId,
aset.Name,
pro.Id,
art.Id,
arttype.Id,
cusart.Id,
par.Id,
act.Name,
cus.Name,
d.Name,
sp.Start,
ep.[End],
par.Created,
usr.FirstName,
usr.LastName,
aspmbr.Email,
code.Description,
par.Result,
cus.IsArbitrary 
GO
