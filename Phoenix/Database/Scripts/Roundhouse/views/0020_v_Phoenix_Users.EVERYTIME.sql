If exists(select * from sysobjects where name like 'v_Phoenix_Users' and xtype like 'V')
	drop view v_Phoenix_Users
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create view [dbo].[v_Phoenix_Users]
as

SELECT 
	u.Id
	, u.FirstName
	, u.LastName
	, aspmbr.Email
	, isNull(customField1.InfoText, '') CustomField1InfoText
	, isNull(customField2.InfoText, '') CustomField2InfoText
	, isNull(employeeId.InfoText, '') EmployeeIdInfoText
	, isNull(department.InfoText, '') DepartmentInfoText
	, isNull(position.InfoText, '') PositionInfoText
	, isNull(supervisor.InfoText, '') SupervisorInfoText
	, isNull(confEmail.InfoText, '') ConfirmationEmailInfoText
	, u.ExpiryDate
FROM 
	Users u 
	INNER JOIN aspnet_Users aspusr ON u.MembershipUserId = aspusr.UserName
	INNER JOIN aspnet_Membership aspmbr ON aspmbr.UserId = aspusr.UserId

OUTER APPLY (
	SELECT ie.* FROM UserInfoElements ie WHERE ie.UserId = u.Id and ie.LeadingTextId = 11
) as customField1

OUTER APPLY (
	SELECT ie.* FROM UserInfoElements ie WHERE ie.UserId = u.Id and ie.LeadingTextId = 12
) as customField2

OUTER APPLY (
	SELECT ie.* FROM UserInfoElements ie WHERE ie.UserId = u.Id and ie.LeadingTextId = 13
) as employeeId

OUTER APPLY (
	SELECT ie.* FROM UserInfoElements ie WHERE ie.UserId = u.Id and ie.LeadingTextId = 14
) as department

OUTER APPLY (
	SELECT ie.* FROM UserInfoElements ie WHERE ie.UserId = u.Id and ie.LeadingTextId = 15
) as position

OUTER APPLY (
	SELECT ie.* FROM UserInfoElements ie WHERE ie.UserId = u.Id and ie.LeadingTextId = 16
) as supervisor

OUTER APPLY (
	SELECT ie.* FROM UserInfoElements ie WHERE ie.UserId = u.Id and ie.LeadingTextId = 17
) as confEmail

GO


