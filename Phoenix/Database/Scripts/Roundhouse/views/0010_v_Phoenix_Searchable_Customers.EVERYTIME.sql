If exists(select * from sysobjects where name like 'v_Phoenix_Searchable_Customers' and xtype like 'V')
	drop view v_Phoenix_Searchable_Customers
GO

/****** Object:  View [dbo].[v_Phoenix_Searchable_Customers]    Script Date: 18-Feb-14 15:52:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

CREATE VIEW [dbo].[v_Phoenix_Searchable_Customers] 
AS 
SELECT	cust.Id AS Id,
		cust.ExternalCustomerId AS ExternalCustomerId, 
		cust.Name AS Name,
		cust.DistributorId AS DistributorId,
		d.Name AS DistributorName,
		lan.Name AS Language,
		cust.ParentId AS ParentId,
		parentcusts.Name as ParentName,
		parentcusts.ExternalCustomerId AS ParentExternalCustomerId,
		cust.IsArbitrary AS IsArbitrary

FROM	dbo.Customers AS cust 
		INNER JOIN dbo.Distributors as d ON d.Id = cust.DistributorId
		LEFT OUTER JOIN dbo.Languages AS lan ON cust.LanguageId = lan.ID 
		LEFT OUTER JOIN dbo.Customers AS parentcusts ON parentcusts.Id = cust.ParentId
 
GO


