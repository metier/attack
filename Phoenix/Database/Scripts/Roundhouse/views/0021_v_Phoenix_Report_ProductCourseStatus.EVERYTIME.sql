If exists(select * from sysobjects where name like 'v_Phoenix_Report_ProductCourseStatus' and xtype like 'V')
	drop view v_Phoenix_Report_ProductCourseStatus
GO

create view [dbo].v_Phoenix_Report_ProductCourseStatus 
as 
with _participants as(
	select 
		p.id as ParticipantId,
		p.UserId,
		art.ProductId,
		a.id as ActivityId,
		a.Name as ActivityName,
		ase.Name as ActivitySetName,
		a.RcoCourseId,
		p.StatusCodeValue as [Status],
		p.Result,
		p.Created EnrollmentDate,
		art.ArticleTypeId,
		art.id as ArticleId,
		art.Title as ArticleName,
		l.Name as [Language],
		periods.startDate,
		periods.enddate
	from participants p
	inner join activities a
	on a.id = p.ActivityId
	inner join CustomerArticles ca
	on ca.id = a.CustomerArticleId
	inner join Articles art
	on art.id = ca.ArticleCopyId
	inner join Languages l
	on art.LanguageId = l.Id
	inner join ActivitySets ase
	on p.ActivitySetId = ase.id
	outer apply(
		select min(ap.Start) as startDate, max(ap.[End]) as enddate	from ActivityPeriods ap 
		where ap.ActivityId = p.ActivityId) as periods
	where
		p.IsDeleted = 0 and 
		a.IsDeleted = 0 and
		ca.IsDeleted = 0 and
		art.IsDeleted = 0
),
UserProducts as(
	select distinct 
		UserId, 
		su.Email, 
		u.MembershipUserId as UserName,
		u.FirstName,
		u.LastName,
		u.PhoneNumber,
		u.ExpiryDate,
		u.customerId, 
		ProductId
	from _participants p
	inner join Users u
	on u.id = p.UserId
	inner join v_phoenix_searchable_Users su
	on u.id = su.Id
), additionalFields as 
(
	select usrinfo.UserId, usrinfo.LeadingTextId, usrinfo.InfoText from UserInfoElements usrinfo
)
select 
	prod.Id as ProductId,
	prod.Title as ProductTitle,
	c.Id as CustomerId,
	c.DistributorId as DistributorId,
	up.UserId,
	c.Name as Customer,
	d.Name as Distributor,
	dbo.fn_product_path(prod.Id) as ProductNumber,
	isNull(isNull(classroom.ActivitySetName, elearning.ActivitySetName), exam.ActivitySetName) as ActivitySetTitle,
	up.FirstName,
	up.LastName,
	up.Email,
	up.PhoneNumber,
	up.ExpiryDate,
	custom1UsrInfo.InfoText as CustomField1,
	custom2UsrInfo.InfoText as CustomField2,
	empUsrInfo.InfoText as EmployeeId,
	departmentUsrInfo.InfoText as Department,
	positionUsrInfo.InfoText as Position,
	supervisorUsrInfo.InfoText as Supervisor,
	confirmationUsrInfo.InfoText as ConfirmationEmail,

	elearning.ActivityName as ActivityTitleE,
	elearning.EnrollmentDate as EnrollmentDateE,
	elearning.StartDate as StartDateE,
	elearning.EndDate as EndDateE,
	elearning.[Status] as StatusE,
	elearning.NumberOfCompletedLessons,
	elearning.NumberOfLessons,
	elearning.PerformanceStatus as CourseStatus,
	elearning.CompletedDate,

	classroom.ActivityName as ActivityTitleC,
	classroom.EnrollmentDate as EnrollmentDateC,
	classroom.StartDate as StartDateC,
	classroom.EndDate as EndDateC,
	classroom.Status as StatusC,

	exam.ActivityName as ActivityTitleEX,
	exam.EnrollmentDate as EnrollmentDateEX,
	exam.StartDate as StartDateEX,
	exam.EndDate as EndDateEX,
	exam.Status as StatusEX,
	exam.Result as ResultEX,

	c.IsArbitrary,
	isNull(isNull(classroom.EnrollmentDate, elearning.EnrollmentDate), exam.EnrollmentDate) as EnrollmentDate
from UserProducts up
inner join Customers c
on c.id = up.CustomerId
inner join Distributors d
on c.DistributorId = d.Id
inner join Products prod on prod.id = up.ProductId
left outer join additionalFields custom1UsrInfo on up.userId = custom1UsrInfo.UserId and custom1UsrInfo.LeadingTextId = 11 -- 'Custom field 1 ID' LeadingText
left outer join additionalFields custom2UsrInfo on up.userId = custom2UsrInfo.UserId and custom2UsrInfo.LeadingTextId = 12 -- 'Custom field 2 ID' LeadingText
left outer join additionalFields empUsrInfo on up.userId = empUsrInfo.UserId and empUsrInfo.LeadingTextId = 13 -- 'Employee ID' LeadingText
left outer join additionalFields departmentUsrInfo on up.userId = departmentUsrInfo.UserId and departmentUsrInfo.LeadingTextId = 14 -- 'Department ID' LeadingText
left outer join additionalFields positionUsrInfo on up.userId = positionUsrInfo.UserId and positionUsrInfo.LeadingTextId = 15 -- 'Position ID' LeadingText
left outer join additionalFields supervisorUsrInfo on up.userId = supervisorUsrInfo.UserId and supervisorUsrInfo.LeadingTextId = 16 -- 'Supervisor ID' LeadingText
left outer join additionalFields confirmationUsrInfo on up.userId = confirmationUsrInfo.UserId and confirmationUsrInfo.LeadingTextId = 17 -- 'Confirmation email ID' LeadingText

outer apply(
	select top 1 
		p.ActivitySetName,
		p.ActivityName,
		p.ArticleName,
		p.[Status],
		p.EnrollmentDate,
		p.[Language],
		rco.[Version] as CourseLink,
		course.points,
		course.completedDate,
		(select count(*) from ScormPerformances spf inner join rcos r on spf.RcoId = r.Id where r.ParentId = rco.Id and spf.UserId = up.userId and spf.[status] = 'C') as NumberOfCompletedLessons, 	 
		(select count(*) from rcos r where r.ParentId = rco.id) as NumberOfLessons,
		course.PerformanceStatus,
		p.startDate,
		p.enddate,
		p.Result
	from _participants p
	left outer join v_Phoenix_SearchableRcos rco 
		ON p.RcoCourseId = rco.Id
	outer apply(
		select max(spf.Score) as Points, max(spf.CompletedDate) CompletedDate, min(spf.[status]) as PerformanceStatus
		from ScormPerformances spf where spf.RcoId = rco.id and spf.UserId = p.userId
		) as course
	where up.UserId = p.UserId
		and p.ProductId = up.ProductId
		and p.ArticleTypeId = 1
	order by p.EnrollmentDate desc
	) elearning
outer apply(
	select top 1 * from _participants p
	where up.UserId = p.UserId
	and p.ProductId = up.ProductId
	and p.ArticleTypeId = 2
	order by p.EnrollmentDate desc
	) classroom
outer apply(
	select top 1 * from _participants p
	where up.UserId = p.UserId
	and p.ProductId = up.ProductId
	and p.ArticleTypeId in(3, 4, 5, 6, 8, 9)
	order by p.EnrollmentDate desc
	) exam