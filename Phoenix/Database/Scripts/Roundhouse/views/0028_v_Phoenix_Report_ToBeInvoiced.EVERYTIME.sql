If exists(select * from sysobjects where name like 'v_Phoenix_Report_ToBeInvoiced' and xtype like 'V')
	drop view v_Phoenix_Report_ToBeInvoiced
GO

CREATE VIEW [dbo].v_Phoenix_Report_ToBeInvoiced 
AS 
select NEWID() as RowId,
	   o.GroupByKey as Id,
	   'GroupByKey' as IdType,
	   d.Id as DistributorId,
	   d.Name as DistributorName,
	   c.Id as CustomerId,
	   c.Name as CustomerName,
	   order_activities.ProductId,
	   order_activities.Path,
	   order_activities.ArticleCode,
	   order_activities.ActivityTitle, 
	   order_activities.EarliestInvoiceDate, 
	   order_activities.CurrencyCode,
	   sum(order_activities.UnitPrice) as Amount,
	   o.OrderStatus as OrderStatus,
	   c.IsArbitrary
from [dbo].[v_Phoenix_FutureOrders] as o
inner join [dbo].[Customers] as c on o.CustomerId = c.Id
inner join [dbo].[Distributors] as d on c.DistributorId = d.Id
outer apply 
(
	select p.Id as ProductId, art.Path, art.ArticleCode, a.Name as ActivityTitle, a.EarliestInvoiceDate, codes.Value as CurrencyCode, ol.UnitPrice
	from [dbo].[v_Phoenix_FutureOrderLines] as ol
	inner join [dbo].[Activities] as a on a.Id = ol.ActivityId
	inner join [dbo].[CustomerArticles] as ca on ca.Id = a.CustomerArticleId
	inner join [dbo].[v_Phoenix_Searchable_Articles_All] as art on art.Id = ca.ArticleCopyId
	inner join [dbo].[Products] as p on p.Id = art.ProductId
	left outer join [dbo].[Codes] as codes on codes.Id = ol.CurrencyCodeId
	where ol.GroupByKey = o.GroupByKey
) as order_activities
where order_activities.UnitPrice is not null
group by o.GroupByKey, d.Id, d.Name, c.Id, c.Name, order_activities.ProductId, order_activities.Path, order_activities.ArticleCode, order_activities.ActivityTitle,  order_activities.EarliestInvoiceDate,  order_activities.CurrencyCode, o.OrderStatus, c.IsArbitrary
union
select NEWID() as RowId,
	   cast(o.Id as nvarchar(max)) as Id,
	   'Id' as IdType,
	   d.Id as DistributorId,
	   d.Name as DistributorName,
	   c.Id as CustomerId,
	   c.Name as CustomerName,
	   order_activities.ProductId,
	   order_activities.Path,
	   order_activities.ArticleCode,
	   order_activities.ActivityTitle, 
	   order_activities.EarliestInvoiceDate, 
	   order_activities.CurrencyCode,
	   order_activities.Amount,
	   o.Status as OrderStatus,
	   c.IsArbitrary
from [dbo].[Orders] as o
inner join [dbo].[Customers] as c on o.CustomerId = c.Id
inner join [dbo].[Distributors] as d on c.DistributorId = d.Id
outer apply 
(
	select orderlines.ProductId, orderlines.Path, orderlines.ArticleCode, orderlines.ActivityTitle, orderlines.EarliestInvoiceDate, orderlines.CurrencyCode, SUM(Price) as Amount
	from
	(
		select prod.Id as ProductId, art.Path, art.ArticleCode, a.Name as ActivityTitle, a.EarliestInvoiceDate, codes.Value as CurrencyCode, p.UnitPrice as Price
		from [dbo].[Orders_Participants] as op
		inner join [dbo].[Participants] p on p.Id = op.ParticipantId
		inner join [dbo].[Activities] as a on a.Id = p.ActivityId
		inner join [dbo].[CustomerArticles] as ca on ca.Id = a.CustomerArticleId
		inner join [dbo].[v_Phoenix_Searchable_Articles_All] as art on art.Id = ca.ArticleCopyId
		inner join [dbo].[Products] as prod on prod.Id = art.ProductId
		left outer join [dbo].[Codes] as codes on codes.Id = p.CurrencyCodeId
		where op.OrderId = o.Id
		union all
		select prod.Id as ProductId, art.Path, art.ArticleCode, a.Name as ActivityTitle, a.EarliestInvoiceDate, codes.Value as CurrencyCode, a.FixedPrice
		from [dbo].[Orders_Activities] as oa
		inner join [dbo].[Activities] as a on a.Id = oa.ActivityId
		inner join [dbo].[CustomerArticles] as ca on ca.Id = a.CustomerArticleId
		inner join [dbo].[v_Phoenix_Searchable_Articles_All] as art on art.Id = ca.ArticleCopyId
		inner join [dbo].[Products] as prod on prod.Id = art.ProductId
		left outer join [dbo].[Codes] as codes on codes.Id = a.CurrencyCodeId
		where oa.OrderId = o.Id
	) as orderlines
	group by orderlines.ProductId, orderlines.Path, orderlines.ArticleCode, orderlines.ActivityTitle, orderlines.EarliestInvoiceDate, orderlines.CurrencyCode
) as order_activities
where o.Status = 1 and Amount is not null