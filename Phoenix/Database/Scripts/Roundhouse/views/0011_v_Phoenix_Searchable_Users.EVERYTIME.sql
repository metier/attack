if exists(select * from sysobjects where name like 'v_Phoenix_Searchable_Users' and xtype like 'V')
	drop view v_Phoenix_Searchable_Users
GO

/****** Object:  View [dbo].[v_Phoenix_Searchable_Users]    Script Date: 18-Feb-14 15:53:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

CREATE VIEW [dbo].[v_Phoenix_Searchable_Users] 
AS 

SELECT
	usr.Id as Id,
	usr.FirstName,
	usr.LastName,
	usr.PhoneNumber,
	aspusr.UserName,
	aspmbrs.Email,
	usr.MembershipUserId,
	cust.Id as CustomerId,
	cust.Name as CustomerName,
	cust.DistributorId as DistributorId,
	cust.IsArbitrary as CustomerIsArbitrary,	
	cast(CASE WHEN rol.RoleName IS NULL THEN 0 ELSE 1 END as bit) AS IsPhoenixUser,
	aspmbrs.LastLoginDate,
	uieCustomField1.InfoText as LeadingText11,--CustomField1,
	uieCustomField2.InfoText as LeadingText12,-- CustomField2,
	uieEmployeeId.InfoText as LeadingText13,-- EmployeeId,
	uieDepartment.InfoText as LeadingText14,-- Department,
	uiePosition.InfoText as LeadingText15,-- Position,
	uieSupervisor.InfoText as LeadingText16,-- Supervisor,
	uieConfirmationEmail.InfoText as LeadingText17,-- ConfirmationEmail	
	(select count(*) from Participants where UserId=usr.Id and IsDeleted=0) as NumberOfParticipations,
	(select count(*) from Enrollments where UserId=usr.Id and IsCancelled=0) as NumberOfEnrollments,
	(select max(Start) from Participants p, ActivityPeriods a where p.UserId=usr.Id and p.IsDeleted=0 and a.ActivityId=p.ActivityId) as LatestCourseDate,
	(select max(Created) from Participants where UserId=usr.Id and IsDeleted=0) as LatestEnrollmentDate,
	usr.ExpiryDate
FROM 
	dbo.aspnet_Users aspusr
	INNER JOIN dbo.Users usr ON usr.MembershipUserId = aspusr.UserName
	INNER JOIN dbo.Customers cust ON cust.Id = usr.CustomerId
	INNER JOIN dbo.aspnet_Membership aspmbrs ON aspmbrs.UserId = aspusr.UserId
	OUTER APPLY (
		SELECT TOP 1 r.* FROM dbo.aspnet_Roles r
		INNER JOIN dbo.aspnet_UsersInRoles aspuir ON aspuir.RoleId = r.RoleId
		WHERE aspuir.UserId = aspusr.UserId AND r.RoleName = 'PhoenixUser') as rol 
		LEFT OUTER JOIN dbo.aspnet_Roles asprr ON asprr.RoleId = rol.RoleId
	OUTER APPLY (
		SELECT TOP 1 uie.InfoText FROM dbo.UserInfoElements uie WHERE uie.UserId = usr.Id and uie.LeadingTextId = 11
	) uieCustomField1
	OUTER APPLY (
		SELECT TOP 1 uie.InfoText FROM dbo.UserInfoElements uie WHERE uie.UserId = usr.Id and uie.LeadingTextId = 12
	) uieCustomField2
	OUTER APPLY (
		SELECT TOP 1 uie.InfoText FROM dbo.UserInfoElements uie WHERE uie.UserId = usr.Id and uie.LeadingTextId = 13
	) uieEmployeeId
	OUTER APPLY (
		SELECT TOP 1 uie.InfoText FROM dbo.UserInfoElements uie WHERE uie.UserId = usr.Id and uie.LeadingTextId = 14
	) uieDepartment
	OUTER APPLY (
		SELECT TOP 1 uie.InfoText FROM dbo.UserInfoElements uie WHERE uie.UserId = usr.Id and uie.LeadingTextId = 15
	) uiePosition
	OUTER APPLY (
		SELECT TOP 1 uie.InfoText FROM dbo.UserInfoElements uie WHERE uie.UserId = usr.Id and uie.LeadingTextId = 16
	) uieSupervisor
	OUTER APPLY (
		SELECT TOP 1 uie.InfoText FROM dbo.UserInfoElements uie WHERE uie.UserId = usr.Id and uie.LeadingTextId = 17
	) uieConfirmationEmail
GO