If exists(select * from sysobjects where name like 'v_Phoenix_report_prepaidClassroms' and xtype like 'V')
	drop view v_Phoenix_report_prepaidClassroms
GO

/****** Object:  View [dbo].[v_Phoenix_report_prepaidClassroms]    Script Date: 18-Feb-14 15:50:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [v_Phoenix_report_prepaidClassroms] as
	with totals as(
		select temp.OrderId, temp.ActivityId, temp.CurrencyCodeId, c.Value as currency, sum(temp.Price) price
		from(
			select op.orderid, p.ActivityId, op.CurrencyCodeId, sum(op.price) Price from Orders_Participants op INNER JOIN Participants p ON p.Id=op.ParticipantId
			group by op.OrderId, p.ActivityId, op.CurrencyCodeId
			union all
			select oa.OrderId, oa.ActivityId, oa.CurrencyCodeId, sum(oa.Price) from Orders_Activities oa
			group by oa.OrderId, oa.ActivityId, oa.CurrencyCodeId
		) as temp
		inner join Codes c on c.Id = temp.CurrencyCodeId
		group by temp.OrderId, temp.ActivityId, temp.CurrencyCodeId, c.Value
	)
	select 
		c.Name as CustomerName,
		c.ExternalCustomerId as 'AccountNumber',
		[dbo].[fn_article_path](art.Id) as 'Article',
		o.WorkOrder as 'WorkOrder',
		a.Name as Course,
		o.Title as OrderTitle,
		d.Name as distributor,
		d.Id as distributorId,
		convert(date, MaxPeriod.EndDate) ClassroomDate,
		convert(date, o.OrderDate) OrderDate,
		o.InvoiceNumber,
		case when o.IsCreditNote = 1 then -1
		else 1
		end *
		isNull(t.Price, 0) Amount,
		t.Currency
	from totals t 
		inner join Activities a on a.Id = t.ActivityId
		inner join customerArticles cart on cart.id = a.CustomerArticleId
		inner join Articles art on art.id = cart.ArticleCopyId
		inner join Orders o on o.id = t.OrderId
		inner join Customers c on c.id = o.CustomerId
		inner join Distributors d on d.id = c.DistributorId
		cross apply(
			select max(isnull(ap.[End], ap.[start])) EndDate 
			from ActivityPeriods ap 
			where ap.ActivityId = a.Id
			) as MaxPeriod
	where 
		a.IsDeleted = 0
		and o.[Status] = 4
		and art.ArticleTypeId not in(1) 			-- for now just excluding e-courses. Will add more 'filters' if requested.
		and o.OrderDate < MaxPeriod.EndDate