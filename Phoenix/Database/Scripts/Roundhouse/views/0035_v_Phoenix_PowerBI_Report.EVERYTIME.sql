If exists(select * from sysobjects where name like 'v_Phoenix_PowerBI_Report' and xtype like 'V')
	drop view v_Phoenix_PowerBI_Report
GO


CREATE view [dbo].[v_Phoenix_PowerBI_Report] 
as 

SELECT p.Created AS EnrollmentDate, u.MembershipUserId AS Username, c.Name AS CustomerName, c.IsArbitrary, 
at.Name,
pc.ProductCodePrefix+prod.ProductNumber AS ProductNumber,
op.WorkOrder,
d.Name AS DistributorName,
CASE WHEN p.CurrencyCodeId=260 THEN 9.5 ELSE 1 END * p.UnitPrice AS UnitPrice,
--CASE WHEN art.ArticleTypeId IN (1) THEN p.Created ELSE (SELECT MAX(ap.[End]) FROM ActivityPeriods ap WHERE ap.ActivityId=a.Id AND ap.[End] IS NOT NULL) END AS CourseDate,
CASE WHEN art.ArticleTypeId IN (1) OR LastActivityPeriod.[End] IS NULL THEN op.IncomeDate ELSE LastActivityPeriod.[End] END AS CourseDate,
op.InvoiceStatus,
op.IncomeDate,
at.Name AS ArticleTypeName,
/*
CASE WHEN art.ArticleTypeId IN (1) THEN 
	--CASE WHEN p.Created<DATEADD(Month, DATEDIFF(Month, 0, p.Created), 0 ) + 6 - (DATEPART(Weekday, DATEADD(Month, DATEDIFF(Month, 0, p.Created), 0)) + @@DateFirst + 3) %7 THEN -- Less than first Tuesday in this month 
	CASE WHEN op.IncomeDate<DATEADD(DAY,1,DATEADD(WEEK, DATEDIFF(WEEK, 0, DATEADD(DAY, 6 - DATEPART(DAY, op.IncomeDate), op.IncomeDate)), 0)) THEN -- Less than first Tuesday in this month 		
		EOMONTH(DATEADD(month,-1, op.IncomeDate))
	ELSE
		EOMONTH(op.IncomeDate)
	END
ELSE 
	EOMONTH((SELECT MAX(ap.[End]) FROM ActivityPeriods ap WHERE ap.ActivityId=a.Id AND ap.[End] IS NOT NULL))
END AS IncomeMonth
*/

CASE WHEN art.ArticleTypeId IN (1) THEN --Elearning
	(SELECT MIN([AccountingDate]) FROM [AccountingCutoffDates] WHERE [MaxDateOfActivity]>=CAST(op.IncomeDate AS DATE))		
--WHEN art.ArticleTypeId IN (2) THEN --Classroom
ELSE

	--Invoicedate er senere enn siste klasseromsdato, da er det invoicedate som bestemmer
	CASE WHEN LastActivityPeriod.[End] IS NOT NULL AND op.IncomeDate IS NOT NULL AND op.IncomeDate > LastActivityPeriod.[End] THEN
		EOMONTH(op.IncomeDate)
	ELSE
		EOMONTH(ISNULL(LastActivityPeriod.[End],op.IncomeDate))
	END
--ELSE 
--	EOMONTH(ISNULL(LastActivityPeriod.[End],op.IncomeDate))
END AS IncomeMonth

--p.Created
--* 
FROM Participants p 
INNER JOIN Users u ON U.Id=P.UserId
INNER JOIN Customers c ON c.Id=p.CustomerId
INNER JOIN Activities a ON a.Id=p.ActivityId
INNER JOIN CustomerArticles ca ON ca.Id=a.CustomerArticleId
INNER JOIN Articles art ON art.Id=ca.ArticleOriginalId
INNER JOIN ArticleTypes at ON art.ArticleTypeId=at.Id
INNER JOIN Products prod ON prod.Id=art.ProductId
INNER JOIN ProductCategories pc ON pc.Id=prod.ProductCategoryId
INNER JOIN Distributors d on d.Id=c.DistributorId
OUTER APPLY (SELECT TOP 1 CASE WHEN Status IN (3,4) THEN o.OrderDate ELSE NULL END AS IncomeDate, o.WorkOrder, CASE WHEN Status=4 and IsCreditNote=0 THEN 'Invoiced' WHEN Status=4 and IsCreditNote=1 THEN 'Credited' WHEN Status=1 THEN 'Draft' WHEN Status=2 THEN 'Transferred' WHEN Status=3 THEN 'Not Invoiced' WHEN Status=5 THEN 'Rejected' ELSE 'N/A' END AS InvoiceStatus FROM Orders_Participants op INNER JOIN Orders o ON o.Id=op.OrderId WHERE op.ParticipantId=p.Id ORDER BY OrderId DESC) AS op
OUTER APPLY (SELECT TOP 1 ap.[End] FROM ActivityPeriods ap WHERE ap.ActivityId=a.Id AND ap.[End] IS NOT NULL ORDER BY ap.[End] DESC) AS LastActivityPeriod
WHERE p.IsNotBillable=0 AND p.IsDeleted=0


--Fixed Price
UNION 
SELECT

op.OrderDate AS EnrollmentDate, 'FixedPrice-' + CAST(oa.Id AS VARCHAR(255)) AS Username, c.Name AS CustomerName, c.IsArbitrary, 

at.Name,
pc.ProductCodePrefix+prod.ProductNumber AS ProductNumber,
op.WorkOrder,
d.Name AS DistributorName,
CASE WHEN oa.CurrencyCodeId=260 THEN 9.5 ELSE 1 END * oa.Price AS UnitPrice,
op.IncomeDate AS CourseDate,
op.InvoiceStatus,
op.IncomeDate,
at.Name AS ArticleTypeName,
(SELECT MIN([AccountingDate]) FROM [AccountingCutoffDates] WHERE [MaxDateOfActivity]>=CAST(op.IncomeDate AS DATE)) AS IncomeMonth


FROM Orders_Activities oa
CROSS APPLY (SELECT OrderDate, CustomerId, CASE WHEN Status IN (3,4) THEN o.OrderDate ELSE NULL END AS IncomeDate, o.WorkOrder, CASE WHEN Status=4 and IsCreditNote=0 THEN 'Invoiced' WHEN Status=4 and IsCreditNote=1 THEN 'Credited' WHEN Status=1 THEN 'Draft' WHEN Status=2 THEN 'Transferred' WHEN Status=3 THEN 'Not Invoiced' WHEN Status=5 THEN 'Rejected' ELSE 'N/A' END AS InvoiceStatus FROM Orders o WHERE o.Id=oa.OrderId) AS op
INNER JOIN Customers c ON c.Id=op.CustomerId
INNER JOIN Activities a ON a.Id=oa.ActivityId
INNER JOIN CustomerArticles ca ON ca.Id=a.CustomerArticleId
INNER JOIN Articles art ON art.Id=ca.ArticleOriginalId
INNER JOIN ArticleTypes at ON art.ArticleTypeId=at.Id
INNER JOIN Products prod ON prod.Id=art.ProductId
INNER JOIN ProductCategories pc ON pc.Id=prod.ProductCategoryId
INNER JOIN Distributors d on d.Id=c.DistributorId



GO
