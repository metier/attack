If exists(select * from sysobjects where name like 'v_Phoenix_Searchable_Products' and xtype like 'V')
	drop view v_Phoenix_Searchable_Products
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW v_Phoenix_Searchable_Products
AS 
SELECT	p.*,
		CONCAT(c.ProductCodePrefix, REPLICATE('0', 2 - LEN(p.ProductNumber)), p.ProductNumber) AS ProductCode,
		t.Name as ProductTypeName,
		c.Name as ProductCategoryName,
		c.[Description] ProductCategoryDescription,
		c.ProductCodePrefix
FROM	
		dbo.Products AS p
		INNER JOIN ProductTypes t
		ON p.ProductTypeId = t.id
		INNER JOIN ProductCategories AS c
		ON p.ProductCategoryId = c.Id 
 
GO

