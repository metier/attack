If exists(select * from sysobjects where name like 'v_Phoenix_CurrentActivityOrders' and xtype like 'V')
	drop view v_Phoenix_CurrentActivityOrders
GO

/****** Object:  View [dbo].[v_Phoenix_CurrentActivityOrders]    Script Date: 18-Feb-14 15:44:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

CREATE VIEW [dbo].[v_Phoenix_CurrentActivityOrders] as 
with activityOrders as (
	select max(OrderId) maxOrderId, ActivityId 
	from [Orders_Activities]
	group by ActivityId
)
SELECT o.*, ao.ActivityId
FROM activityOrders ao
inner join [Orders] o
on ao.maxOrderId = o.Id

GO


