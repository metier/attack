If exists(select * from sysobjects where name like 'v_Phoenix_Report_HubspotParticipants' and xtype like 'V')
	drop view v_Phoenix_Report_HubspotParticipants
GO


CREATE view [dbo].[v_Phoenix_Report_HubspotParticipants] 
as 

WITH Products AS (select dbo.fn_product_path(productId) as ArticleCode , * from Articles) 

	select stuff(
		(select distinct ';' + REPLACE(pr.ArticleCode,'PM24','PM22;PM23')
			from Participants p
			inner join activities a
			on a.id = p.ActivityId
			inner join CustomerArticles ca
			on ca.id = a.CustomerArticleId
			inner join Products pr
			on pr.id = ca.ArticleCopyId
			WHERE 
				p.UserId=u.Id and		
				p.IsDeleted = 0 and 
				a.IsDeleted = 0 and
				ca.IsDeleted = 0 and
				pr.IsDeleted = 0
							
			for xml path(''),type).value('text()[1]','nvarchar(max)'),1,1,N'') as CoursesEnrolled,
			stuff(
			(select distinct ';' + REPLACE(pr.ArticleCode,'PM24','PM22;PM23')
			from Participants p
			inner join activities a
			on a.id = p.ActivityId
			inner join CustomerArticles ca
			on ca.id = a.CustomerArticleId
			inner join Products pr
			on pr.id = ca.ArticleCopyId
			WHERE 
				p.UserId=u.Id and		
				p.IsDeleted = 0 and 
				a.IsDeleted = 0 and
				ca.IsDeleted = 0 and
				pr.IsDeleted = 0 and
				p.StatusCodeValue = 'COMPLETED'
							
			for xml path(''),type).value('text()[1]','nvarchar(max)'),1,1,N'') as CoursesCompleted,

	u.id as UserId, u.FirstName, u.LastName, u.MembershipUserId as UserName, su.Email, u.PhoneNumber, c.DistributorId, c.Name as Customer, c.Id as CustomerId, c.IsArbitrary, d.Name as Distributor,
	(select min(Created) from Participants where IsDeleted=0 and UserId=u.Id) AS FirstEnrollmentDate,
	(select max(Created) from Participants where IsDeleted=0 and UserId=u.Id) AS LastEnrollmentDate
	from 
		Users u
		inner join v_phoenix_searchable_Users su on u.id = su.Id
		inner join Customers c on c.Id=u.CustomerId
		inner join Distributors d on d.Id = c.DistributorId

GO


