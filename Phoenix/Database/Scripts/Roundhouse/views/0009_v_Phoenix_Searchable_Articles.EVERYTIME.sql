If exists(select * from sysobjects where name like 'v_Phoenix_Searchable_Articles' and xtype like 'V')
	drop view v_Phoenix_Searchable_Articles
GO

/****** Object:  View [dbo].[v_Phoenix_Searchable_Articles]    Script Date: 18-Feb-14 15:51:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_Phoenix_Searchable_Articles] 
AS 
SELECT	[Id],
		ProductId,
		[Path], 
		[Title], 
		[Language], 
		StatusCode, 
		ArticleType,
		CourseVersion

FROM	[dbo].[v_Phoenix_Searchable_Articles_All]
WHERE ArticleCopyId IS NULL
 
GO


