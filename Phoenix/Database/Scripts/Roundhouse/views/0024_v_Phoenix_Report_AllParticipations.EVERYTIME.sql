If exists(select * from sysobjects where name like 'v_Phoenix_Report_AllParticipants' and xtype like 'V')
	drop view v_Phoenix_Report_AllParticipants
GO

create view [dbo].v_Phoenix_Report_AllParticipants 
as 
with _participants as(
	select 
		p.id as ParticipantId,
		p.UserId,
		art.ProductId,
		a.id as ActivityId,
		a.Name as ActivityName,
		a.RcoCourseId,
		p.StatusCodeValue as [Status],
		p.Created EnrollmentDate,
		art.ArticleTypeId,
		art.id as ArticleId,
		art.Title as ArticleName,
		l.Name as [Language],
		periods.startDate,
		periods.enddate
	from participants p
	inner join activities a
	on a.id = p.ActivityId
	inner join CustomerArticles ca
	on ca.id = a.CustomerArticleId
	inner join Articles art
	on art.id = ca.ArticleCopyId
	inner join Languages l
	on art.LanguageId = l.Id
	outer apply(
		select min(ap.Start) as startDate, max(ap.[End]) as enddate	from ActivityPeriods ap 
		where ap.ActivityId = p.ActivityId) as periods
	where
		p.IsDeleted = 0 and 
		a.IsDeleted = 0 and
		ca.IsDeleted = 0 and
		art.IsDeleted = 0
),
UserProducts as(
	select distinct 
		UserId, 
		su.Email, 
		u.MembershipUserId as UserName,
		u.FirstName,
		u.LastName,
		u.PhoneNumber,
		u.ExpiryDate,
		su.LastLoginDate,
		u.customerId, 
		ProductId
	from _participants p
	inner join Users u
	on u.id = p.UserId
	inner join v_phoenix_searchable_Users su
	on u.id = su.Id
), additionalFields as 
(
	select usrinfo.UserId, usrinfo.LeadingTextId, usrinfo.InfoText
	from UserInfoElements usrinfo
)
select 
	   d.Name as Distributor,
	   d.Id as DistributorId,
	   dbo.fn_product_path(up.productId) AS ProductPath,
	   up.ProductId,
	   elearning.CourseLink,
	   elearning.ActivityName as ActivityNameE,
	   classroom.ActivityName as ActivityNameC,
	   exam.ActivityName ActivityNameEx,
	   elearning.ArticleName as ArticleNameE,
	   classroom.ArticleName as ArticleNameC,
	   exam.ArticleName as ArticleNameEx,
	   elearning.[Status] as StatusE,
	   classroom.[Status] as StatusC,
	   exam.[Status] as StatusEx,
	   up.UserName,
	   up.Email,
	   up.FirstName,
	   up.LastName,
	   up.PhoneNumber,
	   up.ExpiryDate,
	   up.LastLoginDate,
	   c.Name as Customer,
	   c.Id as CustomerId,
	   ParentCustomer.Name as ParentOrg,
	   case when elearning.NumberOfCompletedLessons is Null then null
	   else concat(elearning.NumberOfCompletedLessons, ' av ', elearning.NumberOfLessons)
	   end as ELessons,
	   elearning.Points,
	   elearning.StartDate as EStartDate,
	   classroom.StartDate as CStartDate,
	   exam.StartDate as ExStartDate,
	   elearning.EndDate as EEndDate,
	   classroom.EndDate as CEndDate,
	   exam.EndDate as ExEndDate,
	   elearning.CompletedDate as ECompleteDate,
	   elearning.EnrollmentDate as EnrollmentDateE,
	   classroom.EnrollmentDate as EnrollmentDateC,
	   exam.EnrollmentDate as EnrollmentDateEx,
	   IsNull(Cast(am.FirstName as varchar(max)) + ' ','') + IsNull(Cast(am.LastName as varchar(max)),'') AS AM,
	   IsNull(Cast(coordinator.FirstName as varchar(max)) + ' ','') + IsNull(Cast(coordinator.LastName as varchar(max)),'') AS Coordinator,
	   elearning.[Language] as CustArtLanguageE,
	   classroom.[Language] as CustArtLanguageC,
	   exam.[Language] as CustArtLanguageEx,
	   part_info.IsAllowUsersToSelfEnrollToCourses  AS SelfEnrollmentCourses,
	   part_info.IsAllowUsersToSelfEnrollToExams  AS SelfEnrollmentExams,
	   userroles.Roles as UserRole,
	   custom1UsrInfo.InfoText as CustomField1,
	   custom2UsrInfo.InfoText as CustomField2,
	   empUsrInfo.InfoText as EmployeeId,
	   departmentUsrInfo.InfoText as Department,
	   positionUsrInfo.InfoText as Position,
	   supervisorUsrInfo.InfoText as Supervisor,
	   confirmationUsrInfo.InfoText as ConfirmationEmail,
	   c.IsArbitrary,
	   isNull(isNull(classroom.EnrollmentDate, elearning.EnrollmentDate), exam.EnrollmentDate) as EnrollmentDate
from UserProducts up
inner join Customers c
on c.id = up.CustomerId
inner join Distributors d
on c.DistributorId = d.Id
left outer join additionalFields custom1UsrInfo on up.userId = custom1UsrInfo.UserId and custom1UsrInfo.LeadingTextId = 11 -- 'Custom field 1 ID' LeadingText
left outer join additionalFields custom2UsrInfo on up.userId = custom2UsrInfo.UserId and custom2UsrInfo.LeadingTextId = 12 -- 'Custom field 2 ID' LeadingText
left outer join additionalFields empUsrInfo on up.userId = empUsrInfo.UserId and empUsrInfo.LeadingTextId = 13 -- 'Employee ID' LeadingText
left outer join additionalFields departmentUsrInfo on up.userId = departmentUsrInfo.UserId and departmentUsrInfo.LeadingTextId = 14 -- 'Department ID' LeadingText
left outer join additionalFields positionUsrInfo on up.userId = positionUsrInfo.UserId and positionUsrInfo.LeadingTextId = 15 -- 'Position ID' LeadingText
left outer join additionalFields supervisorUsrInfo on up.userId = supervisorUsrInfo.UserId and supervisorUsrInfo.LeadingTextId = 16 -- 'Supervisor ID' LeadingText
left outer join additionalFields confirmationUsrInfo on up.userId = confirmationUsrInfo.UserId and confirmationUsrInfo.LeadingTextId = 17 -- 'Confirmation email ID' LeadingText
left outer join ParticipantInfoDefinitions part_info ON c.ParticipantInfoDefinitionId = part_info.Id
outer apply (
	select top 1 contper.CustomerId, contper_user.FirstName, contper_user.LastName
	from MetierContactPersons contper
	inner join Users contper_user ON contper.UserId = contper_user.Id
	where contper.ContactType = 0 and c.Id = contper.CustomerId
	) as am
outer apply (
	select top 1 contper.CustomerId, contper_user.FirstName, contper_user.LastName		--Don't really think I need the 'top 1', but just want to make absolutely sure we don't get duplicate rows
	from MetierContactPersons contper
	inner join Users contper_user ON contper.UserId = contper_user.Id
	where contper.ContactType = 1 and c.Id = contper.CustomerId
	) as coordinator 

outer apply(
	select * from Customers pc
	where pc.id = c.ParentId
) as ParentCustomer
outer apply(
	select top 1 
		p.ActivityName,
		p.ArticleName,
		p.[Status],
		p.EnrollmentDate,
		p.[Language],
		rco.[Version] as CourseLink,
		course.points,
		course.completedDate,
		(select count(*) from ScormPerformances spf inner join rcos r on spf.RcoId = r.Id where r.ParentId = rco.Id and spf.UserId = up.userId and spf.[status] = 'C') as NumberOfCompletedLessons, 	 
		(select count(*) from rcos r where r.ParentId = rco.id) as NumberOfLessons,
		p.startDate,
		p.enddate
	from _participants p
	left outer join v_Phoenix_SearchableRcos rco 
		ON p.RcoCourseId = rco.Id
	outer apply(
		select max(spf.Score) as Points, max(spf.CompletedDate) CompletedDate 
		from ScormPerformances spf where spf.RcoId = rco.id and spf.UserId = p.userId
		) as course
	where up.UserId = p.UserId
		and p.ProductId = up.ProductId
		and p.ArticleTypeId = 1
	order by p.EnrollmentDate desc
	) elearning
outer apply(
	select top 1 * from _participants p
	where up.UserId = p.UserId
	and p.ProductId = up.ProductId
	and p.ArticleTypeId = 2
	order by p.EnrollmentDate desc
	) classroom
outer apply(
	select top 1 * from _participants p
	where up.UserId = p.UserId
	and p.ProductId = up.ProductId
	and p.ArticleTypeId in(3, 4, 5, 6, 8, 9)
	order by p.EnrollmentDate desc
	) exam
outer apply (
	select stuff(
		(select N', ' + asp_role.RoleName 
			from vw_aspnet_Users asp_user 
			left outer join vw_aspnet_UsersInRoles asp_userrole ON asp_user.UserId = asp_userrole.UserId
			left outer join vw_aspnet_Roles asp_role ON asp_userrole.RoleId = asp_role.RoleId
			where up.UserName = asp_user.UserName
			for xml path(''),type).value('text()[1]','nvarchar(max)'),1,2,N'') as Roles
	) as userroles

