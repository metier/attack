If exists(select * from sysobjects where name like 'v_Phoenix_Searchable_CustomerArticles' and xtype like 'V')
	drop view v_Phoenix_Searchable_CustomerArticles
GO


CREATE VIEW [dbo].[v_Phoenix_Searchable_CustomerArticles] 
AS 
SELECT	ca.Id as Id,
		ca.FixedPrice as FixedPrice,
		ca.UnitPrice as UnitPrice,
		ca.CurrencyCodeId as CurrencyCodeId,
		a.Title as Title,
		c.Id as CustomerId,
		c.Name as CustomerName,
		c.DistributorId as DistributorId,
		ca.ArticleCopyId as ArticleCopyId,
		ca.ArticleOriginalId as ArticleOriginalId,
		coursercos.Id as RcoCourseId,
		coursercos.Name as CourseName,
		coursercos.Version as CourseVersion,
		examrcos.Id as RcoExamContainerId,
		examrcos.Name as RcoExamContainerName,
		sc.Id StatusCodeId,
		sc.[Description] as [Status],
		ca.Created as Created
FROM [dbo].[CustomerArticles] ca
INNER JOIN [dbo].[Customers] c ON c.Id = ca.CustomerId
INNER JOIN [dbo].[Articles] a ON a.Id = ca.ArticleCopyId
LEFT OUTER JOIN [dbo].[Article_Elearnings] ea ON ea.Id = a.Id
LEFT OUTER JOIN [dbo].[Article_Mockexams] ma ON ma.Id = a.Id
LEFT OUTER JOIN [dbo].[Rcos] coursercos ON coursercos.Id = ea.RcoCourseId
LEFT OUTER JOIN [dbo].[Rcos] examrcos ON examrcos.Id = ma.RcoExamContainerId
LEFT OUTER JOIN Codes sc on sc.Id = a.StatusCodeId and sc.CodeTypeId = 2
WHERE ca.IsDeleted = 0
GO 



