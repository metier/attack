If exists(select * from sysobjects where name like 'v_Phoenix_ProposedOrderLines' and xtype like 'V')
	drop view v_Phoenix_ProposedOrderLines
GO

/****** Object:  View [dbo].[v_Phoenix_ProposedOrderLines]    Script Date: 18-Feb-14 15:49:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[v_Phoenix_ProposedOrderLines]
as
 
WITH participants as(
       SELECT
             p.Id ParticipantId
             ,isNULL(pas.Name, p.ActivityName) Title
             ,p.FirstName + ' ' + p.LastName DetailedText                       -- This is the text that will appear on the detail-lines of the invoices
             ,pas.Id ActivitySetId
             ,p.ActivityId
             ,c.DistributorId
             ,c.Id CustomerId          
             ,c.ProgramCoordinatorId CoordinatorId
             ,c.ProgramCoordinatorFullName CoordinatorFullName
             ,c.Name
			 ,c.IsArbitrary

             ,Case when c.orderRefLeadingTextId is not NULL then OrderRefLeadingText.InfoText
              else c.orderRefCustomText
              end YourOrder

             ,Case when c.yourRefLeadingTextId is not NULL then YourRefLeadingText.InfoText
              else c.yourRefCustomText
              end YourRef

             ,isNull(c.OurRef, '') OurRef
             ,c.BundledBilling
             ,c.customField1IsGroupByOnInvoice
             ,c.customField2IsGroupByOnInvoice
             ,c.EmployeeIdIsGroupByOnInvoice
             ,c.DepartmentIsGroupByOnInvoice
             ,c.PositionIsGroupByOnInvoice
             ,c.SupervisorIsGroupByOnInvoice
             ,c.ConfirmationEmailIsGroupByOnInvoice

             ,p.CustomField1InfoText
             ,p.CustomField2InfoText
             ,p.EmployeeIdInfoText
             ,p.DepartmentInfoText
             ,p.PositionInfoText
             ,p.SupervisorInfoText
             ,p.ConfirmationEmailInfoText

             ,p.UserId participantUserId
             ,p.CurrencyCodeId
             ,p.UnitPrice
			 ,p.EarliestInvoiceDate
			 ,p.OnlinePaymentReference
       FROM
             v_Phoenix_Participants p
		left join ActivitySets pas
             on p.ActivitySetId = pas.Id
		inner join v_Phoenix_Customers c
             on c.Id = p.customerId

		outer apply
		            (      select ie.InfoText from dbo.ParticipantInfoElements  ie
                           where ie.ParticipantId = p.Id
                           and ie.LeadingTextId = c.OrderRefLeadingTextId
                    ) OrderRefLeadingText
		outer apply
		            (      select ie.InfoText from dbo.ParticipantInfoElements  ie
                           where ie.ParticipantId = p.Id
						   and ie.LeadingTextId = c.YourRefLeadingTextId
                    ) YourRefLeadingText
       WHERE
			 p.IsNotBillable = 0
             and p.isInvoiced = 0
			 and p.EarliestInvoiceDate <= getDate() 

       UNION
 
       SELECT
               -1
             ,isNULL(pas.Name, a.Name)                             -- Title             
             ,concat(a.Name, ' (Fixed price)')
             ,pas.Id ActivitySetId
             ,a.Id       
             ,c.DistributorId          
             ,c.Id CustomerId          
             ,c.ProgramCoordinatorId CoordinatorId
             ,c.ProgramCoordinatorFullName CoordinatorFullName    
             ,c.Name
			 ,c.IsArbitrary

             ,isNULL(c.orderRefCustomText, '') YourOrder    -- Use it if it exists, otherwise use empty string
             ,isNULL(c.yourRefCustomText, '')  YourRef             -- Use it if it exists, otherwise use empty string
 
             ,isNull(c.OurRef, '')
             ,c.BundledBilling
             ,c.customField1IsGroupByOnInvoice
             ,c.customField2IsGroupByOnInvoice
             ,c.EmployeeIdIsGroupByOnInvoice
             ,c.DepartmentIsGroupByOnInvoice
             ,c.PositionIsGroupByOnInvoice
             ,c.SupervisorIsGroupByOnInvoice
             ,c.ConfirmationEmailIsGroupByOnInvoice        
            
             ,''                                                                       -- CustomField1InfoText
             ,''                                                                       -- CustomField2InfoText
             ,''                                                                       -- EmployeeIdInfoText
             ,''                                                                       -- DepartmentInfoText
             ,''                                                                       -- PositionInfoText
             ,''                                                                       -- SupervisorInfoText
             ,''                                                                       -- ConfirmationEmailInfoText
             ,-1                                                                       -- participantUserId
             ,a.CurrencyCodeId
             ,a.FixedPrice
			 ,a.EarliestInvoiceDate,
			 ''																		   -- OnlinePaymentReference
       FROM Activities a
             inner join CustomerArticles ca
                    on     a.CustomerArticleId = ca.Id
             inner join ActivitySets_Activities aa
                    on     aa.ActivityId = a.Id
             inner join ActivitySets pas
                    on     aa.ActivitySetId = pas.Id and
                           pas.CustomerId = ca.CustomerId
             inner join v_Phoenix_Customers c
                    on c.Id = ca.CustomerId                
			 outer apply(
				select top 1 
						oa.ActivityId, 
						o.IsCreditNote, 
						o.[Status]
				from Orders_Activities oa
				inner join Orders o
				on o.id = oa.OrderId
				where oa.ActivityId = a.Id
				order by o.Id desc
			 ) CurrentActivityOrder
       WHERE
             a.FixedPriceNotBillable = 0
             and a.EarliestInvoiceDate <= getDate()
             and a.isDeleted = 0
             and a.id not in(
                    select CurrentActivityOrder.ActivityId
                    where 
							CurrentActivityOrder.IsCreditNote = 0 or
			  				(CurrentActivityOrder.IsCreditNote = 1 and CurrentActivityOrder.[Status] < 2)
					)
)
SELECT
       concat(
			 '{DistributorId:',
             distributorId, ',',
			 'CustomerId:',
             customerId, ',',
			 'ActivitySetId:',
             ActivitySetId, ',',
			 'CurrencyCodeId:',
             CurrencyCodeId, ',',
			 'Signature:"',

			 replace(
				replace(
					 replace(
						 concat(
							 isNULL(YourOrder, ''), '-',
							 isNULL(YourRef, ''), '-',
							 case when BundledBilling = 0 then concat(participantUserId, '-') end,
							 case when customField1IsGroupByOnInvoice = 1 then concat(customField1InfoText, '-') end,
							 case when customField2IsGroupByOnInvoice = 1 then concat(customField2InfoText, '-') end,
							 case when EmployeeIdIsGroupByOnInvoice = 1 then concat(EmployeeIdInfoText, '-') end,
							 case when DepartmentIsGroupByOnInvoice = 1 then concat(DepartmentInfoText, '-') end,
							 case when PositionIsGroupByOnInvoice = 1 then concat(PositionInfoText, '-') end,
							 case when SupervisorIsGroupByOnInvoice = 1 then concat(SupervisorInfoText, '-') end,
							 case when ConfirmationEmailIsGroupByOnInvoice = 1 then concat(ConfirmationEmailInfoText, '-') end,
							 isNULL(OnlinePaymentReference, '')
							),
						'\', '\\'),		-- escape any backslash characters
					'/', '+'),			-- escape any forward slashes 
				'"', '\"'),				-- escape any double quotes
			 '"}'						-- Close JSON string
             )     
             as GroupByKey
             ,*
FROM Participants

GO
