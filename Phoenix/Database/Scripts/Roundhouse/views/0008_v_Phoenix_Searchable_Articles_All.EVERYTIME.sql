If exists(select * from sysobjects where name like 'v_Phoenix_Searchable_Articles_All' and xtype like 'V')
	drop view v_Phoenix_Searchable_Articles_All
GO

/****** Object:  View [dbo].[v_Phoenix_Searchable_Articles_All]    Script Date: 18-Feb-14 15:51:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_Phoenix_Searchable_Articles_All] 
AS 
select	DISTINCT art.Id, prod.Id as ProductId,
		cat.ProductCodePrefix + prod.ProductNumber + RTRIM(arttype.CharCode) + '-' + { fn UCASE(lan.Identifier) }  as Path, 
		cat.ProductCodePrefix + prod.ProductNumber + RTRIM(arttype.CharCode) as ArticleCode, 
		art.Title, 
		lan.Name as Language, 
		code.Description as StatusCode, 
		arttype.Name as ArticleType,
		arttype.Id as ArticleTypeId,
		custart.ArticleCopyId,
		rco.Version as CourseVersion,
		art.ProductDescriptionId,
		art.LanguageId,
		art.IsUserCompetenceInfoRequired
from	dbo.Articles as art 
		inner join dbo.Languages as lan on art.LanguageId = lan.ID 
		inner join dbo.Products as prod on art.ProductId = prod.Id 
		inner join dbo.ProductCategories as cat on prod.ProductCategoryId = cat.Id 
		inner join dbo.Codes as code on art.StatusCodeId = code.Id 
		inner join dbo.ArticleTypes as arttype on art.ArticleTypeId = arttype.Id
		left outer join dbo.CustomerArticles as custart on custart.ArticleCopyId = art.Id
		left outer join Article_Elearnings as elearning on art.Id = elearning.Id
		left outer join Rcos rco on elearning.RcoCourseId = rco.Id
where 
	art.IsDeleted = 0 and 
	isNull(custart.IsDeleted, 0) = 0 
GO


