If exists(select * from sysobjects where name like 'v_Phoenix_FutureOrders' and xtype like 'V')
	drop view v_Phoenix_FutureOrders
GO

/****** Object:  View [dbo].[v_Phoenix_FutureOrders]    Script Date: 18-Feb-14 15:50:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [v_Phoenix_FutureOrders]
(
       GroupByKey,
       DistributorId,
       Title,
       Name,
       CustomerId,
       CoordinatorId,
       CoordinatorFullName,
       YourOrder,
       YourRef,
       OurRef,
       TotalAmount,
       CurrencyCode,
       LastModified,
       OrderStatus,
       BundledBilling,
       CustomerType
)
as
 
SELECT 
       ol.GroupByKey,
       ol.DistributorId,
       Title,
       ol.Name,
       ol.CustomerId,
       isNull(CoordinatorId, -1),
       isNull(CoordinatorFullName, ''),
       isNULL(ol.YourOrder,''),
       isNull(ol.YourRef,'') ,
       isNull(ol.OurRef,''),
       sum(UnitPrice) TotalAmount,
       currencyCode.Value,
       max(ol.EarliestInvoiceDate),						-- Using this for 'LastModified'
       0,												-- OrderStatus = Proposed
       ol.BundledBilling,
       case when ol.IsArbitrary = 1 then 0 else 1 end
FROM
       v_Phoenix_FutureOrderLines ol
inner join Codes currencyCode
       ON CurrencyCodeId = currencyCode.Id
GROUP BY
       ol.GroupByKey,
       ol.DistributorId,
       Title,
       ol.Name,
       ol.CustomerId,
       isNull(CoordinatorId, -1),
       isNull(CoordinatorFullName, ''),
       isNULL(ol.YourOrder,''),
       isNull(ol.YourRef,''),
       isNull(ol.OurRef,''),
       currencyCode.Value,
       ol.BundledBilling,
       ol.IsArbitrary
GO


