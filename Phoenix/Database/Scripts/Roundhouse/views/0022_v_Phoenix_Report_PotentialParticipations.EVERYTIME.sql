If exists(select * from sysobjects where name like 'v_Phoenix_Report_PotentialParticipations' and xtype like 'V')
	drop view v_Phoenix_Report_PotentialParticipations
GO

CREATE VIEW [dbo].[v_Phoenix_Report_PotentialParticipations] 
AS 
with additionalFields as 
(
	select usrinfo.UserId, usrinfo.LeadingTextId, usrinfo.InfoText from UserInfoElements usrinfo
)

select c.Id as CustomerId,
	   c.Name as Customer,
	   ump.ProductId as ProductId,
	   dbo.fn_product_path(ump.ProductId) as ProductPath,
	   u.Id as UserId,
	   u.FirstName as FirstName,
	   u.LastName as LastName,
	   aspmbr.Email as Email,
	   u.PhoneNumber,
	   custom1UsrInfo.InfoText as CustomField1,
	   custom2UsrInfo.InfoText as CustomField2,
	   empUsrInfo.InfoText as EmployeeId,
	   departmentUsrInfo.InfoText as Department,
	   positionUsrInfo.InfoText as Position,
	   supervisorUsrInfo.InfoText as Supervisor,
	   confirmationUsrInfo.InfoText as ConfirmationEmail,
	   c.IsArbitrary,
	   ';' + stuff(
		(select distinct ';' + CAST(ar.ProductId AS VARCHAR(255))
			from Participants p
			inner join activities a
			on a.id = p.ActivityId
			inner join CustomerArticles ca
			on ca.id = a.CustomerArticleId			
			inner join Articles ar
			on ar.id = ca.ArticleCopyId			
						
			WHERE 
				p.UserId=u.Id and		
				p.IsDeleted = 0 and 
				a.IsDeleted = 0 and
				ca.IsDeleted = 0 and
				ar.IsDeleted = 0
			for xml path(''),type).value('text()[1]','nvarchar(max)'),1,1,N'') + ';' as ProductIdsEnrolled

from usermissingproducts ump
inner join Users u on ump.UserId = u.Id
inner join Customers c on u.CustomerId = c.Id
left outer join aspnet_Users aspusr ON u.MembershipUserId = aspusr.UserName
left outer join aspnet_Membership aspmbr ON aspmbr.UserId = aspusr.UserId
left outer join additionalFields custom1UsrInfo on u.Id = custom1UsrInfo.UserId and custom1UsrInfo.LeadingTextId = 11 -- 'Custom field 1 ID' LeadingText
left outer join additionalFields custom2UsrInfo on u.Id = custom2UsrInfo.UserId and custom2UsrInfo.LeadingTextId = 12 -- 'Custom field 2 ID' LeadingText
left outer join additionalFields empUsrInfo on u.Id = empUsrInfo.UserId and empUsrInfo.LeadingTextId = 13 -- 'Employee ID' LeadingText
left outer join additionalFields departmentUsrInfo on u.Id = departmentUsrInfo.UserId and departmentUsrInfo.LeadingTextId = 14 -- 'Department ID' LeadingText
left outer join additionalFields positionUsrInfo on u.Id = positionUsrInfo.UserId and positionUsrInfo.LeadingTextId = 15 -- 'Position ID' LeadingText
left outer join additionalFields supervisorUsrInfo on u.Id = supervisorUsrInfo.UserId and supervisorUsrInfo.LeadingTextId = 16 -- 'Supervisor ID' LeadingText
left outer join additionalFields confirmationUsrInfo on u.Id = confirmationUsrInfo.UserId and confirmationUsrInfo.LeadingTextId = 17 -- 'Confirmation email ID' LeadingText
where (u.ExpiryDate is null or u.ExpiryDate >= getdate()) 
group by 
	c.Id, 
	c.Name, 
	ump.ProductId, 
	u.Id, 
	u.FirstName, 
	u.LastName, 
	u.PhoneNumber,
	aspmbr.Email, 
	custom1UsrInfo.InfoText,
	custom2UsrInfo.InfoText,
	empUsrInfo.InfoText,
	departmentUsrInfo.InfoText,
	positionUsrInfo.InfoText,
	supervisorUsrInfo.InfoText,
	confirmationUsrInfo.InfoText,
	c.IsArbitrary
