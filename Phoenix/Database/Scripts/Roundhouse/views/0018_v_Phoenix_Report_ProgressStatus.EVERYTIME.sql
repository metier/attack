-- OLD NAME BEFORE RENAME
If exists(select * from sysobjects where name like 'v_Phoenix_Report_CustomerElearningProgress' and xtype like 'V')
	drop view v_Phoenix_Report_CustomerElearningProgress
GO

-- NEW NAME
If exists(select * from sysobjects where name like 'v_Phoenix_Report_ProgressStatus' and xtype like 'V')
	drop view v_Phoenix_Report_ProgressStatus
GO

CREATE VIEW [dbo].[v_Phoenix_Report_ProgressStatus] 
AS 
with additionalFields as 
(
	select usrinfo.UserId, usrinfo.LeadingTextId, usrinfo.InfoText
	from UserInfoElements usrinfo
), courseLessonPerformances as
(
	select course_per.RcoId as CourseRcoId, course_per.UserId, course_per.Status as CourseStatus, course_per.CompletedDate,
		   lesson_per.Status as LessonStatus
	from ScormPerformances course_per
	inner join Rcos lessonRco on course_per.RcoId = lessonRco.ParentCourseId and lessonRco.Context = 'lesson'
	left outer join ScormPerformances lesson_per ON lessonRco.Id = lesson_per.RcoId AND lesson_per.UserId = course_per.UserId
), 

courseLessonCounts as
(
	select Rcos.ParentCourseId as CourseRcoId, count(*) AS LessonCount		   
	from Rcos
	WHERE Rcos.Context = 'lesson'
	GROUP BY ParentCourseId
), 
	
	
activityDates as
(
	select ap.ActivityId, min(ap.Start) as ActivityStart
	from ActivityPeriods ap
	group by ap.ActivityId
)

select cus.Id as CustomerId,
	   d.Id as DistributorId,
	   cus.Name as Customer,
	   d.Name as Distributor,
	   art.ProductId as ProductId,
	   art.Id as ArticleId,
	   dbo.fn_article_path(art.Id) as ArticleNumber,
	   act.Id as ActivityId,
	   act.Name as ActivityTitle,
	   actDates.ActivityStart as StartDate,
	   usr.Id as UserId,
	   usr.FirstName as FirstName,
	   usr.LastName as LastName,
	   aspmbr.Email as Email,
	   custom1UsrInfo.InfoText as CustomField1,
	   custom2UsrInfo.InfoText as CustomField2,
	   empUsrInfo.InfoText as EmployeeId,
	   departmentUsrInfo.InfoText as Department,
	   positionUsrInfo.InfoText as Position,
	   supervisorUsrInfo.InfoText as Supervisor,
	   confirmationUsrInfo.InfoText as ConfirmationEmail,
	   cus.IsArbitrary,
	   par.Created as EnrollmentDate,
	   CASE 
		WHEN sum(case when perf.LessonStatus = 'C' then 1 else 0 end) >= min(lessonCounts.LessonCount) THEN 'C'
		WHEN sum(case when perf.LessonStatus = 'C' then 1 else 0 end) > 0  THEN 'I'
		ELSE isnull(perf.CourseStatus, 'N') 
	   END as Status,
	   perf.CompletedDate as CompletedDate,
	   sum(case when perf.LessonStatus = 'C' then 1 else 0 end) as NumberOfCompletedLessons,
	   min(lessonCounts.LessonCount) as NumberOfLessons
from Customers cus
inner join Distributors d on cus.DistributorId = d.Id
inner join Participants par on cus.Id = par.CustomerId
inner join Activities act on par.ActivityId = act.Id
inner join CustomerArticles cusart on act.CustomerArticleId = cusart.Id
inner join Articles art on cusart.ArticleCopyId = art.Id
inner join Users usr on par.UserId = usr.Id
inner join aspnet_Users aspusr ON usr.MembershipUserId = aspusr.UserName
inner join aspnet_Membership aspmbr ON aspmbr.UserId = aspusr.UserId
left outer join additionalFields custom1UsrInfo on usr.Id = custom1UsrInfo.UserId and custom1UsrInfo.LeadingTextId = 11 -- 'Custom field 1 ID' LeadingText
left outer join additionalFields custom2UsrInfo on usr.Id = custom2UsrInfo.UserId and custom2UsrInfo.LeadingTextId = 12 -- 'Custom field 2 ID' LeadingText
left outer join additionalFields empUsrInfo on usr.Id = empUsrInfo.UserId and empUsrInfo.LeadingTextId = 13 -- 'Employee ID' LeadingText
left outer join additionalFields departmentUsrInfo on usr.Id = departmentUsrInfo.UserId and departmentUsrInfo.LeadingTextId = 14 -- 'Department ID' LeadingText
left outer join additionalFields positionUsrInfo on usr.Id = positionUsrInfo.UserId and positionUsrInfo.LeadingTextId = 15 -- 'Position ID' LeadingText
left outer join additionalFields supervisorUsrInfo on usr.Id = supervisorUsrInfo.UserId and supervisorUsrInfo.LeadingTextId = 16 -- 'Supervisor ID' LeadingText
left outer join additionalFields confirmationUsrInfo on usr.Id = confirmationUsrInfo.UserId and confirmationUsrInfo.LeadingTextId = 17 -- 'Confirmation email ID' LeadingText
left outer join courseLessonPerformances perf on act.RcoCourseId = perf.CourseRcoId and usr.Id = perf.UserId
left outer join courseLessonCounts lessonCounts on act.RcoCourseId = lessonCounts.CourseRcoId
left outer join activityDates actDates on act.Id = actDates.ActivityId
where art.ArticleTypeId = 1 and -- Type ELearning
	  act.IsDeleted = 0 and cusart.IsDeleted = 0 and par.IsDeleted = 0
group by cus.Id,
		 d.Id,
		 cus.Name,
		 d.Name,
		 art.ProductId,
		 art.Id,
		 act.Id,
		 act.Name,
		 actDates.ActivityStart,
		 usr.Id,
		 usr.FirstName,
		 usr.LastName,
		 aspmbr.Email,
		 custom1UsrInfo.InfoText,
		 custom2UsrInfo.InfoText,
		 empUsrInfo.InfoText,
		 departmentUsrInfo.InfoText,
		 positionUsrInfo.InfoText,
		 supervisorUsrInfo.InfoText,
		 confirmationUsrInfo.InfoText,
		 cus.IsArbitrary,
		 par.Created,
		 perf.CourseStatus,
		 perf.CompletedDate
 
GO


