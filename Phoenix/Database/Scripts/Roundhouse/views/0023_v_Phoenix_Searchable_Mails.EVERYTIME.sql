If exists(select * from sysobjects where name like 'v_Phoenix_Searchable_Mails' and xtype like 'V')
	drop view v_Phoenix_Searchable_Mails
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

CREATE VIEW [dbo].[v_Phoenix_Searchable_Mails] 
AS 
SELECT	
m.Id, 
m.UserId,
u.FirstName,
u.LastName,
m.MessageType, 
m.[Subject], 
m.RecipientEmail, 
m.Created, 
mailStatus.MailStatus,
mailStatus.StatusTime
FROM Mails AS m
LEFT OUTER JOIN Users u ON u.Id = m.UserId

OUTER APPLY (
	SELECT TOP 1 * FROM MailStatuses ms
	WHERE ms.MailId = m.Id
	ORDER BY StatusTime DESC
) as mailStatus

GO


