if exists(select * from sysobjects where name like 'v_Phoenix_report_SafetyCourseParticipants' and xType = 'V')
	drop view v_Phoenix_report_SafetyCourseParticipants
Go

Create View v_Phoenix_report_SafetyCourseParticipants as
with courses as(
	select 
		c.Name as CustomerName,
		p.UserId UserId,
		p.CustomerId,
		p.id participantId, 
		p.ActivitySetId,
		cm.[Order], 
		ActivityPeriod.EndDate,
		--If elearning, use Participants.CompletedDate as Course Completion Date
		CASE WHEN art.ArticleTypeId=1 THEN p.CompletedDate ELSE  NULL END AS ElearningCompletedDate,
		p.StatusCodeValue,
		p.CompletedDate,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.id and pie.LeadingTextId = 11) as DateOfBirth,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.id and pie.LeadingTextId = 12) as Position,
		--(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.id and pie.LeadingTextId = 13) as HelmetNumber,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.id and pie.LeadingTextId = 14) as IdNumber,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.id and pie.LeadingTextId = 15) as Employer,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.id and pie.LeadingTextId = 16) as SubContractorFor,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.id and pie.LeadingTextId = 17) as ConfirmationEmail,
		(select top 1 co.comment from comments co where co.EntityId = p.EnrollmentId and co.entity='enrollment') as Comment
	from Participants p
	inner join activities a on a.id = p.ActivityId
	inner join CustomerArticles ca on ca.id = a.CustomerArticleId
	inner join Articles art on art.id=ca.ArticleOriginalId
	inner join customers c on c.id = ca.CustomerId
	inner join CourseModules cm on cm.id = ca.ModuleId
	cross apply(
		select min(ap.[Start]) StartDate, max(ap.[End]) EndDate
		from ActivityPeriods ap
		where ap.ActivityId = a.Id
	) as ActivityPeriod
	where 
		c.DistributorId = 20000
--		and c.id = @CustomerId
		and p.IsDeleted = 0
		and ca.IsDeleted = 0
		and a.IsDeleted = 0
),
CompletedCourses as(
	select 
		cs.UserId,
		cs.[order],
		convert(date, max(cs.EndDate), 0) EndDate,
		convert(date, max(cs.ElearningCompletedDate), 0) ElearningCompletedDate
	from courses cs
	where cs.StatusCodeValue in ('COMPLETED', 'DISPENSATION') 	
	group by cs.[Order], cs.UserId
		
)
	select distinct  
		u.id,
		c.CustomerId,
		c.CustomerName,
		u.id UserId,
		u.FirstName,
		u.LastName,
		u.MembershipUserId UserName,
		co.[Description] as CountryOfCitizenship,
		c.DateOfBirth,
		c.Position,
		--c.HelmetNumber,
		c.IdNumber,
		convert(datetime, u.ExpiryDate, 0) as UserExpiryDate,
		c.Employer,
		c.SubContractorFor,
		c.ConfirmationEmail,
		aset.Name ActivitySet,
		ISNULL(ISNULL(convert(datetime, c1.EndDate, 0), convert(datetime, c2.EndDate, 0)), convert(datetime, c3.EndDate, 0)) as CourseDate,
		c1.StatusCodeValue as StatusPart1,
		c2.StatusCodeValue as StatusPart2,
		c3.StatusCodeValue as StatusPart3,
		(select ISNULL(cc.ElearningCompletedDate, cc.EndDate) from completedcourses cc where cc.UserId = u.Id and cc.[Order] = 0) as LatestCompletedStatusPart1,
		(select ISNULL(cc.ElearningCompletedDate, cc.EndDate) from completedcourses cc where cc.UserId = u.Id and cc.[Order] = 1) as LatestCompletedStatusPart2,
		(select ISNULL(cc.ElearningCompletedDate, cc.EndDate) from completedcourses cc where cc.UserId = u.Id and cc.[Order] = 2) as LatestCompletedStatusPart3,
		isNull(c.Comment, '') as Comment
	from Courses c
	inner join users u on c.UserId = u.Id
	inner join ActivitySets aset on aset.id = c.ActivitySetId
	left join Codes co on co.Value = u.Country and co.CodeTypeId = 4
	left join courses c1 on c1.UserId = u.Id and c1.ActivitySetId = aset.Id and c1.[Order] = 0 
	left join courses c2 on c2.UserId = u.Id and c2.ActivitySetId = aset.Id and c2.[Order] = 1 
	left join courses c3 on c3.UserId = u.Id and c3.ActivitySetId = aset.Id and c3.[Order] = 2 

	WHERE u.ExpiryDate IS NULL