If exists(select * from sysobjects where name like 'v_Phoenix_Searchable_Activities' and xtype like 'V')
	drop view v_Phoenix_Searchable_Activities
GO

/****** Object:  View [dbo].[v_Phoenix_Searchable_Activities]    Script Date: 18-Feb-14 15:51:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

CREATE VIEW [dbo].[v_Phoenix_Searchable_Activities] 
AS 
SELECT	newid() as Id,
		act.Id as ActivityId,
		ca.Id as CustomerArticleId,
		act.IsDeleted,
		act.Name,
		act.MomentTimezoneName,
		sp.Start as ActivityStart,
		ep.[End] as ActivityEnd,
		[dbo].[fn_article_path](art.Id) as ArticlePath,
		arttype.Name as ArticleType,
		ca.CustomerId as CustomerId,
		cust.Name as CustomerName,
		cust.DistributorId as DistributorId,
		actset.Id as ActivitySetId,
		actset.Name as ActivitySetName,
		ca.ArticleOriginalId as ArticleOriginalId,
		coursercos.Version as RcoCourseVersion,
		act.RcoCourseId as RcoCourseId,
		examrcos.Name as RcoExamContainerName,
		act.RcoExamContainerId as RcoExamContainerId,
		cast(case when currentOrderStatus.[Status] is null then 1 else case when currentOrderStatus.IsCreditNote = 1 and currentOrderStatus.[Status] > 1 then 1 else 0 end end as bit) as IsProposed,
		act.FixedPrice,
		art.LanguageId,
		art.ArticleTypeId,
		art.ProductId,
		prodCat.ProductCodePrefix + prod.ProductNumber+UPPER(arttype.CharCode) as ArticleCode
FROM	dbo.Activities AS act
		INNER JOIN dbo.CustomerArticles AS ca ON act.CustomerArticleId = ca.Id
		INNER JOIN dbo.Customers AS cust ON cust.Id = ca.CustomerId
		INNER JOIN dbo.Articles AS art ON ca.ArticleCopyId = art.Id
		INNER JOIN dbo.ArticleTypes AS arttype ON art.ArticleTypeId = arttype.Id
		LEFT OUTER JOIN dbo.ActivitySets_Activities AS actset_act ON actset_act.ActivityId = act.Id
		LEFT OUTER JOIN dbo.v_Phoenix_SearchableRcos AS coursercos ON coursercos.Id = act.RcoCourseId
		LEFT OUTER JOIN dbo.v_Phoenix_SearchableRcos AS examrcos ON examrcos.Id = act.RcoExamContainerId
		LEFT OUTER JOIN dbo.ActivitySets AS actset ON actset.Id = actset_act.ActivitySetId
		LEFT OUTER JOIN (SELECT sp2.Start, sp2.ActivityId 
						 FROM dbo.ActivityPeriods as sp2
							  JOIN (SELECT ActivityId, MIN(Start) MinStart 
									FROM dbo.ActivityPeriods
									GROUP BY ActivityId) as sp3 ON sp2.ActivityId = sp3.ActivityId AND sp2.Start = sp3.MinStart
						 GROUP BY sp2.ActivityId, sp2.Start) as sp ON act.Id = sp.ActivityId
		LEFT OUTER JOIN (SELECT ep2.[End], ep2.ActivityId 
						 FROM dbo.ActivityPeriods as ep2
							  JOIN (SELECT ActivityId, MAX([End]) MaxEnd 
									FROM dbo.ActivityPeriods
									GROUP BY ActivityId) as ep3 ON ep2.ActivityId = ep3.ActivityId AND ep2.[End] = ep3.[MaxEnd]
						 GROUP BY ep2.ActivityId, ep2.[End]) as ep ON act.Id = ep.ActivityId
		LEFT OUTER JOIN v_Phoenix_CurrentActivityOrders as currentOrderStatus on currentOrderStatus.ActivityId = act.Id
		LEFT OUTER JOIN dbo.Products as prod ON prod.Id=art.ProductId
		LEFT OUTER JOIN dbo.ProductCategories as prodCat ON prodCat.Id=prod.ProductCategoryId
WHERE act.IsDeleted = 0
 
GO


