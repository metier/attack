If exists(select * from sysobjects where name like 'v_Phoenix_Report_ParticipationStatusPrActivity' and xtype like 'V')
	drop view v_Phoenix_Report_ParticipationStatusPrActivity
GO

create view [dbo].v_Phoenix_Report_ParticipationStatusPrActivity
as 
	select 
		p.id ParticipantId,
		u.id userId,
		c.id CustomerId,
		d.id DistributorId,
		prod.id ProductId,
		d.name Distributor,
		c.name Customer,
		dbo.fn_product_path(prod.Id) as ProductNumber,
		dbo.fn_article_path(art.Id) as ArticleNumber,
		(select name from ArticleTypes at where at.Id = art.ArticleTypeId) as ContentType,
		prod.Title ProductTitle,
		case 
			when e.ActivitySetId <> p.ActivitySetId then concat('* ', aset.Name)
			else aset.Name 
		end ActivitySetTitle,
		a.Name ActivityTitle,
		u.FirstName,
		u.LastName,
		am.Email,
		u.PhoneNumber,
		u.ExpiryDate,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.Id and pie.LeadingTextId = 11) CustomField1,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.Id and pie.LeadingTextId = 12) CustomField2,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.Id and pie.LeadingTextId = 13) EmployeeId,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.Id and pie.LeadingTextId = 14) Department,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.Id and pie.LeadingTextId = 15) Position,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.Id and pie.LeadingTextId = 16) SuperVisor,
		(select pie.InfoText from ParticipantInfoElements pie where pie.ParticipantId = p.Id and pie.LeadingTextId = 17) ConfirmationEmail,
		p.Created EnrollmentDate,
		periods.startDate,
		periods.endDate,
		p.StatusCodeValue as [Status],
		elearning.PerformanceStatus CourseStatus,
		isNull(elearning.NumberOfLessons, 0) NumberOfLessons,
		isNull(elearning.NumberOfCompletedLessons, 0) NumberOfCompletedLessons,
		c.IsArbitrary
	from 
		Participants p
		inner join Users u on u.id = p.UserId
		inner join aspnet_Users au on au.UserName = u.MembershipUserId
		inner join aspnet_Membership am on am.UserId = au.UserId
		inner join Customers c on c.id = p.CustomerId
		inner join Distributors d on d.id = c.DistributorId
		inner join Enrollments e on e.id = p.EnrollmentId
		inner join ActivitySets aset on aset.id = p.ActivitySetId
		inner join activities a on a.id = p.ActivityId
		inner join CustomerArticles ca on ca.id = a.CustomerArticleId
		inner join Articles art on art.id = ca.ArticleCopyId
		inner join Products prod on prod.id = art.ProductId		
		outer apply(
			select min(ap.Start) as startDate, max(ap.[End]) as enddate	from ActivityPeriods ap 
			where ap.ActivityId = p.ActivityId) as periods
	outer apply(
		select top 1 
			rco.[Version] as CourseLink,
			course.points,
			course.completedDate,
			course.PerformanceStatus,
			(select count(distinct spf.RcoId) from ScormPerformances spf inner join rcos r on spf.RcoId = r.Id where r.ParentId = rco.Id and spf.UserId = p.userId and spf.[status] = 'C') as NumberOfCompletedLessons, 	 
			(select count(*) from rcos r where r.ParentId = rco.id) as NumberOfLessons
		from v_Phoenix_SearchableRcos rco 
		outer apply(
			select max(spf.Score) as Points, max(spf.CompletedDate) CompletedDate, min(spf.[status]) as PerformanceStatus
			from ScormPerformances spf where spf.RcoId = rco.id and spf.UserId = p.userId
			) as course
		where 
			rco.id = a.RcoCourseId
			and art.ArticleTypeId = 1
		) elearning
	where
		p.IsDeleted		= 0 and 
		a.IsDeleted		= 0 and
		ca.IsDeleted	= 0 and
		art.IsDeleted	= 0 and
		e.IsCancelled	= 0
