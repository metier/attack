If exists(select * from sysobjects where name like 'DeletePhoenixUser' and xType = 'P')
	Drop procedure DeletePhoenixUser
Go

Create procedure DeletePhoenixUser(@UserNameOrId nvarchar(200))
as
Begin

Declare 
	@userId int,
	@UserName nvarchar(200),
	@NumTablesDeletedFrom int

	Select @UserId = case 
		when (isNumeric(@UserNameOrId) = 1) then @UserNameOrId
		else (select id from Users where MembershipUserId = @UserNameOrId)
		end

	Select @UserName = case 
		when (isNumeric(@UserNameOrId) = 0) then @UserNameOrId
		else (select MembershipUserId from Users where Id = @UserNameOrId)
		end

	if exists
			(select 1 from Participants where @userId = isNull(ExamGraderUserId, 0) union
			 select 1 from Orders_Participants op 
				where op.ParticipantId in(select id from Participants p where p.UserId = @userId) union
			 select 1 from Articles where @UserId in(ModifiedById, CreatedById) union
			 select 1 from Exams where ModifiedById = @UserId and ParticipantId not in(select id from Participants where UserId = @userId) union
			 select 1 from ProductDescriptions where @UserId in(ModifiedById, CreatedById) union
			 select 1 from Questions where @UserId in(ModifiedById, CreatedById) union
			 select 1 from users where @UserName in(ModifiedBy, CreatedBy) and id <> @userId
			)
		throw 90000, 'The user can not be deleted!', 1
	else
	Begin
		Delete from Mails where ParticipantId in (select id from Participants where UserId = @userId)
		Delete from Answers_QuestionOptions where answerId in (select id from Answers where ExamId in (select id from Exams where ParticipantId in (select id from Participants where UserId = @userId)))
		Delete from Answers where ExamId in (select id from Exams where ParticipantId in (select id from Participants where UserId = @userId))
		Delete from Exams_Questions where ExamId in (select id from Exams where ParticipantId in (select id from Participants where UserId = @userId))
		Delete from Exams where ParticipantId in (select id from Participants where UserId = @userId)
		Delete from ParticipantInfoElementsShadowTable where ParticipantId in(select id from Participants where UserId = @userId)
		Delete from ParticipantInfoElements where ParticipantId in (select id from Participants where UserId = @userId)
		Delete from Participants_Attachments where ParticipantId in (select id from Participants where UserId = @userId)
		Delete from ChangeLog where EntityId in(select id from Participants where UserId = @userId) and EntityType = 1

		delete from UserInfoElements where userid = @userId
		Delete from ChangeLog where userid = @userId
		Delete from Participants where UserId = @userId
		Delete from Enrollments where UserId = @userId
		Delete from ScormPerformances where userid = @userId
		Delete from ScormAttempts where userid = @userId
		Delete from usermissingproducts where userid = @userId
		Delete from Resource_Users where userid = @userId
		Delete from UserCompetences_Attachments where UserCompetenceId in(select id from UserCompetences where UserId = @userId)
		Delete from UserCompetences where UserId = @userId
		Delete from PasswordResetRequests where UserId = @userId
		Delete from MetierContactPersons where UserId = @userId
		Delete from Mails where UserId = @userId
		Delete from Comments where UserId = @userId

		exec [dbo].[aspnet_Users_DeleteUser] 'Phoenix', @UserName, 15, @NumTablesDeletedFrom
		delete from users where id = @userId
	End
End

