If exists(select * from sysobjects where name like 'fn_product_path' and xtype like 'FN')
	drop function fn_product_path
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

-- =============================================
-- Author:		Olav Haugen
-- Create date: 09.04.2014
-- Description:	Creates a string representing the path of a product
-- =============================================
CREATE FUNCTION [dbo].[fn_product_path]
(
	@product_id int
)
RETURNS varchar(100)
WITH SCHEMABINDING
AS
BEGIN
	DECLARE @ResultVar varchar(100)

	SELECT @ResultVar = cat.ProductCodePrefix + prod.ProductNumber
	FROM	
	dbo.Products AS prod
	INNER JOIN dbo.ProductCategories AS cat ON prod.ProductCategoryId = cat.Id 
	WHERE prod.Id = @product_id
		
	RETURN @ResultVar
END
 