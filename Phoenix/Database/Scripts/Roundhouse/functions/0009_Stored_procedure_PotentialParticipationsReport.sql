/*
if exists (select * from sysobjects where name like 'Phoenix_UserMissingProducts' and xType='P')
	drop procedure Phoenix_UserMissingProducts
go 

create procedure Phoenix_UserMissingProducts
as
begin
	delete from dbo.usermissingproducts;

	insert into dbo.usermissingproducts
	select art.ProductId as ProductId, usr.Id as UserId, usr.CustomerId
	from Activities act
	inner join CustomerArticles cusart on act.CustomerArticleId = cusart.Id
	inner join Articles art on cusart.ArticleOriginalId = art.Id
	inner join Users usr on cusart.CustomerId = usr.CustomerId
	left outer join Participants par on act.Id = par.ActivityId and usr.Id = par.UserId
	where act.IsDeleted = 0 and cusart.IsDeleted = 0
	group by art.ProductId, usr.Id, usr.CustomerId
	having count(case when par.Id is not null then 1 end) = 0
end

*/

if exists (select * from sysobjects where name like 'Phoenix_UserMissingProducts' and xType='P')
	drop procedure Phoenix_UserMissingProducts
go 

create procedure Phoenix_UserMissingProducts
as
begin
	--truncate table dbo.usermissingproducts;
	delete from dbo.usermissingproducts;

	WITH AvailableProducts AS (
		select distinct art.ProductId as ProductId, cusart.CustomerId
			from Activities act
			inner join CustomerArticles cusart on act.CustomerArticleId = cusart.Id
			inner join Articles art on cusart.ArticleOriginalId = art.Id
			where act.IsDeleted = 0 and cusart.IsDeleted = 0

	),
	ProductsTaken AS (
		select  usr.Id as UserId, usr.CustomerId, art.ProductId
		from users usr
		inner join Participants par on usr.Id = par.UserId and par.IsDeleted=0
		inner join Activities act on act.id=par.ActivityId and act.IsDeleted=0
		inner join CustomerArticles cusart on act.CustomerArticleId = cusart.Id
		inner join Articles art on cusart.ArticleOriginalId = art.Id
	)


	insert into dbo.usermissingproducts (UserId, ProductId, CustomerId)
	select usr.Id as UserId, ap.ProductId, usr.CustomerId
	from Users usr
	inner join AvailableProducts ap on ap.CustomerId = usr.CustomerId
	left join ProductsTaken pt on pt.UserId=usr.Id and ap.ProductId=pt.ProductId
	where pt.ProductId is null
END