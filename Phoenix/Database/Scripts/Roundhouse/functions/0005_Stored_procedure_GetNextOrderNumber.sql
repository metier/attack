if exists (select * from sysobjects where name like 'Phoenix_GetNextOrderNo' and xType='P')
	drop procedure Phoenix_GetNextOrderNo
go

CREATE procedure Phoenix_GetNextOrderNo
AS
BEGIN
	DECLARE @dbName nvarchar(200);
	DECLARE @sql nvarchar(max);
	SELECT @dbName=lower(DB_Name())

	if (@dbName like '%dev%' OR  @dbName like '%test1%' OR @dbName like '%test2%' OR @dbName like '%test3%')
	begin
		insert into [PhoenixCommon].[dbo].[OrderNoSequence](CreatedAt) values(getdate());
		select max(id) + 1 NextOrderNo from [PhoenixCommon].[dbo].[OrderNoSequence];
	end
	else
		SELECT NEXT VALUE FOR [dbo].[Phoenix_AgressoOrderSequence]
end