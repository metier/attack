if exists (select * from sysobjects where name like 'Phoenix_CreateUserWithTokenAuthentication' and xType='P')
	drop procedure Phoenix_CreateUserWithTokenAuthentication
go 
if exists (select * from sysobjects where name like 'CreateUserWithTokenAuthentication' and xType='P')
	drop procedure CreateUserWithTokenAuthentication
go 

create procedure [dbo].[CreateUserWithTokenAuthentication]
	@UserName         nvarchar(256),
	@RoleName         nvarchar(256),
    @AuthToken		  uniqueidentifier
as
begin
	declare 	
		@ApplicationId uniqueidentifier, 
		@roleId uniqueidentifier,
		@now datetime2 = getdate(),
		@userId uniqueidentifier
		
	if( @AuthToken IS NULL )
	begin
        select @AuthToken = NEWID()
    end


	select @ApplicationId = ApplicationId from [dbo].[aspnet_Applications] where ApplicationName = 'Phoenix'
	select @roleId = RoleId from [dbo].[aspnet_Roles] where RoleName = @RoleName

	if not exists(select * from aspnet_Users where UserName = @UserName)
		exec dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, 0, @now, @UserId

	exec dbo.aspnet_UsersInRoles_AddUsersToRoles 'Phoenix', @UserName, @RoleName, @now

	if not exists(select * from AuthenticationTokens where UserName = @UserName)
		insert into dbo.AuthenticationTokens ([UserName], [Token]) values (@UserName, @authToken)
	else
		update dbo.AuthenticationTokens set [Token] = @AuthToken where [UserName] = @UserName

	return 0
end
 
GO


