If exists(select * from sysobjects where name like 'fn_article_path' and xtype like 'FN')
	drop function fn_article_path
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE FUNCTION [dbo].[fn_article_path]
(
	@article_id int
)
RETURNS varchar(100)
WITH SCHEMABINDING
AS
BEGIN
	DECLARE @ResultVar varchar(100)

	SELECT @ResultVar = cat.ProductCodePrefix + '-' + prod.ProductNumber + RTRIM(arttype.CharCode) + '-' + { fn UCASE(lan.Identifier) }
	FROM	
	dbo.Articles AS art 
	INNER JOIN dbo.Languages AS lan ON art.LanguageId = lan.ID 
	INNER JOIN dbo.Products AS prod ON art.ProductId = prod.Id 
	INNER JOIN dbo.ProductCategories AS cat ON prod.ProductCategoryId = cat.Id 
	INNER JOIN dbo.ArticleTypes AS arttype ON art.ArticleTypeId = arttype.Id
	WHERE art.Id = @article_id

	
	RETURN @ResultVar
END
 
GO