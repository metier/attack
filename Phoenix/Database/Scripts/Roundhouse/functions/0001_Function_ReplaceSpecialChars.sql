if exists(
	select * from sysobjects where name like 'ReplaceSpecialChars' and xtype='FN'
)
	Drop function [dbo].[ReplaceSpecialChars]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jon Sommervold
-- Create date: 
-- Description:	Replaces special characters in inputstring
-- =============================================
CREATE FUNCTION ReplaceSpecialChars 
(
	@string nvarchar(200)
)
RETURNS nvarchar(200)
AS
BEGIN

	--Replace lowercase chars
	set @string = replace(@string,'å', '�')
	set @string = replace(@string,'æ', '�')
	set @string = replace(@string,'ø', '�')

	--Replace uppercase chars
	set @string = replace(@string,'Å', '�')
	set @string = replace(@string,'Æ', '�')
	set @string = replace(@string,'Ø', '�')

	RETURN @string 

END
GO

