alter table CustomerArticles
add 
	FixedPriceNotBillable bit,
	UnitPriceNotBillable bit
GO

update CustomerArticles Set FixedPriceNotBillable = case when FixedPrice is null then 1 else 0 end
update CustomerArticles Set UnitPriceNotBillable = case when UnitPrice is null then 1 else 0 end

alter table CustomerArticles 
	alter column FixedPriceNotBillable bit not null

alter table CustomerArticles 
	alter column UnitPriceNotBillable bit not null