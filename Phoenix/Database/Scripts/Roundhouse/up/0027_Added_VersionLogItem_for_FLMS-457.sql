insert into VersionLogItems(InternalReference, Title, [Description], DeploymentDate)
values
('FLMS-457', 'Implementing a version-log for Phoenix', 'Version log implemented so users of the Phoenix system can keep updated with the latest changes to the system. Additional Log items will appear in this list for every change to the system.', getdate())
