if exists (select * from sysobjects where name like 'AddToVersionLog' and xType='P')
	drop procedure AddToVersionLog
go

CREATE procedure AddToVersionLog 
	@InternalReference nvarchar(100),
	@Title nvarchar(400),
	@Description nvarchar(max)
AS
BEGIN

	if exists(select * from VersionLogItems where InternalReference = @InternalReference)
		Update VersionLogItems set 
			Title = @Title,
			[Description] = @Description
		where
			InternalReference = @InternalReference
	else
		Insert into VersionLogItems(InternalReference, Title, [Description], DeploymentDate)
		values(@InternalReference, @Title, @Description, GETDATE())
end

