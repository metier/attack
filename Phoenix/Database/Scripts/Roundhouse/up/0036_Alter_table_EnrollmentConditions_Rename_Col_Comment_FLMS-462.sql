
update EnrollmentConditions set Comment = 
	concat(
		dbo.fn_product_path(ec.ProductId),
		'-',
		(select top 1 CharCode from ArticleTypes at where at.Id = ec.ArticleTypeId)
	)
from EnrollmentConditions ec
where ec.Comment is null

Alter table EnrollmentConditions
Alter column Comment NVARCHAR(max) NOT NULL
Go

EXEC sp_rename 'EnrollmentConditions.Comment', 'Title', 'COLUMN';  
go
