BEGIN TRANSACTION
GO
	ALTER TABLE dbo.Activities ADD
		ECTS nvarchar(20) NULL
	GO

	update Activities
	set ECTS = ae.ECTS
	from activities a
	inner join CustomerArticles ca on ca.id = a.CustomerArticleId
	inner join Articles art on art.id = ca.ArticleCopyId
	inner join Article_Exams ae on ae.id = art.Id
	GO

	update Activities
	set ECTS = ae.ECTS
	from activities a
	inner join CustomerArticles ca on ca.id = a.CustomerArticleId
	inner join Articles art on art.id = ca.ArticleCopyId
	inner join Article_CaseExams ae on ae.id = art.Id
GO
COMMIT

