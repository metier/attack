BEGIN TRANSACTION
GO
EXECUTE sp_rename N'Exams.IsDelivered', N'Tmp_IsSubmitted', 'COLUMN' 
GO
EXECUTE sp_rename N'Exams.RegisteredDelivered', N'Tmp_SubmittedAt_1', 'COLUMN' 
GO
EXECUTE sp_rename N'Exams.Tmp_IsSubmitted', N'IsSubmitted', 'COLUMN' 
GO
EXECUTE sp_rename N'Exams.Tmp_SubmittedAt_1', N'SubmittedAt', 'COLUMN' 
GO
ALTER TABLE dbo.Exams SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
