alter table distributors
add LoginFormUrl nvarchar(200)
GO

alter table distributors
add PasswordResetFormUrl nvarchar(200)
GO

alter table distributors
add AutomailSender   nvarchar(200)
GO

alter table distributors
add LogoUrl nvarchar(200)
GO

declare @environment nvarchar(50)

select @environment = DB_NAME()

update Distributors 
	set PasswordResetFormUrl = case @environment
			when 'Dev_MetierLms'	then 'https://lms.metier.no/dev/learningportal/mymetier/login.aspx'
			when 'Demo1_MetierLms'	then 'https://lms.metier.no/demo1/learningportal/mymetier/login.aspx'
			when 'Test1_MetierLms'	then 'https://lms.metier.no/test1/learningportal/mymetier/login.aspx'
			when 'Test2_MetierLms'	then 'https://lms.metier.no/test2/learningportal/mymetier/login.aspx'
			when 'Test3_MetierLms'	then 'https://lms.metier.no/test3/learningportal/mymetier/login.aspx'
			else 'https://mymetier.net/learningportal/mymetier/login.aspx'
			end,
		LoginFormUrl = case @environment
			when 'Dev_MetierLms'	then 'https://lms.metier.no/dev/learningportal/mymetier/login.aspx'
			when 'Demo1_MetierLms'	then 'https://lms.metier.no/demo1/learningportal/mymetier/login.aspx'
			when 'Test1_MetierLms'	then 'https://lms.metier.no/test1/learningportal/mymetier/login.aspx'
			when 'Test2_MetierLms'	then 'https://lms.metier.no/test2/learningportal/mymetier/login.aspx'
			when 'Test3_MetierLms'	then 'https://lms.metier.no/test3/learningportal/mymetier/login.aspx'
			else 'https://mymetier.net/learningportal/mymetier/login.aspx'
			end,
		AutomailSender = 'noreply@mymetier.net',
		LogoUrl = case @environment
			when 'Dev_MetierLms'	then 'https://lms.metier.no/dev/admin/app/assets/img/metier_logo.png'
			when 'Demo1_MetierLms'	then 'https://lms.metier.no/demo1/admin/app/assets/img/metier_logo.png'
			when 'Test1_MetierLms'	then 'https://lms.metier.no/test1/admin/app/assets/img/metier_logo.png'
			when 'Test2_MetierLms'	then 'https://lms.metier.no/test2/admin/app/assets/img/metier_logo.png'
			when 'Test3_MetierLms'	then 'https://lms.metier.no/test3/admin/app/assets/img/metier_logo.png'
			else 'https://mymetier.net/admin/app/assets/img/metier_logo.png'
			end		
where id <> 10000


update Distributors 
	set PasswordResetFormUrl = 'https://psoetraining.com/password.aspx',
		LoginFormUrl = 'https://psoetraining.com/login.aspx',
		AutomailSender = 'noreply@psoetraining.com',
		LogoUrl = 'https://lms.metier.no/dev/admin/app/assets/img/psotraining_logo.png'
where id = 10000
