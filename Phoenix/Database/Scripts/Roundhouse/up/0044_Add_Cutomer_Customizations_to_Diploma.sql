/*
   Friday, June 1, 20185:22:01 AM
   User: 
   Server: localhost
   Database: Phoenix
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Attachments SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Attachments', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Attachments', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Attachments', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Customers ADD
	DiplomaShowCustomerLogo bit NOT NULL CONSTRAINT DF_Customers_DiplomaShowCustomerLogo DEFAULT 0,
	DiplomaShowCustomSignature bit NOT NULL CONSTRAINT DF_Customers_DiplomaShowCustomSignature DEFAULT 0,
	DiplomaCustomSignatureText varchar(255) NULL,
	DiplomaCustomSignatureTitle varchar(255) NULL,
	DiplomaShowElearningLessons bit NOT NULL CONSTRAINT DF_Customers_DiplomaShowElearningLessons DEFAULT 0,
	DiplomaLogoAttachmentId int NULL,
	DiplomaSignatureAttachmentId int NULL
GO
ALTER TABLE dbo.Customers ADD CONSTRAINT
	FK_Customers_Attachments FOREIGN KEY
	(
	DiplomaSignatureAttachmentId
	) REFERENCES dbo.Attachments
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Customers ADD CONSTRAINT
	FK_Customers_Attachments1 FOREIGN KEY
	(
	DiplomaLogoAttachmentId
	) REFERENCES dbo.Attachments
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Customers SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Customers', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Customers', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Customers', 'Object', 'CONTROL') as Contr_Per 