begin transaction
go

Alter table EnrollmentConditions
add IsStrictRequirement bit NULL
Go

ALTER TABLE dbo.EnrollmentConditions ADD CONSTRAINT
	DF_EnrollmentConditions_IsStrictRequirement DEFAULT 0 FOR IsStrictRequirement
GO

Update EnrollmentConditions set IsStrictRequirement = 0
Go

Alter table EnrollmentConditions
Alter column IsStrictRequirement bit NOT NULL
Go

Commit

