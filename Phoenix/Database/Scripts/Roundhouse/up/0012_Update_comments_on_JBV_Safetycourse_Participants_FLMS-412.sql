
insert into comments (Entity, EntityId, Comment, Created, LastModified, UserId)
select 'enrollment', p.EnrollmentId, c.Comment, min(c.Created), min(c.LastModified), c.UserId
from Comments c
inner join Participants p on p.id = c.EntityId and c.Entity = 'participant'
inner join Customers cust on cust.id = p.CustomerId
where cust.DistributorId = 20000
group by
	c.Entity, p.EnrollmentId, c.Comment, c.UserId


delete Comments
from Comments c
inner join Participants p on p.id = c.EntityId and c.Entity = 'participant'
inner join Customers cust on cust.id = p.CustomerId
where cust.DistributorId = 20000
