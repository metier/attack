--select * from ArticleTypes
INSERT INTO ArticleTypes(ID, NAME, CharCode) VALUES('12','Subscription', 'SU')

--SET IDENTITY_INSERT ProductCategories ON;
--INSERT INTO ProductCategories(ID, NAME, ProductCodePrefix) VALUES('14','Subscription', 'SU')
--SET IDENTITY_INSERT ProductCategories OFF;

SET IDENTITY_INSERT ProductTypes ON;
INSERT INTO ProductTypes(ID, NAME) VALUES('6','Subscription');
SET IDENTITY_INSERT ProductTypes OFF;


---


CREATE TABLE [dbo].[Article_Subscriptions](
	[Id] [int] NOT NULL,
	--[PDU] [nvarchar](10) NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Article_Subscriptions]  WITH CHECK ADD  CONSTRAINT [FK_SubscriptionArticles_Articles] FOREIGN KEY([Id])
REFERENCES [dbo].[Articles] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Article_Subscriptions] CHECK CONSTRAINT [FK_SubscriptionArticles_Articles]
GO