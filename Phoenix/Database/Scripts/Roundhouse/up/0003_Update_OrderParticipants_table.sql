
UPDATE Orders_Participants
	set 
		Price = p.unitprice,
		CurrencyCodeId = p.currencyCodeId,
		OnlinePaymentReference = p.OnlinePaymentReference,
		activityId = p.ActivityId,
		ActivityName = a.Name
	from Orders_Participants op
	inner join Participants p
		on p.id = op.ParticipantId
	inner join Activities a
		on a.id = p.ActivityId



;with infotexts as (
	select e.ParticipantId, 
		case when isNull(e.InfoText, '') = '' then ''
		else concat(t.[Text], ': ', e.InfoText, ', ') 
		end as	textstring, 
		t.LeadingTextId, 
		t.IsVisibleInvoiceLine
	from ParticipantInfoElements e 
	inner join CustomLeadingTexts t
	on t.id = e.CustomLeadingTextId
	)
UPDATE Orders_Participants 
	set Infotext = substring(infoText.resultString, 0, LEN(infotext.resultstring))	
	from orders_participants p
	cross apply
	( select
		concat(
			(select top 1 it.textstring from infotexts it where it.ParticipantId = p.participantId and it.IsVisibleInvoiceLine = 1 and it.LeadingTextId = 11),
			(select top 1 it.textstring from infotexts it where it.ParticipantId = p.participantId and it.IsVisibleInvoiceLine = 1 and it.LeadingTextId = 12),
			(select top 1 it.textstring from infotexts it where it.ParticipantId = p.participantId and it.IsVisibleInvoiceLine = 1 and it.LeadingTextId = 13),
			(select top 1 it.textstring from infotexts it where it.ParticipantId = p.participantId and it.IsVisibleInvoiceLine = 1 and it.LeadingTextId = 14),
			(select top 1 it.textstring from infotexts it where it.ParticipantId = p.participantId and it.IsVisibleInvoiceLine = 1 and it.LeadingTextId = 15),
			(select top 1 it.textstring from infotexts it where it.ParticipantId = p.participantId and it.IsVisibleInvoiceLine = 1 and it.LeadingTextId = 16),
			(select top 1 it.textstring from infotexts it where it.ParticipantId = p.ParticipantId and it.IsVisibleInvoiceLine = 1 and it.LeadingTextId = 17)
			) as resultString
	) as infoText
	where 
	len(infoText.resultString) > 0
