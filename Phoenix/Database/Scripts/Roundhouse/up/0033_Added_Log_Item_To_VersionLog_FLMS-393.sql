exec AddToVersionLog 'FLMS-393', 'Added functionality for merging users.', 'Functionality for merging users is now available. 
A merge-button is located beside the edit-button on the users details page. The logic is somewhat limited, but will hopefully be sufficient for most merge operations.
If not, send a request to phoenix@metier.zendesk.com.'