CREATE TABLE Article_Webinars(
	[Id] [int] NOT NULL,
	[PDU] [nvarchar](10) NULL
) ON [PRIMARY]

GO

ALTER TABLE Article_Webinars  WITH CHECK ADD  CONSTRAINT [FK_WebinarArticles_Articles] FOREIGN KEY([Id])
REFERENCES Articles ([Id])
ON DELETE CASCADE
GO

ALTER TABLE Article_Webinars CHECK CONSTRAINT [FK_WebinarArticles_Articles]
GO


