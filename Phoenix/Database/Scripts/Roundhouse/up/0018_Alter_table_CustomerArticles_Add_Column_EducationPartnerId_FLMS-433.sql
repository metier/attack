BEGIN TRANSACTION
GO
ALTER TABLE dbo.Resources SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.CustomerArticles ADD
	EducationalPartnerId int NULL
GO
ALTER TABLE dbo.CustomerArticles ADD CONSTRAINT
	FK_CustomerArticles_EducationalPartnerResources FOREIGN KEY
	(
	EducationalPartnerId
	) REFERENCES dbo.Resources
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO

	update CustomerArticles
	set EducationalPartnerId = cp.EducationalPartnerResourceId 
	from CustomerArticles ca
	inner join CourseModules cm on cm.id = ca.ModuleId
	inner join CourseSteps cs on  cm.CourseStepId = cs.Id
	inner join CoursePrograms cp on cs.CourseProgramId = cp.Id
	where 
		ca.ModuleId is not null and ca.IsDeleted = 0

	update CustomerArticles
	set EducationalPartnerId = cp.EducationalPartnerResourceId 
	from CustomerArticles ca
	left join CourseExams ce on ce.id = ca.ExamId
	inner join CourseSteps cs on  ce.CourseStepId = cs.Id
	inner join CoursePrograms cp on cs.CourseProgramId = cp.Id
	where 
		ca.ExamId is not null and ca.IsDeleted = 0

GO

ALTER TABLE dbo.CustomerArticles SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

