CREATE TABLE [dbo].[AccountingCutoffDates](
	[Id] [int] IDENTITY(1,1) NOT NULL,	
	[MaxDateOfActivity] [date] NOT NULL,
	[AccountingDate] [date] NOT NULL,
 CONSTRAINT [PK_AccountingCutoffDates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
select * from AccountingCutoffDates

INSERT INTO AccountingCutoffDates VALUES('1/3/19','12/31/18');
INSERT INTO AccountingCutoffDates VALUES('2/4/19','1/31/19');
INSERT INTO AccountingCutoffDates VALUES('3/4/19','2/28/19');
INSERT INTO AccountingCutoffDates VALUES('4/1/19','3/31/19');
INSERT INTO AccountingCutoffDates VALUES('5/6/19','4/30/19');

INSERT INTO AccountingCutoffDates VALUES('6/3/19','5/31/19');
INSERT INTO AccountingCutoffDates VALUES('7/1/19','6/30/19');
INSERT INTO AccountingCutoffDates VALUES('8/5/19','7/31/19');
INSERT INTO AccountingCutoffDates VALUES('9/2/19','8/31/19');
INSERT INTO AccountingCutoffDates VALUES('9/30/19','9/30/19');
INSERT INTO AccountingCutoffDates VALUES('11/4/19','10/31/19');
INSERT INTO AccountingCutoffDates VALUES('12/2/19','11/30/19');

--SELECT MIN([AccountingDate]) FROM [AccountingCutoffDates] WHERE [MaxDateOfActivity]>=CAST(cast('2019-04-01 23:23:23' as datetime) AS DATE)