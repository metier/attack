exec AddToVersionLog 'FLMS-450', 'Duration to enforce expiration of e-learning courses', 
'Previously the Duration / End Date fields determined the target date for e-learning activities, and was used when generating the study plan. 
Those fields have now been moved to the AutoMails tab on the activity, and a new Duration field has been introduced. 
If enabled, the new Duration field will determine how long the course is available to the user. Otherwise activity periods are used to enforce this.'