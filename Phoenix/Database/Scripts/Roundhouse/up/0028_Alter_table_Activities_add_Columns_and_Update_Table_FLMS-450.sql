alter table activities
add 
	CompletionTargetDate DateTime null,
	CompletionTargetDuration BigInt null
GO

update activities 
	set CompletionTargetDate = a.EndDate,
		CompletionTargetDuration = a.Duration,
		Duration = null,
		EndDate = null
from Activities a
inner join CustomerArticles ca on ca.id = a.CustomerArticleId
inner join customers c on c.id = ca.CustomerId
inner join Articles art on art.id = ca.ArticleCopyId
inner join ArticleTypes at on at.Id = art.ArticleTypeId
where 
	art.ArticleTypeId = 1

GO