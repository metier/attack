SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--drop table [CustomerArticleEnrollmentConditions]
CREATE TABLE [CustomerArticleEnrollmentConditions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerArticleId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[ArticleTypeId] [int] NOT NULL,
	[LanguageId] [int] NULL,
	[CustomerId] [int] NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[LastModified] [datetime2](7) NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
	[IsStrictRequirement] [bit] NOT NULL CONSTRAINT [DF_CustomerArticleEnrollmentConditions_IsStrictRequirement]  DEFAULT ((0)),
 CONSTRAINT [PK_CustomerArticleEnrollmentConditions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[CustomerArticleEnrollmentConditions]  WITH CHECK ADD  CONSTRAINT [FK_CustomerArticleEnrollmentConditions_ArticleTypes] FOREIGN KEY([ArticleTypeId])
REFERENCES [dbo].[ArticleTypes] ([Id])
GO

ALTER TABLE [dbo].[CustomerArticleEnrollmentConditions] CHECK CONSTRAINT [FK_CustomerArticleEnrollmentConditions_ArticleTypes]
GO

ALTER TABLE [dbo].[CustomerArticleEnrollmentConditions]  WITH CHECK ADD  CONSTRAINT [FK_CustomerArticleEnrollmentConditions_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO

ALTER TABLE [dbo].[CustomerArticleEnrollmentConditions] CHECK CONSTRAINT [FK_CustomerArticleEnrollmentConditions_Customers]
GO

ALTER TABLE [dbo].[CustomerArticleEnrollmentConditions]  WITH CHECK ADD  CONSTRAINT [FK_CustomerArticleEnrollmentConditions_CustomerArticles] FOREIGN KEY([CustomerArticleId])
REFERENCES [dbo].[CustomerArticles] ([Id])
GO

ALTER TABLE [dbo].[CustomerArticleEnrollmentConditions] CHECK CONSTRAINT [FK_CustomerArticleEnrollmentConditions_CustomerArticles]
GO

ALTER TABLE [dbo].[CustomerArticleEnrollmentConditions]  WITH CHECK ADD  CONSTRAINT [FK_CustomerArticleEnrollmentConditions_Languages] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Languages] ([Id])
GO

ALTER TABLE [dbo].[CustomerArticleEnrollmentConditions] CHECK CONSTRAINT [FK_CustomerArticleEnrollmentConditions_Languages]
GO

ALTER TABLE [dbo].[CustomerArticleEnrollmentConditions]  WITH CHECK ADD  CONSTRAINT [FK_CustomerArticleEnrollmentConditions_Products] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO

ALTER TABLE [dbo].[CustomerArticleEnrollmentConditions] CHECK CONSTRAINT [FK_CustomerArticleEnrollmentConditions_Products]
GO


