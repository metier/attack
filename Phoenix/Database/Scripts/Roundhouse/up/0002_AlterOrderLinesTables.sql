Alter table Orders_Participants
Add 
	[Id] [int] IDENTITY(1,1) NOT NULL,
	Price decimal(18, 0) Null,
	[CurrencyCodeId] int Null,
	OnlinePaymentReference nvarchar(200) Null,
	ActivityId int Null,
	ActivityName nvarchar(200) null,
	Infotext nvarchar(200) null,
	[rowversion] timestamp not null
 CONSTRAINT [PK_Orders_Participants] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

Alter table Orders_Activities
Add
	[Id] [int] IDENTITY(1,1) NOT NULL,
	Price decimal(18, 0) Null,
	[CurrencyCodeId] int Null,
	ActivityName nvarchar(200) null,
	[rowversion] timestamp not null
 CONSTRAINT [PK_Orders_Activities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

