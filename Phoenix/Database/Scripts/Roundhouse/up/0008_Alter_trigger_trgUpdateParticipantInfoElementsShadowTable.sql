if exists(select * from sysobjects where name like 'trgUpdateParticipantInfoElementsShadowTable' and xType = 'TR')
	Drop Trigger trgUpdateParticipantInfoElementsShadowTable
Go
 

CREATE TRIGGER [dbo].[trgUpdateParticipantInfoElementsShadowTable] ON [dbo].[ParticipantInfoElements] 
	AFTER INSERT, UPDATE, DELETE 
AS 

BEGIN
--Determine if this is an INSERT,UPDATE, or DELETE Action or a "failed delete".
DECLARE @Action					as char(1),
		@CustomerId				as int,
		@ParticipantId			as int,
		@LeadingTextId			as int,
		@NewValue				as nvarchar(400) = Null

    SET @Action = (CASE WHEN EXISTS(SELECT * FROM INSERTED)
                         AND EXISTS(SELECT * FROM DELETED)
                        THEN 'U'  -- Set Action to Updated.
                        WHEN EXISTS(SELECT * FROM INSERTED)
                        THEN 'I'  -- Set Action to Insert.
                        WHEN EXISTS(SELECT * FROM DELETED)
                        THEN 'D'  -- Set Action to Deleted.
                        ELSE '' -- Skip. It may have been a "failed delete".   
                    END)

	IF(@Action = '') 
		Return

--Assuming that batchoperations are allways of the same kind, i.e. multi-update or multi-delete...
DECLARE infoText_cursor CURSOR FOR
	with temp as(
		SELECT *
		FROM inserted
		WHERE @Action in('U', 'I')
			UNION
		SELECT *
		FROM deleted
		WHERE @Action = 'D'
	)
	select t.ParticipantId, p.CustomerId, t.LeadingTextId, t.InfoText 
	from temp t
	inner join Participants p
	on t.ParticipantId = p.Id

    OPEN infoText_cursor
    FETCH NEXT FROM infoText_cursor INTO @ParticipantId, @CustomerId, @LeadingTextId, @NewValue
    WHILE @@FETCH_STATUS = 0
	BEGIN

		IF not exists(select * from ParticipantInfoElementsShadowTable where ParticipantId = @ParticipantId)
			Insert into ParticipantInfoElementsShadowTable(ParticipantId) values(@ParticipantId)

	
		UPDATE ParticipantInfoElementsShadowTable
		Set 
			CustomField1		= case when @LeadingTextId = 11 then @NewValue else CustomField1		end, 
			CustomField2		= case when @LeadingTextId = 12 then @NewValue else CustomField2		end,
			EmployeeId			= case when @LeadingTextId = 13 then @NewValue else EmployeeId			end,
			Department			= case when @LeadingTextId = 14 then @NewValue else Department			end,
			Position			= case when @LeadingTextId = 15 then @NewValue else Position			end,
			Supervisor			= case when @LeadingTextId = 16 then @NewValue else Supervisor			end,
			ConfirmationEmail	= case when @LeadingTextId = 17 then @NewValue else ConfirmationEmail	end
		Where 
			ParticipantId = @ParticipantId


	    FETCH NEXT FROM infoText_cursor INTO @ParticipantId, @CustomerId, @LeadingTextId, @NewValue
	END

	CLOSE infoText_cursor   
	DEALLOCATE infoText_cursor

END
 
GO


