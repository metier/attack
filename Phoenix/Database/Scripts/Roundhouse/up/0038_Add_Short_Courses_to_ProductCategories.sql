   IF NOT EXISTS (SELECT * FROM [ProductCategories] 
                   WHERE id= 13)
   BEGIN
	SET IDENTITY_INSERT [ProductCategories] ON;
	insert into [ProductCategories](id, name, ProductCodePrefix) values(13, 'Short Course','S')
	SET IDENTITY_INSERT [ProductCategories] OFF;
   END
   
   