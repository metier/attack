alter table Activities
add 
	FixedPriceNotBillable bit,
	UnitPriceNotBillable bit
GO

update activities Set FixedPriceNotBillable = case when FixedPrice is null then 1 else 0 end
update activities Set UnitPriceNotBillable = case when UnitPrice is null then 1 else 0 end

alter table activities
	alter column FixedPriceNotBillable bit not null

alter table activities
	alter column UnitPriceNotBillable bit not null