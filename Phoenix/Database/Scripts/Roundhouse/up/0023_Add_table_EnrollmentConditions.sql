
CREATE TABLE [dbo].[EnrollmentConditions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ActivityId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[ArticleTypeId] [int] NOT NULL,
	[LanguageId] [int] NULL,
	[CustomerId] [int] NULL,
	[Comment] [nvarchar](max) NULL,
	[Created] datetime2 not null,
	[LastModified] datetime2 not null,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_EnrollmentConditions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[EnrollmentConditions]  WITH CHECK ADD  CONSTRAINT [FK_EnrollmentConditions_EnrollmentConditions] FOREIGN KEY([ActivityId])
REFERENCES [dbo].[Activities] ([Id])
GO

ALTER TABLE [dbo].[EnrollmentConditions] CHECK CONSTRAINT [FK_EnrollmentConditions_EnrollmentConditions]
GO

ALTER TABLE [dbo].[EnrollmentConditions]  WITH CHECK ADD  CONSTRAINT [FK_EnrollmentConditions_ArticleTypes] FOREIGN KEY([ArticleTypeId])
REFERENCES [dbo].[ArticleTypes] ([Id])
GO

ALTER TABLE [dbo].[EnrollmentConditions] CHECK CONSTRAINT [FK_EnrollmentConditions_ArticleTypes]
GO

ALTER TABLE [dbo].[EnrollmentConditions]  WITH CHECK ADD  CONSTRAINT [FK_EnrollmentConditions_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO

ALTER TABLE [dbo].[EnrollmentConditions] CHECK CONSTRAINT [FK_EnrollmentConditions_Customers]
GO

ALTER TABLE [dbo].[EnrollmentConditions]  WITH CHECK ADD  CONSTRAINT [FK_EnrollmentConditions_Languages] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Languages] ([Id])
GO

ALTER TABLE [dbo].[EnrollmentConditions] CHECK CONSTRAINT [FK_EnrollmentConditions_Languages]
GO

ALTER TABLE [dbo].[EnrollmentConditions]  WITH CHECK ADD  CONSTRAINT [FK_EnrollmentConditions_Products] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO

ALTER TABLE [dbo].[EnrollmentConditions] CHECK CONSTRAINT [FK_EnrollmentConditions_Products]
GO


