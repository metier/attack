CREATE TABLE Article_Seminars(
	[Id] [int] NOT NULL,
	[PDU] [nvarchar](10) NULL
) ON [PRIMARY]

GO

ALTER TABLE Article_Seminars  WITH CHECK ADD  CONSTRAINT [FK_SeminarArticles_Articles] FOREIGN KEY([Id])
REFERENCES Articles ([Id])
ON DELETE CASCADE
GO

ALTER TABLE Article_Seminars CHECK CONSTRAINT [FK_SeminarArticles_Articles]
GO


