
alter table Participants add EnrollmentId int
go

Begin transaction

	update Participants
	set EnrollmentId = e.Id
	from participants p
	inner join Enrollments e on e.UserId = p.UserId and e.ActivitySetId = p.ActivitySetId


	update participants 
	set EnrollmentId =
		case 
			when (select count(*) from Enrollments e where e.UserId = p.UserId) = 1	
				then (select top 1 id from Enrollments e where e.UserId = p.UserId)
			when (select count(*) from Enrollments e where e.UserId = p.UserId and e.ActivitySetId <> p.ActivitySetId) = 1 
				then (select top 1 e.id from Enrollments e where e.UserId = p.UserId and e.ActivitySetId <> p.ActivitySetId)

			when (
				select count(*) from enrollments e 
				inner join ActivitySets_Activities asa on asa.ActivitySetId = e.ActivitySetId
				inner join Activities act on act.id = asa.ActivityId
				where 
					e.UserId = p.userId and
					act.CustomerArticleId = a.CustomerArticleId) = 1
				then (
					select top 1 e.id from enrollments e 
					inner join ActivitySets_Activities asa on asa.ActivitySetId = e.ActivitySetId
					inner join Activities act on act.id = asa.ActivityId
					where 
						e.UserId = p.userId and
						act.CustomerArticleId = a.CustomerArticleId)					
			else null
		End
	from participants p
	inner join activities a on a.id = p.ActivityId
	where EnrollmentId is null

	update participants set enrollmentid = 113358 where id = 153447
	update participants set enrollmentid = 105776 where id = 145499
	update participants set enrollmentid = 112134 where id = 157822
	update participants set enrollmentid = 119610 where id = 174279
	update participants set enrollmentid = 98487 where id = 133675
	update participants set enrollmentid = 99787 where id = 135932
	update participants set enrollmentid = 110167 where id = 153705
	update participants set enrollmentid = 110165 where id = 153703
	update participants set enrollmentid = 106758 where id = 147286
	update participants set enrollmentid = 104002 where id = 142617
	update participants set enrollmentid = 103707 where id = 142187
	update participants set enrollmentid = 102900 where id = 140930
	update participants set enrollmentid = 102286 where id = 139933
	update participants set enrollmentid = 63829 where id = 110200
	update participants set enrollmentid = 109789 where id = 152983
	update participants set enrollmentid = 99291 where id = 135128
	update participants set enrollmentid = 106809 where id = 147354
	update participants set enrollmentid = 68310 where id = 115856
	update participants set enrollmentid = 109305 where id = 152049
	update participants set enrollmentid = 104206 where id = 143007
	update participants set enrollmentid = 98022 where id = 132929
	update participants set enrollmentid = 94736 where id = 129443
	update participants set enrollmentid = 103277 where id = 141397
	update participants set enrollmentid = 99149 where id = 134855
	update participants set enrollmentid = 109915 where id = 153214
	update participants set enrollmentid = 111194 where id = 155824
	update participants set enrollmentid = 67184 where id = 117248
	update participants set enrollmentid = 67188 where id = 116088
	update participants set enrollmentid = 67181 where id = 116087
	update participants set enrollmentid = 98594 where id = 133904
	update participants set enrollmentid = 67185 where id = 117255
	update participants set enrollmentid = 67186 where id = 114807
	update participants set enrollmentid = 67194 where id = 117274
	update participants set enrollmentid = 67189 where id = 117249
	update participants set enrollmentid = 67197 where id = 117275
	update participants set enrollmentid = 67182 where id = 114625
	update participants set enrollmentid = 99158 where id = 134868
	update participants set enrollmentid = 99681 where id = 135735
	update participants set enrollmentid = 97967 where id = 132853
	update participants set enrollmentid = 97969 where id = 132857
	update participants set enrollmentid = 67187 where id = 117273
	update participants set enrollmentid = 67183 where id = 117256
	update participants set enrollmentid = 67199 where id = 113805
	update participants set enrollmentid = 106528 where id = 146846
	update participants set enrollmentid = 100225 where id = 136634
	update participants set enrollmentid = 94836 where id = 129595
	update participants set enrollmentid = 98057 where id = 132999
	update participants set enrollmentid = 66496 where id = 114436
	update participants set enrollmentid = 66496 where id = 114435
	update participants set enrollmentid = 69856 where id = 116368
	update participants set enrollmentid = 100417 where id = 136984
	update participants set enrollmentid = 68311 where id = 111876
	update participants set enrollmentid = 98502 where id = 127471
	update participants set enrollmentid = 107483 where id = 147919
	update participants set enrollmentid = 93950 where id = 127592
	update participants set enrollmentid = 115673 where id = 128238
	update participants set enrollmentid = 101226 where id = 138217
	update participants set enrollmentid = 98964 where id = 134711
	update participants set enrollmentid = 100745 where id = 137505
	update participants set enrollmentid = 100745 where id = 137504
	update participants set enrollmentid = 104791 where id = 143873
	update participants set enrollmentid = 101540 where id = 138830
	update participants set enrollmentid = 101540 where id = 138831
	update participants set enrollmentid = 101540 where id = 138832
	update participants set enrollmentid = 101665 where id = 138980
	update participants set enrollmentid = 102225 where id = 142830
	update participants set enrollmentid = 102225 where id = 142829
	update participants set enrollmentid = 109810 where id = 153014
	update participants set enrollmentid = 113376 where id = 160600
	update participants set enrollmentid = 109206 where id = 151866
	update participants set enrollmentid = 117979 where id = 170743
	update participants set enrollmentid = 110065 where id = 153471
	update participants set enrollmentid = 119264 where id = 153628
	update participants set enrollmentid = 119887 where id = 174925
	update participants set enrollmentid = 112648 where id = 158841
	update participants set enrollmentid = 112655 where id = 158863
	update participants set enrollmentid = 117895 where id = 170577

	update participants set enrollmentid = 97967 where id = 132852
	update participants set enrollmentid = 63167 where id = 132856
	update participants set enrollmentid = 111097 where id = 155635
	update participants set enrollmentid = 112491 where id = 171168
	update participants set enrollmentid = 118147 where id = 171169
	update participants set enrollmentid = 104538 where id = 143568

commit transaction


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Enrollments SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Participants ADD CONSTRAINT
	FK_Participants_Enrollments FOREIGN KEY
	(
	EnrollmentId
	) REFERENCES dbo.Enrollments
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Participants SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
