
insert into ProductCategories(name, ProductCodePrefix)
values ('Event', 'EV')

insert into ProductTypes(Name) values ('Event')
GO

declare @eventTypeId int
select @eventTypeId = id from ProductTypes where Name = 'Event'

insert into ProductTemplateParts ([order], title, ProductTypeId, LanguageId, CharLimit)
select [order], title, @eventTypeId, LanguageId, CharLimit from ProductTemplateParts where ProductTypeId = 3

insert into ArticleTypes (id, name, CharCode) values (10, 'Seminar', 'S')
insert into ArticleTypes (id, name, CharCode) values (11, 'Webinar', 'W')

GO
