
alter table UserInfoElements drop constraint FK_UserInfoElements_CustomLeadingTexts
go

begin transaction
go

Update UserInfoElements set CustomLeadingTextId = lt.id
from UserInfoElements u 
inner join CustomLeadingTexts clt on clt.id = u.CustomLeadingTextId
inner join LeadingTexts lt on lt.id = clt.LeadingTextId

commit

EXEC sp_rename 'UserInfoElements.CustomLeadingTextId', 'LeadingTextId', 'COLUMN';

alter table UserInfoElements add constraint FK_UserInfoElements_LeadingTexts FOREIGN KEY (LeadingTextId) REFERENCES LeadingTexts (Id);

