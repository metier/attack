
update Orders_Activities 
set price = a.FixedPrice,
	CurrencyCodeId = a.CurrencyCodeId,
	ActivityName = a.Name
from Orders_Activities oa
inner join Activities a
on a.id = oa.ActivityId
