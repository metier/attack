ALTER TABLE [ScormPerformances] ADD OriginalStatus varchar(1) null;
GO
UPDATE ScormPerformances SET OriginalStatus=Status;
GO
ALTER TABLE [ScormPerformances] ALTER COLUMN OriginalStatus varchar(1) NOT null;