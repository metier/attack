CREATE TABLE CustomInformation(
	Id			int IDENTITY(1,1) NOT NULL,
	EntityId	int not null,
	EntityType	nvarchar(20) not null,
	KeyField	nvarchar(max) null,
	Field1		nvarchar(max) null,
	Field2		nvarchar(max) null,
	Field3		nvarchar(max) null,
	Field4		nvarchar(max) null,
	Field5		nvarchar(max) null,
	RowVersion timestamp not null,
CONSTRAINT [PK_CustomInformation] PRIMARY KEY CLUSTERED (Id ASC)
)
GO

CREATE NONCLUSTERED INDEX IX_CustomInformation ON CustomInformation
(
	EntityType ASC,
	EntityId ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

