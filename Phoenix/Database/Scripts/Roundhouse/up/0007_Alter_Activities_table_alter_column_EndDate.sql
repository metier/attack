ALTER TABLE activities
ALTER COLUMN EndDate datetimeoffset null

GO

UPDATE activities 
SET EndDate = TODATETIMEOFFSET(dateadd(HOUR, 23, dateadd(MINUTE, 59, DATEADD(SECOND, 59, a.EndDate))), 
case when a.enddate > DATEADD(
			day,
			DATEDIFF(day,'19000107',
				DATEADD(month,
					DATEDIFF(MONTH, 0, convert(datetime, concat(year(a.EndDate), '.', '03', '.', '01'), 102))
					,30)
					)/
				7*7,
				'19000107')

		and a.enddate < DATEADD(
			day,
			DATEDIFF(day,'19000107',
				DATEADD(month,
					DATEDIFF(MONTH, 0, convert(datetime, concat(year(a.EndDate), '.', '10', '.', '01'), 102))
					,30)
					)/
				7*7,
				'19000107')
			then 2
			else 1
			end
	)
FROM activities a
WHERE a.EndDate is not null
