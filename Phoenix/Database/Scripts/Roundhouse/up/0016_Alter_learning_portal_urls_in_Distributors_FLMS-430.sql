declare @environment nvarchar(50)

select @environment = DB_NAME()

update Distributors 
	set PasswordResetFormUrl = case @environment
			when 'Dev_MetierLms'	then 'https://lms.metier.no/dev/learningportal/login.aspx'
			when 'Demo1_MetierLms'	then 'https://lms.metier.no/demo1/learningportal/login.aspx'
			when 'Test1_MetierLms'	then 'https://lms.metier.no/test1/learningportal/login.aspx'
			when 'Test2_MetierLms'	then 'https://lms.metier.no/test2/learningportal/login.aspx'
			when 'Test3_MetierLms'	then 'https://lms.metier.no/test3/learningportal/login.aspx'
			else 'https://mymetier.net/learningportal/login.aspx'
			end,
		LoginFormUrl = case @environment
			when 'Dev_MetierLms'	then 'https://lms.metier.no/dev/learningportal/login.aspx'
			when 'Demo1_MetierLms'	then 'https://lms.metier.no/demo1/learningportal/login.aspx'
			when 'Test1_MetierLms'	then 'https://lms.metier.no/test1/learningportal/login.aspx'
			when 'Test2_MetierLms'	then 'https://lms.metier.no/test2/learningportal/login.aspx'
			when 'Test3_MetierLms'	then 'https://lms.metier.no/test3/learningportal/login.aspx'
			else 'https://mymetier.net/learningportal/login.aspx'
			end		
where id <> 10000