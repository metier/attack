
CREATE TABLE [dbo].[VersionLogItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InternalReference] [nvarchar](50) NULL,
	[Title] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[DeploymentDate] [datetime] NOT NULL,
 CONSTRAINT [PK_VersionLogItems] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


