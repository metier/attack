if exists(select * from sysobjects where name like 'Customers_Migration' and xType = 'U') Drop table Customers_Migration; 
if exists(select * from sysobjects where name like 'Languages_Migration' and xType = 'U') Drop table Languages_Migration;
if exists(select * from sysobjects where name like 'Users_Migration' and xType = 'U') Drop table Users_Migration;
if exists(select * from sysobjects where name like 'ResourceTypes_Migration' and xType = 'U') Drop table ResourceTypes_Migration;
if exists(select * from sysobjects where name like 'Resources_Migration' and xType = 'U') Drop table Resources_Migration;
/*

if exists(select * from sysobjects where name like 'iLearnArticles' and xType='V') Drop view iLearnArticles
if exists(select * from sysobjects where name like 'iLearnRcos' and xType='V')	Drop view iLearnRcos
If exists(select * from sysobjects where name like 'ProductMappings' and xType='U') Drop table ProductMappings
If exists(select * from sysobjects where name like 'CustomerDistributors' and xType='U') Drop table CustomerDistributors
If exists(select * from sysobjects where name like 'getDistributorstats' and xType = 'IF') Drop function getDistributorstats
if exists(select * from sysobjects where name = 'iLearnEnrollments' and xtype='V') Drop view iLearnEnrollments

if exists(select * from sysobjects where name like 'GetParentContentGroups' and xType = 'IF') Drop function GetParentContentGroups;
if exists(select * from sysobjects where name like 'GetChildCustomers' and xType = 'IF') Drop function GetChildCustomers;
if exists(select * from sysobjects where name like 'GetChildCustomers' and xType = 'IF') Drop function GetParentCustomers;
if exists(select * from sysobjects where name like 'GetChildCustomers' and xType = 'IF') Drop function GetDistributerStats;

if exists(select * from sysobjects where name like 'iLearnCustomers' and xType = 'V') Drop view iLearnCustomers;
if exists(select * from sysobjects where name like 'MigrationMappings' and xType = 'U') Drop table MigrationMappings;		-- Maybe we should keep this a little longer...

if exists(select * from sysobjects where name like 'MigrationMappings' and xType = 'P') Drop procedure MigrateCustomers;
if exists(select * from sysobjects where name like 'MigrateCustomer' and xType = 'P') Drop procedure MigrateCustomer;
if exists(select * from sysobjects where name like 'InsertCustomer' and xType = 'P') Drop procedure InsertCustomer;
if exists(select * from sysobjects where name like 'InsertCustomerAddresses' and xType = 'P') Drop procedure InsertCustomerAddresses;
if exists(select * from sysobjects where name like 'InsertCustomerInvoicings' and xType = 'P') Drop procedure InsertCustomerInvoicings;
if exists(select * from sysobjects where name like 'InsertCustomLeadingTexts' and xType = 'P') Drop procedure InsertCustomLeadingTexts;
if exists(select * from sysobjects where name like 'InsertParticipantInfoDefinitions' and xType = 'P') Drop procedure InsertParticipantInfoDefinitions;
if exists(select * from sysobjects where name like 'InsertAutomailDefinition' and xType = 'P') Drop procedure InsertAutomailDefinition;
if exists(select * from sysobjects where name like 'InsertCustomer' and xType = 'P') Drop procedure InsertCustomer;
if exists(select * from sysobjects where name like 'InsertIndividualCustomers' and xType = 'P') Drop procedure InsertIndividualCustomers;
if exists(select * from sysobjects where name like 'InsertIndividualCustomerAddresses' and xType = 'P') Drop procedure InsertIndividualCustomerAddresses;
if exists(select * from sysobjects where name like 'InsertIndividualCustomerInvoicings' and xType = 'P') Drop procedure InsertIndividualCustomerInvoicings;
if exists(select * from sysobjects where name like 'InsertIndividualCustomersCustomLeadingTexts' and xType = 'P') Drop procedure InsertIndividualCustomersCustomLeadingTexts;
if exists(select * from sysobjects where name like 'InsertIndividualCustomersParticipantInfoDefinitions' and xType = 'P') Drop procedure InsertIndividualCustomersParticipantInfoDefinitions;
if exists(select * from sysobjects where name like 'InsertIndividualCustomersAutomailDefinition' and xType = 'P') Drop procedure InsertIndividualCustomersAutomailDefinition;

*/