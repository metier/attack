﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Factories;
using Metier.Phoenix.Core.Data.Entities;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Factories
{
    [TestFixture]
    public class PerformanceSummaryFactoryTests
    {
        private PerformanceSummaryFactory _performanceSummaryFactory;

        [SetUp]
        public void Setup()
        {
            _performanceSummaryFactory = new PerformanceSummaryFactory();
        }

        [Test]
        public void Create_ShouldSumPerformance()
        {
            var user = new User();
            var coursePerformances = new List<PerformanceInfo>
            {
                new PerformanceInfo
                {
                    Performances = new List<ScormPerformance>
                    {
                        new ScormPerformance { Score = 10},
                        new ScormPerformance { Score = 20 }
                    },
                    Course = new RcoCourse{Lessons = new List<RcoLesson>()}
                }
            };
            var result = _performanceSummaryFactory.Create(user, coursePerformances);
            Assert.AreEqual(30, result.TotalScore);
        }

        [Test]
        public void Create_GivenNoPerformances_ShouldReturnZero()
        {
            var user = new User();
            var coursePerformances = new List<PerformanceInfo>
            {
                new PerformanceInfo
                {
                    Performances = new List<ScormPerformance> (),
                    Course = new RcoCourse{Lessons = new List<RcoLesson>()}
                }
            };
            var result = _performanceSummaryFactory.Create(user, coursePerformances);
            Assert.AreEqual(0, result.TotalScore);
        }

        [Test]
        public void Create_GivenPerformancesNull_ShouldReturnZero()
        {
            var user = new User();
            var coursePerformances = new List<PerformanceInfo>
            {
                new PerformanceInfo
                {
                    Performances = null,
                    Course = new RcoCourse{Lessons = new List<RcoLesson>()}
                }
            };
            var result = _performanceSummaryFactory.Create(user, coursePerformances);
            Assert.AreEqual(0, result.TotalScore);
        }
    }
}