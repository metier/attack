﻿using System;
using System.Collections.Generic;
using Metier.Phoenix.Api.Facade.Participant;
using Metier.Phoenix.Api.Factories;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Factories
{
    [TestFixture]
    public class ParticipantFactoryTest
    {
        private Mock<IPerformanceProvider> _performanceProviderMock;
        private User user;
        private ParticipantFactory _participantFactory;

        [SetUp]
        public void SetUp()
        {
            _performanceProviderMock = new Mock<IPerformanceProvider>();
            _participantFactory = new ParticipantFactory(_performanceProviderMock.Object);
            user = SetUpUser();
        }

        private User SetUpUser()
        {
            var userId = 1;
            var customerId = 2;

            var mockUser = new User
            {
                Id = userId,
                CustomerId = customerId,
                UserInfoElements = new List<UserInfoElement>()
            };

            for (var count = 11; count <= 17; count++)
            {
                mockUser.UserInfoElements.Add(new UserInfoElement
                {
                    UserId = userId,
                    LeadingText = new LeadingText { Id = count, Text = string.Concat("customerId field ", count), IsMandatory = false, IsMandatoryFixed = false, IsVisible = false, IsVisibleFixed = false }
                });
            }

            return mockUser;
        }

        [Test]
        public void Create_MustSetActivitySetId()
        {
            var participant = _participantFactory.Create(new User(), new Enrollment(), new Activity(), 1337, null);
            Assert.AreEqual(1337, participant.ActivitySetId);
        }

        [Test]
        public void Create_EarliestInvoiceDateIsInTheFuture()
        {
            var earliestInvoiceDate = DateTime.UtcNow.AddDays(100);
            var participant = _participantFactory.Create(new User(), new Enrollment(), new Activity{EarliestInvoiceDate = earliestInvoiceDate}, 0, null);
            Assert.AreEqual(earliestInvoiceDate, participant.EarliestInvoiceDate);
        }

        [Test]
        public void Create_EarliestInvoiceDateIsInThePast()
        {
            var earliestInvoiceDate = DateTime.UtcNow.AddDays(-100);
            var participant = _participantFactory.Create(new User(), new Enrollment(), new Activity { EarliestInvoiceDate = earliestInvoiceDate }, 0, null);
            Assert.AreEqual(DateTime.UtcNow.Date, participant.EarliestInvoiceDate.Value.Date);
        }

        [Test]
        public void Create_EarliestInvoiceDateIsNotSet()
        {
            var participant = _participantFactory.Create(new User(), new Enrollment(), new Activity{EarliestInvoiceDate = null}, 0, null);
            Assert.AreEqual(DateTime.UtcNow.Date, participant.EarliestInvoiceDate.Value.Date);
        }

        
        [Test]
        public void Create_MustSetValuesFromUser()
        {
            var user = new User
            {
                Id = 1,
                CustomerId = 2,
                ParticipantNotBillable = true
            };
            var participant = _participantFactory.Create(user, new Enrollment(), new Activity(), 0, null);

            Assert.AreEqual(user.Id, participant.UserId);
            Assert.AreEqual(user.CustomerId, participant.CustomerId);
            Assert.AreEqual(user.ParticipantNotBillable, participant.IsNotBillable);
        }

        [Test]
        public void Create_MustSetValuesFromActivity()
        {
            var activity = new Activity
            {
                Id = 1,
                UnitPrice = 1234,
                CurrencyCodeId = 2
            };
            var participant = _participantFactory.Create(new User(), new Enrollment(), activity, 0, null);

            Assert.AreEqual(activity.Id, participant.ActivityId);
            Assert.AreEqual(activity.UnitPrice, participant.UnitPrice);
            Assert.AreEqual(activity.CurrencyCodeId, participant.CurrencyCodeId);
        }

        [Test]
        public void Create_MustSetStatusCodeValueEnrolled()
        {
            var participant = _participantFactory.Create(new User(), new Enrollment(), new Activity(), 0, null);

            Assert.AreEqual(ParticipantStatusCodes.Enrolled, participant.StatusCodeValue);
        }

        [Test]
        public void Create_MustCopyValuesInUserInfoElements_FromCustomer()
        {
            var customer = new Customer { Id = 1 };
            var user = new User { CustomerId = customer.Id };

            user.UserInfoElements.Add(new UserInfoElement { Id = 1, LeadingTextId = 11, LeadingText = new LeadingText { Id = 11, Text = "Custom field 1" }, InfoText = "text 1", UserId = 3 });
            user.UserInfoElements.Add(new UserInfoElement { Id = 2, LeadingTextId = 12, LeadingText = new LeadingText { Id = 12, Text = "Custom field 2" }, InfoText = "text 2", UserId = 3 });
            user.UserInfoElements.Add(new UserInfoElement { Id = 3, LeadingTextId = 12, LeadingText = new LeadingText { Id = 12, Text = "Custom field 2" }, InfoText = "text 2", UserId = 3 });

            var participant = _participantFactory.Create(user, new Enrollment(), new Activity(), 0, null);

            Assert.AreEqual(2, participant.ParticipantInfoElements.Count);

            Assert.AreEqual(user.UserInfoElements[0].InfoText, participant.ParticipantInfoElements[0].InfoText);
            Assert.AreEqual(user.UserInfoElements[0].LeadingTextId, participant.ParticipantInfoElements[0].LeadingTextId);

            Assert.AreEqual(user.UserInfoElements[1].InfoText, participant.ParticipantInfoElements[1].InfoText);
            Assert.AreEqual(user.UserInfoElements[1].LeadingTextId, participant.ParticipantInfoElements[1].LeadingTextId);
        }

        [Test]
        public void Create_MustUseValuesFromProvidedInfoElements()
        {
            var infoElements = new List<UserEnrollInfoElement>();
            infoElements.Add(new  UserEnrollInfoElement { LeadingTextId = 11, InfoText = "text 1" });
            infoElements.Add(new UserEnrollInfoElement { LeadingTextId = 12, InfoText = "text 2" });

            var participant = _participantFactory.Create(new User(), new Enrollment(), new Activity(), 0, infoElements);

            Assert.AreEqual(2, participant.ParticipantInfoElements.Count);

            Assert.AreEqual(infoElements[0].InfoText, participant.ParticipantInfoElements[0].InfoText);
            Assert.AreEqual(infoElements[0].LeadingTextId, participant.ParticipantInfoElements[0].LeadingTextId);

            Assert.AreEqual(infoElements[1].InfoText, participant.ParticipantInfoElements[1].InfoText);
            Assert.AreEqual(infoElements[1].LeadingTextId, participant.ParticipantInfoElements[1].LeadingTextId);
        }

        [Test]
        [TestCase(ScormLessonStatus.Complete, ParticipantStatusCodes.Completed)]
        [TestCase(ScormLessonStatus.Incomplete, ParticipantStatusCodes.InProgress)]
        [TestCase(ScormLessonStatus.Failed, ParticipantStatusCodes.Enrolled)]
        [TestCase(ScormLessonStatus.NotAttempted, ParticipantStatusCodes.Enrolled)]
        [TestCase(ScormLessonStatus.Passed, ParticipantStatusCodes.Enrolled)]
        public void Create_GivenActivityWithRcoId_MustSetStatusFromCoursePerformance(string performanceStatus, string participantStatus)
        {
            var user = new User { Id = 1 };
            var activity = new Activity { RcoCourseId = 2 };
            var performance = new ScormPerformance
            {
                Status = performanceStatus
            };
            _performanceProviderMock.Setup(p => p.GetParentPerformance(1, 2)).Returns(performance);

            var participant = _participantFactory.Create(user, new Enrollment(), activity, 0, new List<UserEnrollInfoElement>());

            Assert.AreEqual(participantStatus, participant.StatusCodeValue);
        }
    }
}