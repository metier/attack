﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Factories;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Factories
{
    [TestFixture]
    public class OrderInfoFactoryTests
    {
        private int _ids;
        private string _orderGroupByKey;

        private ContextMock _contextMock;
        private IOrderRepository _orderRepository;
        private Mock<IValidateOrder> _orderValidatorMock;
        private IOrderInfoFactory _orderInfoFactory;


        [SetUp]
        public void Setup()
        {
            _orderGroupByKey = "{DistributorId:1194,CustomerId:1001,ActivitySetId:11626673,CurrencyCodeId:262,Signature:\"--11627995-\"}";

            _contextMock = new ContextMock();
            _orderValidatorMock = new Mock<IValidateOrder>();
            _orderInfoFactory = new OrderInfoFactory(_contextMock.Object);
            _orderRepository = new OrderRepository(_contextMock.Object, _orderValidatorMock.Object);

            //Just for triggering the initialization of mappings between order and orderinfo.
            var ordersProvider = new OrdersProvider(_orderRepository, null, null, null, null, null, null, null, _contextMock.Object, null, null);        
        }

        [Test]
        public void GetProposedOrder_GivenNoOrderLines_MustConvertToOrderInfo()
        {
            var customer = CreateCustomer();
            var order = CreateProposedOrder(customer);
            var orderInfo = _orderInfoFactory.ConvertOrder(order);

            Assert.IsInstanceOf(typeof(OrderInfo), orderInfo);
        }

        [Test]
        public void GetProposedOrder_GivenParticipantOrderLines_MustConvertToOrderInfo()
        {
            var customer = CreateCustomer();
            var order = CreateProposedOrder(customer);

            var participant = CreateParticipant();
            var participantOrderLine = _orderRepository.CreateProposedOrderLine(participant);
            order.ParticipantOrderLines.Add(participantOrderLine);

            var orderInfo = _orderInfoFactory.ConvertOrder(order);
            var participantInfoOrderLine = orderInfo.OrderLines.First(o => o.Type == OrderLineType.Participant);
            var headerInfoOrderLine = orderInfo.OrderLines.First(o => o.Type == OrderLineType.UnitPriceActivity);

            Assert.AreEqual(1, orderInfo.OrderLines.Count(o => o.Type == OrderLineType.Participant));
            Assert.AreEqual(0, participantInfoOrderLine.ValueAddedTax);
            Assert.AreEqual(participant.ActivityId, participantInfoOrderLine.ParentActivityId);

            Assert.AreEqual(1, orderInfo.OrderLines.Count(o => o.Type == OrderLineType.UnitPriceActivity));     //Headerorderline
            Assert.AreEqual(participant.ActivityId, headerInfoOrderLine.Id);
            Assert.AreEqual(participant.ActivityId, headerInfoOrderLine.ParentActivityId);
        }


        //[Test]
        //public void GetOrderLine_MustSetType()
        //{
        //    var participant = CreateParticipant();
        //    var orderLine = _orderRepository.CreateProposedOrderLine(participant);

        //    Assert.AreEqual(OrderLineType.Participant, orderLine.);
        //}

        //[Test]
        //public void GetOrderLine_MustSetValueAddedTaxToZero()
        //{
        //    var participant = CreateParticipant();
        //    var orderLine = _orderRepository.CreateProposedOrderLine(participant);

        //    Assert.AreEqual(0, orderLine.ValueAddedTax);
        //}

        //[Test]
        //public void GetOrderLine_MustSetArticleFromCustomerArticle()
        //{
        //    var activity = CreateActivity();
        //    var participant = CreateParticipant(activity: activity);

        //    var customerArticle = _contextMock.Object.CustomerArticles.First(c => c.Id == activity.CustomerArticleId);
        //    customerArticle.ArticlePath = "expected article path";

        //    var orderLine = _orderRepository.CreateProposedOrderLine(participant);

        //    Assert.AreEqual(customerArticle.ArticlePath, orderLine.Article);
        //}

        //[Test]
        //public void GetOrderLine_MustSetId()
        //{
        //    var participant = CreateParticipant();
        //    var orderLine = _orderRepository.CreateProposedOrderLine(participant);

        //    Assert.AreEqual(orderLine.Id, participant.Id);
        //}

        //[Test]
        //public void GetOrderLine_MustSetParentActivityIdFromActivity()
        //{
        //    var activity = CreateActivity();
        //    var participant = CreateParticipant(activity: activity);

        //    var orderLine = _orderRepository.CreateProposedOrderLine(participant);

        //    Assert.AreEqual(activity.Id, orderLine.ParentActivityId);
        //}

        private Order CreateProposedOrder(Customer customer)
        {
            var order = new Order
            {
                GroupByKey = _orderGroupByKey,
                ActivityOrderLines = new List<ActivityOrderLine>(),
                ParticipantOrderLines = new List<ParticipantOrderLine>(),
                Customer = customer,
                Status = OrderStatus.Proposed,
                IsCreditNote = false,
                IsFullCreditNote = false,
                Title = "Just another order"
            };

            _contextMock.Object.Orders.Add(order);

            return order;
        }

        private Participant CreateParticipant(User user = null, Activity activity = null)
        {
            var customer = CreateCustomer();
            if (user == null) user = CreateUser(customer);
            if (activity == null) activity = CreateActivity();

            var participant = new Participant { Id = GetNextId(), UserId = user.Id, ActivityId = activity.Id, CustomerId = customer.Id, User = user};
            participant.ParticipantInfoElements = CreateParticipantInfoElements(participant.Id);

            _contextMock.Object.Participants.Add(participant);

            return participant;
        }

        private List<ParticipantInfoElement> CreateParticipantInfoElements(int participantId)
        {

            var elements = new List<ParticipantInfoElement>
            {
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participantId, InfoText = "", LeadingText = new LeadingText{Id = 11, Text = "Custom Field 1", IsVisible = true}},
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participantId, InfoText = "", LeadingText = new LeadingText{Id = 12, Text = "Custom Field 2", IsVisible = true}},
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participantId, InfoText = "", LeadingText = new LeadingText{Id = 13, Text = "Employee Id", IsVisible = true}},
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participantId, InfoText = "", LeadingText = new LeadingText{Id = 14, Text = "Department", IsVisible = true}},
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participantId, InfoText = "", LeadingText = new LeadingText{Id = 15, Text = "Position", IsVisible = true}},
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participantId, InfoText = "", LeadingText = new LeadingText{Id = 16, Text = "Supervisor", IsVisible = true}},
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participantId, InfoText = "", LeadingText = new LeadingText{Id = 17, Text = "Confirmation Email", IsVisible = true}}
            };

            return elements;
        }


        private Customer CreateCustomer()
        {
            var customer = new Customer { Id = GetNextId(), Name = "TestCustomer"};
            _contextMock.Object.Customers.Add(customer);
            return customer;
        }

        private User CreateUser(Customer customer)
        {
            var user = new User { Id = GetNextId(), FirstName = "firstname", LastName = "lastname", CustomerId = customer.Id, Customer = customer };
            _contextMock.Object.Users.Add(user);

            return user;
        }


        private Activity CreateActivity(CustomerArticle customerArticle = null)
        {
            if (customerArticle == null) customerArticle = CreateCustomerArticle();
            var activity = new Activity { Id = GetNextId(), CustomerArticleId = customerArticle.Id };
            _contextMock.Object.Activities.Add(activity);
            return activity;
        }

        private CustomerArticle CreateCustomerArticle()
        {
            var customerArticle = new CustomerArticle { Id = GetNextId() };
            _contextMock.Object.CustomerArticles.Add(customerArticle);
            return customerArticle;
        }

        private int GetNextId()
        {
            return ++_ids;
        }
    }
}
