﻿using System;
using System.Collections.Generic;
using Metier.Phoenix.Api.Factories.ExamPlayer;
using Metier.Phoenix.Core.Business;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Factories.ExamPlayer
{
    [TestFixture]
    public class ExamFacadeFactoryTests
    {
        private ExamPlayerFactory _factory;

        private Mock<IParticipantBusiness> _participantBusinessMock;

        [SetUp]
        public void Setup()
        {
            _participantBusinessMock = new Mock<IParticipantBusiness>();

            _factory = new ExamPlayerFactory(_participantBusinessMock.Object);
        }

        [Test]
        public void Create_GivenExam_ShouldSetId()
        {
            var exam = new Phoenix.Core.Data.Entities.Exam { Id = 1337 };
            var participant = new Participant();
            var activity = new Activity();
            var article = new ExamArticle();
            var result = _factory.CreateExam(exam, participant, activity, article);
            Assert.AreEqual(exam.Id, result.Id);
        }

        [Test]
        public void Create_GivenExam_ShouldSetIsSubmitted()
        {
            var exam = new Phoenix.Core.Data.Entities.Exam { Id = 1337, IsSubmitted = true };
            var participant = new Participant();
            var activity = new Activity();
            var article = new ExamArticle();
            var result = _factory.CreateExam(exam, participant, activity, article);
            Assert.AreEqual(exam.IsSubmitted, result.Status.IsSubmitted);
        }

        [Test]
        public void Create_GivenExamParticipantAndActivity_ShouldSetIsTimeRemaining()
        {
            var exam = new Phoenix.Core.Data.Entities.Exam { Id = 1337 };
            var participant = new Participant();
            var activity = new Activity(); 
            var article = new ExamArticle();

            _participantBusinessMock.Setup(p => p.GetDueDate(participant, activity, article, exam)).Returns(DateTime.UtcNow.AddMinutes(30));
            var result = _factory.CreateExam(exam, participant, activity, article);

            // Verify that the time remaining is between 30:00 and 29:59 minutes (the test may use a second to run)
            const long thirtyMinutes = 60 * 30;
            Assert.GreaterOrEqual(result.Status.TimeRemainingInSeconds, thirtyMinutes - 1);
            Assert.GreaterOrEqual(thirtyMinutes, result.Status.TimeRemainingInSeconds);
        }

        [Test]
        public void Create_GivenExamWithDeadlinePassed_ShouldSetIsTimeRemainingZero()
        {
            var exam = new Phoenix.Core.Data.Entities.Exam { Id = 1337 };
            var participant = new Participant();
            var activity = new Activity();
            var article = new ExamArticle();
            _participantBusinessMock.Setup(p => p.GetDueDate(participant, activity, article, exam)).Returns(DateTime.UtcNow.AddMinutes(-30));
            var result = _factory.CreateExam(exam, participant, activity, article);
            
            Assert.AreEqual(0, result.Status.TimeRemainingInSeconds);
        }

        [Test]
        public void Create_GivenExam_ShouldMapQuestionsWithOptions()
        {
            var exam = new Phoenix.Core.Data.Entities.Exam
            {
                Id = 1337,
                Questions = new List<Question>
                {
                    new Question
                    {
                        Id = 1,
                        Text = "What is the meaning of life?",
                        Type = QuestionTypes.MultipleChoiceMultiple,
                        QuestionOptions = new List<QuestionOption>
                        {
                            new QuestionOption { Id = 2, Text = "42" }
                        }
                    }
                }
            };
            var participant = new Participant();
            var activity = new Activity();
            var article = new ExamArticle();
            var result = _factory.CreateExam(exam, participant, activity, article);

            Assert.AreEqual(1, result.Questions.Count);
            Assert.AreEqual(exam.Questions[0].Id, result.Questions[0].Id);
            Assert.AreEqual(exam.Questions[0].Text, result.Questions[0].Text);
            Assert.AreEqual(exam.Questions[0].Type, result.Questions[0].Type);
            Assert.AreEqual(1, result.Questions[0].Order);

            Assert.AreEqual(1, result.Questions[0].Options.Count);
            Assert.AreEqual(exam.Questions[0].QuestionOptions[0].Id, result.Questions[0].Options[0].Id);
            Assert.AreEqual(exam.Questions[0].QuestionOptions[0].Text, result.Questions[0].Options[0].Text);
        }

        [Test]
        public void Create_GivenExam_ShouldMapAnswers()
        {
            var exam = new Phoenix.Core.Data.Entities.Exam
            {
                Id = 1337,
                Answers = new List<Answer>
                {
                    new Answer
                    {
                        Id = 2,
                        QuestionId = 3,
                        IsMarkedForReview = true
                    }
                }
            };
            var participant = new Participant();
            var activity = new Activity();
            var article = new ExamArticle();
            var result = _factory.CreateExam(exam, participant, activity, article);

            Assert.AreEqual(1, result.Answers.Count);
            Assert.AreEqual(exam.Answers[0].Id, result.Answers[0].Id);
            Assert.AreEqual(exam.Answers[0].QuestionId, result.Answers[0].QuestionId);
            Assert.AreEqual(exam.Answers[0].IsMarkedForReview, result.Answers[0].IsMarkedForReview);
        }

        [Test]
        public void Create_GivenExam_ShouldMapAnswers_WhenQuestionType_TrueFalse()
        {
            var exam = new Phoenix.Core.Data.Entities.Exam
            {
                Id = 1337,
                Questions = new List<Question>
                {
                    new Question
                    {
                        Id = 3,
                        Type = QuestionTypes.TrueFalse
                    }
                },
                Answers = new List<Answer>
                {
                    new Answer
                    {
                        Id = 2,
                        QuestionId = 3,
                        BoolAnswer = true
                    }
                }
            };
            var participant = new Participant();
            var activity = new Activity();
            var article = new ExamArticle();
            var result = _factory.CreateExam(exam, participant, activity, article);

            Assert.AreEqual(exam.Answers[0].BoolAnswer, result.Answers[0].BoolAnswer);
            Assert.IsNull(result.Answers[0].SingleChoiceAnswer);
            Assert.AreEqual(0, result.Answers[0].MultipleChoiceAnswers.Count);
        }

        [Test]
        public void Create_GivenExam_ShouldMapAnswers_WhenQuestionType_MultipleChoiceSingle()
        {
            var exam = new Phoenix.Core.Data.Entities.Exam
            {
                Id = 1337,
                Questions = new List<Question>
                {
                    new Question
                    {
                        Id = 3,
                        Type = QuestionTypes.MultipleChoiceSingle
                    }
                },
                Answers = new List<Answer>
                {
                    new Answer
                    {
                        Id = 2,
                        QuestionId = 3,
                        OptionsAnswers = new List<QuestionOption>
                        {
                            new QuestionOption
                            {
                                Id = 42, 
                                QuestionId = 3,
                                IsCorrect = true,
                                Text = "42"
                            }
                        }
                    }
                }
            };
            var participant = new Participant();
            var activity = new Activity();
            var article = new ExamArticle();
            var result = _factory.CreateExam(exam, participant, activity, article);

            Assert.IsNull(result.Answers[0].BoolAnswer);
            Assert.AreEqual(exam.Answers[0].OptionsAnswers[0].Id, result.Answers[0].SingleChoiceAnswer);
            Assert.AreEqual(0, result.Answers[0].MultipleChoiceAnswers.Count);
        }


        [Test]
        public void Create_GivenExam_ShouldMapAnswers_WhenQuestionType_MultipleChoiceMultiple()
        {
            var exam = new Phoenix.Core.Data.Entities.Exam
            {
                Id = 1337,
                Questions = new List<Question>
                {
                    new Question
                    {
                        Id = 3,
                        Type = QuestionTypes.MultipleChoiceMultiple
                    }
                },
                Answers = new List<Answer>
                {
                    new Answer
                    {
                        Id = 2,
                        QuestionId = 3,
                        OptionsAnswers = new List<QuestionOption>
                        {
                            new QuestionOption
                            {
                                Id = 42, 
                                QuestionId = 3,
                                IsCorrect = true,
                                Text = "42"
                            },
                            new QuestionOption
                            {
                                Id = 43, 
                                QuestionId = 3,
                                IsCorrect = false,
                                Text = "43"
                            }
                        }
                    }
                }
            };
            var participant = new Participant();
            var activity = new Activity();
            var article = new ExamArticle();
            var result = _factory.CreateExam(exam, participant, activity, article);

            Assert.IsNull(result.Answers[0].BoolAnswer);
            Assert.IsNull(result.Answers[0].SingleChoiceAnswer);
            Assert.AreEqual(2, result.Answers[0].MultipleChoiceAnswers.Count);
            Assert.AreEqual(42, result.Answers[0].MultipleChoiceAnswers[0]);
            Assert.AreEqual(43, result.Answers[0].MultipleChoiceAnswers[1]);
        }
    }
}