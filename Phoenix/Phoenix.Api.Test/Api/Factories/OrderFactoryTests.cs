﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Factories;
using Metier.Phoenix.Core.Data.Entities;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Factories
{
    [TestFixture]
    public class OrderFactoryTests
    {
        private Mock<IParticipantRepository> _participantRepositoryMock;
        private OrderFactory _factory;
        
        [SetUp]
        public void SetUp()
        {
            _participantRepositoryMock = new Mock<IParticipantRepository>();
            _factory = new OrderFactory(_participantRepositoryMock.Object);
        }

        [Test]
        public void GetOrderLine_MustSetText()
        {
            //var infoElements = new List<ParticipantInfoElement>();

            //TODO: Review and rewrite!
            //var participant = new Participant();
            //participant.ParticipantInfoElements = new List<ParticipantInfoElement>
            //{
            //    new ParticipantInfoElement { InfoText = "something1", CustomLeadingText = new CustomLeadingText { IsVisibleInvoiceLine = true, Text = "Leading1"} },
            //    new ParticipantInfoElement { InfoText = "something2", CustomLeadingText = new CustomLeadingText { IsVisibleInvoiceLine = false, Text = "Leading2"} },
            //    new ParticipantInfoElement { InfoText = "something3", CustomLeadingText = new CustomLeadingText { IsVisibleInvoiceLine = true, Text = "Leading3" } },
            //};

            //var user = new User {FirstName = "first", LastName = "last"};
            //participant.User = user;

            //var orderLine = _factory.GetOrderLineText(user.FirstName, user.LastName, participant);

            //Assert.AreEqual("first last, Leading1: something1, Leading3: something3", orderLine);
        }

        [Test]
        public void GetOrderLine_MustSetTextWhenNoLeadingTexts()
        {
            //TODO: Review and rewrite!
            //var participant = new Participant();
            //participant.ParticipantInfoElements = new List<ParticipantInfoElement>
            //{
            //    new ParticipantInfoElement { InfoText = null, CustomLeadingText = new CustomLeadingText { IsVisibleInvoiceLine = true, Text = null} },
            //    new ParticipantInfoElement { InfoText = null, CustomLeadingText = new CustomLeadingText { IsVisibleInvoiceLine = false, Text = null} },
            //    new ParticipantInfoElement { InfoText = null, CustomLeadingText = new CustomLeadingText { IsVisibleInvoiceLine = true, Text = null } },
            //};
            
            //var user = new User {FirstName = "first", LastName = "last"};
            //participant.User = user;

            //var orderLine = _factory.GetOrderLineText(user.FirstName, user.LastName, participant);

            //Assert.AreEqual("first last", orderLine);
        }
    }
}