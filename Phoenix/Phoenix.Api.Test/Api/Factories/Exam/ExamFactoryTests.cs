﻿using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Factories.Exam;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Exceptions;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Factories.Exam
{
    [TestFixture]
    public class ExamFactoryTests
    {
        private ExamFactory _factory;
        private ContextMock _contextMock;
        private ExamArticle _examArticle;
        private Activity _examActivity;
        private Participant _participant;
        private List<Rco> _rcos;

        [SetUp]
        public void Setup()
        {
            _rcos = new List<Rco> { new Rco { Id = 1 }, new Rco { Id = 2 }, new Rco { Id = 3 } };

            _examArticle = new ExamArticle
            {
                ExamType = ExamTypes.Random,
                LanguageId = 45
            };

            _examActivity = new Activity
            {
                Id = 1,
                ActivityExamCourses = new List<ActivityExamCourse>
                {
                    new ActivityExamCourse { ActivityId = 1, CourseRcoId = 1},
                    new ActivityExamCourse { ActivityId = 1, CourseRcoId = 2},
                    new ActivityExamCourse { ActivityId = 1, CourseRcoId = 3}
                }
            };

            var dummyActivity = new Activity
            {
                Id = 100
            };

            _participant = new Participant
            {
                UserId = 2,
                ActivityId = dummyActivity.Id
            };

            _contextMock = new ContextMock();
            _contextMock.Object.Activities.Add(dummyActivity);
            _rcos.ForEach(rco => _contextMock.Object.Rcos.Add(rco));
            GenerateAndAddExamQuestionsToContext();

            _factory = new ExamFactory(_contextMock.Object);
        }

        private void GenerateAndAddExamQuestionsToContext()
        {
            for (var i = 1; i <= 100; i++)
            {
                var rcos = new List<Rco>();
                int rcoId = 1;

                if (i > 50) rcoId = 2;
                if (i > 80) rcoId = 3;

                rcos = _rcos.Where(r => r.Id == rcoId).ToList();

                _contextMock.Object.Questions.Add(
                    new Question
                    {
                        Id = i,
                        Difficulty = QuestionDifficulties.Medium,
                        LanguageId = 45,
                        Status = QuestionStatuses.Published,
                        Rcos = rcos
                    }
                    );

                //Adding a few customer specific questions as well, based on the result of a modulo operation
                if (i % _rcos.Count == 1)
                    _contextMock.Object.Questions.Add(
                        new Question
                        {
                            Id = i + 10,
                            Difficulty = QuestionDifficulties.Medium,
                            LanguageId = 45,
                            CustomerId = 1,
                            Status = QuestionStatuses.Published,
                            Rcos = rcos
                        }
                        );
            }
        }

        [Test]
        public void GetQuestionsForUseInExam_GivenEnoughExamQuestionsAvailable_ReturnsCorrectSelectionOfQuestions()
        {
            _examActivity.MediumQuestionCount = 8;  
            _examActivity.MediumCustomerSpecificCount = 3;

            var exam = _factory.Create(_examArticle, _examActivity, _participant, new CustomerArticle { CustomerId = 1 });

            Assert.AreEqual(8, exam.Questions.Count);
        }

        [Test]
        public void GetQuestionsForUseInExam_GivenMoreCustomerSpecificQuestionsThanGenericQuestions_ReturnsOnlyCustomerSpecificQuestions()
        {
            _examActivity.MediumQuestionCount = 1;
            _examActivity.MediumCustomerSpecificCount = 10;

            var exam = _factory.Create(_examArticle, _examActivity, _participant, new CustomerArticle { CustomerId = 1 });

            Assert.AreEqual(0, exam.Questions.Where(q => !q.CustomerId.HasValue).ToList().Count);
            Assert.AreEqual(10, exam.Questions.Count);
        }

        [Test]
        public void GetQuestionsForUseInExam_GivenNumberOfQuestionsToSelectIsLessThanNumberOfRcos_ReturnsCorrectSelectionOfQuestions()
        {
            _examActivity.MediumQuestionCount = 2;

            var exam = _factory.Create(_examArticle, _examActivity, _participant, new CustomerArticle { CustomerId = 1 });

            Assert.AreEqual(0, exam.Questions.Where(q => q.CustomerId.HasValue).ToList().Count);
            Assert.AreEqual(2, exam.Questions.Count);
        }

        [Test]
        public void GetRandomQuestionsForUseInExam_GivenUnEqualNumberOfQuestionsPrRcoInContext_ReturnsEvenlyDistributedNumberOfQuestions()
        {
            _examActivity.MediumQuestionCount = 60;

            var exam = _factory.Create(_examArticle, _examActivity, _participant, new CustomerArticle { CustomerId = 1 });

            Assert.AreEqual(0, exam.Questions.Where(q => q.CustomerId.HasValue).ToList().Count);        //Make sure no customer specific questions are retrieved

            Assert.GreaterOrEqual(exam.Questions.Where(q => q.Rcos.Any(r => r.Id == 1)).ToList().Count, 18);
            Assert.GreaterOrEqual(exam.Questions.Where(q => q.Rcos.Any(r => r.Id == 2)).ToList().Count, 18);
            Assert.GreaterOrEqual(exam.Questions.Where(q => q.Rcos.Any(r => r.Id == 3)).ToList().Count, 18);
        }

        [Test]
        public void GetRandomQuestionsForUseInExam_GivenToManyQuestionsToRetrieveForSpecificRco_ReturnsUnEvenlyDistributedNumberOfQuestions()
        {
            _examActivity.MediumQuestionCount = 69;
            _examActivity.MediumCustomerSpecificCount = 0;

            var exam = _factory.Create(_examArticle, _examActivity, _participant, new CustomerArticle { CustomerId = 1 });

            Assert.AreEqual(0, exam.Questions.Where(q => q.CustomerId.HasValue).ToList().Count);        //Make sure no customer specific questions are retrieved

            Assert.LessOrEqual(23, exam.Questions.Where(q => q.Rcos.Any(r => r.Id == 1)).ToList().Count);
            Assert.LessOrEqual(23, exam.Questions.Where(q => q.Rcos.Any(r => r.Id == 2)).ToList().Count);
            Assert.AreEqual(20, exam.Questions.Where(q => q.Rcos.Any(r => r.Id == 3)).ToList().Count);
        }

    }
}