﻿using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.ViewEntities;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Data.Validation
{
    [TestFixture]
    public class QuestionValidatorTests
    {
        private QuestionValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new QuestionValidator();
        }

        [Test]
        public void Validate_Valid_NoErrors()
        {
            var question = GetValidQuestion();
            var result = _validator.Validate(question).ToList();

            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void Validate_ProductsAreRequired()
        {
            var question = GetValidQuestion();
            question.Products.Clear();
            var result = _validator.Validate(question).ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Products are required", result[0].ErrorMessage);
        }

        [Test]
        public void Validate_VersionsAreRequired()
        {
            var question = GetValidQuestion();
            question.Rcos.Clear();
            var result = _validator.Validate(question).ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Versions are required", result[0].ErrorMessage);
        }

        [Test]
        public void Validate_TextIsRequired()
        {
            var question = GetValidQuestion();
            question.Text = null;
            var result = _validator.Validate(question).ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Text is required", result[0].ErrorMessage);
        }

        [Test]
        public void Validate_TypeIsRequired()
        {
            var question = GetValidQuestion();
            question.Type = null;
            var result = _validator.Validate(question).ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Type is required", result[0].ErrorMessage);
        }

        [Test]
        public void Validate_InvalidType()
        {
            var question = GetValidQuestion();
            question.Type = "SomethingInvalid";
            var result = _validator.Validate(question).ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Invalid type", result[0].ErrorMessage);
        }

        [Test]
        public void Validate_LanguageIdRequired()
        {
            var question = GetValidQuestion();
            question.LanguageId = 0;
            var result = _validator.Validate(question).ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("LanguageId is required", result[0].ErrorMessage);
        }

        [Test]
        public void Validate_GivenTypeTrueFalse_CorrectBoolAnswerRequired_Invalid()
        {
            var question = GetValidQuestion();
            question.Type = QuestionTypes.TrueFalse;
            question.CorrectBoolAnswer = null;
            var result = _validator.Validate(question).ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("CorrectBoolAnswer is required", result[0].ErrorMessage);
        }

        [Test]
        public void Validate_GivenTypeTrueFalse_CorrectBoolAnswerRequired_Valid()
        {
            var question = GetValidQuestion();
            question.Type = QuestionTypes.TrueFalse;
            question.CorrectBoolAnswer = false;
            var result = _validator.Validate(question).ToList();

            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void Validate_GivenTypeMultipleChoiceSingle_MustHaveExactlyOneCorrectOption_Valid()
        {
            var question = GetValidQuestion();
            question.Type = QuestionTypes.MultipleChoiceSingle;
            question.QuestionOptions = new List<QuestionOption>
            {
                new QuestionOption { IsCorrect = true },
                new QuestionOption { IsCorrect = false }
            };
            var result = _validator.Validate(question).ToList();

            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void Validate_GivenTypeMultipleChoiceSingle_MustHaveExactlyOneCorrectOption_Invalid()
        {
            var question = GetValidQuestion();
            question.Type = QuestionTypes.MultipleChoiceSingle;
            question.QuestionOptions = new List<QuestionOption>
            {
                new QuestionOption { IsCorrect = true },
                new QuestionOption { IsCorrect = true }
            };
            var result = _validator.Validate(question).ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The question type does not allow multiple correct options", result[0].ErrorMessage);
        }

        [Test]
        [TestCase("S")]
        [TestCase("M")]
        public void Validate_GivenMultipleChoiceTypes_MustHaveAtLeastOneCorrectOption_Invalid(string type)
        {
            var question = GetValidQuestion();
            question.Type = type;
            question.QuestionOptions = new List<QuestionOption>
            {
                new QuestionOption { IsCorrect = false }
            };

            var result = _validator.Validate(question).ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Multiple choice questions must have at least one correct option", result[0].ErrorMessage);
        }


        [Test]
        [TestCase("S")]
        [TestCase("M")]
        public void Validate_GivenMultipleChoiceTypes_MustHaveAtLeastOneCorrectOption_Valid(string type)
        {
            var question = GetValidQuestion();
            question.Type = type;
            question.QuestionOptions = new List<QuestionOption>
            {
                new QuestionOption { IsCorrect = true }
            };

            var result = _validator.Validate(question).ToList();

            Assert.AreEqual(0, result.Count);
        }
        
        private Question GetValidQuestion()
        {
            return new Question
            {
                Text = "Question",
                Type = QuestionTypes.TrueFalse,
                Difficulty = QuestionDifficulties.Hard,
                LanguageId = 1234,
                CorrectBoolAnswer = true,
                Rcos = new List<Rco> {  new Rco() },
                Products = new List<Product> { new Product() }
            };
        }
    }
}