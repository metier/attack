﻿using System;
using System.Linq;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Core.Data.Entities;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Data.Validation
{
    [TestFixture]
    public class ActivitySetValidatorTests
    {
        private ContextMock _contextMock;
        private ActivitySetValidator _activitySetValidator;

        [SetUp]
        public void Setup()
        {
            _contextMock = new ContextMock();
            _activitySetValidator = new ActivitySetValidator(_contextMock.Object);
        }

        [Test]
        public void Validate_GivenActivitySetWithoutEnrollmentFromDate_ShouldReturnValidationError()
        {
            var activitySet = GetValidActivitySet();
            activitySet.EnrollmentFrom = null;

            var result = _activitySetValidator.Validate(activitySet).ToList();

            Assert.AreEqual("Enrollment from-date is required", result[0].ErrorMessage);
        }

        [Test]
        public void Validate_GivenActivitySetWithEnrollmentToDateEarlierThanFromDate_ShouldReturnValidationError()
        {
            var activitySet = GetValidActivitySet();
            activitySet.EnrollmentTo = activitySet.EnrollmentFrom - TimeSpan.FromDays(1);

            var result = _activitySetValidator.Validate(activitySet).ToList();

            Assert.AreEqual("Enrollment end date cannot be earlier than start date", result[0].ErrorMessage);
        }

        [Test]
        public void Validate_GivenPublishedActivitySetWithoutPublishFromDate_ShouldReturnValidationError()
        {
            var activitySet = GetValidActivitySet();
            activitySet.IsPublished = true;
            activitySet.PublishFrom = null;

            var result = _activitySetValidator.Validate(activitySet).ToList();

            Assert.AreEqual("Publish from-date is required when IsPublished property is set", result[0].ErrorMessage);
        }

        [Test]
        public void Validate_GivenPublishedActivitySetWithPublishToDateEarlierThanPublishFromDate_ShouldReturnValidationError()
        {
            var activitySet = GetValidActivitySet();
            activitySet.IsPublished = true;
            activitySet.PublishTo = activitySet.PublishFrom - TimeSpan.FromDays(1);

            var result = _activitySetValidator.Validate(activitySet).ToList();

            Assert.AreEqual("Publish end date cannot be earlier than start date", result[0].ErrorMessage);
        }

        [Test]
        public void Validate_GivenActivitySetWithoutActivities_ShouldBeAbleToDelete()
        {
            const int activitySetId = 123;

            var dbActivitySet = GetValidActivitySet();
            dbActivitySet.Id = activitySetId;
            _contextMock.Object.ActivitySets.Add(dbActivitySet);

            var activitySet = GetValidActivitySet();
            activitySet.Id = activitySetId;
            activitySet.IsDeleted = true;

            var result = _activitySetValidator.Validate(activitySet).ToList();

            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void Validate_GivenActivitySetContainingActivities_ShouldReturnValidationErrorIfDeleting()
        {
            const int activitySetId = 123;
            var existingActivity = new Activity();

            var dbActivitySet = GetValidActivitySet();
            dbActivitySet.Id = activitySetId;
            dbActivitySet.Activities.Add(existingActivity);
            _contextMock.Object.ActivitySets.Add(dbActivitySet);

            var activitySet = GetValidActivitySet();
            activitySet.Id = activitySetId;
            activitySet.IsDeleted = true;
            activitySet.Activities.Add(existingActivity);

            var result = _activitySetValidator.Validate(activitySet).ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Cannot delete activity set containing activities", result[0].ErrorMessage);
        }

        private ActivitySet GetValidActivitySet()
        {
            return new ActivitySet
            {
                Name = "TestActivitySet_" + Guid.NewGuid(),
                IsPublished = true,
                PublishFrom = DateTime.UtcNow,
                PublishTo = DateTime.UtcNow + TimeSpan.FromDays(100),
                EnrollmentFrom = DateTime.UtcNow + TimeSpan.FromDays(7),
                EnrollmentTo = DateTime.UtcNow + TimeSpan.FromDays(21),
                IsUnenrollmentAllowed = true,
                UnenrollmentDeadline = DateTime.UtcNow + TimeSpan.FromDays(28)
            };
        }
    }
}