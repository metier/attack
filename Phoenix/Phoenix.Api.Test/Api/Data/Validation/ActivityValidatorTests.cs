﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Data.Validation
{
    [TestFixture]
    public class ActivityValidatorTests
    {
        private ActivityValidator _activityValidator;

        [SetUp]
        public void Setup()
        {
            _activityValidator = new ActivityValidator();
        }

        [Test]
        public void Validate_GivenPeriodsWithStartBeforeEnd_ShouldNotReturnValidationError()
        {
            var article = new ArticleTestImpl();
            var activity = new Activity{FixedPriceNotBillable = true, UnitPriceNotBillable = true};
            activity.ActivityPeriods = new List<ActivityPeriod>
            {
                new ActivityPeriod { Start = DateTime.UtcNow, End = DateTime.UtcNow }
            };

            var result = _activityValidator.Validate(activity, article);

            Assert.AreEqual(0, result.Count());
        }
        
        [Test]
        public void Validate_GivenPeriodsWithEndBeforeStart_ShouldReturnValidationError()
        {
            var article = new ArticleTestImpl();
            var activity = new Activity { FixedPriceNotBillable = true, UnitPriceNotBillable = true };
            activity.ActivityPeriods = new List<ActivityPeriod>
            {
                new ActivityPeriod { Start = DateTime.UtcNow.AddMinutes(1), End = DateTime.UtcNow }
            };

            var result = _activityValidator.Validate(activity, article).ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("End cannot be before start", result[0].ErrorMessage);
        }

        [Test]
        [TestCase(ArticleTypes.CaseExam)]
        [TestCase(ArticleTypes.ExternalCertification)]
        [TestCase(ArticleTypes.ProjectAssignment)]
        public void Validate_GivenArticleType_DurationOrEndDateRequired(int articleTypeId)
        {
            var article = new ArticleTestImpl();
            article.ArticleTypeId = articleTypeId;

            var activity = new Activity {EndDate = null, Duration = null, FixedPriceNotBillable = true, UnitPriceNotBillable = true};
            var result = _activityValidator.Validate(activity, article);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Duration or end date is required", result.ElementAt(0).ErrorMessage);
        }

        [Test]
        [TestCase(ArticleTypes.CaseExam)]
        [TestCase(ArticleTypes.ExternalCertification)]
        [TestCase(ArticleTypes.ProjectAssignment)]
        public void Validate_GivenArticleType_BothDurationAndEndDateSet_ShouldReturnValidationError(int articleTypeId)
        {
            var article = new ArticleTestImpl();
            article.ArticleTypeId = articleTypeId;

            var activity = new Activity {Id = 1, Duration = 1000, EndDate = DateTime.UtcNow, FixedPriceNotBillable = true, UnitPriceNotBillable = true };
            var period = new ActivityPeriod { ActivityId = activity.Id, Start = DateTimeOffset.Now };
            activity.ActivityPeriods = new List<ActivityPeriod> { period };

            var result = _activityValidator.Validate(activity, article);
            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Duration or end date should be set - not both", result.ElementAt(0).ErrorMessage);
        }

        [Test]
        [TestCase(ArticleTypes.ELearning)]
        public void Validate_GivenArticleType_BothDurationAndPeriodEndDateSet_ShouldReturnValidationError(int articleTypeId)
        {
            var article = new ArticleTestImpl();
            article.ArticleTypeId = articleTypeId;

            var activity = new Activity {Id = 1, Duration = 1000, FixedPriceNotBillable = true, UnitPriceNotBillable = true };
            var period = new ActivityPeriod {ActivityId = activity.Id, Start = DateTimeOffset.Now, End = DateTimeOffset.Now.AddDays(90)};
            activity.ActivityPeriods = new List<ActivityPeriod> { period };

            var result = _activityValidator.Validate(activity, article);
            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Duration or end date should be set - not both", result.ElementAt(0).ErrorMessage);
        }

        [Test]
        [TestCase(ArticleTypes.ELearning)]
        [TestCase(ArticleTypes.MultipleChoice)]
        [TestCase(ArticleTypes.CaseExam)]
        [TestCase(ArticleTypes.ExternalCertification)]
        [TestCase(ArticleTypes.ProjectAssignment)]
        [TestCase(ArticleTypes.InternalCertification)]
        public void Validate_GivenArticleType_DurationSet_EndDateNotRequired(int articleTypeId)
        {
            var article = new ArticleTestImpl();
            article.ArticleTypeId = articleTypeId;

            var activity = new Activity {Id = 1, Duration = 1000, EndDate = null, FixedPriceNotBillable = true, UnitPriceNotBillable = true};
            var period = new ActivityPeriod { ActivityId = activity.Id, Start = DateTimeOffset.Now };
            activity.ActivityPeriods = new List<ActivityPeriod> { period };

            var result = _activityValidator.Validate(activity, article);
            Assert.AreEqual(0, result.Count());
        }

        [Test]
        [TestCase(ArticleTypes.CaseExam)]
        [TestCase(ArticleTypes.ExternalCertification)]
        [TestCase(ArticleTypes.ProjectAssignment)]
        public void Validate_GivenArticleType_EndDateSet_DurationNotRequired(int articleTypeId)
        {
            var article = new ArticleTestImpl();
            article.ArticleTypeId = articleTypeId;

            var activity = new Activity {Id = 1, Duration = null, EndDate = DateTime.UtcNow, FixedPriceNotBillable = true, UnitPriceNotBillable = true};
            var period = new ActivityPeriod { ActivityId = activity.Id, Start = DateTimeOffset.Now };
            activity.ActivityPeriods = new List<ActivityPeriod> { period };

            var result = _activityValidator.Validate(activity, article);

            Assert.AreEqual(0, result.Count());
        }


        [Test]
        [TestCase(ArticleTypes.ELearning)]
        public void Validate_GivenArticleType_DurationOrEndDateNotRequired(int articleTypeId)
        {
            var article = new ArticleTestImpl {ArticleTypeId = articleTypeId};

            var activity = new Activity {Id = 1, FixedPriceNotBillable = true, UnitPriceNotBillable = true};
            var period = new ActivityPeriod {ActivityId = 1, Start = DateTimeOffset.Now};
            activity.ActivityPeriods = new List<ActivityPeriod> {period};

            var result = _activityValidator.Validate(activity, article);
            Assert.AreEqual(0, result.Count());
        }

        [Test]
        [TestCase(ArticleTypes.ELearning)]
        [TestCase(ArticleTypes.MultipleChoice)]
        [TestCase(ArticleTypes.CaseExam)]
        [TestCase(ArticleTypes.ExternalCertification)]
        [TestCase(ArticleTypes.ProjectAssignment)]
        [TestCase(ArticleTypes.InternalCertification)]
        public void Validate_ShouldAllowOpenEndedPeriods(int articleTypeId)
        {
            var article = new ArticleTestImpl();
            article.ArticleTypeId = articleTypeId;
            var activity = new Activity { Duration = 1, FixedPriceNotBillable = true, UnitPriceNotBillable = true };
            activity.ActivityPeriods = new List<ActivityPeriod>
            {
                new ActivityPeriod { Start = DateTime.UtcNow, End = null }
            };

            var result = _activityValidator.Validate(activity, article);

            Assert.AreEqual(0, result.Count());
        }

        [Test]
        [TestCase(ArticleTypes.ELearning)]
        [TestCase(ArticleTypes.MultipleChoice)]
        [TestCase(ArticleTypes.CaseExam)]
        [TestCase(ArticleTypes.ExternalCertification)]
        [TestCase(ArticleTypes.ProjectAssignment)]
        [TestCase(ArticleTypes.InternalCertification)]
        public void Validate_ShouldRequireStartPeriod(int articleTypeId)
        {
            var article = new ArticleTestImpl();
            article.ArticleTypeId = articleTypeId;
            var activity = new Activity { Duration = 1, FixedPriceNotBillable = true, UnitPriceNotBillable = true };
            activity.ActivityPeriods = new List<ActivityPeriod>
            {
                new ActivityPeriod { }
            };

            var result = _activityValidator.Validate(activity, article);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Period start is required", result.ElementAt(0).ErrorMessage);
        }

        [Test]
        [TestCase(ArticleTypes.Classroom)]
        public void Validate_ShouldNotAllowOpenEndedPeriods(int articleTypeId)
        {
            var article = new ArticleTestImpl();
            article.ArticleTypeId = articleTypeId;
            var activity = new Activity { Duration = 1, FixedPriceNotBillable = true, UnitPriceNotBillable = true };
            activity.ActivityPeriods = new List<ActivityPeriod>
            {
                new ActivityPeriod { Start = DateTime.UtcNow, End = null }
            };

            var result = _activityValidator.Validate(activity, article);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Period end is required", result.ElementAt(0).ErrorMessage);
        }

        [Test]
        [TestCase(ArticleTypes.Classroom)]
        public void Validate_ShouldNotAllowEmptyPeriods(int articleTypeId)
        {
            var article = new ArticleTestImpl();
            article.ArticleTypeId = articleTypeId;
            var activity = new Activity {FixedPriceNotBillable = true, UnitPriceNotBillable = true};
            activity.ActivityPeriods = new List<ActivityPeriod>
            {
                new ActivityPeriod { }
            };

            var result = _activityValidator.Validate(activity, article);

            Assert.AreEqual(2, result.Count());
            Assert.AreEqual("Period start is required", result.ElementAt(0).ErrorMessage);
            Assert.AreEqual("Period end is required", result.ElementAt(1).ErrorMessage);
        }

        [Test]
        [TestCase(OrderStatus.Draft)]
        [TestCase(OrderStatus.Invoiced)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Rejected)]
        [TestCase(OrderStatus.Transferred)]
        public void Validate_GivenOrderStatus_ShouldReturnValidationErrorWhenChangingFixedPrice(OrderStatus orderStatus)
        {
            var article = new ArticleTestImpl();
            var postedActivity = new Activity {FixedPriceNotBillable = true, UnitPriceNotBillable = true};
            var currentActivity = new Activity {FixedPriceNotBillable = true, UnitPriceNotBillable = true};
            var currentOrder = new Order {Status = orderStatus};

            postedActivity.FixedPrice = 1.0m;
            currentActivity.FixedPrice = 2.0m;
            
            var result = _activityValidator.Validate(postedActivity, article, currentActivity, currentOrder);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Fixed price cannot be changed for activity because it already exists on an order", result.ElementAt(0).ErrorMessage);
        }

        [Test]
        [TestCase(OrderStatus.Proposed)]
        [TestCase(null)]
        public void Validate_GivenCurrentOrderStatus_ShouldAllowChangingIsNotBillableProperty(OrderStatus orderStatus)
        {
            var article = new ArticleTestImpl();
            var postedActivity = new Activity { FixedPriceNotBillable = true, UnitPriceNotBillable = true };
            var currentActivity = new Activity { FixedPriceNotBillable = true, UnitPriceNotBillable = true };
            var currentOrder = new Order { Status = orderStatus };

            postedActivity.FixedPrice = 1.0m;
            currentActivity.FixedPrice = 2.0m;

            var result = _activityValidator.Validate(postedActivity, article, currentActivity, currentOrder);

            Assert.AreEqual(0, result.Count());
        }

        [Test]
        [TestCase(OrderStatus.Draft)]
        [TestCase(OrderStatus.Invoiced)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Rejected)]
        [TestCase(OrderStatus.Transferred)]
        public void Validate_GivenOrderStatus_ShouldReturnValidationErrorWhenChangingCurrency(OrderStatus orderStatus)
        {
            var article = new ArticleTestImpl();
            var postedActivity = new Activity { FixedPriceNotBillable = true, UnitPriceNotBillable = true };
            var currentActivity = new Activity { FixedPriceNotBillable = true, UnitPriceNotBillable = true };
            var currentOrder = new Order { Status = orderStatus };

            postedActivity.CurrencyCodeId = 1;
            currentActivity.CurrencyCodeId = 2;

            var result = _activityValidator.Validate(postedActivity, article, currentActivity, currentOrder);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Currency cannot be changed for activity because it already exists on an order", result.ElementAt(0).ErrorMessage);
        }

        [Test]
        [TestCase(OrderStatus.Proposed)]
        [TestCase(null)]
        public void Validate_GivenCurrentOrderStatus_ShouldAllowChangingCurrency(OrderStatus orderStatus)
        {
            var article = new ArticleTestImpl();
            var postedActivity = new Activity { FixedPriceNotBillable = true, UnitPriceNotBillable = true };
            var currentActivity = new Activity { FixedPriceNotBillable = true, UnitPriceNotBillable = true };
            var currentOrder = new Order { Status = orderStatus };

            postedActivity.CurrencyCodeId = 1;
            currentActivity.CurrencyCodeId = 2;

            var result = _activityValidator.Validate(postedActivity, article, currentActivity, currentOrder);

            Assert.AreEqual(0, result.Count());
        }

        [Test]
        [TestCase(OrderStatus.Draft)]
        [TestCase(OrderStatus.Invoiced)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Rejected)]
        [TestCase(OrderStatus.Transferred)]
        public void Validate_GivenOrderStatus_ShouldReturnValidationErrorWhenChangingEarliestInvoiceDate(OrderStatus orderStatus)
        {
            var article = new ArticleTestImpl();
            var postedActivity = new Activity { FixedPriceNotBillable = true, UnitPriceNotBillable = true };
            var currentActivity = new Activity { FixedPriceNotBillable = true, UnitPriceNotBillable = true };
            var currentOrder = new Order { Status = orderStatus };

            postedActivity.EarliestInvoiceDate = DateTime.UtcNow - TimeSpan.FromDays(3);
            currentActivity.EarliestInvoiceDate = DateTime.UtcNow + TimeSpan.FromDays(3);

            var result = _activityValidator.Validate(postedActivity, article, currentActivity, currentOrder);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Earliest invoice date cannot be changed for activity because it already exists on an order", result.ElementAt(0).ErrorMessage);
        }

        [Test]
        [TestCase(OrderStatus.Proposed)]
        [TestCase(null)]
        public void Validate_GivenCurrentOrderStatus_ShouldAllowChangingEarliestInvoiceDate(OrderStatus orderStatus)
        {
            var article = new ArticleTestImpl();
            var postedActivity = new Activity { FixedPriceNotBillable = true, UnitPriceNotBillable = true };
            var currentActivity = new Activity { FixedPriceNotBillable = true, UnitPriceNotBillable = true };
            var currentOrder = new Order { Status = orderStatus };

            postedActivity.EarliestInvoiceDate = DateTime.UtcNow - TimeSpan.FromDays(3);
            currentActivity.EarliestInvoiceDate = DateTime.UtcNow + TimeSpan.FromDays(3);

            var result = _activityValidator.Validate(postedActivity, article, currentActivity, currentOrder);

            Assert.AreEqual(0, result.Count());
        }

        [Test]
        [TestCase(OrderStatus.Proposed)]
        [TestCase(null)]
        public void Validate_MultipleCourseConnectionsWithSameId_ShouldAllowChangingEarliestInvoiceDate(OrderStatus orderStatus)
        {
            var article = new ArticleTestImpl();
            var postedActivity = new Activity { FixedPriceNotBillable = true, UnitPriceNotBillable = true };
            var currentActivity = new Activity { FixedPriceNotBillable = true, UnitPriceNotBillable = true };
            var currentOrder = new Order { Status = orderStatus };

            postedActivity.EarliestInvoiceDate = DateTime.UtcNow - TimeSpan.FromDays(3);
            currentActivity.EarliestInvoiceDate = DateTime.UtcNow + TimeSpan.FromDays(3);

            var result = _activityValidator.Validate(postedActivity, article, currentActivity, currentOrder);

            Assert.AreEqual(0, result.Count());
        }

        [Test]
        [TestCase(1, 0, Result = false)]
        [TestCase(1, 1, Result = false)]
        [TestCase(1, 2, Result = true)]
        [TestCase(0, 0, Result = false)]
        [TestCase(0, 1, Result = true)]
        public bool Validate_GivenCombinationOfEasyQuestion_ReturnsExpectedResult(int numberOfQuestion, int numberOfCustomerSpecificQuestions)
        {
            var article = new ArticleTestImpl();
            var postedActivity = new Activity
            {
                EasyQuestionCount = numberOfQuestion,
                EasyCustomerSpecificCount = numberOfCustomerSpecificQuestions,
                FixedPriceNotBillable = true,
                UnitPriceNotBillable = true
            };

            var result = _activityValidator.Validate(postedActivity, article);

            var validationError = result.Where(ve => ve.ErrorMessage == "Cannot have more customer specific easy questions than total number of easy questions");
            return validationError.Any();
        }

        [Test]
        [TestCase(1, 0, Result = false)]
        [TestCase(1, 1, Result = false)]
        [TestCase(1, 2, Result = true)]
        [TestCase(0, 0, Result = false)]
        [TestCase(0, 1, Result = true)]
        public bool Validate_GivenCombinationOfMediumQuestion_ReturnsExpectedResult(int numberOfQuestion, int numberOfCustomerSpecificQuestions)
        {
            var article = new ArticleTestImpl();
            var postedActivity = new Activity
            {
                MediumQuestionCount = numberOfQuestion,
                MediumCustomerSpecificCount = numberOfCustomerSpecificQuestions,
                FixedPriceNotBillable = true,
                UnitPriceNotBillable = true
            };

            var result = _activityValidator.Validate(postedActivity, article);

            var validationError = result.Where(ve => ve.ErrorMessage == "Cannot have more customer specific medium questions than total number of medium questions");
            return validationError.Any();
        }

        [Test]
        [TestCase(1, 0, Result = false)]
        [TestCase(1, 1, Result = false)]
        [TestCase(1, 2, Result = true)]
        [TestCase(0, 0, Result = false)]
        [TestCase(0, 1, Result = true)]
        public bool Validate_GivenCombinationOfHardQuestion_ReturnsExpectedResult(int numberOfQuestion, int numberOfCustomerSpecificQuestions)
        {
            var article = new ArticleTestImpl();
            var postedActivity = new Activity
            {
                HardQuestionCount = numberOfQuestion,
                HardCustomerSpecificCount = numberOfCustomerSpecificQuestions,
                FixedPriceNotBillable = true,
                UnitPriceNotBillable = true
            };

            var result = _activityValidator.Validate(postedActivity, article);

            var validationError = result.Where(ve => ve.ErrorMessage == "Cannot have more customer specific hard questions than total number of hard questions");
            return validationError.Any();
        }


        [Test]
        public void Validate_GivenBillableActivity_ShouldReturnValidationErrorWhenUnitPriceIsMissing()
        {
            var article = new ArticleTestImpl();
            var activity = new Activity();

            activity.UnitPriceNotBillable = false;
            activity.FixedPriceNotBillable = true;
            activity.CurrencyCodeId = 262;

            var result = _activityValidator.Validate(activity, article);

            var validationError = result.Where(ve => ve.ErrorMessage == "A default unit price must be set for billable participants");
            Assert.AreEqual(1, validationError.Count());
        }

        [Test]
        public void Validate_GivenBillableActivity_ShouldReturnValidationErrorWhenFixedPriceIsMissing()
        {
            var article = new ArticleTestImpl();
            var activity = new Activity();


            activity.UnitPriceNotBillable = true;
            activity.FixedPriceNotBillable = false;
            activity.CurrencyCodeId = 262;

            var result = _activityValidator.Validate(activity, article);

            var validationError = result.Where(ve => ve.ErrorMessage == "A fixed price must be set for billable activities");
            Assert.AreEqual(1, validationError.Count());
        }

        [Test]
        [TestCase(false, false, null)]
        [TestCase(false, true, null)]
        [TestCase(true, false, null)]
        [TestCase(false, false, 0)]
        public void Validate_GivenBillableActivity_ShouldReturnValidationErrorWhenCurrencyCodeIsMissing(bool isFixedPriceNotBillable, bool isUnitPriceNotBillable, int? currencyCodeId)
        {
            var article = new ArticleTestImpl();
            var activity = new Activity();
            
            activity.FixedPriceNotBillable = isFixedPriceNotBillable;
            activity.FixedPrice = 100;

            activity.UnitPriceNotBillable = isUnitPriceNotBillable;
            activity.UnitPrice = 100;
            
            activity.CurrencyCodeId = currencyCodeId;

            var result = _activityValidator.Validate(activity, article);

            var validationError = result.Where(ve => ve.ErrorMessage == "A currencycode must be set for activities where fixed price or unit price is specified");
            Assert.AreEqual(1, validationError.Count());
        }



        private class ArticleTestImpl : Article
        {
            
        }


    }
}