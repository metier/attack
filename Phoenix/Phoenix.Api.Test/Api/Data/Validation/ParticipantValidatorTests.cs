﻿using System;
using System.Linq;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Data.Validation
{
    [TestFixture]
    public class ParticipantValidatorTests
    {
        private ParticipantValidator _participantValidator;


        [SetUp]
        public void SetUp()
        {
            var contextMock = new ContextMock();
            _participantValidator = new ParticipantValidator(contextMock.Object);
        }

        [Test]
        [TestCase(OrderStatus.Draft)]
        [TestCase(OrderStatus.Invoiced)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Rejected)]
        [TestCase(OrderStatus.Transferred)]
        public void Validate_GivenOrderStatus_ShouldReturnValidationErrorWhenChangingIsNotBillableProperty(OrderStatus orderStatus)
        {
            var currentParticipant = GetValidParticipant();
            var postedParticipant = GetValidParticipant();

            postedParticipant.IsNotBillable = !currentParticipant.IsNotBillable;
            var order = new Order() {Status = orderStatus};
            var result = _participantValidator.Validate(postedParticipant, currentParticipant, order);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Billable status cannot be changed for participation because it already exists on an order", result.ElementAt(0).ErrorMessage);
        }


        [Test]
        [TestCase(OrderStatus.Proposed)]
        [TestCase(null)]
        public void Validate_GivenCurrentOrderStatus_ShouldAllowChangingIsNotBillableProperty(OrderStatus orderStatus)
        {
            var currentParticipant = GetValidParticipant();
            var postedParticipant = GetValidParticipant();

            postedParticipant.IsNotBillable = !currentParticipant.IsNotBillable;

            var order = new Order() { Status = orderStatus };
            var result = _participantValidator.Validate(postedParticipant, currentParticipant, order);

            Assert.AreEqual(0, result.Count());
        }

        #region Unit tests for Credit notes

        [Test]
        [TestCase(OrderStatus.Proposed)]
        [TestCase(OrderStatus.Draft)]
        public void Validate_GivenCreditOrderStatus_ShouldReturnValidationErrorWhenChangingIsNotBillableProperty(OrderStatus orderStatus)
        {
            var currentParticipant = GetValidParticipant();
            var postedParticipant = GetValidParticipant();

            postedParticipant.IsNotBillable = !currentParticipant.IsNotBillable;
            var creditOrder = new Order() { CreditedOrderId = 1000, IsCreditNote = true, Status = orderStatus };
            var result = _participantValidator.Validate(postedParticipant, currentParticipant, creditOrder);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Billable status cannot be changed for participation because it already exists on an order", result.ElementAt(0).ErrorMessage);
        }


        [Test]
        [TestCase(OrderStatus.Invoiced)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Transferred)]
        [TestCase(OrderStatus.Rejected)]
        public void Validate_GivenCurrentCreditOrderStatus_ShouldAllowChangingIsNotBillableProperty(OrderStatus orderStatus)
        {
            var currentParticipant = GetValidParticipant();
            var postedParticipant = GetValidParticipant();

            postedParticipant.IsNotBillable = !currentParticipant.IsNotBillable;
            var creditOrder = new Order() { CreditedOrderId = 1000, IsCreditNote = true, Status = orderStatus };

            var result = _participantValidator.Validate(postedParticipant, currentParticipant, creditOrder);

            Assert.AreEqual(0, result.Count());
        }

        [Test]
        [TestCase(OrderStatus.Proposed)]
        [TestCase(OrderStatus.Draft)]
        public void Validate_GivenCreditOrderStatus_ShouldReturnValidationErrorWhenChangingCurrencyOnParticipation(OrderStatus orderStatus)
        {
            var currentParticipant = GetValidParticipant();
            var postedParticipant = GetValidParticipant();

            postedParticipant.CurrencyCodeId = 1;
            currentParticipant.CurrencyCodeId = 2;
            var creditOrder = new Order() { CreditedOrderId = 1000, IsCreditNote = true, Status = orderStatus };
            var result = _participantValidator.Validate(postedParticipant, currentParticipant, creditOrder);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Currency cannot be changed for participation because it already exists on an order", result.ElementAt(0).ErrorMessage);
        }

        [Test]
        [TestCase(OrderStatus.Invoiced)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Rejected)]
        [TestCase(OrderStatus.Transferred)]
        public void Validate_GivenCreditOrderStatus_ShouldAllowChangingCurrencyOnParticipation(OrderStatus orderStatus)
        {
            var currentParticipant = GetValidParticipant();
            var postedParticipant = GetValidParticipant();

            postedParticipant.CurrencyCodeId = 1;
            currentParticipant.CurrencyCodeId = 2;

            var creditOrder = new Order() { CreditedOrderId = 1000, IsCreditNote = true, Status = orderStatus };
            var result = _participantValidator.Validate(postedParticipant, currentParticipant, creditOrder);

            Assert.AreEqual(0, result.Count());
        }

        [Test]
        [TestCase(OrderStatus.Draft)]
        [TestCase(OrderStatus.Proposed)]
        public void Validate_GivenCreditOrderStatus_ShouldReturnValidationErrorWhenChangingEarliestInvoiceDate(OrderStatus orderStatus)
        {
            var currentParticipant = GetValidParticipant();
            var postedParticipant = GetValidParticipant();

            postedParticipant.EarliestInvoiceDate = DateTime.UtcNow - TimeSpan.FromDays(3);
            currentParticipant.EarliestInvoiceDate = DateTime.UtcNow - TimeSpan.FromDays(2);

            var creditOrder = new Order() { CreditedOrderId = 1000, IsCreditNote = true, Status = orderStatus };
            var result = _participantValidator.Validate(postedParticipant, currentParticipant, creditOrder);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Earliest invoice date cannot be changed for participation because it already exists on an order", result.ElementAt(0).ErrorMessage);
        }

        [Test]
        [TestCase(OrderStatus.Invoiced)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Rejected)]
        [TestCase(OrderStatus.Transferred)]
        public void Validate_GivenCreditOrderStatus_ShouldAllowChangingEarliestInvoiceDate(OrderStatus orderStatus)
        {
            var currentParticipant = GetValidParticipant();
            var postedParticipant = GetValidParticipant();

            postedParticipant.EarliestInvoiceDate = DateTime.UtcNow - TimeSpan.FromDays(3);
            currentParticipant.EarliestInvoiceDate = DateTime.UtcNow - TimeSpan.FromDays(2);

            var creditOrder = new Order() { CreditedOrderId = 1000, IsCreditNote = true, Status = orderStatus };
            var result = _participantValidator.Validate(postedParticipant, currentParticipant, creditOrder);

            Assert.AreEqual(0, result.Count());
        }

        [Test]
        [TestCase(OrderStatus.Draft)]
        [TestCase(OrderStatus.Proposed)]
        public void Validate_GivenCreditOrderStatus_ShouldReturnValidationErrorWhenChangingOnlinePaymentReference(OrderStatus orderStatus)
        {
            var currentParticipant = GetValidParticipant();
            var postedParticipant = GetValidParticipant();

            postedParticipant.OnlinePaymentReference = "New";
            currentParticipant.OnlinePaymentReference = "Old";

            var creditOrder = new Order() { CreditedOrderId = 1000, IsCreditNote = true, Status = orderStatus };
            var result = _participantValidator.Validate(postedParticipant, currentParticipant, creditOrder);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Online payment reference cannot be changed for participation because it already exists on an order", result.ElementAt(0).ErrorMessage);
        }

        [Test]
        [TestCase(OrderStatus.Invoiced)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Rejected)]
        [TestCase(OrderStatus.Transferred)]
        public void Validate_GivenCreditOrderStatus_ShouldAllowChangingOnlinePaymentReference(OrderStatus orderStatus)
        {
            var currentParticipant = GetValidParticipant();
            var postedParticipant = GetValidParticipant();

            postedParticipant.OnlinePaymentReference = "New";
            currentParticipant.OnlinePaymentReference = "Old";

            var creditOrder = new Order() { CreditedOrderId = 1000, IsCreditNote = true, Status = orderStatus };
            var result = _participantValidator.Validate(postedParticipant, currentParticipant, creditOrder);

            Assert.AreEqual(0, result.Count());
        }

        #endregion

        [Test]
        [TestCase(OrderStatus.Draft)]
        [TestCase(OrderStatus.Invoiced)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Rejected)]
        [TestCase(OrderStatus.Transferred)]
        public void Validate_GivenOrderStatus_ShouldReturnValidationErrorWhenChangingCurrencyOnParticipation(OrderStatus orderStatus)
        {
            var currentParticipant = GetValidParticipant();
            var postedParticipant = GetValidParticipant();

            postedParticipant.CurrencyCodeId = 1;
            currentParticipant.CurrencyCodeId = 2;
            var order = new Order() { Status = orderStatus };
            var result = _participantValidator.Validate(postedParticipant, currentParticipant, order);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Currency cannot be changed for participation because it already exists on an order", result.ElementAt(0).ErrorMessage);
        }

        [Test]
        [TestCase(OrderStatus.Proposed)]
        [TestCase(null)]
        public void Validate_GivenOrderStatus_ShouldAllowChangingCurrencyOnParticipation(OrderStatus orderStatus)
        {
            var currentParticipant = GetValidParticipant();
            var postedParticipant = GetValidParticipant();

            postedParticipant.CurrencyCodeId = 1;
            currentParticipant.CurrencyCodeId = 2;

            var order = new Order() { Status = orderStatus };
            var result = _participantValidator.Validate(postedParticipant, currentParticipant, order);

            Assert.AreEqual(0, result.Count());
        }

        [Test]
        [TestCase(OrderStatus.Draft)]
        [TestCase(OrderStatus.Invoiced)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Rejected)]
        [TestCase(OrderStatus.Transferred)]
        public void Validate_GivenOrderStatus_ShouldReturnValidationErrorWhenChangingEarliestInvoiceDate(OrderStatus orderStatus)
        {
            var currentParticipant = GetValidParticipant();
            var postedParticipant = GetValidParticipant();

            postedParticipant.EarliestInvoiceDate = DateTime.UtcNow - TimeSpan.FromDays(3);
            currentParticipant.EarliestInvoiceDate = DateTime.UtcNow - TimeSpan.FromDays(2);

            var order = new Order() { Status = orderStatus };
            var result = _participantValidator.Validate(postedParticipant, currentParticipant, order);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Earliest invoice date cannot be changed for participation because it already exists on an order", result.ElementAt(0).ErrorMessage);
        }

        [Test]
        [TestCase(null, 262)]
        [TestCase(1000, null)]
        [TestCase(1000, 0)]
        public void Validate_GivenBillableParticipant_ShouldReturnValidationErrorWhenNoPriceOrCurrencyIsSpecified(int? price, int? currencyCodeId)
        {
            var participant = GetValidParticipant();
            var dbParticipant = GetValidParticipant();
            var order = new Order{ Status = OrderStatus.Proposed };

            participant.IsNotBillable = false;
            
            participant.UnitPrice = price;
            participant.CurrencyCodeId = currencyCodeId;

            var result = _participantValidator.Validate(participant, dbParticipant, order);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Price and currency must be specified for billable participants", result.ElementAt(0).ErrorMessage);
        }
       

        [Test]
        [TestCase(OrderStatus.Proposed)]
        [TestCase(null)]
        public void Validate_GivenOrderStatus_ShouldAllowChangingEarliestInvoiceDate(OrderStatus orderStatus)
        {
            var currentParticipant = GetValidParticipant();
            var postedParticipant = GetValidParticipant();

            postedParticipant.EarliestInvoiceDate = DateTime.UtcNow - TimeSpan.FromDays(3);
            currentParticipant.EarliestInvoiceDate = DateTime.UtcNow - TimeSpan.FromDays(2);

            var order = new Order() { Status = orderStatus };
            var result = _participantValidator.Validate(postedParticipant, currentParticipant, order);

            Assert.AreEqual(0, result.Count());
        }

        [Test]
        [TestCase(OrderStatus.Draft)]
        [TestCase(OrderStatus.Invoiced)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Rejected)]
        [TestCase(OrderStatus.Transferred)]
        public void Validate_GivenOrderStatus_ShouldReturnValidationErrorWhenChangingOnlinePaymentReference(OrderStatus orderStatus)
        {
            var currentParticipant = GetValidParticipant();
            var postedParticipant = GetValidParticipant();

            postedParticipant.OnlinePaymentReference = "New";
            currentParticipant.OnlinePaymentReference = "Old";

            var order = new Order() { Status = orderStatus };
            var result = _participantValidator.Validate(postedParticipant, currentParticipant, order);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Online payment reference cannot be changed for participation because it already exists on an order", result.ElementAt(0).ErrorMessage);
        }

        [Test]
        [TestCase(OrderStatus.Proposed)]
        [TestCase(null)]
        public void Validate_GivenOrderStatus_ShouldAllowChangingOnlinePaymentReference(OrderStatus orderStatus)
        {
            var currentParticipant = GetValidParticipant();
            var postedParticipant = GetValidParticipant();

            postedParticipant.OnlinePaymentReference = "New";
            currentParticipant.OnlinePaymentReference = "Old";

            var order = new Order() { Status = orderStatus };
            var result = _participantValidator.Validate(postedParticipant, currentParticipant, order);

            Assert.AreEqual(0, result.Count());
        }

        private Participant GetValidParticipant()
        {
            return new Participant{UnitPrice = 100, CurrencyCodeId = 262, IsNotBillable = false};
        }
    }
}