﻿using System.Linq;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Api.Services.ExternalCustomers;
using Metier.Phoenix.Core.Data.Entities;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Data.Validation
{
    [TestFixture]
    public class CustomerValidatorTests
    {
        CustomerValidator _validator;
        private ContextMock _contextMock;

        [SetUp]
        public void Setup()
        {
            _contextMock = new ContextMock();

            _validator = new CustomerValidator(null, null, _contextMock.Object);
        }

        [Test]
        public void Validate_ShouldNotReturnValidationResult_WhenValidCustomer()
        {
            var customer = GetValidCustomer();
            var result = _validator.ValidateCustomer(customer).ToList();
            
            Assert.AreEqual(0, result.Count);
        }
       
        [Test]
        public void Validate_ShouldReturnValidationResult_WhenNameIsNull()
        {
            var customer = GetValidCustomer();
            customer.Name = null;
            var result = _validator.ValidateCustomer(customer).ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Name is required", result[0].ErrorMessage);
        }

        [Test]
        public void Validate_ShouldReturnValidationResult_WhenLanguageIdIsNull()
        {
            var customer = GetValidCustomer();
            customer.LanguageId = null;
            var result = _validator.ValidateCustomer(customer).ToList();
            
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Language is required", result[0].ErrorMessage);
        }

        //TODO: Remove
        //[Test]
        //public void Validate_ShouldReturnValidationResult_GivenCustomerWithExternalCustomerProvider_WhenExternalCustomerIdIsNull()
        //{
        //    var customer = GetValidCustomer();
        //    _externalCustomerProviderResolverMock.Setup(r => r.HasExternalCustomersProviders(It.IsAny<int>())).Returns(true);
        //    customer.ExternalCustomerId = null;
        //    var result = _validator.ValidateCustomer(customer).ToList();

        //    Assert.AreEqual(1, result.Count);
        //    Assert.AreEqual("External Customer ID is required", result[0].ErrorMessage);
        //}

        [Test]
        public void Validate_ShouldReturnValidationResult_GivenCustomerWithExternalCustomerProvider_WhenExternalCustomerIdIsChanged()
        {
            var customerId = 1111;
            var customer = GetValidCustomer(customerId, "1000");

            _contextMock.Object.Customers.Add(customer);

            var changedCustomer = GetValidCustomer(customerId, "2000");
            var result = _validator.ValidateCustomer(changedCustomer).ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("External Customer ID can not be changed", result[0].ErrorMessage);
        }

        [Test]
        public void Validate_ShouldReturnValidationResult_GivenCustomerWithExternalCustomerProvider_WhenExternalCustomerIdIsRemoved()
        {
            var customerId = 1111;
            var customer = GetValidCustomer(customerId, "1000");

            _contextMock.Object.Customers.Add(customer);

            var changedCustomer = GetValidCustomer(customerId, string.Empty);
            var result = _validator.ValidateCustomer(changedCustomer).ToList();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("External Customer ID can not be changed", result[0].ErrorMessage);
        }

        [Test]
        public void Validate_ShouldReturnValidationResult_GivenCustomerWithExternalCustomerProviderButNo_WhenExternalCustomerIdIsChanged()
        {
            var customerId = 1111;
            var customer = GetValidCustomer(customerId, string.Empty);

            _contextMock.Object.Customers.Add(customer);

            var changedCustomer = GetValidCustomer(customerId, "2000");
            var result = _validator.ValidateCustomer(changedCustomer).ToList();

            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void HasParentReferenceToSelf_HasNoParentId_ReturnsFalse()
        {
            var customer = new Customer {Id = 1};
            var result = _validator.HasParentReferenceToSelf(customer, customer.Id);

            Assert.IsFalse(result);
        }

        [Test]
        public void HasParentReferenceToSelf_HasSelfAsParentId_ReturnsTrue()
        {
            var customer = new Customer { Id = 1, ParentId = 1};
            var result = _validator.HasParentReferenceToSelf(customer, customer.Id);

            Assert.IsTrue(result);
        }

        [Test]
        public void HasParentReferenceToSelf_HasDifferentParentId_ReturnsFalse()
        {
            var parentCustomer = new Customer{Id = 2};
            _contextMock.Object.Customers.Add(parentCustomer);

            var customer = new Customer { Id = 1, ParentId = 2 };
            var result = _validator.HasParentReferenceToSelf(customer, customer.Id);

            Assert.IsFalse(result);
        }

        [Test]
        public void HasParentReferenceToSelf_HasDifferentParentIds_ReturnsFalse()
        {
            var secondParentCustomer = new Customer { Id = 3 };
            _contextMock.Object.Customers.Add(secondParentCustomer);

            var parentCustomer = new Customer { Id = 2, ParentId = 3};
            _contextMock.Object.Customers.Add(parentCustomer);

            var customer = new Customer { Id = 1, ParentId = 2 };
            var result = _validator.HasParentReferenceToSelf(customer, customer.Id);

            Assert.IsFalse(result);
        }

        [Test]
        public void HasParentReferenceToSelf_HasDSelfInParentChain_ReturnsTrue()
        {
            var secondParentCustomer = new Customer {Id = 3, ParentId = 1};
            _contextMock.Object.Customers.Add(secondParentCustomer);

            var parentCustomer = new Customer { Id = 2, ParentId = 3};
            _contextMock.Object.Customers.Add(parentCustomer);

            var customer = new Customer { Id = 1, ParentId = 2 };
            _contextMock.Object.Customers.Add(customer);

            var result = _validator.HasParentReferenceToSelf(customer, customer.Id);

            Assert.IsTrue(result);
        }

        private Customer GetValidCustomer(int customerId, string externalCustomerId)
        {
            var customer = GetValidCustomer();
            customer.Id = customerId;
            customer.ExternalCustomerId = externalCustomerId;

            return customer;
        }

        private Customer GetValidCustomer()
        {
            return new Customer
            {
                Name = "name",
                DistributorId = 1,
                LanguageId = 2,
            };
        }
    }
}