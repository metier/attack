﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Factories;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Exceptions;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Data.Repositories
{
    [TestFixture]
    public class OrderRepositoryTests
    {
        private int _ids;
        private IOrderRepository _orderRepository;

        private ContextMock _contextMock;
        private Mock<IValidateOrder> _orderValidatorMock;
        private Mock<IParticipantRepository> _participantRepositoryMock;

        [SetUp]
        public void Setup()
        {
            _contextMock = new ContextMock();
            _orderValidatorMock = new Mock<IValidateOrder>();
            _participantRepositoryMock = new Mock<IParticipantRepository>();

            _orderRepository = new OrderRepository(_contextMock.Object, _orderValidatorMock.Object);
        }

        [Test]
        public void GetParticipant_MustSetTextToWhateverFromOrderFactory()
        {
            var participant = CreateParticipant();

            var element = participant.ParticipantInfoElements[0];
            element.InfoText = "orderlinetext";

            var orderLine = _orderRepository.CreateProposedOrderLine(participant);

            Assert.AreEqual("firstname lastname, leadingtext: orderlinetext", orderLine.InfoText);
        }

        [Test]
        public void GetParticipant_MustSetPriceToUnitPrice()
        {
            var participant = CreateParticipant();
            participant.UnitPrice = 12345789;

            var orderLine = _orderRepository.CreateProposedOrderLine(participant);

            Assert.AreEqual(participant.UnitPrice, orderLine.Price);
        }

        [Test]
        public void GetParticipant_MustSetCurrencyCode()
        {
            var participant = CreateParticipant();
            participant.CurrencyCodeId = 1234;
            var orderLine = _orderRepository.CreateProposedOrderLine(participant);

            Assert.AreEqual(participant.CurrencyCodeId.Value, orderLine.CurrencyCodeId);
        }

        [Test]
        public void GivenProposedOrderLine_GetParticipantOrderLine_OrderLineIdMustBeZero()
        {
            var participant = CreateParticipant();
            var orderLine = _orderRepository.CreateProposedOrderLine(participant);

            Assert.AreEqual(0, orderLine.Id);       //Id = 0 for all proposed orderlines for participants
        }


        #region AddParticipantsToOrder

        //only allow addingparticipants on orders in the draft state
        [Test]
        public void AddParticipantsToOrder_GivenOrderIsDraft_ShouldReturnAdditionalParticipantOrderLines()
        {
            var participant = CreateParticipant();
            var customer = _contextMock.Object.Customers.First(c => c.Id == participant.CustomerId);
            var activity = _contextMock.Object.Activities.First(a => a.Id == participant.ActivityId);

            var participantToAdd = CreateParticipant();
            participantToAdd.CustomerId = customer.Id;

            var order = CreateOrder(customer, participant, activity, OrderStatus.Draft);

            var orderLinesCount = order.ParticipantOrderLines.Count;
            order = _orderRepository.AddParticipantsToOrder(order, new List<Participant> {participantToAdd});

            Assert.AreEqual(orderLinesCount + 1, order.ParticipantOrderLines.Count);
        }


        //Only allow adding participants not allready on current order
        [Test]
        public void AddParticipantsToOrder_GivenParticipantAlreadyExists_ShouldNotAddParticipantOrderLine()
        {
            var customer = CreateCustomer();
            var user = CreateUser(customer);
            var activity = CreateActivity();
            var participant = CreateParticipant(user, activity);

            participant.CustomerId = customer.Id;
            var order = CreateOrder(customer, participant, activity, OrderStatus.Draft);

            var orderLinesCount = order.ParticipantOrderLines.Count;
            order = _orderRepository.AddParticipantsToOrder(order, new List<Participant> { participant });

            Assert.AreEqual(orderLinesCount, order.ParticipantOrderLines.Count);
        }

        #endregion

        #region AddActivitiesToOrder

        ////Only allow adding participants from same customer
        //[Test]
        //public void AddActivitiesToOrder_GivenActivityFromDifferentCustomer_ShouldReturnException()
        //{
        //    var customer = CreateCustomer();
        //    var participant = CreateParticipant();
        //    var activity = _contextMock.Object.Activities.First(a => a.Id == participant.ActivityId);
        //    var order = CreateOrder(customer, participant, activity, OrderStatus.Draft);

        //    Assert.Throws(Is.TypeOf<ValidationException>(), () => _orderRepository.AddParticipantsToOrder(order, new List<Participant> { participant }));
        //}

        ////Only allow adding participants when in Draft mode
        //[Test]
        //[TestCase(OrderStatus.Proposed)]
        //[TestCase(OrderStatus.Transferred)]
        //[TestCase(OrderStatus.NotInvoiced)]
        //[TestCase(OrderStatus.Invoiced)]
        //[TestCase(OrderStatus.Rejected)]
        //public void AddActivitiesToOrder_GivenOrderIsNotEditable_ShouldReturnException(OrderStatus orderStatus)
        //{
        //    var participant = CreateParticipant();
        //    var customer = _contextMock.Object.Customers.First(c => c.Id == participant.CustomerId);
        //    var activity = _contextMock.Object.Activities.First(a => a.Id == participant.ActivityId);
        //    var order = CreateOrder(customer, participant, activity, orderStatus);

        //    Assert.Throws(Is.TypeOf<ValidationException>(), () => _orderRepository.AddParticipantsToOrder(order, new List<Participant> { participant }));
        //}


        ////only allow addingparticipants that is in the draft state
        //[Test]
        //[TestCase(OrderStatus.Draft)]
        //public void AddActivitiesToOrder_GivenOrderIsEditable_ShouldReturnAdditionalParticipantOrderLines(OrderStatus orderStatus)
        //{
        //    var participant = CreateParticipant();
        //    var customer = _contextMock.Object.Customers.First(c => c.Id == participant.CustomerId);
        //    var activity = _contextMock.Object.Activities.First(a => a.Id == participant.ActivityId);

        //    var participantToAdd = CreateParticipant();
        //    participantToAdd.CustomerId = customer.Id;

        //    var order = CreateOrder(customer, participant, activity, orderStatus);

        //    var orderLinesCount = order.ParticipantOrderLines.Count;
        //    order = _orderRepository.AddParticipantsToOrder(order, new List<Participant> { participantToAdd });

        //    Assert.AreEqual(orderLinesCount + 1, order.ParticipantOrderLines.Count);
        //}


        ////Only allow adding participants not allready on current order
        //[Test]
        //public void AddActivitiesToOrder_GivenOrderIsEditable_ShouldNotAddParticipantOrderLine()
        //{
        //    var customer = CreateCustomer();
        //    var user = CreateUser(customer);
        //    var activity = CreateActivity();
        //    var participant = CreateParticipant(user, activity);

        //    participant.CustomerId = customer.Id;
        //    var order = CreateOrder(customer, participant, activity, OrderStatus.Draft);

        //    var orderLinesCount = order.ParticipantOrderLines.Count;
        //    order = _orderRepository.AddParticipantsToOrder(order, new List<Participant> { participant });

        //    Assert.AreEqual(orderLinesCount, order.ParticipantOrderLines.Count);
        //}

        #endregion


        #region Supporting functions 

        private Participant CreateParticipant(User user = null, Activity activity = null)
        {
            var customer = CreateCustomer();
            customer.CustomLeadingTexts = CreateCustomLeadingTexts(customer);

            if (user == null) user = CreateUser(customer);

            if (activity == null) activity = CreateActivity();

            var participant = new Participant { Id = GetNextId(), User = user, CustomerId = customer.Id, UserId = user.Id, ActivityId = activity.Id };
            participant.ParticipantInfoElements = CreateParticipantInfoElements(participant.Id);
            MapParticipantInfoElementsToCustomLeadingTexts(participant.ParticipantInfoElements, customer.CustomLeadingTexts);

            _contextMock.Object.Participants.Add(participant);
            _contextMock.Object.Activities.Add(activity);
            _contextMock.Object.Users.Add(user);
            _participantRepositoryMock.Setup(u => u.FindById(participant.Id, false)).Returns(participant);

            return participant;
        }

        private List<ParticipantInfoElement> CreateParticipantInfoElements(int participantId)
        {
            var elements = new List<ParticipantInfoElement>
            {
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participantId, InfoText = "", LeadingTextId =  11, LeadingText = new LeadingText{Id = 11, Text = "Custom Field 1", IsVisible = true}},
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participantId, InfoText = "", LeadingTextId =  12, LeadingText = new LeadingText{Id = 12, Text = "Custom Field 2", IsVisible = true}},
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participantId, InfoText = "", LeadingTextId =  13, LeadingText = new LeadingText{Id = 13, Text = "Employee Id", IsVisible = true}},
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participantId, InfoText = "", LeadingTextId =  14, LeadingText = new LeadingText{Id = 14, Text = "Department", IsVisible = true}},
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participantId, InfoText = "", LeadingTextId =  15, LeadingText = new LeadingText{Id = 15, Text = "Position", IsVisible = true}},
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participantId, InfoText = "", LeadingTextId =  16, LeadingText = new LeadingText{Id = 16, Text = "Supervisor", IsVisible = true}},
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participantId, InfoText = "", LeadingTextId =  17, LeadingText = new LeadingText{Id = 17, Text = "Confirmation Email", IsVisible = true}}
            };

            return elements;
        }

        private List<CustomLeadingText> CreateCustomLeadingTexts(Customer customer)
        {
            var list = new List<CustomLeadingText>
            {
                new CustomLeadingText{Customer = customer,CustomerId = customer.Id, LeadingTextId = 11, IsVisibleInvoiceLine = true, IsVisible = true, Text = "leadingtext"},
                new CustomLeadingText{Customer = customer,CustomerId = customer.Id, LeadingTextId = 12, IsVisibleInvoiceLine = true, IsVisible = true, Text = "leadingtext"},
                new CustomLeadingText{Customer = customer,CustomerId = customer.Id, LeadingTextId = 13, IsVisibleInvoiceLine = true, IsVisible = true, Text = "leadingtext"},
                new CustomLeadingText{Customer = customer,CustomerId = customer.Id, LeadingTextId = 14, IsVisibleInvoiceLine = true, IsVisible = true, Text = "leadingtext"},
                new CustomLeadingText{Customer = customer,CustomerId = customer.Id, LeadingTextId = 15, IsVisibleInvoiceLine = true, IsVisible = true, Text = "leadingtext"},
                new CustomLeadingText{Customer = customer,CustomerId = customer.Id, LeadingTextId = 16, IsVisibleInvoiceLine = true, IsVisible = true, Text = "leadingtext"},
                new CustomLeadingText{Customer = customer,CustomerId = customer.Id, LeadingTextId = 17, IsVisibleInvoiceLine = true, IsVisible = true, Text = "leadingtext"}

            };

            return list;
        }

        private void MapParticipantInfoElementsToCustomLeadingTexts(List<ParticipantInfoElement> participantInfoElements, List<CustomLeadingText> customLeadingTexts)
        {
            foreach (var element in participantInfoElements)
            {
                var customLeadingText = customLeadingTexts.FirstOrDefault(clt => clt.LeadingTextId == element.LeadingTextId);

                if (customLeadingText != null)
                    customLeadingText.LeadingText = element.LeadingText;

                element.LeadingText.CustomLeadingTexts = customLeadingTexts.FindAll(clt => clt.LeadingTextId == element.LeadingTextId);
            }
        }

        private User CreateUser(Customer customer)
        {
            var user = new User { Id = GetNextId(), FirstName = "firstname", LastName = "lastname", CustomerId = customer.Id, Customer = customer};
            _contextMock.Object.Users.Add(user);

            return user;
        }

        private Customer CreateCustomer()
        {
            var customer = new Customer {Id = GetNextId()};
            _contextMock.Object.Customers.Add(customer);
            return customer;
        }

        private Activity CreateActivity(CustomerArticle customerArticle = null)
        {
            if (customerArticle == null) customerArticle = CreateCustomerArticle();
            var activity = new Activity { Id = GetNextId(), CustomerArticleId = customerArticle.Id };
            CreateSearchableActivity(activity, customerArticle);

            _contextMock.Object.Activities.Add(activity);

            return activity;
        }

        private void CreateSearchableActivity(Activity activity, CustomerArticle customerArticle)
        {
            if (activity.CustomerArticleId != customerArticle.Id)
                throw new Exception("Mismatching id's between the CustomerArticleId of the activity and the customerArticle.id.");

            var searchableActivity = new SearchableActivityWithActivitySet
            {
                ActivityId = activity.Id,
                CustomerArticleId = customerArticle.Id,
                ArticlePath = customerArticle.ArticlePath
            };

            _contextMock.Object.SearchableActivities.Add(searchableActivity);
        }


        private CustomerArticle CreateCustomerArticle()
        {
            var customerArticle = new CustomerArticle { Id = GetNextId() };
            _contextMock.Object.CustomerArticles.Add(customerArticle);
            return customerArticle;
        }

        private Order CreateOrder(Customer customer, Participant participant, Activity activity, OrderStatus orderStatus)
        {
            var order = new Order
            {
                Id = GetNextId(),
                Customer = customer,
                CustomerId = customer.Id,
                IsCreditNote = false,
                IsFullCreditNote = false,
                Status = orderStatus,
                ParticipantOrderLines = new List<ParticipantOrderLine>(),
                ActivityOrderLines = new List<ActivityOrderLine>()
            };


            order.ParticipantOrderLines.Add(CreateParticipantOrderLine(participant, activity, order));
            _contextMock.Object.Orders.Add(order);

            return order;
        }

        private ParticipantOrderLine CreateParticipantOrderLine(Participant participant, Activity activity, Order order)
        {
            var participantOrderLine = new ParticipantOrderLine
            {
                Id = GetNextId(),
                ActivityId = participant.ActivityId,
                ActivityName = activity.Name,
                CurrencyCodeId = participant.CurrencyCodeId,
                InfoText = participant.GetInfoTexts(),
                OnlinePaymentReference = participant.OnlinePaymentReference,
                Participant = participant,
                Price = participant.UnitPrice,
                Order = order
            };

            _contextMock.Object.ParticipantOrderLines.Add(participantOrderLine);
            return participantOrderLine;
        }



        private int GetNextId()
        {
            return ++_ids;
        }

        #endregion

    }
}
