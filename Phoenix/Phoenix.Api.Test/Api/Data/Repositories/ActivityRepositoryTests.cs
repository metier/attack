﻿using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Api.Messaging;
using Metier.Phoenix.Core.Data.Entities;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Data.Repositories
{
    [TestFixture]
    public class ActivityRepositoryTests
    {
        private ActivityRepository _activityRepository;

        [SetUp]
        public void Setup()
        {
            var context = new ContextMock();
            var validateActivityMock = new Mock<IValidateActivity>();
            var eventAggregatorMock = new Mock<IEventAggregator>();

            _activityRepository = new ActivityRepository(context.Object, validateActivityMock.Object, eventAggregatorMock.Object);
        }


        [Test]
        public void RemoveDuplicateRcoConnections_GivenDuplicateRcos_RemovesDuplicates()
        {
            var activity = new Activity
            {
                ActivityExamCourses =
                    new List<ActivityExamCourse>
                    {
                        new ActivityExamCourse {ActivityId = 1, CourseRcoId = 1},
                        new ActivityExamCourse {ActivityId = 1, CourseRcoId =2},
                        new ActivityExamCourse {ActivityId = 1, CourseRcoId = 1}
                    }
            };

            
            _activityRepository.RemoveDuplicateRcoConnections(activity);

            Assert.AreEqual(2, activity.ActivityExamCourses.Count);
            Assert.AreEqual(1, activity.ActivityExamCourses.Count(e => e.CourseRcoId == 1));
            Assert.AreEqual(1, activity.ActivityExamCourses.Count(e => e.CourseRcoId == 2));
        }

    }
}