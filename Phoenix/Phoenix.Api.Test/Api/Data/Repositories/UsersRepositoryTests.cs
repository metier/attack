﻿using System.Linq;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Exceptions;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Data.Repositories
{
    public class UsersRepositoryTests
    {
        private UsersRepository _usersRepository;
        private ContextMock _context;
        private User _user1;
        private User _user2;

        [SetUp]
        public void Setup()
        {
            _context = new ContextMock();
            _usersRepository = new UsersRepository(_context.Object);

            _user1 = new User {Id = 1};
            _user2 = new User {Id = 2};

            _context.Object.Users.Add(_user1);
            _context.Object.Users.Add(_user2);

            _context.Object.MetierContactPersons.Add(new MetierContactPerson {ContactType = MetierContactType.AccountManager, IsDeleted = false, UserId = _user1.Id, Id = 1});
        }

        [Test]
        public void MergeUsers_GivenBothUsersAreTheSame_ShouldReturnValidationException()
        {
            Assert.Throws(Is.TypeOf<ValidationException>(), () => _usersRepository.MergeUsers(_user1.Id, _user1.Id));
        }

        [Test]
        public void MergeUsers_GivenBothUsersEnrolledToTheSameActivity_ShouldReturnValidationException()
        {
            _context.Object.Participants.Add(new Participant{Id = 1, UserId = _user1.Id, ActivityId = 99});
            _context.Object.Participants.Add(new Participant{Id = 2, UserId = _user2.Id, ActivityId = 99});

            Assert.Throws(Is.TypeOf<ValidationException>(), () => _usersRepository.MergeUsers(_user1.Id, _user2.Id));
        }

        [Test]
        public void MergeUsers_GivenBothUsersEnrolledInTheSameElearningCourse_ShouldReturnValidationException()
        {
            _context.Object.Participants.Add(new Participant{Id = 1, UserId = _user1.Id, ActivityId = 1, IsDeleted = false });
            _context.Object.Participants.Add(new Participant{Id = 2, UserId = _user2.Id, ActivityId = 99, IsDeleted = false });

            _context.Object.Activities.Add(new Activity {Id = 1, RcoCourseId = 100});
            _context.Object.Activities.Add(new Activity {Id = 99, RcoCourseId = 100});

            Assert.Throws(Is.TypeOf<ValidationException>(), () => _usersRepository.MergeUsers(_user1.Id, _user2.Id));
        }

        [Test]
        public void MergeUsers_GivenBothUsersEnrolledInTheSameElearningCourseWhereOneIsDeleted_ShouldNotReturnValidationException()
        {
            _context.Object.Participants.Add(new Participant{Id = 1, UserId = _user1.Id, ActivityId = 1, ExamGraderUserId = 5, IsDeleted = true });
            _context.Object.Participants.Add(new Participant{Id = 2, UserId = _user2.Id, ActivityId = 99, ExamGraderUserId = 6, IsDeleted = false });

            _context.Object.Activities.Add(new Activity {Id = 1, RcoCourseId = 100});
            _context.Object.Activities.Add(new Activity {Id = 99, RcoCourseId = 100});

            Assert.DoesNotThrow(() => _usersRepository.MergeUsers(_user1.Id, _user2.Id));
            Assert.IsNull(_context.Object.Users.FirstOrDefault(u => u.Id == _user2.Id));
        }

        [Test]
        public void MergeUsers_GivenBothUsersEnrolledInTheSameActivitySet_ShouldReturnValidationException()
        {
            _context.Object.Enrollments.Add(new Enrollment { Id = 1, UserId = _user1.Id, ActivitySetId = 99 });
            _context.Object.Enrollments.Add(new Enrollment { Id = 2, UserId = _user2.Id, ActivitySetId = 99 });

            Assert.Throws(Is.TypeOf<ValidationException>(), () => _usersRepository.MergeUsers(1, 2));
        }

        [Test]
        public void MergeUsers_GivenBothUsersHaveProgressOnTheSameRcos_ShouldReturnValidationException()
        {
            _context.Object.ScormPerformances.Add(new ScormPerformance {Id = 1, RcoId = 111, UserId = _user1.Id});
            _context.Object.ScormPerformances.Add(new ScormPerformance {Id = 2, RcoId = 111, UserId = _user2.Id});

            Assert.Throws(Is.TypeOf<ValidationException>(), () => _usersRepository.MergeUsers(1, 2));
        }

    }
}
