﻿using Metier.Phoenix.Api.Security;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Security
{
    [TestFixture]
    public class AllowRolesAttributeTests
    {
        [Test]
        public void Ctor_MustParseSingleRole()
        {
            var attr = new AllowRolesAttribute("role");
            Assert.AreEqual(1, attr.AuthorizeRoles.Length);
            Assert.AreEqual("role", attr.AuthorizeRoles[0]);
        }

        [Test]
        public void Ctor_MustParseMultipleSingleRoles()
        {
            var attr = new AllowRolesAttribute("role1", "role2");
            Assert.AreEqual(2, attr.AuthorizeRoles.Length);
            Assert.AreEqual("role1", attr.AuthorizeRoles[0]);
            Assert.AreEqual("role2", attr.AuthorizeRoles[1]);
        }

        [Test]
        public void Ctor_MustParseCombinationOfMultipleAndSingleRoles()
        {
            var attr = new AllowRolesAttribute("role1,role2", "role3");
            Assert.AreEqual(3, attr.AuthorizeRoles.Length);
            Assert.AreEqual("role1", attr.AuthorizeRoles[0]);
            Assert.AreEqual("role2", attr.AuthorizeRoles[1]);
            Assert.AreEqual("role3", attr.AuthorizeRoles[2]);
        }
    }
}
