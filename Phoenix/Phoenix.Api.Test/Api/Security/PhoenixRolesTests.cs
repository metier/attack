﻿using Metier.Phoenix.Api.Security;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Security
{
    [TestFixture]
    public class PhoenixRolesTests
    {
        [Test]
        [TestCase(PhoenixRoles.LearningPortalUser, "LearningPortalUser")]
        [TestCase(PhoenixRoles.LearningPortalSuperUser, "LearningPortalSuperUser")]
        [TestCase(PhoenixRoles.LearningPortal, "LearningPortalUser,LearningPortalSuperUser")]
        [TestCase(PhoenixRoles.BehindAdmin, "BehindAdmin")]
        [TestCase(PhoenixRoles.BehindContentAdmin, "BehindContentAdmin")]
        [TestCase(PhoenixRoles.BehindSuperAdmin, "BehindSuperAdmin" )]
        [TestCase(PhoenixRoles.Behind, "BehindAdmin,BehindContentAdmin,BehindSuperAdmin")]
        [TestCase(PhoenixRoles.PhoenixUser, "PhoenixUser")]
        [TestCase(PhoenixRoles.Phoenix, "PhoenixUser")]
        [TestCase(PhoenixRoles.Examiner, "Examiner")]
        [TestCase(PhoenixRoles.LearningPortalTokenUser, "LearningPortalTokenUser")]
        [TestCase(PhoenixRoles.EzTokenUser, "EzTokenUser")]
        [TestCase(PhoenixRoles.All, "PhoenixUser,LearningPortalUser,LearningPortalSuperUser,BehindAdmin,BehindContentAdmin,BehindSuperAdmin,Examiner,LearningPortalTokenUser,EzTokenUser")]
        public void LearningPortalRoles(string roles, string expected)
        {
            Assert.AreEqual(expected, roles);
        }
    }
}