﻿using System;
using System.Collections.Generic;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Api.Messaging.Handlers.UserEnrolled;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Messaging.Handlers.UserEnrolled
{
    [TestFixture]
    public class CreateOrderForOnlinePaymentsTests
    {
        private CreateOrderForOnlinePayments _createOrderForOnlinePayments;
        private ContextMock _mockContext;
        private Mock<IOrdersProvider> _orderProvider;

        [SetUp]
        public void SetUp()
        {
            _mockContext = new ContextMock();
            _orderProvider = new Mock<IOrdersProvider>();

            _createOrderForOnlinePayments = new CreateOrderForOnlinePayments(_mockContext.Object, _orderProvider.Object);
        }

        [Test]
        public void HandleEvent_GivenParticipantIdWithPaymentReference_ShouldInvoiceCorrespondingGroupByKey()
        {
            const int participantId = 123;
            const string groupByKey = "someGroupByKey";

            var participant = new Participant{Id = participantId};
            _mockContext.Object.Participants.Add(participant);
            _mockContext.Object.ProposedOrderLines.Add(new ProposedOrderLine {ParticipantId = participantId, GroupByKey = groupByKey});

            participant.OnlinePaymentReference = "I have a payment reference";
            _createOrderForOnlinePayments.Handle(new UserEnrolledEvent{ParticipantIds = new List<int>{participantId}});

            _orderProvider.Verify(op => op.InvoiceByGroupByKey(It.Is<string>(g => g.Equals(groupByKey)), It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void HandleEvent_GivenParticipantIdWithoutPaymentReference_ShouldNotInvoice()
        {
            const int participantId = 123;
            const string groupByKey = "someGroupByKey";

            var participant = new Participant { Id = participantId };
            _mockContext.Object.Participants.Add(participant);
            _mockContext.Object.ProposedOrderLines.Add(new ProposedOrderLine { ParticipantId = participantId, GroupByKey = groupByKey });

            participant.OnlinePaymentReference = string.Empty;

            _createOrderForOnlinePayments.Handle(new UserEnrolledEvent { ParticipantIds = new List<int> { participantId } });

            _orderProvider.Verify(op => op.InvoiceByGroupByKey(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        [Test]
        public void HandleEvent_GivenParticipantIdsWithDifferentPaymentReference_ShouldCreateOrderForEveryCorrespondingGroupByKey()
        {
            const int participantId = 123;
            const int participantId2 = 234;
            const int participantId3 = 345;

            const string groupByKey = "someGroupByKey";
            const string groupByKey2 = "someOtherGroupByKey";

            var participant = new Participant { Id = participantId };
            var participant2 = new Participant { Id = participantId2 };
            var participant3 = new Participant { Id = participantId3 };
            _mockContext.Object.Participants.Add(participant);
            _mockContext.Object.Participants.Add(participant2);
            _mockContext.Object.Participants.Add(participant3);
            _mockContext.Object.ProposedOrderLines.Add(new ProposedOrderLine { ParticipantId = participantId, GroupByKey = groupByKey });
            _mockContext.Object.ProposedOrderLines.Add(new ProposedOrderLine { ParticipantId = participantId2, GroupByKey = groupByKey });
            _mockContext.Object.ProposedOrderLines.Add(new ProposedOrderLine { ParticipantId = participantId3, GroupByKey = groupByKey2 });

            participant.OnlinePaymentReference = participant2.OnlinePaymentReference = participant3.OnlinePaymentReference = "The payment reference";
            _createOrderForOnlinePayments.Handle(new UserEnrolledEvent { ParticipantIds = new List<int> { participantId, participantId2, participantId3 } });

            _orderProvider.Verify(op => op.InvoiceByGroupByKey(It.Is<string>(g => g.Equals(groupByKey)), It.IsAny<string>()), Times.Once);
            _orderProvider.Verify(op => op.InvoiceByGroupByKey(It.Is<string>(g => g.Equals(groupByKey2)), It.IsAny<string>()), Times.Once);
        }
    }
}