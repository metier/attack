﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Api.Messaging.Handlers.ParticipantCreated;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Templates.EnrollmentConfirmation;
using Metier.Phoenix.Mail.Templates.EnrollmentConfirmationCustomer;
using Metier.Phoenix.Mail.Templates.EnrollmentNotQualifiedForExam;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Messaging.Handlers.ParticipantCreated
{
    [TestFixture]
    public class SendEnrollmentConfirmationTests
    {
        private SendEnrollmentConfirmation _sendEnrollmentConfirmation;
        private ContextMock _contextMock;
        private Mock<IEnrollmentConfirmationFactory> _enrollmentConfirmationFactoryMock;
        private Mock<IAutoMailer> _autoMailerMock;
        private Mock<IEnrollmentConfirmationCustomerFactory> _enrollmentConfirmationCustomerFatoryMock;
        private Mock<IEnrollmentNotQualifiedForExamFactory> _enrollmentNotQualifiedForExamFactoryMock;
            
        [SetUp]
        public void Setup()
        {
            _contextMock = new ContextMock();
            _enrollmentConfirmationFactoryMock = new Mock<IEnrollmentConfirmationFactory>();
            _enrollmentNotQualifiedForExamFactoryMock = new Mock<IEnrollmentNotQualifiedForExamFactory>();
            _enrollmentConfirmationCustomerFatoryMock = new Mock<IEnrollmentConfirmationCustomerFactory>();
            _autoMailerMock = new Mock<IAutoMailer>();

            _enrollmentConfirmationCustomerFatoryMock.Setup(e => e.CreateMessages(It.IsAny<Participant>()))
                .Returns(new List<AutoMailMessage> {new AutoMailMessage()});
            _enrollmentNotQualifiedForExamFactoryMock.Setup(e => e.CreateMessage(It.IsAny<Participant>()))
                .Returns(new AutoMailMessage());

            _sendEnrollmentConfirmation = new SendEnrollmentConfirmation(   
                _contextMock.Object,
                _enrollmentConfirmationFactoryMock.Object, 
                _enrollmentConfirmationCustomerFatoryMock.Object,
                _autoMailerMock.Object);
        }

        [Test]
        public void Handle_GivenParticipantForActivityWithPastEnrollmentConfirmationDate_ShouldSendEnrollmentConfirmation()
        {
            var participant = SetupParticipant(DateTime.Now - TimeSpan.FromDays(2), true);
            _sendEnrollmentConfirmation.Handle(new ParticipantCreatedEvent{ParticipantId = participant.Id});

            _enrollmentConfirmationFactoryMock.Verify(e => e.CreateMessage(It.Is<Participant>(p => p.Id == participant.Id)), Times.Once);
        }
        
        [Test]
        public void Handle_GivenParticipantForActivityWithPastEnrollmentConfirmationDateAndFalseActivityFlag_ShouldSendNotEnrollmentConfirmation()
        {
            var participant = SetupParticipant(DateTime.Now - TimeSpan.FromDays(2), false);
            _sendEnrollmentConfirmation.Handle(new ParticipantCreatedEvent{ParticipantId = participant.Id});

            _enrollmentConfirmationFactoryMock.Verify(e => e.CreateMessage(It.Is<Participant>(p => p.Id == participant.Id)), Times.Never);
        }

        [Test]
        public void Handle_GivenParticipantForActivityWithCurrentDateAsEnrollmentConfirmationDate_ShouldSendEnrollmentConfirmation()
        {
            var participant = SetupParticipant(DateTime.Now.Date, true);
            _sendEnrollmentConfirmation.Handle(new ParticipantCreatedEvent { ParticipantId = participant.Id });

            _enrollmentConfirmationFactoryMock.Verify(e => e.CreateMessage(It.Is<Participant>(p => p.Id == participant.Id)), Times.Once);
        }

        [Test]
        public void Handle_GivenParticipantForActivityWithFutureEnrollmentConfirmationDate_ShouldNotSendEnrollmentConfirmation()
        {
            var participant = SetupParticipant(DateTime.Now + TimeSpan.FromDays(1), true);
            _sendEnrollmentConfirmation.Handle(new ParticipantCreatedEvent { ParticipantId = participant.Id });

            _enrollmentConfirmationFactoryMock.Verify(e => e.CreateMessage(It.IsAny<Participant>()), Times.Never);
        }

        [Test]
        public void Handle_GivenParticipantEnrolledAndDefaultEmailConfirmationSet_ShouldSendEnrollmentConfirmationToCustomer()
        {
            var participant = SetupParticipant(DateTime.Now + TimeSpan.FromDays(1), true);

            _sendEnrollmentConfirmation.Handle(new ParticipantCreatedEvent { ParticipantId = participant.Id });

            _enrollmentConfirmationCustomerFatoryMock.Verify(e => e.CreateMessages(It.IsAny<Participant>()), Times.Once);
            _autoMailerMock.Verify(a => a.Send(It.IsAny<AutoMailMessage>()), Times.AtLeastOnce);
        }

        [Test]
        public void Handle_GivenParticipantForExpiredUser_ShouldNotSendEnrollmentConfirmation()
        {
            var participant = SetupParticipant(DateTime.Now - TimeSpan.FromDays(2), true);
            var user = _contextMock.Object.Users.First(u => u.Id == participant.UserId);
            user.ExpiryDate = DateTime.Now - TimeSpan.FromDays(2);
            _sendEnrollmentConfirmation.Handle(new ParticipantCreatedEvent { ParticipantId = participant.Id });

            _enrollmentConfirmationFactoryMock.Verify(e => e.CreateMessage(It.Is<Participant>(p => p.Id == participant.Id)), Times.Never);
        }

        [Test]
        public void Handle_GivenParticipantForNonExpiredUser_ShouldNotSendEnrollmentConfirmation()
        {
            var participant = SetupParticipant(DateTime.Now - TimeSpan.FromDays(2), true);
            var user = _contextMock.Object.Users.First(u => u.Id == participant.UserId);
            user.ExpiryDate = DateTime.Now + TimeSpan.FromDays(2);
            _sendEnrollmentConfirmation.Handle(new ParticipantCreatedEvent { ParticipantId = participant.Id });

            _enrollmentConfirmationFactoryMock.Verify(e => e.CreateMessage(It.Is<Participant>(p => p.Id == participant.Id)), Times.Once);
        }

        private Participant SetupParticipant(DateTime enrollmentConfirmationDate, bool shouldSendEnrollmentConfirmation)
        {
            const int activityId = 1;
            const int participantId = 2;
            const int customerArticleId = 3;
            const int articleOriginalId = 4;
            const int articleCopyId = 5;
            const int customerId = 2;
            
            var activity = new Activity
            {
                Id = activityId,
                CustomerArticleId = customerArticleId,
                EnrollmentConfirmationDate = enrollmentConfirmationDate,
                SendEnrollmentConfirmationAutomail = shouldSendEnrollmentConfirmation
            };
            _contextMock.Object.Activities.Add(activity);

            var customerArticle = new CustomerArticle()
            {
                Id = customerArticleId,
                ArticleCopyId = articleCopyId,
                ArticleOriginalId = articleOriginalId,
                CurrencyCodeId = 262,        //Norwegian NOK
                UnitPrice = 1,
                FixedPrice = 0,
                CustomerId = customerId
            };
            _contextMock.Object.CustomerArticles.Add(customerArticle);

            var article = new ElearningArticle
            {
                Id = articleCopyId,
                ProductId = 1,
                LanguageId = 45,
                IsDeleted = false,
                ArticleTypeId = 1,
                StatusCodeId = 1
            };
            _contextMock.Object.Articles.Add(article);

            var participant = new Participant
            {
                Id = participantId,
                ActivityId = activityId,
                UserId = 1,
                CustomerId = customerId
            };
            _contextMock.Object.Participants.Add(participant);

            var customer = new Customer
            {
                Id = participant.CustomerId,
                ParticipantInfoDefinition = new ParticipantInfoDefinition
                {
                    EmailConfirmationAddress = "confirmationaddress@test.com"
                },
                CustomLeadingTexts = new List<CustomLeadingText>
                {
                    new CustomLeadingText
                    {
                        Id = 1,
                        LeadingTextId = LeadingTextIds.ConfirmationEmail,
                        IsVisible = true
                    }
                },
                AutomailDefinition = new AutomailDefinition
                {
                    IsEnrollmentNotification = true
                }
            };
            _contextMock.Object.Customers.Add(customer);

            var user = new User
            {
                Id = participant.UserId,
                UserInfoElements = new List<UserInfoElement>
                {
                    new UserInfoElement
                    {
                        LeadingTextId = 11,
                        LeadingText = customer.CustomLeadingTexts.First().LeadingText,
                        InfoText = "user.confirmation.address@test.com"
                    }
                }
            };
            _contextMock.Object.Users.Add(user);

            return participant;
        }
    }
}