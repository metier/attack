﻿using System.Linq;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Messaging;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Api.Messaging.Handlers.ParticipantCreated;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Messaging.Handlers.ParticipantCreated
{
    [TestFixture]
    public class CreateCoursePerformanceTests
    {
        private ContextMock _contextMock;
        private Mock<IEventAggregator> _eventAggregatorMock;

        private CreateCoursePerformance _createCoursePerformance;

        [SetUp]
        public void Setup()
        {
            _contextMock = new ContextMock();
            _eventAggregatorMock = new Mock<IEventAggregator>();
            _createCoursePerformance = new CreateCoursePerformance(_contextMock.Object, _eventAggregatorMock.Object);
        }

        [Test]
        public void Handle_GivenParticipantWithActivityWithRcoCourseId_ShouldCreatePerformanceIfNotExists()
        {
            var activity = new Activity { Id = 1, RcoCourseId = 3 };
            var participant = new Participant { Id = 2, ActivityId = activity.Id, UserId = 4};

            _contextMock.Object.Participants.Add(participant);
            _contextMock.Object.Activities.Add(activity);

            _createCoursePerformance.Handle(new ParticipantCreatedEvent { ParticipantId = participant.Id });

            var performance = _contextMock.Object.ScormPerformances.First(p => p.UserId == participant.UserId && p.RcoId == activity.RcoCourseId);

            Assert.IsNotNull(performance);
            Assert.AreEqual(activity.RcoCourseId.GetValueOrDefault(), performance.RcoId);
            Assert.AreEqual(participant.UserId, performance.UserId);
            Assert.AreEqual(ScormLessonStatus.NotAttempted, performance.Status);
            _contextMock.Verify(c => c.SaveChanges(), Times.Once);
            _eventAggregatorMock.Verify(e => e.Publish(It.IsAny<PerformanceStatusChangedEvent>()), Times.Once);
        }

        [Test]
        public void Handle_GivenParticipantWithActivityWithRcoCourseIdWithExistingPerformance_ShouldNotCreatePerformance()
        {
            var activity = new Activity { Id = 1, RcoCourseId = 3 };
            var participant = new Participant { Id = 2, ActivityId = activity.Id, UserId = 4};
            var performance = new ScormPerformance { Id = 5, UserId = participant.UserId, RcoId = activity.RcoCourseId.GetValueOrDefault() };

            _contextMock.Object.Participants.Add(participant);
            _contextMock.Object.Activities.Add(activity);
            _contextMock.Object.ScormPerformances.Add(performance);

            _createCoursePerformance.Handle(new ParticipantCreatedEvent { ParticipantId = participant.Id });

            Assert.AreEqual(1, _contextMock.Object.ScormPerformances.Count());
            _contextMock.Verify(c => c.SaveChanges(), Times.Never);
            _eventAggregatorMock.Verify(e => e.Publish(It.IsAny<PerformanceStatusChangedEvent>()), Times.Never);
        }
    }
}