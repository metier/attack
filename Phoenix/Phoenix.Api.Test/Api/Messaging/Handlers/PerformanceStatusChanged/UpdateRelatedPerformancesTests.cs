﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Messaging;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Api.Messaging.Handlers.PerformanceStatusChanged;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Messaging.Handlers.PerformanceStatusChanged
{
    [TestFixture]
    public class UpdateRelatedPerformancesTests
    {
        private ContextMock _contextMock;
        private Mock<IEventAggregator> _eventAggregatorMock;
        private Mock<IPerformanceProvider> _performanceProviderMock;
        private Mock<IParticipantsProvider> _participantProviderMock;
        private Mock<IRcoProvider> _rcoProviderMock;
		private Mock<IScormProvider> _scormProviderMock;
		
		private UpdateRelatedPerformances _updateRelatedPerformances;
        
        [SetUp]
        public void Setup()
        {
            _contextMock = new ContextMock();
            _eventAggregatorMock = new Mock<IEventAggregator>();
            _performanceProviderMock = new Mock<IPerformanceProvider>();
            _participantProviderMock = new Mock<IParticipantsProvider>();
            _rcoProviderMock = new Mock<IRcoProvider>();
			_scormProviderMock = new Mock<IScormProvider>();

			_updateRelatedPerformances = new UpdateRelatedPerformances(_contextMock.Object, _eventAggregatorMock.Object, _performanceProviderMock.Object, _participantProviderMock.Object, _rcoProviderMock.Object, _scormProviderMock.Object );
        }

        [Test]
        public void Handle_GivenCoursePerformance_MustUpdateStatus()
        {
            _contextMock.Object.Rcos.Add(new Rco { Context = "course", Id = 1 });
            _contextMock.Object.ScormPerformances.Add(new ScormPerformance { Id = 2, RcoId = 1, UserId = 1234});

            _updateRelatedPerformances.Handle(new PerformanceStatusChangedEvent { PerformanceId = 2 });

            _participantProviderMock.Verify(p => p.UpdateStatus(1234, 1), Times.Once);
            _contextMock.Verify(c => c.SaveChanges(), Times.Never);
        }

        [Test]
        [TestCase(ScormLessonStatus.Complete, ScormLessonStatus.Incomplete)]
        [TestCase(ScormLessonStatus.Incomplete, ScormLessonStatus.Incomplete)]
        [TestCase(ScormLessonStatus.NotAttempted, ScormLessonStatus.NotAttempted)]
        public void Handle_GivenLesson_MustUpdateCoursePerformance(string performanceStaus, string expectedCoursePerformanceStatus)
        {
            const int rcoId = 1;
            const int rco2Id = 2;
            const int courseRcoId = 20;
            const int userId = 10;

            var lessonRco = new Rco { Context = "lesson", Id = rcoId, IsFinalTest = false };
            var lesson2Rco = new Rco {Context = "lesson", Id = rco2Id, IsFinalTest = false};
            var courseRco = new Rco { Context = "course", Id = courseRcoId };
            _contextMock.Object.Rcos.Add(lessonRco);
            _contextMock.Object.Rcos.Add(lesson2Rco);
            _contextMock.Object.Rcos.Add(courseRco);

            _rcoProviderMock.Setup(r => r.GetTopParent(rcoId)).Returns(_contextMock.Object.Rcos.First(r => r.Id == courseRcoId));

            _contextMock.Object.ScormPerformances.Add(new ScormPerformance { Id = 2, UserId = userId, RcoId = rcoId, Status = performanceStaus });
            _contextMock.Object.ScormPerformances.Add(new ScormPerformance { Id = 2, UserId = userId, RcoId = rco2Id, Status = ScormLessonStatus.NotAttempted });
            _contextMock.Object.ScormPerformances.Add(new ScormPerformance { Id = 3, UserId = userId, RcoId = courseRcoId, Status = ScormLessonStatus.NotAttempted});
            _performanceProviderMock.Setup(p => p.GetPerformancesForAllLessons(userId, rcoId)).Returns(_contextMock.Object.ScormPerformances);
            //_performanceProviderMock.Setup(p => p.GetPerformancesForAllLessons(userId, rcoId)).Returns(_contextMock.Object.ScormPerformances.First(s => s.UserId == userId));

            _updateRelatedPerformances.Handle(new PerformanceStatusChangedEvent { PerformanceId = 2 });

            var coursePerformance = _contextMock.Object.ScormPerformances.First(p => p.RcoId == courseRcoId);

            Assert.AreEqual(expectedCoursePerformanceStatus, coursePerformance.Status);
        }


        [Test]
        [TestCase(ScormLessonStatus.Complete, ScormLessonStatus.Complete)]
        [TestCase(ScormLessonStatus.Incomplete, ScormLessonStatus.Incomplete)]
        [TestCase(ScormLessonStatus.NotAttempted, ScormLessonStatus.NotAttempted)]
        public void Handle_GivenFinalTest_MustUpdateCoursePerformance(string performanceStaus, string expectedCoursePerformanceStatus)
        {
            const int rcoId = 1;
            const int courseRcoId = 2;

            var lessonRco = new Rco { Context = "lesson", Id = rcoId, IsFinalTest = true };
            var courseRco = new Rco { Context = "course", Id = courseRcoId };
            _contextMock.Object.Rcos.Add(lessonRco);
            _contextMock.Object.Rcos.Add(courseRco);

            _rcoProviderMock.Setup(r => r.GetTopParent(rcoId)).Returns(_contextMock.Object.Rcos.First(r => r.Id == courseRcoId));

            _contextMock.Object.ScormPerformances.Add(new ScormPerformance { Id = 2, RcoId = rcoId, Status = performanceStaus});
            _contextMock.Object.ScormPerformances.Add(new ScormPerformance { Id = 3, RcoId = courseRcoId });

            _updateRelatedPerformances.Handle(new PerformanceStatusChangedEvent { PerformanceId = 2 });

            var coursePerformance = _contextMock.Object.ScormPerformances.First(p => p.RcoId == courseRcoId);

            Assert.AreEqual(expectedCoursePerformanceStatus, coursePerformance.Status);
        }

        [Test]
        public void Handle_GivenCompletedFinalTest_MustUpdateCoursePerformance()
        {
            const int rcoId = 1;
            const int courseRcoId = 2;

            var lessonRco = new Rco { Context = "lesson", Id = rcoId, IsFinalTest = true };
            var courseRco = new Rco { Context = "course", Id = courseRcoId };
            
            _contextMock.Object.Rcos.Add(lessonRco);
            _contextMock.Object.Rcos.Add(courseRco);

            _rcoProviderMock.Setup(r => r.GetTopParent(rcoId)).Returns(courseRco);

            _contextMock.Object.ScormPerformances.Add(new ScormPerformance { Id = 2, RcoId = rcoId, Status = ScormLessonStatus.Complete});
            _contextMock.Object.ScormPerformances.Add(new ScormPerformance { Id = 3, RcoId = courseRcoId });

            _updateRelatedPerformances.Handle(new PerformanceStatusChangedEvent { PerformanceId = 2 });

            var coursePerformance = _contextMock.Object.ScormPerformances.First(p => p.RcoId == courseRcoId);

            Assert.GreaterOrEqual(DateTime.UtcNow, coursePerformance.CompletedDate);
            Assert.GreaterOrEqual(0, coursePerformance.TimeUsedInSeconds);
        }        
        
        [Test]
        public void Handle_GivenCompleteFinalTest_MustCompleteRelatedLessons()
        {
            const int rcoId = 1;
            const int rcoIdRelated = 2;
            const int courseRcoId = 3;

            var lessonRco = new Rco { Context = "lesson", Id = rcoId, IsFinalTest = true };
            var relatedLessonRco = new Rco { Context = "lesson", Id = rcoIdRelated };
            var courseRco = new Rco { Context = "course", Id = courseRcoId };
            
            _contextMock.Object.Rcos.Add(lessonRco);
            _contextMock.Object.Rcos.Add(courseRco);

            _rcoProviderMock.Setup(r => r.GetTopParent(rcoId)).Returns(courseRco);
            _rcoProviderMock.Setup(r => r.GetCourseRcos(rcoId, false)).Returns(new List<Rco> { lessonRco, relatedLessonRco });

            _contextMock.Object.ScormPerformances.Add(new ScormPerformance { Id = 2, RcoId = rcoId, Status = ScormLessonStatus.Complete});
            _contextMock.Object.ScormPerformances.Add(new ScormPerformance { Id = 3, RcoId = courseRcoId });
            _contextMock.Object.ScormPerformances.Add(new ScormPerformance { Id = 4, RcoId = rcoIdRelated });

            _updateRelatedPerformances.Handle(new PerformanceStatusChangedEvent { PerformanceId = 2 });

            var relatedPerformance = _contextMock.Object.ScormPerformances.First(p => p.RcoId == rcoIdRelated);

            Assert.AreEqual(ScormLessonStatus.Complete, relatedPerformance.Status);
            Assert.GreaterOrEqual(DateTime.UtcNow, relatedPerformance.CompletedDate);
            Assert.GreaterOrEqual(0, relatedPerformance.TimeUsedInSeconds);
        }

        [Test]
        public void Handle_GivenCompleteFinalTest_MustPublishEventForAllChangedLessonStatuses()
        {
            const int lesson1RcoId = 1;
            const int lesson2RcoId = 2;
            const int lesson3RcoId = 3;
            const int courseRcoId = 4;

            var lesson1Rco = new Rco { Context = "lesson", Id = lesson1RcoId, IsFinalTest = true };
            var lesson2Rco = new Rco { Context = "lesson", Id = lesson2RcoId };
            var lesson3Rco = new Rco { Context = "lesson", Id = lesson3RcoId };
            var courseRco = new Rco { Context = "course", Id = courseRcoId };

            var lesson1Per = new ScormPerformance { Id = 2, RcoId = lesson1RcoId, Status = ScormLessonStatus.Complete };
            var lesson2Per = new ScormPerformance { Id = 3, RcoId = lesson2RcoId, Status = ScormLessonStatus.Incomplete };
            var lesson3Per = new ScormPerformance { Id = 4, RcoId = lesson3RcoId, Status = ScormLessonStatus.Complete };
            var coursePer = new ScormPerformance { Id = 5, RcoId = courseRcoId, Status = ScormLessonStatus.Incomplete };

            _contextMock.Object.Rcos.Add(lesson1Rco);
            _contextMock.Object.Rcos.Add(lesson2Rco);
            _contextMock.Object.Rcos.Add(lesson3Rco);
            _contextMock.Object.Rcos.Add(courseRco);

            _contextMock.Object.ScormPerformances.Add(lesson1Per);
            _contextMock.Object.ScormPerformances.Add(lesson2Per);
            _contextMock.Object.ScormPerformances.Add(lesson3Per);
            _contextMock.Object.ScormPerformances.Add(coursePer);

            _performanceProviderMock.Setup(p => p.GetPerformancesForAllLessons(lesson1Per.UserId, lesson1Per.RcoId)).Returns(new List<ScormPerformance> { lesson1Per, lesson2Per });
            _rcoProviderMock.Setup(r => r.GetTopParent(lesson1RcoId)).Returns(courseRco);
            _rcoProviderMock.Setup(r => r.GetCourseRcos(lesson1RcoId, false)).Returns(new List<Rco> { lesson1Rco, lesson2Rco });

            _updateRelatedPerformances.Handle(new PerformanceStatusChangedEvent { PerformanceId = lesson1Per.Id });

            _eventAggregatorMock.Verify(e => e.Publish(It.Is((PerformanceStatusChangedEvent ev) => ev.PerformanceId == lesson2Per.Id)), Times.Once);
            _eventAggregatorMock.Verify(e => e.Publish(It.Is((PerformanceStatusChangedEvent ev) => ev.PerformanceId == coursePer.Id)), Times.Once);
        }

        [Test]
        public void Handle_GivenRegularLesson_ShouldCompleteCourseIfAllLessonsAreCompleted()
        {
            const int lesson1RcoId = 1;
            const int lesson2RcoId = 2;
            const int courseRcoId = 3;

            var lesson1Rco = new Rco { Context = "lesson", Id = lesson1RcoId };
            var lesson2Rco = new Rco { Context = "lesson", Id = lesson2RcoId };
            var courseRco = new Rco { Context = "course", Id = courseRcoId };

            var lesson1Per = new ScormPerformance { Id = 2, RcoId = lesson1RcoId, Status = ScormLessonStatus.Complete };
            var lesson2Per = new ScormPerformance { Id = 3, RcoId = lesson2RcoId, Status = ScormLessonStatus.Complete };
            var coursePer = new ScormPerformance { Id = 4, RcoId = courseRcoId, Status = ScormLessonStatus.Incomplete };

            _contextMock.Object.Rcos.Add(lesson1Rco);
            _contextMock.Object.Rcos.Add(lesson2Rco);
            _contextMock.Object.Rcos.Add(courseRco);

            _contextMock.Object.ScormPerformances.Add(lesson1Per);
            _contextMock.Object.ScormPerformances.Add(lesson2Per);
            _contextMock.Object.ScormPerformances.Add(coursePer);

            _performanceProviderMock.Setup(p => p.GetPerformancesForAllLessons(lesson1Per.UserId, lesson1Per.RcoId)).Returns(new List<ScormPerformance> { lesson1Per, lesson2Per });
            _rcoProviderMock.Setup(r => r.GetTopParent(lesson1RcoId)).Returns(courseRco);

            _updateRelatedPerformances.Handle(new PerformanceStatusChangedEvent { PerformanceId = lesson1Per.Id });

            Assert.AreEqual(ScormLessonStatus.Complete, coursePer.Status);
        }

        [Test]
        public void Handle_GivenRegularLesson_ShouldNotCompleteCourseIfNotAllLessonsAreCompleted()
        {
            const int lesson1RcoId = 1;
            const int lesson2RcoId = 2;
            const int courseRcoId = 3;

            var lesson1Rco = new Rco { Context = "lesson", Id = lesson1RcoId };
            var lesson2Rco = new Rco { Context = "lesson", Id = lesson2RcoId };
            var courseRco = new Rco { Context = "course", Id = courseRcoId };

            var lesson1Per = new ScormPerformance { Id = 2, RcoId = lesson1RcoId, Status = ScormLessonStatus.Complete };
            var lesson2Per = new ScormPerformance { Id = 3, RcoId = lesson2RcoId, Status = ScormLessonStatus.Incomplete };
            var coursePer = new ScormPerformance { Id = 4, RcoId = courseRcoId, Status = ScormLessonStatus.Incomplete };

            _contextMock.Object.Rcos.Add(lesson1Rco);
            _contextMock.Object.Rcos.Add(lesson2Rco);
            _contextMock.Object.Rcos.Add(courseRco);

            _contextMock.Object.ScormPerformances.Add(lesson1Per);
            _contextMock.Object.ScormPerformances.Add(lesson2Per);
            _contextMock.Object.ScormPerformances.Add(coursePer);

            _performanceProviderMock.Setup(p => p.GetPerformancesForAllLessons(lesson1Per.UserId, lesson1Per.RcoId)).Returns(new List<ScormPerformance> { lesson1Per, lesson2Per });
            _rcoProviderMock.Setup(r => r.GetTopParent(lesson1RcoId)).Returns(courseRco);

            _updateRelatedPerformances.Handle(new PerformanceStatusChangedEvent { PerformanceId = lesson1Per.Id });

            Assert.AreEqual(ScormLessonStatus.Incomplete, coursePer.Status);
        }

        [Test]
        public void Handle_GivenRegularLessonWithStatusIncomplete_ShouldSetCourseIncomplete()
        {
            const int lesson1RcoId = 1;
            const int courseRcoId = 3;

            var lesson1Rco = new Rco { Context = "lesson", Id = lesson1RcoId };
            var courseRco = new Rco { Context = "course", Id = courseRcoId };

            var lesson1Per = new ScormPerformance { Id = 2, RcoId = lesson1RcoId, Status = ScormLessonStatus.Incomplete };
            var coursePer = new ScormPerformance { Id = 4, RcoId = courseRcoId, Status = ScormLessonStatus.NotAttempted };

            _contextMock.Object.Rcos.Add(lesson1Rco);
            _contextMock.Object.Rcos.Add(courseRco);

            _contextMock.Object.ScormPerformances.Add(lesson1Per);
            _contextMock.Object.ScormPerformances.Add(coursePer);

            _performanceProviderMock.Setup(p => p.GetPerformancesForAllLessons(lesson1Per.UserId, lesson1Per.RcoId)).Returns(new List<ScormPerformance> { lesson1Per });
            _rcoProviderMock.Setup(r => r.GetTopParent(lesson1RcoId)).Returns(courseRco);

            _updateRelatedPerformances.Handle(new PerformanceStatusChangedEvent { PerformanceId = lesson1Per.Id });

            Assert.AreEqual(ScormLessonStatus.Incomplete, coursePer.Status);
        }
    }
}