﻿using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Api.Messaging.Handlers;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Messaging.Handlers.ActivityResourcesChanged
{
    [TestFixture]
    public class AllocateExaminersToExamParticipantsTests
    {
        private AllocateExaminersToExamParticipants _testSubject;
        private ContextMock _contextMock;
        private Mock<IArticleRepository> _articleRepositoryMock;

        private const int ExaminerResourceType = 5;
        private const int SomethingElseResourceType = 1;

        [SetUp]
        public void Setup()
        {
            _contextMock = new ContextMock();
            _articleRepositoryMock = new Mock<IArticleRepository>();

            _testSubject = new AllocateExaminersToExamParticipants(_contextMock.Object, _articleRepositoryMock.Object);
        }

        [Test]
        [TestCase(ArticleTypes.CaseExam)]
        [TestCase(ArticleTypes.ProjectAssignment)]
        public void AllocateExaminersForActivity_GivenValidArticleTypes_ShouldAllocate(ArticleTypes articleType)
        {
            // Test input
            const int activityId = 1;
            var article = new CaseExamArticle { ArticleTypeId = (int)articleType };
            var activity = new Activity { Id = activityId };

            // Configure mocks
            _articleRepositoryMock.Setup(a => a.FindByActivityId(activityId)).Returns(article);
            _contextMock.Object.Activities.Add(activity);

            _testSubject.AllocateExaminersForActivity(activityId);

            _contextMock.Verify(c => c.SaveChanges(), Times.Once);
        }

        [Test]
        [TestCase(ArticleTypes.Classroom)]
        [TestCase(ArticleTypes.ELearning)]
        [TestCase(ArticleTypes.ExternalCertification)]
        [TestCase(ArticleTypes.InternalCertification)]
        [TestCase(ArticleTypes.Mockexam)]
        [TestCase(ArticleTypes.MultipleChoice)]
        [TestCase(ArticleTypes.Other)]
        public void AllocateExaminersForActivity_GivenInvalidArticleTypes_ShouldNotAllocate(ArticleTypes articleType)
        {
            // Test input
            const int activityId = 1;
            var article = new CaseExamArticle { ArticleTypeId = (int)articleType };
            var activity = new Activity { Id = activityId };

            // Configure mocks
            _articleRepositoryMock.Setup(a => a.FindByActivityId(activityId)).Returns(article);
            _contextMock.Object.Activities.Add(activity);

            _testSubject.AllocateExaminersForActivity(activityId);

            _contextMock.Verify(c => c.SaveChanges(), Times.Never);
        }

        [Test]
        public void AllocateExaminersForActivity_ShouldAllocateExaminerResourcesEvenly()
        {
            // Test input
            const int activityId = 1;
            var activity = new Activity { Id = activityId };
            var article = new CaseExamArticle { ArticleTypeId = (int)ArticleTypes.CaseExam };

            activity.Resources.Add(new Resource { Id = 1, ResourceTypeId = ExaminerResourceType });
            activity.Resources.Add(new Resource { Id = 2, ResourceTypeId = ExaminerResourceType });
            activity.Resources.Add(new Resource { Id = 3, ResourceTypeId = SomethingElseResourceType });

            activity.Participants.Add(new Participant { Id = 1 });
            activity.Participants.Add(new Participant { Id = 2 });
            activity.Participants.Add(new Participant { Id = 3 });
            activity.Participants.Add(new Participant { Id = 4 });
            activity.Participants.Add(new Participant { Id = 5 });

            // Configure mocks
            _articleRepositoryMock.Setup(a => a.FindByActivityId(activityId)).Returns(article);
            _contextMock.Object.Activities.Add(activity);

            _testSubject.AllocateExaminersForActivity(activityId);

            Assert.AreEqual(1, activity.Participants[0].ExaminerResourceId);
            Assert.AreEqual(2, activity.Participants[1].ExaminerResourceId);
            Assert.AreEqual(1, activity.Participants[2].ExaminerResourceId);
            Assert.AreEqual(2, activity.Participants[3].ExaminerResourceId);
            Assert.AreEqual(1, activity.Participants[4].ExaminerResourceId);
        }


        [Test]
        public void AllocateExaminersForActivity_ShouldNotAllocateExaminerResourcesToParticipantsWithGrades()
        {
            // Test input
            const int activityId = 1;
            var activity = new Activity { Id = activityId };
            var article = new CaseExamArticle { ArticleTypeId = (int)ArticleTypes.CaseExam };

            activity.Resources.Add(new Resource { Id = 1, ResourceTypeId = ExaminerResourceType });
            activity.Resources.Add(new Resource { Id = 2, ResourceTypeId = ExaminerResourceType });
            activity.Resources.Add(new Resource { Id = 3, ResourceTypeId = SomethingElseResourceType });

            activity.Participants.Add(new Participant { Id = 1 });
            activity.Participants.Add(new Participant { Id = 2, ExamGrade = "A"});
            
            // Configure mocks
            _articleRepositoryMock.Setup(a => a.FindByActivityId(activityId)).Returns(article);
            _contextMock.Object.Activities.Add(activity);

            _testSubject.AllocateExaminersForActivity(activityId);

            Assert.AreEqual(1, activity.Participants[0].ExaminerResourceId);
            Assert.IsNull(activity.Participants[1].ExaminerResourceId);
        }

        [Test]
        public void AllocateExaminersForActivity_ShouldNotAllocateExaminerResourcesToDeletedParticipants()
        {
            // Test input
            const int activityId = 1;
            var activity = new Activity { Id = activityId };
            var article = new CaseExamArticle { ArticleTypeId = (int)ArticleTypes.CaseExam };

            activity.Resources.Add(new Resource { Id = 1, ResourceTypeId = ExaminerResourceType });
            activity.Resources.Add(new Resource { Id = 2, ResourceTypeId = ExaminerResourceType });
            activity.Resources.Add(new Resource { Id = 3, ResourceTypeId = SomethingElseResourceType });

            activity.Participants.Add(new Participant { Id = 1 });
            activity.Participants.Add(new Participant { Id = 2, IsDeleted = true });
            
            // Configure mocks
            _articleRepositoryMock.Setup(a => a.FindByActivityId(activityId)).Returns(article);
            _contextMock.Object.Activities.Add(activity);

            _testSubject.AllocateExaminersForActivity(activityId);

            Assert.AreEqual(1, activity.Participants[0].ExaminerResourceId);
            Assert.IsNull(activity.Participants[1].ExaminerResourceId);
        }

        [Test]
        public void AllocateExaminersForActivity_ShouldResetExaminerResourceId_WhenNoExaminers()
        {
            // Test input
            const int activityId = 1;
            var activity = new Activity { Id = activityId };
            var article = new CaseExamArticle { ArticleTypeId = (int)ArticleTypes.CaseExam };
            
            activity.Participants.Add(new Participant { Id = 1, ExaminerResourceId = 2});
            
            // Configure mocks
            _articleRepositoryMock.Setup(a => a.FindByActivityId(activityId)).Returns(article);
            _contextMock.Object.Activities.Add(activity);

            _testSubject.AllocateExaminersForActivity(activityId);

            Assert.IsNull(activity.Participants[0].ExaminerResourceId);
        }
    }
}