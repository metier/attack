﻿using Metier.Phoenix.Api.Reporting.Utilities;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Reporting.Utilities
{
    [TestFixture]
    public class ProgressHelperTests
    {
        [Test]
        [TestCase(1, 2, "1/2 (50%)")]
        [TestCase(1, 3, "1/3 (33%)")]
        [TestCase(1, 6, "1/6 (17%)")]
        public void GetProgress_MustFormatProgressWithRoundedPercentage(int completed, int total, string expected)
        {
            Assert.AreEqual(expected, ProgressHelper.GetProgress(total, completed));
        }
    }
}