﻿using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Reporting.Reports.ProgressStatuses;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Reporting.Reports.ProgressStatuses
{
    [TestFixture]
    public class ProgressStatusesReportProviderTests
    {
        ProgressStatusesReportProvider _reportProvider;

        ContextMock _contextMock;
        Mock<IAuthorizationProvider> _authorizationProviderMock;
        Mock<ICustomersProvider> _customersProviderMock;

        [SetUp]
        public void Setup()
        {   
            _contextMock = new ContextMock();
            _authorizationProviderMock = new Mock<IAuthorizationProvider>();
            _customersProviderMock = new Mock<ICustomersProvider>();
            _reportProvider = new ProgressStatusesReportProvider(_contextMock.Object, _authorizationProviderMock.Object, _customersProviderMock.Object);
        }

        [Test]
        public void GetReport_NonAdminUser_GivenNoCustomerIdsAndNoActivityIds_ShouldThrowUnauthorizedException()
        {
            var definition = new ProgressStatusesReportDefinition(null, null, null, null, null, null, null, FileFormats.Excel);
            _authorizationProviderMock.Setup(a => a.IsAdmin()).Returns(false);
            Assert.Throws<UnauthorizedException>(() => _reportProvider.GetReport(definition));
        }

        [Test]
        public void GetReport_NonAdminUser_GivenCustomerIdsNotPartOfUser_ShouldThrowUnauthorizedException()
        {
            var definition = new ProgressStatusesReportDefinition(new[] { 1, 2 }, null, null, null, null, null, null, FileFormats.Excel);
            _authorizationProviderMock.Setup(a => a.IsAdmin()).Returns(false);
            _authorizationProviderMock.Setup(a => a.HasCustomer(1, _customersProviderMock.Object)).Returns(true);
            _authorizationProviderMock.Setup(a => a.HasCustomer(2, _customersProviderMock.Object)).Returns(false);
            Assert.Throws<UnauthorizedException>(() => _reportProvider.GetReport(definition));
        }

        [Test]
        public void GetReport_NonAdminUser_GivenActivityIdsNotPartOfUser_ShouldThrowUnauthorizedException()
        {
            var definition = new ProgressStatusesReportDefinition(null, null, null, new[] { 1, 2 }, null, null, null, FileFormats.Excel);
            _authorizationProviderMock.Setup(a => a.IsAdmin()).Returns(false);
            _authorizationProviderMock.Setup(a => a.HasCustomerOwningActivity(1)).Returns(true);
            _authorizationProviderMock.Setup(a => a.HasCustomerOwningActivity(2)).Returns(false);
            Assert.Throws<UnauthorizedException>(() => _reportProvider.GetReport(definition));
        }

        [Test]
        public void GetReport_NonAdminUser_GivenActivityIdsPartOfUser_ShouldReturnReport()
        {
            var definition = new ProgressStatusesReportDefinition(null, null, null, new[] { 1 }, null, null, null, FileFormats.Excel);

            _authorizationProviderMock.Setup(a => a.IsAdmin()).Returns(false);
            _authorizationProviderMock.Setup(a => a.HasCustomerOwningActivity(1)).Returns(true);
            
            var report = _reportProvider.GetReport(definition);

            Assert.IsNotNull(report);
        }

        [Test]
        public void GetReport_NonAdminUser_GivenCustomerIdsPartOfUser_ShouldReturnReport()
        {
            var definition = new ProgressStatusesReportDefinition(new[] { 1 }, null, null, null, null, null, null, FileFormats.Excel);

            _customersProviderMock.Setup(c => c.GetChildrenIds(1, true)).Returns(new[] { 1 });

            _authorizationProviderMock.Setup(a => a.IsAdmin()).Returns(false);
            _authorizationProviderMock.Setup(a => a.HasCustomer(1, _customersProviderMock.Object)).Returns(true);
            
            var report = _reportProvider.GetReport(definition);

            Assert.IsNotNull(report);
        }

        [Test]
        public void GetReport_AdminUser_ShouldReturnReport()
        {
            var definition = new ProgressStatusesReportDefinition(null, null, null, null, null, null, null, FileFormats.Excel);

            _authorizationProviderMock.Setup(a => a.IsAdmin()).Returns(true);
            
            var report = _reportProvider.GetReport(definition);

            Assert.IsNotNull(report);
        }
    }
}