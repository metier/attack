﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Metier.Phoenix.Core.Business.DocumentProduction.Attributes;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats.Excel;
using NUnit.Framework;
using OfficeOpenXml;

namespace Metier.Phoenix.UnitTests.Api.Reporting.Formats.Excel
{
    [TestFixture]
    public class ExcelFactoryTests
    {
        private ExcelFactory _excelFactory;

        [SetUp]
        public void Setup()
        {
            _excelFactory = new ExcelFactory();
        }

        [Test]
        public void CreateDocument_GivenListOfItems_ShouldCreateExcelDocumentWithAllPropertiesAsColumns()
        {
            var items = new List<Item>
            {
                new Item("item 1", 1),
                new Item("item 2", 2)
            };

            var excelFile = _excelFactory.CreateDocument(items);

            var package = new ExcelPackage(new MemoryStream(excelFile));
            var worksheet = package.Workbook.Worksheets["Report"];

            Assert.AreEqual("Property1", worksheet.Cells[1 , 1].Text);
            Assert.AreEqual("Property2", worksheet.Cells[1,  2].Text);

            Assert.AreEqual("item 1", worksheet.Cells[2, 1].Text);
            Assert.AreEqual("1", worksheet.Cells[2, 2].Text);

            Assert.AreEqual("item 2", worksheet.Cells[3, 1].Text);
            Assert.AreEqual("2", worksheet.Cells[3, 2].Text);

            // Verify that outer bounds is empty
            Assert.IsNullOrEmpty(worksheet.Cells[4, 1].Text);
            Assert.IsNullOrEmpty(worksheet.Cells[1, 3].Text);
        }
       
        [Test]
        public void CreateDocument_ShouldCreateColumnsFromDisplayNameAttribute()
        {
            var items = new List<ItemWithCustomDisplayName>
            {
                new ItemWithCustomDisplayName { ClarkKent = "hello", OtherProperty = "other"}
            };

            var excelFile = _excelFactory.CreateDocument(items);

            var package = new ExcelPackage(new MemoryStream(excelFile));
            var worksheet = package.Workbook.Worksheets["Report"];

            Assert.AreEqual("Superman", worksheet.Cells[1, 1].Text);
            Assert.AreEqual("OtherProperty", worksheet.Cells[1, 2].Text);
            Assert.AreEqual("hello", worksheet.Cells[2, 1].Text);
            Assert.AreEqual("other", worksheet.Cells[2, 2].Text);
        }

        [Test]
        public void CreateDocument_ShouldSetBackgroundColorForRowWithProperty()
        {
            var expectedColor = Color.FromArgb(255, 0, 255);
            var items = new List<ItemWithRowStyle>
            {
                new ItemWithRowStyle{ValueProperty = "MyValue1", RowStyleOptions = new ExcelRowStyleOptions{BackgroundColor = expectedColor } },
                new ItemWithRowStyle{ValueProperty = "MyValue2"}
            };

            var excelFile = _excelFactory.CreateDocument(items);

            var package = new ExcelPackage(new MemoryStream(excelFile));
            var worksheet = package.Workbook.Worksheets["Report"];

            Assert.AreEqual("ValueProperty", worksheet.Cells[1, 1].Text);
            Assert.IsNullOrEmpty(worksheet.Cells[1, 2].Text);
            Assert.AreEqual("MyValue1", worksheet.Cells[2, 1].Text);
            Assert.IsNullOrEmpty(worksheet.Cells[2, 2].Text);
            Assert.AreEqual("MyValue2", worksheet.Cells[3, 1].Text);
            Assert.IsNullOrEmpty(worksheet.Cells[3, 2].Text);

            Assert.AreEqual("FFFF00FF", worksheet.Row(2).Style.Fill.BackgroundColor.Rgb);
        }
        
        [Test]
        public void CreateDocument_ShouldCreateColumnsFromListOfItems()
        {
            var items = new List<ItemWithListOfColumns>
            {
                new ItemWithListOfColumns
                {
                    ListOfItems = new List<ItemWithCustomDisplayName>
                    {
                        new ItemWithCustomDisplayName { ClarkKent = "1", OtherProperty = "Other1"}
                    },
                    OtherProperty = "Prop1"
                },
                new ItemWithListOfColumns
                {
                    ListOfItems = new List<ItemWithCustomDisplayName>
                    {
                        new ItemWithCustomDisplayName { ClarkKent = "2"},
                        new ItemWithCustomDisplayName { ClarkKent = "3"}
                    },
                    OtherProperty = "Prop2"
                },
            };

            var excelFile = _excelFactory.CreateDocument(items);

            var package = new ExcelPackage(new MemoryStream(excelFile));
            var worksheet = package.Workbook.Worksheets["Report"];

            // Headings
            Assert.AreEqual("Superman", worksheet.Cells[1, 1].Text);
            Assert.AreEqual("OtherProperty", worksheet.Cells[1, 2].Text);
            Assert.AreEqual("Superman", worksheet.Cells[1, 3].Text);
            Assert.AreEqual("OtherProperty", worksheet.Cells[1, 4].Text);
            Assert.AreEqual("OtherProperty", worksheet.Cells[1, 5].Text);
            Assert.IsNullOrEmpty(worksheet.Cells[1, 6].Text);

            // Row 1
            Assert.AreEqual("1", worksheet.Cells[2, 1].Text);
            Assert.AreEqual("Other1", worksheet.Cells[2, 2].Text);
            Assert.IsNullOrEmpty(worksheet.Cells[2, 3].Text);
            Assert.IsNullOrEmpty(worksheet.Cells[2, 4].Text);
            Assert.AreEqual("Prop1", worksheet.Cells[2, 5].Text);
            Assert.IsNullOrEmpty(worksheet.Cells[2, 6].Text);
            
            // Row 2
            Assert.AreEqual("2", worksheet.Cells[3, 1].Text);
            Assert.IsNullOrEmpty(worksheet.Cells[3, 2].Text);
            Assert.AreEqual("3", worksheet.Cells[3, 3].Text);
            Assert.IsNullOrEmpty(worksheet.Cells[3, 4].Text);
            Assert.AreEqual("Prop2", worksheet.Cells[3, 5].Text);
            Assert.IsNullOrEmpty(worksheet.Cells[3, 6].Text);
        }
        
        [Test]
        public void CreateDocument_ShouldCreateColumnsWithCountFromListOfItems()
        {
            var items = new List<ItemWithListOfColumnsWithCount>
            {
                new ItemWithListOfColumnsWithCount
                {
                    ListOfItems = new List<ItemWithCountListColumns>
                    {
                        new ItemWithCountListColumns { ClarkKent = "something" },
                        new ItemWithCountListColumns { ClarkKent = "something else" }
                    }
                }
            };

            var excelFile = _excelFactory.CreateDocument(items);

            var package = new ExcelPackage(new MemoryStream(excelFile));
            var worksheet = package.Workbook.Worksheets["Report"];

            // Headings
            Assert.AreEqual("Superman 1", worksheet.Cells[1, 1].Text);
            Assert.AreEqual("Superman 2", worksheet.Cells[1, 2].Text);
            Assert.IsNullOrEmpty(worksheet.Cells[1, 3].Text);
        }        
        
        [Test]
        public void CreateDocument_ShouldNotCreateColumnsFromListOfItems_WhenNoItems()
        {
            var items = new List<ItemWithListOfColumnsWithCount>();

            var excelFile = _excelFactory.CreateDocument(items);

            var package = new ExcelPackage(new MemoryStream(excelFile));
            var worksheet = package.Workbook.Worksheets["Report"];

            // Headings
            Assert.IsNullOrEmpty(worksheet.Cells[1, 1].Text);
        }

        public class ItemWithListOfColumns
        {
            public IList<ItemWithCustomDisplayName> ListOfItems { get; set; }
            
            public string OtherProperty { get; set; }
        }   
        
        public class ItemWithListOfColumnsWithCount
        {
            public IList<ItemWithCountListColumns> ListOfItems { get; set; }
        }        
        
        public class ItemWithCustomDisplayName
        {
            [DisplayName("Superman")]
            public string ClarkKent { get; set; }
            
            public string OtherProperty { get; set; }
        }

        public class ItemWithCountListColumns
        {
            [DisplayName("Superman")]
            [CountListColumns]
            public string ClarkKent { get; set; }
        }

        public class ItemWithRowStyle
        {
            public ExcelRowStyleOptions RowStyleOptions { get; set; }
            public string ValueProperty { get; set; }
        }

        public class Item
        {
            public Item(string property1, int property2)
            {
                Property1 = property1;
                Property2 = property2;
            }

            public string Property1 { get; set; }
            public int Property2 { get; set; }
        }
    }
}