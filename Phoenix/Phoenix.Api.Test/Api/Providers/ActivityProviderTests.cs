﻿using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Factories.ExamPlayer;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Queryables;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Providers
{
    [TestFixture]
    public class ActivityProviderTests
    {
        private int _ids;

        private ContextMock _contextMock;
        private Mock<IExamPlayerFactory> _examPlayerFactoryMock;
        private ActivityProvider _activityProvider;

        private ProductCategory _projectManagementProductCategory;
        private Product _projectManagementProduct;
        private ArticleType _eLearningArticleType;
        private Language _norwegianLanguage;

        [SetUp]
        public void Setup()
        {
            _contextMock = new ContextMock();
            _examPlayerFactoryMock = new Mock<IExamPlayerFactory>();
            _activityProvider = new ActivityProvider(_contextMock.Object, _examPlayerFactoryMock.Object);

            _projectManagementProductCategory = new ProductCategory
            {
                Id = 1,
                Name = "Project Management",
                ProductCodePrefix = "PM"
            };
            _projectManagementProduct = new Product
            {
                Id = 1,
                ProductCategory = _projectManagementProductCategory,
                ProductCategoryId = _projectManagementProductCategory.Id,
                ProductNumber = "02"
            };
            _eLearningArticleType = new ArticleType {Id = 1, Name = "E-learning", CharCode = "E"};
            _norwegianLanguage = new Language {Id = 45, Name = "Norwegian", Identifier = "NO"};
        }

        #region Invoicing tests

        [Test]
        public void GetOrderLine_MustSetTextToActivityName()
        {
            var activity = CreateActivity();
            activity.Name = "exepected name";

            var orderLine = _activityProvider.GetOrderLine(activity.Id);

            Assert.AreEqual(activity.Name, orderLine.Text);
        }

        [Test]
        public void GetOrderLine_MustSetPriceToFixedPrice()
        {
            var activity = CreateActivity();
            activity.FixedPrice = 12345789;

            var orderLine = _activityProvider.GetOrderLine(activity.Id);

            Assert.AreEqual(activity.FixedPrice, orderLine.Price);
        }

        [Test]
        public void GetOrderLine_MustSetValueAddedTaxToZero()
        {
            var activity = CreateActivity();
            var orderLine = _activityProvider.GetOrderLine(activity.Id);

            Assert.AreEqual(0, orderLine.ValueAddedTax);
        }

        [Test]
        public void GetOrderLine_MustSetType()
        {
            var activity = CreateActivity();
            var orderLine = _activityProvider.GetOrderLine(activity.Id);

            Assert.AreEqual(OrderLineType.FixedPriceActivity, orderLine.Type);
        }

        [Test]
        public void GetOrderLine_MustSetCurrencyCode()
        {
            var activity = CreateActivity();
            activity.CurrencyCodeId = 1234;
            var orderLine = _activityProvider.GetOrderLine(activity.Id);

            Assert.AreEqual(activity.CurrencyCodeId.Value, orderLine.CurrencyCodeId);
        }

        [Test]
        public void GetOrderLine_MustSetId()
        {
            var activity = CreateActivity();
            var orderLine = _activityProvider.GetOrderLine(activity.Id);

            Assert.AreEqual(orderLine.Id, activity.Id);
        }

        [Test]
        public void GetOrderLine_MustSetArticleFromCustomerArticle()
        {
            var activity = CreateActivity();
            var customerArticle = _contextMock.Object.CustomerArticles.First(c => c.Id == activity.CustomerArticleId);

            var orderLine = _activityProvider.GetOrderLine(activity.Id);

            Assert.IsNotEmpty(orderLine.Article);
            Assert.AreEqual(customerArticle.ArticlePath, orderLine.Article);
        }

        #endregion

        private CustomerArticle CreateCustomerArticle()
        {
            var article = new ElearningArticle
            {
                Id = 1,
                Product = _projectManagementProduct,
                ArticleType = _eLearningArticleType,
                ArticleTypeId = _eLearningArticleType.Id,
                Language = _norwegianLanguage,
                LanguageId = _norwegianLanguage.Id
            };
            var customerArticle = new CustomerArticle
            {
                Id = GetNextId(),
                ArticleCopy = article,
                ArticleCopyId = article.Id
            };

            _contextMock.Object.CustomerArticles.Add(customerArticle);
            return customerArticle;
        }

        private Activity CreateActivity()
        {
            var customerArticle = CreateCustomerArticle();
            var activity = new Activity {Id = GetNextId(), CustomerArticleId = customerArticle.Id};
            _contextMock.Object.Activities.Add(activity);
            return activity;
        }

        private int GetNextId()
        {
            return ++_ids;
        }
    }
}