﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using Metier.Phoenix.Api.Configuration;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade.Account;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Core.Security;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Templates.PasswordResetRequest;
using Metier.Phoenix.Mail.Templates.UserRegistrationNotification;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Providers
{
    [TestFixture]
    public class AccountsProviderTests
    {
        private int id;
        private List<MembershipUser> membershipUsers;
        private const string LearningPortalSourceSystem = "LearningPortal";

        private Mock<IUsersRepository> _usersRepositoryMock;
        private Mock<IPhoenixMembership> _phoenixMembershipMock;
        private Mock<IPhoenixMemberShipProvider> _phoenixMembershipProvider;
        private Mock<IPhoenixMembershipRoles> _phoenixMembershipRolesMock;
        private Mock<IIdentityProvider> _identityProviderMock;
        private Mock<IDistributorRepository> _distributorRepositoryMock;
        private ContextMock _contextMock;
        private Mock<IConfiguration> _configurationMock;
        private Mock<IPasswordResetRequestMessageFactory> _passwordResetRequestMessageFactoryMock;
        private Mock<IAutoMailer> _autoMailerMock;
        private Mock<IAuthorizationProvider> _authorizationProviderMock;
        private Mock<ICodeRepository> _codeRepositoryMock;

        private ICustomersProvider _customersProvider;
        private Mock<ICustomerRepository> _customerRepository;

        private AccountsProvider _accountsProvider;
        private Mock<IUserRegistrationNotificationFactory> _userRegistrationNotificationFactoryMock;
        private Mock<IPasswordResetRequestFactory> _passwordResetRequestFactoryMock;

        [SetUp]
        public void Setup()
        {
            id = 0;
            membershipUsers = new List<MembershipUser>();

            _usersRepositoryMock = new Mock<IUsersRepository>();
            _phoenixMembershipMock = new Mock<IPhoenixMembership>();
            _phoenixMembershipProvider = new Mock<IPhoenixMemberShipProvider>();
            _phoenixMembershipRolesMock = new Mock<IPhoenixMembershipRoles>();
            _identityProviderMock = new Mock<IIdentityProvider>();
            _distributorRepositoryMock = new Mock<IDistributorRepository>();
            _contextMock = new ContextMock();
            _configurationMock = new Mock<IConfiguration>();
            _passwordResetRequestFactoryMock = new Mock<IPasswordResetRequestFactory>();
            _passwordResetRequestMessageFactoryMock = new Mock<IPasswordResetRequestMessageFactory>();
            _userRegistrationNotificationFactoryMock = new Mock<IUserRegistrationNotificationFactory>();
            _autoMailerMock = new Mock<IAutoMailer>();
            _autoMailerMock.Setup(a => a.Send(It.IsAny<AutoMailMessage>())).Returns(true);
            _authorizationProviderMock = new Mock<IAuthorizationProvider>();
            _codeRepositoryMock = new Mock<ICodeRepository>();

            _customerRepository = new Mock<ICustomerRepository>();
            //_customersProviderMock = new Mock<ICustomersProvider>();
            _customersProvider = new CustomersProvider(_contextMock.Object, _customerRepository.Object);


            _phoenixMembershipMock.Setup(p => p.GetProvider(It.IsAny<MetierLmsContext>())).Returns(_phoenixMembershipProvider.Object);
            _phoenixMembershipProvider.Setup(p => p.ResetPassword(It.IsAny<string>(), It.IsAny<string>())).Returns(Guid.NewGuid().ToString());
            _phoenixMembershipProvider.Setup(p => p.ChangePassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            _phoenixMembershipMock.Setup(m => m.GetUser(It.IsAny<string>(), It.IsAny<bool>())).Returns((string username, bool userIsOnline) => PhoenixMembershipMock_GetUser(username));   
            _phoenixMembershipMock.Setup(m => m.GetUserNameByEmail(It.IsAny<string>())).Returns((string email) => PhoenixMembershipMock_GetUserNameByEmail(email));   
            _phoenixMembershipRolesMock.Setup(p => p.GetRolesForUser(It.IsAny<string>())).Returns(new[] {"PhoenixUser"});

            _distributorRepositoryMock.Setup(d => d.GetDistributors()).Returns(new List<Distributor>
            {
                new Distributor {Id = 1194, Name = "Metier Academy Norway"},
                new Distributor {Id = 1234, Name = "Metier Mock Distributor"}
            });

            _configurationMock.Setup(c => c.GetPasswordResetFormUrl(LearningPortalSourceSystem)).Returns("http://learningportal/passwordResetForm.aspx");
            _configurationMock.Setup(c => c.PasswordResetTokenExpirationInMinutes).Returns(60*24);
            _userRegistrationNotificationFactoryMock.Setup(
                u => u.CreateUserRegistrationNotificationMessage(It.IsAny<User>(), null, null))
                .Returns(new AutoMailMessage
                {
                    Subject = "UnitTest",
                    Body = "<!doctype html>",
                    RecipientEmail = "test@test.com"
                });
            _codeRepositoryMock.Setup(c => c.IsValidCodeValue(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            _customerRepository.Setup(c => c.Create(It.IsAny<Customer>())).Returns((Customer customer) => CustomersRepositoryMock_CreateCustomer(customer));
            _customerRepository.Setup(c => c.Update(It.IsAny<int>(), It.IsAny<Customer>())).Returns((int id, Customer customer) => customer);
            _usersRepositoryMock.Setup(u => u.Create(It.IsAny<User>())).Returns((User user) => UsersRepositoryMock_CreateUser(user));
            _usersRepositoryMock.Setup(u => u.Update(It.IsAny<User>())).Returns((User user) => user);

            _accountsProvider = new AccountsProvider(_usersRepositoryMock.Object, _phoenixMembershipMock.Object, _phoenixMembershipRolesMock.Object, _customersProvider, _identityProviderMock.Object, 
                                                     _distributorRepositoryMock.Object, _contextMock.Object, _configurationMock.Object, _autoMailerMock.Object, _authorizationProviderMock.Object,
                                                     _userRegistrationNotificationFactoryMock.Object, _codeRepositoryMock.Object, _passwordResetRequestFactoryMock.Object, _passwordResetRequestMessageFactoryMock.Object);
        }

        private string PhoenixMembershipMock_GetUserNameByEmail(string email)
        {
            var user = membershipUsers.FirstOrDefault(u => u.Email == email);
            return user?.UserName;
        }

        private Customer CustomersRepositoryMock_CreateCustomer(Customer customer)
        {
            customer.AutomailDefinition = new AutomailDefinition
            {
                Id = GetNextId(),
                IsWelcomeNotification = false       //Do not send welcome notifications. This can be tested in other unit tests
            };
            _contextMock.Object.Customers.Add(customer);
            return customer;
        }
        private User UsersRepositoryMock_CreateUser(User user)
        {
            var customer = _contextMock.Object.Customers.FirstOrDefault(c => c.Id == user.CustomerId);

            user.Id = GetNextId();
            user.Customer = customer;

            _contextMock.Object.Users.Add(user);

            return user;
        }

        private MembershipUser PhoenixMembershipMock_GetUser(string userName)
        {
            var user = new MembershipUser("SqlProvider", userName, Guid.NewGuid(), userName + "@mockmail.com", "", "", true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.MinValue);
            return user;
        }

        #region Password Reset tests

        [Test]
        public void RequestPasswordReset_ValidPasswordReset_SendsSingleAutomailToUsersEmailAddress()
        {
            var user = CreateUserAccount();

            _passwordResetRequestFactoryMock.Setup(pf => pf.CreatePasswordResetRequest(user.User)).Returns(new PasswordResetRequest
            {
                Created = DateTime.UtcNow,
                Id = 1,
                ResetToken = Guid.NewGuid(),
                UserId = user.User.Id
            });

            _accountsProvider.RequestPasswordReset(user.Email, LearningPortalSourceSystem);

            _autoMailerMock.Verify(a => a.Send(It.Is<AutoMailMessage>(m => m.RecipientEmail == user.Email)), Times.Once());
        }

        [Test]
        public void RequestPasswordReset_RegistrationNotificationDateIsNull_SendsSingleAutomailToUsersEmailAddress()
        {
            var user = CreateUserAccount();
            user.User.UserRegistrationNotificationDate = null;

            _passwordResetRequestFactoryMock.Setup(pf => pf.CreatePasswordResetRequest(user.User)).Returns(new PasswordResetRequest
            {
                Created = DateTime.UtcNow,
                Id = 1,
                ResetToken = Guid.NewGuid(),
                UserId = user.User.Id
            });

            _accountsProvider.RequestPasswordReset(user.Email, LearningPortalSourceSystem);

            _autoMailerMock.Verify(a => a.Send(It.Is<AutoMailMessage>(m => m.RecipientEmail == user.Email)), Times.Once());
        }

        [Test]
        public void RequestPasswordReset_RegistrationNotificationDateIsInThePast_SendsSingleAutomailToUsersEmailAddress()
        {
            var user = CreateUserAccount();
            user.User.UserRegistrationNotificationDate = DateTime.UtcNow - TimeSpan.FromSeconds(1);

            _passwordResetRequestFactoryMock.Setup(pf => pf.CreatePasswordResetRequest(user.User)).Returns(new PasswordResetRequest
            {
                Created = DateTime.UtcNow,
                Id = 1,
                ResetToken = Guid.NewGuid(),
                UserId = user.User.Id
            });

            _accountsProvider.RequestPasswordReset(user.Email, LearningPortalSourceSystem);

            _autoMailerMock.Verify(a => a.Send(It.Is<AutoMailMessage>(m => m.RecipientEmail == user.Email)), Times.Once());
        }

        [Test]
        public void RequestPasswordReset_RegistrationNotificationDateIsInTheFuture_ThrowsValidationException()
        {
            var user = CreateUserAccount();
            user.User.UserRegistrationNotificationDate = DateTime.UtcNow + TimeSpan.FromDays(1);

            Assert.Throws(Is.TypeOf<ValidationException>(), () => _accountsProvider.RequestPasswordReset(user.Email, LearningPortalSourceSystem));
        }

        [Test]
        public void RequestPasswordReset_RegistrationNotificationDateIsInTheFutureButSameDate_SendsSingleAutomailToUsersEmailAddress()
        {
            var user = CreateUserAccount();
            user.User.UserRegistrationNotificationDate = DateTime.UtcNow + TimeSpan.FromSeconds(5);

            _passwordResetRequestFactoryMock.Setup(pf => pf.CreatePasswordResetRequest(user.User)).Returns(new PasswordResetRequest
            {
                Created = DateTime.UtcNow,
                Id = 1,
                ResetToken = Guid.NewGuid(),
                UserId = user.User.Id
            });

            _accountsProvider.RequestPasswordReset(user.Email, LearningPortalSourceSystem);

            _autoMailerMock.Verify(a => a.Send(It.Is<AutoMailMessage>(m => m.RecipientEmail == user.Email)), Times.Once());
        }

        [Test]
        public void RequestPasswordReset_UsingInvalidSource_ThrowsValidationException()
        {
            const string source = "invalidSource";
            var user = CreateUserAccount();

            _configurationMock.Setup(c => c.GetPasswordResetFormUrl(source)).Returns(string.Empty);

            Assert.Throws(Is.TypeOf<ValidationException>(), () => _accountsProvider.RequestPasswordReset(user.Email, source));  
        }

        [Test]
        public void RequestPasswordReset_UsingNonExistingUser_ThrowsEntityNotFoundException()
        {
            const string invalidEmail = "notauser@test.com";
            _usersRepositoryMock.Setup(u => u.FindByUsername(invalidEmail)).Returns((User) null);

            Assert.Throws(Is.TypeOf<EntityNotFoundException>(), () => _accountsProvider.RequestPasswordReset(invalidEmail, LearningPortalSourceSystem));
        }

        [Test]
        public void ResetPassword_ValidTokenAndEmail_ReturnsTrue()
        {
            var validToken = Guid.NewGuid();
            var user = CreateUserAccount();

            user.User.UserRegistrationNotificationDate = DateTime.UtcNow - TimeSpan.FromDays(100);      //Make sure the "Virgin user" logic does not interfere (function 'IsResetRequestForVirginUser' in the AccountsProvider class).

            _contextMock.Object.PasswordResetRequests.Add(new PasswordResetRequest
            {
                Created = DateTime.UtcNow,
                ResetToken = validToken,
                UserId = user.User.Id
            });

            var result = _accountsProvider.ResetPassword(validToken, user.Email, "newPassword");

            Assert.IsTrue(result);
        }

        [Test]
        public void ResetPassword_ValidTokenAndEmail_ResetsPasswordWithGivenPassword()
        {
            var validToken = Guid.NewGuid();
            const string newPassword = "newPassword";
            
            var user = CreateUserAccount();
            _contextMock.Object.PasswordResetRequests.Add(new PasswordResetRequest
            {
                Created = DateTime.UtcNow,
                ResetToken = validToken,
                UserId = user.User.Id
            });

            _accountsProvider.ResetPassword(validToken, user.Email, newPassword);

            _phoenixMembershipProvider.Verify(
                p => p.ChangePassword(It.Is<string>(s => s == user.Username), It.IsAny<string>(), It.Is<string>(s => s == newPassword)), 
                Times.Once);
        }

        [Test]
        public void ResetPassword_NonExistingToken_ThrowsValidationException()
        {
            Guid nonExistingToken = Guid.NewGuid();
            const string newPassword = "newPassword";

            var user = CreateUserAccount();

            Assert.Throws(Is.TypeOf<ValidationException>(), () => _accountsProvider.ResetPassword(nonExistingToken, user.Email, newPassword));
        }

        [Test]
        public void ResetPassword_ExpiredToken_ThrowsValidationException()
        {
            var validToken = Guid.NewGuid();
            var user = CreateUserAccount();
            _contextMock.Object.PasswordResetRequests.Add(new PasswordResetRequest
            {
                Created = DateTime.UtcNow - TimeSpan.FromDays(2),
                ResetToken = validToken,
                UserId = user.User.Id
            });

            Assert.Throws(Is.TypeOf<ValidationException>(), () => _accountsProvider.ResetPassword(validToken, user.Email, "newPassword"));
        }

        [Test]
        public void ResetPassword_VirginUserAndExpiredToken_ResetsPasswordWithGivenPassword()
        {
            const string newPassword = "newPassword";
            var validToken = Guid.NewGuid();
            var user = CreateUserAccount();

            user.User.UserRegistrationNotificationDate = DateTime.UtcNow - TimeSpan.FromDays(10);

            _contextMock.Object.PasswordResetRequests.Add(new PasswordResetRequest
            {
                Created = DateTime.UtcNow - TimeSpan.FromDays(10),
                ResetToken = validToken,
                UserId = user.User.Id
            });

            _accountsProvider.ResetPassword(validToken, user.Email, newPassword);

            _phoenixMembershipProvider.Verify(
                p => p.ChangePassword(It.Is<string>(s => s == user.Username), It.IsAny<string>(), It.Is<string>(s => s == newPassword)),
                Times.Once);
        }

        [Test]
        public void ResetPassword_EmailNotRegisteredForUser_ThrowsEntityNotFoundException()
        {
            var token = Guid.NewGuid();
            var providedEmail = "thisemailiswrong@email.com";
            var user = CreateUserAccount();
            _contextMock.Object.PasswordResetRequests.Add(new PasswordResetRequest
            {
                Created = DateTime.UtcNow,
                ResetToken = token,
                UserId = user.User.Id
            });
            _phoenixMembershipMock.Setup(p => p.GetUserNameByEmail(It.Is<string>(s => s == providedEmail))).Returns(string.Empty);

            Assert.Throws(Is.TypeOf<EntityNotFoundException>(), () => _accountsProvider.ResetPassword(token, providedEmail, "something"));
        }

        #endregion


        [Test]
        public void CreateArbitraryAccount_GivenCustomerAndUserAccountDoesNotExist_ShouldCreateCustomerAndUserAccount()
        {
            var customerName = "Tekno tikk takk";
            var distributorId = 1194;
            var email = "testuser@testing.com";

            var request = CreateArbitraryAccountRequest(customerName, distributorId, email);

            var response = _accountsProvider.CreateArbitraryAccount(request, LearningPortalSourceSystem);
            var user = _contextMock.Object.Users.First(u => u.Id == response.UserId);
            var customer = _contextMock.Object.Customers.First(c => c.Id == response.ArbitraryCustomerId);

            Assert.AreEqual(customerName, customer.Name);
            Assert.AreEqual(distributorId, customer.DistributorId);
            Assert.AreEqual(45, customer.LanguageId);

            Assert.AreEqual("firstname", user.FirstName);
            Assert.AreEqual("lastname", user.LastName);
            Assert.AreEqual(email, user.MembershipUserId);
            Assert.AreEqual(43, user.PreferredLanguageId);

        }

        [Test]
        public void CreateArbitraryAccount_GivenExistingUserIsAlreadyRelatedToADifferentArbitraryCustomer_ShouldCreateNewArbitraryCustomerAndChangeRelation()
        {
            var customerName = "Tekno tikk takk";
            var distributorId = 1194;
            var email = "testuser@testing.com";

            _usersRepositoryMock.Setup(u => u.FindByUsername(It.IsAny<string>())).Returns((string username) => _contextMock.Object.Users.FirstOrDefault(u => u.MembershipUserId == username));

            var request = CreateArbitraryAccountRequest(customerName, distributorId, email);

            var orgCustomer = new Customer {Id = GetNextId(), IsArbitrary = true, DistributorId = distributorId, CompanyRegistrationNumber = "999999"};
            _contextMock.Object.Customers.Add(orgCustomer);
            _contextMock.Object.Users.Add(new User {Id = 100, CustomerId = orgCustomer.Id, Customer = orgCustomer, MembershipUserId = email});


            var response = _accountsProvider.CreateArbitraryAccount(request, LearningPortalSourceSystem);
            var user = _contextMock.Object.Users.First(u => u.Id == response.UserId);
            var customer = _contextMock.Object.Customers.First(c => c.Id == response.ArbitraryCustomerId);

            Assert.AreNotEqual(orgCustomer.Id, user.CustomerId);
            Assert.AreEqual(customer.Id, user.CustomerId);
        }

        [Test]
        public void CreateArbitraryAccount_GivenExistingUserIsAlreadyRelatedToADifferentCorporateCustomer_ShouldCreateNewArbitraryCustomerButNotChangeRelation()
        {
            var customerName = "Tekno tikk takk";
            var distributorId = 1194;
            var email = "testuser@testing.com";

            _usersRepositoryMock.Setup(u => u.FindByUsername(It.IsAny<string>())).Returns((string username) => _contextMock.Object.Users.FirstOrDefault(u => u.MembershipUserId == username));

            var request = CreateArbitraryAccountRequest(customerName, distributorId, email);

            var orgCustomer = new Customer {Id = GetNextId(), IsArbitrary = false, DistributorId = distributorId, CompanyRegistrationNumber = "999999"};
            _contextMock.Object.Customers.Add(orgCustomer);
            var orgUser = new User {Id = 100, CustomerId = orgCustomer.Id, Customer = orgCustomer, MembershipUserId = email};
            _contextMock.Object.Users.Add(orgUser);
            membershipUsers.Add(new MembershipUser("SqlProvider", orgUser.MembershipUserId, Guid.NewGuid(), orgUser.MembershipUserId, "", "", true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.MinValue));


            var response = _accountsProvider.CreateArbitraryAccount(request, LearningPortalSourceSystem);
            var user = _contextMock.Object.Users.First(u => u.Id == response.UserId);
            var customer = _contextMock.Object.Customers.First(c => c.Id == response.ArbitraryCustomerId);

            Assert.AreNotEqual(orgCustomer.Id, customer.Id);
            Assert.AreEqual(orgCustomer.Id, user.CustomerId);
            Assert.AreNotEqual(customer.Id, user.CustomerId);
        }

        private ArbitraryAccountRequest CreateArbitraryAccountRequest(string customerName, int distributorId, string email)
        {
            return new ArbitraryAccountRequest
            {
                FirstName = "firstname",
                LastName = "lastname",
                City = "Oslo",
                CompanyName = customerName,
                CompanyRegistrationNumber = "1234113",
                Country = "NO",
                LanguageId = 45,
                DistributorId = distributorId,
                PreferredLanguageId = 43,
                Phone = "12345",
                Email = email,
                ZipCode = "1234",
                StreetAddress1 = "Street address 1"
            };
        }

        private UserAccount CreateUserAccount()
        {
            return CreateUserAccount("username@test.com", "useremail@test.com");
        }

        private UserAccount CreateUserAccount(string username, string email)
        {
            _distributorRepositoryMock.Setup(d => d.GetDistributors()).Returns(new List<Distributor> { new Distributor { Id = 1234 } });
            var userAccount = new UserAccount
            {
                Username = username,
                Email = email,
                User = new User
                {
                    Id = 12345,
                    Customer = new Customer { DistributorId = 1234 }
                }
            };
            _usersRepositoryMock.Setup(u => u.FindByUsername(userAccount.Username)).Returns(userAccount.User);

            var membershipUserMock = new Mock<MembershipUser>();
            membershipUserMock.Setup(m => m.UserName).Returns(userAccount.Username);
            membershipUserMock.Setup(m => m.Email).Returns(userAccount.Email);

            _phoenixMembershipMock.Setup(p => p.GetUser(It.Is<string>(u => u == userAccount.Username), It.IsAny<bool>())).Returns(membershipUserMock.Object);
            _phoenixMembershipMock.Setup(p => p.GetUserNameByEmail(It.Is<string>(s => s == userAccount.Email))).Returns(userAccount.Username);

            _passwordResetRequestMessageFactoryMock.Setup(
                u =>
                    u.CreatePasswordResetMessage(It.IsAny<Guid>(),
                        userAccount.Email,userAccount.User.Id, It.IsAny<string>())).Returns(new AutoMailMessage
                        {
                            MessageType = MessageType.PasswordResetRequest,
                            RecipientEmail = userAccount.Email
                        });

            return userAccount;
        }

        private int GetNextId()
        {
            return ++id;
        }

    }
}