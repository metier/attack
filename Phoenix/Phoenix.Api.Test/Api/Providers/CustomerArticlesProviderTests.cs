﻿using System.Linq;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data.Entities;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Providers
{
    [TestFixture]
    public class CustomerArticlesProviderTests
    {
        private Mock<ICustomerArticleRepository> _repositoryMock;
        private ContextMock _contextMock;
        private CustomerArticlesProvider _provider;
        
        [SetUp]
        public void Setup()
        {
            _repositoryMock = new Mock<ICustomerArticleRepository>();
            _contextMock = new ContextMock();

            _provider = new CustomerArticlesProvider(_repositoryMock.Object, _contextMock.Object);
        }


        [Test]
        public void GetOrderLine_MustSetArticleFromCustomerArticle()
        {
            _contextMock.Object.CustomerArticles.Add(CreateCustomerArticle());
            var customerArticle = _provider.Get(1);

            Assert.IsInstanceOf(typeof(CustomerArticle), customerArticle);
        }


        private CustomerArticle CreateCustomerArticle()
        {
            var productCategory = new ProductCategory { Id = 1, Name = "Project Management", ProductCodePrefix = "PM" };
            var product = new Product { Id = 1, ProductCategory = productCategory, ProductCategoryId = productCategory.Id, ProductNumber = "02" };
            var articleType = new ArticleType { Id = 1, Name = "E-learning", CharCode = "E" };
            var language = new Language { Id = 45, Name = "Norwegian", Identifier = "NO" };
            var article = new ElearningArticle { Id = 1, Product = product, ArticleType = articleType, ArticleTypeId = articleType.Id, Language = language, LanguageId = language.Id };

            var customerArticle = new CustomerArticle { Id = 1, ArticleCopy = article, ArticleCopyId = article.Id };

            _contextMock.Object.CustomerArticles.Add(customerArticle);
            return customerArticle;
        }


    }
}