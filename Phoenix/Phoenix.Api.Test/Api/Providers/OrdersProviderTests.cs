﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Factories;
using Metier.Phoenix.Api.Messaging;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Api.Services.ExternalCustomers;
using Metier.Phoenix.Api.Services.Invoices;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Exceptions;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Providers
{
    [TestFixture]
    public class OrdersProviderTests
    {
        private int _ids;

        private Mock<IOrderRepository> _orderRepositoryMock;
        private Mock<ICustomersProvider> _customersProviderMock;
        private Mock<IOrdersProviderResolver> _ordersProviderResolverMock;
        private Mock<IOrdersService> _ordersServiceMock;
        private Mock<IParticipantsProvider> _participantsProviderMock;
        private Mock<IActivityProvider> _activityProviderMock;
        private Mock<IOrderFactory> _orderFactoryMock;
        private Mock<IValidateOrder> _orderValidatorMock;
        private Mock<IExternalCustomersProviderFactory> _externalCustomersProviderFactoryMock;

        private ContextMock _contextMock;

        private IOrderInfoFactory _orderInfoFactory;
        private OrdersProvider _provider;

        [SetUp]
        public void Setup()
        {
            _orderRepositoryMock = new Mock<IOrderRepository>();
            _customersProviderMock = new Mock<ICustomersProvider>();
            _ordersProviderResolverMock = new Mock<IOrdersProviderResolver>();
            _ordersServiceMock = new Mock<IOrdersService>();
            _participantsProviderMock = new Mock<IParticipantsProvider>();
            _activityProviderMock = new Mock<IActivityProvider>();
            _orderFactoryMock = new Mock<IOrderFactory>();
            _orderValidatorMock = new Mock<IValidateOrder>();
            _externalCustomersProviderFactoryMock = new Mock<IExternalCustomersProviderFactory>();
            _contextMock = new ContextMock();

            _orderInfoFactory = new OrderInfoFactory(_contextMock.Object);

            _provider = new OrdersProvider(_orderRepositoryMock.Object, _customersProviderMock.Object,
                _ordersServiceMock.Object, _ordersProviderResolverMock.Object, _participantsProviderMock.Object,
                _activityProviderMock.Object, _orderFactoryMock.Object, _orderValidatorMock.Object,
                _contextMock.Object, _externalCustomersProviderFactoryMock.Object, _orderInfoFactory);

            _orderRepositoryMock.Setup(r => r.FindOrder(It.IsAny<int>())).Returns((int orderId) => _contextMock.Object.Orders.FirstOrDefault(o=> o.Id == orderId));
            _orderRepositoryMock.Setup(r => r.Update(It.IsAny<int>(), It.IsAny<Order>(), It.IsAny<bool>()))
                .Returns((int orderId, Order order, bool forceUpdate) => order);
            _orderRepositoryMock.Setup(r => r.AddParticipantsToOrder(It.IsAny<Order>(), It.IsAny<List<Participant>>()))
                .Returns((Order order, List<Participant> participants) => OrderRepository_AddParticipantsToOrder(order, participants));


            _orderRepositoryMock.Setup(r => r.Create(It.IsAny<Order>())).Returns((Order order) => order);

            _participantsProviderMock.Setup(p => p.GetParticipants(It.IsAny<int[]>(), It.IsAny<bool>()))
                .Returns((int[] participantIds, bool includeDeletedParticipants) => _contextMock.Object.Participants.Where(p => participantIds.Contains(p.Id) && (includeDeletedParticipants || (p.IsDeleted == false))).ToList());
        }


        private Order OrderRepository_AddParticipantsToOrder(Order order, List<Participant> participants)
        {
            foreach (var participant in participants)
            {
                order.ParticipantOrderLines.Add(
                    new ParticipantOrderLine
                    {
                        ActivityId = participant.ActivityId,
                        Participant = participant,
                    }
                    );

            }

            return order;
        }

        /*
         * CreditOrder
         * CreateCreditOrderDraft
         * RemoveOrderlinesFromOrder
         * Create
         * Update
         * InvoiceByOrderId
         */

        #region GetOrderInfo

        [Test]
        [TestCase(OrderStatus.Transferred)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Invoiced)]
        [TestCase(OrderStatus.Rejected)]
        public void GetOrderInfo_GivenInvoice_ShouldRetrieveOrderLineDataFromParticipantOrderLines(OrderStatus orderStatus)
        {
            var customer = CreateCustomer();
            var user = CreateUser(customer);
            var activity = CreateActivity();
            var participant = CreateParticipant(user, activity);
            var order = CreateOrder(customer, participant, activity, orderStatus);
            var infoText = participant.GetInfoTexts();

            user.FirstName = "Changed Firstname";
            user.LastName = "Changed Lastname";
            participant.UnitPrice = participant.UnitPrice + 220;
            participant.CurrencyCodeId = participant.CurrencyCodeId + 3;
            activity.Name = "Changed activityname";

            var orderInfo = _provider.GetOrderInfo(order.Id);
            var participantOrderLine = orderInfo.OrderLines.First(ol => ol.Type == OrderLineType.Participant && ol.ParticipantId.Equals(participant.Id));
            var headerOrderLine = orderInfo.OrderLines.First(ol => ol.Id == activity.Id);

            Assert.AreEqual(1000, participantOrderLine.Price);
            Assert.AreEqual(262, participantOrderLine.CurrencyCodeId);
            Assert.AreEqual(infoText, participantOrderLine.Text);
            
            //The names of the activities are not stored on each participantorderline, so any changes to these will be reflected on the invoice.
            //Could be improved, but this rarely occurs so it's not a huge problem.
            Assert.AreEqual("Changed activityname", headerOrderLine.Text);
        }

        [Test]
        [TestCase(OrderStatus.Draft)]
        [TestCase(OrderStatus.Proposed)]
        public void GetOrderInfo_GivenInvoice_ShouldRetrieveOrderLineDataFromParticipant(OrderStatus orderStatus)
        {
            var customer = CreateCustomer();
            var user = CreateUser(customer);
            var activity = CreateActivity();
            var participant = CreateParticipant(user, activity);
            var order = CreateOrder(customer, participant, activity, orderStatus);

            user.FirstName = "Changed Firstname";
            user.LastName = "Changed Lastname";
            participant.UnitPrice = participant.UnitPrice + 220;
            participant.CurrencyCodeId = participant.CurrencyCodeId + 3;
            activity.Name = "Changed activityname";

            var orderInfo = _provider.GetOrderInfo(order.Id);
            var participantOrderLine = orderInfo.OrderLines.First(ol => ol.Type == OrderLineType.Participant && ol.ParticipantId.Equals(participant.Id));
            var headerOrderLine = orderInfo.OrderLines.First(ol => ol.Id == activity.Id);

            Assert.AreEqual(1220, participantOrderLine.Price);
            Assert.AreEqual(265, participantOrderLine.CurrencyCodeId);
            Assert.AreEqual(participant.GetInfoTexts(), participantOrderLine.Text);
            Assert.AreEqual("Changed activityname", headerOrderLine.Text);
        }

        [Test]
        [TestCase(OrderStatus.Draft)]
        [TestCase(OrderStatus.Transferred)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Invoiced)]
        [TestCase(OrderStatus.Rejected)]
        public void GetOrderInfo_GivenCreditNote_ShouldRetrieveOrderLineDataFromParticipantOrderLines(OrderStatus orderStatus)
        {
            var customer = CreateCustomer();
            var user = CreateUser(customer);
            var activity = CreateActivity();
            var participant = CreateParticipant(user, activity);
            var order = CreateOrder(customer, participant, activity, orderStatus);
            var infoText = participant.GetInfoTexts();

            order.IsCreditNote = true;

            user.FirstName = "Changed Firstname";
            user.LastName = "Changed Lastname";
            participant.UnitPrice = participant.UnitPrice + 220;
            participant.CurrencyCodeId = participant.CurrencyCodeId + 3;
            activity.Name = "Changed activityname";

            var orderInfo = _provider.GetOrderInfo(order.Id);
            var participantOrderLine = orderInfo.OrderLines.First(ol => ol.Type == OrderLineType.Participant && ol.ParticipantId.Equals(participant.Id));
            var headerOrderLine = orderInfo.OrderLines.First(ol => ol.Id == activity.Id);

            Assert.AreEqual(1000, participantOrderLine.Price);
            Assert.AreEqual(262, participantOrderLine.CurrencyCodeId);
            Assert.AreEqual(infoText, participantOrderLine.Text);

            //The names of the activities are not stored on each participantorderline, so any changes to these will be reflected on the invoice.
            //Could be improved, but this rarely occurs so it's not a huge problem.
            Assert.AreEqual("Changed activityname", headerOrderLine.Text);
        }

        #endregion

        #region Reset order

        [Test]
        public void ResetOrder_GivenInvoice_ShouldClearOrderLineData()
        {
            var customer = CreateCustomer();
            var user = CreateUser(customer);
            var activity = CreateActivity();
            var participant = CreateParticipant(user, activity);
            var order = CreateOrder(customer, participant, activity, OrderStatus.Rejected);

            _provider.ResetOrder(order.Id);

            Assert.IsNull(order.ParticipantOrderLines.First().Price);
            Assert.IsNull(order.ParticipantOrderLines.First().CurrencyCodeId);
            Assert.AreEqual(string.Empty, order.ParticipantOrderLines.First().ActivityName);
            Assert.AreEqual(string.Empty, order.ParticipantOrderLines.First().OnlinePaymentReference);
            Assert.AreEqual(string.Empty, order.ParticipantOrderLines.First().InfoText);

            Assert.AreEqual(OrderStatus.Draft, order.Status);
        }

        [Test]
        public void ResetOrder_GivenCreditNote_ShouldNotClearOrderLineData()
        {
            var customer = CreateCustomer();
            var user = CreateUser(customer);
            var activity = CreateActivity();
            var participant = CreateParticipant(user, activity);
            var order = CreateOrder(customer, participant, activity, OrderStatus.Rejected);

            order.IsCreditNote = true;
            _provider.ResetOrder(order.Id);

            Assert.AreEqual(participant.UnitPrice, order.ParticipantOrderLines.First().Price);
            Assert.AreEqual(participant.CurrencyCodeId, order.ParticipantOrderLines.First().CurrencyCodeId);
            Assert.AreEqual(participant.OnlinePaymentReference, order.ParticipantOrderLines.First().OnlinePaymentReference);
            Assert.AreEqual(participant.GetInfoTexts(), order.ParticipantOrderLines.First().InfoText);
            Assert.AreEqual(activity.Name, order.ParticipantOrderLines.First().ActivityName);

            Assert.AreEqual(OrderStatus.Draft, order.Status);
        }

        [Test]
        public void ResetOrder_GivenCreditNoteCreatedBeforeOrderWasRejected_ShouldThrowValidationException()
        {
            var customer = CreateCustomer();
            var user = CreateUser(customer);
            var activity = CreateActivity();
            var participant = CreateParticipant(user, activity);
            var rejectedOrder = CreateOrder(customer, participant, activity, OrderStatus.Rejected);
            var creditNote = CreateOrder(customer, participant, activity, OrderStatus.Draft);
            creditNote.IsCreditNote = true;

            Assert.Throws(Is.TypeOf<ValidationException>(), () => _provider.ResetOrder(rejectedOrder.Id));
        }

        [Test]
        public void ResetOrder_GivenNewInvoiceCreatedBeforeCreditNoteWasRejected_ShouldThrowValidationException()
        {
            var customer = CreateCustomer();
            var user = CreateUser(customer);
            var activity = CreateActivity();
            var participant = CreateParticipant(user, activity);
            var rejectedCreditNote = CreateOrder(customer, participant, activity, OrderStatus.Rejected);

            CreateOrder(customer, participant, activity, OrderStatus.Draft);
            rejectedCreditNote.IsCreditNote = true;

            Assert.Throws(Is.TypeOf<ValidationException>(), () => _provider.ResetOrder(rejectedCreditNote.Id));
        }

        #endregion

        #region AddParticipantsToOrder

        //Only allow adding participants from same customer
        [Test]
        public void AddParticipantsToOrder_GivenParticipationFromDifferentCustomer_ShouldReturnException()
        {
            var customer = CreateCustomer();
            var participant = CreateParticipant();
            var activity = _contextMock.Object.Activities.First(a => a.Id == participant.ActivityId);
            var order = CreateOrder(customer, participant, activity, OrderStatus.Draft);

            Assert.Throws(Is.TypeOf<ValidationException>(), () => _provider.AddParticipantsToOrder(order.Id, new[] {participant.Id}));
        }

        //Only allow adding participants when in Draft mode
        [Test]
        [TestCase(OrderStatus.Proposed)]
        [TestCase(OrderStatus.Transferred)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Invoiced)]
        [TestCase(OrderStatus.Rejected)]
        public void AddParticipantsToOrder_GivenOrderIsNotDraft_ShouldReturnException(OrderStatus orderStatus)
        {
            var participant = CreateParticipant();
            var customer = _contextMock.Object.Customers.First(c => c.Id == participant.CustomerId);
            var activity = _contextMock.Object.Activities.First(a => a.Id == participant.ActivityId);
            var order = CreateOrder(customer, participant, activity, orderStatus);

            Assert.Throws(Is.TypeOf<ValidationException>(), () => _provider.AddParticipantsToOrder(order.Id, new[] { participant.Id }));
        }


        //only allow addingparticipants on orders in the draft state
        [Test]
        public void AddParticipantsToOrder_GivenOrderIsDraft_ShouldAddParticipants()
        {
            var participant = CreateParticipant();
            var customer = _contextMock.Object.Customers.First(c => c.Id == participant.CustomerId);
            var activity = _contextMock.Object.Activities.First(a => a.Id == participant.ActivityId);

            var participantToAdd = CreateParticipant();
            participantToAdd.CustomerId = customer.Id;

            var order = CreateOrder(customer, participant, activity, OrderStatus.Draft);

            Assert.DoesNotThrow(() => _provider.AddParticipantsToOrder(order.Id, new[] { participant.Id }));
            Assert.AreEqual(2, order.ParticipantOrderLines.Count);
        }

        [Test]
        public void AddParticipantsToOrder_GivenParticipantExistsOnOrder_ShouldReturnException()
        {
            var participant = CreateParticipant();
            var customer = _contextMock.Object.Customers.First(c => c.Id == participant.CustomerId);
            var activity = _contextMock.Object.Activities.First(a => a.Id == participant.ActivityId);
            var order = CreateOrder(customer, participant, activity, OrderStatus.Invoiced);
            var newOrder = CreateOrder(customer, participant, activity, OrderStatus.Draft);

            Assert.Throws(Is.TypeOf<ValidationException>(), () => _provider.AddParticipantsToOrder(newOrder.Id, new[] { participant.Id }));
        }

        [Test]
        [TestCase(OrderStatus.Transferred)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Invoiced)]
        public void AddParticipantsToOrder_GivenParticipantExistsOnCreditedOrder_ShouldAddParticipantToOrder(OrderStatus orderStatus)
        {
            var participant = CreateParticipant();
            var dummyParticipant = CreateParticipant(); //Need this to create the new order, although not used for anythying...
            dummyParticipant.CustomerId = participant.CustomerId;

            var customer = _contextMock.Object.Customers.First(c => c.Id == participant.CustomerId);
            var activity = _contextMock.Object.Activities.First(a => a.Id == participant.ActivityId);
            var creditNote = CreateOrder(customer, participant, activity, orderStatus);
            var newOrder = CreateOrder(customer, dummyParticipant, activity, OrderStatus.Draft);

            creditNote.IsCreditNote = true;

            _provider.AddParticipantsToOrder(newOrder.Id, new[] {participant.Id});

            Assert.AreEqual(2, newOrder.ParticipantOrderLines.Count);
        }

        [Test]
        [TestCase(OrderStatus.Draft)]
        [TestCase(OrderStatus.Rejected)]
        public void AddParticipantsToOrder_GivenParticipantExistsOnCreditedOrder_ShouldThrowException(OrderStatus orderStatus)
        {
            var participant = CreateParticipant();
            var dummyParticipant = CreateParticipant(); //Need this to create the new order, although not used for anythying...
            dummyParticipant.CustomerId = participant.CustomerId;

            var customer = _contextMock.Object.Customers.First(c => c.Id == participant.CustomerId);
            var activity = _contextMock.Object.Activities.First(a => a.Id == participant.ActivityId);
            var creditNote = CreateOrder(customer, participant, activity, orderStatus);
            var newOrder = CreateOrder(customer, dummyParticipant, activity, OrderStatus.Draft);

            creditNote.IsCreditNote = true;

            Assert.Throws(Is.TypeOf<ValidationException>(), () => _provider.AddParticipantsToOrder(newOrder.Id, new[] { participant.Id }));
        }

        #endregion


        #region Supporting functions

        private User CreateUser(Customer customer)
        {
            var user = new User
            {
                Id = GetNextId(),
                FirstName = "Firstname",
                LastName = "Lastname",
                Customer = customer,
                CustomerId = customer.Id
            };

            _contextMock.Object.Users.Add(user);

            return user;
        }

        private Activity CreateActivity(CustomerArticle customerArticle = null)
        {
            if (customerArticle == null) customerArticle = CreateCustomerArticle();
            var activity = new Activity { Id = GetNextId(), CustomerArticleId = customerArticle.Id };
            CreateSearchableActivity(activity, customerArticle);

            _contextMock.Object.Activities.Add(activity);

            return activity;
        }

        private CustomerArticle CreateCustomerArticle()
        {
            var customerArticle = new CustomerArticle { Id = GetNextId() };
            _contextMock.Object.CustomerArticles.Add(customerArticle);
            return customerArticle;
        }

        private Participant CreateParticipant(User user = null, Activity activity = null)
        {
            var customer = CreateCustomer();
            customer.CustomLeadingTexts = CreateCustomLeadingTexts(customer);

            if (user == null) user = CreateUser(customer);

            if (activity == null) activity = CreateActivity();

            var participant = new Participant
            {
                Id = GetNextId(),
                User = user,
                CustomerId = customer.Id,
                UserId = user.Id,
                ActivityId = activity.Id,
                UnitPrice = 1000,
                CurrencyCodeId = 262,
                OnlinePaymentReference = "opr"
            };
            participant.ParticipantInfoElements = CreateParticipantInfoElements(participant);
            MapParticipantInfoElementsToCustomLeadingTexts(participant.ParticipantInfoElements, customer.CustomLeadingTexts);

            _contextMock.Object.Participants.Add(participant);
            _contextMock.Object.Activities.Add(activity);
            _contextMock.Object.Users.Add(user);

            return participant;
        }

        private List<ParticipantInfoElement> CreateParticipantInfoElements(Participant participant)
        {
            var elements = new List<ParticipantInfoElement>
            {
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participant.Id, InfoText = "", LeadingTextId = 11, LeadingText = new LeadingText {Id = 11, Text = "Custom Field 1", IsVisible = true, CustomLeadingTexts = CreateCustomLeadingTexts(participant)}},
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participant.Id, InfoText = "", LeadingTextId = 12, LeadingText = new LeadingText {Id = 12, Text = "Custom Field 2", IsVisible = true, CustomLeadingTexts = CreateCustomLeadingTexts(participant)}},
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participant.Id, InfoText = "", LeadingTextId = 13, LeadingText = new LeadingText {Id = 13, Text = "Employee Id", IsVisible = true, CustomLeadingTexts = CreateCustomLeadingTexts(participant)}},
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participant.Id, InfoText = "", LeadingTextId = 14, LeadingText = new LeadingText {Id = 14, Text = "Department", IsVisible = true, CustomLeadingTexts = CreateCustomLeadingTexts(participant)}},
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participant.Id, InfoText = "", LeadingTextId = 15, LeadingText = new LeadingText {Id = 15, Text = "Position", IsVisible = true, CustomLeadingTexts = CreateCustomLeadingTexts(participant)}},
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participant.Id, InfoText = "", LeadingTextId = 16, LeadingText = new LeadingText {Id = 16, Text = "Supervisor", IsVisible = true, CustomLeadingTexts = CreateCustomLeadingTexts(participant)}},
                new ParticipantInfoElement {Id = GetNextId(), ParticipantId = participant.Id, InfoText = "", LeadingTextId = 17, LeadingText = new LeadingText {Id = 17, Text = "Confirmation Email", IsVisible = true, CustomLeadingTexts = CreateCustomLeadingTexts(participant)}}
            };

            return elements;
        }

        private List<CustomLeadingText> CreateCustomLeadingTexts(Participant participant)
        {
            return new List<CustomLeadingText> {new CustomLeadingText {CustomerId = participant.CustomerId, IsVisibleInvoiceLine = false}};
        }
        
        private Customer CreateCustomer()
        {
            var customer = new Customer
            {
                Id = GetNextId(),
                Name = "Test customer"
            };

            _contextMock.Object.Customers.Add(customer);
            return customer;
        }

        private Order CreateOrder(Customer customer, Participant participant, Activity activity, OrderStatus orderStatus)
        {
            var order = new Order
            {
                Id = GetNextId(),
                Customer = customer,
                CustomerId = customer.Id,
                IsCreditNote = false,
                IsFullCreditNote = false,
                Status = orderStatus,
                ParticipantOrderLines = new List<ParticipantOrderLine>(),
                ActivityOrderLines = new List<ActivityOrderLine>()
            };


            order.ParticipantOrderLines.Add(CreateParticipantOrderLine(participant, activity, order));
            _contextMock.Object.Orders.Add(order);

            return order;
        }


        private ParticipantOrderLine CreateParticipantOrderLine(Participant participant, Activity activity, Order order)
        {
            var participantOrderLine = new ParticipantOrderLine
            {
                Id= GetNextId(),
                ActivityId = participant.ActivityId,
                ActivityName = activity.Name,
                CurrencyCodeId = participant.CurrencyCodeId,
                InfoText = participant.GetInfoTexts(),
                OnlinePaymentReference = participant.OnlinePaymentReference,
                Participant = participant,
                Price = participant.UnitPrice,
                Order = order
            };

            _contextMock.Object.ParticipantOrderLines.Add(participantOrderLine);
            return participantOrderLine;
        }

        private List<CustomLeadingText> CreateCustomLeadingTexts(Customer customer)
        {
            var list = new List<CustomLeadingText>
            {
                new CustomLeadingText{Customer = customer,CustomerId = customer.Id, LeadingTextId = 11, IsVisibleInvoiceLine = true, IsVisible = true, Text = "leadingtext"},
                new CustomLeadingText{Customer = customer,CustomerId = customer.Id, LeadingTextId = 12, IsVisibleInvoiceLine = true, IsVisible = true, Text = "leadingtext"},
                new CustomLeadingText{Customer = customer,CustomerId = customer.Id, LeadingTextId = 13, IsVisibleInvoiceLine = true, IsVisible = true, Text = "leadingtext"},
                new CustomLeadingText{Customer = customer,CustomerId = customer.Id, LeadingTextId = 14, IsVisibleInvoiceLine = true, IsVisible = true, Text = "leadingtext"},
                new CustomLeadingText{Customer = customer,CustomerId = customer.Id, LeadingTextId = 15, IsVisibleInvoiceLine = true, IsVisible = true, Text = "leadingtext"},
                new CustomLeadingText{Customer = customer,CustomerId = customer.Id, LeadingTextId = 16, IsVisibleInvoiceLine = true, IsVisible = true, Text = "leadingtext"},
                new CustomLeadingText{Customer = customer,CustomerId = customer.Id, LeadingTextId = 17, IsVisibleInvoiceLine = true, IsVisible = true, Text = "leadingtext"}

            };

            return list;
        }

        private void MapParticipantInfoElementsToCustomLeadingTexts(List<ParticipantInfoElement> participantInfoElements, List<CustomLeadingText> customLeadingTexts)
        {
            foreach (var element in participantInfoElements)
            {
                var customLeadingText = customLeadingTexts.FirstOrDefault(clt => clt.LeadingTextId == element.LeadingTextId);

                if (customLeadingText != null)
                    customLeadingText.LeadingText = element.LeadingText;

                element.LeadingText.CustomLeadingTexts = customLeadingTexts.FindAll(clt => clt.LeadingTextId == element.LeadingTextId);
            }
        }

        private void CreateSearchableActivity(Activity activity, CustomerArticle customerArticle)
        {
            if (activity.CustomerArticleId != customerArticle.Id)
                throw new Exception("Mismatching id's between the CustomerArticleId of the activity and the customerArticle.id.");

            var searchableActivity = new SearchableActivityWithActivitySet
            {
                ActivityId = activity.Id,
                CustomerArticleId = customerArticle.Id,
                ArticlePath = customerArticle.ArticlePath
            };

            _contextMock.Object.SearchableActivities.Add(searchableActivity);
        }

        private int GetNextId()
        {
            return ++_ids;
        }

        #endregion
    }
}