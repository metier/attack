﻿using System;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Api.Facade.Account;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Services.ExternalCustomers;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Exceptions;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.UnitTests.Api.Providers
{
    [TestFixture]
    public class CustomersProviderTests
    {
        private ICustomersProvider _provider;

        private Mock<ICustomerRepository> _customerRepositoryMock;
        private ContextMock _contextMock;

        [SetUp]
        public void SetUp()
        {
            _customerRepositoryMock = new Mock<ICustomerRepository>();
            _contextMock = new ContextMock();

            //_customerRepositoryMock.Setup(m => m.Create(It.IsAny<Customer>())).Returns(CreateCustomer());
            _customerRepositoryMock.Setup(m => m.FindById(It.IsAny<int>())).Returns((int customerId) => _contextMock.Object.Customers.FirstOrDefault(c => c.Id == customerId));
            _customerRepositoryMock.Setup(m => m.Delete(It.IsAny<Customer>())).Returns((Customer customer) => DeleteCustomer(customer));

            _provider = new CustomersProvider(_contextMock.Object,
                                              _customerRepositoryMock.Object);
        }

        //private Customer CreateCustomer(Customer customer)
        //{
        //    return customer;
        //}

        private bool DeleteCustomer(Customer customer)
        {
            _contextMock.Object.Customers.Remove(customer);
            return true;
        }

        [Test]
        [TestCase(0)]
        [TestCase(null)]
        public void CreateArbitraryCustomer_GivenDistributorIdIsNotSet_ShouldThrowException(int? distributorId)
        {
            const string expectedExceptionMessage = "DistributorId is not set";
            
            var userAccount = ValidUserAccount();
            userAccount.DistributorId = distributorId;
            Assert.Throws<ValidationException>(() => _provider.CreateArbitraryCustomer(userAccount), expectedExceptionMessage);
        }
        
        [Test]
        public void CreateArbitraryCustomer_GivenCustomerIdIsSet_ShouldThrowException()
        {
            const string expectedExceptionMessage = "CustomerId is set. Arbitrary customers cannot be associated with another customer";
            
            var userAccount = ValidUserAccount();
            userAccount.CustomerId = 1;

            Assert.Throws<ValidationException>(() => _provider.CreateArbitraryCustomer(userAccount), expectedExceptionMessage);
        }

        [Test]
        public void CreateCustomer_GivenMatchingCompanyRegistrationNumbers_ShouldNotInsertCustomer()
        {
            _contextMock.Object.Customers.Add(validCustomer(true));
            var customer = validCustomer(true);
            customer.Id = 0;
            customer.Name = "Changed name";
            customer.Addresses[0].AddressStreet1 = "Changed street address 1";
            customer.Addresses[0].City = "Changed city";

            _customerRepositoryMock.Setup(m => m.Create(It.IsAny<Customer>()))
                .Callback<Customer>(c => _contextMock.Object.Customers.Add(c))
                .Returns(customer);

            var createdCustomer = _provider.Create(customer);

            Assert.IsNotNull(createdCustomer);
            Assert.AreEqual(1, createdCustomer.Id);
            Assert.AreEqual(1, _contextMock.Object.Customers.Count(), "Only one customer should exist in the database!");
        }

        [Test]
        public void CreateCustomer_GivenMissingCompanyRegistrationNumberOnNewCustomer_ShouldNotInsertCustomer()
        {
            _contextMock.Object.Customers.Add(validCustomer(true));
            var customer = validCustomer(true);
            customer.Id = 0;
            customer.CompanyRegistrationNumber = string.Empty;

            _customerRepositoryMock.Setup(m => m.Create(It.IsAny<Customer>()))
                .Callback<Customer>(c => _contextMock.Object.Customers.Add(c))
                .Returns(customer);

            var foundCustomer = _provider.Create(customer);

            Assert.IsNotNull(foundCustomer);
            Assert.AreEqual(1, foundCustomer.Id);
            Assert.AreEqual(1, _contextMock.Object.Customers.Count(), "Only one customer should exist in the database!");      
        }

        [Test]
        public void CreateCustomer_GivenMatchingCustomersButDifferentCompanyRegistrationNumbers_ShouldInsertCustomer()
        {

            _contextMock.Object.Customers.Add(validCustomer(true));
            var customer = validCustomer(true);
            customer.Id = 0;
            customer.CompanyRegistrationNumber = "123411";

            _customerRepositoryMock.Setup(m => m.Create(It.IsAny<Customer>()))
                .Callback<Customer>(c => _contextMock.Object.Customers.Add(c))
                .Returns(customer);

            var foundCustomer = _provider.Create(customer);

            Assert.IsNotNull(foundCustomer);
            Assert.AreNotEqual(1, foundCustomer.Id);
            Assert.Greater(_contextMock.Object.Customers.Count(), 1, "More than one customer should exist in the database!");      
        }

        [Test]
        public void CreateCustomer_GivenLeadingOrTrailingSpacesInPostalAdress_ShouldRemoveSpacesAndInsertCustomer()
        {

            var customer = validCustomer(true);
            customer.Id = 0;
            customer.Addresses.First().AddressStreet1 = "    Sleiverudåsen 12 ";
            customer.Addresses.First().AddressStreet2 = "Oppgang 2    ";
            customer.Addresses.First().ZipCode = "    1339";
            customer.Addresses.First().City = "    Oslo   ";
            customer.Addresses.First().Country = "    NO   ";

            _customerRepositoryMock.Setup(m => m.Create(It.IsAny<Customer>()))
                .Callback<Customer>(c => _contextMock.Object.Customers.Add(c))
                .Returns(customer);

            var foundCustomer = _provider.Create(customer);

            Assert.IsNotNull(foundCustomer);

            Assert.IsTrue(foundCustomer.Addresses.First().AddressStreet1.StartsWith("Sleiverudåsen 12"));
            Assert.IsTrue(foundCustomer.Addresses.First().AddressStreet1.EndsWith(" 12"));
            Assert.IsTrue(foundCustomer.Addresses.First().AddressStreet2.EndsWith("Oppgang 2"));
            Assert.IsTrue(foundCustomer.Addresses.First().ZipCode.EndsWith("1339"));
            Assert.IsTrue(foundCustomer.Addresses.First().City.StartsWith("Oslo"));
            Assert.IsTrue(foundCustomer.Addresses.First().City.EndsWith("o"));
            Assert.IsTrue(foundCustomer.Addresses.First().Country.StartsWith("NO"));
        }

        [Test]
        public void DeleteCustomer_GivenValidCustomer_ShouldDeleteCustomer()
        {
            var customer = validCustomer(true);
            customer.ExternalCustomerId = null;
            _contextMock.Object.Customers.Add(customer);

            Assert.IsNotNull(_provider.GetCustomer(customer.Id));   //Making sure the customer exists in the context.

            Assert.IsTrue(_provider.Delete(customer.Id));

            var deleted = _contextMock.Object.Customers.FirstOrDefault(c => c.Id == customer.Id);
            Assert.IsNull(deleted);

            deleted = _provider.GetCustomer(customer.Id);
            Assert.IsNull(deleted);
        }

        [Test]
        public void DeleteCustomer_GivenInvalidCustomer_ShouldReturnValidationErrors()
        {
            var customer = validCustomer(true);
            customer.ExternalCustomerId = "1234";

            _contextMock.Object.Users.Add(new User {Id = 1, CustomerId = customer.Id});
            _contextMock.Object.Participants.Add(new Participant {Id = 1, CustomerId = customer.Id});
            _contextMock.Object.Orders.Add(new Order {Id = 1, CustomerId = customer.Id});
            _contextMock.Object.CoursePrograms.Add(new CourseProgram {Id = 1, CustomerId = customer.Id});
            _contextMock.Object.ActivitySets.Add(new ActivitySet {Id = 1, CustomerId = customer.Id});
            _contextMock.Object.ActivitySetShares.Add(new ActivitySetShare {ActivitySetId = 1, CustomerId = customer.Id});
            _contextMock.Object.CustomerArticles.Add(new CustomerArticle { Id = 1, CustomerId = customer.Id });
            _contextMock.Object.ProductDescriptions.Add(new ProductDescription { Id = 1, CustomerId = customer.Id });
            _contextMock.Object.EnrollmentConditions.Add(new EnrollmentCondition { Id = 1, CustomerId = customer.Id });
            //_contextMock.Object.CustomerArticleEnrollmentConditions.Add(new CustomerArticleEnrollmentCondition { Id = 1, CustomerId = customer.Id }); //TODO: Uncomment when FLMS-462 is merged
            _contextMock.Object.Questions.Add(new Question { Id = 1, CustomerId = customer.Id });

            _contextMock.Object.Customers.Add(customer);

            try
            {
                _provider.Delete(customer.Id);
            }
            catch (ValidationException e)
            {
                Assert.AreEqual("Could not delete customer", e.Message);
                Assert.AreEqual("Customer has reference to account in external accounting system.", e.ValidationResults?.ToList()[0]?.ErrorMessage);
                Assert.AreEqual("Customer must not contain any users.", e.ValidationResults?.ToList()[1]?.ErrorMessage);
                Assert.AreEqual("Customer must not have any related participations.", e.ValidationResults?.ToList()[2]?.ErrorMessage);
                Assert.AreEqual("Customer must not have any related orders.", e.ValidationResults?.ToList()[3]?.ErrorMessage);
                Assert.AreEqual("Customer must not have any course programs.", e.ValidationResults?.ToList()[4]?.ErrorMessage);
                Assert.AreEqual("Customer must not have any activitysets.", e.ValidationResults?.ToList()[5]?.ErrorMessage);
                Assert.AreEqual("Customer must not have any shared activitysets.", e.ValidationResults?.ToList()[6]?.ErrorMessage);
                Assert.AreEqual("Customer must not have any customer articles.", e.ValidationResults?.ToList()[7]?.ErrorMessage);
                Assert.AreEqual("Customer must not have any related product descriptions.", e.ValidationResults?.ToList()[8]?.ErrorMessage);
                Assert.AreEqual("Customer must not have any related enrollment conditions.", e.ValidationResults?.ToList()[9]?.ErrorMessage);
                //Assert.AreEqual("Customer must not have any related enrollment conditions on customer articles.", e.ValidationResults?.ToList()[10]?.ErrorMessage); //TODO: Uncomment when FLMS-462 is merged
                Assert.AreEqual("Customer must not have any custom exam questions.", e.ValidationResults?.ToList()[10]?.ErrorMessage);
            }
        }

        private Customer validCustomer(bool isArbitrary)
        {
            return new Customer
            {
                Id = 1,
                Name = "testcustomer as",
                DistributorId = 1194,
                ExternalCustomerId = "1000",
                CompanyRegistrationNumber = "98989898",
                IsArbitrary = isArbitrary,
                LanguageId = 45,
                IsRoot = true,
                Addresses = new List<CustomerAddress>
                {
                    new CustomerAddress
                    {
                        AddressType = AddressType.Invoice,
                        AddressStreet1 = "street 1",
                        AddressStreet2 = "street 2",
                        ZipCode = "0302",
                        City = "Oslo",
                        Country = "NO"
                    }
                }
            };
        }


        private UserAccount ValidUserAccount()
        {
            return new UserAccount
            {
                CustomerId = 0,
                DistributorId = 1
            };
        }
    }
}