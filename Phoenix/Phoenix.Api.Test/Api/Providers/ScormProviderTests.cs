﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Messaging;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Exceptions;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Providers
{
    [TestFixture]
    public class ScormProviderTests
    {
        private int _ids;

        private Mock<IPerformanceProvider> _performanceProviderMock;
        private Mock<IScormAttemptRepository> _scormAttemptRepositoryMock;
        private Mock<IAuthorizationProvider> _authorizationProviderMock;
        private Mock<IEventAggregator> _eventAggregatorMock;
        private ContextMock _contextMock;

        private ScormProvider _provider;

        [SetUp]
        public void Setup()
        {
            _eventAggregatorMock = new Mock<IEventAggregator>();
            _scormAttemptRepositoryMock = new Mock<IScormAttemptRepository>();
            // Return whatever is passed in:
            _scormAttemptRepositoryMock.Setup(r => r.Create(It.IsAny<ScormAttempt>())).Returns((ScormAttempt a) =>
            {
                _contextMock.Object.ScormAttempts.Add(a);
                return a;
            });

            _performanceProviderMock = new Mock<IPerformanceProvider>();
            // Return whatever is passed in:
            _performanceProviderMock.Setup(r => r.Create(It.IsAny<ScormPerformance>())).Returns((ScormPerformance p) =>
            {
                _contextMock.Object.ScormPerformances.Add(p);
                return p;
            });

            _contextMock = new ContextMock();

            _authorizationProviderMock = new Mock<IAuthorizationProvider>();
            _authorizationProviderMock.Setup(a => a.IsAdminOr(It.IsAny<int>())).Returns(true);
            _provider = new ScormProvider(_contextMock.Object, 
                                          _performanceProviderMock.Object, 
                                          _scormAttemptRepositoryMock.Object, 
                                          _eventAggregatorMock.Object, 
                                          _authorizationProviderMock.Object);
        }

        [Test]
        public void CreateSession_ShouldReturnCreatedAttempt()
        {
            var user = CreateUser();
            var rco = CreateRco();

            var attempt = _provider.CreateSession(user.Id, rco.Id);

            Assert.AreEqual(user.Id, attempt.UserId);
            Assert.AreEqual(ScormAttemptStatus.Active, attempt.InternalState);
            Assert.AreEqual(ScormLessonStatus.NotAttempted, attempt.Status);
        }

        [Test]
        public void CreateSession_GivenUserIdAndRcoIdForExistingPerformance_ShouldUseExistingPerformance()
        {
            var user = CreateUser();
            var rco = CreateRco();

            var performance = CreatePerformance();
            performance.UserId = user.Id;
            performance.RcoId = rco.Id;

            var attempt = _provider.CreateSession(user.Id, rco.Id);

            Assert.AreEqual(performance.Id, attempt.PerformanceId);
        }
        
        [Test]
        public void CreateSession_GivenNewUserIdAndRcoId_ShouldCreateNewPerformance()
        {
            var user = CreateUser();
            var rco = CreateRco();
            
            var attempt = _provider.CreateSession(user.Id, rco.Id);

            Assert.AreEqual(attempt.PerformanceId, _contextMock.Object.ScormPerformances.First().Id);
        }
 
        [Test]
        public void CreateSession_GivenNewUserIdAndRcoId_ShouldCreateCoursePerformance()
        {
            var user = CreateUser();
            var rco = CreateRco();
            
            _provider.CreateSession(user.Id, rco.Id);

            _performanceProviderMock.Verify(p => p.CreateParentPerformanceIfNotExists(user.Id, rco.Id), Times.Once);
        }

        [Test]
        public void CreateSession_ShouldCopyStatusAndSuspendDataFromMostRecentAttemptForPerformance()
        {
            var user = CreateUser();
            var rco = CreateRco();
            var performance = CreatePerformance();
            performance.UserId = user.Id;
            performance.RcoId = rco.Id;

            var attempt1 = CreateAttempt(performance.Id);
            attempt1.Status = ScormLessonStatus.Passed;
            attempt1.SuspendData = "data1";

            var attempt2 = CreateAttempt(performance.Id);
            attempt2.Status = ScormLessonStatus.Failed;
            attempt2.SuspendData = "data2";

            var attempt = _provider.CreateSession(performance.UserId, performance.RcoId);

            Assert.AreEqual(attempt2.Status, attempt.Status);
            Assert.AreEqual(attempt2.SuspendData, attempt.SuspendData);
        }

        [Test]
        public void CreateSession_GivenNonExistingUser_ShouldThrowEntityNotFoundException()
        {
            Assert.Throws(Is.TypeOf<EntityNotFoundException>().And.Message.EqualTo("User does not exist"), () =>
            {
                const int userId = 1234;
                const int rcoId = 5678;
                _provider.CreateSession(userId, rcoId);
            });  
        }

        [Test]
        public void CreateSession_GivenNonExistingRco_ShouldThrowEntityNotFoundException()
        {
            Assert.Throws(Is.TypeOf<EntityNotFoundException>().And.Message.EqualTo("Rco does not exist"), () =>
            {
                var user = CreateUser();
                const int rcoId = 5678;
                _provider.CreateSession(user.Id, rcoId);
            });  
        }

        [Test]
        public void CreateSession_ShouldAutoTerminateActiveAttempts()
        {
            var user = CreateUser();
            var rco = CreateRco();

            var performance = CreatePerformance();
            performance.UserId = user.Id;
            performance.RcoId = rco.Id;
            
            
            var a1 = CreateAttempt(performance.Id);
            var a2 = CreateAttempt(performance.Id);
            
            _provider.CreateSession(performance.UserId, performance.RcoId);

            Assert.AreEqual(ScormAttemptStatus.Complete, a1.InternalState);
            Assert.AreEqual(ScormAttemptStatus.Complete, a2.InternalState);
        }

        [Test]
        public void TerminateSession_NonExistingSession_ShouldThrowEntityNotFoundException()
        {
            Assert.Throws(Is.TypeOf<EntityNotFoundException>().And.Message.EqualTo("Session does not exist or is terminated"), 
                () => _provider.TerminateSession(Guid.NewGuid()));  
        }

        [Test]
        public void TerminateSession_CompletedSession_ShouldThrowEntityNotFoundException()
        {
            Assert.Throws(Is.TypeOf<EntityNotFoundException>().And.Message.EqualTo("Session does not exist or is terminated"), () =>
            {
                var attempt = CreateAttempt();
                attempt.InternalState = ScormAttemptStatus.Complete;
                
                _provider.TerminateSession(attempt.SessionId);
            });  
        }

        [Test]
        public void TerminateSession_ShouldTerminateAttempts()
        {
            var attempt = CreateAttempt();
            attempt.StartTime = DateTime.UtcNow.AddSeconds(-10);
            
            _provider.TerminateSession(attempt.SessionId);

            Assert.AreEqual(ScormAttemptStatus.Complete, attempt.InternalState);
            Assert.GreaterOrEqual(attempt.TimeUsedInSeconds, 10);
        }


        [Test]
        public void TerminateSession_ShouldCopyValuesFromAttemptToPerformanceTerminateAttempts()
        {
            var performance = CreatePerformance();
            var attempt = CreateAttempt(performance.Id);
            attempt.Status = ScormLessonStatus.Passed;
            
            _provider.TerminateSession(attempt.SessionId);

            Assert.AreEqual(ScormLessonStatus.Passed, performance.Status);
        }

        [Test]
        public void TerminateSession_AttemptComplete_ShouldSetCompletedDateAndTimeUsed()
        {
            var performance = CreatePerformance();
            var attempt = CreateAttempt(performance.Id);
            attempt.Status = ScormLessonStatus.Complete;
            
            _provider.TerminateSession(attempt.SessionId);

            Assert.AreEqual(attempt.TimeUsedInSeconds, performance.TimeUsedInSeconds.GetValueOrDefault());
            Assert.GreaterOrEqual(performance.CompletedDate, DateTime.UtcNow.AddSeconds(-5));
        }
        
        [Test]
        public void TerminateSession_StatusChanged_ShouldSendEvent()
        {
            var rco = CreateRco();
            var performance = CreatePerformance(rco.Id);
            performance.Status = ScormLessonStatus.Incomplete;
            var attempt = CreateAttempt(performance.Id);
            attempt.Status = ScormLessonStatus.Complete;
            
            _provider.TerminateSession(attempt.SessionId);

            _eventAggregatorMock.Verify(e => e.Publish(It.IsAny<PerformanceStatusChangedEvent>()), Times.Once);
        }     
  
        [Test]
        public void TerminateSession_StatusNotChanged_ShouldNotSendEvent()
        {
            var rco = CreateRco();
            var performance = CreatePerformance(rco.Id);
            performance.Status = ScormLessonStatus.Complete;
            var attempt = CreateAttempt(performance.Id);
            attempt.Status = ScormLessonStatus.Complete;
            
            _provider.TerminateSession(attempt.SessionId);

            _eventAggregatorMock.Verify(e => e.Publish(It.IsAny<PerformanceStatusChangedEvent>()), Times.Never);
        }

        [Test]
        public void TerminateSession_GivenNullScore_ShouldNotOverwritePerformanceScore()
        {
            var performance = CreatePerformance();
            performance.Score = 10;
            var attempt = CreateAttempt(performance.Id);
            attempt.Score = null;

            _provider.TerminateSession(attempt.SessionId);

            Assert.AreEqual(10, performance.Score);
        }
        
        [Test]
        public void TerminateSession_GivenNonNullScore_ShouldOverwritePerformanceScore()
        {
            var performance = CreatePerformance();
            performance.Score = 10;
            var attempt = CreateAttempt(performance.Id);
            attempt.Score = 20;

            _provider.TerminateSession(attempt.SessionId);

            Assert.AreEqual(20, performance.Score);
        }
        
        [Test]
        public void GetAttempt_ShouldReturnAttempt()
        {
            var attempt = CreateAttempt();
            attempt.InternalState = ScormAttemptStatus.Active;

            var result = _provider.GetAttempt(attempt.SessionId);
            Assert.AreSame(attempt, result);
        }
        
        [Test]
        public void GetAttempt_NotActive_ShouldThrowEntityNotFoundException()
        {
            Assert.Throws(Is.TypeOf<EntityNotFoundException>().And.Message.EqualTo("Session does not exist or is terminated"),
                () =>
                {
                    var attempt = CreateAttempt();
                    attempt.InternalState = ScormAttemptStatus.Complete;
                    _provider.GetAttempt(attempt.SessionId);
                });
        }

        [Test]
        public void GetAttempt_NonExistingSession_ShouldThrowEntityNotFoundException()
        {
            Assert.Throws(Is.TypeOf<EntityNotFoundException>().And.Message.EqualTo("Session does not exist or is terminated"),
                () => _provider.GetAttempt(Guid.NewGuid()));
        }

        [Test]
        public void UpdateStatus_ShouldUpdateStatusOnAttempt()
        {
            var attempt = CreateAttempt();
            _provider.UpdateStatus(attempt.SessionId, ScormLessonStatus.Passed);

            Assert.AreEqual(ScormLessonStatus.Passed, attempt.Status);
        }

        [Test]
        public void UpdateStatus_InvalidStatus_ShouldThrowValidationException()
        {
            Assert.Throws<ValidationException>(() =>
            {
                var attempt = CreateAttempt();
                _provider.UpdateStatus(attempt.SessionId, "something invalid");
            });
        }
        
        [Test]
        public void UpdateScore_ShouldUpdateScoreOnAttempt()
        {
            var attempt = CreateAttempt();
            _provider.UpdateScore(attempt.SessionId, 42);

            Assert.AreEqual(42, attempt.Score);
        }
        
        [Test]
        public void UpdateSuspendData_ShouldUpdateSuspendData()
        {
            const string suspendData = "new suspend data";
            
            var attempt = CreateAttempt();
            _provider.UpdateSuspendData(attempt.SessionId, suspendData);

            Assert.AreEqual(suspendData, attempt.SuspendData);
            _contextMock.Verify(c => c.SaveChanges(), Times.Once);
        }
        [Test]
        public void ResetRco_ShouldResetPerformanceAndAttempt()
        {
            var user = CreateUser();
            var rco = CreateRco();
            var performance = CreatePerformance(rco.Id, user.Id);
            var attempt = CreateAttempt(performance.Id);
            performance.Status = ScormLessonStatus.Complete;
            performance.Score = 100;
            attempt.Score = 100;
            attempt.SuspendData = "1234";
            _provider.ResetRcoStatus(rco.Id,user.Id, false);
            Assert.AreEqual(performance.Status, ScormLessonStatus.NotAttempted);
            Assert.AreEqual(performance.Score, 0);
            var newAttempt = _contextMock.Object.ScormAttempts.Where(a => a.PerformanceId == performance.Id).Last();

            Assert.AreEqual(newAttempt.Status, ScormLessonStatus.NotAttempted);
            Assert.Null(newAttempt.Score);
        }
        [Test]
        public void ResetRco_ShouldResetChildRcos()
        {
            var user = CreateUser();
            var rco = CreateRco();
            var performancesToCheck = new List<ScormPerformance>();
            foreach(var rcoItem in _contextMock.Object.Rcos)
            {
                if (rcoItem.Id == rco.Id || rcoItem.ParentId == rco.Id)
                {
                    var performance = CreatePerformance(rcoItem.Id, user.Id);
                    performancesToCheck.Add(performance);
                    var attempt = CreateAttempt(performance.Id);
                    performance.Status = ScormLessonStatus.Complete;
                    performance.Score = 100;
                    attempt.Score = 100;
                    attempt.SuspendData = "1234";
                }
            }
            _provider.ResetRcoStatus(rco.Id, user.Id, true);
            foreach (var performance in performancesToCheck)
            {
                Assert.AreEqual(performance.Status, ScormLessonStatus.NotAttempted);
                Assert.AreEqual(performance.Score, 0);
                var newAttempt = _contextMock.Object.ScormAttempts.Where(a => a.PerformanceId == performance.Id).Last();

                Assert.AreEqual(newAttempt.Status, ScormLessonStatus.NotAttempted);
                Assert.Null(newAttempt.Score);
            }
        }

        private User CreateUser()
        {
            var user = new User
            {
                Id = GetNextId()
            };
            _contextMock.Object.Users.Add(user);
            return user;
        }

        private Rco CreateRco()
        {
            var rco = new Rco
            {
                Id = GetNextId()
            };
            _contextMock.Object.Rcos.Add(rco);
            for(var counter = 0; counter < 10; counter++)
            {
                var rcoChild = new Rco { Id = GetNextId(), ParentId = rco.Id };
                _contextMock.Object.Rcos.Add(rcoChild);
            }

            return rco;
        }

        private ScormPerformance CreatePerformance(int? cmsRcoId = null, int? userId = null)
        {
            if (cmsRcoId == null)
            {
                cmsRcoId = CreateRco().Id;
            }

            if (userId == null)
            {
                userId = CreateUser().Id;
            }
            var performance = new ScormPerformance
            {
                Id = GetNextId(),
                RcoId = cmsRcoId.Value,
                UserId = userId.Value
            };
            _contextMock.Object.ScormPerformances.Add(performance);
            return performance;
        }

        private ScormAttempt CreateAttempt(int? performanceId = null)
        {
            if (performanceId == null) performanceId = CreatePerformance().Id;
            var attempt = new ScormAttempt
            {
                Id = GetNextId(),
                PerformanceId = performanceId.Value,
                InternalState = ScormAttemptStatus.Active,
                SessionId = Guid.NewGuid(),
                StartTime = DateTime.Now.AddSeconds(_ids) // make sure date is higher every time run
            };
            _contextMock.Object.ScormAttempts.Add(attempt);
            return attempt;
        }
        
        private int GetNextId()
        {
            return ++_ids;
        }
    }
}