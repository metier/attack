﻿using System;
using System.Linq;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Sorting;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Providers
{
    [TestFixture]
    public class RcoProviderTests
    {
        private RcoProvider _provider;

        private Mock<IRcoRepository> _rcoRepositoryMock;
        private ContextMock _contextMock;
        
        [SetUp]
        public void Setup()
        {
            _rcoRepositoryMock = new Mock<IRcoRepository>();
            var sortNumberedStringMock = new Mock<ISortNumberedStrings>();

            sortNumberedStringMock.Setup(s => s.PadInitialNumbersForSorting(It.IsAny<string>(), It.IsAny<bool>()))
                .Returns((string input, bool isFirst) => input);

            _contextMock = new ContextMock();
            _provider = new RcoProvider(_rcoRepositoryMock.Object, _contextMock.Object, sortNumberedStringMock.Object);
        }

        [Test]
        public void GetCourse_GivenNonExistingId_ShouldThrowException()
        {
            Assert.Throws(Is.TypeOf<EntityNotFoundException>().With.Message.EqualTo("Course does not exist"), 
                () => _provider.GetCourse(123456));
        }

        [Test]
        public void GetCourse_GivenIdToCourseWithLessonsWithLessons_ShouldCreateRecusiveStructure()
        {
            _contextMock.Object.Rcos.Add(new Rco { Id = 1, Context = "course", ChildCount = 2 });
            _contextMock.Object.Rcos.Add(new Rco { Id = 2, ParentId = 1 });
            _contextMock.Object.Rcos.Add(new Rco { Id = 3, ParentId = 1, ChildCount = 1 });
            _contextMock.Object.Rcos.Add(new Rco { Id = 4, ParentId = 3 });

            var course = _provider.GetCourse(1);

            Assert.AreEqual(1, course.Id);
            Assert.AreEqual(2, course.Lessons.Count);
            Assert.AreEqual(2, course.Lessons[0].Id);
            Assert.AreEqual(3, course.Lessons[1].Id);
            Assert.AreEqual(1, course.Lessons[1].Lessons.Count);
            Assert.AreEqual(4, course.Lessons[1].Lessons[0].Id);
        }

        [Test]
        public void GetCourse_GivenCourseChildCountZero_ShouldNotIncludeChildren()
        {
            _contextMock.Object.Rcos.Add(new Rco { Id = 1, Context = "course", ChildCount = 0 });
            _contextMock.Object.Rcos.Add(new Rco { Id = 2, ParentId = 1 });

            var course = _provider.GetCourse(1);

            Assert.AreEqual(1, course.Id);
            Assert.AreEqual(0, course.Lessons.Count);
        }


        [Test]
        public void GetCourse_GivenCourseWithLessonWithChildCountZero_ShouldNotIncludeChildren()
        {
            _contextMock.Object.Rcos.Add(new Rco { Id = 1, Context = "course", ChildCount = 1 });
            _contextMock.Object.Rcos.Add(new Rco { Id = 2, ParentId = 1, ChildCount = 0 });
            _contextMock.Object.Rcos.Add(new Rco { Id = 3, ParentId = 2 });

            var course = _provider.GetCourse(1);

            Assert.AreEqual(1, course.Id);
            Assert.AreEqual(1, course.Lessons.Count);
            Assert.AreEqual(0, course.Lessons[0].Lessons.Count);
        }

        [Test]
        public void GetCourse_GivenCourseWithLessonWithLessonChildCountZero_ShouldNotIncludeChildren()
        {
            _contextMock.Object.Rcos.Add(new Rco { Id = 1, Context = "course", ChildCount = 1 });
            _contextMock.Object.Rcos.Add(new Rco { Id = 2, ParentId = 1, ChildCount = 1 });
            _contextMock.Object.Rcos.Add(new Rco { Id = 3, ParentId = 2, ChildCount = 0});
            _contextMock.Object.Rcos.Add(new Rco { Id = 4, ParentId = 3 });

            var course = _provider.GetCourse(1);

            Assert.AreEqual(1, course.Id);
            Assert.AreEqual(1, course.Lessons.Count);
            Assert.AreEqual(1, course.Lessons[0].Lessons.Count);
            Assert.AreEqual(0, course.Lessons[0].Lessons[0].Lessons.Count);
        }

        [Test]
        public void GetCourse_GivenIdToCourse_MustSetPropertiesFromRco()
        {
            var courseRco = new Rco
            {
                Id = 1, 
                Created = DateTime.Now,
                Version = "The version",
                Name = "The name",
                GoldPoints = 123,
                SilverPoints = 456,
                UnlockTestPoints = 789,
                Context = "course"
            };
            _contextMock.Object.Rcos.Add(courseRco);

            var course = _provider.GetCourse(1);

            Assert.AreEqual(courseRco.Id, course.Id);
            Assert.AreEqual(courseRco.Name, course.Name);
            Assert.AreEqual(courseRco.Version, course.Version);
            Assert.AreEqual(courseRco.Created, course.Created);
            Assert.AreEqual(courseRco.GoldPoints, course.GoldPoints);
            Assert.AreEqual(courseRco.SilverPoints, course.SilverPoints);
            Assert.AreEqual(courseRco.UnlockTestPoints, course.UnlockFinalPoints);
        }

        [Test]
        public void GetCourse_GivenIdToCourse_MustSetLanguage()
        {
            _contextMock.Object.Languages.Add(new Language { Identifier = "NO", Name = "Norwegian" });

            var courseRco = new Rco { Id = 1, Language = "no", Context = "course" };
            _contextMock.Object.Rcos.Add(courseRco);

            var course = _provider.GetCourse(1);

            Assert.AreEqual(courseRco.Language.ToUpper(), course.LanguageIdentifier);
            Assert.AreEqual(_contextMock.Object.Languages.First().Name, course.Language);
        }

        [Test]
        public void GetCourse_GivenIdToCourse_MustSetPropertiesOnLessonFromRco()
        {
            var courseRco = new Rco { Id = 1, Context = "course", ChildCount = 1 };
            var lessonRco = new Rco { Id = 2, ParentId = 1, Created = DateTime.Now, Name = "The name", IsFinalTest = true, Weight = 6 };
            _contextMock.Object.Rcos.Add(courseRco);
            _contextMock.Object.Rcos.Add(lessonRco);

            var course = _provider.GetCourse(1);
            var lesson = course.Lessons[0];

            Assert.AreEqual(lessonRco.Id, lesson.Id);
            Assert.AreEqual(lessonRco.Name, lesson.Name);
            Assert.AreEqual(lessonRco.IsFinalTest, lesson.IsFinalTest);
            Assert.AreEqual(lessonRco.Created, lesson.Created);
            Assert.AreEqual(lessonRco.Weight, lesson.Weight);
        }

        [Test]
        public void GetCourseLessons_ShouldReturnAllLessons()
        {
            _contextMock.Object.Rcos.Add(new Rco { Id = 1, ParentId = null, Context = "course", ChildCount = 2 });
            _contextMock.Object.Rcos.Add(new Rco { Id = 2, ParentId = 1, Context = "lesson", ChildCount = 1 });
            _contextMock.Object.Rcos.Add(new Rco { Id = 3, ParentId = 2, Context = "lesson" });
            _contextMock.Object.Rcos.Add(new Rco { Id = 4, ParentId = 1, Context = "lesson" });

            var lessons = _provider.GetCourseRcos(4).ToList();

            Assert.AreEqual(3, lessons.Count);

            Assert.AreEqual(2, lessons[0].Id);
            Assert.AreEqual(3, lessons[1].Id);
            Assert.AreEqual(4, lessons[2].Id);
        }

        [Test]
        public void GetCourseLessons_GivenZeroChildCount_NotIncludeLessons()
        {
            _contextMock.Object.Rcos.Add(new Rco { Id = 1, ParentId = null, Context = "course", ChildCount = 0});
            _contextMock.Object.Rcos.Add(new Rco { Id = 2, ParentId = 1, Context = "lesson" });
            _contextMock.Object.Rcos.Add(new Rco { Id = 3, ParentId = 2, Context = "lesson" });
            _contextMock.Object.Rcos.Add(new Rco { Id = 4, ParentId = 1, Context = "lesson" });

            var lessons = _provider.GetCourseRcos(4).ToList();

            Assert.AreEqual(0, lessons.Count);
        }

        [Test]
        public void GetCourseLessons_GivenZeroChildCountOnLesson_NotIncludeChildLessons()
        {
            _contextMock.Object.Rcos.Add(new Rco { Id = 1, ParentId = null, Context = "course", ChildCount = 1});
            _contextMock.Object.Rcos.Add(new Rco { Id = 2, ParentId = 1, Context = "lesson", ChildCount = 0});
            _contextMock.Object.Rcos.Add(new Rco { Id = 3, ParentId = 2, Context = "lesson" });
            _contextMock.Object.Rcos.Add(new Rco { Id = 4, ParentId = 2, Context = "lesson" });

            var lessons = _provider.GetCourseRcos(1).ToList();

            Assert.AreEqual(1, lessons.Count);
        }

        [Test]
        public void GetCourseLessons_GivenZeroChildCountOnLessonsLessons_NotIncludeChildLessons()
        {
            _contextMock.Object.Rcos.Add(new Rco { Id = 1, ParentId = null, Context = "course", ChildCount = 1});
            _contextMock.Object.Rcos.Add(new Rco { Id = 2, ParentId = 1, Context = "lesson", ChildCount = 1});
            _contextMock.Object.Rcos.Add(new Rco { Id = 3, ParentId = 2, Context = "lesson", ChildCount = 0});
            _contextMock.Object.Rcos.Add(new Rco { Id = 4, ParentId = 3, Context = "lesson" });

            var lessons = _provider.GetCourseRcos(1).ToList();

            Assert.AreEqual(2, lessons.Count);
        }

        [Test]
        public void GetCourseLessons_ShouldReturnAllLessonsIncludingCourse()
        {
            _contextMock.Object.Rcos.Add(new Rco { Id = 1, ParentId = null, Context = "course", ChildCount = 2 });
            _contextMock.Object.Rcos.Add(new Rco { Id = 2, ParentId = 1, Context = "lesson", ChildCount = 1 });
            _contextMock.Object.Rcos.Add(new Rco { Id = 3, ParentId = 2, Context = "lesson" });
            _contextMock.Object.Rcos.Add(new Rco { Id = 4, ParentId = 1, Context = "lesson" });

            var lessons = _provider.GetCourseRcos(1, true).ToList();

            Assert.AreEqual(4, lessons.Count);

            Assert.AreEqual(1, lessons[0].Id);
            Assert.AreEqual(2, lessons[1].Id);
            Assert.AreEqual(3, lessons[2].Id);
            Assert.AreEqual(4, lessons[3].Id);
        }

        [Test]
        public void GetCourseLessons_TopNodeIsLesson_ShouldReturnAllLessonsIncludingTopNode()
        {
            _contextMock.Object.Rcos.Add(new Rco { Id = 1, ParentId = null, Context = "lesson", ChildCount = 2 });
            _contextMock.Object.Rcos.Add(new Rco { Id = 2, ParentId = 1, Context = "lesson", ChildCount = 1 });
            _contextMock.Object.Rcos.Add(new Rco { Id = 3, ParentId = 2, Context = "lesson" });
            _contextMock.Object.Rcos.Add(new Rco { Id = 4, ParentId = 1, Context = "lesson" });

            var lessons = _provider.GetCourseRcos(4).ToList();

            Assert.AreEqual(4, lessons.Count);

            Assert.AreEqual(1, lessons[0].Id);
            Assert.AreEqual(2, lessons[1].Id);
            Assert.AreEqual(3, lessons[2].Id);
            Assert.AreEqual(4, lessons[3].Id);
        }
    }
}