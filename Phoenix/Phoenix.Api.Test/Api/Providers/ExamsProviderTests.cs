﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Factories.Exam;
using Metier.Phoenix.Api.Factories.ExamPlayer;
using Metier.Phoenix.Api.Messaging;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Business;
using Metier.Phoenix.Core.Business.ChangeLog;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Exams;
using Metier.Phoenix.Core.Exceptions;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Providers
{
    public class ExamsProviderTests
    {
        private ExamsProvider _examsProvider;

        private ContextMock _contextMock;
        private Mock<IAuthorizationProvider> _authorizationProviderMock;
        private Mock<IExamFactory> _examFactoryMock;
        private Mock<IExamPlayerFactory> _examFacadeFactoryMock;
        private Mock<IEventAggregator> _eventAggregatorMock;
        private Phoenix.Api.Facade.ExamPlayer.Exam _expectedExamFacade;
        private Mock<IChangeLogProvider> _changeLogProvider;
        private Exam _expectedExamEntity;

        [SetUp]
        public void Setup()
        {
            _contextMock = new ContextMock();
            _authorizationProviderMock = new Mock<IAuthorizationProvider>();
            _examFactoryMock = new Mock<IExamFactory>();
            _examFacadeFactoryMock = new Mock<IExamPlayerFactory>();
            _eventAggregatorMock = new Mock<IEventAggregator>();
            _changeLogProvider = new Mock<IChangeLogProvider>();
            var examAnswerHandler = new Mock<IExamGrader>();
            var participantBusiness = new Mock<IParticipantBusiness>();
            _examsProvider = new ExamsProvider(_contextMock.Object, _authorizationProviderMock.Object, _examFactoryMock.Object, _examFacadeFactoryMock.Object, examAnswerHandler.Object, _eventAggregatorMock.Object, participantBusiness.Object, _changeLogProvider.Object);
        }

        [Test]
        public void StartExam_ShouldThrowIfTooEarly()
        {
            SetupMocks();
            
            _contextMock.Object.Activities.ElementAt(0).ActivityPeriods[0].Start = DateTime.UtcNow.AddSeconds(10);
            var activityId = _contextMock.Object.Activities.ElementAt(0).Id;

            var ex = Assert.Throws<ValidationException>(() => _examsProvider.StartExam(activityId));
            Assert.AreEqual("Exam is not yet available", ex.ValidationResults.ElementAt(0).ErrorMessage);
        }

        [Test]
        public void StartExam_ShouldThrowIfTooLate()
        {
            SetupMocks();

            _contextMock.Object.Activities.ElementAt(0).ActivityPeriods[0].End = DateTime.UtcNow.AddSeconds(-10);
            var activityId = _contextMock.Object.Activities.ElementAt(0).Id;

            var ex = Assert.Throws<ValidationException>(() => _examsProvider.StartExam(activityId));
            Assert.AreEqual("Exam is no longer available", ex.ValidationResults.ElementAt(0).ErrorMessage);
        }

        [Test]
        public void StartExam_ShouldThrowNoStartPeriod()
        {
            SetupMocks();

            _contextMock.Object.Activities.ElementAt(0).ActivityPeriods[0].Start = null;
            var activityId = _contextMock.Object.Activities.ElementAt(0).Id;

            var ex = Assert.Throws<ValidationException>(() => _examsProvider.StartExam(activityId));
            Assert.AreEqual("Unable not determine availability for activity", ex.ValidationResults.ElementAt(0).ErrorMessage);
        }

        [Test]
        public void StartExam_ShouldThrowNoPeriods()
        {
            SetupMocks();

            _contextMock.Object.Activities.ElementAt(0).ActivityPeriods = new List<ActivityPeriod>();
            var activityId = _contextMock.Object.Activities.ElementAt(0).Id;

            var ex = Assert.Throws<ValidationException>(() => _examsProvider.StartExam(activityId));
            Assert.AreEqual("Unable not determine availability for activity", ex.ValidationResults.ElementAt(0).ErrorMessage);
        }

        [Test]
        public void StartExam_ShouldThrowExamExists()
        {
            SetupMocks();
            var activityId = _contextMock.Object.Activities.ElementAt(0).Id;
            var participantId = _contextMock.Object.Participants.ElementAt(0).Id;
            _contextMock.Object.Exams.Add(new Exam { Id = 42, ParticipantId = participantId });

            var ex = Assert.Throws<ValidationException>(() => _examsProvider.StartExam(activityId));
            Assert.AreEqual("Participant "+ participantId + " already has an active exam with ID 42", ex.ValidationResults.ElementAt(0).ErrorMessage);
        }

        [Test]
        public void StartExam_ShouldThrowIfActivityNotFound()
        {
            SetupMocks();

            var ex = Assert.Throws<EntityNotFoundException>(() => _examsProvider.StartExam(123456));
            Assert.AreEqual("Activity with ID 123456 not found", ex.Message);
        }

        [Test]
        public void StartExam_ShouldThrowIfCurrentUserIsNotParticipatingActivity()
        {
            SetupMocks();
            // Point at something non-existing
            _contextMock.Object.Participants.ElementAt(0).UserId = 123456;
            var activityId = _contextMock.Object.Activities.ElementAt(0).Id;

            var ex = Assert.Throws<ValidationException>(() => _examsProvider.StartExam(activityId));
            Assert.AreEqual(1, ex.ValidationResults.Count());
            Assert.AreEqual("Current user is not a participant of activity " + activityId, ex.ValidationResults.ElementAt(0).ErrorMessage);
        }
        

        [Test]
        public void StartExam_ShouldThrowIfCustomerArticleNotFound()
        {
            SetupMocks();
            // Point at something non-existing
            _contextMock.Object.Activities.ElementAt(0).CustomerArticleId = 123456;
            var activityId = _contextMock.Object.Activities.ElementAt(0).Id;

            var ex = Assert.Throws<EntityNotFoundException>(() => _examsProvider.StartExam(activityId));
            Assert.AreEqual("Customer Article with ID 123456 not found", ex.Message);
        }

        [Test]
        public void StartExam_ShouldThrowIfArticleNotFound()
        {
            SetupMocks();
            // Point at something non-existing
            _contextMock.Object.CustomerArticles.ElementAt(0).ArticleCopyId = 123456;
            var activityId = _contextMock.Object.Activities.ElementAt(0).Id;

            var ex = Assert.Throws<EntityNotFoundException>(() => _examsProvider.StartExam(activityId));
            Assert.AreEqual("Exam Article with ID 123456 not found", ex.Message);
        }

        [Test]
        public void StartExam_ShouldReturnFacadeFromExam()
        {
            SetupMocks();

            var activityId = _contextMock.Object.Activities.ElementAt(0).Id;
            var examFacade = _examsProvider.StartExam(activityId);
            Assert.AreSame(_expectedExamFacade, examFacade);
        }

        [Test]
        public void StartExam_ShouldSaveExam()
        {
            SetupMocks();

            var activityId = _contextMock.Object.Activities.ElementAt(0).Id;
            _examsProvider.StartExam(activityId);
            Assert.AreSame(_expectedExamEntity, _contextMock.Object.Exams.ElementAt(0));
            _contextMock.Verify(c => c.SaveChanges(), Times.Once());
        }

        [Test]
        public void GetExamResult_ShouldReturnExamWithAnswers()
        {
            SetupMocks();

            const int participantId = 1;

            _expectedExamEntity.ParticipantId = participantId;
            _expectedExamEntity.Answers.Add(new Answer { Id = 1 });
            _contextMock.Object.Exams.Add(_expectedExamEntity);

            var result = _examsProvider.GetExamResult(participantId);

            Assert.AreSame(_expectedExamEntity, result);
            Assert.AreEqual(1, result.Answers.Count);
        }

        [Test]
        public void GetExamResult_NoExamExistForParticipant_ShouldReturnNull()
        {
            const int participantId = 1234;

            var result = _examsProvider.GetExamResult(participantId);

            Assert.IsNull(result);
        }

        private void SetupMocks()
        {
            var user = new User { Id = 2 };
            var participant = new Participant { Id = 1, UserId = user.Id };
            var article = new ExamArticle { Id = 5 };
            var customerArticle = new CustomerArticle { Id = 4, ArticleCopyId = article.Id };
            var activity = new Activity
            {
                Id = 3, 
                Participants = new List<Participant> { participant }, 
                CustomerArticleId = customerArticle.Id,
                ActivityPeriods = new List<ActivityPeriod> 
                { 
                    new ActivityPeriod
                    {
                        Start = DateTime.UtcNow.AddHours(-1),
                        End = DateTime.UtcNow.AddHours(1)
                    } 
                }
            };

            _contextMock.Object.Participants.Add(participant);
            _contextMock.Object.Activities.Add(activity);
            _contextMock.Object.CustomerArticles.Add(customerArticle);
            _contextMock.Object.Articles.Add(article);
            _authorizationProviderMock.Setup(a => a.GetCurrentUser()).Returns(user);

            _expectedExamFacade = new Phoenix.Api.Facade.ExamPlayer.Exam();
            _expectedExamEntity = new Exam();

            _examFactoryMock.Setup(f => f.Create(article, activity, participant, customerArticle)).Returns(_expectedExamEntity);
            _examFacadeFactoryMock.Setup(f => f.CreateExam(_expectedExamEntity, participant, activity, article)).Returns(_expectedExamFacade);
        }
    }
}