﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Factories;
using Metier.Phoenix.Api.Messaging;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Providers
{
    [TestFixture]
    public class PerformanceProviderTests
    {
        private ContextMock _contextMock;
        private Mock<IRcoProvider> _rcoProviderMock;
        private Mock<IEventAggregator> _eventAggregatorMock;
        private Mock<IPerformanceSummaryFactory> _performanceSummaryFactoryMock;

        private PerformanceProvider _provider;

        [SetUp]
        public void Setup()
        {
            _rcoProviderMock = new Mock<IRcoProvider>();    
            _contextMock = new ContextMock();
            _eventAggregatorMock = new Mock<IEventAggregator>();
            _performanceSummaryFactoryMock = new Mock<IPerformanceSummaryFactory>();

            _provider = new PerformanceProvider(_contextMock.Object, _rcoProviderMock.Object, _eventAggregatorMock.Object, _performanceSummaryFactoryMock.Object);
        }

        [Test]
        public void GetPerformances_GivenUserIdWithoutPerformances_MustReturnLessonsNotAttempted()
        {
            var cmsRcos = new List<Rco>
            {
                new Rco { Id = 1, Context = "course" },
                new Rco { Id = 2, ParentId = 1, Context = "lesson" },
                new Rco { Id = 3, ParentId = 1, Context = "lesson" }
            };
            _contextMock.Object.Articles.Add(new ElearningArticle { Id = 4, RcoCourseId = 1 });
            _rcoProviderMock.Setup(r => r.GetCourseRcos(1, true)).Returns(cmsRcos);
            _rcoProviderMock.Setup(r => r.GetCourse(1)).Returns(new RcoCourse());

            var coursePerformance = _provider.GetPerformancesByArticle(1234, 4);

            Assert.AreEqual(3, coursePerformance.Performances.Count);
            Assert.AreEqual(ScormLessonStatus.NotAttempted, coursePerformance.Performances[0].Status);
            Assert.AreEqual(ScormLessonStatus.NotAttempted, coursePerformance.Performances[1].Status);
            Assert.AreEqual(ScormLessonStatus.NotAttempted, coursePerformance.Performances[2].Status);
        }

        [Test]
        public void GetPerformances_GivenUserIdWithSomePerformances_MustReturnLessonsNotAttemptedAndExistingPerformances()
        {
            var cmsRcos = new List<Rco>
            {
                new Rco { Id = 1, Context = "course" },
                new Rco { Id = 2, ParentId = 1, Context = "lesson" },
                new Rco { Id = 3, ParentId = 1, Context = "lesson" }
            };
            _contextMock.Object.Articles.Add(new ElearningArticle { Id = 4, RcoCourseId = 1 });
            _contextMock.Object.ScormPerformances.Add(new ScormPerformance { Id = 5, RcoId = 2, UserId = 1234, Status = ScormLessonStatus.Complete });

            _rcoProviderMock.Setup(r => r.GetCourseRcos(1, true)).Returns(cmsRcos);
            _rcoProviderMock.Setup(r => r.GetCourse(1)).Returns(new RcoCourse());

            var coursePerformance = _provider.GetPerformancesByArticle(1234, 4);

            Assert.AreEqual(3, coursePerformance.Performances.Count);
            Assert.AreEqual(ScormLessonStatus.NotAttempted, coursePerformance.Performances[0].Status);
            Assert.AreSame(_contextMock.Object.ScormPerformances.ElementAt(0), coursePerformance.Performances[1]);
            Assert.AreEqual(ScormLessonStatus.NotAttempted, coursePerformance.Performances[2].Status);
        }

        [Test]
        public void GetPerformances_UserId_Zero_ThrowsArgumentException()
        {
            Assert.Throws(Is.TypeOf<ArgumentException>().And.Message.EqualTo("User id must be set"), () => _provider.GetPerformancesByArticle(0, 1234));
        }

        [Test]
        public void GetPerformances_ArticleId_Zero_ThrowsArgumentException()
        {
            Assert.Throws(Is.TypeOf<ArgumentException>().And.Message.EqualTo("Article id must be set"), () => _provider.GetPerformancesByArticle(1234, 0));
        }

        [Test]
        public void RegisterPerformance_GivenNonExisitngUser_ShouldThrowEntityNotFoundException()
        {
            Assert.Throws(Is.TypeOf<EntityNotFoundException>().And.Message.EqualTo("User does not exist"), () => _provider.RegisterPerformance(1, 2, null));
        }

        [Test]
        public void RegisterPerformance_RcoId_Zero_ThrowsArgumentException()
        {
            Assert.Throws(Is.TypeOf<ArgumentException>().And.Message.EqualTo("RCO id must be set"), () => _provider.RegisterPerformance(1, 0, null));
        }

        [Test]
        public void GetPerformancesByUserId_ShouldCreatePerformancesForAllCoursesWithUserPerformance()
        {
            const int userId = 1234;
            _contextMock.Object.ScormPerformances.Add(new ScormPerformance { RcoId = 1, UserId = userId });
            _contextMock.Object.ScormPerformances.Add(new ScormPerformance { RcoId = 2, UserId = userId });
            _contextMock.Object.ScormPerformances.Add(new ScormPerformance { RcoId = 3, UserId = userId });

            var cmsRco1 = new Rco { Id = 1, Context = "course"};
            _contextMock.Object.Rcos.Add(cmsRco1);
            var cmsRco2 = new Rco { Id = 2, Context = "course"};
            _contextMock.Object.Rcos.Add(cmsRco2);
            _contextMock.Object.Rcos.Add(new Rco { Id = 3, Context = "lesson"});

            var course1 = new RcoCourse();
            _rcoProviderMock.Setup(r => r.GetCourse(1)).Returns(course1);
            _rcoProviderMock.Setup(r => r.GetCourseRcos(1, true)).Returns(new List<Rco> { cmsRco1 });
            var course2 = new RcoCourse();
            _rcoProviderMock.Setup(r => r.GetCourse(2)).Returns(course2);
            _rcoProviderMock.Setup(r => r.GetCourseRcos(2, true)).Returns(new List<Rco> { cmsRco2 });

            var performances = _provider.GetPerformancesByUserId(userId).ToList();

            Assert.AreEqual(2, performances.Count);
            Assert.AreEqual(userId, performances[0].UserId);
            Assert.AreSame(course1, performances[0].Course);
            Assert.AreEqual(1, performances[0].Performances.Count);
            
            Assert.AreEqual(userId, performances[1].UserId);
            Assert.AreSame(course2, performances[1].Course);
            Assert.AreEqual(1, performances[1].Performances.Count);
        }

        [Test]
        public void RegisterPerformance_ExistingUserWithExistingPerformance_ShouldUpdatePerformance()
        {
            _contextMock.Object.Users.Add(new User { Id = 1 });
            _contextMock.Object.ScormPerformances.Add(new ScormPerformance
            {
                Id = 2, 
                RcoId = 3, 
                UserId = 1
            });
            _contextMock.Object.Rcos.Add(new Rco { Id = 3 });
            _rcoProviderMock.Setup(r => r.GetTopParent(3)).Returns(new Rco());

            var request = new ScormPerformanceRegisterRequest
            {
                CompletedDate = DateTime.UtcNow,
                Score = 1337,
                Status = ScormLessonStatus.Complete,
                TimeUsedInSeconds = 42
            };
            var result = _provider.RegisterPerformance(1, 3, request);

            Assert.AreEqual(1, result.UserId);
            Assert.AreEqual(3, result.RcoId);
            Assert.AreEqual(request.CompletedDate, result.CompletedDate);
            Assert.AreEqual(request.Score, result.Score);
            Assert.AreEqual(request.Status, result.Status);
            Assert.AreEqual(request.TimeUsedInSeconds, result.TimeUsedInSeconds);
        }

        [Test]
        public void RegisterPerformance_ExistingUserWithoutExistingPerformance_ShouldReturnNewPerformance()
        {
            _contextMock.Object.Users.Add(new User { Id = 1 });
            var request = new ScormPerformanceRegisterRequest
            {
                CompletedDate = DateTime.UtcNow,
                Score = 1337,
                Status = ScormLessonStatus.Complete,
                TimeUsedInSeconds = 42
            };
            _rcoProviderMock.Setup(r => r.GetTopParent(3)).Returns(new Rco());
            _contextMock.Object.Rcos.Add(new Rco { Id = 3 });
            var result = _provider.RegisterPerformance(1, 3, request);

            Assert.AreEqual(1, result.UserId);
            Assert.AreEqual(3, result.RcoId);
            Assert.AreEqual(request.CompletedDate, result.CompletedDate);
            Assert.AreEqual(request.Score, result.Score);
            Assert.AreEqual(request.Status, result.Status);
            Assert.AreEqual(request.TimeUsedInSeconds, result.TimeUsedInSeconds);
        }

        [Test]
        public void RegisterPerformance_StatusChanged_ShouldPublishEvent()
        {
            _contextMock.Object.ScormPerformances.Add(new ScormPerformance { RcoId = 1, UserId = 1 });
            _rcoProviderMock.Setup(r => r.GetTopParent(3)).Returns(new Rco { Id = 1, Context = "course" });

            _contextMock.Object.Users.Add(new User { Id = 1 });
            _contextMock.Object.ScormPerformances.Add(new ScormPerformance
            {
                Id = 2,
                RcoId = 3,
                UserId = 1,
                Status = ScormLessonStatus.Complete
            });
            _contextMock.Object.Rcos.Add(new Rco { Id = 3 });

            var request = new ScormPerformanceRegisterRequest
            {
                Status = ScormLessonStatus.Incomplete
            };
            _provider.RegisterPerformance(1, 3, request);
            _eventAggregatorMock.Verify(e => e.Publish(It.IsAny<PerformanceStatusChangedEvent>()), Times.Once);
        }

        [Test]
        public void RegisterPerformance_StatusNotChanged_ShouldNotPublishEvent()
        {
            _contextMock.Object.ScormPerformances.Add(new ScormPerformance { RcoId = 1, UserId = 1 });
            _rcoProviderMock.Setup(r => r.GetTopParent(3)).Returns(new Rco { Id = 1, Context = "course" });

            _contextMock.Object.Users.Add(new User { Id = 1 });
            _contextMock.Object.ScormPerformances.Add(new ScormPerformance
            {
                Id = 2,
                RcoId = 3,
                UserId = 1,
                Status = ScormLessonStatus.Complete
            });
            _contextMock.Object.Rcos.Add(new Rco { Id = 3 });
            
            var request = new ScormPerformanceRegisterRequest
            {
                Status = ScormLessonStatus.Complete
            };
            _provider.RegisterPerformance(1, 3, request);
            _eventAggregatorMock.Verify(e => e.Publish(It.IsAny<PerformanceStatusChangedEvent>()), Times.Never);
        }

        [Test]
        public void RegisterPerformance_GivenFinalTestWithPreviousStatusCompleted_ShouldNotPublishEvent()
        {
            _contextMock.Object.ScormPerformances.Add(new ScormPerformance { RcoId = 1, UserId = 1 });
            _rcoProviderMock.Setup(r => r.GetTopParent(3)).Returns(new Rco { Id = 1, Context = "course" });

            _contextMock.Object.Users.Add(new User { Id = 1 });
            _contextMock.Object.ScormPerformances.Add(new ScormPerformance
            {
                Id = 2,
                RcoId = 3,
                UserId = 1,
                Status = ScormLessonStatus.Complete
            });
            _contextMock.Object.Rcos.Add(new Rco { Id = 3, IsFinalTest = true });

            var request = new ScormPerformanceRegisterRequest
            {
                Status = ScormLessonStatus.Incomplete
            };
            _provider.RegisterPerformance(1, 3, request);
            _eventAggregatorMock.Verify(e => e.Publish(It.IsAny<PerformanceStatusChangedEvent>()), Times.Never);
        }

        [Test]
        public void CreateParentPerformanceIfNotExists_ShouldCreatePerformanceForCourseIfNotExist()
        {
            const int userId = 1234;
            var courseRco = new Rco { Id = 1, Context = "course" };
            _rcoProviderMock.Setup(r => r.GetTopParent(2)).Returns(courseRco);

            _provider.CreateParentPerformanceIfNotExists(userId, 2);

            Assert.AreEqual(1, _contextMock.Object.ScormPerformances.Count());
            Assert.AreEqual(1, _contextMock.Object.ScormPerformances.ElementAt(0).RcoId);
        }

        [Test]
        public void CreateParentPerformanceIfNotExists_ShouldNotCreatePerformanceForCourseIfAlreadyExist()
        {
            const int userId = 1234;
            var courseRco = new Rco { Id = 1, Context = "course" };
            _rcoProviderMock.Setup(r => r.GetTopParent(2)).Returns(courseRco);
            _contextMock.Object.ScormPerformances.Add(new ScormPerformance { UserId = userId, RcoId = courseRco.Id });

            _provider.CreateParentPerformanceIfNotExists(userId, 2);

            Assert.AreEqual(1, _contextMock.Object.ScormPerformances.Count());
            Assert.AreEqual(1, _contextMock.Object.ScormPerformances.ElementAt(0).RcoId);
        }

        [Test]
        public void CreateParentPerformanceIfNotExists_ShouldNotCreatePerformance_IfParentIsSameAsRelated()
        {
            const int userId = 1234;
            var courseRco = new Rco { Id = 1, Context = "course" };
            _rcoProviderMock.Setup(r => r.GetTopParent(1)).Returns(courseRco);

            _provider.CreateParentPerformanceIfNotExists(userId, 1);

            Assert.AreEqual(0, _contextMock.Object.ScormPerformances.Count());
        }
    }
}