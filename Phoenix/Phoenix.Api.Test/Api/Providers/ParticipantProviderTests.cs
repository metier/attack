﻿using System.Linq;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Factories;
using Metier.Phoenix.Api.Messaging;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Business;
using Metier.Phoenix.Core.Business.ChangeLog;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Providers
{
    [TestFixture]
    public class ParticipantProviderTests
    {
        private Mock<IActivitySetRepository> _activitySetRepositoryMock;
        private Mock<IParticipantRepository> _participantRepositoryMock;
        private Mock<IOrderFactory> _orderFactoryMock;
        private Mock<IRcoProvider> _rcoProviderMock;
        private Mock<IAuthorizationProvider> _authorizationProviderMock;
        private Mock<IEventAggregator> _eventAggregatorMock;
        private Mock<IParticipantBusiness> _participantBusinessMock;
        private Mock<IChangeLogProvider> _changeLogProviderMock;
        private ContextMock _contextMock;
        
        private ParticipantsProvider _participantsProvider;
        
        [SetUp]
        public void Setup()
        {
            _contextMock = new ContextMock();

            _activitySetRepositoryMock = new Mock<IActivitySetRepository>();
            _participantRepositoryMock = new Mock<IParticipantRepository>();
            _orderFactoryMock = new Mock<IOrderFactory>();
            _rcoProviderMock = new Mock<IRcoProvider>();
            _authorizationProviderMock = new Mock<IAuthorizationProvider>();
            _eventAggregatorMock = new Mock<IEventAggregator>();
            _participantBusinessMock = new Mock<IParticipantBusiness>();
            _changeLogProviderMock = new Mock<IChangeLogProvider>();

            _participantsProvider = new ParticipantsProvider(
                _activitySetRepositoryMock.Object,
                _participantRepositoryMock.Object,
                _orderFactoryMock.Object, 
                _rcoProviderMock.Object,
                _authorizationProviderMock.Object,
                _eventAggregatorMock.Object,
                _participantBusinessMock.Object,
                _changeLogProviderMock.Object,
                _contextMock.Object);
        }

        [Test]
        [TestCase(ScormLessonStatus.Complete, ParticipantStatusCodes.Completed)]
        [TestCase(ScormLessonStatus.Incomplete, ParticipantStatusCodes.InProgress)]
        [TestCase(ScormLessonStatus.NotAttempted, ParticipantStatusCodes.Enrolled)]
        public void UpdateStatus_ShouldUpdateStatusForAllParticipants(string performanceStatus, string participantStatus)
        {
            var performance = new ScormPerformance
            {
                Id = 1,
                RcoId = 2,
                UserId = 3,
                Status = performanceStatus
            };
            var courseRco = new Rco { Id = 1234, Context = "course" };
            var participant1 = new Participant { ActivityId = 1, UserId = performance.UserId };
            var participant2 = new Participant { ActivityId = 1, UserId = performance.UserId };
            var activity = new Activity { Id = 1, RcoCourseId = courseRco.Id };
            _contextMock.Object.ScormPerformances.Add(performance);
            _contextMock.Object.Participants.Add(participant1);
            _contextMock.Object.Participants.Add(participant2);
            _contextMock.Object.Activities.Add(activity);

            _rcoProviderMock.Setup(r => r.GetTopParent(performance.RcoId)).Returns(courseRco);
            
            _participantsProvider.UpdateStatus(performance.UserId, performance.RcoId);

            Assert.AreEqual(participantStatus, participant1.StatusCodeValue);
            Assert.AreEqual(participantStatus, participant2.StatusCodeValue);
        }

    }
}