﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Services.Agresso.Invoicing.Factories;
using Metier.Phoenix.Core.Data.Entities;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Services.Invoices.Agresso.Factories
{
    [TestFixture]
    public class AgressoOrderFactoryTests
    {
        private AgressoOrderFactory _factory;
        private Mock<IAgressoOrderDetailFactory> _agressoOrderDetailFactoryMock;
        private Mock<ICodeRepository> _codeRepositoryMock;
        
        [SetUp]
        public void Setup()
        {
            _agressoOrderDetailFactoryMock = new Mock<IAgressoOrderDetailFactory>();
            _codeRepositoryMock = new Mock<ICodeRepository>();
            _codeRepositoryMock.Setup(c => c.GetCodeValue(It.IsAny<int>())).Returns("NOK");
            
            _factory = new AgressoOrderFactory(_codeRepositoryMock.Object, _agressoOrderDetailFactoryMock.Object);
        }

        [Test]
        public void CreateOrder_MustIgnoreActivitiesWithoutFixedPrice()
        {
            var activities = new List<Activity> { new Activity() };

            var order = CreateTestOrder(activities);
            var agressoOrder = _factory.CreateOrder(order, "ExtermalCustomerId", "TestWorkOrder", "TestOrderNumber", null);
            var orderLines = agressoOrder.Order[0].Details;

            Assert.AreEqual(0, orderLines.Length);
        }

        [Test]
        public void CreateOrder_MustSetConstantValues()
        {
            var order = _factory.CreateOrder(CreateTestOrder(), "ExternalCustomerId", "", "", null);

            Assert.AreEqual("LM", order.Order[0].VoucherType);
            Assert.AreEqual("42", order.Order[0].TransType);
            Assert.AreEqual("BF", order.Order[0].Header.ReferenceCode[0].Code);
        }

        [Test]
        public void CreateOrder_MustSetValuesInHeader()
        {
            var instantBeforeTest = DateTime.Now;
            const string externalCustomerId = "ExtermalCustomerId";
            const string workOrder = "wo";
            var order = _factory.CreateOrder(CreateTestOrder(), externalCustomerId, workOrder, "", null);
            var header = order.Order[0].Header;

            Assert.AreEqual(workOrder, header.ReferenceCode.First(r => r.Code == "BF").Value);
            Assert.GreaterOrEqual(header.OrderDate, instantBeforeTest);
            Assert.IsTrue(header.OrderDateSpecified);
            Assert.IsTrue(header.ExchRateSpecified);
            Assert.AreEqual(1.00F, header.ExchRate);
            Assert.AreEqual(externalCustomerId, header.Buyer.BuyerNo);
            Assert.AreEqual(externalCustomerId, header.Buyer.MainBuyerNo);
        }

        [Test]
        public void CreateOrder_MustSetValuesOnOrder()
        {
            const string orderNumber = "orderno";
            var order = _factory.CreateOrder(CreateTestOrder(), "ExtermalCustomerId", "", orderNumber, null);

            Assert.AreEqual(orderNumber, order.Order[0].OrderNo);
            Assert.AreEqual(DateTime.Today.ToString("yyyyMM"), order.Order[0].Period);
        }

        private Order CreateTestOrder(List<Activity> activities = null, List<Participant> participants = null)
        {
            var order = new Order{YourRef = "YourRef", OurRef = "OurRef", YourOrder = "YourOrder"};
            if (activities != null)
            {
                order.ActivityOrderLines = activities.Select(a => new ActivityOrderLine
                {
                    Activity = a,
                    ActivityName = a.Name,
                    CurrencyCodeId = a.CurrencyCodeId,
                    Order = order,
                    Price= a.FixedPrice
                }).ToList();

            }

            if (participants != null)
            {
                order.ParticipantOrderLines = participants.Select(p => new ParticipantOrderLine
                {
                    Participant = p,
                    Price = p.UnitPrice,
                    CurrencyCodeId = p.CurrencyCodeId,
                    OnlinePaymentReference = p.OnlinePaymentReference,
                    ActivityId = p.ActivityId,
                    InfoText = p.User != null ? string.Concat(p.User.FirstName, " ", p.User.LastName, ", ", p.GetInfoTexts()) : p.GetInfoTexts(),
                    ActivityName = "Name of activity"

                }).ToList();
            }

            if(order.ActivityOrderLines == null) order.ActivityOrderLines = new List<ActivityOrderLine>();
            if (order.ParticipantOrderLines == null) order.ParticipantOrderLines = new List<ParticipantOrderLine>();

            return order;
        }
    }
}