﻿using System;
using System.Linq;
using Metier.Phoenix.Api.Factories;
using Metier.Phoenix.Api.Services.Agresso.Invoicing.Factories;
using Metier.Phoenix.Core.Data.Entities;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Services.Invoices.Agresso.Factories
{
    [TestFixture]
    public class AgressoOrderDetailFactoryTests
    {
        private ContextMock _contextMock;
        private Mock<IOrderFactory> _orderFactoryMock;
        
        private AgressoOrderDetailFactory _factory;
        
        [SetUp]
        public void Setup()
        {
            _orderFactoryMock = new Mock<IOrderFactory>();
            _contextMock = new ContextMock();
            _factory = new AgressoOrderDetailFactory(_contextMock.Object, _orderFactoryMock.Object);
        }

        [Test]
        public void GetDetailForActivity_MustSetValuesFromArticle()
        {
            var customerArticle = new CustomerArticle { Id = 1, ArticleCopyId = 2};
            var article = new Mock<Article>();
            article.Object.Id = 2;
            article.SetupGet(a => a.ArticleCode).Returns("code");

            _contextMock.Object.CustomerArticles.Add(customerArticle);
            _contextMock.Object.Articles.Add(article.Object);

            var activity = new Activity { CustomerArticleId = 1 };
            var order = new Order();
            var details = _factory.GetDetailForActivity(order, activity);

            Assert.AreEqual("code", details.BuyerProductCode);
        }

        [Test]
        public void GetDetailForActivity_MustSetValuesFromActivity()
        {
            var activity = new Activity
            {
                CustomerArticleId = SetCustomerArticleMock(),
                FixedPrice = 1234,
                Name = "title"
            };
            
            var order = new Order();

            var details = _factory.GetDetailForActivity(order, activity);

            Assert.AreEqual(1234, details.Price);
            Assert.AreEqual(1234, details.LineTotal);
            Assert.AreEqual("title (fixed price)", details.BuyerProductDescr);
        }

        [Test]
        public void GetDetailForActivity_GivenCreditNote_MustSetQuantityAndTotalNegative()
        {
            var activity = new Activity { CustomerArticleId = SetCustomerArticleMock(), FixedPrice = 1000 };
            var order = new Order { IsCreditNote = true };

            var details = _factory.GetDetailForActivity(order, activity);

            Assert.AreEqual(-1, details.Quantity);
            Assert.AreEqual(-1000, details.LineTotal);
            Assert.AreEqual(1000, details.Price);
        }

        [Test]
        public void GetDetailForActivity_GivenNonCreditNote_MustSetQuantityPositive()
        {
            var activity = new Activity { CustomerArticleId = SetCustomerArticleMock(), FixedPrice = 1000 };
            var order = new Order { IsCreditNote = false };

            var details = _factory.GetDetailForActivity(order, activity);

            Assert.AreEqual(1, details.Quantity);
            Assert.AreEqual(1000, details.LineTotal);
            Assert.AreEqual(1000, details.Price);
        }

        [Test]
        public void GetDetailForActivity_MustSetDetailInfo()
        {
            var activity = new Activity { CustomerArticleId = SetCustomerArticleMock() };
            var order = new Order();

            var details = _factory.GetDetailForActivity(order, activity);

            Assert.IsTrue(details.DetailInfo.UseLineTotal);
            Assert.IsTrue(details.DetailInfo.UseLineTotalSpecified);
        }

        [Test]
        public void GetDetailsForParticipants_MustGroupByActivityThenUnitPrice()
        {
            var customerArticleId = SetCustomerArticleMock();

            _contextMock.Object.Activities.Add(new Activity { Id = 1, CustomerArticleId = customerArticleId });
            _contextMock.Object.Activities.Add(new Activity { Id = 2, CustomerArticleId = customerArticleId });

            _contextMock.Object.ParticipantOrderLines.Add(new ParticipantOrderLine { Id = 1, ActivityId = 1, Price = 3000 });
            _contextMock.Object.ParticipantOrderLines.Add(new ParticipantOrderLine { Id = 2, ActivityId = 1, Price = 1000 });
            _contextMock.Object.ParticipantOrderLines.Add(new ParticipantOrderLine { Id = 3, ActivityId = 1, Price = 1000 });
            _contextMock.Object.ParticipantOrderLines.Add(new ParticipantOrderLine { Id = 4, ActivityId = 2, Price = 1000 });
            
            var participants = _contextMock.Object.ParticipantOrderLines.ToList();
            var order = new Order();

            var details = _factory.GetDetailsForParticipantOrderLines(order, participants).ToList();

            Assert.AreEqual(3, details.Count);
            Assert.AreEqual(3000, details[0].LineTotal);
            Assert.AreEqual(2000, details[1].LineTotal);
            Assert.AreEqual(1000, details[2].LineTotal);
        }

        [Test]
        public void GetDetailsForParticipants_MustSetValuesFromArticle()
        {
            var customerArticle = new CustomerArticle { Id = 1, ArticleCopyId = 2 };
            var article = new Mock<Article>();
            article.Object.Id = 2;
            article.SetupGet(a => a.ArticleCode).Returns("code");

            _contextMock.Object.CustomerArticles.Add(customerArticle);
            _contextMock.Object.Articles.Add(article.Object);

            _contextMock.Object.Activities.Add(new Activity { Id = 1, CustomerArticleId = customerArticle.Id });
            _contextMock.Object.ParticipantOrderLines.Add(new ParticipantOrderLine { Id = 1, ActivityId = 1, Price = 1000 });

            var participants = _contextMock.Object.ParticipantOrderLines.ToList();
            var order = new Order();

            var details = _factory.GetDetailsForParticipantOrderLines(order, participants).ToList();

            Assert.AreEqual("code", details[0].BuyerProductCode);
        }

        [Test]
        public void GetDetailsForParticipants_MustSetValuesFromActivity()
        {
            var customerArticleId = SetCustomerArticleMock();

            _contextMock.Object.Activities.Add(new Activity { Id = 1, CustomerArticleId = customerArticleId, Name = "title"});
            _contextMock.Object.ParticipantOrderLines.Add(new ParticipantOrderLine { Id = 1, ActivityId = 1, Price = 2000 });

            var participants = _contextMock.Object.ParticipantOrderLines.ToList();
            var order = new Order { IsCreditNote = true };

            var details = _factory.GetDetailsForParticipantOrderLines(order, participants).ToList();

            Assert.AreEqual(1, details.Count);
            Assert.AreEqual("title", details[0].BuyerProductDescr);
        }

        [Test]
        public void GetDetailsForParticipants_GivenCreditNote_MustSetNegativeQuantityAndNegativeTotal()
        {
            var customerArticleId = SetCustomerArticleMock();

            _contextMock.Object.Activities.Add(new Activity { Id = 1, CustomerArticleId = customerArticleId });
            
            _contextMock.Object.ParticipantOrderLines.Add(new ParticipantOrderLine { Id = 1, ActivityId = 1, Price = 2000 });
            _contextMock.Object.ParticipantOrderLines.Add(new ParticipantOrderLine { Id = 2, ActivityId = 1, Price = 2000 });

            var participants = _contextMock.Object.ParticipantOrderLines.ToList();
            var order = new Order { IsCreditNote = true };

            var details = _factory.GetDetailsForParticipantOrderLines(order, participants).ToList();

            Assert.AreEqual(1, details.Count);
            Assert.AreEqual(-4000, details[0].LineTotal);
            Assert.AreEqual(-2, details[0].Quantity);
            Assert.AreEqual(2000, details[0].Price);
        }

        [Test]
        public void GetDetailsForParticipants_GivenNonCreditNote_MustSetPostiveNumbers()
        {
            var customerArticleId = SetCustomerArticleMock();

            _contextMock.Object.Activities.Add(new Activity { Id = 1, CustomerArticleId = customerArticleId });
            
            _contextMock.Object.ParticipantOrderLines.Add(new ParticipantOrderLine { Id = 1, ActivityId = 1, Price = 2000 });
            _contextMock.Object.ParticipantOrderLines.Add(new ParticipantOrderLine { Id = 2, ActivityId = 1, Price = 2000 });

            var participants = _contextMock.Object.ParticipantOrderLines.ToList();
            var order = new Order { IsCreditNote = false };

            var details = _factory.GetDetailsForParticipantOrderLines(order, participants).ToList();

            Assert.AreEqual(1, details.Count);
            Assert.AreEqual(4000, details[0].LineTotal);
            Assert.AreEqual(2, details[0].Quantity);
            Assert.AreEqual(2000, details[0].Price);
        }

        [Test]
        public void GetDetailsForParticipants_MustSetProductSpecificationsByUser()
        {
            var customerArticleId = SetCustomerArticleMock();

            _contextMock.Object.Activities.Add(new Activity { Id = 1, CustomerArticleId = customerArticleId });
            
            _contextMock.Object.Participants.Add(new Participant { Id = 1, ActivityId = 1, UnitPrice = 2000, UserId = 1 });
            _contextMock.Object.Participants.Add(new Participant { Id = 2, ActivityId = 1, UnitPrice = 2000, UserId = 2 });

            _contextMock.Object.Users.Add(new User { Id = 1, FirstName = "first1", LastName = "last1"});
            _contextMock.Object.Users.Add(new User { Id = 2, FirstName = "first2", LastName = "last2" });

            var participants = _contextMock.Object.Participants.ToList();

            _orderFactoryMock.Setup(o => o.GetOrderLineText("first1", "last1", participants[0])).Returns("info1");
            _orderFactoryMock.Setup(o => o.GetOrderLineText("first2", "last2", participants[1])).Returns("info2");

            var order = new Order { IsCreditNote = false };

			var participantOrderLines = _contextMock.Object.ParticipantOrderLines.ToList();

            var details = _factory.GetDetailsForParticipantOrderLines(order, participantOrderLines).ToList();

            Assert.AreEqual(1, details.Count);
            Assert.AreEqual(1, details[0].ProductSpecification[0].SeqNo);
            Assert.AreEqual("info1", details[0].ProductSpecification[0].Info);
            Assert.AreEqual(2, details[0].ProductSpecification[1].SeqNo);
            Assert.AreEqual("info2", details[0].ProductSpecification[1].Info);
        }

        [Test]
        [TestCase("", Result = 0)]
        [TestCase("abc", Result = 1)]
        [TestCase("abcd", Result = 2)]
        [TestCase("abcdef", Result = 2)]
        [TestCase("abcdefg", Result = 3)]
        public int SplitStringByLength_GivenInput_ReturnsExpectedNumberOfString(string stringForSplitting)
        {
            var result = AgressoOrderDetailFactory.SplitByLength(stringForSplitting, 3);
            return result.Count();
        }

        [Test]
        [TestCase("I am 60 characters------------------------------------------", 0, Result = "I am 60 characters------------------------------------------")]
        [TestCase("I am 61 characters-------------------------------------------", 0, Result = "I am 61 characters------------------------------------------")]
        [TestCase("I am 61 characters-------------------------------------------", 1, Result = "-")]
        [TestCase("Per Olsen, --------------------------------------------------", 0, Result = "Per Olsen")]
        [TestCase("Per Olsen, --------------------------------------------------", 1, Result = "--------------------------------------------------")]
        [TestCase("Per Olsen, ------------------------------------------------------------", 1, Result = "------------------------------------------------------------")]
        [TestCase("Per Olsen, -----------------------------------------------------------|more than 60 additional fields characters", 2, Result = "more than 60 additional fields characters")]
        public string AdjustOrderLineToAgressoProductSpecifications_GivenOrderLineText_ReturnsExpectedResult(
            string orderlineText, int arrayElement)
        {
            var adjustedOrderlines = _factory.AdjustOrderLineToAgressoProductSpecifications(orderlineText);
            return adjustedOrderlines.ToArray()[arrayElement];
        }
        
        private int SetCustomerArticleMock()
        {
            var customerArticle = new CustomerArticle {Id = 1, ArticleCopyId = 2};
            var article = new ClassroomArticle {Id = customerArticle.ArticleCopyId };
            _contextMock.Object.CustomerArticles.Add(customerArticle);
            _contextMock.Object.Articles.Add(article);
            return customerArticle.Id;
        }
    }
}