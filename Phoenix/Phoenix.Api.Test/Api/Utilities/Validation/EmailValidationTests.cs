﻿using Metier.Phoenix.Core.Utilities.Validation;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Utilities.Validation
{
    [TestFixture]
    public class EmailValidationTests
    {
        [Test]
        [TestCase("a@a")]
        [TestCase("a@b.c")]
        [TestCase("a@b.co")]
        [TestCase("a@b.co.com")]
        [TestCase("a@b.abcdef")]
        [TestCase("a@b.abcdefg")]
        public void IsValidEmail_ValidWhen(string validDomain)
        {
            Assert.IsTrue(EmailValidation.IsValidEmail(validDomain));
        }

        [Test]
        [TestCase("a.")]
        [TestCase("@")]
        [TestCase("a@")]
        [TestCase("a@a.")]
        [TestCase("a@a,com")]
        [TestCase("a b@a.com")]
        [TestCase("a.com")]
        [TestCase("")]
        [TestCase(null)]
        public void IsValidEmail_InvalidWhen(string invalidDomain)
        {
            Assert.IsFalse(EmailValidation.IsValidEmail(invalidDomain));
        }
    }
}