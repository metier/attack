﻿using Metier.Phoenix.Core.Utilities.Validation;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Api.Utilities.Validation
{
    [TestFixture]
    public class DomainValidationTests
    {
        [Test]
        [TestCase("example.co")]
        [TestCase("example.abcdef")]
        [TestCase("example.co.com")]
        public void IsValidDomain_ValidWhen(string validDomain)
        {
            Assert.IsTrue(DomainValidation.IsValidDomain(validDomain));
        }

        [Test]
        [TestCase("nodot")]
        [TestCase(".blabla")]
        [TestCase("blabla.")]
        [TestCase("example.s")]
        [TestCase("example.abcdefg")]
        [TestCase("")]
        [TestCase(null)]
        public void IsValidDomain_InvalidWhen(string invalidDomain)
        {
            Assert.IsFalse(DomainValidation.IsValidDomain(invalidDomain));
        }
    }
}