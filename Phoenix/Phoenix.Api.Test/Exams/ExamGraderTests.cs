﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Business.ChangeLog;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Exams;
using Metier.Phoenix.Core.Exceptions;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Exams
{
    public class ExamGraderTests
    {
        private ContextMock _contextMock;
        private Mock<IChangeLogProvider> _changeLogProviderMock;
        private ExamGrader _examGrader;

        [SetUp]
        public void Setup()
        {
            _contextMock = new ContextMock();
            _changeLogProviderMock = new Mock<IChangeLogProvider>();
            _examGrader = new ExamGrader(_contextMock.Object, _changeLogProviderMock.Object);
        }

        [Test]
        public void IsCorrectAnswer_CorrectAnswerForTrueFalseQuestion_ReturnsTrue()
        {
            var question = new Question
            {
                Id = 1,
                Type = QuestionTypes.TrueFalse,
                CorrectBoolAnswer = true
            };
            var answer = new Answer
            {
                BoolAnswer = true,
                QuestionId = 1
            };

            var result = _examGrader.IsCorrectAnswer(question, answer);

            Assert.IsTrue(result);
        }

        [Test]
        public void IsCorrectAnswer_FalseAnswerForTrueFalseQuestion_ReturnsFalse()
        {
            var question = new Question
            {
                Id = 1,
                Type = QuestionTypes.TrueFalse,
                CorrectBoolAnswer = true
            };
            var answer = new Answer
            {
                BoolAnswer = false,
                QuestionId = 1
            };

            var result = _examGrader.IsCorrectAnswer(question, answer);

            Assert.IsFalse(result);
        }

        [Test]
        public void IsCorrectAnswer_NoSetAnswerForTrueFalseQuestion_ReturnsFalse()
        {
            var question = new Question
            {
                Id = 1,
                Type = QuestionTypes.TrueFalse,
                CorrectBoolAnswer = true
            };
            var answer = new Answer
            {
                BoolAnswer = null,
                QuestionId = 1
            };

            var result = _examGrader.IsCorrectAnswer(question, answer);

            Assert.IsFalse(result);
        }

        [Test]
        public void IsCorrectAnswer_QuestionMissingAnswerForTrueFalseQuestion_ThrowsException()
        {
            var question = new Question
            {
                Id = 1,
                Type = QuestionTypes.TrueFalse,
            };
            var answer = new Answer
            {
                BoolAnswer = true,
                QuestionId = 1
            };

            Assert.Throws(Is.TypeOf<ExamException>(), () => _examGrader.IsCorrectAnswer(question, answer));
        }

        [Test]
        public void IsCorrectAnswer_CorrectOptionsSelectedForMultipleChoiceQuestion_ReturnsTrue()
        {
            var question = new Question
            {
                Id = 1,
                Type = QuestionTypes.MultipleChoiceMultiple,
                QuestionOptions = new List<QuestionOption>
                {
                    new QuestionOption { Id = 1, IsCorrect = true },
                    new QuestionOption { Id = 2, IsCorrect = false },
                    new QuestionOption { Id = 3, IsCorrect = true }
                }
            };
            var answer = new Answer
            {
                QuestionId = 1,
                OptionsAnswers = new List<QuestionOption>
                {
                    new QuestionOption { Id = 1 },
                    new QuestionOption { Id = 3 }
                }
            };

            var result = _examGrader.IsCorrectAnswer(question, answer);

            Assert.IsTrue(result);
        }

        [Test]
        public void IsCorrectAnswer_TooManyOptionsSelectedForMultipleChoiceQuestion_ReturnsFalse()
        {
            var question = new Question
            {
                Id = 1,
                Type = QuestionTypes.MultipleChoiceMultiple,
                QuestionOptions = new List<QuestionOption>
                {
                    new QuestionOption { Id = 1, IsCorrect = true },
                    new QuestionOption { Id = 2, IsCorrect = false },
                    new QuestionOption { Id = 3, IsCorrect = true }
                }
            };
            var answer = new Answer
            {
                QuestionId = 1,
                OptionsAnswers = new List<QuestionOption>
                {
                    new QuestionOption { Id = 1 },
                    new QuestionOption { Id = 2 },
                    new QuestionOption { Id = 3 }
                }
            };

            var result = _examGrader.IsCorrectAnswer(question, answer);

            Assert.IsFalse(result);
        }

        [Test]
        public void IsCorrectAnswer_MissingCorrectOptionForMultipleChoiceQuestion_ReturnsFalse()
        {
            var question = new Question
            {
                Id = 1,
                Type = QuestionTypes.MultipleChoiceMultiple,
                QuestionOptions = new List<QuestionOption>
                {
                    new QuestionOption { Id = 1, IsCorrect = true },
                    new QuestionOption { Id = 2, IsCorrect = false },
                    new QuestionOption { Id = 3, IsCorrect = true }
                }
            };
            var answer = new Answer
            {
                QuestionId = 1,
                OptionsAnswers = new List<QuestionOption>
                {
                    new QuestionOption { Id = 1 }
                }
            };

            var result = _examGrader.IsCorrectAnswer(question, answer);

            Assert.IsFalse(result);
        }

        [Test]
        public void IsCorrectAnswer_NoCorrectOptionsInQuestionNoSelectedOptionsInAnswer_Returnstrue()
        {
            var question = new Question
            {
                Id = 1,
                Type = QuestionTypes.MultipleChoiceMultiple,
                QuestionOptions = new List<QuestionOption>
                {
                    new QuestionOption { Id = 1, IsCorrect = false },
                    new QuestionOption { Id = 2, IsCorrect = false },
                    new QuestionOption { Id = 3, IsCorrect = false }
                }
            };
            var answer = new Answer
            {
                QuestionId = 1,
                OptionsAnswers = new List<QuestionOption>()
            };

            var result = _examGrader.IsCorrectAnswer(question, answer);

            Assert.IsTrue(result);
        }

        [Test]
        public void IsCorrectAnswer_NoOptionsInQuestionNoSelectedOptionsInAnswer_Returnstrue()
        {
            var question = new Question
            {
                Id = 1,
                Type = QuestionTypes.MultipleChoiceMultiple,
                QuestionOptions = new List<QuestionOption>()
            };
            var answer = new Answer
            {
                QuestionId = 1,
                OptionsAnswers = new List<QuestionOption>()
            };

            var result = _examGrader.IsCorrectAnswer(question, answer);

            Assert.IsTrue(result);
        }

        [Test]
        public void IsCorrectAnswer_NoOptionsInQuestionSelectedOptionsInAnswer_ReturnsFalse()
        {
            var question = new Question
            {
                Id = 1,
                Type = QuestionTypes.MultipleChoiceMultiple,
                QuestionOptions = new List<QuestionOption>()
            };
            var answer = new Answer
            {
                QuestionId = 1,
                OptionsAnswers = new List<QuestionOption>
                {
                    new QuestionOption { Id = 1 }
                }
            };

            var result = _examGrader.IsCorrectAnswer(question, answer);

            Assert.IsFalse(result);
        }

        [Test]
        public void IsCorrectAnswer_UnknownQuestionTypeinQuestion_ThrowsException()
        {
            var question = new Question
            {
                Type = "Something unknown",
            };
            var answer = new Answer();

            Assert.Throws(Is.TypeOf<ExamException>(), () => _examGrader.IsCorrectAnswer(question, answer));
        }

        [Test]
        [TestCase(70000, "5", Result = "A")]
        [TestCase(70, "5", Result = "A")]
        [TestCase(65, "5", Result = "A")]
        [TestCase(35, "5", Result = "B")]
        [TestCase(5, "5", Result = "Failed")]
        [TestCase(0, "5", Result = "Failed")]
        [TestCase(70, "UnknownEcts", Result = "A")]
        [TestCase(65, "UnknownEcts", Result = "B")]
        [TestCase(35, "UnknownEcts", Result = "Failed")]
        public string DetermineGrade_GivenAchievedScore_ShouldReturnExpectedGrade(int achievedScore, string ects)
        {
            const int totalScoreExam = 70;

            var unorderedGradeScales = new List<GradeScale>
            {
                new GradeScale {Ects = "5", Grade = "B", Id = 1, MinPercentage = 0.3m},
                new GradeScale {Ects = "5", Grade = "A", Id = 1, MinPercentage = 0.7m},
                new GradeScale {Ects = "5", Grade = "Failed", Id = 1, MinPercentage = 0},
                new GradeScale {Ects = "DEFAULT", Grade = "B", Id = 1, MinPercentage = 0.6m},
                new GradeScale {Ects = "DEFAULT", Grade = "A", Id = 1, MinPercentage = 0.95m},
                new GradeScale {Ects = "DEFAULT", Grade = "Failed", Id = 1, MinPercentage = 0}
            };
            foreach (var gradeScale in unorderedGradeScales)
            {
                _contextMock.Object.GradeScales.Add(gradeScale);
            }

            return _examGrader.DetermineGrade(ects, achievedScore, totalScoreExam);
        }

        [Test]
        public void DetermineGrade_TotalScoreForExamIsZero_ReturnsAnEmptyGrade()
        {
            const int totalScoreExam = 0;

            var grade = _examGrader.DetermineGrade("DEFAULT", 1, totalScoreExam);

            Assert.AreEqual(string.Empty, grade);
        }
    }
}