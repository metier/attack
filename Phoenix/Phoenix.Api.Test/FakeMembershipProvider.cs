﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using Metier.Phoenix.Api.Security;

namespace Metier.Phoenix.UnitTests
{
    public class FakeMembershipProvider : SqlMembershipProvider, IPhoenixMemberShipProvider
    {
    }
}
