﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Accounting;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Mail.Monitoring
{
    [TestFixture]
    public class MailAccountingTests
    {
        private ContextMock _contextMock;
        private MailAccounting _mailAccounting;

        [SetUp]
        public void Setup()
        {
            _contextMock = new ContextMock();
            _mailAccounting = new MailAccounting(_contextMock.Object);
        }

        [Test]
        public void CreateMail_GivenValidInput_SavesNewMailToDatabase()
        {
            var autoMailMessage = GetValidAutoMailMessage();
            
            Assert.AreEqual(0, _contextMock.Object.Mails.Count());
            _mailAccounting.CreateMail(autoMailMessage);
            Assert.AreEqual(1, _contextMock.Object.Mails.Count());
        }

        [Test]
        public void CreateMail_GivenValidInput_SavesNewMailWithCreatedStatus()
        {
            var autoMailMessage = GetValidAutoMailMessage();

            _mailAccounting.CreateMail(autoMailMessage);

            var mail = _contextMock.Object.Mails.First();
            Assert.AreEqual(1, mail.MailStatuses.Count);
            Assert.AreEqual(MailStatusType.Created, mail.MailStatuses.First().Status);
        }

        [Test]
        public void CreateMail_GivenValidInput_MailCreatedAndCreatedStatusAreSame()
        {
            var autoMailMessage = GetValidAutoMailMessage();

            _mailAccounting.CreateMail(autoMailMessage);

            var mail = _contextMock.Object.Mails.First();
            Assert.AreEqual(mail.Created, mail.MailStatuses.First().StatusTime);
        }

        [Test]
        public void CreateMail_GivenValidInput_SetsRecipientFromAutoMailMessage()
        {
            var autoMailMessage = GetValidAutoMailMessage();

            _mailAccounting.CreateMail(autoMailMessage);

            var mail = _contextMock.Object.Mails.First();
            Assert.AreEqual(autoMailMessage.RecipientEmail, mail.RecipientEmail);
        }

        [Test]
        public void CreateMail_GivenValidInput_SetsSubjectFromAutoMailMessage()
        {
            var autoMailMessage = GetValidAutoMailMessage();

            _mailAccounting.CreateMail(autoMailMessage);

            var mail = _contextMock.Object.Mails.First();
            Assert.AreEqual(autoMailMessage.Subject, mail.Subject);
        }

        [Test]
        public void CreateMail_GivenValidInput_SetsUserIdFromAutoMailMessage()
        {
            var autoMailMessage = GetValidAutoMailMessage();

            _mailAccounting.CreateMail(autoMailMessage);

            var mail = _contextMock.Object.Mails.First();
            Assert.AreEqual(autoMailMessage.UserId, mail.UserId);
        }

        [Test]
        public void CreateMail_GivenValidInput_SetsParticipantIdFromAutoMailMessage()
        {
            var autoMailMessage = GetValidAutoMailMessage();

            _mailAccounting.CreateMail(autoMailMessage);

            var mail = _contextMock.Object.Mails.First();
            Assert.AreEqual(autoMailMessage.ParticipantId, mail.ParticipantId);
        }

        [Test]
        public void CreateMail_GivenValidInput_SetsActivityIdFromAutoMailMessage()
        {
            var autoMailMessage = GetValidAutoMailMessage();

            _mailAccounting.CreateMail(autoMailMessage);

            var mail = _contextMock.Object.Mails.First();
            Assert.AreEqual(autoMailMessage.ActivityId, mail.ActivityId);
        }

        [Test]
        public void CreateMail_ShouldAddSerializedAutoMailMessageToMail()
        {
            var autoMailMessage = GetValidAutoMailMessage();
            var serializedMessage = JsonConvert.SerializeObject(autoMailMessage);

            _mailAccounting.CreateMail(autoMailMessage);

            Assert.AreEqual(serializedMessage, _contextMock.Object.Mails.First().SerializedMailMessage.SerializedMessage);
        }

        [Test]
        public void UpdateStatus_GivenExistingMail_LatestStatusEntryShouldEqualSetStatus()
        {
            const int mailId = 123;
            var mail = new Phoenix.Core.Data.Entities.Mail
            {
                Id = mailId,
                MailStatuses =
                    new List<MailStatus>
                    {
                        new MailStatus { Status = MailStatusType.Created, StatusTime = DateTime.UtcNow - TimeSpan.FromSeconds(1)}
                    }
            };
            _contextMock.Object.Mails.Add(mail);

            _mailAccounting.UpdateStatus(mailId, MailStatusType.Sent);

            var contextEntry = _contextMock.Object.Mails.First(m => m.Id == mailId);
            var latestStatus = contextEntry.MailStatuses.OrderByDescending(s => s.StatusTime).First();
            Assert.AreEqual(MailStatusType.Sent, latestStatus.Status);
        }

        [Test]
        public void UpdateStatus_GivenNonExistingMail_ShouldThrowArgumentException()
        {
            Assert.Throws<ArgumentException>(() => _mailAccounting.UpdateStatus(123, MailStatusType.Sent));
        }

        private AutoMailMessage GetValidAutoMailMessage()
        {
            return new AutoMailMessage
            {
                Body = @"<!doctype html>",
                MessageType = MessageType.PasswordResetRequest,
                RecipientEmail = "recipient@test.com",
                Subject = "I am a unit test mail message",
                UserId = 1234,
                ParticipantId = 5678,
                ActivityId = 9012
            };
        }
    }
}