﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Core.Business;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Mail.FieldFactories;
using Metier.Phoenix.Mail.Formatters;
using Metier.Phoenix.Mail.Helpers;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Mail.FieldFactories
{
    [TestFixture]
    public class ParticipantFieldsFactoryTests
    {
        private ParticipantFieldsFactory _factory;

        private ContextMock _context;
        private Mock<IAddressFormatter> _addressFormatterMock;
        private Mock<IElearningCourseHelper> _resolveElearningRegistrationsMock;
        private Mock<IParticipantBusiness> _participantBusinessMock;

        [SetUp]
        public void Setup()
        {
            _context = new ContextMock();
            _addressFormatterMock = new Mock<IAddressFormatter>();
            _resolveElearningRegistrationsMock = new Mock<IElearningCourseHelper>();
            _participantBusinessMock = new Mock<IParticipantBusiness>();

            _factory = new ParticipantFieldsFactory(_context.Object, _addressFormatterMock.Object, _resolveElearningRegistrationsMock.Object, _participantBusinessMock.Object);
        }

        [Test]
        public void CreateFields_ShouldReturnActivitySetTitle()
        {
            var participant = CreateParticipant();
            var participantActivitySet = _context.Object.ActivitySets.First(aset => aset.Id == participant.ActivitySetId);

            var field = GetField(participant, "activityset-title");

            Assert.AreEqual(participantActivitySet.Name, field.Value);
        }
        
        [Test]
        public void CreateFields_ShouldReturnActivityTitle()
        {
            var participant = CreateParticipant();
            var participantActivity = _context.Object.Activities.First(a => a.Id == participant.ActivityId);
            
            var field = GetField(participant, "activity-title");

            Assert.AreEqual(participantActivity.Name, field.Value);
        }

        [Test]
        public void CreateFields_ShouldReturnActivityStartDate()
        {
            var participant = CreateParticipant();
            
            var field = GetField(participant, "activity-start-date");

            Assert.AreEqual("07.04.2014", field.Value);
        }

        [Test]
        public void CreateFields_ShouldReturnActivityEndDate()
        {
            var participant = CreateParticipant();

            var field = GetField(participant, "activity-end-date");
            
            Assert.AreEqual("08.04.2014", field.Value);
        }

        [Test]
        public void CreateFields_ShouldReturnActivityStartTime()
        {
            var participant = CreateParticipant();

            var field = GetField(participant, "activity-start-time");

            Assert.AreEqual("10:00", field.Value);
        }

        [Test]
        public void CreateFields_ShouldReturnActivityEndTime()
        {
            var participant = CreateParticipant();

            var field = GetField(participant, "activity-end-time");

            Assert.AreEqual("09:00", field.Value);
        }

        [Test]
        public void CreateFields_ShouldReturnActivityPeriods()
        {
            const string expected = "<p><strong>Dato:</strong> 07.04.2014 - 07.04.2014<br/><strong>Tid:</strong> 10:00 - 11:00</p><p><strong>Dato:</strong> 08.04.2014 - 08.04.2014<br/><strong>Tid:</strong> 08:00 - 09:00</p>";
            var participant = CreateParticipant();

            var field = GetField(participant, "activity-periods");

            Assert.AreEqual(expected, field.Value);
        }

        [Test]
        public void CreateFields_ShouldReturnInstructor()
        {
            var participant = CreateParticipant();
            var instructor = _context.Object.Activities.First().Resources.First(r => r.ResourceType.Name == "Instructor");
            var instructorField = GetField(participant, "instructor");
            var hasInstructorField = GetField(participant, "instructor-visibility");

            Assert.AreEqual(instructor.Name, instructorField.Value);
            Assert.IsNullOrEmpty(hasInstructorField.Value);
        }

        [Test]
        public void CreateFields_ShouldNotReturnInstructor()
        {
            var participant = CreateParticipant();
            _context.Object.Activities.First().Resources = new List<Resource>(); // No resources
            var instructorField = GetField(participant, "instructor");
            var hasInstructorField = GetField(participant, "instructor-visibility");

            Assert.IsEmpty(instructorField.Value);
            Assert.AreEqual("style=\"display:none !important; font-size: 0; max-height: 0; line-height: 0; mso-hide: all; \"", hasInstructorField.Value);
        }

        [Test]
        public void CreateFields_ShouldReturnActivityAddress()
        {
            var participant = CreateParticipant();
            var location = _context.Object.Activities.First().Resources.First(r => r.ResourceType.Name == "Location");
            _addressFormatterMock.Setup(a => a.GetAddressFromResource(location)).Returns("The address");
            var locationField = GetField(participant, "activity-address");
            var hasLocationField = GetField(participant, "activity-address-visibility");

            Assert.AreEqual("The address", locationField.Value);
            Assert.IsNullOrEmpty(hasLocationField.Value);
        }

        [Test]
        public void CreateFields_ShouldNotReturnLocation()
        {
            var participant = CreateParticipant();
            _addressFormatterMock.Setup(a => a.GetAddressFromResource(It.IsAny<Resource>())).Returns("The address");
            _context.Object.Activities.First().Resources = new List<Resource>(); // No resources
            var locationField = GetField(participant, "activity-address");
            var hasLocationField = GetField(participant, "activity-address-visibility");

            Assert.IsEmpty(locationField.Value);
            Assert.AreEqual("style=\"display:none !important; font-size: 0; max-height: 0; line-height: 0; mso-hide: all; \"", hasLocationField.Value);
        }

        [Test]
        public void CreateFields_ShouldReturnECoach()
        {
            var participant = CreateParticipant();
            var field = GetField(participant, "e-coach");
            var hasECoachfield = GetField(participant, "e-coach-visibility");

            Assert.AreEqual(string.Empty, hasECoachfield.Value);
            Assert.AreEqual("Kari Nordmann", field.Value);
        }

        [Test]
        public void CreateFields_ShouldNotReturnECoach()
        {
            var participant = CreateParticipant();
            _context.Object.Activities.First().Resources = new List<Resource>(); // No resources
            var field = GetField(participant, "e-coach");
            var hasECoachfield = GetField(participant, "e-coach-visibility");

            Assert.AreEqual("style=\"display:none !important; font-size: 0; max-height: 0; line-height: 0; mso-hide: all; \"", hasECoachfield.Value);
            Assert.AreEqual(string.Empty, field.Value);
        }
        
        private KeyValuePair<string, string> GetField(Participant participant, string fieldName)
        {
            var fields = _factory.CreateFields(participant.Id, "NO");
            return fields.First(f => f.Key == fieldName);
        }

        private Participant CreateParticipant()
        {
            const int userId = 1;
            const int customerId = 2;
            const int activityId = 3;
            const int activitySetId = 4;
            const string firstName = "Terje";

            var user = new User
            {
                Id = userId,
                FirstName = firstName,
                CustomerId = customerId,
                Customer = new Customer
                {
                    Id = customerId, 
                    ParticipantInfoDefinition = new ParticipantInfoDefinition{ CustomerPortalUrl = "http://www.vg.no" }
                }
            };
            _context.Object.Users.Add(user);
            
            var searchableUser = new SearchableUser
            {
                Id = userId,
                FirstName = firstName
            };
            _context.Object.SearchableUsers.Add(searchableUser);
            var activity = new Activity
            {
                Id = activityId,
                Name = "Activity title",
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod{ ActivityId = activityId, Start = DateTimeOffset.Parse("2014-04-08 08:00:00.0000000 +09:00"), End = DateTimeOffset.Parse("2014-04-08 09:00:00.0000000 +09:00")},
                    new ActivityPeriod{ ActivityId = activityId, Start = DateTimeOffset.Parse("2014-04-07 10:00:00.0000000 +09:00"), End = DateTimeOffset.Parse("2014-04-07 11:00:00.0000000 +09:00")}
                },
                Resources = new List<Resource>
                {
                    CreateLocationResource(),
                    CreateInstructorResource(),
                    CreateEcoachResource()
                }
            };
            _context.Object.Activities.Add(activity);

            var activitySet = new ActivitySet
            {
                Id = activitySetId,
                Name = "Activity set title"
            };
            _context.Object.ActivitySets.Add(activitySet);

            var participant = new Participant
            {
                UserId = userId,
                ActivityId = activityId,
                ActivitySetId = activitySetId,
                StatusCodeValue = "INPROGRESS"
            };
            _context.Object.Participants.Add(participant);
            return participant;
        }

        private Resource CreateEcoachResource()
        {
            return new Resource
            {
                ResourceType = new ResourceTypes { Name = "E-Coach" },
                Name = "Kari Nordmann"
            };
        }

        private Resource CreateInstructorResource()
        {
            return new Resource
            {
                ResourceType = new ResourceTypes { Name = "Instructor" },
                Name = "Test Instruktor"
            };
        }

        private Resource CreateLocationResource()
        {
            return new Resource
            {
                ResourceType = new ResourceTypes { Name = "Location" },
                Addresses = new List<ResourceAddress> {  new ResourceAddress() }
            };
        }
    }
}