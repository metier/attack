﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Mail.FieldFactories;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Mail.FieldFactories
{
    [TestFixture]
    public class UserFieldsFactoryTests
    {
        private UserFieldsFactory _factory;

        private ContextMock _context;

        [SetUp]
        public void Setup()
        {
            _context = new ContextMock();
            _factory = new UserFieldsFactory(_context.Object);
        }

        [Test]
        public void CreateFields_ShouldReturnFirstName()
        {
            var user = CreateUser();
            var field = GetField(user, "first-name");

            Assert.AreEqual(user.FirstName, field.Value);
        }

        [Test]
        public void CreateFields_ShouldReturnFullName()
        {
            var user = CreateUser();
            var field = GetField(user, "user-fullname");

            Assert.AreEqual("Per Olsen", field.Value);
        }

        [Test]
        public void CreateFields_ShouldReturnEmail()
        {
            var user = CreateUser();
            var field = GetField(user, "user-email");

            Assert.AreEqual("per.epost@test.com", field.Value);
        }

        [Test]
        public void CreateFields_ShouldReturnUserName()
        {
            var user = CreateUser();
            var field = GetField(user, "username");

            Assert.AreEqual("per.bruker@test.com", field.Value);
        }

        [Test]
        public void CreateFields_ShouldReturnSupervisor()
        {
            var user = CreateUser();
            var customLeadingTextSupervisor = user.Customer.CustomLeadingTexts.First(cl => cl.LeadingTextId == LeadingTextIds.Supervisor);

            user.UserInfoElements.Add(new UserInfoElement
            {
                LeadingTextId = customLeadingTextSupervisor.LeadingTextId,
                LeadingText = customLeadingTextSupervisor.LeadingText,
                InfoText = "Sune Super"
            });

            var hasSupervisorField = GetField(user, "participant-supervisor-visibility");
            var supervisorField = GetField(user, "participant-supervisor");


            Assert.AreEqual("Sune Super", supervisorField.Value);
            Assert.IsNullOrEmpty(hasSupervisorField.Value);
        }

        [Test]
        public void CreateFields_ShouldNotReturnSupervisor()
        {
            var user = CreateUser();
            user.UserInfoElements = new List<UserInfoElement>();

            var hasSupervisorField = GetField(user, "participant-supervisor-visibility");
            var supervisorField = GetField(user, "participant-supervisor");


            Assert.IsNullOrEmpty(supervisorField.Value);
            Assert.AreEqual("style=\"display:none !important; font-size: 0; max-height: 0; line-height: 0; mso-hide: all; \"", hasSupervisorField.Value);
        }
        
        private KeyValuePair<string, string> GetField(User user, string fieldName)
        {
            var fields = _factory.CreateFields(user.Id);
            return fields.First(f => f.Key == fieldName);
        }

        private User CreateUser()
        {
            var customer = new Customer
            {
                Id = 1,
                CustomLeadingTexts =
                    new List<CustomLeadingText>
                    {
                        new CustomLeadingText {LeadingTextId = LeadingTextIds.Supervisor, IsVisible = true, Id = 10}
                    }
            };
            _context.Object.Customers.Add(customer);
            var user = new User
            {
                Id = 1,
                FirstName = "Per",
                LastName = "Olsen",
                Customer = customer,
                CustomerId = customer.Id
            };
            _context.Object.Users.Add(user);
            var searchableUser = new SearchableUser
            {
                Id = user.Id,
                UserName = "per.bruker@test.com",
                Email = "per.epost@test.com",
                CustomerId = customer.Id
            };
            _context.Object.SearchableUsers.Add(searchableUser);
            return user;
        }
    }
}