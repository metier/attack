﻿using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Mail.FieldFactories;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Mail.FieldFactories
{
    [TestFixture]
    public class CustomerFieldsFactoryTests
    {
        private CustomerFieldsFactory _factory;

        private ContextMock _context;

        [SetUp]
        public void Setup()
        {
            _context = new ContextMock();
            _factory = new CustomerFieldsFactory(_context.Object);
        }

        [Test]
        public void CreateFields_ShouldReturnCustomerName()
        {
            var customer = CreateCustomer();
            var field = GetField(customer, "customer-name");

            Assert.AreEqual("The name of the customer", field.Value);
        }

        [Test]
        public void CreateFields_ShouldReturnCustomerPortal()
        {
            var customer = CreateCustomer();
            var field = GetField(customer, "customer-portal");

            Assert.AreEqual(customer.ParticipantInfoDefinition.CustomerPortalUrl, field.Value);
        }

        private KeyValuePair<string, string> GetField(Customer customer, string fieldName)
        {
            var fields = _factory.CreateFields(customer.Id);
            return fields.First(f => f.Key == fieldName);
        }

        private Customer CreateCustomer()
        {
            var distributor = new Distributor
            {
                Id = 1,
                AutomailSender = "noreply@mymetier.net"
            };

            var customer = new Customer
            {
                Id = 1,
                DistributorId = 1,
                Name = "The name of the customer",
                ParticipantInfoDefinition = new ParticipantInfoDefinition
                {
                    CustomerPortalUrl = "http://www.vg.no"
                }
            };

            _context.Object.Distributors.Add(distributor);
            _context.Object.Customers.Add(customer);

            return customer;
        }
    }
}