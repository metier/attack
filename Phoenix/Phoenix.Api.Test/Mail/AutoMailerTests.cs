﻿using System;
using System.Net.Mail;
using System.Transactions;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Accounting;
using Metier.Phoenix.Mail.Authorization;
using Metier.Phoenix.Mail.Configuration;
using Metier.Phoenix.Mail.Configuration.ConfigurationSections;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Mail
{
    [TestFixture]
    public class AutoMailerTests
    {
        private AutoMailer _autoMailer;

        private Mock<ISmtpClientProxy> _smtpClientProxyMock;
        private Mock<IAutoMailAuthorization> _authorizationMock;
        private Mock<IMailAccounting> _accountingMock;
        
        [SetUp]
        public void Setup()
        {
            _authorizationMock = new Mock<IAutoMailAuthorization>();
            _accountingMock = new Mock<IMailAccounting>();
            _smtpClientProxyMock = new Mock<ISmtpClientProxy>();
            _authorizationMock.Setup(a => a.CanSend(It.IsAny<string>())).Returns(true);
            var automailConfigurationMock = new Mock<IAutoMailConfiguration>();
            automailConfigurationMock.Setup(ac => ac.Sender).Returns("noreply@unittest.com");

            _autoMailer = new AutoMailer(_smtpClientProxyMock.Object, _authorizationMock.Object, _accountingMock.Object, automailConfigurationMock.Object);
        }

        [Test]
        public void Send_SingleMessage_MustSendMailAfterTransactionComplete()
        {
            var msg1 = new MailMessage();
           
            using (var transaction = new TransactionScope())
            {
                _autoMailer.Send(msg1, "whatever@example.com", 1);

                _smtpClientProxyMock.Verify(c => c.Send(msg1), Times.Never);

                transaction.Complete();
            }
            _smtpClientProxyMock.Verify(c => c.Send(msg1), Times.Once);
        }        
        
        [Test]
        public void Send_WithoutTransactionScope_MustSendMail()
        {
            var msg1 = new MailMessage();
            _autoMailer.Send(msg1, "whatever@example.com", 1);
            _smtpClientProxyMock.Verify(c => c.Send(msg1), Times.Once);
        }

        [Test]
        public void Send_SingleMessage_MustNotSendMailWhenTransactionDoesNotComplete()
        {
            var msg1 = new MailMessage();
           
            using (var transaction = new TransactionScope())
            {
                _autoMailer.Send(msg1, "whatever@example.com", 1);
                _smtpClientProxyMock.Verify(c => c.Send(msg1), Times.Never);
            }
            _smtpClientProxyMock.Verify(c => c.Send(msg1), Times.Never);
        }

        [Test]
        public void Send_MultipleMessages_MustSendMailsAfterTransactionCompletes()
        {
            var msg1 = new MailMessage();
            var msg2 = new MailMessage();
           
            using (var transaction = new TransactionScope())
            {
                _autoMailer.Send(msg1, "whatever@example.com", 1);
                _autoMailer.Send(msg2, "whatever@example.com", 2);

                _smtpClientProxyMock.Verify(c => c.Send(msg1), Times.Never);
                _smtpClientProxyMock.Verify(c => c.Send(msg2), Times.Never);

                transaction.Complete();
            }
            _smtpClientProxyMock.Verify(c => c.Send(msg1), Times.Once);
            _smtpClientProxyMock.Verify(c => c.Send(msg2), Times.Once);
        }

        [Test]
        public void Send_NestedTransactions_MustNotSendMailsWhenInnerTransactionDoesNotComplete()
        {
            var msg1 = new MailMessage();
            var msg2 = new MailMessage();

            try
            {
                using (var transaction = new TransactionScope())
                {
                    using (var transaction2 = new TransactionScope())
                    {
                        _autoMailer.Send(msg1, "whatever@example.com", 1);
                    }

                    _autoMailer.Send(msg2, "whatever@example.com", 2);

                    _smtpClientProxyMock.Verify(c => c.Send(msg1), Times.Never);
                    _smtpClientProxyMock.Verify(c => c.Send(msg2), Times.Never);

                    transaction.Complete();
                }
            }
            catch (TransactionAbortedException)
            {
                // Swallow exception
            }
            
            _smtpClientProxyMock.Verify(c => c.Send(msg1), Times.Never);
            _smtpClientProxyMock.Verify(c => c.Send(msg2), Times.Never);
        }

        [Test]
        public void Send_NestedTransactions_MustSendMailsAfterOuterTransactionComplete()
        {
            var msg1 = new MailMessage();
            var msg2 = new MailMessage();
           
            using (var transaction = new TransactionScope())
            {
                using (var transaction2 = new TransactionScope())
                {
                    _autoMailer.Send(msg1, "whatever@example.com", 1);
                    transaction2.Complete();
                }

                _autoMailer.Send(msg2, "whatever@example.com", 2);

                _smtpClientProxyMock.Verify(c => c.Send(msg1), Times.Never);
                _smtpClientProxyMock.Verify(c => c.Send(msg2), Times.Never);

                transaction.Complete();
            }
            _smtpClientProxyMock.Verify(c => c.Send(msg1), Times.Once);
            _smtpClientProxyMock.Verify(c => c.Send(msg2), Times.Once);
        }

        [Test]
        public void Send_SequentialTransactions_MustSendMailsAfterEachTransactionCompletes()
        {
            var msg1 = new MailMessage();
            var msg2 = new MailMessage();

            using (var transaction = new TransactionScope())
            {
                _autoMailer.Send(msg1, "whatever@example.com", 1);
                
                _smtpClientProxyMock.Verify(c => c.Send(msg1), Times.Never);

                transaction.Complete();
            }
            _smtpClientProxyMock.Verify(c => c.Send(msg1), Times.Once);

            using (var transaction = new TransactionScope())
            {
                _autoMailer.Send(msg2, "whatever@example.com", 2);

                _smtpClientProxyMock.Verify(c => c.Send(msg1), Times.Once);
                _smtpClientProxyMock.Verify(c => c.Send(msg2), Times.Never);

                transaction.Complete();
            }
            _smtpClientProxyMock.Verify(c => c.Send(msg1), Times.Once);
            _smtpClientProxyMock.Verify(c => c.Send(msg2), Times.Once);
        }

        [Test]
        public void Send_SequentialTransactions_MustOnlySendMailForCompletedTransactions()
        {
            var msg1 = new MailMessage();
            var msg2 = new MailMessage();

            using (new TransactionScope())
            {
                _autoMailer.Send(msg1, "whatever@example.com", 1);
                
                _smtpClientProxyMock.Verify(c => c.Send(msg1), Times.Never);
            }
            _smtpClientProxyMock.Verify(c => c.Send(msg1), Times.Never);

            using (var transaction = new TransactionScope())
            {
                _autoMailer.Send(msg2, "whatever@example.com", 2);

                _smtpClientProxyMock.Verify(c => c.Send(msg1), Times.Never);
                _smtpClientProxyMock.Verify(c => c.Send(msg2), Times.Never);

                transaction.Complete();
            }
            _smtpClientProxyMock.Verify(c => c.Send(msg1), Times.Never);
            _smtpClientProxyMock.Verify(c => c.Send(msg2), Times.Once);
        }

        [Test]
        public void Get_GivenValidId_MustReturnDeserializedAutoMailMessage()
        {
            const int mailId = 1234;
            var autoMailMessage = new AutoMailMessage
            {
                Body = "The content",
                MessageType = MessageType.UserRegistrationNotification,
                RecipientEmail = "someone@example.com",
                Subject = "The subject"
            };

            var mail = new Phoenix.Core.Data.Entities.Mail
            {
                SerializedMailMessage = new SerializedMailMessage
                {
                    SerializedMessage = JsonConvert.SerializeObject(autoMailMessage)
                }
            };
            _accountingMock.Setup(a => a.GetMail(mailId)).Returns(mail);

            var result = _autoMailer.Get(mailId);

            Assert.AreEqual(autoMailMessage.Body, result.Body);
            Assert.AreEqual(autoMailMessage.MessageType, result.MessageType);
            Assert.AreEqual(autoMailMessage.RecipientEmail, result.RecipientEmail);
            Assert.AreEqual(autoMailMessage.Subject, result.Subject);
        }

		[Test]
		public void SendTestMessage()
		{
			//new Metier.Phoenix.Mail.Templates.ReminderElearning.ReminderElearningFactory ( )
			const int mailId = 1234;
			var autoMailMessage = new AutoMailMessage
			{
				Body = "The content",
				MessageType = MessageType.UserRegistrationNotification,
				RecipientEmail = "someone@example.com",
				Subject = "The subject"
			};

			var mail = new Phoenix.Core.Data.Entities.Mail
			{
				SerializedMailMessage = new SerializedMailMessage
				{
					SerializedMessage = JsonConvert.SerializeObject(autoMailMessage)
				}
			};
			_accountingMock.Setup(a => a.GetMail(mailId)).Returns(mail);

			var result = _autoMailer.Get(mailId);

			Assert.AreEqual(autoMailMessage.Body, result.Body);
			Assert.AreEqual(autoMailMessage.MessageType, result.MessageType);
			Assert.AreEqual(autoMailMessage.RecipientEmail, result.RecipientEmail);
			Assert.AreEqual(autoMailMessage.Subject, result.Subject);
		}
	}
}