﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Mail.Formatters;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Mail.Formatters
{
    [TestFixture]
    public class AddressFormatterTests
    {
        private ContextMock _contextMock;

        private AddressFormatter _addressFormatter;

        [SetUp]
        public void Setup()
        {
            _contextMock = new ContextMock();
            _addressFormatter = new AddressFormatter(_contextMock.Object);
        }

        [Test]
        public void GetAddressFromResource_GivenNullResource_MustReturnEmptyString()
        {
            var address = _addressFormatter.GetAddressFromResource(null);
            Assert.IsEmpty(address);
        }

        [Test]
        public void GetAddressFromResource_GivenResourceWithoutAddress_MustReturnEmptyString()
        {
            var resource = new Resource
            {
                Addresses = new List<ResourceAddress>()
            };
            var address = _addressFormatter.GetAddressFromResource(resource);
            Assert.IsEmpty(address);
        }

        [Test]
        public void GetAddressFromResource_GivenResourceWithAddress_MustFormatAddress()
        {
            var resource = new Resource
            {
                Addresses = new List<ResourceAddress>
                {
                    new ResourceAddress
                    {
                        StreetAddress = "Testveien 1",
                        StreetAddress2 = "PB 5678",
                        ZipCode = "1234",
                        City = "Testbyen",
                        Country = "NO"
                    }
                }
            };
            _contextMock.Object.Codes.Add(new Code { CodeTypeId = 4, Value = "NO", Description = "Norge" });

            var address = _addressFormatter.GetAddressFromResource(resource);
            Assert.AreEqual(address, "Testveien 1, PB 5678, 1234 Testbyen, Norge");
        }

        [Test]
        public void GetAddressFromResource_GivenResourceWithAddressWithoutStreetAddress2_MustFormatAddress()
        {
            var resource = new Resource
            {
                Addresses = new List<ResourceAddress>
                {
                    new ResourceAddress
                    {
                        StreetAddress = "Testveien 1",
                        ZipCode = "1234",
                        City = "Testbyen",
                        Country = "NO"
                    }
                }
            };
            _contextMock.Object.Codes.Add(new Code { CodeTypeId = 4, Value = "NO", Description = "Norge" });

            var address = _addressFormatter.GetAddressFromResource(resource);
            Assert.AreEqual(address, "Testveien 1, 1234 Testbyen, Norge");
        }

        [Test]
        public void GetAddressFromResource_GivenResourceWithAddressWithoutStreetAddress_MustFormatAddress()
        {
            var resource = new Resource
            {
                Addresses = new List<ResourceAddress>
                {
                    new ResourceAddress
                    {
                        ZipCode = "1234",
                        City = "Testbyen",
                        Country = "NO"
                    }
                }
            };
            _contextMock.Object.Codes.Add(new Code { CodeTypeId = 4, Value = "NO", Description = "Norge" });

            var address = _addressFormatter.GetAddressFromResource(resource);
            Assert.AreEqual(address, "1234 Testbyen, Norge");
        }


        [Test]
        public void GetAddressFromResource_GivenResourceWithAddressWithoutCountry_MustFormatAddress()
        {
            var resource = new Resource
            {
                Addresses = new List<ResourceAddress>
                {
                    new ResourceAddress
                    {
                        StreetAddress = "Testveien 1",
                        ZipCode = "1234",
                        City = "Testbyen"
                    }
                }
            };
            var address = _addressFormatter.GetAddressFromResource(resource);
            Assert.AreEqual(address, "Testveien 1, 1234 Testbyen");
        }
    }
}