﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Mail.Configuration;
using Metier.Phoenix.Mail.Configuration.ConfigurationSections;
using Metier.Phoenix.Mail.Helpers;
using Metier.Phoenix.Mail.FieldFactories;
using Metier.Phoenix.Mail.Templates;
using Metier.Phoenix.Mail.Templates.PasswordResetRequest;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Mail.Templates.PasswordResetRequest
{
    [TestFixture]
    public class PasswordResetRequestFactoryTests
    {
        private const string PasswordResetFormUrl = "http://somedomain/resetpassword";
        private const string RecipientEmailAddress = "sendmessagehere@test.com";
        private const int UserId = 1234;
        private PasswordResetRequestMessageFactory _userAccountAutoMailFactory;
        private Mock<IMailMessageFactory> _mailMessageFactoryMock;
		private Mock<IUserFieldsFactory> _iUserFieldsFactoryMock;
		private Mock<ILanguageHelper> _languageHelperMock;
        private ContextMock _contextMock;
        private User _psoUser;


        [SetUp]
        public void Setup()
        {
            _mailMessageFactoryMock = new Mock<IMailMessageFactory>();
			_iUserFieldsFactoryMock = new Mock<IUserFieldsFactory>();
			_languageHelperMock = new Mock<ILanguageHelper>();
            _contextMock = new ContextMock();
            _contextMock.Object.Users.Add(new User { Id = UserId, CustomerId = 4567 });
            _contextMock.Object.Customers.Add(new Customer { Id = 4567, DistributorId = 1194 });

            _psoUser = new User { Id = 2233, CustomerId = 4455 };
            _contextMock.Object.Users.Add(_psoUser);
            _contextMock.Object.Customers.Add(new Customer { Id = 4455, DistributorId = 10000 });

            _contextMock.Object.Distributors.Add(new Distributor { Id = 1194, Name = "Metier Academy Norway", AutomailSender = "noreply@mymetier.net" });
            _contextMock.Object.Distributors.Add(new Distributor { Id = 10000, Name = "PSO", AutomailSender = "noreply@psotraining.com" });

            _userAccountAutoMailFactory = new PasswordResetRequestMessageFactory(_contextMock.Object, _mailMessageFactoryMock.Object, _iUserFieldsFactoryMock.Object, _languageHelperMock.Object);
        }

        [Test]
        public void CreatePasswordResetMessage_ValidArguments_ShouldSetRecipientEmailArgumentAsRecipientOfMessage()
        {
            var message = _userAccountAutoMailFactory.CreatePasswordResetMessage(Guid.NewGuid(), RecipientEmailAddress, UserId, PasswordResetFormUrl);

            Assert.AreEqual(RecipientEmailAddress, message.RecipientEmail);
        }

        [Test]
        public void CreatePasswordResetMessage_ValidArguments_ShouldSetUserId()
        {
            var message = _userAccountAutoMailFactory.CreatePasswordResetMessage(Guid.NewGuid(), RecipientEmailAddress, UserId, PasswordResetFormUrl);

            Assert.AreEqual(UserId, message.UserId);
        }

        [Test]
        public void CreatePasswordResetMessage_ValidArguments_ShouldSubjectFromMessageFactory()
        {
            _mailMessageFactoryMock.Setup(f => f.GetSubject(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Distributor>(), null)).Returns("the subject");
            var message = _userAccountAutoMailFactory.CreatePasswordResetMessage(Guid.NewGuid(), RecipientEmailAddress, UserId, PasswordResetFormUrl);

            Assert.AreEqual("the subject", message.Subject);
        }

        [Test]
        public void CreatePasswordResetMessage_ValidArguments_ShouldSetContentToResultFromTemplateFormatter()
        {
            const string resultFromTemplateFormatter = "This should be message content";
            _mailMessageFactoryMock.Setup(t => t.GetBody(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>(), It.IsAny<Distributor>(), It.IsAny<string>())).Returns(resultFromTemplateFormatter);

            var message = _userAccountAutoMailFactory.CreatePasswordResetMessage(Guid.NewGuid(), RecipientEmailAddress, UserId, PasswordResetFormUrl);

            Assert.AreEqual(resultFromTemplateFormatter, message.Body);
        }

        [Test]
        public void GetBody_GivenCustomTemplateForDistributor_ShouldReturnCustomMailBody()
        {
            var autoMailConfigurationMock = new Mock<IAutoMailConfiguration>();
            autoMailConfigurationMock.Setup(c => c.Urls).Returns(new UrlsConfigurationElementCollection());
            var mailMessageFactory = new MailMessageFactory(autoMailConfigurationMock.Object);

            var passwordResetRequestMessageFactory = new PasswordResetRequestMessageFactory(_contextMock.Object, mailMessageFactory, _iUserFieldsFactoryMock.Object, _languageHelperMock.Object);

            var message = passwordResetRequestMessageFactory.CreatePasswordResetMessage(Guid.NewGuid(), RecipientEmailAddress, _psoUser.Id, PasswordResetFormUrl);

            Assert.GreaterOrEqual(Regex.Matches(message.Body, "PSOeTraining").Count, 1);
        }

        [Test]
        public void GetBody_GivenCustomTemplateForDistributorAndLanguage_ShouldReturnCustomMailBody()
        {
            var autoMailConfigurationMock = new Mock<IAutoMailConfiguration>();
            autoMailConfigurationMock.Setup(c => c.Urls).Returns(new UrlsConfigurationElementCollection());
            var mailMessageFactory = new MailMessageFactory(autoMailConfigurationMock.Object);
            _languageHelperMock.Setup(m => m.GetPreferredLanguageIdentifier(It.IsAny<User>(), It.IsAny<Customer>())).Returns("EN");

            var passwordResetRequestMessageFactory = new PasswordResetRequestMessageFactory(_contextMock.Object, mailMessageFactory, _iUserFieldsFactoryMock.Object, _languageHelperMock.Object);

            var message = passwordResetRequestMessageFactory.CreatePasswordResetMessage(Guid.NewGuid(), RecipientEmailAddress, _psoUser.Id, PasswordResetFormUrl);

            Assert.GreaterOrEqual(Regex.Matches(message.Body, "PSOeTraining").Count, 1);
        }
    }
}