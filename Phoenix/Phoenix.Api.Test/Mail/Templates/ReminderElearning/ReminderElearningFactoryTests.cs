﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Mail.FieldFactories;
using Metier.Phoenix.Mail.Helpers;
using Metier.Phoenix.Mail.Templates;
using Metier.Phoenix.Mail.Templates.ReminderElearning;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Mail.Templates.ReminderElearning
{
    [TestFixture]
    public class ReminderElearningFactoryTests
    {
        private const string ResourceAheadName = "ReminderWorkshopExam";
        private readonly string _templateResourceAheadName = typeof(ReminderElearningFactory).Namespace + ".ReminderWorkshopExamMessage.html";

        private ReminderElearningFactory _reminderElearningFactory;
        private ContextMock _context;

        private string _userEmail;
        private Mock<IMailMessageFactory> _mailMessageFactoryMock;
        private Mock<IParticipantFieldsFactory> _particpantFieldsFactoryMock;
        private Mock<ICustomerFieldsFactory> _customerFieldsFactoryMock;
        private Mock<IUserFieldsFactory> _userFieldsFactoryMock;

        [SetUp]
        public void Setup()
        {
            _particpantFieldsFactoryMock = new Mock<IParticipantFieldsFactory>();
            _particpantFieldsFactoryMock.Setup(f => f.CreateFields(It.IsAny<int>(), It.IsAny<string>())).Returns(new Dictionary<string, string>());

            _customerFieldsFactoryMock = new Mock<ICustomerFieldsFactory>();
            _customerFieldsFactoryMock.Setup(f => f.CreateFields(It.IsAny<int>())).Returns(new Dictionary<string, string>());

            _userFieldsFactoryMock = new Mock<IUserFieldsFactory>();
            _userFieldsFactoryMock.Setup(f => f.CreateFields(It.IsAny<int>())).Returns(new Dictionary<string, string>());

            _context = new ContextMock();
            _mailMessageFactoryMock = new Mock<IMailMessageFactory>();

            var resolveLanguage = new Mock<ILanguageHelper>();
            resolveLanguage.Setup(r => r.GetPreferredLanguageIdentifier(It.IsAny<User>(), It.IsAny<Customer>())).Returns("NO");

            _reminderElearningFactory = new ReminderElearningFactory(_context.Object, 
                _userFieldsFactoryMock.Object,
                _particpantFieldsFactoryMock.Object, 
                resolveLanguage.Object, 
                _mailMessageFactoryMock.Object);
        }
        [Test]
        public void CreateMessage_ValidArguments_ShouldSetRecipientToUserAssociatedToParticipant()
        {
            var participant = CreateParticipant();
            var message = _reminderElearningFactory.CreateMessage(participant, ResourceAheadName, _templateResourceAheadName, MessageType.ReminderElearning);
            Assert.AreEqual(_userEmail, message.RecipientEmail);
        }

        [Test]
        public void CreateMessage_ValidArguments_ShouldSetUserIdToUserAssociatedToParticipant()
        {
            var participant = CreateParticipant();
            var message = _reminderElearningFactory.CreateMessage(participant, ResourceAheadName, _templateResourceAheadName, MessageType.ReminderElearning);
            Assert.AreEqual(participant.UserId, message.UserId);
        }

        [Test]
        public void CreateMessage_ValidArguments_ShouldSetParticipantId()
        {
            var participant = CreateParticipant();
            var message = _reminderElearningFactory.CreateMessage(participant, ResourceAheadName, _templateResourceAheadName, MessageType.ReminderElearning);
            Assert.AreEqual(participant.Id, message.ParticipantId);
        }

        private Participant CreateParticipant()
        {
            const int userId = 1;
            const int customerId = 2;
            const int activityId = 3;
            const int distributorId = 1194;

            _userEmail = "user.email@test.com";
            var user = new User
            {
                Id = userId,
                CustomerId = customerId,
                Customer = new Customer
                {
                    Id = customerId,
                    DistributorId = distributorId
                }
            };
            _context.Object.Users.Add(user);
            _context.Object.Customers.Add(user.Customer);

            var searchableUser = new SearchableUser
            {
                Id = userId,
                DistributorId = distributorId,
                Email = _userEmail
            };
            _context.Object.SearchableUsers.Add(searchableUser);

            _context.Object.Distributors.Add(new Distributor { Id = distributorId, Name = "Metier Academy Norway", AutomailSender = "noreply@mymetier.net" });

            return new Participant
            {
                UserId = userId,
                ActivityId = activityId
            };
        }
    }
}