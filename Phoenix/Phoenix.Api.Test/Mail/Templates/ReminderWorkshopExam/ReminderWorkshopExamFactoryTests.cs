﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Mail.FieldFactories;
using Metier.Phoenix.Mail.Helpers;
using Metier.Phoenix.Mail.Templates;
using Metier.Phoenix.Mail.Templates.ReminderWorkshopExam;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Mail.Templates.ReminderWorkshopExam
{
    [TestFixture]
    public class ReminderWorkshopExamFactoryTests
    {
        private ReminderWorkshopExamFactory _reminderWorkshopExamFactory;
        private ContextMock _context;

        private string _userEmail;
        private Mock<IMailMessageFactory> _mailMessageFactoryMock;
        private Mock<IParticipantFieldsFactory> _particpantFieldsFactoryMock;
        private Mock<ICustomerFieldsFactory> _customerFieldsFactoryMock;
        private Mock<IUserFieldsFactory> _userFieldsFactoryMock;

        [SetUp]
        public void Setup()
        {
            _particpantFieldsFactoryMock = new Mock<IParticipantFieldsFactory>();
            _particpantFieldsFactoryMock.Setup(f => f.CreateFields(It.IsAny<int>(), It.IsAny<string>())).Returns(new Dictionary<string, string>());

            _customerFieldsFactoryMock = new Mock<ICustomerFieldsFactory>();
            _customerFieldsFactoryMock.Setup(f => f.CreateFields(It.IsAny<int>())).Returns(new Dictionary<string, string>());

            _userFieldsFactoryMock = new Mock<IUserFieldsFactory>();
            _userFieldsFactoryMock.Setup(f => f.CreateFields(It.IsAny<int>())).Returns(new Dictionary<string, string>());

            _context = new ContextMock();
            _mailMessageFactoryMock = new Mock<IMailMessageFactory>();

            var resolveLanguage = new Mock<ILanguageHelper>();
            resolveLanguage.Setup(r => r.GetPreferredLanguageIdentifier(It.IsAny<User>(), It.IsAny<Customer>())).Returns("NO");

            _reminderWorkshopExamFactory = new ReminderWorkshopExamFactory(_mailMessageFactoryMock.Object,
                                                                        _particpantFieldsFactoryMock.Object,
                                                                        _customerFieldsFactoryMock.Object,
                                                                        _userFieldsFactoryMock.Object,
                                                                        resolveLanguage.Object,
                                                                        _context.Object);
        }

        [Test]
        public void CreateMessage_ValidArguments_ShouldSetRecipientToUserAssociatedToParticipant()
        {
            var participant = CreateParticipant();
            var message = _reminderWorkshopExamFactory.CreateMessage(participant);
            Assert.AreEqual(_userEmail, message.RecipientEmail);
        }

        [Test]
        public void CreateMessage_ValidArguments_ShouldSetUserIdToUserAssociatedToParticipant()
        {
            var participant = CreateParticipant();
            var message = _reminderWorkshopExamFactory.CreateMessage(participant);
            Assert.AreEqual(participant.UserId, message.UserId);
        }

        [Test]
        public void CreateMessage_ValidArguments_ShouldSetParticipantId()
        {
            var participant = CreateParticipant();
            var message = _reminderWorkshopExamFactory.CreateMessage(participant);
            Assert.AreEqual(participant.Id, message.ParticipantId);
        }

        private Participant CreateParticipant()
        {
            const int userId = 1;
            const int customerId = 2;
            const int activityId = 3;
            const int distributorId = 1194;

            _userEmail = "user.email@test.com";
            var user = new User
            {
                Id = userId,
                CustomerId = customerId,
                Customer = new Customer
                {
                    Id = customerId,
                    DistributorId = distributorId
                }
            };
            _context.Object.Users.Add(user);

            var searchableUser = new SearchableUser
            {
                Id = userId,
                DistributorId = distributorId,
                Email = _userEmail
            };
            _context.Object.SearchableUsers.Add(searchableUser);

            _context.Object.Distributors.Add(new Distributor { Id = 1194, Name = "Metier Academy Norway", AutomailSender = "noreply@mymetier.net" });

            return new Participant
            {
                UserId = userId,
                ActivityId = activityId
            };
        }
    }
}