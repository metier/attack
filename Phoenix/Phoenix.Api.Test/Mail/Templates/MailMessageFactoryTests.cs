﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Mail.Configuration;
using Metier.Phoenix.Mail.Configuration.ConfigurationSections;
using Metier.Phoenix.Mail.Templates;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Mail.Templates
{
    [TestFixture]
    public class MailMessageFactoryTests
    {
        private Mock<IAutoMailConfiguration> _autoMailConfigurationMock;

        private MailMessageFactory _mailMessageFactory;
        private string _templateResourceName;
        private Distributor _distributor;

        [SetUp]
        public void SetUp()
        {
            _autoMailConfigurationMock = new Mock<IAutoMailConfiguration>();
            _autoMailConfigurationMock.Setup(c => c.Urls).Returns(new UrlsConfigurationElementCollection());
            _templateResourceName = typeof(MailMessageFactory).Namespace + ".TestTemplate.html";
            _mailMessageFactory = new MailMessageFactory(_autoMailConfigurationMock.Object);
            _distributor = new Distributor {Id = 1194, Name = "Metier Academy Norway"};
        }

        [Test]
        public void GetSubject_GivenResourceKeyAndValidLanguageIdentifier_ReturnsSubjectInCorrectLanguage()
        {
            var subject = _mailMessageFactory.GetSubject("UserRegistrationNotification", "NO", _distributor);
            Assert.AreEqual("Velkommen til myMetier", subject);
        }

        [Test]
        public void GetSubject_GivenResourceKeyAndUnknownLanguageIdentifier_ReturnsSubjectInDefaultLanguage()
        {
            var subject = _mailMessageFactory.GetSubject("UserRegistrationNotification", "", _distributor);
            Assert.AreEqual("myMetier: Your Account Has Been Created", subject);
        }

        [Test]
        public void GetSubject_GivenKeywordedResourceAndNoDictionary_ReturnsSubjectContainingKeywords()
        {
            var subject = _mailMessageFactory.GetSubject("EnrollmentConfirmation", "NO", _distributor);
            Assert.AreEqual("myMetier: Påmeldingsbekreftelse for {activity-title}", subject);
        }

        [Test]
        public void GetSubject_GivenKeywordedResourceAndDictionary_ReplacesKeywordsInSubjectWithValues()
        {
            const string activityTitle = "Aktivitetstittel";

            var subject = _mailMessageFactory.GetSubject("EnrollmentConfirmation", "NO", _distributor,
                new Dictionary<string, string>
                {
                    { "activity-title", activityTitle }
                });

            Assert.AreEqual(string.Format("myMetier: Påmeldingsbekreftelse for {0}", activityTitle), subject);
        }

        [Test]
        public void GetBody_GivenExistingTemplate_ShouldReturnTemplateAsString()
        {
            var result = _mailMessageFactory.GetBody(_templateResourceName, new Dictionary<string, string>{{"Something", "Something"}});

            Assert.IsNotNullOrEmpty(result);
        }

        [Test]
        public void GetBody_GivenMatchingTemplateFields_ShouldMergeIntoTemplateResult()
        {
            const string multipleMergeText = "This text should occur twice";
            const string singleMergeText = "While this text should occur once";

            var result = _mailMessageFactory.GetBody(_templateResourceName, new Dictionary<string, string>
            {
                { "multipleOccurrenceTemplateField", multipleMergeText },
                { "singleOccurrenceTemplateField", singleMergeText }
            });

            Assert.AreEqual(2, Regex.Matches(result, multipleMergeText).Count);
            Assert.AreEqual(1, Regex.Matches(result, singleMergeText).Count);
        }

        [Test]
        public void GetBody_GivenNonMatchingTemplateFields_ShouldReturnTemplateWithTemplateFieldPlaceholders()
        {
            const string templateFieldText = "This will not be in template";

            var result = _mailMessageFactory.GetBody(_templateResourceName, new Dictionary<string, string>
            {
                { "iAmNotATemplateField", templateFieldText },
            });

            Assert.AreEqual(2, Regex.Matches(result, "{multipleOccurrenceTemplateField}").Count);
            Assert.AreEqual(1, Regex.Matches(result, "{singleOccurrenceTemplateField}").Count);
            Assert.AreEqual(0, Regex.Matches(result, templateFieldText).Count);
        }

        [Test]
        public void GetBody_GivenTemplateResourceNameNotInFormatterAssembly_ThrowsArgumentException()
        {
            Assert.Throws<ArgumentException>(() => _mailMessageFactory.GetBody("IAmNotAValidAssemblyResourceName", new Dictionary<string, string>{{"Something", "Something"}}));
        }


    }
}