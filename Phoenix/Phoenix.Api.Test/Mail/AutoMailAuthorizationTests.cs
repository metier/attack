﻿using System.Collections.Generic;
using Metier.Phoenix.Mail.Authorization;
using Metier.Phoenix.Mail.Configuration;
using Metier.Phoenix.Mail.Configuration.ConfigurationSections;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Mail
{
    [TestFixture]
    public class AutoMailAuthorizationTests
    {
        private Mock<IAutoMailConfiguration> _configurationMock;
        private AutoMailAuthorization _autoMailAuthorizationPermitter;
        private Mock<WhiteListConfigurationElement> _whiteListElementMock;
        private ContextMock _contextMock;

        [SetUp]
        public void Setup()
        {
            _configurationMock = new Mock<IAutoMailConfiguration>();
            _configurationMock.Setup(c => c.AutoMailEnabled).Returns(true);

            _whiteListElementMock = new Mock<WhiteListConfigurationElement>();
            _whiteListElementMock.Setup(w => w.Email).Returns("*");
            var whiteListMock = new Mock<IWhiteListConfigurationElementCollection>();

            var whiteListEnumerator = (new List<WhiteListConfigurationElement> { _whiteListElementMock.Object }).GetEnumerator();
            whiteListMock.Setup(w => w.GetEnumerator()).Returns(whiteListEnumerator);

            _configurationMock.Setup(c => c.AutoMailWhiteList).Returns(whiteListMock.Object);

            _contextMock = new ContextMock();

            _autoMailAuthorizationPermitter = new AutoMailAuthorization(_configurationMock.Object, _contextMock.Object);
        }

        [Test]
        public void CanSend_ValidEmail_ReturnsTrue()
        {
            var canSend = _autoMailAuthorizationPermitter.CanSend("valid.email@test.com");
            Assert.IsTrue(canSend);
        }

        [Test]
        public void CanSend_ValidEmailAutoMailDisabled_ReturnsFalse()
        {
            _configurationMock.Setup(c => c.AutoMailEnabled).Returns(false);

            var canSend = _autoMailAuthorizationPermitter.CanSend("valid.email@test.com");
            Assert.IsFalse(canSend);
        }

        [Test]
        public void CanSend_InValidEmail_ReturnsFalse()
        {
            var canSend = _autoMailAuthorizationPermitter.CanSend("invalid.emailtest.com");
            Assert.IsFalse(canSend);
        }

        [Test]
        public void CanSend_ValidEmailMatchingWhiteList_ReturnsTrue()
        {
            _whiteListElementMock.Setup(w => w.Email).Returns("*@mydomain.com");

            var canSend = _autoMailAuthorizationPermitter.CanSend("valid.email@mydomain.com");
            Assert.IsTrue(canSend);
        }

        [Test]
        public void CanSend_ValidEmailNotWhiteListed_ReturnsFalse()
        {
            _whiteListElementMock.Setup(w => w.Email).Returns("*@notyourdomain.com");

            var canSend = _autoMailAuthorizationPermitter.CanSend("valid.email@mydomain.com");
            Assert.IsFalse(canSend);
        }

        [Test]
        [TestCase("valid.email@email.com", true)]
        [TestCase("notavalidemailaddress.com", false)]
        public void IsValidEmailAddress_GivenInvalidRecipientAddress_ShouldReturnFalse(string emailAddress, bool expectedResult)
        {
            var canSend = _autoMailAuthorizationPermitter.IsValidEmailAddresses(emailAddress);
            Assert.AreEqual(expectedResult, canSend);
        }

        [Test]
        [TestCase("*", "amivalid@question.com", true)]
        [TestCase("*@question.com", "amivalid@question.com", true)]
        [TestCase("*@anotherdomain.com", "amivalid@question.com", false)]
        [TestCase("*@*.com", "amivalid@question.com", true)]
        [TestCase("*@*.com", "amivalid@question.com.net", false)]
        [TestCase("*question*", "amivalid@question.net", true)]
        [TestCase("*metier@question.com", "somethingmetier@question.com", true)]
        [TestCase("*metier@question.com", "somethingmetNOTier@question.com", false)]
        public void IsRecipientWhiteListed_GivenWhiteListAddress_ShouldReturnExpectedResult(string whiteListAddress, string recipientEmail, bool expectedResult)
        {
            var result = _autoMailAuthorizationPermitter.IsWhiteListMatch(whiteListAddress, recipientEmail);

            Assert.IsTrue(expectedResult == result);
        }
    }
}