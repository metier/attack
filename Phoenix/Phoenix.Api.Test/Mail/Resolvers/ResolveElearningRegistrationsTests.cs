﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Core.Business.DateAndTime;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Extensions;
using Metier.Phoenix.Core.Utilities.Sorting;
using Metier.Phoenix.Mail.Helpers;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Mail.Resolvers
{
    [TestFixture]
    public class ResolveElearningRegistrationsTests
    {
        private ContextMock _context;
        private ElearningCourseHelper _resolver;

        [SetUp]
        public void Setup()
        {
            _context = new ContextMock();
            var sortNumberedStringMock = new Mock<ISortNumberedStrings>();
            sortNumberedStringMock.Setup(s => s.PadInitialNumbersForSorting(It.IsAny<string>(), It.IsAny<bool>()))
                .Returns((string input, bool isFirst) => input);

            _resolver = new ElearningCourseHelper(_context.Object, sortNumberedStringMock.Object);
        }

        [Test]
        public void GetWeeklyLessons_NumberOfLessonsEqualNumberOfWeeks()
        {
            var weeks = 2;
            var rcos = new List<Rco>
            {
                new Rco {Name = "A"},
                new Rco {Name = "B"}
            };

            var week3 = new IsoWeek(3, 2016);
            var week4 = new IsoWeek(4, 2016);
            var startDate = new DateTime(2016, 1, 20);

            var result = _resolver.GetWeeklyLessons(rcos, weeks, startDate);

            Assert.IsTrue(week3.Equals(result.Keys.ElementAt(0)));
            Assert.AreSame(rcos[0], result.FirstOrDefault(x=> week3.Equals(x.Key)).Value[0]);

            Assert.IsTrue(week4.Equals(result.Keys.ElementAt(1)));
            Assert.AreSame(rcos[1], result.FirstOrDefault(x=> week4.Equals(x.Key)).Value[0]);
        }

        [Test]
        public void GetWeeklyLessons_NumberOfWeeksGreaterThanNumberOfLessons_OverTwiceTheNumberOfLessons()
        {
            var weeks = 5;
            var rcos = new List<Rco>
            {
                new Rco {Id = 1},
                new Rco {Id = 2}
            };

            var week3 = new IsoWeek(3, 2016);
            var week6 = new IsoWeek(6, 2016);
            var startDate = new DateTime(2016, 1, 11);  //Week 2

            var result = _resolver.GetWeeklyLessons(rcos, weeks, startDate);

            Assert.IsTrue(week3.Equals(result.Keys.ElementAt(0)));
            Assert.AreSame(rcos[0], result.FirstOrDefault(x => week3.Equals(x.Key)).Value[0]);

            Assert.IsTrue(week6.Equals(result.Keys.ElementAt(1)));
            Assert.AreSame(rcos[1], result.FirstOrDefault(x => week6.Equals(x.Key)).Value[0]);
        }

        [Test]
        public void GetWeeklyLessons_NumberOfWeeksMoreThanTwoTimesGreaterThanNumberOfLessons_OverTwiceTheNumberOfLessons()
        {
            var weeks = 15;
            var rcos = new List<Rco>
            {
                new Rco {Id = 1},
                new Rco {Id = 2},
                new Rco {Id = 3}
            };

            var week5 = new IsoWeek(5, 2016);
            var week10 = new IsoWeek(10, 2016);
            var week15 = new IsoWeek(15, 2016);

            var startDate = new DateTime(2016, 1, 4);  //Week 1 - 2016

            var result = _resolver.GetWeeklyLessons(rcos, weeks, startDate);

            Assert.IsTrue(week5.Equals(result.Keys.ElementAt(0)));
            Assert.AreSame(rcos[0], result.FirstOrDefault(x => week5.Equals(x.Key)).Value[0]);

            Assert.IsTrue(week10.Equals(result.Keys.ElementAt(1)));
            Assert.AreSame(rcos[1], result.FirstOrDefault(x => week10.Equals(x.Key)).Value[0]);

            Assert.IsTrue(week15.Equals(result.Keys.ElementAt(2)));
            Assert.AreSame(rcos[2], result.FirstOrDefault(x => week15.Equals(x.Key)).Value[0]);
        }

        [Test]
        public void GetWeeklyLessons_NumberOfWeeksMoreThanUnevenNumberOfLessons_OverTwiceTheNumberOfLessons()
        {
            var weeks = 16;
            var rcos = new List<Rco>
            {
                new Rco {Id = 1},
                new Rco {Id = 2},
                new Rco {Id = 3}
            };

            var week5 = new IsoWeek(5, 2016);
            var week10 = new IsoWeek(10, 2016);
            var week16 = new IsoWeek(16, 2016);

            var startDate = new DateTime(2016, 1, 4);  //Week 1 - 2016

            var result = _resolver.GetWeeklyLessons(rcos, weeks, startDate);

            Assert.IsTrue(week5.Equals(result.Keys.ElementAt(0)));
            Assert.AreSame(rcos[0], result.FirstOrDefault(x => week5.Equals(x.Key)).Value[0]);

            Assert.IsTrue(week10.Equals(result.Keys.ElementAt(1)));
            Assert.AreSame(rcos[1], result.FirstOrDefault(x => week10.Equals(x.Key)).Value[0]);

            Assert.IsTrue(week16.Equals(result.Keys.ElementAt(2)));
            Assert.AreSame(rcos[2], result.FirstOrDefault(x => week16.Equals(x.Key)).Value[0]);
        }

        [Test]
        public void GetWeeklyLessons_NumberOfWeeksGreaterThanNumberOfLessons_NumberOfLessonsInBetweenNumberOfWeeks()
        {
            var weeks = 5;
            var rcos = new List<Rco>
            {
                new Rco {Id = 1},
                new Rco {Id = 2},
                new Rco {Id = 3}
            };

            var week3 = new IsoWeek(3, 2016);
            var week5 = new IsoWeek(5, 2016);
            var week7 = new IsoWeek(7, 2016);
            var startDate = new DateTime(2016, 1, 20); //Week 3

            var result = _resolver.GetWeeklyLessons(rcos, weeks, startDate);

            Assert.IsTrue(week3.Equals(result.Keys.ElementAt(0)));
            Assert.AreSame(rcos[0], result.FirstOrDefault(x => week3.Equals(x.Key)).Value[0]);

            Assert.IsTrue(week5.Equals(result.Keys.ElementAt(1)));
            Assert.AreSame(rcos[1], result.FirstOrDefault(x => week5.Equals(x.Key)).Value[0]);

            Assert.IsTrue(week7.Equals(result.Keys.ElementAt(2)));
            Assert.AreSame(rcos[2], result.FirstOrDefault(x => week7.Equals(x.Key)).Value[0]);
        }

        [Test]
        public void GetWeeklyLessons_NumberOfWeeksGreaterThanNumberOfLessons_NumberOfLessonsCloseToNumberOfWeeks()
        {
            var weeks = 5;
            var rcos = new List<Rco>
            {
                new Rco {Id = 1},
                new Rco {Id = 2},
                new Rco {Id = 3},
                new Rco {Id = 4}
            };

            var week3 = new IsoWeek(3, 2016);
            var week4 = new IsoWeek(4, 2016);
            var week5 = new IsoWeek(5, 2016);
            var week7 = new IsoWeek(7, 2016);
            var startDate = new DateTime(2016, 1, 20);

            var result = _resolver.GetWeeklyLessons(rcos, weeks, startDate);

            Assert.IsTrue(week3.Equals(result.Keys.ElementAt(0)));
            Assert.AreSame(rcos[0], result.FirstOrDefault(x => week3.Equals(x.Key)).Value[0]);

            Assert.IsTrue(week4.Equals(result.Keys.ElementAt(1)));
            Assert.AreSame(rcos[1], result.FirstOrDefault(x => week4.Equals(x.Key)).Value[0]);

            Assert.IsTrue(week5.Equals(result.Keys.ElementAt(2)));
            Assert.AreSame(rcos[2], result.FirstOrDefault(x => week5.Equals(x.Key)).Value[0]);

            Assert.IsTrue(week7.Equals(result.Keys.ElementAt(3)));
            Assert.AreSame(rcos[3], result.FirstOrDefault(x => week7.Equals(x.Key)).Value[0]);
        }

        [Test]
        public void GetWeeklyLessons_NumberOfLessonsGreaterThanNumberOfWeeks()
        {
            var weeks = 5;
            var rcos = new List<Rco>
            {
                new Rco {Id = 1},
                new Rco {Id = 2},
                new Rco {Id = 3},
                new Rco {Id = 4},
                new Rco {Id = 5},
                new Rco {Id = 6},
                new Rco {Id = 7},
            };

            var startDate = new DateTime(2016, 1, 20);

            var result = _resolver.GetWeeklyLessons(rcos, weeks, startDate);

            var week = new IsoWeek(3, 2016);
            Assert.IsTrue(week.Equals(result.Keys.ElementAt(0)));
            Assert.AreSame(rcos[0], result.FirstOrDefault(x => week.Equals(x.Key)).Value[0]);
            Assert.AreSame(rcos[1], result.FirstOrDefault(x => week.Equals(x.Key)).Value[1]);

            week = new IsoWeek(4, 2016);
            Assert.IsTrue(week.Equals(result.Keys.ElementAt(1)));
            Assert.AreSame(rcos[2], result.FirstOrDefault(x => week.Equals(x.Key)).Value[0]);
            Assert.AreSame(rcos[3], result.FirstOrDefault(x => week.Equals(x.Key)).Value[1]);

            week = new IsoWeek(5, 2016);
            Assert.IsTrue(week.Equals(result.Keys.ElementAt(2)));
            Assert.AreSame(rcos[4], result.FirstOrDefault(x => week.Equals(x.Key)).Value[0]);

            week = new IsoWeek(6, 2016);
            Assert.IsTrue(week.Equals(result.Keys.ElementAt(3)));
            Assert.AreSame(rcos[5], result.FirstOrDefault(x => week.Equals(x.Key)).Value[0]);

            week = new IsoWeek(7, 2016);
            Assert.IsTrue(week.Equals(result.Keys.ElementAt(4)));
            Assert.AreSame(rcos[6], result.FirstOrDefault(x => week.Equals(x.Key)).Value[0]);
        }

        [Test]
        public void GetElearningDeadline_ParticipationHasDuration_ShouldGetDeadlineFromParticipantCreationDate()
        {
            var participant = CreateParticipant();
            var activity = _context.Object.Activities.First();
            activity.EndDate = null;
            var duration = TimeSpan.FromDays(60);
            activity.CompletionTargetDuration = (long)duration.TotalMilliseconds;
            var customerArticle = new CustomerArticle { ArticleCopy = new ElearningArticle { ArticleTypeId = 1 } };
            _context.Object.CustomerArticles.Add(customerArticle);


            var deadline = participant.Created + duration;

            var result = _resolver.GetElearningDeadline(participant, activity);

            Assert.AreEqual(deadline.ToString("dd.MM.yyyy"), result.Value.ToString("dd.MM.yyyy"));
        }

        [Test]
        public void CreateFields_ParticipationHasEndDate_ShouldGetDeadlineFromEndDate()
        {
            var participant = CreateParticipant();
            var activity = _context.Object.Activities.First();
            activity.CompletionTargetDate = new DateTime(2014, 12, 22, 09, 00, 00);
            activity.Duration = null;
            var customerArticle = new CustomerArticle {ArticleCopy = new ElearningArticle {ArticleTypeId = 1}};
            _context.Object.CustomerArticles.Add(customerArticle);

            var result = _resolver.GetElearningDeadline(participant, activity);

            Assert.AreEqual(activity.CompletionTargetDate.Value, result);
            //Assert.AreEqual("22.12.2014 09:00", result.Value.ToString("dd.MM.yyyy hh:mm"));
        }

        private Participant CreateParticipant()
        {
            const int activityId = 3;

            var activity = new Activity
            {
                Id = activityId,
                Name = "Activity title",
                ActivityPeriods = new List<ActivityPeriod> { new ActivityPeriod { ActivityId = activityId} }
            };

            _context.Object.Activities.Add(activity);

            var participant = new Participant
            {
                ActivityId = activityId,
            };
            _context.Object.Participants.Add(participant);
            return participant;
        }
    }
}