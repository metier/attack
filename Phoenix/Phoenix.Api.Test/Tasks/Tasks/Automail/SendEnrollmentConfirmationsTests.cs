﻿using System;
using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Templates.EnrollmentConfirmation;
using Metier.Phoenix.Tasks.Tasks.Automail;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Tasks.Tasks.Automail
{
    [TestFixture] 
    public class SendEnrollmentConfirmationsTests
    {
        private ContextMock _contextMock;
        private Mock<IEnrollmentConfirmationFactory> _enrollmentConfirmationsMessageFactory;
        private SendEnrollmentConfirmations _sendEnrollmentConfirmations;
        private Random _random;
        private Mock<IAutoMailer> _autoMailer;

        [SetUp]
        public void Setup()
        {
            _random = new Random();

            _contextMock = new ContextMock();
            _enrollmentConfirmationsMessageFactory = new Mock<IEnrollmentConfirmationFactory>();
            _autoMailer = new Mock<IAutoMailer>();

            _enrollmentConfirmationsMessageFactory.Setup(
                e => e.CreateMessage(It.IsAny<Participant>(), false));

            _sendEnrollmentConfirmations = new SendEnrollmentConfirmations(_contextMock.Object, _enrollmentConfirmationsMessageFactory.Object, _autoMailer.Object);
        }

        [Test]
        public void Run_GivenParticipantWhereEnrollmentConfirmationHasNotBeenSentWithPastEnrollmentConfirmationDate_ShouldSendEnrollmentConfirmation()
        {
            CreateParticipant(DateTime.UtcNow - TimeSpan.FromDays(1));
            _sendEnrollmentConfirmations.Run();

            _autoMailer.Verify(a => a.Send(It.IsAny<AutoMailMessage>()), Times.Once);
        }

        [Test]
        public void Run_GivenParticipantWhereEnrollmentConfirmationHasNotBeenSentWithFutureEnrollmentConfirmationDate_ShouldNotSendEnrollmentConfirmation()
        {
            CreateParticipant(DateTime.UtcNow + TimeSpan.FromDays(1));
            _sendEnrollmentConfirmations.Run();

            _autoMailer.Verify(a => a.Send(It.IsAny<AutoMailMessage>()), Times.Never);
        }

        [Test]
        public void Run_GivenParticipantWhereEnrollmentConfirmationHasNotBeenSentWithoutEnrollmentConfirmationDate_ShouldNotSendEnrollmentConfirmation()
        {
            CreateParticipant(null);
            _sendEnrollmentConfirmations.Run();

            _autoMailer.Verify(a => a.Send(It.IsAny<AutoMailMessage>()), Times.Never);
        }

        [Test]
        public void Run_GivenParticipantWhereEnrollmentConfirmationHasBeenSent_ShouldNotSendEnrollmentConfirmation()
        {
            var participant = CreateParticipant(DateTime.UtcNow - TimeSpan.FromDays(1));
            AddEnrollmentConfirmationMail(participant);

            _sendEnrollmentConfirmations.Run();
            _autoMailer.Verify(a => a.Send(It.IsAny<AutoMailMessage>()), Times.Never);
        }

        [Test]
        public void Run_GivenParticipantForUserWithAutomailsDisabled_ShouldNotSendEnrollmentConfirmation()
        {
            var user = new User
            {
                Id = _random.Next(1, 100000),
                UserNotificationType = UserNotificationTypes.No
            };
            _contextMock.Object.Users.Add(user);
            var participant = CreateParticipant(DateTime.UtcNow - TimeSpan.FromDays(1));
            participant.UserId = user.Id;

            _sendEnrollmentConfirmations.Run();
            _autoMailer.Verify(a => a.Send(It.IsAny<AutoMailMessage>()), Times.Never);
        }

        [Test]
        public void Run_GivenParticipantForUserWithAutomailsEnabled_ShouldSendEnrollmentConfirmation()
        {
            var user = new User
            {
                Id = _random.Next(1, 100000),
                UserNotificationType = UserNotificationTypes.Yes
            };
            _contextMock.Object.Users.Add(user);
            var participant = CreateParticipant(DateTime.UtcNow - TimeSpan.FromDays(1));
            participant.UserId = user.Id;

            _sendEnrollmentConfirmations.Run();
            _autoMailer.Verify(a => a.Send(It.IsAny<AutoMailMessage>()), Times.Once);
        }

        private Participant CreateParticipant(DateTime? enrollmentConfirmationDate)
        {
            var participantId = _random.Next(1, 100000);
            var activityId = _random.Next(1, 100000);
            var userId = _random.Next(1, 100000);
            var articleId = _random.Next(1, 10000);
            var customerArticleId = _random.Next(1, 10000);
            var customerId = _random.Next(1, 10000);
            var automailDefinitionId = _random.Next(1, 10000);

            var automailDefinition = new AutomailDefinition
            {
                Id = automailDefinitionId,
                IsEnrollmentNotification = true
            };

            var customer = new Customer
            {
                Id = customerId,
                AutomailDefinitionId = automailDefinition.Id,
                AutomailDefinition = automailDefinition
            };
            _contextMock.Object.Customers.Add(customer);

            var article = new ElearningArticle
            {
                Id = articleId,
                ArticleTypeId = (int)ArticleTypes.ELearning,
                IsDeleted = false
            };
            _contextMock.Object.Articles.Add(article);

            var customerArticle = new CustomerArticle
            {
                Id = customerArticleId,
                ArticleCopyId = article.Id,
                CustomerId = customerId,
                IsDeleted = false
            };
            _contextMock.Object.CustomerArticles.Add(customerArticle);

            var activity = new Activity
            {
                Id = activityId,
                EnrollmentConfirmationDate = enrollmentConfirmationDate,
                SendEnrollmentConfirmationAutomail = true,
                CustomerArticleId = customerArticleId,
                CustomerArticle = customerArticle
            };
            _contextMock.Object.Activities.Add(activity);

            var user = new User
            {
                Id = userId,
                UserNotificationType = UserNotificationTypes.Yes,
                CustomerId = customerId,
                Customer = customer
            };
            _contextMock.Object.Users.Add(user);

            var participant = new Participant
            {
                Id = participantId,
                ActivityId = activityId,
                UserId = userId,
                CustomerId = customerId,
                StatusCodeValue = ParticipantStatusCodes.Enrolled
            };
            _contextMock.Object.Participants.Add(participant);
            return participant;
        }

        private void AddEnrollmentConfirmationMail(Participant participant)
        {
            var mailId = _random.Next(1, 100000);
            var created = DateTime.UtcNow - TimeSpan.FromHours(1);
            _contextMock.Object.Mails.Add(new Phoenix.Core.Data.Entities.Mail
            {
                Created = created,
                Id = mailId,
                ParticipantId = participant.Id,
                MessageType = MessageType.EnrollmentConfirmation,
                MailStatuses = new List<MailStatus>
                {
                    new MailStatus
                    {
                        Status = MailStatusType.Created,
                        MailId = mailId,
                        Id = _random.Next(1, 100000),
                        StatusTime = created
                    },
                    new MailStatus
                    {
                        Status = MailStatusType.Sent,
                        MailId = mailId,
                        Id = _random.Next(1, 100000),
                        StatusTime = DateTime.UtcNow - TimeSpan.FromMinutes(45)
                    }
                }
            });
        }
    }
}