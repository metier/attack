﻿using System;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Security;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Templates.PasswordResetRequest;
using Metier.Phoenix.Mail.Templates.UserRegistrationNotification;
using Metier.Phoenix.Tasks.Configuration;
using Metier.Phoenix.Tasks.Tasks.Automail;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Tasks.Tasks.Automail
{
    [TestFixture] 
    public class SendUserRegistrationNotificationsTests
    {
        private Mock<IUserRegistrationNotificationFactory> _userRegistrationNotificationFactoryMock;
        private Mock<IAutoMailer> _autoMailerMock;
        private Mock<IPhoenixTasksConfiguration> _configuration;
        private ContextMock _contextMock;
        private SendUserRegistrationNotifications _sendUserRegistrationNotification;

        [SetUp]
        public void Setup()
        {
            _userRegistrationNotificationFactoryMock = new Mock<IUserRegistrationNotificationFactory>();
            _autoMailerMock = new Mock<IAutoMailer>();
            _contextMock = new ContextMock();
            _configuration = new Mock<IPhoenixTasksConfiguration>();
            var passwordResetRequestMessageFactory = new Mock<IPasswordResetRequestMessageFactory>();
            var passwordResetRequestFactory = new Mock<IPasswordResetRequestFactory>();

            _sendUserRegistrationNotification = new SendUserRegistrationNotifications(_contextMock.Object, 
                _userRegistrationNotificationFactoryMock.Object, 
                _autoMailerMock.Object, 
                _configuration.Object,
                passwordResetRequestMessageFactory.Object,
                passwordResetRequestFactory.Object);
        }

        [Test]
        public void Run_GivenUserWithNotificationDateInPastNotSent_ShouldSendAutoMailForNotification()
        {
            var user = GetUserWithPastNotificationDate(12345);
            _contextMock.Object.Users.Add(user);
            _contextMock.Object.Mails.Add(CreateMailForUser(user));
            
            var autoMailMessage = CreateAutoMailMessage();
            _userRegistrationNotificationFactoryMock.Setup(
                uf => uf.CreateUserRegistrationNotificationMessage(It.Is<User>(u => u.Id == user.Id), null, null)).Returns(autoMailMessage);

            _sendUserRegistrationNotification.Run();

            _autoMailerMock.Verify(am => am.Send(It.Is<AutoMailMessage>(m => m == autoMailMessage)), Times.Never);
        }

        [Test]
        public void Run_GivenRegisteredUsers_ShouldSendAutoMailOnlyForUsersNotNotifiedWithPastNotificationDate()
        {
            var user = GetUserWithPastNotificationDate(1234);
            var user2 = GetUserWithPastNotificationDate(2345);
            var user3 = GetUserWithFutureNotificationDate(3456);

            var customer = CreateCustomer();
            _contextMock.Object.Customers.Add(customer);

            user.CustomerId = customer.Id;
            user2.CustomerId = customer.Id;
            user3.CustomerId = customer.Id;

            _contextMock.Object.Users.Add(user);
            _contextMock.Object.Users.Add(user2);
            _contextMock.Object.Users.Add(user3);
            _contextMock.Object.Mails.Add(CreateMailForUser(user));

            var autoMailMessage = CreateAutoMailMessage();
            _userRegistrationNotificationFactoryMock.Setup(
                uf => uf.CreateUserRegistrationNotificationMessage(It.IsAny<User>(), null, null)).Returns(autoMailMessage);

            _sendUserRegistrationNotification.Run();

            _autoMailerMock.Verify(am => am.Send(It.Is<AutoMailMessage>(m => m == autoMailMessage)), Times.Once);
        }

        [Test]
        public void Run_GivenExpiredUserRegistrationNotificationDate_ShouldNotSendAutoMailForUser()
        {
            var user = GetUserWithExpiredNotificationDate(1234);

            var customer = CreateCustomer();
            _contextMock.Object.Customers.Add(customer);

            user.CustomerId = customer.Id;

            _contextMock.Object.Users.Add(user);

            var autoMailMessage = CreateAutoMailMessage();
            _userRegistrationNotificationFactoryMock.Setup(
                uf => uf.CreateUserRegistrationNotificationMessage(It.IsAny<User>(), null, null)).Returns(autoMailMessage);

            _sendUserRegistrationNotification.Run();

            _autoMailerMock.Verify(am => am.Send(It.Is<AutoMailMessage>(m => m == autoMailMessage)), Times.Never);
        }

        [Test]
        public void Run_GivenExpiredUser_ShouldNotSendAutoMailForUser()
        {
            var user = GetUserWithPastNotificationDate(1234);
            var customer = CreateCustomer();

            user.ExpiryDate = DateTime.Now.AddDays(-4);
            user.CustomerId = customer.Id;

            _contextMock.Object.Customers.Add(customer);
            _contextMock.Object.Users.Add(user);

            var autoMailMessage = CreateAutoMailMessage();
            _userRegistrationNotificationFactoryMock.Setup(
                uf => uf.CreateUserRegistrationNotificationMessage(It.IsAny<User>(), null, null)).Returns(autoMailMessage);

            _sendUserRegistrationNotification.Run();

            _autoMailerMock.Verify(am => am.Send(It.Is<AutoMailMessage>(m => m == autoMailMessage)), Times.Never);
        }

        [Test]
        public void Run_GivenUserSetToExpireInTheFuture_ShouldSendAutoMailForUser()
        {
            var user = GetUserWithPastNotificationDate(1234);
            var customer = CreateCustomer();

            user.ExpiryDate = DateTime.Now.AddDays(4);
            user.CustomerId = customer.Id;

            _contextMock.Object.Customers.Add(customer);
            _contextMock.Object.Users.Add(user);

            var autoMailMessage = CreateAutoMailMessage();
            _userRegistrationNotificationFactoryMock.Setup(
                uf => uf.CreateUserRegistrationNotificationMessage(It.IsAny<User>(), null, null)).Returns(autoMailMessage);

            _sendUserRegistrationNotification.Run();

            _autoMailerMock.Verify(am => am.Send(It.Is<AutoMailMessage>(m => m == autoMailMessage)), Times.Once);
        }

        [Test]
        public void Run_GivenUserWithNotificationDateInPastSent_ShouldNotSendAutoMailForNotification()
        {
            var user = GetUserWithPastNotificationDate(1234);
            var customer = CreateCustomer();

            user.CustomerId = customer.Id;

            _contextMock.Object.Users.Add(user);
            _contextMock.Object.Customers.Add(customer);
            
            var autoMailMessage = CreateAutoMailMessage();
            _userRegistrationNotificationFactoryMock.Setup(
                uf => uf.CreateUserRegistrationNotificationMessage(It.Is<User>(u => u.Id == user.Id), null, null)).Returns(autoMailMessage);

            _sendUserRegistrationNotification.Run();

            _autoMailerMock.Verify(am => am.Send(It.Is<AutoMailMessage>(m => m == autoMailMessage)), Times.Once);
        }

        private Phoenix.Core.Data.Entities.Mail CreateMailForUser(User user)
        {
            return new Phoenix.Core.Data.Entities.Mail
            {
                Created = DateTime.UtcNow - TimeSpan.FromMinutes(60),
                Id = 1,
                MessageType = MessageType.UserRegistrationNotification,
                UserId = user.Id
            };
        }

        private Customer CreateCustomer()
        {
            var automailDefinition = new AutomailDefinition {Id = 1, IsWelcomeNotification = true};
            var customer = new Customer { Id = 1, Name = "Testcustomer", AutomailDefinition = automailDefinition};

            return customer;
        }


        private User GetUserWithFutureNotificationDate(int id)
        {
            return new User
            {
                Id = id,
                UserRegistrationNotificationDate = DateTime.UtcNow + TimeSpan.FromDays(1)
            };
        }

        private User GetUserWithPastNotificationDate(int id)
        {
            return new User
            {
                Id = id,
                UserRegistrationNotificationDate = DateTime.UtcNow - TimeSpan.FromDays(1)
            };
        }
        private User GetUserWithExpiredNotificationDate(int id)
        {
            return new User
            {
                Id = id,
                UserRegistrationNotificationDate = DateTime.UtcNow - TimeSpan.FromDays(5)
            };
        }

        private static AutoMailMessage CreateAutoMailMessage()
        {
            return new AutoMailMessage
            {
                MessageType = MessageType.UserRegistrationNotification,
                Subject = "Automail subject",
                Body = "Autmoal body",
                RecipientEmail = "recipient@test.com"
            };
        }
    }
}