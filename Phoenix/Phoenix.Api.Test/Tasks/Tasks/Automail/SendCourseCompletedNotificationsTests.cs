﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls.WebParts;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Templates.EnrollmentConfirmation;
using Metier.Phoenix.Mail.Templates.ReminderElearning;
using Metier.Phoenix.Tasks.Tasks.Automail;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Tasks.Tasks.Automail
{
    [TestFixture] 
    public class SendCourseCompletedNotificationsTests
    {
        private ContextMock _contextMock;
        private Mock<IReminderElearningFactory> _reminderElearningFactory;
        private SendCourseCompletedNotification _sendCourseCompletedNotification;
        private Random _random;
        private Mock<IAutoMailer> _autoMailer;

        [SetUp]
        public void Setup()
        {
            _random = new Random();

            _contextMock = new ContextMock();
            _reminderElearningFactory = new Mock<IReminderElearningFactory>();
            _autoMailer = new Mock<IAutoMailer>();

            _reminderElearningFactory.Setup(
                e => e.CreateCompletedMessage(It.IsAny<Participant>()));

            _sendCourseCompletedNotification = new SendCourseCompletedNotification(_reminderElearningFactory.Object, _contextMock.Object, _autoMailer.Object);
        }

        [Test]
        public void Run_GivenParticipantWhereCompletionNotificationHasNotBeenSentAndWithinCutOffDate_ShouldSendCourseCompletedNotification()
        {
            CreateParticipant(DateTime.UtcNow - TimeSpan.FromDays(1));
            _sendCourseCompletedNotification.Run();

            _autoMailer.Verify(a => a.Send(It.IsAny<AutoMailMessage>()), Times.Once);
        }

        [Test]
        public void Run_GivenParticipantWhereComepletionNotificationHasNotBeenSentAndOutsideCutOffDate_ShouldNotSendCourseCompletedNotification()
        {
            CreateParticipant(DateTime.UtcNow - TimeSpan.FromDays(4));
            _sendCourseCompletedNotification.Run();

            _autoMailer.Verify(a => a.Send(It.IsAny<AutoMailMessage>()), Times.Never);
        }

        [Test]
        public void Run_GivenParticipantNotComplete_ShouldNotSendCourseCompletedNotification()
        {
            var participant = CreateParticipant(DateTime.UtcNow - TimeSpan.FromDays(1));
            participant.StatusCodeValue = ParticipantStatusCodes.InProgress;

            _sendCourseCompletedNotification.Run();

            _autoMailer.Verify(a => a.Send(It.IsAny<AutoMailMessage>()), Times.Never);
        }

        [Test]
        public void Run_GivenCompletionNoticeAlreadySent_ShouldNotSendCourseCompletedNotification()
        {
            var participant = CreateParticipant(DateTime.UtcNow - TimeSpan.FromDays(1));
            CreateCourseCompletionMailForParticipant(participant);

            _sendCourseCompletedNotification.Run();
            _autoMailer.Verify(a => a.Send(It.IsAny<AutoMailMessage>()), Times.Never);
        }

        private void CreateCourseCompletionMailForParticipant(Participant participant)
        {
            var mailStatusId = _random.Next(1, 10000);
            var mailId = _random.Next(1, 10000);

            var mailStatus = new MailStatus
            {
                Id = mailStatusId,
                Status = MailStatusType.Sent,
                MailId = mailId
            };

            var mail = new Phoenix.Core.Data.Entities.Mail
            {
                Id = mailId,
                ActivityId = participant.ActivityId,
                ParticipantId = participant.Id,
                Created = DateTime.Now.AddDays(-1),
                MessageType = MessageType.ElearningCompleted,
                MailStatuses = new List<MailStatus> {mailStatus}
            };

            _contextMock.Object.Mails.Add(mail);
        }

        private Participant CreateParticipant(DateTime? courseCompletedDate)
        {
            var participantId = _random.Next(1, 100000);
            var activityId = _random.Next(1, 100000);
            var userId = _random.Next(1, 100000);
            var articleId = _random.Next(1, 10000);
            var customerArticleId = _random.Next(1, 10000);
            var customerId = _random.Next(1, 10000);
            var automailDefinitionId = _random.Next(1, 10000);

            var automailDefinition = new AutomailDefinition
            {
                Id = automailDefinitionId,
                IsEnrollmentNotification = true,
                IsProgressNotificationElearning = true
            };

            var customer = new Customer
            {
                Id = customerId,
                AutomailDefinitionId = automailDefinition.Id,
                AutomailDefinition = automailDefinition
            };
            _contextMock.Object.Customers.Add(customer);

            var article = new ElearningArticle
            {
                Id = articleId,
                ArticleTypeId = (int)ArticleTypes.ELearning,
                IsDeleted = false
            };
            _contextMock.Object.Articles.Add(article);

            var customerArticle = new CustomerArticle
            {
                Id = customerArticleId,
                ArticleCopyId = article.Id,
                CustomerId = customerId,
                IsDeleted = false
            };
            _contextMock.Object.CustomerArticles.Add(customerArticle);

            var activity = new Activity
            {
                Id = activityId,
                EnrollmentConfirmationDate = DateTime.Now.AddDays(-10),
                SendEnrollmentConfirmationAutomail = true,
                CustomerArticleId = customerArticleId,
                CustomerArticle = customerArticle
            };
            _contextMock.Object.Activities.Add(activity);

            var user = new User
            {
                Id = userId,
                UserNotificationType = UserNotificationTypes.Yes,
                CustomerId = customerId,
                Customer = customer
            };
            _contextMock.Object.Users.Add(user);

            var participant = new Participant
            {
                Id = participantId,
                ActivityId = activityId,
                UserId = userId,
                CustomerId = customerId,
                CompletedDate = courseCompletedDate,
                StatusCodeValue = ParticipantStatusCodes.Completed
            };
            _contextMock.Object.Participants.Add(participant);
            return participant;
        }

        private void AddEnrollmentConfirmationMail(Participant participant)
        {
            var mailId = _random.Next(1, 100000);
            var created = DateTime.UtcNow - TimeSpan.FromHours(1);
            _contextMock.Object.Mails.Add(new Phoenix.Core.Data.Entities.Mail
            {
                Created = created,
                Id = mailId,
                ParticipantId = participant.Id,
                MessageType = MessageType.EnrollmentConfirmation,
                MailStatuses = new List<MailStatus>
                {
                    new MailStatus
                    {
                        Status = MailStatusType.Created,
                        MailId = mailId,
                        Id = _random.Next(1, 100000),
                        StatusTime = created
                    },
                    new MailStatus
                    {
                        Status = MailStatusType.Sent,
                        MailId = mailId,
                        Id = _random.Next(1, 100000),
                        StatusTime = DateTime.UtcNow - TimeSpan.FromMinutes(45)
                    }
                }
            });
        }
    }
}