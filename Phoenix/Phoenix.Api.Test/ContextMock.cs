﻿using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Data.ViewEntities.Reports;
using Moq;

namespace Metier.Phoenix.UnitTests
{
    public class ContextMock : Mock<MetierLmsContext>
    {
        public ContextMock()
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor

            // Tables
            Setup(c => c.Customers).Returns(Object.Customers ?? (Object.Customers = new InMemoryDbSet<Customer>()));
            Setup(c => c.ParticipantInfoDefinitions).Returns(Object.ParticipantInfoDefinitions ?? (Object.ParticipantInfoDefinitions = new InMemoryDbSet<ParticipantInfoDefinition>()));
            Setup(c => c.Languages).Returns(Object.Languages ?? (Object.Languages = new InMemoryDbSet<Language>()));
            Setup(c => c.LeadingTextes).Returns(Object.LeadingTextes ?? (Object.LeadingTextes = new InMemoryDbSet<LeadingText>()));
            Setup(c => c.CustomLeadingTexts).Returns(Object.CustomLeadingTexts ?? (Object.CustomLeadingTexts = new InMemoryDbSet<CustomLeadingText>()));
            Setup(c => c.ProductDescriptions).Returns(Object.ProductDescriptions ?? (Object.ProductDescriptions = new InMemoryDbSet<ProductDescription>()));
            Setup(c => c.Products).Returns(Object.Products ?? (Object.Products = new InMemoryDbSet<Product>()));
            Setup(c => c.ProductCategories).Returns(Object.ProductCategories ?? (Object.ProductCategories = new InMemoryDbSet<ProductCategory>()));
            Setup(c => c.ProductTypes).Returns(Object.ProductTypes ?? (Object.ProductTypes = new InMemoryDbSet<ProductType>()));
            Setup(c => c.CodeTypes).Returns(Object.CodeTypes ?? (Object.CodeTypes = new InMemoryDbSet<CodeType>()));
            Setup(c => c.Codes).Returns(Object.Codes ?? (Object.Codes = new InMemoryDbSet<Code>()));
            Setup(c => c.Articles).Returns(Object.Articles ?? (Object.Articles = new InMemoryDbSet<Article>()));
            Setup(c => c.ElearningArticles).Returns(Object.ElearningArticles ?? (Object.ElearningArticles = new InMemoryDbSet<ElearningArticle>()));
            Setup(c => c.Attachments).Returns(Object.Attachments ?? (Object.Attachments = new InMemoryDbSet<Attachment>()));
            Setup(c => c.Users).Returns(Object.Users ?? (Object.Users = new InMemoryDbSet<User>()));
            Setup(c => c.HelpTexts).Returns(Object.HelpTexts ?? (Object.HelpTexts = new InMemoryDbSet<HelpText>()));
            Setup(c => c.CoursePrograms).Returns(Object.CoursePrograms ?? (Object.CoursePrograms = new InMemoryDbSet<CourseProgram>()));
            Setup(c => c.CustomerArticles).Returns(Object.CustomerArticles ?? (Object.CustomerArticles = new InMemoryDbSet<CustomerArticle>()));
            Setup(c => c.Distributors).Returns(Object.Distributors ?? (Object.Distributors = new InMemoryDbSet<Distributor>()));
            Setup(c => c.Resources).Returns(Object.Resources ?? (Object.Resources = new InMemoryDbSet<Resource>()));
            Setup(c => c.Comments).Returns(Object.Comments ?? (Object.Comments = new InMemoryDbSet<Comment>()));
            Setup(c => c.Activities).Returns(Object.Activities ?? (Object.Activities = new InMemoryDbSet<Activity>()));
            Setup(c => c.ActivityPeriods).Returns(Object.ActivityPeriods ?? (Object.ActivityPeriods = new InMemoryDbSet<ActivityPeriod>()));
            Setup(c => c.ActivitySets).Returns(Object.ActivitySets ?? (Object.ActivitySets = new InMemoryDbSet<ActivitySet>()));
            Setup(c => c.ActivitySetShares).Returns(Object.ActivitySetShares ?? (Object.ActivitySetShares = new InMemoryDbSet<ActivitySetShare>()));
            Setup(c => c.Enrollments).Returns(Object.Enrollments ?? (Object.Enrollments = new InMemoryDbSet<Enrollment>()));
            Setup(c => c.Participants).Returns(Object.Participants ?? (Object.Participants = new InMemoryDbSet<Participant>()));
            Setup(c => c.Orders).Returns(Object.Orders ?? (Object.Orders = new InMemoryDbSet<Order>()));
            Setup(c => c.AuthenticationTokens).Returns(Object.AuthenticationTokens ?? (Object.AuthenticationTokens = new InMemoryDbSet<AuthenticationToken>()));
            Setup(c => c.ScormAttempts).Returns(Object.ScormAttempts ?? (Object.ScormAttempts = new InMemoryDbSet<ScormAttempt>()));
            Setup(c => c.ScormPerformances).Returns(Object.ScormPerformances ?? (Object.ScormPerformances = new InMemoryDbSet<ScormPerformance>()));
            Setup(c => c.PasswordResetRequests).Returns(Object.PasswordResetRequests ?? (Object.PasswordResetRequests = new InMemoryDbSet<PasswordResetRequest>()));
            Setup(c => c.Mails).Returns(Object.Mails ?? (Object.Mails = new InMemoryDbSet<Phoenix.Core.Data.Entities.Mail>()));
            Setup(c => c.Exams).Returns(Object.Exams ?? (Object.Exams = new InMemoryDbSet<Exam>()));
            Setup(c => c.GradeScales).Returns(Object.GradeScales ?? (Object.GradeScales = new InMemoryDbSet<GradeScale>()));
            Setup(c => c.Questions).Returns(Object.Questions ?? (Object.Questions = new InMemoryDbSet<Question>()));
            Setup(c => c.EnrollmentConditions).Returns(Object.EnrollmentConditions ?? (Object.EnrollmentConditions = new InMemoryDbSet<EnrollmentCondition>()));
            Setup(c => c.MetierContactPersons).Returns(Object.MetierContactPersons ?? (Object.MetierContactPersons = new InMemoryDbSet<MetierContactPerson>()));
            Setup(c => c.ChangeLog).Returns(Object.ChangeLog ?? (Object.ChangeLog = new InMemoryDbSet<ChangeLogEntry>()));
            Setup(c => c.UserCompetences).Returns(Object.UserCompetences ?? (Object.UserCompetences = new InMemoryDbSet<UserCompetence>()));
            Setup(c => c.ParticipantOrderLines).Returns(Object.ParticipantOrderLines ?? (Object.ParticipantOrderLines = new InMemoryDbSet<ParticipantOrderLine>()));

            // Views
            Setup(c => c.ImportedUsers).Returns(Object.ImportedUsers ?? (Object.ImportedUsers = new InMemoryDbSet<ImportedMembershipUser>()));
            Setup(c => c.SearchableArticles).Returns(Object.SearchableArticles ?? (Object.SearchableArticles = new InMemoryDbSet<SearchableArticle>()));
            Setup(c => c.SearchableCustomers).Returns(Object.SearchableCustomers ?? (Object.SearchableCustomers = new InMemoryDbSet<SearchableCustomer>()));
            Setup(c => c.SearchableActivities).Returns(Object.SearchableActivities ?? (Object.SearchableActivities = new InMemoryDbSet<SearchableActivity>()));
            Setup(c => c.SearchableActivitiesWithActivitySets).Returns(Object.SearchableActivitiesWithActivitySets ?? (Object.SearchableActivitiesWithActivitySets = new InMemoryDbSet<SearchableActivityWithActivitySet>()));
            Setup(c => c.SearchableUsers).Returns(Object.SearchableUsers ?? (Object.SearchableUsers = new InMemoryDbSet<SearchableUser>()));
            Setup(c => c.SearchableOrders).Returns(Object.SearchableOrders ?? (Object.SearchableOrders = new InMemoryDbSet<SearchableOrder>()));
            Setup(c => c.SearchableParticipants).Returns(Object.SearchableParticipants ?? (Object.SearchableParticipants = new InMemoryDbSet<SearchableParticipant>()));
            Setup(c => c.SearchableCustomerArticles).Returns(Object.SearchableCustomerArticles ?? (Object.SearchableCustomerArticles = new InMemoryDbSet<SearchableCustomerArticle>()));
            Setup(c => c.SearchableMails).Returns(Object.SearchableMails ?? (Object.SearchableMails = new InMemoryDbSet<SearchableMail>()));
            Setup(c => c.ProposedOrderLines).Returns(Object.ProposedOrderLines ?? (Object.ProposedOrderLines = new InMemoryDbSet<ProposedOrderLine>()));
            Setup(c => c.Rcos).Returns(Object.Rcos ?? (Object.Rcos = new InMemoryDbSet<Rco>()));
            Setup(c => c.SearchableCourseRcos).Returns(Object.SearchableCourseRcos ?? (Object.SearchableCourseRcos = new InMemoryDbSet<SearchableRco>()));
            Setup(c => c.ProgressStatuses).Returns(Object.ProgressStatuses ?? (Object.ProgressStatuses = new InMemoryDbSet<ProgressStatus>()));

            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }
    }
}