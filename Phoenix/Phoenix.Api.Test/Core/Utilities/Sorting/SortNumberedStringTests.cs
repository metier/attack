﻿using Metier.Phoenix.Core.Utilities.Sorting;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Core.Utilities.Sorting
{
    [TestFixture]
    public class SortNumberedStringTests
    {
        private SortNumberedStrings _sortNumberedStrings;

        [SetUp]
        public void Setup()
        {
            _sortNumberedStrings = new SortNumberedStrings();
        }

        [Test]
        [TestCase("1. Lesson title", false, Result = "0001. Lesson title")]
        [TestCase("01. Lesson title", false, Result = "0001. Lesson title")]
        [TestCase("10. Lesson title", false, Result = "0010. Lesson title")]
        [TestCase("100. Lesson title", false, Result = "0100. Lesson title")]
        [TestCase("1000. Lesson title", false, Result = "1000. Lesson title")]
        [TestCase("10000. Lesson title", false, Result = "10000. Lesson title")]
        [TestCase("No digits", false, Result = "No digits")]
        [TestCase("-1No digits", false, Result = "-1No digits")]
        [TestCase("No 1 digits", false, Result = "No 1 digits")]
        [TestCase("Anything", true, Result = "9999Anything")]
        [TestCase("", false, Result = "")]
        [TestCase(null, false, Result = null)]
        [TestCase("", true, Result = "9999")]
        [TestCase(null, true, Result = "9999")]
        public string PadInitialNumbersForSorting_GivenInput_ReturnsExpectedString(string stringForSorting, bool isSortedAtTop)
        {
            return _sortNumberedStrings.PadInitialNumbersForSorting(stringForSorting, isSortedAtTop);
        }
    }
}