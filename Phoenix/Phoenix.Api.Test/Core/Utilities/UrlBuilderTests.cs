﻿using Metier.Phoenix.Core.Utilities;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Core.Utilities
{
    [TestFixture]
    public class UrlBuilderTests
    {
        [Test]
        [TestCase("http://something", "http://something?param=myParam")]
        [TestCase("http://something/", "http://something/?param=myParam")]
        [TestCase("http://something/?a=1", "http://something/?a=1&param=myParam")]
        [TestCase("http://something?a=1", "http://something?a=1&param=myParam")]
        [TestCase("http://something/#/route", "http://something/#/route?param=myParam")]
        [TestCase("http://something/#/route?a=1", "http://something/#/route?a=1&param=myParam")]
        public void GetUrl_MustAppendParamToBase(string urlBase, string expected)
        {
            const string param = "myParam";
            var result = new UrlBuilder(urlBase).AddParameter("param", param).ToString();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void GetUrl_MustAppendTwoParams()
        {
            const string param1 = "myParam1";
            const string param2 = "myParam2";
            var result = new UrlBuilder("http://something").AddParameter("param1", param1).AddParameter("param2", param2).ToString();
            Assert.AreEqual("http://something?param1=myParam1&param2=myParam2", result);
        }
    }
}