﻿using System;
using System.Collections.Generic;
using Metier.Phoenix.Core.Business;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Utilities.Extensions;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Core.Business
{
    [TestFixture]
    public class ParticipantBusinessTests
    {
        private ContextMock _contextMock;
        private ParticipantBusiness _participantBusiness;

        [SetUp]
        public void Setup()
        {
            _contextMock = new ContextMock();
            _participantBusiness = new ParticipantBusiness(_contextMock.Object);
        }

        private void ConstructMockObjects(Participant participant, Activity activity, Article article, CustomerArticle customerArticle, Exam exam)
        {
            _contextMock.Object.Exams.Add(exam);
            ConstructMockObjects(participant, activity, article, customerArticle);
        }
        private void ConstructMockObjects(Participant participant, Activity activity, Article article, CustomerArticle customerArticle)
        {
            _contextMock.Object.Participants.Add(participant);
            _contextMock.Object.Articles.Add(article);
            _contextMock.Object.Activities.Add(activity);
            _contextMock.Object.CustomerArticles.Add(customerArticle);
        }

        [Test]
        public void GetDueDate_GivenActivityWithEndDate_ShouldReturnEndDate()
        {
            var particiant = new Participant();
            var activity = new Activity { EndDate = DateTime.Parse("2014-04-30 06:00:00") };
            var article = GetArticle(ArticleTypes.CaseExam);
            var activityPeriods = new List<ActivityPeriod> { new ActivityPeriod { ActivityId = activity.Id, Start = DateTimeOffset.Now, End = DateTimeOffset.Now.AddDays(1) } };
            activity.ActivityPeriods = activityPeriods;

            var dueDate = _participantBusiness.GetDueDate(particiant, activity, article);

            Assert.AreEqual(activity.EndDate.AsUtcDateTime(), dueDate);
        }

        [Test]
        public void GetDueDate_GivenElearningActivityWithDurationAndParticipantCreatedAfterActivityStart_ShouldReturnParticipantCreatedWithAddedDuration()
        {
            var particiant = new Participant { Created = DateTime.Parse("2014-04-30 06:00:00") };
            var activity = new Activity { CompletionTargetDuration = 60000 };
            var article = GetArticle(ArticleTypes.ELearning);
            var activityPeriods = new List<ActivityPeriod> {new ActivityPeriod {ActivityId = activity.Id, Start = particiant.Created.AddDays(-1), End = particiant.Created.AddDays(14)}};
            activity.ActivityPeriods = activityPeriods;

            var dueDate = _participantBusiness.GetDueDate(particiant, activity, article);

            Assert.AreEqual(DateTime.Parse("2014-04-30 06:01:00"), dueDate);
        }

        [Test]
        [TestCase(ArticleTypes.CaseExam)]
        [TestCase(ArticleTypes.ExternalCertification)]
        [TestCase(ArticleTypes.ProjectAssignment)]
        public void GetDueDate_GivenActivityWithArticleTypesWithDuration_ShouldReturnActivityPeriodStartWithAddedDuration(ArticleTypes articleType)
        {
            var particiant = new Participant();
            var activity = new Activity
            {
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {Start = DateTimeOffset.Parse("2014-04-30 04:00:00.0000000 +05:00")} // 2014-04-29 23:00:00 UTC
                },
                Duration = 60000
            };
            var article = GetArticle(articleType);

            var dueDate = _participantBusiness.GetDueDate(particiant, activity, article);

            Assert.AreEqual(DateTime.Parse("2014-04-29 23:01:00"), dueDate);
        }



        [Test]
        [TestCase(ArticleTypes.MultipleChoice)]
        [TestCase(ArticleTypes.InternalCertification)]
        public void GetDueDate_GivenActivityWithExamArticleTypes_ShouldReturnLatestActivityPeriodsEndWithAddedDuration_WhenExamIsNull(ArticleTypes articleType)
        {
            var particiant = new Participant { AdditionalTime = 60000 }; // 1 minute
            var activity = new Activity
            {
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod 
                    { 
                        Start = DateTimeOffset.Parse("2014-04-30 01:00:00.0000000 +05:00"), //  2014-04-29 20:00:00 UTC
                        End = DateTimeOffset.Parse("2014-04-30 04:00:00.0000000 +05:00")    // 2014-04-29 23:00:00 UTC
                    } 
                },
                Duration = 60000 // 1 minute
            };
            var article = GetArticle(articleType);

            var dueDate = _participantBusiness.GetDueDate(particiant, activity, article, null);

            Assert.AreEqual(DateTime.Parse("2014-04-29 23:02:00"), dueDate);
        }


        [Test]
        [TestCase(ArticleTypes.MultipleChoice)]
        [TestCase(ArticleTypes.InternalCertification)]
        public void GetDueDate_GivenActivityWithExamArticleTypes_ShouldReturnNull_WhenNoEndPeriod_WhenExamIsNull(ArticleTypes articleType)
        {
            var particiant = new Participant { AdditionalTime = 60000 }; // 1 minute
            var activity = new Activity
            {
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod 
                    { 
                        Start = DateTimeOffset.Parse("2014-04-30 01:00:00.0000000 +05:00"), //  2014-04-29 20:00:00 UTC
                    } 
                },
                Duration = 60000 // 1 minute
            };
            var article = GetArticle(articleType);

            var dueDate = _participantBusiness.GetDueDate(particiant, activity, article, null);

            Assert.IsNull(dueDate);
        }

        [Test]
        [TestCase(ArticleTypes.MultipleChoice)]
        [TestCase(ArticleTypes.InternalCertification)]
        public void GetDueDate_GivenActivityWithExamArticleTypes_AndExam_ShouldReturnExamCreateDateWithAddedDuration(ArticleTypes articleType)
        {
            var particiant = new Participant { AdditionalTime = 60000 }; // 1 minute
            var activity = new Activity
            {
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod 
                    { 
                        Start = DateTimeOffset.Parse("2014-04-30 01:00:00.0000000 +05:00"), //  2014-04-29 20:00:00 UTC
                    } 
                },
                Duration = 60000 // 1 minute
            };
            var article = GetArticle(articleType);
            var exam = new Exam
            {
                Created = DateTime.Parse("2014-04-30 08:00:00")
            };
            var dueDate = _participantBusiness.GetDueDate(particiant, activity, article, exam);

            Assert.AreEqual(DateTime.Parse("2014-04-30 08:02:00"), dueDate);
        }

        [Test]
        public void GetDueDate_GivenActivityWithoutEndDateAndDuration_ShouldReturnLatestActivityPeriodsEnd()
        {
            var particiant = new Participant();
            var activity = new Activity
            {
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {End = DateTimeOffset.Parse("2014-04-29 06:00:00.0000000 +05:00")}, // 2014-04-29 01:00:00 UTC
                    new ActivityPeriod {End = DateTimeOffset.Parse("2014-04-30 04:00:00.0000000 +05:00")} // 2014-04-29 23:00:00 UTC
                }
            };
            var article = GetArticle(ArticleTypes.Classroom);

            var dueDate = _participantBusiness.GetDueDate(particiant, activity, article);

            Assert.AreEqual(DateTime.Parse("2014-04-29 23:00:00"), dueDate);
        }

        [Test]
        [TestCase(ArticleTypes.ELearning)]
        [TestCase(ArticleTypes.Classroom)]
        [TestCase(ArticleTypes.CaseExam)]
        [TestCase(ArticleTypes.ProjectAssignment)]
        [TestCase(ArticleTypes.Other)]
        [TestCase(ArticleTypes.Mockexam)]
        [TestCase(ArticleTypes.ExternalCertification)]
        public void GetAvailability_GivenActivityTypeWithEndDateAndValidAvailabilityTimespan_EndDateIsOnlyTargetDate(ArticleTypes articleType)
        {
            var earliestDate = DateTime.UtcNow - TimeSpan.FromDays(2);
            var latestDate = DateTime.UtcNow + TimeSpan.FromDays(5);

            var article = GetArticle(articleType);
            var customerArticle = new CustomerArticle { ArticleCopy = article };
            var activity = new Activity
            {
                Id = 1,
                CustomerArticle = customerArticle,
                // End date is before end of availability period
                EndDate = latestDate - TimeSpan.FromDays(1),
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {Start = earliestDate, End = DateTime.UtcNow + TimeSpan.FromHours(1)},
                    new ActivityPeriod {Start = DateTime.UtcNow - TimeSpan.FromHours(1), End = latestDate}
                }
            };
            var participant = new Participant { ActivityId = activity.Id };
            ConstructMockObjects(participant, activity, article, customerArticle);

            var availability = _participantBusiness.GetAvailability(participant.Id);

            Assert.AreEqual(true, availability.IsAvailable);
            Assert.AreEqual(earliestDate, availability.AvailabilityStart.Value);
            // Activity available entire availability period
            Assert.AreEqual(latestDate, availability.AvailabilityEnd.Value);
        }

        [Test]
        [TestCase(ArticleTypes.Classroom)]
        [TestCase(ArticleTypes.CaseExam)]
        [TestCase(ArticleTypes.ProjectAssignment)]
        [TestCase(ArticleTypes.Other)]
        [TestCase(ArticleTypes.Mockexam)]
        [TestCase(ArticleTypes.ExternalCertification)]
        public void GetAvailability_GivenActivityTypeWithDurationAndValidAvailabilityTimespan_AvailabilityTimespanDeterminesAvailability(ArticleTypes articleType)
        {
            int activityId = 1;
            var earliestDate = DateTime.UtcNow - TimeSpan.FromDays(2);
            var latestDate = DateTime.UtcNow + TimeSpan.FromDays(5);

            var article = GetArticle(articleType);
            article.Id = 3;

            var customerArticle = new CustomerArticle {Id = 2, ArticleCopy = article, ArticleCopyId = article.Id};

            var activity = new Activity
            {
                Id = activityId,
                CustomerArticle = customerArticle,
                CustomerArticleId = customerArticle.Id,
                // Duration goes beyond availability timespan
                Duration = (long) TimeSpan.FromDays(365).TotalMilliseconds,
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {Id = 1, ActivityId = activityId, Start = earliestDate, End = DateTime.UtcNow + TimeSpan.FromHours(1)},
                    new ActivityPeriod {Id = 2, ActivityId = activityId, Start = DateTime.UtcNow - TimeSpan.FromHours(1), End = latestDate}
                }
            };

            var participant = new Participant
            {
                Id = 1,
                ActivityId = activity.Id,
                Created = earliestDate + TimeSpan.FromDays(1)
            };
            ConstructMockObjects(participant, activity, article, customerArticle);

            var availability = _participantBusiness.GetAvailability(participant.Id);

            Assert.AreEqual(true, availability.IsAvailable);
            Assert.AreEqual(earliestDate, availability.AvailabilityStart.Value);
            // Activity available for availability period
            Assert.AreEqual(latestDate, availability.AvailabilityEnd.Value);
        }

        [Test]
        [TestCase(ArticleTypes.ELearning)]
        public void GetAvailability_GivenElearningActivityWithDurationAndValidAvailabilityTimespan_DurationDeterminesAvailability(ArticleTypes articleType)
        {
            int activityId = 1;
            var earliestDate = DateTime.UtcNow - TimeSpan.FromDays(2);
            //var latestDate = DateTime.UtcNow + TimeSpan.FromDays(5);

            var article = GetArticle(articleType);
            article.Id = 3;

            var customerArticle = new CustomerArticle {Id = 2, ArticleCopy = article, ArticleCopyId = article.Id};

            var activity = new Activity
            {
                Id = activityId,
                CustomerArticle = customerArticle,
                CustomerArticleId = customerArticle.Id,
                // Duration goes beyond availability timespan
                Duration = (long) TimeSpan.FromDays(365).TotalMilliseconds,
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {Id = 1, ActivityId = activityId, Start = earliestDate}
                    //new ActivityPeriod {Id = 1, ActivityId = activityId, Start = earliestDate, End = latestDate}
                }
            };

            var participant = new Participant
            {
                Id = 1,
                ActivityId = activity.Id,
                Created = earliestDate + TimeSpan.FromDays(1)
            };
            ConstructMockObjects(participant, activity, article, customerArticle);

            var availability = _participantBusiness.GetAvailability(participant.Id);

            Assert.AreEqual(true, availability.IsAvailable);
            Assert.AreEqual(participant.Created, availability.AvailabilityStart.Value);
            // Activity available for availability period
            Assert.AreEqual(participant.Created.AddMilliseconds(activity.Duration.GetValueOrDefault()), availability.AvailabilityEnd.Value);
        }

        [Test]
        [TestCase(ArticleTypes.Classroom)]
        [TestCase(ArticleTypes.CaseExam)]
        [TestCase(ArticleTypes.ProjectAssignment)]
        [TestCase(ArticleTypes.Other)]
        [TestCase(ArticleTypes.Mockexam)]
        [TestCase(ArticleTypes.ExternalCertification)]
        public void GetAvailability_GivenActivityTypeWithinDurationAndPastAvailabilityTimespan_NotAvailable(ArticleTypes articleType)
        {
            var earliestDate = DateTime.UtcNow - TimeSpan.FromDays(10);
            // Activity expired yesterday
            var latestDate = DateTime.UtcNow - TimeSpan.FromDays(1);

            var article = GetArticle(articleType);
            var customerArticle = new CustomerArticle { ArticleCopy = article };
            var activity = new Activity
            {
                Id = 1,
                CustomerArticle = customerArticle,
                // Duration goes beyond availability timespan, activity still within duration
                Duration = (long)TimeSpan.FromDays(365).TotalMilliseconds,
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {Start = earliestDate, End = earliestDate + TimeSpan.FromDays(2)},
                    new ActivityPeriod {Start = earliestDate - TimeSpan.FromDays(1), End = latestDate}
                }
            };
            var participant = new Participant
            {
                ActivityId = activity.Id,
                Created = earliestDate + TimeSpan.FromDays(1)
            };

            ConstructMockObjects(participant, activity, article, customerArticle);

            var availability = _participantBusiness.GetAvailability(participant.Id);

            Assert.AreEqual(false, availability.IsAvailable);
            Assert.IsTrue(availability.AvailabilityStart.HasValue);
            Assert.IsTrue(availability.AvailabilityEnd.HasValue);
        }

        [Test]
        [TestCase(ArticleTypes.ELearning)]
        public void GetAvailability_GivenActivityTypeWithinDurationAndPastAvailabilityTimespan_IsAvailable(ArticleTypes articleType)
        {
            var earliestDate = DateTime.UtcNow - TimeSpan.FromDays(10);
            // Activity expired yesterday
            var latestDate = DateTime.UtcNow - TimeSpan.FromDays(1);

            var article = GetArticle(articleType);
            var customerArticle = new CustomerArticle { ArticleCopy = article };
            var activity = new Activity
            {
                Id = 1,
                CustomerArticle = customerArticle,
                // Duration goes beyond availability timespan, activity still within duration
                Duration = (long)TimeSpan.FromDays(365).TotalMilliseconds,
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {Start = earliestDate, End = latestDate}
                }
            };

            var participant = new Participant
            {
                ActivityId = activity.Id,
                Created = earliestDate + TimeSpan.FromDays(1)
            };

            ConstructMockObjects(participant, activity, article, customerArticle);

            var availability = _participantBusiness.GetAvailability(participant.Id);

            Assert.AreEqual(true, availability.IsAvailable);
            Assert.IsTrue(availability.AvailabilityStart.HasValue);
            Assert.IsTrue(availability.AvailabilityEnd.HasValue);
        }

        [Test]
        [TestCase(ArticleTypes.Classroom)]
        [TestCase(ArticleTypes.CaseExam)]
        [TestCase(ArticleTypes.ProjectAssignment)]
        [TestCase(ArticleTypes.Other)]
        [TestCase(ArticleTypes.Mockexam)]
        [TestCase(ArticleTypes.ExternalCertification)]
        public void GetAvailability_GivenActivityTypePastDurationAndOpenEndedAvailabilityTimespan_AvailableForever(ArticleTypes articleType)
        {
            var earliestDate = DateTime.UtcNow - TimeSpan.FromDays(10);
            // Activity never expires
            var latestDate = (DateTime?)null;

            var article = GetArticle(articleType);
            var customerArticle = new CustomerArticle { ArticleCopy = article };
            var activity = new Activity
            {
                Id = 1,
                // Activity past duration
                CustomerArticle = customerArticle,
                Duration = (long)TimeSpan.FromDays(1).TotalMilliseconds,
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {Start = earliestDate, End = DateTime.UtcNow + TimeSpan.FromHours(1)},
                    new ActivityPeriod {Start = DateTime.UtcNow - TimeSpan.FromHours(1), End = latestDate}
                }
            };
            var participant = new Participant
            {
                ActivityId = activity.Id,
                Created = earliestDate + TimeSpan.FromDays(1)
            };

            ConstructMockObjects(participant, activity, article, customerArticle);

            var availability = _participantBusiness.GetAvailability(participant.Id);

            Assert.AreEqual(true, availability.IsAvailable);
            Assert.AreEqual(earliestDate, availability.AvailabilityStart.Value);
            Assert.IsFalse(availability.AvailabilityEnd.HasValue);
        }

        [Test]
        [TestCase(ArticleTypes.ELearning)]
        public void GetAvailability_GivenActivityTypePastDurationAndOpenEndedAvailabilityTimespan_NotAvailable(ArticleTypes articleType)
        {
            var earliestDate = DateTime.UtcNow - TimeSpan.FromDays(10);
            // Activity never expires
            var latestDate = (DateTime?)null;

            var article = GetArticle(articleType);
            var customerArticle = new CustomerArticle { ArticleCopy = article };
            var activity = new Activity
            {
                Id = 1,
                // Activity past duration
                CustomerArticle = customerArticle,
                Duration = (long)TimeSpan.FromDays(1).TotalMilliseconds,
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {Start = earliestDate, End = latestDate}
                }
            };
            var participant = new Participant
            {
                ActivityId = activity.Id,
                Created = earliestDate - TimeSpan.FromDays(1)
            };

            ConstructMockObjects(participant, activity, article, customerArticle);

            var availability = _participantBusiness.GetAvailability(participant.Id);

            Assert.AreEqual(false, availability.IsAvailable);
            Assert.AreEqual(earliestDate, availability.AvailabilityStart.Value);
            Assert.IsTrue(availability.AvailabilityEnd.HasValue);
        }

        [Test]
        [TestCase(ArticleTypes.MultipleChoice)]
        [TestCase(ArticleTypes.InternalCertification)]
        public void GetAvailability_GivenActivityTypeWithNoExamAndValidAvailabilityTimespan_ActivityAvailable(ArticleTypes articleType)
        {
            var earliestDate = DateTime.UtcNow - TimeSpan.FromDays(2);
            var latestDate = DateTime.UtcNow + TimeSpan.FromDays(5);

            var article = GetArticle(articleType);
            var customerArticle = new CustomerArticle { ArticleCopy = article };
            var activity = new Activity
            {
                Id = 1,
                CustomerArticle = customerArticle,
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {Start = earliestDate, End = DateTime.UtcNow + TimeSpan.FromHours(1)},
                    new ActivityPeriod {Start = DateTime.UtcNow - TimeSpan.FromHours(1), End = latestDate}
                }
            };
            var participant = new Participant {ActivityId = activity.Id};

            ConstructMockObjects(participant, activity, article, customerArticle);

            var availability = _participantBusiness.GetAvailability(participant.Id);

            Assert.AreEqual(true, availability.IsAvailable);
            Assert.AreEqual(earliestDate, availability.AvailabilityStart.Value);
            Assert.AreEqual(latestDate, availability.AvailabilityEnd.Value);
        }

        [Test]
        [TestCase(ArticleTypes.Classroom)]
        [TestCase(ArticleTypes.CaseExam)]
        [TestCase(ArticleTypes.ProjectAssignment)]
        [TestCase(ArticleTypes.Other)]
        [TestCase(ArticleTypes.Mockexam)]
        [TestCase(ArticleTypes.ExternalCertification)]
        public void GetAvailability_GivenActivityTypeParticipationCreatedBeforeValidAvailabilityTimespan_AvailableWithinAvailabilityTimespan(ArticleTypes articleType)
        {
            var earliestDate = DateTime.UtcNow - TimeSpan.FromDays(2);
            var latestDate = DateTime.UtcNow + TimeSpan.FromDays(5);

            var article = GetArticle(articleType);
            var customerArticle = new CustomerArticle { ArticleCopy = article };

            var activity = new Activity
            {
                Id = 1,
                Duration = (long)TimeSpan.FromDays(3).TotalMilliseconds,
                CustomerArticle = customerArticle,
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {Start = earliestDate, End = DateTime.UtcNow + TimeSpan.FromHours(1)},
                    new ActivityPeriod {Start = DateTime.UtcNow - TimeSpan.FromHours(1), End = latestDate}
                }
            };
            var participant = new Participant
            {
                ActivityId = activity.Id,
                Created = earliestDate - TimeSpan.FromDays(1)
            };

            ConstructMockObjects(participant, activity, article, customerArticle);            
            var availability = _participantBusiness.GetAvailability(participant.Id);

            Assert.AreEqual(true, availability.IsAvailable);
            Assert.AreEqual(earliestDate, availability.AvailabilityStart.Value);
            Assert.AreEqual(latestDate, availability.AvailabilityEnd.Value);
        }

        [Test]
        [TestCase(ArticleTypes.ELearning)]
        public void GetAvailability_GivenActivityTypeParticipationCreatedBeforeValidAvailabilityTimespan_AvailableWithinDuration(ArticleTypes articleType)
        {
            var utcNow = DateTime.UtcNow;
            var earliestDate = utcNow - TimeSpan.FromDays(2);
            var latestDate = utcNow + TimeSpan.FromDays(5);

            var article = GetArticle(articleType);
            var customerArticle = new CustomerArticle { ArticleCopy = article };

            var activity = new Activity
            {
                Id = 1,
                Duration = (long)TimeSpan.FromDays(3).TotalMilliseconds,
                CustomerArticle = customerArticle,
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {Start = earliestDate, End = latestDate}
                }
            };
            var participant = new Participant
            {
                ActivityId = activity.Id,
                Created = earliestDate - TimeSpan.FromDays(1)
            };

            ConstructMockObjects(participant, activity, article, customerArticle);
            var availability = _participantBusiness.GetAvailability(participant.Id);

            Assert.AreEqual(true, availability.IsAvailable);
            Assert.AreEqual(earliestDate, availability.AvailabilityStart.Value);
            Assert.AreEqual(utcNow + TimeSpan.FromDays(1), availability.AvailabilityEnd.Value);
        }

        [Test]
        [TestCase(ArticleTypes.Classroom)]
        [TestCase(ArticleTypes.CaseExam)]
        [TestCase(ArticleTypes.ProjectAssignment)]
        [TestCase(ArticleTypes.Other)]
        [TestCase(ArticleTypes.Mockexam)]
        [TestCase(ArticleTypes.ExternalCertification)]
        public void GetAvailability_GivenActivityTypeParticipationCreatedNoValidAvailabilityTimespan_NotAvailable(ArticleTypes articleType)
        {
            var earliestDate = DateTime.UtcNow + TimeSpan.FromDays(2);
            var latestDate = DateTime.UtcNow + TimeSpan.FromDays(7);

            var article = GetArticle(articleType);
            var customerArticle = new CustomerArticle { ArticleCopy = article };
            var activity = new Activity
            {
                Id = 1,
                CustomerArticle = customerArticle,
                Duration = (long)TimeSpan.FromDays(3).TotalMilliseconds,
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {Start = earliestDate, End = earliestDate + TimeSpan.FromDays(2)},
                    new ActivityPeriod {Start = earliestDate + TimeSpan.FromDays(1), End = latestDate}
                }
            };
            var participant = new Participant
            {
                ActivityId = activity.Id,
                // Participant created before activity available
                Created = earliestDate - TimeSpan.FromDays(1)
            };

            ConstructMockObjects(participant, activity, article, customerArticle);

            var availability = _participantBusiness.GetAvailability(participant.Id);

            Assert.AreEqual(false, availability.IsAvailable);
            Assert.IsFalse(availability.AvailabilityStart.HasValue);
            Assert.IsFalse(availability.AvailabilityEnd.HasValue);
        }

        [Test]
        [TestCase(ArticleTypes.MultipleChoice)]
        [TestCase(ArticleTypes.InternalCertification)]
        public void GetAvailability_GivenActivityTypeWithOpenEndedAvailability_ActivityAvailable(ArticleTypes articleType)
        {
            var earliestDate = DateTime.UtcNow - TimeSpan.FromDays(2);

            var article = GetArticle(articleType);
            var customerArticle = new CustomerArticle { ArticleCopy = article };
            var activity = new Activity
            {
                Id = 1,
                CustomerArticle = customerArticle,
                ActivityPeriods = new List<ActivityPeriod>
                {
                    // Open ended
                    new ActivityPeriod {Start = earliestDate, End = null},
                    new ActivityPeriod {Start = DateTime.UtcNow - TimeSpan.FromHours(1), End = DateTime.UtcNow + TimeSpan.FromDays(10)}
                }
            };
            var participant = new Participant { ActivityId = activity.Id };

            ConstructMockObjects(participant, activity, article, customerArticle);
            var availability = _participantBusiness.GetAvailability(participant.Id);

            Assert.AreEqual(true, availability.IsAvailable);
            Assert.AreEqual(earliestDate, availability.AvailabilityStart.Value);
            // No end date
            Assert.IsFalse(availability.AvailabilityEnd.HasValue);
        }

        [Test]
        [TestCase(ArticleTypes.MultipleChoice)]
        [TestCase(ArticleTypes.InternalCertification)]
        [TestCase(ArticleTypes.ELearning)]
        [TestCase(ArticleTypes.Classroom)]
        [TestCase(ArticleTypes.CaseExam)]
        [TestCase(ArticleTypes.ProjectAssignment)]
        [TestCase(ArticleTypes.Other)]
        [TestCase(ArticleTypes.Mockexam)]
        [TestCase(ArticleTypes.ExternalCertification)]
        public void GetAvailability_GivenActivityTypeOutsideValidAvailabilityTimespan_ActivityUnavailable(ArticleTypes articleType)
        {
            var earliestDate = DateTime.UtcNow - TimeSpan.FromDays(2);
            var latestDate = DateTime.UtcNow + TimeSpan.FromDays(5);

            var article = GetArticle(articleType);
            var customerArticle = new CustomerArticle { ArticleCopy = article };

            var activity = new Activity
            {
                Id = 1,
                CustomerArticle = customerArticle,
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {Start = earliestDate, End = DateTime.UtcNow - TimeSpan.FromHours(1)},
                    new ActivityPeriod {Start = DateTime.UtcNow + TimeSpan.FromHours(1), End = latestDate}
                }
            };

            var participant = new Participant { ActivityId = activity.Id };
            ConstructMockObjects(participant, activity, article, customerArticle);

            var availability = _participantBusiness.GetAvailability(participant.Id);

            Assert.IsFalse(availability.IsAvailable);
            Assert.IsTrue(availability.AvailabilityStart.HasValue);
            Assert.IsTrue(availability.AvailabilityEnd.HasValue);
        }

        [Test]
        [TestCase(ArticleTypes.MultipleChoice)]
        [TestCase(ArticleTypes.InternalCertification)]
        public void GetAvailability_GivenActivityTypeWithExamAndExamDurationLongerThanAvailabilityPeriod_AvailabilityExtended(ArticleTypes articleType)
        {
            var earliestDate = DateTime.UtcNow - TimeSpan.FromDays(2);
            var latestDate = DateTime.UtcNow + TimeSpan.FromMinutes(30);

            var article = GetArticle(articleType);
            var customerArticle = new CustomerArticle { ArticleCopy = article };

            var activity = new Activity
            {
                Id = 1,
                CustomerArticle = customerArticle,
                // Duration 2 hours
                Duration = (long)TimeSpan.FromHours(2).TotalMilliseconds,
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {Start = earliestDate, End = DateTime.UtcNow + TimeSpan.FromMinutes(15)},
                    new ActivityPeriod {Start = DateTime.UtcNow - TimeSpan.FromHours(1), End = latestDate}
                }
            };
            var participant = new Participant { Id = 2, ActivityId = activity.Id };
            // Exam started 1 hour before availablility period ends
            var exam = new Exam {ParticipantId = 2, Created = latestDate - TimeSpan.FromHours(1)};

            ConstructMockObjects(participant, activity, article, customerArticle, exam);

            var availability = _participantBusiness.GetAvailability(participant.Id);

            Assert.AreEqual(true, availability.IsAvailable);
            Assert.AreEqual(earliestDate, availability.AvailabilityStart.Value);
            // Availability period extended until exam duration ends
            Assert.AreEqual(exam.Created + TimeSpan.FromMilliseconds(activity.Duration.Value), availability.AvailabilityEnd.Value);
        }

        [Test]
        [TestCase(ArticleTypes.MultipleChoice)]
        [TestCase(ArticleTypes.InternalCertification)]
        public void GetAvailability_GivenActivityTypeWithValidExamAndAvailabilityPeriodExpired_AvailabilityExtended(ArticleTypes articleType)
        {
            var earliestDate = DateTime.UtcNow - TimeSpan.FromDays(2);
            // Availability period expired 2 hours ago
            var latestDate = DateTime.UtcNow - TimeSpan.FromHours(2);

            var article = GetArticle(articleType);
            var customerArticle = new CustomerArticle { ArticleCopy = article };

            var activity = new Activity
            {
                Id = 1,
                CustomerArticle = customerArticle,
                // Duration 10 hours
                Duration = (long)TimeSpan.FromHours(10).TotalMilliseconds,
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {Start = earliestDate, End = DateTime.UtcNow - TimeSpan.FromHours(3)},
                    new ActivityPeriod {Start = DateTime.UtcNow - TimeSpan.FromHours(4), End = latestDate}
                }
            };
            var participant = new Participant { Id = 2, ActivityId = activity.Id };
            // Exam started 3,5 hours ago
            var exam = new Exam { ParticipantId = 2, Created = DateTime.UtcNow - TimeSpan.FromHours(3) - TimeSpan.FromMinutes(30) };

            ConstructMockObjects(participant, activity, article, customerArticle, exam);

            var availability = _participantBusiness.GetAvailability(participant.Id);

            Assert.AreEqual(true, availability.IsAvailable);
            Assert.AreEqual(earliestDate, availability.AvailabilityStart.Value);
            // Availability period extended until exam duration ends
            Assert.AreEqual(exam.Created + TimeSpan.FromMilliseconds(activity.Duration.Value), availability.AvailabilityEnd.Value);
        }

        [Test]
        [TestCase(ArticleTypes.MultipleChoice)]
        [TestCase(ArticleTypes.InternalCertification)]
        public void GetAvailability_GivenActivityTypeWithExamAndParticipantHasAdditionalTime_AvailabilityExtended(ArticleTypes articleType)
        {
            var earliestDate = DateTime.UtcNow - TimeSpan.FromDays(2);
            var latestDate = DateTime.UtcNow + TimeSpan.FromMinutes(30);

            var article = GetArticle(articleType);
            var customerArticle = new CustomerArticle { ArticleCopy = article };
            var activity = new Activity
            {
                Id = 1,
                // Duration 2 hours
                Duration = (long)TimeSpan.FromHours(2).TotalMilliseconds,
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {Start = earliestDate, End = DateTime.UtcNow + TimeSpan.FromMinutes(15)},
                    new ActivityPeriod {Start = DateTime.UtcNow - TimeSpan.FromHours(1), End = latestDate}
                }
            };
            var participant = new Participant
            {
                Id = 2, ActivityId = activity.Id,
                // Additional time 1 hour
                AdditionalTime = (long)TimeSpan.FromHours(1).TotalMilliseconds
            };
            // Exam started 1 hour before availablility period ends
            var exam = new Exam { ParticipantId = 2, Created = latestDate - TimeSpan.FromHours(1) };
            ConstructMockObjects(participant, activity, article, customerArticle, exam);

            var availability = _participantBusiness.GetAvailability(participant.Id);

            Assert.AreEqual(true, availability.IsAvailable);
            Assert.AreEqual(earliestDate, availability.AvailabilityStart.Value);
            // Availability period extended until exam duration + additional time ends
            Assert.AreEqual(exam.Created + TimeSpan.FromMilliseconds(activity.Duration.Value) + TimeSpan.FromMilliseconds(participant.AdditionalTime.Value), availability.AvailabilityEnd.Value);
        }

        [Test]
        [TestCase(ArticleTypes.ELearning)]
        public void GetAvailability_GivenActivityTypeWithElearningAndParticipantHasAdditionalTime_AvailabilityExtended(ArticleTypes articleType)
        {
            var earliestDate = DateTime.UtcNow - TimeSpan.FromDays(2);
            var latestDate = DateTime.UtcNow - TimeSpan.FromMinutes(10);

            var article = GetArticle(articleType);
            var customerArticle = new CustomerArticle { ArticleCopy = article };
            var activity = new Activity
            {
                Id = 1,
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {Start = earliestDate, End = DateTime.UtcNow - TimeSpan.FromMinutes(15)},
                    new ActivityPeriod {Start = DateTime.UtcNow - TimeSpan.FromHours(1), End = latestDate}
                }
            };

            var participant = new Participant
            {
                Id = 2, ActivityId = activity.Id,
                AdditionalTime = (long)TimeSpan.FromHours(1).TotalMilliseconds                          // Additional time 1 hour
            };

            ConstructMockObjects(participant, activity, article, customerArticle);

            var availability = _participantBusiness.GetAvailability(participant.Id);

            Assert.AreEqual(true, availability.IsAvailable);
            Assert.AreEqual(earliestDate, availability.AvailabilityStart.Value);
            // Availability period extended until exam duration + additional time ends
            Assert.AreEqual(latestDate + TimeSpan.FromMilliseconds(participant.AdditionalTime.Value), availability.AvailabilityEnd.Value);
        }

        [Test]
        [TestCase(ArticleTypes.MultipleChoice)]
        [TestCase(ArticleTypes.InternalCertification)]
        public void GetAvailability_GivenActivityTypeWithExamAndExamDurationWithinAvailabilityPeriod_AvailabilityShortened(ArticleTypes articleType)
        {
            var earliestDate = DateTime.UtcNow - TimeSpan.FromDays(2);
            var latestDate = DateTime.UtcNow + TimeSpan.FromDays(5);

            var article = GetArticle(articleType);
            var customerArticle = new CustomerArticle { ArticleCopy = article };
            var activity = new Activity
            {
                Id = 1,
                CustomerArticle = customerArticle,
                // Duration 2 hours
                Duration = (long)TimeSpan.FromHours(2).TotalMilliseconds,
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {Start = earliestDate, End = DateTime.UtcNow + TimeSpan.FromHours(1)},
                    new ActivityPeriod {Start = DateTime.UtcNow - TimeSpan.FromHours(1), End = latestDate}
                }
            };
            var participant = new Participant
            {
                Id = 2, ActivityId = activity.Id,
                // 30 minuites additional time
                AdditionalTime = (long)TimeSpan.FromMinutes(30).TotalMilliseconds
            };
            // Exam started 1 hours ago, thus 1 hour left
            var exam = new Exam { ParticipantId = 2, Created = DateTime.UtcNow - TimeSpan.FromHours(1) };
            ConstructMockObjects(participant, activity, article, customerArticle, exam);

            var availability = _participantBusiness.GetAvailability(participant.Id);

            Assert.AreEqual(true, availability.IsAvailable);
            Assert.AreEqual(earliestDate, availability.AvailabilityStart.Value);
            // Available until exam ends
            Assert.AreEqual(exam.Created + TimeSpan.FromMilliseconds(activity.Duration.Value) + TimeSpan.FromMilliseconds(participant.AdditionalTime.Value), availability.AvailabilityEnd.Value);
        }

        [Test]
        [TestCase(ArticleTypes.MultipleChoice)]
        [TestCase(ArticleTypes.InternalCertification)]
        public void GetAvailability_GivenActivityTypeWithExamAndExamDurationWithinAvailabilityPeriodAndExamExpired_Unavailable(ArticleTypes articleType)
        {
            var earliestDate = DateTime.UtcNow - TimeSpan.FromDays(2);
            var latestDate = DateTime.UtcNow + TimeSpan.FromDays(5);

            var article = GetArticle(articleType);
            var customerArticle = new CustomerArticle { ArticleCopy = article };
            var activity = new Activity
            {
                Id = 1,
                CustomerArticle = customerArticle,
                // Duration 2 hours
                Duration = (long)TimeSpan.FromHours(2).TotalMilliseconds,
                ActivityPeriods = new List<ActivityPeriod>
                {
                    new ActivityPeriod {Start = earliestDate, End = DateTime.UtcNow + TimeSpan.FromHours(1)},
                    new ActivityPeriod {Start = DateTime.UtcNow - TimeSpan.FromHours(1), End = latestDate}
                }
            };
            var participant = new Participant
            {
                Id = 2,
                ActivityId = activity.Id,
                // 30 minuites additional time
                AdditionalTime = (long)TimeSpan.FromMinutes(30).TotalMilliseconds
            };
            // Exam started 3 hours ago, thus finished
            var exam = new Exam { ParticipantId = 2, Created = DateTime.UtcNow - TimeSpan.FromHours(3) };
            ConstructMockObjects(participant, activity, article, customerArticle, exam);

            var availability = _participantBusiness.GetAvailability(participant.Id);

            Assert.AreEqual(false, availability.IsAvailable);
            Assert.IsFalse(availability.AvailabilityStart.HasValue);
            Assert.IsFalse(availability.AvailabilityEnd.HasValue);
        }
        
        private Article GetArticle(ArticleTypes articleType)
        {
            switch (articleType)
            {
                case ArticleTypes.ELearning:
                    return new ElearningArticle {ArticleTypeId = (int) articleType};
                case ArticleTypes.CaseExam:
                    return new CaseExamArticle {ArticleTypeId = (int) articleType};
                case ArticleTypes.ExternalCertification:
                    return new CaseExamArticle {ArticleTypeId = (int) articleType};
                case ArticleTypes.ProjectAssignment:
                    return new CaseExamArticle {ArticleTypeId = (int) articleType};
                case ArticleTypes.InternalCertification:
                    return new ExamArticle {ArticleTypeId = (int) articleType};
                case ArticleTypes.Mockexam:
                    return new MockexamArticle {ArticleTypeId = (int) articleType};
                case ArticleTypes.MultipleChoice:
                    return new ExamArticle {ArticleTypeId = (int) articleType};
                case ArticleTypes.Other:
                    return new OtherArticle {ArticleTypeId = (int) articleType};
                case ArticleTypes.Seminar:
                    return new SeminarArticle {ArticleTypeId = (int) articleType};
                case ArticleTypes.Webinar:
                    return new WebinarArticle {ArticleTypeId = (int) articleType};
                case ArticleTypes.Classroom:
                    return new ClassroomArticle {ArticleTypeId = (int) articleType};
            }

            return null;
        }
    }
}