﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Core.Business.DateAndTime;
using Metier.Phoenix.Core.Exceptions;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Core.Business.DateAndTime
{
    [TestFixture]
    public class WeekTests
    {

        [Test]
        public void GetWeek_GivenValidWeekAndYear_ShouldReturnNewWeek()
        {
            var week = new IsoWeek(52, 2013);       //Last date in 2013 belongs to the first week of 2014
            Assert.IsNotNull(week);

            week = new IsoWeek(1, 2016);
            Assert.IsNotNull(week);
        }

        [Test]
        public void GetIsoWeekFromDate_GivenValidDate_ShouldReturnCorrectWeek()
        {
            var week = IsoWeek.GetIsoWeekFromDate(new DateTime(2016, 10, 10));       
            Assert.IsTrue(new IsoWeek(41, 2016).Equals(week));

            week = IsoWeek.GetIsoWeekFromDate(new DateTime(2013, 12, 31));       //Last date in 2013 belongs to the first week of 2014
            Assert.IsTrue(new IsoWeek(1, 2014).Equals(week));

            week = IsoWeek.GetIsoWeekFromDate(new DateTime(2016, 1, 2));       //First saterday in 2015 belongs to the week 53 of 2015
            Assert.IsTrue(new IsoWeek(53, 2015).Equals(week));
        }

        [Test]
        public void GetWeek_GivenInvalidWeekNo_ShouldThrowValidationException()
        {
            Assert.Throws(Is.TypeOf<ValidationException>(), () => new IsoWeek(0, 2016));
            Assert.Throws(Is.TypeOf<ValidationException>(), () => new IsoWeek(55, 2016));
            Assert.Throws(Is.TypeOf<ValidationException>(), () => new IsoWeek(53, 2016));   //Max week number for 2016 is 52
        }

        [Test]
        [TestCase(1, 2016, 2, 2016)]
        [TestCase(1, 2016, 2, 2017)]
        [TestCase(52, 2016, 2, 2017)]
        public void LessThanOperator_GivenFirstWeekIsBeforeLastWeek_ShouldReturnCorrectComparison(int firstWeekNo, int firstYear, int lastWeekNo, int lastYear)
        {
            IsoWeek firstWeek = new IsoWeek(firstWeekNo, firstYear);
            IsoWeek lastWeek = new IsoWeek(lastWeekNo, lastYear);

            Assert.IsTrue(firstWeek < lastWeek);
        }

        [Test]
        [TestCase(1, 2016, 2, 2016)]
        [TestCase(1, 2016, 2, 2017)]
        [TestCase(52, 2016, 2, 2017)]
        [TestCase(52, 2016, 52, 2016)]
        public void GreaterThanOrEqualOperator_GivenFirstWeekIsBeforeLastWeek_ShouldReturnCorrectComparison(int firstWeekNo, int firstYear, int lastWeekNo, int lastYear)
        {
            IsoWeek firstWeek = new IsoWeek(firstWeekNo, firstYear);
            IsoWeek lastWeek = new IsoWeek(lastWeekNo, lastYear);

            Assert.IsTrue(lastWeek >= firstWeek);
        }

        [Test]
        [TestCase(1, 2016, 1, 2016)]
        [TestCase(12, 2015, 12, 2015)]
        [TestCase(52, 2017, 52, 2017)]
        public void Equals_GivenFirstWeekIsEqualToLastWeek_ShouldReturnTrue(int firstWeekNo, int firstYear, int lastWeekNo, int lastYear)
        {
            IsoWeek firstWeek = new IsoWeek(firstWeekNo, firstYear);
            IsoWeek lastWeek = new IsoWeek(lastWeekNo, lastYear);
            
            Assert.IsTrue(firstWeek.Equals(lastWeek));
        }

        //Not really a test of IsoWeek, but wants to make sure sorting a list of IsoWeeks works as expected
        [Test]
        public void SortList_GivenUnorderedListOfWeeks_ShouldSortListCorrectly()
        {
            var weeks = new List<IsoWeek>();

            weeks.Add(new IsoWeek(43, 2016));
            weeks.Add(new IsoWeek(51, 2013));
            weeks.Add(new IsoWeek(4, 2016));
            weeks.Add(new IsoWeek(16, 2015));
            weeks.Add(new IsoWeek(4, 2016));
            weeks.Add(new IsoWeek(1, 2018));

            var sortedWeeks = weeks.OrderBy(x=>x);

            IsoWeek currentWeek = sortedWeeks.ElementAt(0);
            foreach (var week in sortedWeeks)
            {
                Assert.IsTrue(week >= currentWeek);
                currentWeek = week;
            }

        }
    }
}
