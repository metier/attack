﻿using System;
using System.Linq;
using Metier.Phoenix.Api.Facade.Account;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Security;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Core.Security
{
    [TestFixture]
    public class PasswordResetRequestFactoryTests
    {
        private ContextMock _contextMock;

        [SetUp]
        public void Setup()
        {
            _contextMock = new ContextMock();
        }

        [Test]
        public void CreatePasswordResetRequest_RequestingResetWithExistingResetRequest_DeletesExistingResetRequest()
        {
            var existingPasswordResetToken = Guid.NewGuid();
            var userAccount = CreateUserAccount();

            _contextMock.Object.PasswordResetRequests.Add(new PasswordResetRequest
            {
                UserId = userAccount.User.Id,
                ResetToken = existingPasswordResetToken
            });

            Assert.AreEqual(1, _contextMock.Object.PasswordResetRequests.Count(p => p.ResetToken == existingPasswordResetToken));

            var passwordResetRequestFactory = new PasswordResetRequestFactory(_contextMock.Object);
            passwordResetRequestFactory.CreatePasswordResetRequest(userAccount.User);

            Assert.AreEqual(0, _contextMock.Object.PasswordResetRequests.Count(p => p.ResetToken == existingPasswordResetToken));
            Assert.AreEqual(1, _contextMock.Object.PasswordResetRequests.Count(p => p.UserId == userAccount.User.Id));
        }

        [Test]
        public void CreatePasswordResetRequest_ValidPasswordReset_RegistersPasswordResetInDatabase()
        {
            var userAccount = CreateUserAccount();

            var passwordResetRequestFactory = new PasswordResetRequestFactory(_contextMock.Object);
            passwordResetRequestFactory.CreatePasswordResetRequest(userAccount.User);

            Assert.AreEqual(1, _contextMock.Object.PasswordResetRequests.Count(p => p.UserId == userAccount.User.Id));
        }

        private UserAccount CreateUserAccount()
        {
            var userAccount = new UserAccount
            {
                Username = "username@test.com",
                Email = "useremail@test.com",
                User = new User
                {
                    Id = 12345
                }
            };
            return userAccount;
        }
    }
}