﻿using Metier.Phoenix.Core.Data.Entities;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Core.Data.Entities
{
    [TestFixture]
    public class ProductTests
    {
        [Test]
        public void ProductPath_ShouldReturnProductCategoryPrefixWithProductNumber()
        {
            var product = new Product();
            product.ProductNumber = "07.5";
            product.ProductCategory = new ProductCategory { ProductCodePrefix = "PM" };

            Assert.AreEqual("PM07.5", product.ProductPath);
        }
    }
}