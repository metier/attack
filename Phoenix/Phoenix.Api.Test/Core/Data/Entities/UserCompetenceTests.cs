﻿using System;
using Metier.Phoenix.Core.Data.Entities;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Core.Data.Entities
{
    [TestFixture]
    public class UserCompetenceTests
    {
        [Test]
        [TestCase("01029849818", Result = "F")]
        [TestCase("01029849737", Result = "M")]
        [TestCase("0102984973", Result = "")]
        [TestCase("010298497371", Result = "")]
        [TestCase("", Result = "")]
        [TestCase(null, Result = "")]
        public string GetDateOfBirthFromSocialSecurityNumber_GivenSocialSecurityNumber_ReturnsExpectedResult(string socialSecurityNumber)
        {
            var userCompetence = new UserCompetence {SocialSecurityNumber = socialSecurityNumber};
            return userCompetence.GetSexFromSocialSecurityNumber();
        }

        [Test]
        [TestCase("01029849818", Result = "01.02.1998")]
        [TestCase("04080098721", Result = "04.08.2000")]
        [TestCase("0102984973", Result = "")]
        [TestCase("010298497371", Result = "")]
        [TestCase("", Result = "")]
        [TestCase(null, Result = "")]
        public string GetDateOfBirthFromSocialSecurityNumber_GivenSocialSecurityNumber_ReturnsExceptedResult(string socialSecurityNumber)
        {
            var userCompetence = new UserCompetence { SocialSecurityNumber = socialSecurityNumber };
            var dateTime = userCompetence.GetDateOfBirthFromSocialSecurityNumber();
            if (dateTime != null)
            {
                return dateTime.Value.ToString("dd.MM.yyyy");
            }
            return "";
        }
    }
}