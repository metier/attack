﻿using Metier.Phoenix.Core.Data.Entities;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Core.Data.Entities
{
    [TestFixture]
    public class ArticleTests
    {
        [Test]
        public void ArticlePath_ShouldReturnProductPathWithVersionWithLanguage()
        {
            var productMock = new Mock<Product>();
            productMock.SetupGet(p => p.ProductPath).Returns("PM-01");
            productMock.Setup(p => p.ProductCategory).Returns(new ProductCategory());
            
            var article = new ElearningArticle
            {
                Product = productMock.Object,
                Language = new Language
                {
                    Identifier = "no"
                },
                ArticleType = new ArticleType
                {
                    CharCode = "e"
                }
            };

            Assert.AreEqual("PM-01E-NO", article.ArticlePath);
        }

        [Test]
        public void Article_MustBeAbstract()
        {
            Assert.IsTrue(typeof(Article).IsAbstract);
        }
    }
}