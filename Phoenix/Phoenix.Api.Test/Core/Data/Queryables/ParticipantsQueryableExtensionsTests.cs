﻿using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Queryables;
using Moq;
using NUnit.Framework;

namespace Metier.Phoenix.UnitTests.Core.Data.Queryables
{
    [TestFixture]
    public class ParticipantsQueryableExtensionsTests
    {
        private int _ids;

        private ContextMock _contextMock;

        private Customer _mockCustomer;
        private ProductCategory _projectManagementProductCategory;
        private Product _projectManagementProduct;
        private ArticleType _eLearningArticleType;
        private ArticleType _classroomArticleType;
        private Language _norwegianLanguage;

        [SetUp]
        public void Setup()
        {
            _contextMock = new ContextMock();

            _mockCustomer = new Customer { Id = 2338, Name = "Diverse kunde", IsArbitrary = false };
            _projectManagementProductCategory = new ProductCategory
            {
                Id = 1,
                Name = "Project Management",
                ProductCodePrefix = "PM"
            };
            _projectManagementProduct = new Product
            {
                Id = 1,
                ProductCategory = _projectManagementProductCategory,
                ProductCategoryId = _projectManagementProductCategory.Id,
                ProductNumber = "02"
            };
            _eLearningArticleType = new ArticleType { Id = 1, Name = "E-learning", CharCode = "E" };
            _classroomArticleType = new ArticleType { Id = 2, Name = "Classroom", CharCode = "C" };
            _norwegianLanguage = new Language { Id = 45, Name = "Norwegian", Identifier = "NO" };
        }

        #region Qualification status tests


        [Test]
        [TestCase(ParticipantStatusCodes.Cancelled, false, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.NoShow, false, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.NotAttended, false, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.OnHold, false, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.Enrolled, false, Result = ParticipantQualificationStatus.PartiallyQualified)]
        [TestCase(ParticipantStatusCodes.InProgress, false, Result = ParticipantQualificationStatus.PartiallyQualified)]
        [TestCase(ParticipantStatusCodes.Completed, false, Result = ParticipantQualificationStatus.Qualified)]
        [TestCase(ParticipantStatusCodes.Dispensation, false, Result = ParticipantQualificationStatus.Qualified)]
        [TestCase(ParticipantStatusCodes.Cancelled, true, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.NoShow, true, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.NotAttended, true, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.OnHold, true, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.Enrolled, true, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.InProgress, true, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.Completed, true, Result = ParticipantQualificationStatus.Qualified)]
        [TestCase(ParticipantStatusCodes.Dispensation, true, Result = ParticipantQualificationStatus.Qualified)]
        public string
            GetParticipantsWithQualificationStatus_GivenActivityWithEnrollmeltConditions_ShouldReturnParticipantsWithCorrectQualificationStatus
            (string participantStatusCode, bool isStrict = false)
        {
            var examActivity = CreateMultipleChoiceExamActivity(true, isStrict);
            var activity = CreateElearningActivity();

            var user1 = new User { Id = 1 };
            var participant1U1 = new Participant
            {
                Id = 1,
                ActivityId = examActivity.Id,
                UserId = user1.Id,
                EnrollmentId = 1,
                IsDeleted = false
            };
            var participant2U1 = new Participant
            {
                Id = 2,
                ActivityId = activity.Id,
                UserId = user1.Id,
                IsDeleted = false,
                StatusCodeValue = participantStatusCode
            };

            _contextMock.Object.Users.Add(user1);
            _contextMock.Object.Participants.Add(participant1U1);
            _contextMock.Object.Participants.Add(participant2U1);

            //Avoiding 'Activity.IncludeParticipants' as this calls context.Entry which is a method that must be mocked and makes the test setup more complex.
            _contextMock.Object.Participants.Where(p => p.ActivityId == examActivity.Id)
                .WithQualificationStatus(examActivity, _contextMock.Object);

            var returnedParticipant = _contextMock.Object.Participants.First(p => p.Id == participant1U1.Id);
            return returnedParticipant.QualificationStatus;
        }

        [Test]
        [TestCase(ParticipantStatusCodes.Cancelled, Result = ParticipantQualificationStatus.Qualified)]
        [TestCase(ParticipantStatusCodes.NoShow, Result = ParticipantQualificationStatus.Qualified)]
        [TestCase(ParticipantStatusCodes.NotAttended, Result = ParticipantQualificationStatus.Qualified)]
        [TestCase(ParticipantStatusCodes.OnHold, Result = ParticipantQualificationStatus.Qualified)]
        [TestCase(ParticipantStatusCodes.Enrolled, Result = ParticipantQualificationStatus.Qualified)]
        [TestCase(ParticipantStatusCodes.InProgress, Result = ParticipantQualificationStatus.Qualified)]
        [TestCase(ParticipantStatusCodes.Completed, Result = ParticipantQualificationStatus.Qualified)]
        [TestCase(ParticipantStatusCodes.Dispensation, Result = ParticipantQualificationStatus.Qualified)]
        public string
            GetParticipantsWithQualificationStatus_GivenActivityWithoutEnrollmeltConditions_ShouldReturnAllParticipantsAsQualified
            (string participantStatusCode)
        {
            var examActivity = CreateMultipleChoiceExamActivity(false);
            var activity = CreateElearningActivity();

            var user1 = new User { Id = 1 };
            var participant1U1 = new Participant
            {
                Id = 1,
                ActivityId = examActivity.Id,
                UserId = user1.Id,
                EnrollmentId = 1,
                IsDeleted = false
            };
            var participant2U1 = new Participant
            {
                Id = 2,
                ActivityId = activity.Id,
                UserId = user1.Id,
                IsDeleted = false,
                StatusCodeValue = participantStatusCode
            };

            _contextMock.Object.Users.Add(user1);
            _contextMock.Object.Participants.Add(participant1U1);
            _contextMock.Object.Participants.Add(participant2U1);

            //Avoiding 'Activity.IncludeParticipants' as this calls context.Entry which is a method that must be mocked and makes the test setup more complex.
            _contextMock.Object.Participants.Where(p => p.ActivityId == examActivity.Id)
                .WithQualificationStatus(examActivity, _contextMock.Object);

            var returnedParticipant = _contextMock.Object.Participants.First(p => p.Id == participant1U1.Id);
            return returnedParticipant.QualificationStatus;
        }

        [Test]
        [TestCase(ParticipantStatusCodes.Cancelled, false, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.NoShow, false, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.NotAttended, false, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.OnHold, false, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.Enrolled, false, Result = ParticipantQualificationStatus.PartiallyQualified)]
        [TestCase(ParticipantStatusCodes.InProgress, false, Result = ParticipantQualificationStatus.PartiallyQualified)]
        [TestCase(ParticipantStatusCodes.Completed, false, Result = ParticipantQualificationStatus.Qualified)]
        [TestCase(ParticipantStatusCodes.Dispensation, false, Result = ParticipantQualificationStatus.Qualified)]
        [TestCase(ParticipantStatusCodes.Cancelled, true, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.NoShow, true, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.NotAttended, true, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.OnHold, true, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.Enrolled, true, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.InProgress, true, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.Completed, true, Result = ParticipantQualificationStatus.Qualified)]
        [TestCase(ParticipantStatusCodes.Dispensation, true, Result = ParticipantQualificationStatus.Qualified)]
        public string
            GetParticipantsWithQualificationStatus_GivenActivityWithMultipleEnrollmeltConditions_ShouldReturnCorrectQualificationStatus
            (string participantStatusCode, bool isStrict = false)
        {
            var examActivity = CreateMultipleChoiceExamActivity();
            var elearningActivity = CreateElearningActivity();
            var classroomActivity = CreateClassroomActivity();

            CreateClassroomEnrollmentConditionOnActivity(examActivity, isStrict);

            var user1 = new User { Id = 1 };
            var participant1U1 = new Participant
            {
                Id = 1,
                ActivityId = examActivity.Id,
                UserId = user1.Id,
                EnrollmentId = 1,
                IsDeleted = false
            };
            var participant2U1 = new Participant
            {
                Id = 2,
                ActivityId = elearningActivity.Id,
                UserId = user1.Id,
                IsDeleted = false,
                StatusCodeValue = ParticipantStatusCodes.Completed
            };
            var participant3U1 = new Participant
            {
                Id = 3,
                ActivityId = classroomActivity.Id,
                UserId = user1.Id,
                IsDeleted = false,
                StatusCodeValue = participantStatusCode
            };

            _contextMock.Object.Users.Add(user1);
            _contextMock.Object.Participants.Add(participant1U1);
            _contextMock.Object.Participants.Add(participant2U1);
            _contextMock.Object.Participants.Add(participant3U1);

            //Avoiding 'Activity.IncludeParticipants' as this calls context.Entry which is a method that must be mocked and makes the test setup more complex.
            _contextMock.Object.Participants.Where(p => p.ActivityId == examActivity.Id)
                .WithQualificationStatus(examActivity, _contextMock.Object);

            var returnedParticipant = _contextMock.Object.Participants.First(p => p.Id == participant1U1.Id);
            return returnedParticipant.QualificationStatus;
        }

        [Test]
        [TestCase(ParticipantStatusCodes.Cancelled, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.NoShow, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.NotAttended, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.OnHold, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.Enrolled, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.InProgress, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.Completed, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.Dispensation, Result = ParticipantQualificationStatus.NotQualified)]
        public string
            GetParticipantsWithQualificationStatus_GivenActivityWithUnfulfilledEnrollmeltConditions_ShouldReturnAllParticipantsAsNotQualified
            (string participantStatusCode)
        {
            var examActivity = CreateMultipleChoiceExamActivity();
            var activity = CreateElearningActivity();

            CreateClassroomEnrollmentConditionOnActivity(examActivity);

            var user1 = new User { Id = 1 };
            var participant1U1 = new Participant
            {
                Id = 1,
                ActivityId = examActivity.Id,
                UserId = user1.Id,
                EnrollmentId = 1,
                IsDeleted = false
            };
            var participant2U1 = new Participant
            {
                Id = 2,
                ActivityId = activity.Id,
                UserId = user1.Id,
                IsDeleted = false,
                StatusCodeValue = participantStatusCode
            };

            _contextMock.Object.Users.Add(user1);
            _contextMock.Object.Participants.Add(participant1U1);
            _contextMock.Object.Participants.Add(participant2U1);

            //Avoiding 'Activity.IncludeParticipants' as this calls context.Entry which is a method that must be mocked and makes the test setup more complex.
            _contextMock.Object.Participants.Where(p => p.ActivityId == examActivity.Id)
                .WithQualificationStatus(examActivity, _contextMock.Object);

            var returnedParticipant = _contextMock.Object.Participants.First(p => p.Id == participant1U1.Id);
            return returnedParticipant.QualificationStatus;
        }

        [Test]
        [TestCase(ParticipantStatusCodes.Cancelled, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.NoShow, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.NotAttended, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.OnHold, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.Enrolled, Result = ParticipantQualificationStatus.PartiallyQualified)]
        [TestCase(ParticipantStatusCodes.InProgress, Result = ParticipantQualificationStatus.PartiallyQualified)]
        [TestCase(ParticipantStatusCodes.Completed, Result = ParticipantQualificationStatus.Qualified)]
        [TestCase(ParticipantStatusCodes.Dispensation, Result = ParticipantQualificationStatus.Qualified)]
        public string GetParticipantsWithQualificationStatus_GivenActivityWithSpecificEnrollmeltCondition_ShouldReturnCorrectQualificationStatus(string participantStatusCode)
        {
            var examActivity = CreateMultipleChoiceExamActivity();
            var elearningActivity = CreateElearningActivity();
            var classroomActivity = CreateClassroomActivity();

            var condition = CreateClassroomEnrollmentConditionOnActivity(examActivity);
            condition.Customer = _mockCustomer;
            condition.CustomerId = _mockCustomer.Id;
            condition.Language = _norwegianLanguage;
            condition.LanguageId = _norwegianLanguage.Id;

            var user1 = new User { Id = 1 };
            var participant1U1 = new Participant
            {
                Id = 1,
                ActivityId = examActivity.Id,
                UserId = user1.Id,
                EnrollmentId = 1,
                IsDeleted = false
            };
            var participant2U1 = new Participant
            {
                Id = 2,
                ActivityId = elearningActivity.Id,
                UserId = user1.Id,
                IsDeleted = false,
                StatusCodeValue = ParticipantStatusCodes.Completed
            };
            var participant3U1 = new Participant
            {
                Id = 3,
                ActivityId = classroomActivity.Id,
                UserId = user1.Id,
                IsDeleted = false,
                StatusCodeValue = participantStatusCode
            };

            _contextMock.Object.Users.Add(user1);
            _contextMock.Object.Participants.Add(participant1U1);
            _contextMock.Object.Participants.Add(participant2U1);
            _contextMock.Object.Participants.Add(participant3U1);

            //Avoiding 'Activity.IncludeParticipants' as this calls context.Entry which is a method that must be mocked and makes the test setup more complex.
            _contextMock.Object.Participants.Where(p => p.ActivityId == examActivity.Id)
                .WithQualificationStatus(examActivity, _contextMock.Object);

            var returnedParticipant = _contextMock.Object.Participants.First(p => p.Id == participant1U1.Id);
            return returnedParticipant.QualificationStatus;
        }

        [Test]
        public void GetParticipantsWithQualificationStatus_GivenActivityWithMultipleParticipants_ShouldReturnCorrectQualificationStatus()
        {
            var examActivity = CreateMultipleChoiceExamActivity();
            var elearningActivity = CreateElearningActivity();

            var user1 = new User { Id = 1 };
            var participant1U1 = new Participant
            {
                Id = 1,
                ActivityId = examActivity.Id,
                UserId = user1.Id,
                EnrollmentId = 1,
                IsDeleted = false
            };
            var participant2U1 = new Participant
            {
                Id = 2,
                ActivityId = elearningActivity.Id,
                UserId = user1.Id,
                IsDeleted = false,
                StatusCodeValue = ParticipantStatusCodes.Enrolled
            };

            var user2 = new User { Id = 2 };
            var participant1U2 = new Participant
            {
                Id = 3,
                ActivityId = examActivity.Id,
                UserId = user2.Id,
                EnrollmentId = 2,
                IsDeleted = false
            };
            var participant2U2 = new Participant
            {
                Id = 4,
                ActivityId = elearningActivity.Id,
                UserId = user2.Id,
                IsDeleted = false,
                StatusCodeValue = ParticipantStatusCodes.Cancelled
            };

            _contextMock.Object.Users.Add(user1);
            _contextMock.Object.Users.Add(user2);

            _contextMock.Object.Participants.Add(participant1U1);
            _contextMock.Object.Participants.Add(participant2U1);
            _contextMock.Object.Participants.Add(participant1U2);
            _contextMock.Object.Participants.Add(participant2U2);

            //Avoiding 'Activity.IncludeParticipants' as this calls context.Entry which is a method that must be mocked and makes the test setup more complex.
            _contextMock.Object.Participants.Where(p => p.ActivityId == examActivity.Id)
                .WithQualificationStatus(examActivity, _contextMock.Object);

            var participantU1 = _contextMock.Object.Participants.First(p => p.Id == participant1U1.Id);
            var participantU2 = _contextMock.Object.Participants.First(p => p.Id == participant1U2.Id);

            Assert.AreEqual( ParticipantQualificationStatus.PartiallyQualified, participantU1.QualificationStatus);
            Assert.AreEqual( ParticipantQualificationStatus.NotQualified, participantU2.QualificationStatus);
        }

        [Test]
        [TestCase(ParticipantStatusCodes.Cancelled, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.NoShow, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.NotAttended, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.OnHold, Result = ParticipantQualificationStatus.NotQualified)]
        [TestCase(ParticipantStatusCodes.Enrolled, Result = ParticipantQualificationStatus.PartiallyQualified)]
        [TestCase(ParticipantStatusCodes.InProgress, Result = ParticipantQualificationStatus.PartiallyQualified)]
        [TestCase(ParticipantStatusCodes.Completed, Result = ParticipantQualificationStatus.Qualified)]
        [TestCase(ParticipantStatusCodes.Dispensation, Result = ParticipantQualificationStatus.Qualified)]
        public string GetParticipantsWithQualificationStatus_GivenActivityWithMultipleEnrollmentConditions_ShouldReturnCorrectQualificationStatus(string participantStatusCode)
        {
            var user = new User { Id = 1 };
            var examActivity = CreateMultipleChoiceExamActivity();
            var elearningActivity = CreateElearningActivity();
            var classroomActivity = CreateClassroomActivity();
            CreateClassroomEnrollmentConditionOnActivity(examActivity);

            var participant1 = new Participant
            {
                Id = 1,
                ActivityId = examActivity.Id,
                UserId = user.Id,
                EnrollmentId = 1,
                IsDeleted = false
            };
            var participant2 = new Participant
            {
                Id = 2,
                ActivityId = elearningActivity.Id,
                UserId = user.Id,
                IsDeleted = false,
                StatusCodeValue = participantStatusCode
            };

            var participant3 = new Participant
            {
                Id = 3,
                ActivityId = classroomActivity.Id,
                UserId = user.Id,
                IsDeleted = false,
                StatusCodeValue = ParticipantStatusCodes.Completed
            };

            _contextMock.Object.Users.Add(user);
            _contextMock.Object.Participants.Add(participant1);
            _contextMock.Object.Participants.Add(participant2);
            _contextMock.Object.Participants.Add(participant3);

            //Avoiding 'Activity.IncludeParticipants' as this calls context.Entry which is a method that must be mocked and makes the test setup more complex.
            _contextMock.Object.Participants.Where(p => p.ActivityId == examActivity.Id)
                .WithQualificationStatus(examActivity, _contextMock.Object);

            var participant = _contextMock.Object.Participants.First(p => p.Id == participant1.Id);
            return participant.QualificationStatus;
        }

        #endregion

        #region Supporting methods

        private EnrollmentCondition CreateClassroomEnrollmentConditionOnActivity(Activity activity, bool isStrict = false)
        {
            var enrollmentCondition = new EnrollmentCondition
            {
                Id = 2,
                ActivityId = activity.Id,
                ProductId = _projectManagementProduct.Id,
                Product = _projectManagementProduct,
                ArticleTypeId = _classroomArticleType.Id,
                ArticleType = _classroomArticleType,
                IsStrictRequirement = isStrict
            };

            activity.EnrollmentConditions.Add(enrollmentCondition);
            _contextMock.Object.EnrollmentConditions.Add(enrollmentCondition);

            return enrollmentCondition;
        }

        private CustomerArticle CreateElearningCustomerArticle()
        {
            var article = new ElearningArticle
            {
                Id = 1,
                Product = _projectManagementProduct,
                ProductId = _projectManagementProduct.Id,
                ArticleType = _eLearningArticleType,
                ArticleTypeId = _eLearningArticleType.Id,
                Language = _norwegianLanguage,
                LanguageId = _norwegianLanguage.Id
            };

            var customerArticle = new CustomerArticle
            {
                Id = GetNextId(),
                ArticleCopy = article,
                ArticleCopyId = article.Id
            };

            _contextMock.Object.Articles.Add(article);
            _contextMock.Object.CustomerArticles.Add(customerArticle);
            return customerArticle;
        }

        private CustomerArticle CreateClassroomCustomerArticle()
        {
            var article = new ClassroomArticle
            {
                Id = 2,
                Product = _projectManagementProduct,
                ProductId = _projectManagementProduct.Id,
                ArticleType = _classroomArticleType,
                ArticleTypeId = _classroomArticleType.Id,
                Language = _norwegianLanguage,
                LanguageId = _norwegianLanguage.Id,
                IsDeleted = false
            };

            var customerArticle = new CustomerArticle
            {
                Id = GetNextId(),
                CustomerId = _mockCustomer.Id,
                ArticleCopy = article,
                ArticleCopyId = article.Id,
                IsDeleted = false
            };

            _contextMock.Object.Articles.Add(article);
            _contextMock.Object.CustomerArticles.Add(customerArticle);
            return customerArticle;
        }

        private Activity CreateElearningActivity()
        {
            var customerArticle = CreateElearningCustomerArticle();
            var activity = new Activity
            {
                Id = GetNextId(),
                CustomerArticleId = customerArticle.Id,
                CustomerArticle = customerArticle
            };
            _contextMock.Object.Activities.Add(activity);
            return activity;
        }

        private Activity CreateClassroomActivity()
        {
            var customerArticle = CreateClassroomCustomerArticle();
            var activity = new Activity
            {
                Id = GetNextId(),
                CustomerArticleId = customerArticle.Id,
                CustomerArticle = customerArticle
            };
            _contextMock.Object.Activities.Add(activity);
            return activity;
        }

        private CustomerArticle CreateMultipleChoiceExamCustomerArticle()
        {
            var productCategory = new ProductCategory { Id = 4, Name = "Exam", ProductCodePrefix = "EX" };
            var product = new Product
            {
                Id = 87,
                ProductCategory = productCategory,
                ProductCategoryId = productCategory.Id,
                ProductNumber = "01"
            };
            var articleType = new ArticleType { Id = 3, Name = "Multiple Choice", CharCode = "MC" };

            var article = new ExamArticle
            {
                Id = 1110,
                Product = product,
                ProductId = product.Id,
                ArticleType = articleType,
                ArticleTypeId = articleType.Id,
                Language = _norwegianLanguage,
                LanguageId = _norwegianLanguage.Id,
                IsDeleted = false
            };

            var customerArticle = new CustomerArticle
            {
                Id = GetNextId(),
                ArticleCopy = article,
                ArticleCopyId = article.Id,
                IsDeleted = false
            };

            _contextMock.Object.CustomerArticles.Add(customerArticle);
            return customerArticle;
        }

        private Activity CreateMultipleChoiceExamActivity(bool includeEnrollmentConditions = true, bool isStrict = false)
        {
            var customerArticle = CreateMultipleChoiceExamCustomerArticle();
            var activity = new Activity
            {
                Id = GetNextId(),
                CustomerArticleId = customerArticle.Id,
                CustomerArticle = customerArticle
            };

            if (includeEnrollmentConditions)
            {
                var conditions = new List<EnrollmentCondition>
                {
                    new EnrollmentCondition
                    {
                        Id = 1,
                        ActivityId = activity.Id,
                        ProductId = _projectManagementProduct.Id,
                        Product = _projectManagementProduct,
                        ArticleTypeId = _eLearningArticleType.Id,
                        ArticleType = _eLearningArticleType,
                        IsStrictRequirement = isStrict
                    }
                };

                activity.EnrollmentConditions = conditions;
                _contextMock.Object.EnrollmentConditions.Add(conditions.First());
            }

            _contextMock.Object.Activities.Add(activity);

            return activity;
        }

        private int GetNextId()
        {
            return ++_ids;
        }
    }

    #endregion

}
