﻿module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            development: {
                options: {
                    paths: ['app/styles', 'app/styles.content']
                },
                files: {
                    'app/styles/app-compiled.css': 'app/styles/app.less',
                    'app/styles/app-compiled-responsive-override.css': 'app/styles/app-responsive-override.less',
                    'app/styles.content/app-content-compiled.css': 'app/styles.content/app-content.less'
                }
            }
        },
        cssmin: {
            minify: {
                expand: true,
                cwd: 'app/styles',
                src: ['app-compiled.css', 'app-compiled-responsive-override.css'],
                dest: 'app/styles',
                ext: '.min.css'
            }
        },
        jshint: {
            options: {
                jshintrc: true
            },
            all: ['app/phoenix/**/*.js', 'app/phoenix*/**/*.js']
        },
        watch: {
            options: {
                livereload: true  
            },
            htmlfiles: {
                files: ['app/phoenix/**/*.html', 'app/phoenix*/**/*.html']
            },
            styles: {
                files: ['app/styles/**/*.less', 'app/phoenix/**/*.less', 'app/phoenix*/**/*.less'],
                tasks: ['less', 'cssmin']
            },
            scripts: {
                files: ['.jshintrc', 'app/phoenix/**/*.js', 'app/phoenix*/**/*.js'],
                tasks: ['jshint']
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'app/assets/img_non_optimized',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'app/assets/img/'
                }]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('server', ['connect:livereload', 'watch']);

    grunt.registerTask('default', ['less', 'cssmin']);
};