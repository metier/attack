﻿using System.Configuration;

namespace Metier.Phoenix.Web.Admin.Configuration
{
    public static class PhoenixConfiguration
    {
        public static string GetPhoenixApiUrl()
        {
            var url = ConfigurationManager.AppSettings["PhoenixApiUrl"];
            return url.EndsWith("/") ? url : url + "/";
        }
    }
}