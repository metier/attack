﻿describe('Product list controller', function () {
    'use strict';

    beforeEach(module('phoenix.products'));

    // Inject dependencies
    beforeEach(inject(function ($rootScope, $controller) {
        this.scope = $rootScope.$new();
        this.controller = $controller;
    }));

    it('Should set resolved products on scope', function () {
        // Arrange
        this.products = {};
        
        // Act: Resolve controller
        this.controller('ProductListCtrl', { $scope: this.scope, products: this.products, productTypes: [], productCategories: [] });
        
        // Assert
        expect(this.scope.products).toBe(this.products);
    });
});