﻿describe('Product descriptions controller', function () {
    'use strict';

    beforeEach(module('phoenix.products'));

    // Inject dependencies
    beforeEach(inject(function ($rootScope, $controller) {
        this.scope = $rootScope.$new();
        this.controller = $controller('ProductDescriptionsCtrl', { $scope: this.scope, product: {}, productDescriptionsSimple: [] });;
    }));

    describe('getCustomerList', function () {

        it('Should remove duplicate customers', function () {
            // Arrange
            var productDescriptions = [{ CustomerName: 'generic', CustomerId: null }, { CustomerName: 'duplicate', CustomerId: 1 }, { CustomerName: 'duplicate', CustomerId: 1 }];

            // Act
            var result = this.controller.getCustomerList(productDescriptions);
            
            // Assert
            expect(result.length).toBe(2);
        });
        
        it('Should add generic if not in array', function () {
            // Arrange
            var productDescriptions = [];

            // Act
            var result = this.controller.getCustomerList(productDescriptions);

            // Assert
            expect(result.length).toBe(1);
            expect(result[0].customerId).toEqual(null);
            expect(result[0].customerName).toEqual(null);
        });
        
        it('Should add generic to top of array', function () {
            // Arrange
            var productDescriptions = [{ CustomerName: 'something', CustomerId: 1 }];

            // Act
            var result = this.controller.getCustomerList(productDescriptions);

            // Assert
            expect(result.length).toBe(2);
            expect(result[0].customerId).toEqual(null);
            expect(result[0].customerName).toEqual(null);
            expect(result[1].customerId).toEqual(1);
            expect(result[1].customerName).toEqual('something');
        });
        

        it('Should move generic to top of array', function () {
            // Arrange
            var productDescriptions = [{ CustomerName: 'something', CustomerId: 1 }, { CustomerName: null, CustomerId: null }];

            // Act
            var result = this.controller.getCustomerList(productDescriptions);

            // Assert
            expect(result.length).toBe(2);
            expect(result[0].customerId).toEqual(null);
            expect(result[0].customerName).toEqual(null);
            expect(result[1].customerId).toEqual(1);
            expect(result[1].customerName).toEqual('something');
        });
        
        it('Should sort customers by name', function () {
            // Arrange
            var productDescriptions = [{ CustomerName: 'b', CustomerId: 1 }, { CustomerName: 'a', CustomerId: 2 }, { CustomerName: null, CustomerId: null }];

            // Act
            var result = this.controller.getCustomerList(productDescriptions);

            // Assert
            expect(result.length).toBe(3);
            expect(result[0].customerId).toEqual(null);
            expect(result[0].customerName).toEqual(null);
            expect(result[1].customerId).toEqual(2);
            expect(result[1].customerName).toEqual('a');
            expect(result[2].customerId).toEqual(1);
            expect(result[2].customerName).toEqual('b');
        });
    });
    
});