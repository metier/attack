﻿describe('products service', function () {
    'use strict';
    var $httpBackend, productsService;

    // Bootstrap Phoenix App in Mock context
    beforeEach(module('phoenix.products'));

    // Get dependencies
    beforeEach(inject(function (_ProductsService_, _$httpBackend_) {
        $httpBackend = _$httpBackend_;
        productsService = _ProductsService_;
    }));

    it('getAll should return whatever is returned from API', function () {
        var fromApi = [], result;
        
        // Arrange
        $httpBackend.when('GET', 'apipath/products').respond(fromApi);

        // Act
        productsService.getAll().then(function (res) {
            result = res;
        });
        $httpBackend.flush();

        // Assert
        expect(result).toBe(fromApi);
    });

});