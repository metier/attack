﻿describe('Product descriptions directive controller', function () {
    'use strict';
    var $rootScope, $scope, $q, $controller, self = this;

    beforeEach(module('phoenix.products'));
    
    beforeEach(inject(function (_$rootScope_, _$controller_, _$q_) {
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $controller = _$controller_;
        $q = _$q_;
        
        self.ProductDescriptionsService = {
            get: function () { },
            getProductDescriptions: function () { }
        };
    }));
    
    beforeEach(function () {
        self.productDescriptionsResult = [{
            title: 'Result from service'
        }];
        
        var getProductDescriptonsDeferred = $q.defer();
        getProductDescriptonsDeferred.resolve(self.productDescriptionsResult);

        spyOn(self.ProductDescriptionsService, 'getProductDescriptions').andReturn(getProductDescriptonsDeferred.promise);
    });

});