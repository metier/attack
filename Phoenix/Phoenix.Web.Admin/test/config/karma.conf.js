module.exports = function (config) {
    config.set({
        basePath: '../../',
        files: [
            'app/bower_components/angular/angular.js',
            'test/config/phoenix.conf.js',
            'test/bower_components/angular-mocks/angular-mocks.js',

            "app/bower_components/jquery/jquery.js",
            "app/bower_components/jquery-ui/ui/jquery.ui.core.js",
            "app/bower_components/jquery-ui/ui/jquery.ui.widget.js",
            "app/bower_components/jquery-ui/ui/jquery.ui.mouse.js",
            "app/bower_components/jquery-ui/ui/jquery.ui.sortable.js",
            "app/bower_components/angular-sanitize/angular-sanitize.js",
            "app/bower_components/angular-cookies/angular-cookies.js",
            "app/bower_components/angular-ui-router/release/angular-ui-router.js",
            "app/bower_components/toastr/toastr.js",
            "app/bower_components/bootstrap/dist/js/bootstrap.js",
            "app/bower_components/underscore/underscore.js",
            "app/bower_components/momentjs/moment.js",
            "app/bower_components/angular-ui-utils/modules/event/event.js",
            "app/bower_components/angular-ui-utils/modules/format/format.js",
            "app/bower_components/angular-ui-utils/modules/highlight/highlight.js",
            "app/bower_components/angular-ui-utils/modules/ie-shiv/ie-shiv.js",
            "app/bower_components/angular-ui-utils/modules/indeterminate/indeterminate.js",
            "app/bower_components/angular-ui-utils/modules/inflector/inflector.js",
            "app/bower_components/angular-ui-utils/modules/jq/jq.js",
            "app/bower_components/angular-ui-utils/modules/keypress/keypress.js",
            "app/bower_components/angular-ui-utils/modules/mask/mask.js",
            "app/bower_components/angular-ui-utils/modules/reset/reset.js",
            "app/bower_components/angular-ui-utils/modules/route/route.js",
            "app/bower_components/angular-ui-utils/modules/scrollfix/scrollfix.js",
            "app/bower_components/angular-ui-utils/modules/showhide/showhide.js",
            "app/bower_components/angular-ui-utils/modules/unique/unique.js",
            "app/bower_components/angular-ui-utils/modules/validate/validate.js",
            "app/bower_components/angular-ui-utils/modules/utils.js",
            "app/bower_components/angular-ui-sortable/src/sortable.js",
            "app/bower_components/ngUpload/ng-upload.js",
            
            
            "app/bower_components/textAngular/textAngular.js",
            
            'app/lib/**/*.js',
            
            'app/phoenix.shared/phoenix.shared.js',
            'app/phoenix.shared/**/*.js',

            'app/phoenix.products/phoenix.products.js',
            'app/phoenix.products/**/*.js',

            'app/phoenix.orders/phoenix.orders.js',
            'app/phoenix.orders/**/*.js',

            'app/phoenix.customers/phoenix.customers.js',
            'app/phoenix.customers/**/*.js',

            'app/phoenix/phoenix.js',
            'app/phoenix/**/*.js',
            'test/unit/**/*.js'
        ],
        exclude: [
            
        ],
        autoWatch: true,
        frameworks: [
            'jasmine'
        ],
        browsers: [
            'IE'
        ],
        plugins: [
            'karma-junit-reporter',
            'karma-chrome-launcher',
            'karma-ie-launcher',
            'karma-jasmine'
        ],
        junitReporter: {
            outputFile: 'test_out/unit.xml',
            suite: 'unit'
        }
    });
};
