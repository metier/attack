﻿using System.Web.Mvc;

namespace Metier.Phoenix.Web.Admin.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
