﻿angular.module('phoenix.users').filter('lessonStatus', ['lessonStatuses', function (lessonStatuses) {
    'use strict';
    return function (lessonStatus) {
        return _(lessonStatuses).find(function (ct) {
            return ct.value === lessonStatus;
        }).text;
    };
}]);