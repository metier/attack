/*global angular*/
angular.module('phoenix.users', ['phoenix.config', 'phoenix.shared', 'phoenix.customers', 'ui.utils', 'ui.bootstrap.datepicker', 'ui.router'])
    .config(['$stateProvider', 'phoenix.config', function ($stateProvider, config) {
        'use strict';
        $stateProvider.state('userlist', {
            url: '/users?query&page&limit&orderBy&orderDirection',
            controller: 'UserListCtrl',
            templateUrl: config.basePath + 'app/phoenix.users/views/user-list.html',
            resolve: {
                users: ['UsersService', 'DistributorsService', '$stateParams', function (userService, distributorsService, $stateParams) {
                    var options = {
                        distributorId: distributorsService.get().Id,
                        query: $stateParams.query,
                        page: parseInt($stateParams.page, 10) || 1,
                        limit: parseInt($stateParams.limit, 10) || 15,
                        orderBy: $stateParams.orderBy,
                        orderDirection: $stateParams.orderDirection
                    };
                    return userService.search(options);
                }]
            }
        });

        $stateProvider.state('usercreate', {
            url: '/users/create',
            abstract: true,
            controller: 'UserCreateCtrl',
            templateUrl: config.basePath + 'app/phoenix.users/views/user-create.html',
            resolve: {
                userAccount: function () {
                    return {
                        User: {
                            IsAllowUserEcts: true,
                            UserNotificationType: 0
                        },
                        Roles: ['LearningPortalUser']
                    };
                },
                languages: ['LanguagesService', function (languagesService) {
                    return languagesService.getAll();
                }],
                codes: ['CodesService', function (codesService) {
                    return  codesService.getAll();
                }],
                predefinedLeadingTexts: ['LeadingTextsService', function (leadingTextService) {
                    return leadingTextService.getAllPredefined();
                }],
                participants: function () { return null; },
                loggedInUser: function () { return null; },
                competence: function () { return null; }
            }
        }).state('usercreate.edit', {
            controller: 'UserEditCtrl',
            templateUrl: config.basePath + 'app/phoenix.users/views/edit/user-edit.html',
            url: '',
            resolve: {
                diplomas: ['UsersService', '$stateParams', function (usersService, $stateParams) {
                    return $stateParams && $stateParams.userId ? usersService.getDiplomas($stateParams.userId) : [];
                }]
            }
        }).state('usercreate.edit.section', {
            url: '/?section&customerId'
        });

        $stateProvider.state('user', {
            url: '/users/:userId',
            controller: 'UserCtrl',
            templateUrl: config.basePath + 'app/phoenix.users/views/user.html',
            resolve: {
                userAccount: ['UsersService', 'CustomersService', '$stateParams', function (usersService, customersService, $stateParams) {
                    return usersService.getUserAccountByUserId($stateParams.userId).then(function(userAccount) {
                        return customersService.getLeadingTexts(userAccount.CustomerId).then(function (customLeadingTexts) {
                            userAccount.User.Customer.CustomLeadingTexts = customLeadingTexts;
                            return userAccount;
                        });
                    });
                }],
                codes: ['CodesService', function (codesService) {
                    return codesService.getAll();
                }],
                participants: ['ParticipantsService', '$stateParams', function (participantsService, $stateParams) {
                    return participantsService.search({ userId: $stateParams.userId, orderBy: 'participantId', orderDirection: 'desc', limit: -1 });
                }],
                competence: ['UsersService', '$stateParams', function (usersService, $stateParams) {
                    return usersService.getCompetence($stateParams.userId);
                }],
                diplomas: ['UsersService', '$stateParams', function (usersService, $stateParams) {
                        return usersService.getDiplomas($stateParams.userId);
                    }]
            }
        }).state('user.overview', {
            url: '/',
            template: '<div ui-view></div>',
            abstract: true
        }).state('user.overview.details', {
            url: '/?section',
            templateUrl: config.basePath + 'app/phoenix.users/views/view/user-view.html',
            controller: 'UserViewCtrl',
            resolve: {
                diplomas: ['UsersService', '$stateParams', function (usersService, $stateParams) {
                    return usersService.getDiplomas($stateParams.userId);
                }]
            }
        }).state('user.overview.edit', {
            url: 'edit',
            controller: 'UserEditCtrl',
            templateUrl: config.basePath + 'app/phoenix.users/views/edit/user-edit.html',
            resolve: {
                userAccount: ['UsersService', '$stateParams', function (usersService, $stateParams) {
                    return usersService.getUserAccountByUserId($stateParams.userId);
                }],
                languages: ['LanguagesService', function (languagesService) {
                    return languagesService.getAll();
                }],
                predefinedLeadingTexts: ['LeadingTextsService', function (leadingTextService) {
                    return leadingTextService.getAllPredefined();
                }],
                loggedInUser: ['AuthenticationService', function (authenticationService) {
                    return authenticationService.getLoggedInUser();
                }],
                diplomas: ['UsersService', '$stateParams', function (usersService, $stateParams) {
                    return usersService.getDiplomas($stateParams.userId);
                }]
            }
        }).state('user.overview.edit.section', {
            url: '/?section'
        });
    }]);