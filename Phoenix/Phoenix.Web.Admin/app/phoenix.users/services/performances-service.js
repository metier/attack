﻿angular.module('phoenix.users').factory('PerformancesService', [
    '$http',
    '$q',
    'phoenix.config',
    'asyncHelper',
    function ($http, $q, config, asyncHelper) {
        'use strict';
        return {
            get: function(userId, rcoId) {
                var url = config.apiPath + 'performances/' + userId + '/' + rcoId;
                return asyncHelper.resolveHttp($http.get(url));
            },
            register: function(userId, rcoId, request) {
                var url = config.apiPath + 'performances/register/' + userId + '/' + rcoId;
                return asyncHelper.resolveHttp($http.put(url, request));
            }
        };
    }
]);