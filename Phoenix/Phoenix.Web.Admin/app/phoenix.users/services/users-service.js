﻿/*global angular */
angular.module('phoenix.users').factory('UsersService', [
    '$http',
    '$q',
    'phoenix.config',
    'asyncHelper',
    function ($http, $q, config, asyncHelper) {
        'use strict';

        return {
            createUser: function(userAccount, autoGeneratePassword) {
                var url = config.apiPath + 'accounts',
                    deferred = $q.defer(),
                    parameters = {
                        autoGeneratePassword: autoGeneratePassword,
                        source: 'LearningPortal'
                    };

                $http.post(url, userAccount, { params: parameters }).success(function(result) {
                    deferred.resolve(result);
                }).error(function(error) {
                    deferred.reject(error);
                });

                return deferred.promise;
            },
            saveUser: function(userAccount) {
                var url = config.apiPath + 'accounts',
                    deferred = $q.defer();

                $http.put(url, userAccount).success(function(result) {
                    deferred.resolve(result);
                }).error(function(error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            },
            mergeUsers: function(primaryUserId, secondaryUserId) {
                var url = config.apiPath + 'accounts/' + primaryUserId + '/merge/' + secondaryUserId,
                    deferred = $q.defer();

                $http.put(url).success(function (result) {
                    deferred.resolve(result);
                }).error(function(error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            },
            getUserAccountByUserId: function(userId) {
                var url = config.apiPath + 'accounts?userId=' + userId;
                return asyncHelper.resolveHttp($http.get(url), true);
            },
            getCompetence: function(userId) {
                var url = config.apiPath + 'users/' + userId + '/competence';
                return asyncHelper.resolveHttp($http.get(url), false);
            },
            getDiplomas: function (userId) {
                var url = config.apiPath + 'users/' + userId + '/diplomas';
                return asyncHelper.resolveHttp($http.get(url), false);
            },

            saveDiploma: function (userId, diploma) {
                var url = config.apiPath + 'users/' + userId + '/diplomas' ,
                    deferred = $q.defer();

                $http.post(url, diploma).success(function (result) {
                    deferred.resolve(result);
                }).error(function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            },

            editDiploma: function (userId, diploma) {
                var url = config.apiPath + 'users/' + userId + '/diplomas',
                    deferred = $q.defer();

                $http.put(url, diploma).success(function (result) {
                    deferred.resolve(result);
                }).error(function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            },

            deleteDiploma: function (userId, diplomaId) {
                var url = config.apiPath + 'users/' + userId + '/diplomas/' + diplomaId,
                    deferred = $q.defer();

                $http.delete(url).success(function (result) {
                    deferred.resolve(result);
                }).error(function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            },

            getUserAccountByEmail: function (email, interceptErrors) {
                var url = config.apiPath + 'accounts?email=' + email;
                return asyncHelper.resolveHttp($http.get(url, { interceptErrors: interceptErrors }), false);
            },
            getUserAccountByUsername: function(username) {
                var url = config.apiPath + 'accounts?username=' + username;
                return asyncHelper.resolveHttp($http.get(url), false);
            },
            getPerformances: function (userId, options) {
                var url = config.apiPath + 'users/' + userId + '/performances',
                    parameters = {
                        activityId: options.activityId,
                        articleId: options.articleId
                    };
                return asyncHelper.resolveHttp($http.get(url, { params: parameters }));
            },
            changePassword: function(userId, newPassword) {
                var url = config.apiPath + 'accounts/' + userId + '/changepassword',
                    payload = {
                        NewPassword: newPassword
                    };
                return asyncHelper.resolveHttp($http.post(url, payload));
            },
            requestPasswordReset: function (email) {
                var url = config.apiPath + 'accounts/requestpasswordreset?email=' + email + '&source=LearningPortal';
                return asyncHelper.resolveHttp($http.put(url));
            },
            resetPassword: function(token, email, newPassword) {
                var url = config.apiPath + 'accounts/resetpassword?email=' + email + '&token=' + token + '&newPassword=' + newPassword;
                return asyncHelper.resolveHttp($http.put(url));
            },
            search: function (options) {
                var parameters = {
                    distributorId: options.distributorId,
                    customerId: options.customerId,
                    activityId: options.activityId,
                    activitySetId: options.activitySetId,
                    phoenixUsersOnly: options.phoenixUsersOnly,
                    query: options.query,
                    limit: options.limit,
                    orderBy: options.orderBy,
                    orderDirection: options.orderDirection
                };

                if (typeof options.page === 'number' && options.page > 0) {
                    parameters.skip = (options.page - 1) * parameters.limit;
                }
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'users', { params: parameters }));
            }
        };
    }
]);