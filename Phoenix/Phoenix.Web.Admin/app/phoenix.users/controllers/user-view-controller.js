angular.module('phoenix.users').controller('UserViewCtrl', [
    '$scope',
    'codes',
    'phoenix.config',
    'competence',
    'diplomas',
    'CommentsService',
    function ($scope, codes, config, competence,  diplomas, commentsService) {
        'use strict';
        var self = this;
        $scope.competence = competence;
        $scope.diplomas = diplomas;

        $scope.templates = [
            {
                name: 'Basic Info',
                url: config.basePath + 'app/phoenix.users/views/view/user-view-info.html',
                queryParam: 'basic'
            },
            {
                name: 'Advanced Info',
                url: config.basePath + 'app/phoenix.users/views/view/user-view-advanced.html',
                queryParam: 'advanced'
            },
            {
                name: 'Enrollments',
                url: config.basePath + 'app/phoenix.users/views/user-participants.html',
                queryParam: 'enrollments'
            },
            {
                name: 'Comments',
                url: config.basePath + 'app/phoenix.users/views/view/user-view-comments.html',
                queryParam: 'comments'
            }

        ];

        if ($scope.competence) {
            $scope.templates.push({
                name: 'Competence',
                url: config.basePath + 'app/phoenix.users/views/view/user-competence.html',
                queryParam: 'competence'
            });
        }

        $scope.templates.push({
            name: 'Transcripts',
            url: config.basePath + 'app/phoenix.users/views/view/user-diploma.html',
            queryParam: 'transcripts'
        });

        function initAdditionalInfo() {
            $scope.additionalInfos = [];
            _($scope.userAccount.User.Customer.CustomLeadingTexts).each(function (customLeadingText) {
                if (customLeadingText.IsVisible) {
                    var userInfoElement = _($scope.userAccount.User.UserInfoElements).find(function (userInfoElement) {
                        return userInfoElement.LeadingTextId === customLeadingText.LeadingTextId;
                    }) || {};
                    $scope.additionalInfos.push({
                        Label: customLeadingText.Text,
                        Text: userInfoElement.InfoText
                    });
                }
            });
        }

        self.initRoles = function () {
            $scope.hasRolePhoenixUser = _($scope.userAccount.Roles).contains('PhoenixUser');
            $scope.hasRoleLearningPortalUser = _($scope.userAccount.Roles).contains('LearningPortalUser');
            $scope.hasRoleLearningPortalSuperUser = _($scope.userAccount.Roles).contains('LearningPortalSuperUser');
            $scope.hasRoleBehindAdmin = _($scope.userAccount.Roles).contains('BehindAdmin');
            $scope.hasRoleBehindContentAdmin = _($scope.userAccount.Roles).contains('BehindContentAdmin');
            $scope.hasRoleBehindSuperAdmin = _($scope.userAccount.Roles).contains('BehindSuperAdmin');
            $scope.hasRoleExaminer = _($scope.userAccount.Roles).contains('Examiner');
        };

        $scope.getParticipantStatusDisplay = function (codeValue) {
            return _(codes.participantStatuses).findWhere({ Value: codeValue }).Display;
        };

        $scope.$on('$stateChangeSuccess', function (event, toState, toParams) {
            $scope.section = toParams.section || 'basic';
            $scope.isCustomerContext = toState.name.indexOf('customer') === 0;
            $scope.isUserContext = toState.name.indexOf('user') === 0;
        });

        $scope.getFileDownload = function (fileId) {
            return config.apiPath + 'files/' + fileId;
        };

        /*
        Logic for grouping ActivitySets.
        */

        self.getActivitySetsFromEnrollments = function () {
            var participants = $scope.participants.Items;
            var activitysets = [];

            _.each(participants, function(participant) {
                var activitySetIsNew = _.pluck(activitysets, 'Id').indexOf(participant.ActivitySetId) === -1;
                if (activitySetIsNew) {
                    var activitySet = {};
                    activitySet.Id = participant.ActivitySetId;
                    activitySet.Name = participant.ActivitySetName;
                    activitySet.CustomerId = participant.ActivityOwnerCustomerId;
                    
                    activitysets.push(activitySet);
                }

            });

            return activitysets;
        }


        $scope.participationsInActivitySets = function (activitySetId) {
            var participants = $scope.participants.Items;
            var filtered = _.filter(participants, function (participant) { return participant.ActivitySetId === activitySetId });

            return filtered;
        }

        initAdditionalInfo();
        self.initRoles();

        $scope.uniqueActivitysets = self.getActivitySetsFromEnrollments();

    }
]);