﻿/*global angular */
/*jslint maxlen: 150 */
angular.module('phoenix.shared').directive('attachmentUploadDiploma', ['phoenix.config', function (config) {
    'use strict';
    return {
        restrict: 'EA',
        scope: {
            fileUploaded: '&',
            editTitle: '@',
            editDescription: '@',
            accept: '@'
        },
        controller: 'AttachmentUploadDirectiveCtrl',
        templateUrl: config.basePath + 'app/phoenix.shared/directives/attachment/attachment-upload.html',
        compile: function (element, attrs) {
            if (!attrs.editTitle) { attrs.editTitle = 'true'; }
            if (!attrs.editDescription) { attrs.editDescription = 'true'; }
            if (!attrs.accept) { attrs.accept = '*'; }
        }
    };
}]);