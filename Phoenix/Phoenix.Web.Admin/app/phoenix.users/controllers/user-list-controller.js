﻿angular.module('phoenix.users').controller('UserListCtrl', [
    '$scope',
    '$state',
    '$modal',
    '$location',
    'phoenix.config',
    'users',
    function ($scope, $state, $modal, $location, config, users) {
        'use strict';
        $scope.users = users || { TotalCount: 0, Items: [] };
        $scope.query = $location.search().query;
        $scope.orderBy = $location.search().orderBy || '';
        $scope.orderDirection = $location.search().orderDirection || '';
        $scope.page = $location.search().page;
        $scope.limit = $location.search().limit || 15;
        $scope.maxPages = $scope.users.TotalCount;

        $scope.onQueryChanged = function () {
            if ($scope.query === '') {
                $scope.search();
            }
        };

        $scope.search = function () {
            if (!$state.params.query && $scope.query === '') { return; }
            if ($scope.isCustomerContext) {
                $state.go('customer.users', { query: $scope.query, page: 1 });
            } else {
                $state.go('userlist', { query: $scope.query, page: 1 });
            }
        };

        $scope.navigate = function (page) {
            if (page < 1 || page > $scope.maxPages) {
                return;
            }
            $location.search('page', page);
        };

        $scope.bulkImport = function () {
            var customerId = $scope.isCustomerContext ? $state.params.customerId : null,
                modalInstance = $modal.open({
                    backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.users/views/modals/users-bulkimport-modal.html',
                controller: 'UsersBulkImportModalCtrl',
                resolve: {
                    customer: ['CustomersService', function(customersService) {
                        return customersService.get(customerId);
                    }],
                    leadingTexts: ['LeadingTextsService', function (leadingTextsService) {
                        return leadingTextsService.getAllPredefined();
                    }]
                },
                windowClass: 'modal-huge'
            });
            modalInstance.result.then(function () {
                $state.go($state.current, $state.params, { reload: true });
            });
        };

        $scope.bulkUpdate = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.users/views/modals/users-bulkedit-modal.html',
                controller: 'UsersBulkEditModalCtrl',
                resolve: {
                    leadingTexts: ['LeadingTextsService', function (leadingTextsService) {
                        return leadingTextsService.getAllPredefined();
                    }]
                },
                windowClass: 'modal-huge'
            });
            modalInstance.result.then(function () {
                $state.go($state.current, $state.params, { reload: true });
            });
        };

        $scope.$on('$stateChangeSuccess', function (event, toState) {
            $scope.isCustomerContext = toState.name.indexOf('customer') === 0;
            $scope.isUserContext = toState.name.indexOf('user') === 0;
        });
    }
]);