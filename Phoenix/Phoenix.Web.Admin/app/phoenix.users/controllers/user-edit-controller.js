/*global _*/

angular.module('phoenix.users').controller('UserEditCtrl', [
    '$scope',
    '$state',
    '$stateParams',
    '$location',
    '$q',
    'phoenix.config',
    'CustomersService',
    'UsersService',
    'NotificationService',
    'userAccount',
    'languages',
    'codes',
    'predefinedLeadingTexts',
    'DistributorsService',
    'participants',
    'loggedInUser',
    'competence',
    'diplomas',
    function ($scope, $state, $stateParams, $location, $q, config, customersService, usersService, notificationService, userAccount, languages, codes, predefinedLeadingTexts, distributorsService, participants, loggedInUser, competence, diplomas) {
        'use strict';
        var self = this;
       
        $scope.currentEditingUserId = !$stateParams ? undefined : $stateParams.userId;
        $scope.isBusySaving = false;
        $scope.isBusyChangingPassword = false;
        $scope.locals = {};
        $scope.model = {};
        $scope.competence = competence;
        $scope.diplomas = diplomas;
        $scope.filesUploadUrl = config.apiPath + "files";

        $scope.expiryDatePickerOpened = false;

        $scope.participants = participants;

        $scope.getParticipantStatusDisplay = function (codeValue) {
            return _(codes.participantStatuses).findWhere({ Value: codeValue }).Display;
        };

        $scope.templates = [
            {
                name: 'Basic Info',
                url: config.basePath + 'app/phoenix.users/views/edit/user-edit-info.html',
                queryParam: 'basic'
            },
            {
                name: 'Advanced Info',
                url: config.basePath + 'app/phoenix.users/views/edit/user-edit-advanced.html',
                queryParam: 'advanced'
            },
            {
                name: 'Enrollments',
                url: config.basePath + 'app/phoenix.users/views/user-participants.html',
                queryParam: 'enrollments'
            }
        ];

        if ($scope.competence) {
            $scope.templates.push({
                name: 'Competence',
                url: config.basePath + 'app/phoenix.users/views/view/user-competence.html',
                queryParam: 'competence'
            });
        }

        try {
            console.log($scope);
        } catch (e) {
            console.warn(e);
        }

        if ($scope.currentEditingUserId)
            $scope.templates.push({
                name: 'Transcripts',
                url: config.basePath + 'app/phoenix.users/views/view/user-diploma.html',
                queryParam: 'transcripts'
            });

        $scope.updateUsername = function () {
            if (!$scope.locals.Username) {
                $scope.locals.Username = $scope.userAccount.Email;
            }
        };

        $scope.$watch('locals.arbitraryCustomer', function () {
            if ($scope.locals.arbitraryCustomer) {
                self.removeCustomerFromUser();
                $scope.userAccount.DistributorId = distributorsService.get().Id;
            } else {
                $scope.userAccount.DistributorId = null;
                if ($scope.locals.customer) {
                    $scope.onCustomerSelected($scope.locals.customer);
                }
            }
        });


        self.getFileNameFromPath = function (path) {
            return path.replace(/^.*[\\\/]/, '');
        };


        $scope.onImageSelected = function (imageValue) {
            $scope.selectedImagePath = imageValue;
            self.originalFilename = self.getFileNameFromPath(imageValue);
        };

        $scope.onfileUpload = function (content, completed) {
            $scope.isBusySaving = true;
            if (completed && content) {
                if (!content.Title) {
                    content.Title = self.originalFilename;
                }

                var file = {
                    id: content.FileId,
                    url: config.apiPath + 'files/' + content.FileId,
                    title: content.Title
                };
                $scope.model.title = '';
                $scope.model.description = '';
                $scope.onFileUploaded( file );
            }
        };

        $scope.getFileDownload = function (fileId) {
            return config.apiPath + 'files/' + fileId;
        };

        $scope.editDiploma = function (diploma) {
            $scope.model = Object.assign({}, diploma);
        };

        $scope.cancelEditDiploma = function () {
            $scope.model = {};
        };

        $scope.saveEditDiploma = function () {
            var diploma = $scope.model;
            usersService.editDiploma($scope.userAccount.User.Id
                , diploma).then(function (e) {
                    $scope.isBusySaving = false;
                    $scope.model = {};
                    notificationService.alertSuccess({ message: 'Transcript was successfully modified.' });
                    if ($scope.diplomas)
                        $scope.diplomas = $scope.diplomas.map(obj => obj.Id === diploma.Id ? e : obj);
                }, function (error) {
                    $scope.isBusySaving = false;
                    notificationService.alertError({ message: error });
                });
        };
        
        $scope.deleteDiploma = function (diploma) {
            if (confirm(`Are you sure you want to remove '${diploma.Title}'?`)) {
                usersService.deleteDiploma($scope.userAccount.User.Id
                    , diploma.Id).then(function (e) {
                        $scope.isBusySaving = false;
                        notificationService.alertSuccess({ message: 'Diploma was successfully removed.' });
                        if ($scope.diplomas)
                            $scope.diplomas = $scope.diplomas.filter(item => !(item.Id === diploma.Id));
                    }, function (error) {
                        $scope.isBusySaving = false;
                        notificationService.alertError({ message: error });
                    });
            }
        };

        $scope.save = function () {
            var autogeneratePassword = false;
            if (!$scope.userAccount.User.Id) {
                $scope.userAccount.Username = $scope.locals.Username;
                $scope.isBusySaving = true;

                if (!$scope.locals.setPasswordManually) {
                    $scope.userAccount.password = null;
                    $scope.userAccount.passwordRepeat = null;
                    autogeneratePassword = true;
                }
                
                usersService.createUser($scope.userAccount, autogeneratePassword).then(function (result) {
                    $scope.isBusySaving = false;
                    if ($scope.isCustomerContext) {
                        $state.go('customer.user.overview.details', { customerId: result.CustomerId, userId: result.Id, section: $state.params.section });
                    } else {
                        $state.go('user.overview.details', { userId: result.Id, section: $state.params.section });
                    }
                    
                }, function () {
                    $scope.isBusySaving = false;
                });
            } else {
                if ($scope.userAccount.Username !== $scope.locals.Username) {
                    $scope.userAccount.NewUsername = $scope.locals.Username;
                }
                $scope.isBusySaving = true;
                usersService.saveUser($scope.userAccount).then(function (result) {
                    usersService.getDiplomas($scope.userAccount.User.Id).then(function (diplomas) {
                        $scope.diplomas = diplomas;
                        $scope.isBusySaving = false;
                        $scope.userAccount = result;
                        $scope.$emit('userSaved', result);
                        if ($scope.isCustomerContext) {
                            $state.go('customer.user.overview.details', { customerId: result.User.CustomerId, userId: result.User.Id, section: $state.params.section, diplomas: diplomas });
                        } else {
                            $state.go('user.overview.details', { userId: result.User.Id, section: $state.params.section, diplomas: diplomas });
                        }
                    });
                }, function () {
                    $scope.isBusySaving = false;
                });
            }
        };

        $scope.createAndAddNew = function () {
            var autogeneratePassword = false;

            $scope.isBusySaving = true;

            if (!$scope.locals.setPasswordManually) {
                $scope.userAccount.password = null;
                $scope.userAccount.passwordRepeat = null;
                autogeneratePassword = true;
            }
            $scope.userAccount.Username = $scope.locals.Username;
            usersService.createUser($scope.userAccount, autogeneratePassword).then(function () {
                $scope.isBusySaving = false;
                if ($scope.isCustomerContext) {
                    $state.go('customer.usercreate.edit', null, { reload: true });
                } else {
                    $state.go('usercreate.edit', null, { reload: true });
                }
            }, function () {
                $scope.isBusySaving = false;
            });
        };
        
        self.setTemplateFromQueryString = function () {
            var section = $location.search().section || 'basic',
            template = _($scope.templates).find(function (t) { return t.queryParam === section; });
            $scope.activeTemplate = template;
        };

        $scope.setActiveTemplate = function (template) {
            $scope.activeTemplate = template;
            $location.search('section', template.queryParam);
        };

        $scope.isActiveTemplate = function (template) {
            var section = $location.search().section || 'basic';
            return template.queryParam === section;
        };

        $scope.onFileUploaded = function (file) {
            usersService.saveDiploma($scope.userAccount.User.Id
                , {
                    UserId: $scope.userAccount.User.Id,
                    FileId : file.id,
                    Title: file.title
                }).then(function (e) {
                    $scope.isBusySaving = false; notificationService.alertSuccess({ message: 'Transcript was successfully uploaded.' });
                    if (!$scope.diplomas) {
                        $scope.diplomas = [];
                    }
                    $scope.diplomas.push(e);

                    $scope.model = {};

                   
                    document.forms[0].elements.file.value = "";


                }, function () {
                    $scope.isBusySaving = false;
                });
            $scope.model = {};
            //$scope.userAccount.User.AttachmentId = attachment.Id;
            //$scope.imageSource = config.filesPath + attachment.FileId;
        };

        $scope.deleteImage = function () {
            $scope.userAccount.User.Attachment = null;
            $scope.userAccount.User.AttachmentId = null;
            $scope.imageSource = self.placeholderImageSource;
        };

        $scope.changePassword = function () {
            if ($scope.userAccount.passwordRepeat && ($scope.userAccount.passwordRepeat === $scope.userAccount.password)) {
                $scope.isBusyChangingPassword = true;
                usersService.changePassword($scope.userAccount.Username, $scope.userAccount.password).then(function () {
                    $scope.isBusyChangingPassword = false;
                    notificationService.alertSuccess({ message: 'Password successfully changed!' });
                }, function () {
                    $scope.isBusyChangingPassword = false;
                });
            }
        };

        $scope.resetPassword = function() {
            $scope.isBusyResettingPassword = true;
            usersService.requestPasswordReset($scope.userAccount.Email).then(function() {
                $scope.isBusyResettingPassword = false;
                notificationService.alertSuccess({ message: 'Password reset notification sent to user' });
            }, function () {
                $scope.isBusyResettingPassword = false;
            });
        };

        $scope.getUserInfoElement = function (customLeadingText) {
            var infoElement = _($scope.userAccount.User.UserInfoElements).find(function (e) {
                return e.LeadingTextId === customLeadingText.Id;
            });
            if (!infoElement) {
                infoElement = {
                    UserId: $scope.userAccount.User.Id,
                    LeadingTextId: customLeadingText.Id
                };
                $scope.userAccount.User.UserInfoElements.push(infoElement);
            }
            return infoElement;
        };


        $scope.getCustomLeadingText = function (leadingText) {

            var customLeadingText;

            if(!$scope.userAccount.User.Customer){
                customLeadingText = {
                    LeadingText: leadingText,
                    LeadingTextId: leadingText.Id,
                    Text: leadingText.Text,
                    IsVisible: leadingText.IsVisible
                }
            }
            else{
                customLeadingText = _($scope.userAccount.User.Customer.CustomLeadingTexts).find(function (e) {
                    return e.LeadingTextId === leadingText.Id;
                });                
            }
           
            return customLeadingText;
        };


        self.setLeadingTexts = function () {
                // Copy elements to make sure UI events fires
                $scope.leadingTexts = angular.copy(predefinedLeadingTexts);
        };

        self.inheritFromCustomer = function () {
            if ($scope.userAccount.User.Customer) {
                if (!$scope.userAccount.User.PreferredLanguageId && $scope.userAccount.User.Customer.LanguageId) {
                    $scope.userAccount.User.PreferredLanguageId = $scope.userAccount.User.Customer.LanguageId;
                }
                if ($scope.userAccount.User.Customer.AutomailDefinition) {
                    if (!$scope.userAccount.User.UserNotificationType && $scope.userAccount.User.Customer.AutomailDefinition.UserNotificationType) {
                        $scope.userAccount.User.UserNotificationType = $scope.userAccount.User.Customer.AutomailDefinition.UserNotificationType;
                    }
                }

            }
        };

        $scope.searchCustomer = function (searchString) {
            return customersService.search({ query: searchString, limit: 50 }).then(function (result) {
                return result.Items;
            });
        };

        self.setCustomerOnUser = function (customerId) {
            customersService.get(customerId).then(function (result) {
                $scope.userAccount.CustomerId = customerId;
                $scope.userAccount.User.CustomerId = customerId;
                $scope.userAccount.User.Customer = result;
                if (!$scope.isEditMode) {
                    $scope.userAccount.User.UserInfoElements = [];
                }
                
                $scope.locals.customer = result;
                $scope.userSelectedCustomer = result;
                self.setLeadingTexts();  
                self.inheritFromCustomer();
            });
        };

        self.removeCustomerFromUser = function () {
            $scope.userAccount.CustomerId = null;
            $scope.userAccount.User.CustomerId = null;
            $scope.userAccount.User.Customer = null;
            if (!$scope.isEditMode) {
                $scope.userAccount.User.UserInfoElements = [];
            }
            $scope.userSelectedCustomer = null;
            $scope.locals.customer = null;
            self.setLeadingTexts();
            self.inheritFromCustomer();
        };

        $scope.onCustomerSelected = function (customer) {
            if ($scope.userAccount.User.CustomerId !== customer.Id) {
                self.setCustomerOnUser(customer.Id);
            }
        };
        
        self.configureTemplate = function () {
            if ($scope.userAccount.User.CustomerId) {
                self.setCustomerOnUser($scope.userAccount.User.CustomerId);
            }

            if ($scope.userAccount.User.Attachment) {
                $scope.imageSource = config.filesPath + $scope.userAccount.User.Attachment.FileId;
            }
        };

        self.initRoles = function () {
            $scope.hasRolePhoenixUser = _($scope.userAccount.Roles).contains('PhoenixUser');
            $scope.hasRoleLearningPortalUser = _($scope.userAccount.Roles).contains('LearningPortalUser');
            $scope.hasRoleLearningPortalSuperUser = _($scope.userAccount.Roles).contains('LearningPortalSuperUser');
            $scope.hasRoleBehindAdmin = _($scope.userAccount.Roles).contains('BehindAdmin');
            $scope.hasRoleBehindContentAdmin = _($scope.userAccount.Roles).contains('BehindContentAdmin');
            $scope.hasRoleBehindSuperAdmin = _($scope.userAccount.Roles).contains('BehindSuperAdmin');
            $scope.hasRoleExaminer = _($scope.userAccount.Roles).contains('Examiner');
        };

        $scope.roleChanged = function (role, isChecked) {
            if (isChecked) {
                $scope.userAccount.Roles.push(role);
            } else {
                var index = _($scope.userAccount.Roles).indexOf(role);
                if (index > -1) {
                    $scope.userAccount.Roles.splice(index, 1);
                }
            }
            
        };

        $scope.cancel = function () {
            if ($scope.userAccount.User.Id) {
                usersService.getDiplomas($scope.userAccount.User.Id).then(function (diplomas) {
                    $scope.diplomas = diplomas;
                    $state.go(self.closeState, self.closeParams);
                });
            } else 
                $state.go(self.closeState, self.closeParams);
        };

        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            $scope.isCustomerContext = toState.name.indexOf('customer') === 0;
            $scope.isUserContext = toState.name.indexOf('user') === 0;
            $scope.isCreateContext = toState.name.indexOf('usercreate') === 0 || toState.name.indexOf('customer.usercreate') === 0;
            // Navigate on "Close" to where you came from. Default to activity list
            if (!self.closeState) {
                if (!fromState.name ||
                    fromState.name.indexOf('usercreate') === 0 ||
                    fromState.name.indexOf('user.overview.edit') === 0 || 
                    fromState.name.indexOf('customer.usercreate') === 0 || 
                    fromState.name.indexOf('customer.user.overview.edit') === 0) {
                    if ($scope.isCustomerContext) {
                        self.closeState = $scope.isEditMode ? 'customer.user.overview.details' : 'customer.users';
                        self.closeParams = fromParams;
                    } else {
                        self.closeState = $scope.isEditMode ? 'user.overview.details' : 'userlist';
                        self.closeParams = fromParams;
                    }
                } else {
                    self.closeState = fromState;
                    self.closeParams = fromParams;
                }
            }
        });

        self.init = function () {
            $scope.userAccount = userAccount;
            $scope.userAccount.User.UserInfoElements = $scope.userAccount.User.UserInfoElements || [];
            if (!$scope.userAccount.User.Id && !$scope.userAccount.User.UserRegistrationNotificationDate) {
                $scope.userAccount.User.UserRegistrationNotificationDate = moment.utc().toDate();
            }
            if ($state.params.customerId) {
                $scope.userAccount.User.CustomerId = parseInt($state.params.customerId, 10);
            }

            if (loggedInUser) {
                $scope.isEditingLoggedInUser = loggedInUser.Username === userAccount.Username;
            }

            $scope.locals.Username = $scope.userAccount.Username;
            self.predefinedLeadingTexts = predefinedLeadingTexts;
            $scope.languages = languages;
            $scope.countries = codes.countryCodes;
            self.configureTemplate();
            self.initRoles();
            self.setTemplateFromQueryString();

            $scope.$watch('userAccount.User.CustomerId', function() {
                if ($scope.userAccount.User.CustomerId) {
                    self.setCustomerOnUser($scope.userAccount.User.CustomerId);
                }
            });

        };

        self.init();
    }
]);
