angular.module('phoenix.customers').controller('UsersBulkEditModalCtrl', [
    '$scope',
    '$modalInstance',
    '$q',
    'UsersService',
    'CustomersService',
    'DistributorsService',
    'ValidationHelper',
    'leadingTexts',
    function ($scope, $modalInstance, $q, usersService, customersService, distributorsService, validationHelper, leadingTexts) {
        'use strict';
        var self = this;
        self.numberOfUpdatedUsers = 0;
        
        $scope.locals = {
            users: [],
            selectedUsers: [],
            leadingTexts: leadingTexts
        };  
        
        $scope.getUserInfoElement = function (user, leadingText) {
            if (!user.userAccount) {
                return {};
            } else {
                var userInfoElement = _(user.userAccount.User.UserInfoElements).find(function (uie) {
                    return uie.LeadingTextId === leadingText.Id;
                });
                return userInfoElement;
            }
        };

        $scope.verifyUser = function (user) {
            var deferred = $q.defer();
            user.isBusyVerifying = true;
            user.isValid = false;
            user.emailValidationText = null;

            // Validate required
            if (!user.Email) {
                user.isBusyVerifying = false;
                user.isValid = false;
                user.emailValidationText = 'E-mail is required';
                $scope.onUserSelected();
                deferred.resolve();
                return deferred.promise;
            }

            // Validate email
            user.isValid = validationHelper.isValidEmail(user.Email);
            if (!user.isValid) {
                user.isBusyVerifying = false;
                user.emailValidationText = 'Invalid e-mail';
                $scope.onUserSelected();
                deferred.resolve();
                return deferred.promise;
            }

            // Validate user exists
            usersService.getUserAccountByEmail(user.Email, false).then(function (foundUser) {
                if (foundUser) {
                    customersService.getLeadingTexts(foundUser.CustomerId).then(function (customLeadingTexts) {
                        user.isValid = true;
                        user.userAccount = foundUser;
                        _(customLeadingTexts).each(function (customLeadingText) {
                            var textFromExcel = user[customLeadingText.LeadingText.Text];
                            var userInfoElement = _(user.userAccount.User.UserInfoElements).find(function (uie) {
                                return uie.LeadingTextId === customLeadingText.LeadingTextId;
                            });
                            if (!userInfoElement) {
                                if (textFromExcel.toLowerCase() === '<blank>') {
                                    textFromExcel = '';
                                }
                                user.userAccount.User.UserInfoElements.push({
                                    LeadingTextId: customLeadingText.LeadingTextId,
                                    UserId: user.userAccount.User.Id,
                                    InfoText: textFromExcel
                                });
                            } else if (textFromExcel) {
                                if (textFromExcel.toLowerCase() === '<blank>') {
                                    textFromExcel = '';
                                }
                                userInfoElement.InfoText = textFromExcel;
                            }
                        });
                        if (user.ExternalUserId) {
                            if (user.ExternalUserId.toLowerCase() === '<blank>') {
                                user.ExternalUserId = '';
                            }                            
                            user.userAccount.User.ExternalUserId = user.ExternalUserId;
                        }
                        user.customLeadingTexts = customLeadingTexts;
                        user.isBusyVerifying = false;
                        $scope.onUserSelected();

                        // Hack to force re-trigger of ng-init
                        $scope.locals.leadingTexts = angular.copy($scope.locals.leadingTexts);

                        deferred.resolve();
                    }, function() {
                        user.emailValidationText = 'Failed to look up user by e-mail';
                        user.isBusyVerifying = false;
                        $scope.onUserSelected();
                        deferred.resolve();
                    });
                } else {
                    user.emailValidationText = 'User with e-mail was not found';
                    user.isValid = false;
                    user.isBusyVerifying = false;
                    $scope.onUserSelected();
                    deferred.resolve();
                }
            }, function () {
                user.emailValidationText = 'Failed to look up user by e-mail';
                user.isValid = false;
                user.isBusyVerifying = false;
                $scope.onUserSelected();
                deferred.resolve();
            });
            return deferred.promise;
        };
        
        $scope.toggleSelectAllUsers = function () {
            _($scope.locals.users).each(function (u) {
                if (u.isValid) {
                    u.isSelected = !!$scope.locals.isAllUsersSelected;
                }
            });
            $scope.onUserSelected();
        };

        $scope.onUserSelected = function () {
            var selectableUsers = _($scope.locals.users).filter(function (u) {
                return u.isValid;
            });

            $scope.locals.canSelectAll = selectableUsers.length === 0;
            $scope.locals.selectedUsers = _($scope.locals.users).filter(function (u) {
                return u.isSelected;
            });

            $scope.locals.isAllUsersSelected = selectableUsers.length === $scope.locals.selectedUsers.length && selectableUsers.length > 0;
        };

        $scope.$watch('locals.selectedFile', function () {
            $scope.locals.users = [];
            $scope.onUserSelected();
            if ($scope.locals.selectedFile && $scope.locals.selectedFile.users) {
                _($scope.locals.selectedFile.users).each(function (user) {
                    $scope.verifyUser(user).then(function() {
                        $scope.locals.users.push(user);
                    });
                });
            }
        });

        function updateUser(user) {
            var deferred = $q.defer();
            user.isBusyUpdating = true;
            user.isUpdated = false;
            usersService.saveUser(user.userAccount).then(function (updated) {
                user.isBusyUpdating = false;
                user.isUpdated = true;
                user.userAccount = updated;
                deferred.resolve();
            }, function () {
                user.isBusyUpdating = false;
                deferred.reject();
            });
            return deferred.promise;
        }

        $scope.update = function () {
            $q.all(_($scope.locals.selectedUsers).map(function (user) {
                return updateUser(user);
            })).then(function () {
                // Hack to force re-trigger of ng-init
                $scope.locals.leadingTexts = angular.copy($scope.locals.leadingTexts);
            });
        };
        
        $scope.close = function () {
            if (self.numberOfUpdatedUsers > 0) {
                $modalInstance.close(self.enrolledUsers);
            } else {
                $modalInstance.dismiss('cancel');
            }
        };
    }
]);