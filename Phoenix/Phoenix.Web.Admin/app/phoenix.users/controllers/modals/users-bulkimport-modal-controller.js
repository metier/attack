﻿angular.module('phoenix.users').controller('UsersBulkImportModalCtrl', [
    '$scope',
    '$modalInstance',
    '$q',
    'customer',
    'leadingTexts',
    'UsersService',
    'DistributorsService',
    'ValidationHelper',
    'NotificationService',
    function ($scope, $modalInstance, $q, customer, leadingTexts, usersService, distributorsService, validationHelper, notificationService) {
        'use strict';
        var self = this;
        if (customer.CustomLeadingTexts.length === 0) {
            var errorMsg = 'Could not find any leading texts for ' + customer.Name + '. Please make sure all the required fields are provided.';
            notificationService.alertError({ message: errorMsg });
            throw errorMsg;
        }

        self.numberOfImportedUsers = 0;
        $scope.customerId = customer.Id;
        
        $scope.locals = {
            users: [],
            selectedUsers: [],
            leadingTexts: leadingTexts
        };

        $scope.getCustomLeadingText = function (leadingText) {
            return _(customer.CustomLeadingTexts).find(function (clt) {
                return clt.LeadingTextId === leadingText.Id;
            });
        };

        self.getInfoElements = function (user) {
            var infoElements = [];
            _($scope.locals.leadingTexts).each(function (lt) {
                var customLeadingText = $scope.getCustomLeadingText(lt);
                infoElements.push({
                    LeadingTextId: customLeadingText.LeadingTextId,
                    InfoText: user[lt.Text]
                });
            });
            return infoElements;
        };
        
        self.isUserValid = function (user) {
            return !user.isPhoenixEmail &&
                   !user.isPhoenixUsername &&
                   !!user.FirstName &&
                   !!user.LastName &&
                   !!user.Email &&
                   !!user.Username &&
                   user.isEmailValid;
        };

        $scope.verifyUser = function (user) {
            user.isBusyVerifying = true;
            user.isSelected = false;
            user.isPhoenixEmail = false;
            user.isPhoenixUsername = false;
            user.isCreated = false;
            
            $q.all([$scope.lookupUserByEmail(user), $scope.lookupUserByUsername(user)])
              .then(function (results) {
                  var emailResult = results[0],
                      usernameResults = results[1];
                  
                  // Update user variables
                  user.isBusyVerifying = false;
                  user.isPhoenixEmail = !!emailResult && emailResult.Email === user.Email;
                  user.isPhoenixUsername = !!usernameResults;
                  user.isEmailValid = validationHelper.isValidEmail(user.Email);
                  user.isValid = self.isUserValid(user);
                  
                  $scope.onUserSelected();
              }, function () {
                  user.isBusyVerifying = false;
                  user.isValid = false;

                  $scope.onUserSelected();
              });
        };

        $scope.lookupUserByEmail = function (user) {
            var deferred = $q.defer();
            usersService.search({
                query: user.Email,
                distributorId: distributorsService.get().Id
            }).then(function (result) {
                deferred.resolve(result.Items[0]);
            }, function () {
                deferred.reject();
            });
            return deferred.promise;
        };

        $scope.lookupUserByUsername = function (user) {
            var deferred = $q.defer();
            usersService.getUserAccountByUsername(user.Username).then(function (result) {
                deferred.resolve(result);
            }, function () {
                deferred.resolve(null);
            });
            return deferred.promise;
        };

        $scope.toggleSelectAllUsers = function () {
            _($scope.locals.users).each(function (user) {
                if (self.isUserValid(user)) {
                    user.isSelected = !!$scope.locals.isAllUsersSelected;
                }
            });
            $scope.onUserSelected();
        };

        $scope.onUserSelected = function () {
            var selectableUsers = _($scope.locals.users).filter(function (u) {
                return self.isUserValid(u) && !u.isCreated;
            }); 

            $scope.locals.canSelectAll = selectableUsers.length === 0;
            $scope.locals.selectedUsers = _($scope.locals.users).filter(function (u) {
                return u.isSelected;
            });

            $scope.locals.isAllUsersSelected = selectableUsers.length === $scope.locals.selectedUsers.length && selectableUsers.length > 0;
        };
        
        $scope.importSelectedUsers = function () {
            _($scope.locals.selectedUsers).each(function (user) {
                var userNotificationDate = user['Registration Notification'] || moment.utc().format('DD.MM.YYYY');
                user.isBusyCreating = true;
                usersService.createUser({
                    CustomerId: $scope.customerId,
                    Username: user.Username,
                    Email: user.Email,
                    User: {
                        CustomerId: $scope.customerId,
                        FirstName: user.FirstName,
                        LastName: user.LastName,
                        UserInfoElements: self.getInfoElements(user),
                        IsAllowUserEcts: true,
                        UserRegistrationNotificationDate: moment.utc(userNotificationDate, 'DD.MM.YYYY')
                    },
                    Roles: ['LearningPortalUser']
                }, true).then(function () {
                    self.numberOfImportedUsers++;
                    user.isBusyCreating = false;
                    user.isSelected = false;
                    user.isValid = true;
                    user.isCreated = true;
                    $scope.onUserSelected();
                }, function () {
                    user.isCreated = false;
                    user.isBusyCreating = false;
                    $scope.onUserSelected();
                });
            });
        };

        $scope.$watch('locals.selectedFile', function () {
            $scope.locals.users = $scope.locals.selectedFile ? $scope.locals.selectedFile.users : [];
            _($scope.locals.users).each(function (user) {
                if (!user.Username) {
                    user.Username = user.Email;
                }
                $scope.verifyUser(user);
            });
            $scope.onUserSelected();
        });

        $scope.close = function () {
            if (self.numberOfImportedUsers > 0) {
                $modalInstance.close();
            } else {
                $modalInstance.dismiss('cancel');
            }
        };
    }
]);