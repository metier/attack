﻿angular.module('phoenix.users').controller('MergeUsersModalCtrl', [
    '$scope',
    '$modalInstance',
    'user',
    'UsersService',
    'DistributorsService',
    function ($scope, $modalInstance, user, usersService, distributorsService) {
        'use strict';
        var self = this;
        $scope.user = user;
        $scope.isMerging = false;

        self.init = function () {
            $scope.radioModel = 'Left';
        };
        
        $scope.onUserSelected = function(selectedUser) {
            $scope.selectedUser = selectedUser;
        }

        $scope.getDistributorId = function() {
            return distributorsService.get().Id;
        }

        $scope.setMergeDirection = function(direction) {
            $scope.radioModel = direction;
        }

        $scope.merge = function () {
            $scope.isMerging = true;

            if ($scope.radioModel === 'Left') {
                usersService.mergeUsers($scope.user.Id, $scope.selectedUser.Id).then(function () {
                    $modalInstance.close($scope.user);
                    $scope.isMerging = false;
                });
            }
            else if ($scope.radioModel === 'Right') {
                usersService.mergeUsers($scope.selectedUser.Id, $scope.user.Id).then(function () {
                    $modalInstance.close($scope.selectedUser);
                    $scope.isMerging = false;
                });
            }
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        self.init();
    }
]);

    