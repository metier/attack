﻿angular.module('phoenix.users').controller('UsersSelectModalCtrl', [
    '$scope',
    '$modalInstance',
    'customerId',
    'UsersService',
    function ($scope, $modalInstance, customerId, usersService) {
        'use strict';
        var self = this;
        $scope.customerId = customerId;
        
        self.initialData = null;
        $scope.limit = 10;
        $scope.isInitialized = false;
        $scope.lastSearchQuery = '';
        $scope.selectedArticle = null;
        $scope.model = {
            query: '',
            selectedUsers: [],
            searchAllCustomers: false
        };

        $scope.$watch('model.searchAllCustomers', function () {
            self.init();
        });

        $scope.search = function () {
            if (!$scope.model.query) {
                return;
            }
            $scope.isSearching = true;
            $scope.lastSearchQuery = $scope.model.query;
            usersService.search({ query: $scope.model.query, limit: $scope.limit, page: 1, customerId: $scope.model.searchAllCustomers ? null : customerId }).then(function (result) {
                $scope.isSearching = false;
                $scope.users = result;
            }, function () {
                $scope.isSearching = false;
            });
        };

        $scope.navigate = function (page) {
            usersService.search({ query: $scope.model.query, limit: $scope.limit, page: page, customerId: $scope.model.searchAllCustomers ? null : customerId }).then(function (result) {
                $scope.users = result;
            });
        };

        $scope.toggleSelect = function (user) {
            var selectedUser = self.getSelectedUser(user.Id),
                index;
            if (!selectedUser) {
                $scope.model.selectedUsers.push(user);
            } else {
                index = $scope.model.selectedUsers.indexOf(selectedUser);
                if (index > -1) {
                    $scope.model.selectedUsers.splice(index, 1);
                }
            }
        };

        $scope.isSelected = function (user) {
            return !!self.getSelectedUser(user.Id);
        };

        self.getSelectedUser = function (id) {
            return _($scope.model.selectedUsers).find(function (a) { return a.Id === id; });
        };

        $scope.onQueryChanged = function () {
            if ($scope.model.query === '') {
                $scope.users = self.initialData;
            }
        };
        
        self.init = function () {
            $scope.model.query = '';
            usersService.search({ query: $scope.model.query, limit: $scope.limit, page: 1, customerId: $scope.model.searchAllCustomers ? null : customerId }).then(function (result) {
                $scope.isInitialized = true;
                self.initialData = result;
                $scope.users = result;
            });
        };

        self.init();


        $scope.ok = function () {
            $modalInstance.close($scope.model.selectedUsers);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }
]);