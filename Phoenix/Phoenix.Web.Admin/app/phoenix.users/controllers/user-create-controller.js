﻿angular.module('phoenix.users').controller('UserCreateCtrl', [
    '$scope',
    function ($scope) {
        'use strict';        
        $scope.$on('$stateChangeSuccess', function (event, toState) {
            $scope.isCustomerContext = toState.name.indexOf('customer') === 0;
            $scope.isUserContext = toState.name.indexOf('user') === 0;
        });
    }
]);