﻿angular.module('phoenix.users').controller('UserCtrl', [
    '$scope',
    'userAccount',
    'participants',
    'codes',
    '$state',
    '$modal',
    'phoenix.config',
    function ($scope, userAccount, participants, codes, $state, $modal, config) {
        'use strict';
        $scope.userAccount = userAccount;
        $scope.participants = participants;
        
        $scope.getCountryName = function (countryCode) {
            var codeItem = _(codes.countryCodes).find(function (item) {
                return item.Value === countryCode;
            });
            if (codeItem) {
                return codeItem.Display;
            }
            return null;
        };

        $scope.$on('userSaved', function (event, updatedUserAccount) {
            $scope.userAccount = updatedUserAccount;
        });

        $scope.isActive = function (state, absolute) {
            if (absolute) {
                return $state.current.name === state;
            }
            return $state.current.name.indexOf(state) === 0;
        };

        $scope.getSection = function () {
            return $state.params.section;
        };

        $scope.mergeUsers = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.users/views/modals/merge-users-modal.html',
                controller: 'MergeUsersModalCtrl',
                resolve: {
                    user: function () { return $scope.userAccount.User; }
                },
                windowClass: 'modal-wider'
            });
            modalInstance.result.then(function (user) {
                if ($scope.isCustomerContext) {
                    $state.go('customer.user.overview.details', { userId: user.Id, section: $state.params.section });
                } else {
                    $state.go('user.overview.details', { userId: user.Id, section: $state.params.section });
                }
            });
        };

        $scope.$on('$stateChangeSuccess', function (event, toState) {
            $scope.isCustomerContext = toState.name.indexOf('customer') === 0;
            $scope.isUserContext = toState.name.indexOf('user') === 0;
            $scope.isEditMode = toState.name.indexOf('user.overview.edit') === 0 || toState.name.indexOf('customer.user.overview.edit') === 0;
        });
    }
]);