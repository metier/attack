﻿angular.module('phoenix.users').constant('lessonStatuses', [
    { value: 'N', text: 'Not attempted' },
    { value: 'I', text: 'Incompleted' },
    { value: 'C', text: 'Completed' },
    { value: 'P', text: 'Passed' },
    { value: 'F', text: 'Failed' }
]);