﻿/*global angular */
/*jslint maxlen: 150 */
angular.module('phoenix.resources').filter('resourceTypeName', [function () {
    'use strict';
    return function (resourceTypeId) {
        if (resourceTypeId === 1) {
            return 'Instructor';
        }
        if (resourceTypeId === 2) {
            return 'Location';
        }
        if (resourceTypeId === 3) {
            return 'E-Coach';
        }
        if (resourceTypeId === 4) {
            return 'Partners';
        }
        if (resourceTypeId === 5) {
            return 'Examiner';
        }
        if (resourceTypeId === 6) { 
            return 'Invigilator';
        }
        if (resourceTypeId === 7) {
            return 'Study Advisor';
        }
        return '';
    };
}]);