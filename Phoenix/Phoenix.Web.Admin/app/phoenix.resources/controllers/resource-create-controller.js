﻿angular.module('phoenix.resources').controller('ResourceCreateCtrl', [
    '$scope',
    '$state',
    'resourceWrapper',
    'codes',
    'ResourcesService',
    'NotificationService',
    'UsersService',
    'educationPartnerSchemaTypes',
    function ($scope, $state, resourceWrapper, codes, resourceService, notificationService, usersService, educationPartnerSchemaTypes) {
        'use strict';
        var self = this, ADDRESSTYPE_POST = 1;

        $scope.resource = angular.copy(resourceWrapper.resource);
        $scope.localModel = {};
        $scope.isEditMode = !!$scope.resource.Id;
        $scope.isBusySaving = false;
        self.isExaminerResourceType = $scope.resource.ResourceTypeId === 5;

        self.initAddress = function (addressType) {
            var address;

            address = _.find($scope.resource.Addresses, function (a) {
                return a.AddressType === addressType;
            });

            if (!address) {
                address = { AddressType: addressType, ResourceId: $scope.resource.Id };
                $scope.resource.Addresses.push(address);
            }

            return address;
        };

        $scope.onResourceDocumentUploaded = function (attachment) {
            if (attachment) {
                $scope.resource.Attachments.push(attachment);
            }
        };

        $scope.deleteAttachment = function (attachment) {
            var index = $scope.resource.Attachments.indexOf(attachment);
            if (index > -1) {
                $scope.resource.Attachments.splice(index, 1);
            }
        };
        
        self.setUser = function (user) {
            $scope.resource.UserId = user.Id;
            $scope.userSelectedCustomer = user;
            $scope.localModel.user = user;
        };
        
        self.removeUser = function () {
            $scope.resource.User = null;
            $scope.resource.UserId = null;
            $scope.userSelectedCustomer = null;
            $scope.localModel.user = null;
            $scope.roleWarningMessage = null;
        };
        
        function verifyUserRole(userId, requiredRole) {
            usersService.getUserAccountByUserId(userId).then(function (userAccount) {
                if (!_(userAccount.Roles).contains(requiredRole)) {
                    $scope.roleWarningMessage = 'User is missing the role ' + requiredRole;
                } else {
                    $scope.roleWarningMessage = null;
                }
            });
        }

        $scope.isPersonResource = function () {
            return $scope.resource.ResourceTypeId === 1 || 
                   $scope.resource.ResourceTypeId === 3 ||
                   $scope.resource.ResourceTypeId === 5 ||
                   $scope.resource.ResourceTypeId === 6 ||
                   $scope.resource.ResourceTypeId === 7;
        };
        
        $scope.onUserSelected = function (user) {
            if ($scope.resource.UserId !== user.Id) {
                self.setUser(user);
                if (self.isExaminerResourceType) {
                    verifyUserRole(user.Id, 'Examiner');
                }
            }
        };
        
        $scope.setValidUser = function (user) {
            if (user && $scope.userSelectedCustomer) {
                if (!$scope.resource.Name) {
                    $scope.resource.Name = $scope.userSelectedCustomer.FirstName + ' ' + $scope.userSelectedCustomer.LastName;
                }
                if (!$scope.resource.ContactEmail) {
                    $scope.resource.ContactEmail = $scope.userSelectedCustomer.Email;
                }
                $scope.localModel.user = $scope.userSelectedCustomer;
            } else {
                self.removeUser();
            }
        };
        
        $scope.searchUser = function (query) {
            return usersService.search({ query: query, limit: 20 }).then(function (result) {
                return result.Items;
            });
        };
        
        $scope.save = function () {
            $scope.isBusySaving = true;
            var saveFunc = $scope.isEditMode ? resourceService.update : resourceService.create;
            saveFunc($scope.resource).then(function (result) {
                $scope.isBusySaving = false;
                resourceWrapper.resource = result;
                $state.go('resources.item.view', { resourceId: result.Id });
            }, function () {
                $scope.isBusySaving = false;
            });
        };
        
        $scope.cancel = function () {
            if ($scope.isEditMode) {
                $state.go('resources.item.view');
            } else {
                $state.go('resourceslist');
            }
        };

        self.init = function () {
            var user;

            $scope.countries = codes.countryCodes;
            $scope.currencies = codes.currencyCodes;
            $scope.educationPartnerSchemaTypes = educationPartnerSchemaTypes;
            $scope.mailAddress = self.initAddress(ADDRESSTYPE_POST);

            if ($scope.resource.User) {
                user = {
                    Id: $scope.resource.User.Id,
                    FirstName: $scope.resource.User.FirstName,
                    LastName: $scope.resource.User.LastName
                };
                self.setUser(user);
                if (self.isExaminerResourceType) {
                    verifyUserRole(user.Id, 'Examiner');
                }
            }
        };
        
        self.init();
    }
]);