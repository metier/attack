﻿angular.module('phoenix.resources').controller('ResourceListCtrl', [
    '$scope',
    '$state',
    'resources',
    '$location',
    function ($scope, $state, resources, $location) {
        'use strict';
        $scope.resources = resources;
        $scope.query = $location.search().query;
        $scope.orderBy = $location.search().orderBy || '';
        $scope.orderDirection = $location.search().orderDirection || '';

        $scope.page = $location.search().page;
        $scope.limit = parseInt($location.search().limit, 10) || 15;
        $scope.maxPages = $scope.resources.TotalCount;

        $scope.onQueryChanged = function () {
            if ($scope.query === '') {
                $scope.search();
            }
        };

        $scope.search = function () {
            if (!$state.params.query && $scope.query === '') { return; }
            $state.go('resourceslist', { query: $scope.query, page: 1 });
        };

        $scope.navigate = function (page) {
            if (page < 1 || page > $scope.maxPages) {
                return;
            }
            $location.search('page', page);
        };
    }
]);