﻿/*global angular, _*/
angular.module('phoenix.resources').controller('ResourceCtrl', [
    '$scope',
    'resourceWrapper',
    'codes',
    'UsersService',
    function ($scope, resourceWrapper, codes, usersService) {
        'use strict';
        var self = this, ADDRESSTYPE_POST = 1;
        $scope.resource = resourceWrapper.resource;
        self.isExaminerResourceType = $scope.resource.ResourceTypeId === 5;

        $scope.isPersonResource = function () {
            return $scope.resource.ResourceTypeId === 1 ||
                   $scope.resource.ResourceTypeId === 3 ||
                   $scope.resource.ResourceTypeId === 5 ||
                   $scope.resource.ResourceTypeId === 6 ||
                   $scope.resource.ResourceTypeId === 7;
        };

        function verifyUserRole(userId, requiredRole) {
            usersService.getUserAccountByUserId(userId).then(function (userAccount) {
                if (!_(userAccount.Roles).contains(requiredRole)) {
                    $scope.roleWarningMessage = 'User is missing the role ' + requiredRole;
                } else {
                    $scope.roleWarningMessage = null;
                }
            });
        }
        
        $scope.getCountryName = function (countryCode) {
            var codeItem = _($scope.countries).find(function (item) {
                return item.Value === countryCode;
            });
            if (codeItem) {
                return codeItem.Display;
            }
            return null;
        };

        self.initAddress = function (addressType) {
            var address;

            address = _.find($scope.resource.Addresses, function (a) {
                return a.AddressType === addressType;
            });

            return address;
        };
        
        self.init = function () {
            $scope.countries = codes.countries;
            $scope.mailAddress = self.initAddress(ADDRESSTYPE_POST);

            if (self.isExaminerResourceType && $scope.resource.User) {
                verifyUserRole($scope.resource.User.Id, 'Examiner');
            }
        };
        
        self.init();
    }
]);