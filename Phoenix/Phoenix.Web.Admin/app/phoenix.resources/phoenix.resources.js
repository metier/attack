﻿/*global angular*/
angular.module('phoenix.resources', ['phoenix.config', 'phoenix.shared', 'ui.router', 'ngUpload'])
    .config(['$stateProvider', 'phoenix.config', function ($stateProvider, config) {
        'use strict';
        $stateProvider.state('resources', {
            url: '/resources',
            abstract: true,
            template: '<ui-view />',
            resolve: {
                codes: ['CodesService', function (codesService) {
                    return codesService.getAll();
                }]
            }
        }).state('resourceslist', {
            url: '/resources?page&limit&orderBy&orderDirection&query',
            controller: 'ResourceListCtrl',
            templateUrl: config.basePath + 'app/phoenix.resources/views/resource-list.html',
            resolve: {
                resources: ['ResourcesService', '$stateParams', function (resourcesService, $stateParams) {
                    var options = {
                        query: $stateParams.query,
                        page: parseInt($stateParams.page, 10) || 1,
                        limit: parseInt($stateParams.limit, 10) || 15,
                        orderBy: $stateParams.orderBy,
                        orderDirection: $stateParams.orderDirection
                    };
                    return resourcesService.search(options);
                }]
            }
        }).state('resources.create', {
            url: '/create/:resourceTypeId',
            controller: 'ResourceCreateCtrl',
            templateUrl: config.basePath + 'app/phoenix.resources/views/resource-create.html',
            resolve: {
                resourceWrapper: ['$stateParams', function ($stateParams) {
                    return {
                        resource: {
                            ResourceTypeId: parseInt($stateParams.resourceTypeId, 10),
                            Addresses: [],
                            Attachments: []
                        }
                    };
                }]
            }
        }).state('resources.item', {
            url: '/:resourceId',
            abstract: true,
            template: '<ui-view />',
            resolve: {
                resourceWrapper: ['ResourcesService', '$stateParams', '$q', function (resourcesService, $stateParams, $q) {
                    var deferred = $q.defer();
                    resourcesService.findById($stateParams.resourceId).then(function (result) {
                        deferred.resolve({
                            resource: result
                        });
                    }, function () {
                        deferred.reject();
                    });
                    return deferred.promise;
                }]
            }
        }).state('resources.item.view', {
            url: '',
            controller: 'ResourceCtrl',
            templateUrl: config.basePath + 'app/phoenix.resources/views/resource.html'
        }).state('resources.item.edit', {
            url: '/edit',
            controller: 'ResourceCreateCtrl',
            templateUrl: config.basePath + 'app/phoenix.resources/views/resource-create.html'
        });
    }]);