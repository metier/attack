﻿/*global angular */
angular.module('phoenix.resources').factory('ResourcesService', ['$http', '$q', 'asyncHelper', 'phoenix.config', function ($http, $q, asyncHelper, config) {
    'use strict';
    
    return {
        search: function (options) {
            var url = config.apiPath + 'resources',
                parameters;
            
            parameters = {
                query: options.query,
                type: options.type,
                searchColumn: options.searchColumn,
                orderBy: options.orderBy,
                orderDirection: options.orderDirection
            };
            
            if (typeof options.limit === 'number') {
                parameters.limit = options.limit;
            }
            if (typeof options.page === 'number' && options.page > 0) {
                parameters.skip = (options.page - 1) * parameters.limit;
            }
            return asyncHelper.resolveHttp($http.get(url, { params: parameters }));
        },
        create: function (resource) {
            var url = config.apiPath + 'resources';
            return asyncHelper.resolveHttp($http.post(url, resource));
        },
        update: function (resource) {
            var url = config.apiPath + 'resources',
                deferred = $q.defer();
            
            $http.put(url, resource).success(function (result) {
                deferred.resolve(result);
            }).error(function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        },
        findById: function (resourceId) {
            var url = config.apiPath + 'resources/' + resourceId;
            return asyncHelper.resolveHttp($http.get(url), true);
        },
        getEducationalPartners: function () {
            var url = config.apiPath + 'resources';

            var parameters = {
                type: 4
            };

            return asyncHelper.resolveHttp($http.get(url, { params: parameters }));
        }

    };
}]);