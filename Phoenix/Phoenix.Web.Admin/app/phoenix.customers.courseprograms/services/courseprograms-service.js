﻿angular.module('phoenix.customers').factory('CourseProgramsService', [
    '$http',
    'asyncHelper',
    'phoenix.config',
    function ($http, asyncHelper, config) {
        'use strict';
        return {
            get: function (id) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'courseprograms/' + id), true);
            },
            getByCustomerId: function (customerId) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'courseprograms', { params: { customerId: customerId }}), true);
            },
            create: function (courseProgram) {
                return asyncHelper.resolveHttp($http.post(config.apiPath + 'courseprograms', courseProgram));
            },
            update: function (courseProgram) {
                return asyncHelper.resolveHttp($http.put(config.apiPath + 'courseprograms/' + courseProgram.Id, courseProgram));
            }
        };
    }
]);