﻿/*global _*/
angular.module('phoenix.customers').factory('CourseProgramsUtilities', [
    function () {
        'use strict';
        return {
            getItem: function (courseProgram, itemId, type) {
                var item;
                _(courseProgram.Steps).each(function (step) {
                    if (type === 'exam') {
                        item = _(step.Exams).find(function (e) { return e.Id === itemId; });
                    }
                    if (type === 'module') {
                        item = _(step.Modules).find(function (m) { return m.Id === itemId; });
                    }
                    if (item) {
                        return;
                    }
                });
                return item;
            }         
        };
    }
]);