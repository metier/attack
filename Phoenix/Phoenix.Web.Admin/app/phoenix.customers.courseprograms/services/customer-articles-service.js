﻿angular.module('phoenix.customers').factory('CustomerArticlesService', [
    '$http',
    'asyncHelper',
    'phoenix.config',
    function ($http, asyncHelper, config) {
        'use strict';
        return {
            get: function (id) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'customerarticles/' + id), true);
            },
            getByExamId: function (examId) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'customerarticles', { params: { examId: examId } }), true);
            },
            getByModuleId: function (moduleId) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'customerarticles', { params: { moduleId: moduleId } }), true);
            },
            getByArticleId: function (articleId, distributorId) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'customerarticles', { params: { articleId: articleId, distributorId: distributorId } }), true);
            },
            create: function (customerArticle) {
                return asyncHelper.resolveHttp($http.post(config.apiPath + 'customerarticles', customerArticle));
            },
            update: function (customerArticle) {
                return asyncHelper.resolveHttp($http.put(config.apiPath + 'customerarticles/' + customerArticle.Id, customerArticle));
            },
            getEnrollmentConditions: function (id) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'customerarticles/' + id + '/enrollmentconditions'), false);
            },
            saveEnrollmentConditions: function (enrollmentConditions, customerArticleId) {
                _.each(enrollmentConditions, function(condition) {
                    condition.CustomerArticleId = customerArticleId;
                });
                var url = config.apiPath + 'customerarticles' + "/" + customerArticleId + "/enrollmentconditions";
                return asyncHelper.resolveHttp($http.put(url, enrollmentConditions));
            }
        };
    }
]);