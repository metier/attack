﻿angular.module('phoenix.customers').controller('CustomerArticleCreateCtrl', [
    '$scope',
    'CodesService',
    'ArticlesService',
    'ProductDescriptionsService',
    'CustomerArticlesService',
    'NotificationService',
    function ($scope, codesService, articlesService, productDescriptionsService, customerArticlesService, notificationService) {
        'use strict';
        $scope.customerArticle = $scope.customerArticle || {};

        $scope.$watch('selectedArticleId', function () {
            if ($scope.selectedArticleId) {
                articlesService.get($scope.selectedArticleId).then(function (article) {
                    $scope.article = article;
                    $scope.customerArticle.Title = $scope.article.Title;

                    if (article.ProductDescriptionId) {
                        productDescriptionsService.get(article.ProductDescriptionId).then(function (productDescription) {
                            $scope.productDescription = productDescription;
                        });
                    } else {
                        $scope.productDescription = null;
                        notificationService.alertWarning({
                            header: 'Article ' + $scope.article.ArticlePath,
                            message: 'Article is missing product description'
                        });
                    }
                });
            }
        });
        
        $scope.onToggleSelection = function (control) {
            if (control === "FixedPrice")
                $scope.customerArticle.FixedPriceNotBillable = !$scope.customerArticle.FixedPriceNotBillable;

            if (control === "UnitPrice")
                $scope.customerArticle.UnitPriceNotBillable = !$scope.customerArticle.UnitPriceNotBillable;
        }


        $scope.createCustomerArticle = function () {
            if ($scope.itemType === 'module') {
                $scope.customerArticle.ModuleId = $scope.itemId;
            }
            if ($scope.itemType === 'exam') {
                $scope.customerArticle.ExamId = $scope.itemId;
            }
            $scope.customerArticle.CustomerId = $scope.customerId;
            $scope.customerArticle.ArticleOriginalId = $scope.article.Id;

            customerArticlesService.create($scope.customerArticle).then(function (result) {
                $scope.$emit('CustomerArticleCreated', result);
            });
        };
        
        $scope.saveCustomerArticle = function () {
            customerArticlesService.update($scope.customerArticle).then(function (result) {
                $scope.$emit('CustomerArticleUpdated', result);
            });
        };
        
        codesService.getAll().then(function (codes) {
            $scope.currencies = codes.currencyCodes;
        });
    }
]);