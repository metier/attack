﻿angular.module('phoenix.customers').directive('customerArticleCreate', [
    'phoenix.config',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            controller: 'CustomerArticleCreateCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.courseprograms/directives/customer-article-create.html'
        };
    }
]);