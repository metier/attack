﻿angular.module('phoenix.customers').controller('CustomerArticleActivitiesCtrl', [
    '$scope',
    '$state',
    '$modal',
    '$location',
    'phoenix.config',
    'customer',
    'activities',
    'ArticlesService',
    'ActivitiesService',
    'RcosService',
    'data',
    'courseProgram',
    'educationalPartners',
    function ($scope, $state, $modal, $location, config, customer, activities, articlesService, activitiesService, rcosService, data, courseProgram, educationalPartners) {
        'use strict';

        $scope.activities = activities;
        $scope.customer = customer;
        $scope.isMockexam = data.customerArticleArticle.ArticleTypeId === 8;

        $scope.orderBy = $location.search().orderBy || '';
        $scope.orderDirection = $location.search().orderDirection || '';

        $scope.create = function () {
            $scope.isBusyCreating = true;
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.customers.activities/views/modals/activity-create-modal.html',
                controller: 'ActivityCreateModalCtrl',
                resolve: {
                    customer: function () { return $scope.customer; },
                    customerArticle: function () { return angular.copy($scope.customerArticle); },
                    copyActivity: function () { return null; },
                    articleCopy: function () {
                        return articlesService.get($scope.customerArticle.ArticleCopyId);
                    },
                    courseProgram: function () { return courseProgram; },
                    educationalPartners: function () { return educationalPartners; }
                },
                windowClass: 'modal-wider'
            });
            modalInstance.result.then(function () {
                $state.go('customer.courseprograms.item.view.cpitem.customerarticle.activities', null, { reload: true });
            });
            modalInstance.opened.then(function () {
                $scope.isBusyCreating = false;
            });
        };

        $scope.openRcoCourseInfoModal = function (rcoId) {
            $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.products/views/modals/rco-course-info-modal.html',
                controller: 'RcoCourseInfoModalCtrl',
                resolve: {
                    rcoCourse: function () {
                        return rcosService.getCourse(rcoId);
                    }
                },
                windowClass: 'modal-wide'
            });
        };

        $scope.openRcoExamInfoModal = function (rcoId) {
            $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.products/views/modals/rco-exam-info-modal.html',
                controller: 'RcoExamInfoModalCtrl',
                resolve: {
                    rcoExam: function () {
                        return rcosService.getExam(rcoId);
                    }
                },
                windowClass: 'modal-wide'
            });
        };

        $scope.copy = function (activityListItem) {
            activityListItem.isBusyCopying = true;

            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.customers.activities/views/modals/activity-create-modal.html',
                controller: 'ActivityCreateModalCtrl',
                resolve: {
                    customer: function () { return $scope.customer; },
                    customerArticle: function () { return angular.copy($scope.customerArticle); },
                    copyActivity: function () { return activitiesService.get(activityListItem.ActivityId); },
                    articleCopy: function() {
                        return articlesService.get($scope.customerArticle.ArticleCopyId);
                    },
                    courseProgram: function () { return courseProgram; },
                    educationalPartners: function () { return educationalPartners; }
                },
                windowClass: 'modal-wider'
            });
            modalInstance.result.then(function () {
                $state.go('customer.courseprograms.item.view.cpitem.customerarticle.activities', null, { reload: true });
            });
            modalInstance.opened.then(function () {
                activityListItem.isBusyCopying = false;
            });
        };

        $scope.delete = function (activityListItem) {
            if (confirm("Do you really want to delete the activity?")) {
                activityListItem.isBusyDeleting = true;
                activitiesService.get(activityListItem.ActivityId).then(function (activity) {
                    activity.IsDeleted = true;
                    activitiesService.update(activity).then(function () {
                        activityListItem.isBusyDeleting = false;
                        $state.go('customer.courseprograms.item.view.cpitem.customerarticle.activities', null, { reload: true });
                    }, function () {
                        activityListItem.isBusyDeleting = false;
                    });
                });
            }
        };
    }
]);