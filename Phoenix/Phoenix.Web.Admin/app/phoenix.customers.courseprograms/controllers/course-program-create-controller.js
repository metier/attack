﻿angular.module('phoenix.customers').controller('CourseProgramCreateCtrl', [
    '$scope',
    '$state',
    'courseProgram',
    'coursePrograms',
    'CourseProgramsService',
    'NotificationService',
    'CustomerArticlesService',
    function ($scope, $state, courseProgram, coursePrograms, courseProgramsService, notificationService, customerArticlesService) {
        'use strict';
        $scope.originalCourseProgram = angular.copy(courseProgram);
        $scope.courseProgram = angular.copy($scope.originalCourseProgram);
        $scope.ects = ['5', '7.5', '10', '15'];

        $scope.isEditMode = !!$scope.originalCourseProgram.Id;
        if (!$scope.isEditMode) {
            coursePrograms.push($scope.originalCourseProgram);
        }
        $scope.isBusy = false;

        $scope.sortableModulesOptions = {
            placeholder: 'cp-module-placeholder',
            connectWith: '.cp-modules-dropzone',
            // Make drop at end of horizontal list work correctly:
            start: function (e) {
                angular.element(e.target).data("ui-sortable").floating = true;
            }
        };
        
        $scope.sortableExamsOptions = {
            placeholder: 'cp-exam-placeholder',
            connectWith: '.cp-exams'
        };
        
        $scope.sortableStepsOptions = {
            placeholder: 'cp-step-placeholder'
        };
        
        $scope.addStep = function () {
            $scope.courseProgram.Steps.unshift({
                Exams: [],
                Modules: [],
                Name: ''
            });
        };
        
        $scope.removeStep = function (step) {
            if (confirm('Are you sure you want to remove the step?')) {
                if (step.Exams.length > 0 || step.Modules.length > 0) {
                    notificationService.alertError({
                        header: 'Unable to remove step',
                        message: 'Exams and modules in this step must be removed before this step can be removed'
                    });
                } else {
                    var index = $scope.courseProgram.Steps.indexOf(step);
                    if (index > -1) {
                        $scope.courseProgram.Steps.splice(index, 1);
                    }
                }
            }
        };
        
        $scope.addModule = function (step) {
            step.Modules.push({
                Name: ''
            });
        };

        $scope.removeModule = function (step, module) {
            if (confirm('Are you sure you want to remove the module?')) {
                $scope.isBusy = true;
                customerArticlesService.getByModuleId(module.Id).then(function(result) {
                    if (result.length > 0) {
                        notificationService.alertError({
                            header: 'Unable to remove module',
                            message: 'Customer article(s) must be deleted from module before module can be removed'
                        });
                    } else {
                        var index = step.Modules.indexOf(module);
                        if (index > -1) {
                            step.Modules.splice(index, 1);
                        }
                    }
                    $scope.isBusy = false;
                }, function() {
                    $scope.isBusy = false;
                });
            }
        };
        
        $scope.addExam = function (step) {
            step.Exams.unshift({
                Name: ''
            });
        };

        $scope.removeExam = function (step, exam) {
            if (confirm('Are you sure you want to remove the exam?')) {
                $scope.isBusy = true;
                customerArticlesService.getByExamId(exam.Id).then(function (result) {
                    if (result.length > 0) {
                        notificationService.alertError({
                            header: 'Unable to remove exam',
                            message: 'Customer article(s) must be deleted from exam before exam can be removed'
                        });
                    } else {
                        var index = step.Exams.indexOf(exam);
                        if (index > -1) {
                            step.Exams.splice(index, 1);
                        }
                    }
                    $scope.isBusy = false;
                }, function () {
                    $scope.isBusy = false;
                });
            }
        };
        
        function correctOrder(cp) {
            _(cp.Steps).each(function (step, index) {
                step.Order = index;
                _(step.Modules).each(function (module, mIndex) {
                    module.Order = mIndex;
                });
                _(step.Exams).each(function (exam, eIndex) {
                    exam.Order = eIndex;
                });
            });
        }

        function updateCourseStepIds(courseProgram) {
            _(courseProgram.Steps).each(function (step) {
                _(step.Modules).each(function (module) {
                    if (step.Id) {
                        module.CourseStepId = step.Id;
                    }
                });
                _(step.Exams).each(function(exam) {
                    if (step.Id) {
                        exam.CourseStepId = step.Id;
                    }
                });
            });
        }

        $scope.save = function () {
            $scope.isBusy = true;
            correctOrder($scope.courseProgram);
            updateCourseStepIds($scope.courseProgram);

            var saveFunction = $scope.courseProgram.Id ? courseProgramsService.update : courseProgramsService.create;
            saveFunction($scope.courseProgram).then(function (result) {
                $scope.isBusy = false;
                $state.go('customer.courseprograms.item.view', { courseProgramId: result.Id }, { reload: true });
                
            }, function() {
                $scope.isBusy = false;
            });
        };
        
        $scope.cancel = function () {
            if ($scope.isEditMode) {
                $state.go('customer.courseprograms.item.view');
            } else {
                var index = $scope.coursePrograms.indexOf($scope.originalCourseProgram);
                if (index > -1) {
                    $scope.coursePrograms.splice(index, 1);
                }
                if ($scope.coursePrograms.length > 0) {
                    $state.go('customer.courseprograms.item.view', { courseProgramId: $scope.coursePrograms[0].Id });
                } else {
                    $state.go('customer.courseprograms');
                }

            }
        };
        
        $scope.$on('$stateChangeStart', function () {
            if (!$scope.isEditMode) {
                var index = $scope.coursePrograms.indexOf($scope.originalCourseProgram);
                if (index > -1) {
                    $scope.coursePrograms.splice(index, 1);
                }
            }
        });
    }
]);