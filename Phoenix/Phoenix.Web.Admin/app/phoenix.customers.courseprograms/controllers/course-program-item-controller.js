﻿angular.module('phoenix.customers').controller('CourseProgramItemCtrl', [
    '$scope',
    '$state',
    '$modal',
    'customer',
    'courseProgram',
    'coursePrograms',
    'customerArticles',
    'CourseProgramsService',
    'CourseProgramsUtilities',
    'NotificationService',
    'CustomerArticlesService',
    'phoenix.config',
    function ($scope, $state, $modal, customer, courseProgram, coursePrograms, customerArticles, courseProgramsService, courseProgramsUtilities, notificationService, customerArticlesService, config) {
        'use strict';
        var self = this;
        $scope.type = $state.params.type;
        $scope.itemId = parseInt($state.params.itemId, 10);
        $scope.customerArticles = customerArticles;

        self.init = function (courseProgram) {
            $scope.originalCourseProgram = courseProgram;
            $scope.courseProgram = angular.copy($scope.originalCourseProgram);
            $scope.originalCourseProgramItem = courseProgramsUtilities.getItem(courseProgram, $scope.itemId, $scope.type);
            $scope.courseProgramItem = angular.copy($scope.originalCourseProgramItem);
        };

        self.init(courseProgram);

        $scope.isCustomerArticleSelected = function (customerArticle) {
            return $state.params.customerArticleId === String(customerArticle.Id);
        };
        
        $scope.selectCustomerArticle = function (customerArticle) {
            $state.go('customer.courseprograms.item.view.cpitem.customerarticle.activities', {
                customerArticleId: customerArticle.Id,
                limit: null,
                orderBy: null,
                orderDirection: null
            });
        };
        
        $scope.deleteCustomerArticle = function (customerArticle) {
            if (confirm('Are you sure you want to delete the customer article?')) {
                customerArticle.IsDeleted = true;
                customerArticlesService.update(customerArticle).then(function () {
                    var index = customerArticles.indexOf(customerArticle);
                    if (index > -1) {
                        customerArticles.splice(index, 1);
                    }
                    if (customerArticles.length > 0) {
                        $state.go('customer.courseprograms.item.view.cpitem', { customerArticleId: $scope.customerArticles[0].Id });
                    } else {
                        $state.go('customer.courseprograms.item.view.cpitem');
                    }
                });
            }
        };
        
        $scope.createNewCustomerArticle = function() {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.customers.courseprograms/views/modals/customer-article-create-modal.html',
                controller: 'CustomerArticleCreateModalCtrl',
                resolve: {
                    customerId: function() { return customer.Id; },
                    itemId: function () { return $scope.itemId; },
                    itemType: function () { return $scope.type; }
                    
                },
                windowClass: 'modal-wider'
            });
            modalInstance.result.then(function (customerArticle) {
                $scope.customerArticles.push(customerArticle);
                $scope.selectCustomerArticle(customerArticle);
            });
        };
        
        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState) {
            if (toState.name === 'customer.courseprograms.item.view.cpitem' && fromState.name !== 'customer.courseprograms.item.view.cpitem.customerarticle.activities') {
                if ($scope.customerArticles.length > 0) {
                    $state.go('customer.courseprograms.item.view.cpitem.customerarticle.activities', { customerArticleId: $scope.customerArticles[0].Id }, { location: 'replace' });
                }
            }
        });
    }
]);