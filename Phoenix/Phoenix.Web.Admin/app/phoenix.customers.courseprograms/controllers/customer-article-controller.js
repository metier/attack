﻿angular.module('phoenix.customers').controller('CustomerArticleCtrl', [
    '$scope',
    '$state',
    '$modal',
    'phoenix.config',
    'data',
    'customerArticles',
    'codes',
    function ($scope, $state, $modal, config, data, customerArticles, codes) {
        'use strict';
        var self = this;
        
        self.init = function (data) {
            $scope.customerArticle = data.customerArticle;
            $scope.currencyCode = self.getCurrencyCode($scope.customerArticle.CurrencyCodeId);
            $scope.customerArticleArticle = data.customerArticleArticle;
            $scope.selectedEducationalPartner = { Id: $scope.customerArticle.EducationalPartnerId };
        };

        self.getCurrencyCode = function (codeId) {
            var currency = _(codes.currencyCodes).find(function (c) { return c.Id === codeId; });
            if (currency) {
                return currency.Value;
            }
            return '';
        };


        $scope.showCustomInfoModal = function () {
            $scope.isBusyCreating = true;

            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.customers.courseprograms/views/modals/customer-article-custom-information-modal.html',
                controller: 'CustomerArticleCustomInformationModalCtrl',
                resolve: {
                    customerArticle: function () { return $scope.customerArticle; }
                },
                windowClass: 'modal-wider'
            });
            //modalInstance.result.then(function () {
            //    $state.go('customer.courseprograms.item.view.cpitem.customerarticle.activities', null, { reload: true });
            //});
            modalInstance.opened.then(function () {
                $scope.isBusyCreating = false;
            });
        };

        $scope.showEnrollmentConditionsModal = function () {
            $scope.isBusyCreating = true;

            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.customers.courseprograms/views/modals/customer-article-enrollment-conditions-modal.html',
                controller: 'CustomerArticleEnrollmentConditionsModalCtrl',
                resolve: {
                    customerArticle: function () { return $scope.customerArticle; }
                },
                windowClass: 'modal-wider'
            });
            modalInstance.opened.then(function () {
                $scope.isBusyCreating = false;
            });
        };



        $scope.edit = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.customers.courseprograms/views/modals/customer-article-edit-modal.html',
                controller: 'CustomerArticleEditModalCtrl',
                resolve: {
                    articleId: function () { return $scope.customerArticle.ArticleCopyId; },
                    customerArticle: function () { return angular.copy($scope.customerArticle);}
                },
                windowClass: 'modal-wider'
            });
            modalInstance.result.then(function (editedCustomerArticle) {
                var indexOfEditCustomerArticle = customerArticles.indexOf($scope.customerArticle);
                if (indexOfEditCustomerArticle > -1) {
                    customerArticles[indexOfEditCustomerArticle] = editedCustomerArticle;
                }
                self.init(editedCustomerArticle);
            });
        };
        
        self.init(data);
    }
]);