﻿angular.module('phoenix.customers').controller('CourseProgramsCtrl', [
    '$scope',
    '$state',
    '$location',
    'CourseProgramsService',
    'coursePrograms',
    function ($scope, $state, $location, courseProgramsService, coursePrograms) {
        'use strict';
        var self = this;
        $scope.coursePrograms = coursePrograms;
        $scope.selectedCourseProgram = null;
        
        $scope.select = function (courseProgram) {
            if (!courseProgram.Id) {
                return;
            }
            if (courseProgram) {
                $state.go('customer.courseprograms.item.view', { courseProgramId: courseProgram.Id });
            } else {
                $state.go('customer.courseprograms');
            }
        };
        
        $scope.isSelected = function (courseProgram) {
            if ($scope.isCreating() && !courseProgram.Id) {
                return !$state.params.courseProgramId;
            }
            return $state.params.courseProgramId === String(courseProgram.Id);
        };
        
        $scope.isCreating = function () {
            return $state.is('customer.courseprograms.create');
        };
        
        $scope.deleteCourseProgram = function (courseProgram) {
            if (confirm('Are you sure you want to delete the course program?')) {
                if (courseProgram.Id) {
                    courseProgram.IsDeleted = true;
                    courseProgramsService.update(courseProgram).then(function () {
                        self.removeCourseProgram(courseProgram);
                    });
                } else {
                    self.removeCourseProgram(courseProgram);
                }
            }
        };

        self.removeCourseProgram = function (courseProgram) {
            var index = $scope.coursePrograms.indexOf(courseProgram);
            if (index > -1) {
                $scope.coursePrograms.splice(index, 1);
            }
            if ($scope.coursePrograms.length > 0) {
                $scope.select($scope.coursePrograms[0]);
            } else {
                $scope.select(null);
            }
        };

        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState) {
            if (toState.name === 'customer.courseprograms' && fromState.name !== 'customer.courseprograms.item.view') {
                if ($scope.coursePrograms.length > 0) {
                    $state.go('customer.courseprograms.item.view', { courseProgramId: $scope.coursePrograms[0].Id });
                }
            }
        });
    }
]);