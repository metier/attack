﻿angular.module('phoenix.customers').controller('CourseProgramViewCtrl', [
    '$scope',
    '$state',
    'coursePrograms',
    'courseProgram',
    'educationalPartners',
    'CourseProgramsService',
    function ($scope, $state, coursePrograms, courseProgram, educationalPartners, courseProgramsService) {
        'use strict';
        $scope.courseProgram = angular.copy(courseProgram);
        $scope.selectedEducationalPartner = { Id: $scope.courseProgram.EducationalPartnerResourceId };
        
        $scope.isSelected = function (item, type) {
            return $state.params.type === type && $state.params.itemId === String(item.Id);
        };

        $scope.select = function (item, type) {
            // Do not navigate if we're already here
            if (item.Id === parseInt($state.params.itemId, 10) && type === $state.params.type) {
                return;
            }
            // Navigate back to self before proceeding
            $state.go('customer.courseprograms.item.view').then(function () {
                $state.go('customer.courseprograms.item.view.cpitem', { itemId: item.Id, type: type });
            });
        };

        $scope.save = function () {
            $scope.isBusy = true;
            $scope.courseProgram.EducationalPartnerResourceId = ($scope.selectedEducationalPartner ? $scope.selectedEducationalPartner.Id : null);

            courseProgramsService.update($scope.courseProgram).then(function () {
                $state.go($state.current, $state.current.params, { reload: true });
                $scope.isBusy = false;
            }, function () {
                $scope.isBusy = false;
            });
        };
    }
]);