﻿angular.module('phoenix.customers').controller('CustomerArticleCreateModalCtrl', [
    '$scope',
    '$modalInstance',
    'customerId',
    'itemId',
    'itemType',
    function($scope, $modalInstance, customerId, itemId, itemType) {
        'use strict';
        $scope.selectedArticleId = null;
        $scope.state = 0;
        $scope.customerId = customerId;
        $scope.itemId = itemId;
        $scope.itemType = itemType;
        
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
        
        $scope.onArticleSelected = function (articleId) {
            $scope.selectedArticleId = articleId;
        };
        
        $scope.next = function () {
            $scope.state += 1;
        };
        
        $scope.previous = function () {
            $scope.state -= 1;
        };
        
        $scope.$on('CustomerArticleCreated', function (event, result) {
            $modalInstance.close(result);
        });
    }
]);