﻿angular.module('phoenix.customers').controller('CustomerArticleEnrollmentConditionsModalCtrl', [
    '$scope',
    '$modalInstance',
    'customerArticle',
    function ($scope, $modalInstance, customerArticle) {
        'use strict';

        $scope.customerArticle = customerArticle;

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    }
]);