﻿angular.module('phoenix.customers').controller('CustomerArticleEditModalCtrl', [
    '$scope',
    '$modalInstance',
    'articleId',
    'customerArticle',
    function ($scope, $modalInstance, articleId, customerArticle) {
        'use strict';
        $scope.selectedArticleId = articleId;
        $scope.customerArticle = customerArticle;
        
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
        
        $scope.$on('CustomerArticleUpdated', function (event, result) {
            $modalInstance.close(result);
        });
    }
]);