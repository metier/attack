﻿angular.module('phoenix.customers').controller('CustomerArticleCustomInformationModalCtrl', [
    '$scope',
    '$modalInstance',
    'customerArticle',
    function ($scope, $modalInstance, customerArticle) {
        'use strict';

        $scope.customerArticle = customerArticle;

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.save = function () {
            $scope.isSaving = true;
            $scope.$broadcast('saveActivity');
        };

        //$scope.$on('ActivitySaved', function (event, createdActivity) {
        //    $scope.isSaving = false;
        //    $modalInstance.close(createdActivity);
        //});

        //$scope.$on('ActivitySaveFailed', function () {
        //    $scope.isSaving = false;
        //});

    }
]);