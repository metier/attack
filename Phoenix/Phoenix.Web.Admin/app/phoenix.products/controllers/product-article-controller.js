﻿angular.module('phoenix.products').controller('ProductArticleCtrl', [
    '$scope',
    '$stateParams',
    '$modal',
    'phoenix.config',
    'RcosService',
    'customerArticles',
    function ($scope, $stateParams, $modal, config, rcosService, customerArticles) {
        'use strict';
        $scope.articleId = parseInt($stateParams.articleId, 10);
        $scope.productId = parseInt($stateParams.productId, 10);
        $scope.customerId = parseInt($stateParams.customerId, 10);
        $scope.customerArticles = customerArticles;
        
        $scope.$on('articleLoaded', function (event, article) {
            $scope.article = article;
            $scope.isMockexam = article.ArticleTypeId === 8;
        });

        $scope.openRcoCourseInfoModal = function (rcoCourseId) {
            $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.products/views/modals/rco-course-info-modal.html',
                controller: 'RcoCourseInfoModalCtrl',
                resolve: {
                    rcoCourse: function () {
                        return rcosService.getCourse(rcoCourseId);
                    }
                },
                windowClass: 'modal-wide'
            });
        };

        $scope.openRcoExamInfoModal = function (rcoExamId) {
            $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.products/views/modals/rco-exam-info-modal.html',
                controller: 'RcoExamInfoModalCtrl',
                resolve: {
                    rcoExam: function () {
                        return rcosService.getExam(rcoExamId);
                    }
                },
                windowClass: 'modal-wide'
            });
        };
    }]
);