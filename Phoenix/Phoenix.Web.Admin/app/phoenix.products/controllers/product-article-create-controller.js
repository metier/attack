﻿/*global angular*/
/*jslint maxlen: 180 */
angular.module('phoenix.products').controller('ProductArticleCreateCtrl', ['$scope', '$stateParams', '$location', '$window', function ($scope, $stateParams, $location, $window) {
    'use strict';
    $scope.articleTypeId = parseInt($stateParams.articleTypeId, 10);
    $scope.productId = parseInt($stateParams.productId, 10);
    
    $scope.article = {
        ArticleTypeId: $scope.articleTypeId,
        ProductId: $scope.articleTypeId,
        Attachments: []
    };
    
    $scope.onSave = function (articleId) {
        $location.replace();
        $location.path('/products/' + $scope.productId + '/articles/' + articleId);
    };
    
    $scope.onCancel = function () {
        $window.history.back();
    };
}]);