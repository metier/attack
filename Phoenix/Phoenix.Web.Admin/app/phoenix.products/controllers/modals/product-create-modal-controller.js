﻿angular.module('phoenix.products').controller('ProductCreateModalCtrl', [
    '$scope',
    '$modalInstance',
    'editProduct',
    'productTypes',
    'productCategories',
    'ProductsService',
    function ($scope, $modalInstance, editProduct, productTypes, productCategories, productsService) {
        'use strict';
        $scope.isEditMode = editProduct !== undefined;
        $scope.product = editProduct || { IsActive: true };
        $scope.productCategories = productCategories;
        $scope.productTypes = productTypes;

        $scope.getCategoryFriendlyName = function (category) {
            return category.Name + ' (' + category.ProductCodePrefix + ')';
        };

        $scope.create = function () {
            $scope.isSaving = true;
            productsService.create($scope.product).then(function (created) {
                $modalInstance.close(created);
            }).finally(function () {
                $scope.isSaving = false;
            });
        };

        $scope.save = function () {
            $scope.isSaving = true;
            productsService.update($scope.product).then(function (updated) {
                $modalInstance.close(updated);
            }).finally(function () {
                $scope.isSaving = false;
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }
]);