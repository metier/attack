﻿angular.module('phoenix.products').controller('RcoCourseInfoModalCtrl', [
    '$scope',
    '$modalInstance',
    'rcoCourse',
    function ($scope, $modalInstance, rcoCourse) {
        'use strict';
        $scope.rcoCourse = rcoCourse;

        $scope.close = function () {
            $modalInstance.dismiss('cancel');
        };
    }]
);