﻿angular.module('phoenix.products').controller('RcoCourseSelectModalCtrl', [
    '$scope',
    '$modalInstance',
    'RcosService',
    function ($scope, $modalInstance, rcosService) {
    'use strict';
    var self = this;
    self.initialData = null;

    $scope.locals = {
        query: '',
        limit: 10,
        isInitialized: false,
        lastSearchQuery: '',
        selectedCourse: null
    };
    
    $scope.search = function () {
        if (!$scope.locals.query) {
            return;
        }
        $scope.isSearching = true;
        $scope.locals.lastSearchQuery = $scope.locals.query;
        $scope.locals.selectedCourse = null;
        rcosService.getCourses({ query: $scope.locals.query, limit: $scope.locals.limit, page: 1 }).then(function (result) {
            $scope.isSearching = false;
            $scope.courses = result;
        }, function () {
            $scope.isSearching = false;
        });
    };

    $scope.navigate = function (page) {
        rcosService.getCourses({ query: $scope.locals.query, limit: $scope.locals.limit, page: page }).then(function (result) {
            $scope.courses = result;
        });
    };

    $scope.onQueryChanged = function () {
        if ($scope.locals.query === '') {
            $scope.courses = self.initialData;
        }
    };

    $scope.select = function (course) {
        if ($scope.locals.selectedCourse && course.Id === $scope.locals.selectedCourse.Id) {
            $scope.locals.selectedCourse = null;
        } else {
            $scope.locals.selectedCourse = course;
        }
    };

    self.init = function () {
        $scope.isSearching = true;
        rcosService.getCourses({ query: '', limit: $scope.locals.limit, page: 1 }).then(function (result) {
            $scope.locals.isInitialized = true;
            self.initialData = result;
            $scope.courses = result;
            $scope.isSearching = false;
        }, function() {
            $scope.isSearching = false;
        });
    };

    $scope.ok = function () {
        $modalInstance.close($scope.locals.selectedCourse);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    self.init();
}]);