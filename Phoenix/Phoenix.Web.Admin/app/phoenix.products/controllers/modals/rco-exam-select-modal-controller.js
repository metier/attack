﻿angular.module('phoenix.products').controller('RcoExamSelectModalCtrl', [
    '$scope',
    '$modalInstance',
    'RcosService',
    function ($scope, $modalInstance, rcosService) {
    'use strict';
    var self = this;
    self.initialData = null;

    $scope.locals = {
        query: '',
        limit: 10,
        isInitialized: false,
        lastSearchQuery: '',
        selectedExam: null
    };
    
    $scope.search = function () {
        if (!$scope.locals.query) {
            return;
        }
        $scope.isSearching = true;
        $scope.locals.lastSearchQuery = $scope.locals.query;
        $scope.locals.selectedExam = null;
        rcosService.getExams({ query: $scope.locals.query, limit: $scope.locals.limit, page: 1 }).then(function (result) {
            $scope.isSearching = false;
            $scope.exams = result;
        }, function () {
            $scope.isSearching = false;
        });
    };

    $scope.navigate = function (page) {
        rcosService.getExams({ query: $scope.locals.query, limit: $scope.locals.limit, page: page }).then(function (result) {
            $scope.exams = result;
        });
    };

    $scope.onQueryChanged = function () {
        if ($scope.locals.query === '') {
            $scope.exams = self.initialData;
        }
    };

    $scope.select = function (exam) {
        if ($scope.locals.selectedExam && exam.Id === $scope.locals.selectedExam.Id) {
            $scope.locals.selectedExam = null;
        } else {
            $scope.locals.selectedExam = exam;
        }
    };

    self.init = function () {
        $scope.isSearching = true;
        rcosService.getExams({ query: '', limit: $scope.locals.limit, page: 1 }).then(function (result) {
            $scope.locals.isInitialized = true;
            self.initialData = result;
            $scope.exams = result;
            $scope.isSearching = false;
        }, function() {
            $scope.isSearching = false;
        });
    };

    $scope.ok = function () {
        $modalInstance.close($scope.locals.selectedExam);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    self.init();
}]);