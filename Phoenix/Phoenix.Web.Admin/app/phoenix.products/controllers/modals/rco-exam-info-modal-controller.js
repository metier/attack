﻿angular.module('phoenix.products').controller('RcoExamInfoModalCtrl', [
    '$scope',
    '$modalInstance',
    'rcoExam',
    function ($scope, $modalInstance, rcoExam) {
        'use strict';
        $scope.rcoExam = rcoExam;

        $scope.close = function () {
            $modalInstance.dismiss('cancel');
        };
    }]
);