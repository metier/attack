﻿angular.module('phoenix.products').controller('ProductListCtrl', [
    '$scope',
    '$state',
    '$modal',
    'allProducts',
    'phoenix.config',
    'ProductsService',
    'NotificationService',
    'productTypes',
    'productCategories',
    function ($scope, $state, $modal, allProducts, config, productsService, notificationService, productTypes, productCategories) {
        'use strict';
        var self = this;
        $scope.productTypes = productTypes;
        $scope.allProducts = allProducts;
        $scope.activeProducts = _(allProducts).filter(function (p) { return p.IsActive; });
        $scope.inactiveProducts = _(allProducts).filter(function (p) { return !p.IsActive; });
        $scope.productCategories = productCategories;

        self.showCreateProductDialog = function (editProduct) {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.products/views/modals/product-create-modal.html',
                controller: 'ProductCreateModalCtrl',
                resolve: {
                    editProduct: function () { return angular.copy(editProduct); },
                    productTypes: function () { return $scope.productTypes; },
                    productCategories: function () { return $scope.productCategories; }
                }
            });

            modalInstance.result.then(function () {
                $state.go('productlist.active', null, { reload: true });
            });
        };

        $scope.createProduct = function () {
            self.showCreateProductDialog();
        };

        $scope.editProduct = function (product) {
            self.showCreateProductDialog(product);
        };
    }
]);