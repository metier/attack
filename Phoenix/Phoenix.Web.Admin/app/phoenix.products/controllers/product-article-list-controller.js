﻿/*global angular*/
angular.module('phoenix.products').controller('ProductArticleListCtrl', ['$scope', '$location', '$modal', 'phoenix.config', 'product', 'articles', function ($scope, $location, $modal, config, product, articles) {
    'use strict';
    var self = this;
    $scope.product = product;
    $scope.articles = articles || { TotalCount: 0, Items: [] };
    $scope.orderBy = $location.search().orderBy || '';
    $scope.orderDirection = $location.search().orderDirection || '';

    $scope.page = $location.search().page;
    $scope.limit = parseInt($location.search().limit, 10) || 15;
    $scope.maxPages = $scope.articles.TotalCount;

    $scope.navigate = function (page) {
        if (page < 1 || page > $scope.maxPages) {
            return;
        }
        $location.search('page', page);
    };

    $scope.createArticle = function (articleTypeId) {
        $location.path('/products/' + $scope.product.Id + '/articles/create/' + articleTypeId);
    };

    self.clearQueryString = function () {
        $location.search('page', null);
        $location.search('orderBy', null);
        $location.search('orderDirection', null);
    };
}]);