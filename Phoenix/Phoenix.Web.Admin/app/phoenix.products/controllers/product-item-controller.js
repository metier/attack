﻿angular.module('phoenix.products').controller('ProductItemCtrl', ['$scope', '$state', 'product', function ($scope, $state, product) {
    'use strict';
    $scope.product = product;
    
    $scope.isActive = function (state, absolute) {
        if (absolute) {
            return $state.current.name === state;
        }
        return $state.current.name.indexOf(state) === 0;
    };
}]);