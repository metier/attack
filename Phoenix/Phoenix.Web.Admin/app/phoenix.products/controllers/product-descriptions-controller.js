﻿angular.module('phoenix.products').controller('ProductDescriptionsCtrl', ['$scope', '$rootScope', '$location', 'product', 'productDescriptionsSimple', function ($scope, $rootScope, $location, product, productDescriptionsSimple) {
    'use strict';
    var self = this;
    
    $scope.product = product;
    self.productDescriptionsSimple = productDescriptionsSimple;
    
    $scope.isSelectedCustomer = function (customerId) {
        return $scope.selectedCustomer === customerId;
    };

    $scope.selectCustomer = function (customerId) {
        $scope.selectedCustomer = customerId;
        $location.search('customerId', customerId);
    };

    $scope.onLanguageSelected = function () {
        return;
    };
    
    $scope.onCustomerCopyCreated = function (customer) {
        var customerInTab = _($scope.customers).find(function (c) {
            return c.customerId === customer.Id;
        });
        if (!customerInTab) {
            customerInTab = {
                customerName: customer.Name,
                customerId: customer.Id
            };
            $scope.customers.push(customerInTab);
        }
        $scope.selectCustomer(customerInTab.customerId);
    };
    
    self.getCustomerList = function (productDescriptionsSimple) {
        var mappedArray, customerList, generic;
        mappedArray = _.map(productDescriptionsSimple, function (productDescription) {
            return {
                customerName: productDescription.CustomerName,
                customerId: productDescription.CustomerId
            };
        });
        customerList = _.uniq(mappedArray, false, function (customerElement) {
            return customerElement.customerId;
        });
        
        generic = _(customerList).find(function (customer) {
            return customer.customerId === null;
        });
        if (generic) {
            customerList.splice(customerList.indexOf(generic), 1);
            customerList = _(customerList).sortBy(function (item) { return item.customerName; });
        } else {
            generic = {
                customerId: null
            };
        }
        customerList.unshift(generic);
        
        return customerList;
    };

    self.navigateFromQueryString = function () {
        var customerId;
        
        customerId = parseInt($location.search().customerId, 10) || $scope.customers[0].customerId;
        $scope.selectedCustomer = customerId;
    };

    self.initialize = function () {
        $scope.customers = self.getCustomerList(self.productDescriptionsSimple);
        self.navigateFromQueryString();
    };

    self.initialize();
}]);