﻿angular.module('phoenix.products').controller('ProductListTableCtrl', [
    '$scope',
    'products',
    function ($scope, products) {
        'use strict';
        $scope.products = products;
    }
]);