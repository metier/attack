﻿/*global angular */
angular.module('phoenix.products').factory('ProductDescriptionsService', [
    '$http',
    '$log',
    'phoenix.config',
    'asyncHelper',
    '$q',
    function ($http, $log, config, asyncHelper, $q) {
        'use strict';
        return {
            search: function (options) {
                var url = config.apiPath + 'productdescriptions',
                    parameters;

                parameters = {
                    query: options.query,
                    sortBy: options.sortBy,
                    sortDirection: options.sortDirection,
                    skip: 0,
                    limit: 20
                };

                if (typeof options.limit === 'number') {
                    parameters.limit = options.limit;
                }
                if (typeof options.page === 'number' && options.page > 0) {
                    parameters.skip = (options.page - 1) * parameters.limit;
                }

                return asyncHelper.resolveHttp($http({ method: 'GET', url: url, params: parameters }));
            },
            get: function (id, withoutReferences, includeDeleted) {
                var url = config.apiPath + 'productdescriptions/' + id,
                    parameters;
                parameters = {
                    withoutReferences: withoutReferences,
                    includeDeleted: includeDeleted
                };
                return asyncHelper.resolveHttp($http.get(url, { params: parameters }), true);
            },
            getProductDescriptions: function (options) {
                if (!options.productId) {
                    throw { name: 'Product description service error', message: 'ProductId must be specified' };
                }

                var parameters = {
                    productId: options.productId,
                    customerId: options.customerId,
                    genericOnly: options.genericOnly || false,
                    simple: options.simple || false
                },
                url = config.apiPath + 'productdescriptions';
            
                return asyncHelper.resolveHttp($http.get(url, { params: parameters }));
            },
            update: function (productDescription) {
                var url = config.apiPath + 'productdescriptions/' + productDescription.Id,
                    deferred = $q.defer();
                $http.put(url, productDescription).success(function (result) {
                    deferred.resolve(result);
                }).error(function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            },
            create: function (productDescription) {
                var url = config.apiPath + 'productdescriptions/', 
                    deferred = $q.defer();
                $http.post(url, productDescription).success(function(result) {
                    deferred.resolve(result);
                }).error(function(error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            }
        };
}]);