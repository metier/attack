﻿/*global angular */
angular.module('phoenix.products').factory('ArticlesService', [
    '$http',
    'phoenix.config',
    'asyncHelper',
    function($http, config, asyncHelper) {
        'use strict';

        return {
            getArticles: function(options) {
                var parameters = {
                        status: options.status,
                        query: options.query,
                        productId: options.productId,
                        orderBy: options.orderBy,
                        orderDirection: options.orderDirection
                    },
                    url = config.apiPath + 'articles';

                if (typeof options.limit === 'number') {
                    parameters.limit = options.limit;
                }
                if (typeof options.page === 'number' && options.page > 0) {
                    parameters.skip = (options.page - 1) * parameters.limit;
                }

                return asyncHelper.resolveHttp($http.get(url, { params: parameters }));
            },
            create: function(article) {
                var url = config.apiPath + 'articles';
                return asyncHelper.resolveHttp($http.post(url, article));
            },
            update: function(article) {
                var url = config.apiPath + 'articles/' + article.Id;
                return asyncHelper.resolveHttp($http.put(url, article));
            },
            get: function(articleId) {
                var url = config.apiPath + 'articles/' + articleId;
                return asyncHelper.resolveHttp($http.get(url));
            },
            getArticletype: function(articletypeId) {
                var url = config.apiPath + 'articletypes/' + articletypeId;
                return asyncHelper.resolveHttp($http.get(url));
            },
            searchArticletypes: function(searchFilter) {
                var parameters = {
                    query: searchFilter
                };

                return asyncHelper.resolveHttp($http.get(config.apiPath + 'articletypes', { params: parameters }));
            }
        };
    }
]);