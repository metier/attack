﻿angular.module('phoenix.products').factory('RcosService', [
    '$http',
    'phoenix.config',
    'asyncHelper',
    function ($http, config, asyncHelper) {
        'use strict';
        return {
            search: function(options) {
                var parameters = {
                    query: options.query,
                    rcoIds: options.rcoIds,
                    orderBy: options.orderBy,
                    orderDirection: options.orderDirection
                },
                url = config.apiPath + 'rcos';

                if (typeof options.limit === 'number') {
                    parameters.limit = options.limit;
                }
                if (typeof options.page === 'number' && options.page > 0) {
                    parameters.skip = (options.page - 1) * parameters.limit;
                }

                return asyncHelper.resolveHttp($http.get(url, { params: parameters }), false);
            },
            getCourses: function (options) {
                var parameters = {
                    query: options.query,
                    orderBy: options.orderBy,
                    orderDirection: options.orderDirection
                },
                url = config.apiPath + 'rcos/courses';  
            
                if (typeof options.limit === 'number') {
                    parameters.limit = options.limit;
                }
                if (typeof options.page === 'number' && options.page > 0) {
                    parameters.skip = (options.page - 1) * parameters.limit;
                }

                return asyncHelper.resolveHttp($http.get(url, { params: parameters }), false);
            },
            getExams: function (options) {
                var parameters = {
                    query: options.query,
                    orderBy: options.orderBy,
                    orderDirection: options.orderDirection
                },
                url = config.apiPath + 'rcos/exams';  
            
                if (typeof options.limit === 'number') {
                    parameters.limit = options.limit;
                }
                if (typeof options.page === 'number' && options.page > 0) {
                    parameters.skip = (options.page - 1) * parameters.limit;
                }

                return asyncHelper.resolveHttp($http.get(url, { params: parameters }), false);
            },
            getCourse: function (courseId, redirectOn404) {
                if (redirectOn404 !== false) {
                    redirectOn404 = true;
                }
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'rcos/courses/' + courseId), redirectOn404);
            },
            getExam: function (examId, redirectOn404) {
                if (redirectOn404 !== false) {
                    redirectOn404 = true;
                }
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'rcos/exams/' + examId), redirectOn404);
            }
        };
}]);