/*global angular */
angular.module('phoenix.products').factory('ProductsService', [
    '$http',
    '$q',
    'phoenix.config',
    'asyncHelper',
    function ($http, $q, config, asyncHelper) {
        'use strict';
        return {
            getAllProcuctCategories: function () {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'productcategories'));
            },
            getAllProcuctTypes: function () {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'producttypes'));
            },
            getAll: function () {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'products'));
            },
            getAllActive: function () {
                return this.getAll().then(function (products) {
                    return _(products).filter(function (p) { return p.IsActive; });
                });
            },
            search: function (searchFilter) {
                var parameters = {
                    query: searchFilter
                };

                return asyncHelper.resolveHttp($http.get(config.apiPath + 'products', { params: parameters }));
            },
            create: function (product) {
                return asyncHelper.resolveHttp($http.post(config.apiPath + 'products', product));
            },
            update: function (product) {
                return asyncHelper.resolveHttp($http.put(config.apiPath + 'products/' + product.Id, product));
            },
            get: function (productId) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'products/' + productId), true);
            },
            getProductType: function (productId) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'products/' + productId + '/producttype'));
            }
        };
}]);