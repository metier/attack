﻿/*global angular */
/*jslint maxlen: 150 */
angular.module('phoenix.products').filter('articleTypeName', [function () {
    'use strict';
    return function (articleTypeId) {
        if (articleTypeId === 1) {
            return 'E-Learning';
        }
        if (articleTypeId === 2) {
            return 'Classroom';
        }
        if (articleTypeId === 3) {
            return 'Multiple Choice';
        }
        if (articleTypeId === 4) {
            return 'Case Exam';
        }
        if (articleTypeId === 5) {
            return 'Project Assignment';
        }
        if (articleTypeId === 6) {
            return 'Internal Certification';
        }
        if (articleTypeId === 7) {
            return 'Other';
        }
        if (articleTypeId === 8) {
            return 'Mock Exam';
        }
        if (articleTypeId === 9) {
            return 'External Certification';
        }
        if (articleTypeId === 10) {
            return 'Seminar';
        }
        if (articleTypeId === 11) {
            return 'Webinar';
        }
        return '';
    };
}]);