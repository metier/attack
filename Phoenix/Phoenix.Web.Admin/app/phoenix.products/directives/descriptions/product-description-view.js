﻿/*global angular*/
/*jslint maxlen: 150 */
angular.module('phoenix.products').directive('productDescriptionView', ['phoenix.config', function (config) {
    'use strict';
    return {
        restrict: 'E',
        scope: {
            productDescription: '=',
            inShortDescriptionOnly: '@shortDescriptionOnly',
            inCanToggleDetails: '@canToggleDetails',
            inShowImage: '@showImage'
        },
        controller: 'ProductDescriptionViewDirectiveCtrl',
        templateUrl: config.basePath + 'app/phoenix.products/directives/descriptions/product-description-view.html',
        compile: function (element, attrs) {
            if (!attrs.shortDescriptionOnly) { attrs.shortDescriptionOnly = 'false'; }
            if (!attrs.canToggleDetails) { attrs.canToggleDetails = 'false'; }
            if (!attrs.showImage) { attrs.showImage = 'true'; }
        }
    };
}]);