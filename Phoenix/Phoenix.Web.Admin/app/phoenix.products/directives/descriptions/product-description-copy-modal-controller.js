﻿/*global angular */
/*jslint maxlen: 170 */
angular.module('phoenix.products').controller('ProductDescriptionCopyModalCtrl', ['$scope',
    '$modalInstance', 'CustomersService', function ($scope, $modalInstance, customersService) {
        'use strict';
        $scope.locals = {};
        $scope.searchCustomer = function (searchString) {
            return customersService.search({ query: searchString, limit: 50 }).then(function (result) {
                return result.Items;
            });
        };

        $scope.onCustomerSelected = function (customer) {
            $scope.locals.userSelectedCustomer = customer;
        };

        $scope.setValidSelectedCustomer = function () {
            if ($scope.locals.selectedCustomer) {
                $scope.locals.selectedCustomer = $scope.locals.userSelectedCustomer;
            } else {
                $scope.locals.selectedCustomer = null;
                $scope.locals.userSelectedCustomer = null;
            }
        };

        $scope.ok = function (selectedCustomer) {
            $modalInstance.close(selectedCustomer);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);