﻿/*global angular */
/*jslint maxlen: 170 */
angular.module('phoenix.products').controller('SelectProductDescriptionModalCtrl', ['$scope',
    '$modalInstance', 'options', 'ProductDescriptionsService', '$q', function ($scope, $modalInstance, options, productDescriptionsService, $q) {
        'use strict';

        if (options.customerId) {
            $scope.isCustomerContext = true;

            var promises = [
                productDescriptionsService.getProductDescriptions({
                    productId: options.productId,
                    simple: false,
                    genericOnly: options.genericOnly
                }),
                productDescriptionsService.getProductDescriptions({
                    productId: options.productId,
                    customerId: options.customerId,
                    simple: false
                })
            ];

            $q.all(promises).then(function (results) {
                _(results[1]).each(function (item) {
                    item.isGeneric = true;
                });
                $scope.productDescriptions = results[0].concat(results[1]);
            });
        } else {
            productDescriptionsService.getProductDescriptions({
                productId: options.productId,
                simple: false,
                genericOnly: options.genericOnly
            }).then(function (result) {
                $scope.productDescriptions = result;
            });
        }
        
        $scope.ok = function (productDescriptionId) {
            $modalInstance.close(productDescriptionId);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);