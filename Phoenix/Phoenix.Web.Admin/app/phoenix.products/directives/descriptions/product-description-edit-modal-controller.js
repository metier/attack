﻿/*global angular */
/*jslint maxlen: 250 */
angular.module('phoenix.products').controller('ProductDescriptionEditModalCtrl', ['$scope', '$modalInstance', 'productDescriptionId', 'productId', function ($scope, $modalInstance, productDescriptionId, productId) {
    'use strict';
    $scope.productDescriptionId = productDescriptionId;
    $scope.productId = productId;

    $scope.saved = function () {
        $modalInstance.close();
    };

    $scope.cancelled = function () {
        $modalInstance.dismiss('cancel');
    };
}]);