﻿/*global angular*/
/*jslint maxlen: 150 */
angular.module('phoenix.products').directive('productDescriptionSummary', ['phoenix.config', function (config) {
    'use strict';
    return {
        restrict: 'E',
        scope: {
            inProductDescriptionId: '=productDescriptionId',
            productId: '=',
            customerId: '=',
            isEditable: '=',
            isChangeable: '=',
            onProductDescriptionChanged: '&'
        },
        controller: 'ProductDescriptionSummaryDirectiveCtrl',
        templateUrl: config.basePath + 'app/phoenix.products/directives/descriptions/product-description-summary.html'
    };
}]);