﻿angular.module('phoenix.products').controller('ProductDescriptionViewDirectiveCtrl', [
    '$scope',
    function ($scope) {
        'use strict';
        var self = this;
        $scope.canToggleDetails = $scope.inCanToggleDetails === 'true';
        $scope.shortDescriptionOnly = $scope.inShortDescriptionOnly === 'true';
        $scope.showImage = $scope.inShowImage === 'true';

        self.getShortDescription = function (productDescription) {
            return _(productDescription.ProductDescriptionTexts).find(function (item) {
                return item.ProductTemplatePart.Order === 1;
            });
        };

        self.init = function () {
            $scope.shortDescription = self.getShortDescription($scope.productDescription);
        };

        $scope.showDetails = function () {
            $scope.shortDescriptionOnly = false;
        };

        $scope.hideDetails = function () {
            $scope.shortDescriptionOnly = true;
        };

        $scope.$watch('productDescription', function () {
            if ($scope.productDescription) {
                self.init();
            }
        });
    }
]);