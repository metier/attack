﻿angular.module('phoenix.products').controller('ProductDescriptionCreateDirectiveCtrl', ['$scope',
    'ProductDescriptionsService', 'ProductsService', 'phoenix.config', '$q', 'NotificationService', 'LanguagesService', function ($scope, productDescriptionsService, productsService, config, $q, notificationService, languagesService) {
    'use strict';
    var self = this;
    $scope.isInitialized = false;
    $scope.isEditMode = false;
    $scope.isBusy = false;
    
    self.changeTemplates = function (productDescription) {
        var templates = self.getTemplates(productDescription.LanguageId);
        _(templates).each(function (template) {
            var productDescriptionText, productDescriptionTexts;
            productDescriptionTexts = _($scope.productDescription.ProductDescriptionTexts).filter(function (pdt) {
                return pdt.ProductTemplatePart.Order === template.Order;
            });
            if (productDescriptionTexts.length > 0) {
                productDescriptionText = productDescriptionTexts[0];
                productDescriptionText.ProductTemplatePartId = template.Id;
                productDescriptionText.ProductTemplatePart = template;
            } else {
                $scope.productDescription.ProductDescriptionTexts.push(self.getProductDescriptionText(template, productDescription.Id));
            }
        });
    };

    self.getTemplates = function (languageId) {
        var templates = _($scope.productType.ProductTemplateParts).filter(function (template) {

            return template.LanguageId === languageId;
        });
        return _(templates).sortBy(function (template) { return template.Order; });
    };

    self.getProductDescriptionText = function (template, productDescriptionId) {
        return {
            Text: '',
            ProductTemplatePartId: template.Id,
            ProductTemplatePart: template,
            ProductDescriptionId: productDescriptionId
        };
    };
        
    self.setLanguage = function (name) {
        var language = _($scope.languages).find(function (lan) {
            return lan.Identifier === name;
        });
        if (language) {
            $scope.productDescription.LanguageId = language.Id;
        }
        self.changeTemplates($scope.productDescription);
    };

    self.filterLanguagesToTemplates = function (languages) {
        return _(languages).filter(function (language) {
            return _($scope.productType.ProductTemplateParts).some(function (template) {
                return template.LanguageId === language.Id;
            });
        });
    };
        
    self.setProductDescription = function (productDescription) {
        self.originalProductDescription = productDescription;
        $scope.productDescription = angular.copy(productDescription);
        self.changeTemplates(productDescription);
    };

    self.handleError = function () {
        $scope.isBusy = false;
    };

    $scope.cancel = function () {
        self.setProductDescription(self.originalProductDescription);
        $scope.onCancel();
    };

    $scope.save = function () {
        $scope.isBusy = true;
        var saveFunction = $scope.isEditMode ? productDescriptionsService.update : productDescriptionsService.create;
        saveFunction($scope.productDescription).then(function (result) {
            $scope.isBusy = false;
            self.setProductDescription(result);
            $scope.isEditMode = true;
            $scope.onSave({ productDescriptionId: result.Id });
        }, self.handleError);
    };
    
    $scope.languageChanged = function () {
        self.changeTemplates($scope.productDescription);
    };
        
    $scope.onFileUploaded = function (attachment) {
        $scope.productDescription.Attachment = attachment;
        $scope.productDescription.AttachmentId = attachment.Id;
    };
        
    $scope.deleteImage = function () {
        $scope.productDescription.Attachment = null;
        $scope.productDescription.AttachmentId = null;
    };
        
    $scope.$watch('productDescriptionId', function () {
        if (!$scope.isInitialized) {    
            $scope.isInitialized = true;
            $scope.isEditMode = $scope.productDescriptionId !== undefined;
            
            var promises = [
                productsService.getProductType($scope.productId),
                languagesService.getAll()
            ];

            if ($scope.isEditMode) {
                promises.push(productDescriptionsService.get($scope.productDescriptionId, true));
            }

            $q.all(promises).then(function (results) {
                $scope.productType = results[0];
                $scope.languages = self.filterLanguagesToTemplates(results[1]);

                if ($scope.isEditMode) {
                    self.setProductDescription(results[2]);
                } else {
                    self.setProductDescription({
                        ProductDescriptionTexts: [],
                        ProductId: $scope.productId,
                        CustomerId: $scope.customerId
                    });
                    self.setLanguage('EN');
                }
            });          
        }
    });
}]);