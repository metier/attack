﻿angular.module('phoenix.products').controller('ProductDescriptionsDirectiveCtrl', ['$scope',
    '$rootScope', 'ProductDescriptionsService', 'phoenix.config', '$modal', function ($scope, $rootScope, productDescriptionsService, config, $modal) {
    'use strict';
    var self = this;
    
    $scope.isInitialized = false;
    $scope.productDescriptionsInfo = [];
        
    $scope.isSelected = function (productDescriptonInfo) {
        return $scope.selectedProductDescriptionInfo === productDescriptonInfo;
    };
        
    $scope.select = function (productDescriptonInfo) {
        $scope.selectedProductDescriptionInfo = productDescriptonInfo;
        $scope.languageId = $scope.selectedProductDescriptionInfo.productDescription.LanguageId;
        $scope.onLanguageSelected($scope.languageId);
    };

    $scope.edit = function (productDescriptionInfo) {
        productDescriptionInfo.isEditing = true;
    };
        
    $scope.copy = function (productDescriptionInfo) {
        self.showCreateCopyDialog(productDescriptionInfo, $scope);
    };
        
    self.showCreateCopyDialog = function (productDescriptionInfo) {
        var modalInstance = $modal.open({
            backdrop: 'static',
            templateUrl: config.basePath + 'app/phoenix.products/directives/descriptions/product-description-copy-modal.html',
            controller: 'ProductDescriptionCopyModalCtrl',
            resolve: {
            }
        });

        modalInstance.result.then(function (selectedCustomer) {
            var productDescriptionInfoCopy = self.copyProductDescriptionInfo(productDescriptionInfo);
            productDescriptionInfoCopy.productDescription.CustomerId = selectedCustomer.Id;
            productDescriptionsService.create(productDescriptionInfoCopy.productDescription).then(function () {
                $scope.onCustomerCopyCreated(selectedCustomer);
            });            
        });
    };
    
    self.copyProductDescriptionInfo = function (original) {
        var copy = self.createEmptyProductDescriptionInfo();
        copy.productDescription.Title = original.productDescription.Title;
        copy.productDescription.AttachmentId = original.productDescription.AttachmentId;
        copy.productDescription.ProductDescriptionTexts =
            _(original.productDescription.ProductDescriptionTexts).map(function (item) {
                return {
                    ProductTemplatePartId: item.ProductTemplatePartId,
                    Text: item.Text
                };
            });
        copy.productDescription.LanguageId = original.productDescription.LanguageId;
        copy.isEditing = false;
        
        return copy;
    };

    self.remove = function (productDescriptionInfo) {
        $scope.productDescriptionsInfo.splice($scope.productDescriptionsInfo.indexOf(productDescriptionInfo), 1);
        if ($scope.productDescriptionsInfo.length > 0) {
            $scope.select($scope.productDescriptionsInfo[0]);
        }
    };

    $scope.addNewLanguage = function () {
        if ($scope.isInitialized) {
            var productDescription = self.createEmptyProductDescriptionInfo();
            $scope.productDescriptionsInfo.push(productDescription);
            $scope.select(productDescription);
        }
    };
        
    $scope.deleteProductDescription = function (productDescriptionInfo) {
        if (confirm("Are you sure you want to delete the product description?")) {
            if (productDescriptionInfo.productDescription.Id) {
                productDescriptionInfo.productDescription.IsDeleted = true;
                productDescriptionsService.update(productDescriptionInfo.productDescription).then(function () {
                    self.remove(productDescriptionInfo);
                }, function() {
                    productDescriptionInfo.productDescription.IsDeleted = false;
                });
            } else {
                self.remove(productDescriptionInfo);
            }
        }
    };
        
    $scope.saved = function (productDescriptionId) {
        $scope.selectedProductDescriptionInfo.productDescription.Id = productDescriptionId;
        self.updateProductDescriptionInfo($scope.selectedProductDescriptionInfo);
    };

    $scope.cancelled = function () {
        if ($scope.selectedProductDescriptionInfo.productDescription.Id) {
            $scope.selectedProductDescriptionInfo.isEditing = false;
        } else {
            self.remove($scope.selectedProductDescriptionInfo);
        }
    };
        
    self.updateProductDescriptionInfo = function (productDescriptionInfo) {
        var update;

        productDescriptionsService.get(productDescriptionInfo.productDescription.Id).then(function (result) {
            update = _($scope.productDescriptionsInfo).find(function (pdi) {
                return pdi.productDescription.Id === result.Id;
            });
            update.productDescription = result;
            update.isEditing = false;
        });
    };
        
    self.createEmptyProductDescriptionInfo = function () {
        return {
            isEditing: true,
            isSelected: false,
            productDescription: {
                ProductId: $scope.productId,
                CustomerId: $scope.customerId
            }
        };
    };

    self.createProductDescriptionInfo = function (productDescription) {
        return {
            productDescription: productDescription,
            isEditing: false
        };
    };
        
    self.initialize = function (productDescriptions) {
        var selectedPdi;
        
        if (!productDescriptions || productDescriptions.length === 0) {
            $scope.productDescriptionsInfo.push(self.createEmptyProductDescriptionInfo());
        } else {
            $scope.productDescriptionsInfo = _.map(productDescriptions, function (pd) {
                return self.createProductDescriptionInfo(pd);
            });
        }

        selectedPdi = _($scope.productDescriptionsInfo).find(function (pdi) {
            return pdi.productDescription.LanguageId === $scope.languageId;
        }) || $scope.productDescriptionsInfo[0];
        $scope.select(selectedPdi);

        $scope.isInitialized = true;
    };
    
    //initialize when productId resolved in directive
    $scope.$watch('productId', function () {
        if (!$scope.isInitialized) {
            productDescriptionsService.getProductDescriptions({
                productId: $scope.productId,
                customerId: $scope.customerId,
                genericOnly: !$scope.customerId
            }).then(function (result) {
                self.initialize(result);
            });
        }
    });
        
    // Select proper product description if language id changes
    $scope.$watch('languageId', function () {
        if ($scope.isInitialized) {
            if ($scope.languageId && $scope.selectedProductDescriptionInfo.productDescription.LanguageId !== $scope.languageId) {
                var productDescriptionInfo = _($scope.productDescriptionsInfo).find(function (pdi) {
                    return pdi.productDescription.LanguageId === $scope.languageId;
                });
                if (productDescriptionInfo) {    
                    $scope.select(productDescriptionInfo);
                }
            }
        }
    });
}]);