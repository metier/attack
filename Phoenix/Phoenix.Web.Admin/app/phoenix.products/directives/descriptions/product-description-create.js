﻿/*global angular*/
/*jslint maxlen: 150 */
angular.module('phoenix.products').directive('productDescriptionCreate', ['phoenix.config', function (config) {
    'use strict';
    return {
        restrict: 'E',
        scope: {
            productId: '=',
            productDescriptionId: '=',
            customerId: '=',
            onSave: '&',
            onCancel: '&'
        },
        controller: 'ProductDescriptionCreateDirectiveCtrl',
        templateUrl: config.basePath + 'app/phoenix.products/directives/descriptions/product-description-create.html'
    };
}]);