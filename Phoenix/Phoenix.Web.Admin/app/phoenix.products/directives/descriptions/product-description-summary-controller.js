﻿/*global angular, _ */
/*jslint maxlen: 250 */
angular.module('phoenix.products').controller('ProductDescriptionSummaryDirectiveCtrl', [
    '$scope', 
    '$modal', 
    '$state', 
    'ProductDescriptionsService', 
    'phoenix.config', 
    function ($scope, $modal, $state, productDescriptionsService, config) {
        'use strict';
        var self = this;
        $scope.shortDescriptionOnly = true;
        self.getShortDescription = function (productDescription) {
            return _(productDescription.ProductDescriptionTexts).find(function (item) {
                return item.ProductTemplatePart.Order === 1; 
            }).Text;
        };

        self.init = function (productDescriptionId) {
            productDescriptionsService.get(productDescriptionId, true, true).then(function (result) {
                $scope.productDescription = result;
                $scope.productDescriptionId = productDescriptionId;
                $scope.shortDescription = self.getShortDescription($scope.productDescription);
                $scope.onProductDescriptionChanged({ productDescription: result });
            });
        };

        $scope.edit = function (productDescriptionId, productId) {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.products/directives/descriptions/product-description-edit-modal.html',
                controller: 'ProductDescriptionEditModalCtrl',
                resolve: {
                    productDescriptionId: function () { return productDescriptionId; },
                    productId: function () { return productId; }
                },
                windowClass: 'modal-wider'
            });

            modalInstance.result.then(function () {
                self.init($scope.productDescriptionId);
            });
        };
    
        $scope.selectProductDescription = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.products/directives/descriptions/select-product-description-modal.html',
                controller: 'SelectProductDescriptionModalCtrl',
                resolve: {
                    options: function () {
                        return {
                            genericOnly: true,
                            productId: $scope.productId,
                            customerId: $scope.customerId || $state.params.customerId
                        };
                    }
                }
            });

            modalInstance.result.then(function (productDescriptionId) {
                if (productDescriptionId !== $scope.productDescriptionId) {
                    self.init(productDescriptionId);
                }
            });
        };

        $scope.$watch('inProductDescriptionId', function () {
            if ($scope.inProductDescriptionId && $scope.inProductDescriptionId !== $scope.productDescriptionId) {
                self.init($scope.inProductDescriptionId);
            }
        });
    }
]);