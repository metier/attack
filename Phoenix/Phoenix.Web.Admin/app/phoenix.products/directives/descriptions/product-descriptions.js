﻿/*global angular*/
/*jslint maxlen: 150 */
angular.module('phoenix.products').directive('productDescriptions', ['phoenix.config', function (config) {
    'use strict';
    return {
        restrict: 'E',
        scope: {
            productId: '@',
            productTypeName: '@',
            customerId: '@',
            languageId: '=',
            onCustomerCopyCreated: '=',
            onLanguageSelected: '='
        },
        controller: 'ProductDescriptionsDirectiveCtrl',
        templateUrl: config.basePath + 'app/phoenix.products/directives/descriptions/product-descriptions.html'
    };
}]);