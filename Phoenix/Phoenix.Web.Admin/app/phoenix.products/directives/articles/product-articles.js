﻿/*global angular*/
/*jslint maxlen: 150 */
angular.module('phoenix.products').directive('productArticles', ['phoenix.config', function (config) {
    'use strict';
    return {
        restrict: 'E',
        scope: {
            productId: '='
        },
        controller: 'ProductArticlesDirectiveCtrl',
        templateUrl: config.basePath + 'app/phoenix.products/directives/articles/product-articles.html'
    };
}]);