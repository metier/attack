﻿/*global angular*/
/*jslint maxlen: 150 */
angular.module('phoenix.products').directive('productArticle', ['phoenix.config', function (config) {
    'use strict';
    return {
        restrict: 'E',
        scope: {
            productId: '=',
            articleId: '=',
            customerId: '=',
            externalArticle: '=article',
            title: '=',
            onSave: '&',
            onCancel: '&'
        },
        controller: 'ProductArticleDirectiveCtrl',
        templateUrl: config.basePath + 'app/phoenix.products/directives/articles/product-article.html'
    };
}]);