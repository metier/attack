﻿angular.module('phoenix.products').controller('ProductArticleCreateDirectiveCtrl', [
    '$scope',
    'CodesService',
    'TranslationService',
    'ArticlesService',
    'NotificationService',
    'ProductDescriptionsService',
    'RcosService',
    'LanguagesService',
    '$q',
    '$modal',
    'phoenix.config',
    'examTypes',
    function ($scope, codesService, translationService, articlesService, notificationService, productDescriptionsService, rcosService, languagesService, $q, $modal, config, examTypes) {
        'use strict';
        var self = this;
        $scope.localModel = {};
        $scope.isInitialized = false;
        $scope.isBusy = false;
        $scope.isEditMode = true;
        $scope.examRcoCourses = [];
        $scope.article = {
            Attachments: [],
            Version: 1
        };

        $scope.isElearning = function () {
            return $scope.article.ArticleTypeId === 1;
        };

        $scope.isClassroom = function () {
            return $scope.article.ArticleTypeId === 2 || $scope.article.ArticleTypeId === 7;
        };
        
        $scope.isMultipleChoice = function () {
            return $scope.article.ArticleTypeId === 3;
        };

        $scope.isCaseExam = function () {
            return $scope.article.ArticleTypeId === 4;
        };

        $scope.isProjectAssignment = function () {
            return $scope.article.ArticleTypeId === 5;
        };

        $scope.isInternalCertification = function () {
            return $scope.article.ArticleTypeId === 6;
        };

        $scope.isMockexam = function () {
            return $scope.article.ArticleTypeId === 8;
        };

        $scope.isExternalCertification = function () {
            return $scope.article.ArticleTypeId === 9;
        };

        $scope.isSeminar = function () {
            return $scope.article.ArticleTypeId === 10;
        };

        $scope.isWebinar= function () {
            return $scope.article.ArticleTypeId === 11;
        };

        $scope.onMasteryScorePercentageChanged = function() {
            if (angular.isNumber($scope.article.MasteryScorePercentage)) {
                $scope.article.MasteryScore = $scope.article.MasteryScorePercentage / 100;
            } else {
                $scope.article.MasteryScore = null;
            }
        };
        
        self.getLanguageByIdentifier = function (identifier) {
            return _($scope.languages).find(function (lan) {
                return lan.Identifier === identifier;
            });
        };

        self.getLanguageById = function (id) {
            return _($scope.languages).find(function (lan) {
                return lan.Id === id;
            });
        };

        $scope.languageChanged = function () {
            if (!$scope.article.LanguageId) {
                return;
            }
            var language = self.getLanguageById($scope.article.LanguageId);

            if (!$scope.selectedProductDescription) {
                var productDescription = self.getProductDescription($scope.productDescriptions, language) || self.getProductDescription($scope.productDescriptions, self.getLanguageByIdentifier('EN'));
                if (productDescription) {
                    self.setProductDescription(productDescription);
                }    
            }
            
            if (!$scope.article.NoResultResponse) {
                translationService.translate('article_feedback', language.Identifier).then(function (translation) {
                    $scope.article.NoResultResponse = translation;
                });
            }
        };

        $scope.easyQuestionCountChanged = function() {
            $scope.updateNumberOfQuestions();
            if (!$scope.article.EasyQuestionCount) {
                $scope.article.EasyCustomerSpecificCount = null;
            }
        };

        $scope.mediumQuestionCountChanged = function () {
            $scope.updateNumberOfQuestions();
            if (!$scope.article.MediumQuestionCount) {
                $scope.article.MediumCustomerSpecificCount = null;
            }
        };

        $scope.hardQuestionCountChanged = function () {
            $scope.updateNumberOfQuestions();
            if (!$scope.article.HardQuestionCount) {
                $scope.article.HardCustomerSpecificCount = null;
            }
        };

        $scope.updateNumberOfQuestions = function() {
            $scope.article.NumberOfQuestions = ($scope.article.EasyQuestionCount || 0) + ($scope.article.MediumQuestionCount || 0) + ($scope.article.HardQuestionCount || 0);
        };

        $scope.save = function () {
            $scope.isBusy = true;

            var saveFunction = $scope.isEditMode ? articlesService.update : articlesService.create;
            saveFunction($scope.article).then(function (result) {
                $scope.article = result;
                $scope.isEditMode = true;
                $scope.onSave({ articleId: result.Id });
                $scope.isBusy = false;
            }, self.handleError);
        };

        $scope.cancel = function () {
            $scope.onCancel({ articleId: $scope.article.Id });
        };

        $scope.onFileUploaded = function (attachment) {
            $scope.article.Attachments.push(attachment);
        };

        $scope.onExamDocumentUploaded = function (attachment) {
            $scope.article.CaseAttachment = attachment;
        };

        $scope.deleteExamDocument = function () {
            $scope.article.CaseAttachment = null;
            $scope.article.CaseAttachmentId = null;
        };

        $scope.deleteAttachment = function (attachment) {
            $scope.article.Attachments.indexOf(attachment);
            var index = $scope.article.Attachments.indexOf(attachment);
            if (index > -1) {
                $scope.article.Attachments.splice(index, 1);
            }
        };

        $scope.onProductDescriptionChanged = function (productDescription) {
            self.setProductDescription(productDescription);
        };

        self.handleError = function () {
            $scope.isBusy = false;
        };

        self.setProductDescription = function (productDescription) {
            $scope.selectedProductDescription = productDescription;
            $scope.article.ProductDescriptionId = productDescription.Id;
            if (!$scope.isEditMode) {
                $scope.article.Title = productDescription.Title;
            }
        };

        self.getProductDescription = function (productDescriptions, language) {
            return _(productDescriptions).find(function (pd) {
                return pd.LanguageId === language.Id;
            });
        };

        $scope.openSelectRcoCourseModal = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.products/views/modals/rco-course-select-modal.html',
                controller: 'RcoCourseSelectModalCtrl',
                resolve: {
                },
                windowClass: 'modal-wider'
            });
            modalInstance.result.then(function (selectedRcoCourse) {
                rcosService.getCourse(selectedRcoCourse.Id, false).then(function (result) {
                    $scope.rcoCourse = result;
                    $scope.article.RcoCourseId = result.Id;
                });
            });
        };

        $scope.openSelectExamCourseModal = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.products/views/modals/rco-course-select-modal.html',
                controller: 'RcoCourseSelectModalCtrl',
                resolve: {
                },
                windowClass: 'modal-wider'
            });

            modalInstance.result.then(function (selectedRcoCourse) {
                var existing = _($scope.article.ArticleExamCourses).find(function (course) {
                    return course.CourseRcoId === selectedRcoCourse.Id;
                });
                if (!!existing) {
                    return;
                }

                rcosService.getCourse(selectedRcoCourse.Id, false).then(function (result) {
                    $scope.examRcoCourses.push(result);
                    $scope.article.ArticleExamCourses.push({
                        ArticleId: $scope.article.Id,
                        CourseRcoId: result.Id
                    });
                });
            });
        };
        
        $scope.getExamRcoCourse = function (rcoId) {
            return _($scope.examRcoCourses).find(function (rco) { return rco.Id === rcoId; });
        };

        $scope.openRcoCourseInfoModal = function (rcoCourseId) {
            $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.products/views/modals/rco-course-info-modal.html',
                controller: 'RcoCourseInfoModalCtrl',
                resolve: {
                    rcoCourse: function () {
                        return rcosService.getCourse(rcoCourseId, true);
                    }
                },
                windowClass: 'modal-wide'
            });
        };
        
        $scope.removeRcoCourse = function () {
            $scope.article.RcoCourseId = null;
            $scope.rcoCourse = null;
        };

        $scope.removeArticleExamCourse = function (index) {
            if (index > -1) {
                $scope.article.ArticleExamCourses.splice(index, 1);
            }
        };

        $scope.openSelectRcoExamModal = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.products/views/modals/rco-exam-select-modal.html',
                controller: 'RcoExamSelectModalCtrl',
                resolve: {
                },
                windowClass: 'modal-wider'
            });
            modalInstance.result.then(function (selectedRcoExam) {
                rcosService.getExam(selectedRcoExam.Id).then(function (result) {
                    $scope.rcoExam = result;
                    $scope.article.RcoExamContainerId = result.Id;
                });
            });
        };

        $scope.openRcoExamInfoModal = function () {
            $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.products/views/modals/rco-exam-info-modal.html',
                controller: 'RcoExamInfoModalCtrl',
                resolve: {
                    rcoExam: function () {
                        return rcosService.getExam($scope.article.RcoExamContainerId);
                    }
                },
                windowClass: 'modal-wide'
            });
        };

        $scope.removeRcoExam = function () {
            $scope.article.RcoExamContainerId = null;
            $scope.rcoExam = null;
        };

        $scope.$watch('editArticleId', function () {
            if ($scope.editArticleId) {
                articlesService.get($scope.editArticleId).then(function (article) {
                    $scope.article = article;
                    if ($scope.article.Product) {
                        $scope.internalProductId = $scope.article.Product.Id;
                    }
                });
            }
        });

        $scope.$watch('editArticle', function () {
            if ($scope.editArticle) {
                $scope.article = angular.copy($scope.editArticle);
                if ($scope.article.Product) {
                    $scope.internalProductId = $scope.article.Product.Id;
                }
            }
        });

        $scope.$watch('isNew', function () {
            if ($scope.isNew) {
                $scope.isEditMode = false;
            }
        });

        $scope.$watch('createCustomerId', function () {
            if ($scope.createCustomerId) {
                $scope.article.CustomerId = $scope.createCustomerId;
            }
        });

        $scope.$watch('createArticleTypeId', function () {
            if ($scope.createArticleTypeId) {
                $scope.article.ArticleTypeId = $scope.createArticleTypeId;
            }
        });

        $scope.$watch('createProductId', function () {
            if ($scope.createProductId) {
                $scope.internalProductId = $scope.createProductId;
            }
        });

        $scope.$watch('internalProductId', function () {
            if ($scope.internalProductId) {
                $scope.article.ProductId = $scope.internalProductId;
                self.init();
            }
        });

        self.init = function () {
            $scope.article.ArticleExamCourses = $scope.article.ArticleExamCourses || [];

            if (angular.isNumber($scope.article.MasteryScore)) {
                $scope.article.MasteryScorePercentage = $scope.article.MasteryScore * 100;
            }
            var promises = [
                codesService.getAll(),
                languagesService.getAll(),
                productDescriptionsService.getProductDescriptions({ productId: $scope.article.ProductId, genericOnly: true, simple: true })
            ];

            if ($scope.article.RcoCourseId) {
                var internalCoursePromise = $q.defer();

                rcosService.getCourse($scope.article.RcoCourseId, false).then(function(result) {
                    $scope.rcoCourse = result;
                    internalCoursePromise.resolve();
                }, function () {
                    $scope.article.RcoCourseId = null;
                    internalCoursePromise.resolve();
                });

                promises.push(internalCoursePromise);
            }

            if ($scope.article.RcoExamContainerId) {
                var internalExamPromise = $q.defer();

                rcosService.getExam($scope.article.RcoExamContainerId, false).then(function (result) {
                    $scope.rcoExam = result;
                    internalExamPromise.resolve();
                }, function () {
                    $scope.article.RcoExamContainerId = null;
                    internalExamPromise.resolve();
                });

                promises.push(internalExamPromise);
            }
            
            if (!$scope.article.Id && ($scope.isMultipleChoice() || $scope.isCaseExam() || $scope.isExternalCertification() || $scope.isProjectAssignment())) {
                $scope.article.IsUserCompetenceInfoRequired = true;
            }

            if (!$scope.article.Id && $scope.isElearning()) {
                $scope.article.ProviderNumber = 1402;
                $scope.article.ProviderName = "Metier Project Management Academy AS";
            }

            if ($scope.isMultipleChoice() || $scope.isInternalCertification()) {
                if (!$scope.article.ExamType) {
                    $scope.article.ExamType = 0;
                }
                if (!$scope.article.ExamResultResponseType) {
                    $scope.article.ExamResultResponseType = 0;
                }
            }
            $scope.examTypes = examTypes;

            // Fetch data in parallel   
            $q.all(promises).then(function (results) {
                $scope.articleStatuses = results[0].articleStatuses;
                $scope.languages = results[1];
                $scope.productDescriptions = results[2];
                $scope.ects = ['5', '7.5', '10', '15'];     //TODO: Extract Into a config-file or DB. The same 'magic numbers' are duplicated in activity-create-exam-controller.js

                var invalidCourses = [];

                if ($scope.article.ArticleExamCourses && $scope.article.ArticleExamCourses.length > 0) {
                    var examCoursePromises = _($scope.article.ArticleExamCourses).map(function (course) {
                        var examCoursePromise = $q.defer();

                        rcosService.getCourse(course.CourseRcoId, false).then(function(rco) {
                            $scope.examRcoCourses.push(rco);
                        }, function () {
                            invalidCourses.push(course);
                        })
                        .finally(function () {
                            examCoursePromise.resolve();
                        });

                        return examCoursePromise.promise;
                    });

                    $q.all(examCoursePromises).then(function (rcos) {
                        _(invalidCourses).each(function (invalidCourse) {
                            $scope.article.ArticleExamCourses.splice($scope.article.ArticleExamCourses.indexOf(invalidCourse), 1);
                        });

                        _(rcos).each(function (rco) {
                            $scope.examRcoCourses.push(rco);
                        });
                        $scope.isInitialized = true;
                    });
                } else {
                    $scope.isInitialized = true;
                }

                
            });
        };
    }]
);