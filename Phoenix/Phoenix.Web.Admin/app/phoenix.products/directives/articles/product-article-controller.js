﻿angular.module('phoenix.products').controller('ProductArticleDirectiveCtrl', ['$scope', 'ArticlesService', function ($scope, articlesService) {
    'use strict';
    var self = this;
    $scope.isInitialized = false;
    $scope.isEditing = false;
    $scope.isNew = false;
    
    self.updateTitle = function (article) {
        $scope.title = article.ArticlePath + ' - ' + article.Title;
    };
    
    $scope.onArticleSaved = function (articleId) {
        if (!$scope.externalArticle) {
            articlesService.get(articleId).then(function (article) {
                $scope.article = article;
                self.updateTitle(article);
            });
        }
        $scope.isEditing = false;
        $scope.onSave({ articleId: articleId });
    };
    
    $scope.edit = function () {
        $scope.isEditing = true;
    };

    $scope.onArticleCancelled = function (articleId) {
        $scope.isEditing = false;
        $scope.onCancel({ articleId: articleId });
    };

    $scope.$watch('externalArticle', function () {
        if ($scope.externalArticle) {
            $scope.article = angular.copy($scope.externalArticle);
            $scope.isInitialized = true;
            self.updateTitle($scope.externalArticle);
            if (!$scope.article.Id) {
                $scope.isNew = true;
                $scope.isEditing = true;
            }
        }
    });

    $scope.$watch('articleId', function () {
        if ($scope.articleId) {
            articlesService.get($scope.articleId).then(function (article) {
                $scope.$emit('articleLoaded', article);
                $scope.article = article;
                $scope.title = article.ArticlePath + ' - ' + article.Title;
                $scope.isEditing = false;
                $scope.isInitialized = true;
            });
        }
    });
}]);