﻿angular.module('phoenix.products').controller('ProductArticleSearchDirectiveCtrl', [
    '$scope',
    'ArticlesService',
    function ($scope, articlesService) {
        'use strict';
        var self = this;
        self.initialData = null;
        $scope.limit = 10;
        $scope.isInitialized = false;
        $scope.lastSearchQuery = '';
        $scope.selectedArticle = null;

        $scope.search = function () {
            if (!$scope.query) {
                return;
            }
            $scope.isSearching = true;
            $scope.lastSearchQuery = $scope.query;
            $scope.select(null);
            articlesService.getArticles({ status: ['Published'], query: $scope.query, limit: $scope.limit, page: 1 }).then(function (result) {
                $scope.isSearching = false;
                $scope.articles = result;
            }, function () {
                $scope.isSearching = false;
            });
        };
        
        $scope.navigate = function (page) {
            articlesService.getArticles({ status: ['Published'], query: $scope.query, limit: $scope.limit, page: page }).then(function (result) {
                $scope.articles = result;
            });
        };

        $scope.onQueryChanged = function () {
            if ($scope.query === '') {
                $scope.articles = self.initialData;
            }
        };

        $scope.select = function (articleId) {
            if (articleId === $scope.selectedArticleId) {
                $scope.selectedArticleId = null;    
            } else {
                $scope.selectedArticleId = articleId;
            }
            $scope.articleSelected({ articleId: $scope.selectedArticleId });
        };

        $scope.numberOfVisibleArticles = function () {
            return Math.min($scope.articles.TotalCount, $scope.limit);
        };

        self.init = function () {
            articlesService.getArticles({ status: ['Published'], query: '', limit: $scope.limit, page: 1 }).then(function (result) {
                $scope.isInitialized = true;
                self.initialData = result;
                $scope.articles = result;
            });
        };

        self.init();
    }
]);