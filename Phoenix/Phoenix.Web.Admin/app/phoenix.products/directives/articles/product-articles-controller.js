﻿/*global angular, confirm, _*/
/*jslint maxlen: 150 */
angular.module('phoenix.products').controller('ProductArticlesDirectiveCtrl', ['$scope', 'ArticlesService', function ($scope, articlesService) {
    'use strict';
    var self = this;
    $scope.isInitialized = false;
    $scope.articles = [];
    $scope.selectedArticle = {};

    $scope.isSelected = function (article) {
        return $scope.selectedArticle === article;
    };

    $scope.select = function (article) {
        $scope.selectedArticle = article;
        $scope.languageId = $scope.selectedArticle.LanguageId;
    };

    $scope.deleteArticle = function (article) {
        if (confirm("Are you sure you want to delete the article?")) {
            if (article.Id) {
                article.IsDeleted = true;
                articlesService.update(article).then(function () {
                    self.remove(article);
                });
            } else {
                self.remove(article);
            }
        }
    };

    $scope.addNewLanguage = function () {
        if ($scope.isInitialized) {
            var article = self.createEmptyArticle();
            $scope.articles.push(article);
            $scope.select(article);
        }
    };

    $scope.onArticleSaved = function (articleId) {
        articlesService.get(articleId).then(function (article) {
            $scope.selectedArticle.Id = article.Id;
            var updated = _($scope.articles).find(function (a) {
                return a.Id === article.Id;
            }),
            index = $scope.articles.indexOf(updated);
            if (index > -1) {
                $scope.articles[index] = article;
            }
            $scope.selectedArticle = article;
        });
    };
    
    $scope.onArticleCancelled = function (articleId) {
        var article = self.getArticle(articleId);
        $scope.selectedArticle = article;
        
        if (!article.Id) {
            self.remove(article);
        }
    };

    self.createEmptyArticle = function () {
        return {
            ProductId: $scope.productId
        };
    };

    self.remove = function (article) {
        $scope.articles.splice($scope.articles.indexOf(article), 1);
        if ($scope.articles.length > 0) {
            $scope.select($scope.articles[0]);
        }
    };
    
    self.getArticle = function (articleId) {
        return _($scope.articles).find(function (a) {
            return a.Id === articleId;
        });
    };

    self.initialize = function () {
        // Get all articles for a given product and a given article type
        articlesService.getArticles({ productId: $scope.productId, articleTypeId: $scope.articleTypeId }).then(function (articles) {
            $scope.articles = articles.Items;
            if (articles.Items.length === 0) {    
                $scope.articles.push(self.createEmptyArticleInfo());
            }
            
            $scope.select($scope.articles[0]);
            $scope.isInitialized = true;
        });        
    };
    
    self.initialize();
}]);