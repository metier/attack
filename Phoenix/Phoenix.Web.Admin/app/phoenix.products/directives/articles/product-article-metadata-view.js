﻿/*global angular*/
/*jslint maxlen: 150 */
angular.module('phoenix.products').directive('productArticleMetadataView', ['phoenix.config', function (config) {
    'use strict';
    return {
        restrict: 'E',
        scope: {
            inArticleId: '=articleId',
            inArticle: '=article'
        },
        controller: 'ProductArticleMetadataViewDirectiveCtrl',
        templateUrl: config.basePath + 'app/phoenix.products/directives/articles/product-article-metadata-view.html'
    };
}]);