﻿/*global angular*/
/*jslint maxlen: 150 */
angular.module('phoenix.products').directive('productArticleView', ['phoenix.config', function (config) {
    'use strict';
    return {
        restrict: 'E',
        scope: {
            articleId: '=',
            articleInstance: '=article'
        },
        controller: 'ProductArticleViewDirectiveCtrl',
        templateUrl: config.basePath + 'app/phoenix.products/directives/articles/product-article-view.html'
    };
}]);