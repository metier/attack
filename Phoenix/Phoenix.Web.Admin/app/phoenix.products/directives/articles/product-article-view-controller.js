﻿/*global angular, _*/
/*jslint maxlen: 190 */
angular.module('phoenix.products').controller('ProductArticleViewDirectiveCtrl', ['$scope', 'CodesService', 'ArticlesService', function ($scope, codesService, articlesService) {
    'use strict';
    var self = this;
    $scope.isInitialized = false;
    
    $scope.isElearning = function () {
        return $scope.article.ArticleTypeId === 1;
    };

    $scope.isClassroom = function () {
        return $scope.article.ArticleTypeId === 2 || $scope.article.ArticleTypeId === 7;
    };
    
    $scope.isMultipleChoice = function () {
        return $scope.article.ArticleTypeId === 3;
    };

    $scope.isCaseExam = function () {
        return $scope.article.ArticleTypeId === 4;
    };

    $scope.isProjectAssignment = function () {
        return $scope.article.ArticleTypeId === 5;
    };

    $scope.isInternalCertification = function () {
        return $scope.article.ArticleTypeId === 6;
    };

    $scope.isMockexam = function () {
        return $scope.article.ArticleTypeId === 8;
    };
    
    $scope.isExternalCertification = function () {
        return $scope.article.ArticleTypeId === 9;
    };

    self.getCodeDisplay = function (codes, id) {
        return _(codes).find(function (c) {
            return c.Id === id;
        }).Display;
    };

    $scope.$watch('articleInstance', function () {
        if ($scope.articleInstance) {
            $scope.article = $scope.articleInstance;
            $scope.isInitialized = true;
        }
    });

    $scope.$watch('articleId', function () {
        if ($scope.articleId) {
            articlesService.get($scope.articleId).then(function (article) {
                $scope.article = article;
                $scope.isInitialized = true;
            });
        }
    });
    
    self.init = function () {
        codesService.getAll().then(function (result) {
            self.articleStatuses = result.articleStatuses;
        });
    };
    
    self.init();
}]);