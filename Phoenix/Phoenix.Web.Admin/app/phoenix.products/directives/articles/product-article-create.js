﻿/*global angular*/
/*jslint maxlen: 150 */
angular.module('phoenix.products').directive('productArticleCreate', ['phoenix.config', '$parse', function (config) {
    'use strict';
    var self = {};
    self.parseIntOrNull = function (value) {
        if (value) {
            return parseInt(value, 10);
        }
        return null;
    };
    return {
        restrict: 'E',
        scope: {
            createArticleTypeId: '=articleTypeId',
            createProductId: '=productId',
            createCustomerId: '=customerId',
            editArticleId: '=articleId',
            editArticle: '=article',
            canSave: '=',
            setBusySave: '=',
            isNew: '=',

            onSave: '&',
            onCancel: '&'
        },
        controller: 'ProductArticleCreateDirectiveCtrl',
        templateUrl: config.basePath + 'app/phoenix.products/directives/articles/product-article-create.html'
    };
}]);