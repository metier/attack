﻿/*global angular, _*/
/*jslint maxlen: 190 */
angular.module('phoenix.products').controller('ProductArticleMetadataViewDirectiveCtrl', [
    '$scope',
    '$modal',
    '$q',
    'phoenix.config',
    'CodesService',
    'ArticlesService',
    'RcosService',
    'examTypes',
    'NotificationService',
    function ($scope, $modal, $q, config, codesService, articlesService, rcosService, examTypes, notificationService) {
        'use strict';
        var self = this;
        $scope.isInitialized = false;
        $scope.examRcoCourses = [];
        
        $scope.isElearning = function () {
            return $scope.article.ArticleTypeId === 1;
        };

        $scope.isClassroom = function () {
            return $scope.article.ArticleTypeId === 2 || $scope.article.ArticleTypeId === 7;
        };

        $scope.isMultipleChoice = function () {
            return $scope.article.ArticleTypeId === 3;
        };

        $scope.isCaseExam = function () {
            return $scope.article.ArticleTypeId === 4;
        };

        $scope.isProjectAssignment = function () {
            return $scope.article.ArticleTypeId === 5;
        };

        $scope.isInternalCertification = function () {
            return $scope.article.ArticleTypeId === 6;
        };

        $scope.isMockexam = function () {
            return $scope.article.ArticleTypeId === 8;
        };

        $scope.isExternalCertification = function () {
            return $scope.article.ArticleTypeId === 9;
        };

        self.getCodeDisplay = function (codes, id) {
            return _(codes).find(function (c) {
                return c.Id === id;
            }).Display;
        };

        $scope.getArticleStatusDisplay = function (id) {
            if (!id || !self.articleStatuses) {
                return '';
            }
            return self.getCodeDisplay(self.articleStatuses, id);
        };

        $scope.getExamTypeDisplay = function(id) {
            if (id === undefined || id === null || !examTypes) {
                return '';
            }
            return _(examTypes).find(function(item) {
                return item.value === id;
            }).text;
        };

        $scope.getExamRcoCourse = function (rcoId) {
            return _($scope.examRcoCourses).find(function (rco) { return rco && rco.Id === rcoId; });
        };

        function handleMissingRcoLink() {
            notificationService.alertWarning({
                message: 'The selected course/exam link does no longer exist. It may have been deleted from the external system, please reconnect.',
                header: 'Invalid course/exam link'
            });
        }

        function resolveArticleData(article) {
            $scope.article = article;
            if ($scope.article.RcoCourseId) {
                rcosService.getCourse($scope.article.RcoCourseId, false).then(function (course) {
                    $scope.rcoCourse = course;
                    $scope.isInitialized = true;
                }, function() {
                    handleMissingRcoLink();
                    $scope.isInitialized = true;
                });
            } else if ($scope.article.RcoExamContainerId) {
                rcosService.getExam($scope.article.RcoExamContainerId, false).then(function(exam) {
                    $scope.rcoExam = exam;
                    $scope.isInitialized = true;
                }, function () {
                    handleMissingRcoLink();
                    $scope.isInitialized = true;
                });
            } else {
                initArticleExamCourses().then(function () {
                    $scope.isInitialized = true;
                });
            }

        }


        $scope.$watch('inArticle', function () {
            if ($scope.inArticle) {
                resolveArticleData($scope.inArticle);
            }
        });

        $scope.openRcoCourseInfoModal = function (rcoCourseId) {
            $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.products/views/modals/rco-course-info-modal.html',
                controller: 'RcoCourseInfoModalCtrl',
                resolve: {
                    rcoCourse: function() {
                        return rcosService.getCourse(rcoCourseId);
                    }
                },
                windowClass: 'modal-wide'
            });
        };

        $scope.openRcoExamInfoModal = function () {
            $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.products/views/modals/rco-exam-info-modal.html',
                controller: 'RcoExamInfoModalCtrl',
                resolve: {
                    rcoExam: function () {
                        return $scope.rcoExam;
                    }
                },
                windowClass: 'modal-wide'
            });
        };

        $scope.$watch('inArticleId', function () {
            if ($scope.inArticleId) {
                self.tracker.createPromise();
                articlesService.get($scope.inArticleId).then(function (article) {
                    resolveArticleData(article);
                });
            }
        });

        function initArticleExamCourses() {
            var deferred = $q.defer();

            if ($scope.article.ArticleExamCourses && $scope.article.ArticleExamCourses.length > 0) {
                var examCoursePromises = _($scope.article.ArticleExamCourses).map(function (course) {
                    var coursePromise = $q.defer();

                    
                    rcosService.getCourse(course.CourseRcoId, false).then(function(rco) {
                        $scope.examRcoCourses.push(rco);
                        coursePromise.resolve();
                    },function() {
                        handleMissingRcoLink();
                        coursePromise.resolve();
                    });

                    return coursePromise.promise;
                });

                $q.all(examCoursePromises).then(function (rcos) {
                    _(rcos).each(function (rco) {
                        $scope.examRcoCourses.push(rco);
                    });
                    deferred.resolve();
                });
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }
        

        self.init = function () {
            codesService.getAll().then(function (result) {
                self.articleStatuses = result.articleStatuses;
            });
        };

        self.init();
    }]
);