﻿angular.module('phoenix.products').directive('productArticleSearch', [
    'phoenix.config',
    function(config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                articleSelected: '&' // signature: articleSelected(id)   
            },
            controller: 'ProductArticleSearchDirectiveCtrl',
            templateUrl: config.basePath + 'app/phoenix.products/directives/articles/product-article-search.html'
        };
    }
]);