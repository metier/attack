﻿/*global angular */
angular.module('phoenix.products', ['phoenix.config', 'phoenix.shared', 'ui.bootstrap', 'ui.utils', 'placeholderShim', 'ngUpload', 'ui.router', 'textAngular', 'ngSanitize']
        ).config(['$stateProvider', 'phoenix.config', function ($stateProvider, config) {
            'use strict';
            
            $stateProvider.state('productlist', {
                url: '/products',
                abstract: true,
                controller: 'ProductListCtrl',
                templateUrl: config.basePath + 'app/phoenix.products/views/product-list.html',
                resolve: {
                    allProducts: ['ProductsService', function (productsService) {
                        return productsService.getAll();
                    }],
                    productTypes: ['ProductsService', function (productsService) {
                        return productsService.getAllProcuctTypes();
                    }],
                    productCategories: ['ProductsService', function (productsService) {
                        return productsService.getAllProcuctCategories();
                    }]
                }
            }).state('productlist.active', {
                url: '/active',
                controller: 'ProductListTableCtrl',
                templateUrl: config.basePath + 'app/phoenix.products/views/product-list-table.html',
                resolve: {
                    products: ['allProducts', function (allProducts) {
                        return _(allProducts).filter(function (p) { return p.IsActive; });
                    }]
                }
            }).state('productlist.inactive', {
                url: '/inactive',
                controller: 'ProductListTableCtrl',
                templateUrl: config.basePath + 'app/phoenix.products/views/product-list-table.html',
                resolve: {
                    products: ['allProducts', function (allProducts) {
                        return _(allProducts).filter(function (p) { return !p.IsActive; });
                    }]
                }
            });
            
            $stateProvider.state('products', {
                url: '/products',
                template: '<ui-view />'
            });

            $stateProvider.state('products.item', {
                url: '/:productId',
                controller: 'ProductItemCtrl',
                abstract: true,
                templateUrl: config.basePath + 'app/phoenix.products/views/product-item.html',
                resolve: {
                    product: ['$stateParams', 'ProductsService', function ($stateParams, productsService) {
                        return productsService.get($stateParams.productId);
                    }]
                }
            });
            
            $stateProvider.state('products.item.descriptions', {
                url: '/descriptions?customerId',
                controller: 'ProductDescriptionsCtrl',
                templateUrl: config.basePath + 'app/phoenix.products/views/product-descriptions.html',
                resolve: {
                    productDescriptionsSimple: ['ProductDescriptionsService', '$stateParams', function (productDescriptionsService, $stateParams) {
                        return productDescriptionsService.getProductDescriptions({
                            productId: $stateParams.productId,
                            simple: true
                        });
                    }]
                }
            });
            
            $stateProvider.state('products.item.articles', {
                url: '/articles?page&limit&orderBy&orderDirection',
                controller: 'ProductArticleListCtrl',
                templateUrl: config.basePath + 'app/phoenix.products/views/product-article-list.html',
                resolve: {
                    articles: ['ArticlesService', '$stateParams', function (articlesService, $stateParams) {
                        var options = {
                            productId: $stateParams.productId,
                            page: parseInt($stateParams.page, 10) || 1,
                            limit: parseInt($stateParams.limit, 10) || 15,
                            orderBy: $stateParams.orderBy,
                            orderDirection: $stateParams.orderDirection
                        };
                        return articlesService.getArticles(options);
                    }]
                }
            });

            $stateProvider.state('products.item.article', {
                url: '/articles/:articleId',
                controller: 'ProductArticleCtrl',
                templateUrl: config.basePath + 'app/phoenix.products/views/product-article.html',
                resolve: {
                    customerArticles: [
                        'CustomerArticlesService',
                        'DistributorsService',
                        '$stateParams',
                        function(customerArticlesService, distributorsService, $stateParams) {
                            return customerArticlesService.getByArticleId($stateParams.articleId, distributorsService.get().Id);
                        }
                    ]
                }
            });

            $stateProvider.state('products.item.articlecreate', {
                url: '/articles/create/:articleTypeId',
                controller: 'ProductArticleCreateCtrl',
                templateUrl: config.basePath + 'app/phoenix.products/views/product-article-create.html'
            });
            
        }]);