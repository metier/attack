﻿angular.module('phoenix.reporting').controller('ReportsCtrl', [
    '$scope',
    '$state',
    function ($scope, $state) {
    'use strict';
    $scope.reports = [
        { category: 'Customer', text: 'Progress Status E-learning', state: 'reports.progressstatus' },
        { category: 'Customer', text: 'Participations Status', state: 'reports.productcoursestatus' },
        { category: 'Customer', text: 'Participations Status pr. activity', state: 'reports.participationsstatuspractivity' },
        { category: 'Customer', text: 'Potential Participations', state: 'reports.potentialparticipations' },
        { category: 'Customer', text: 'All Participations', state: 'reports.allparticipations' },
        { category: 'Customer', text: 'All Participations pr. activity', state: 'reports.allparticipationspractivity' },
        { category: 'Customer', text: 'Forecast', state: 'reports.ordersforinvoicing' },
        { category: 'Customer', text: 'Prepaid Classrooms', state: 'reports.prepaidclassrooms' },
        { category: 'Customer', text: 'Hubspot Participants', state: 'reports.hubspotparticipants' },
        { category: 'Exams', text: 'Exams', state: 'reports.exams' },
        { category: 'Automails', text: 'Automails Log', state: 'reports.maillog' },
        { category: 'Customer Specific', text: 'Follobanen - Safety course participants', state: 'reports.customerspecific_jernbaneverket' }
];
    $scope.selectedReport = null;

    $scope.reportSelected = function () {
        $state.go($scope.selectedReport.state);
    };

    $scope.$on('$stateChangeSuccess', function (event, toState) {
        $scope.selectedReport = _($scope.reports).find(function(r) {
            return r.state === toState.name;
        });
    });
}]);