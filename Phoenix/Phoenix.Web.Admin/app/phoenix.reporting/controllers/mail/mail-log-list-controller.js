﻿angular.module('phoenix.reporting').controller('MailLogListCtrl', [
    '$scope',
    '$state',
    '$location',
    '$modal',
    'mails',
    'mailStatuses',
    'mailMessageTypes',
    'MailsService',
    'NotificationService',
    'phoenix.config',
    function ($scope, $state, $location, $modal, mails, mailStatuses, mailMessageTypes, mailsService, notificationService, config) {
        'use strict';
        $scope.mails = mails;

        $scope.fromDate = !!$location.search().fromDate ? moment($location.search().fromDate).toDate() : moment().subtract(1, 'days').toDate();
        $scope.toDate = !!$location.search().toDate ? moment($location.search().toDate).toDate() : moment().toDate();

        $scope.mailStatusFilterValues = mailStatuses;
        $scope.mailMessageTypeFilterValues = mailMessageTypes;

        $scope.mails = mails || { TotalCount: 0, Items: [] };
        $scope.query = $location.search().query;
        $scope.orderBy = $location.search().orderBy || '';
        $scope.orderDirection = $location.search().orderDirection || '';
        $scope.page = $location.search().page;
        $scope.limit = $location.search().limit || 15;
        $scope.maxPages = $scope.mails.TotalCount;
        
        $scope.onQueryChanged = function () {
            if ($scope.query === '') {
                $scope.search();
            }
        };

        $scope.search = function () {
            if (!$state.params.query && $scope.query === '') { return; }
            $state.go('reports.maillog', { query: $scope.query });
        };

        $scope.navigate = function (page) {
            if (page < 1 || page > $scope.maxPages) {
                return;
            }
            $location.search('page', page);
        };
        
        $scope.applyFilter = function () {
            if ($scope.fromDate && moment($scope.fromDate).isValid()) {
                $location.search('fromDate', moment($scope.fromDate).format('YYYY-MM-DD'));
            } else {
                $location.search('fromDate', null);
            }

            if ($scope.toDate && moment($scope.toDate).isValid()) {
                $location.search('toDate', moment($scope.toDate).format('YYYY-MM-DD'));
            } else {
                $location.search('toDate', null);
            }
        };

        $scope.send = function (mail) {
            if (mail.MailStatus === 1 && !confirm("Message has already been sent, do you really want to re-send it?")) {
                return;
            }
            mail.isSending = true;
            mailsService.send(mail.Id).then(function (success) {
                var response = success.data;
                mail.isSending = false;
                mail.MailStatus = response.Status;
                mail.StatusTime = response.StatusTime;
                if (response.Status === 1) {
                    notificationService.alertSuccess({ message: 'The e-mail was successfully sent' });
                }
            }, function (error) {
                var response = error.data;
                if (error.status === 503 && response) {
                    mail.MailStatus = response.Status;
                    mail.StatusTime = response.StatusTime;
                    notificationService.alertError({ message: 'Failed to send the e-mail' });
                }
                mail.isSending = false;
            });
        };

        $scope.preview = function (mail) {
            mail.isOpeningPreview = true;

            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.reporting/views/modals/mail-preview-modal.html',
                controller: 'MailPreviewModalCtrl',
                resolve: {
                    mail: function () { return mailsService.preview(mail.Id); }
                },
                windowClass: 'modal-widest'
            });
            modalInstance.opened.then(function () {
                mail.isOpeningPreview = false;
            });
        };
    }
]);