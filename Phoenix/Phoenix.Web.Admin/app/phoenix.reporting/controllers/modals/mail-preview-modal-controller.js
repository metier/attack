﻿angular.module('phoenix.reporting').controller('MailPreviewModalCtrl', [
    '$scope',
    '$modalInstance',
    'mail',
    function ($scope, $modalInstance, mail) {
        'use strict';
        $scope.mail = mail;
        $scope.close = function () {
            $modalInstance.dismiss('cancel');
        };
    }
]);