﻿angular.module('phoenix.reporting').directive('lessonStatusFilter', [
    'phoenix.config',
    'lessonStatuses',
    function (config, lessonStatuses) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                report: '='
            },
            templateUrl: config.basePath + 'app/phoenix.reporting/directives/lesson-status-filter/lesson-status-filter.html',
            link: function (scope, element, attrs, controller) {
                controller.init(lessonStatuses);
            },
            controller: [
                '$scope',
                function ($scope) {
                    var self = this;
                    self.init = function (statuses) {
                        $scope.lessonStatuses = statuses;
                    };
            }]
        };
    }
]);