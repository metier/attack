﻿angular.module('phoenix.reporting').directive('distributorFilter', [
    'phoenix.config',
    'DistributorsService',
    function (config, distributorsService) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                report: '='
            },
            templateUrl: config.basePath + 'app/phoenix.reporting/directives/distributor-filter/distributor-filter.html',
            controller: 'DistributorFilterCtrl',
            link: function(scope, element, attrs, controller) {
                distributorsService.getAll().then(function (distributors) {
                    controller.init(distributors);
                });
            }
        };
    }
]);

angular.module('phoenix.reporting').controller('DistributorFilterCtrl', [
    '$scope',
    function ($scope) {
        'use strict';
        var self = this;
        self.init = function (distributors) {
            $scope.distributors = _(distributors).map(function (d) {
                return { value: d.Id, text: d.Name };
            });
        };
    }
]);