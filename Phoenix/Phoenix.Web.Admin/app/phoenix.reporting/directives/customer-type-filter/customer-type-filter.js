﻿angular.module('phoenix.reporting').directive('customerTypeFilter', [
    'phoenix.config',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                report: '='
            },
            templateUrl: config.basePath + 'app/phoenix.reporting/directives/customer-type-filter/customer-type-filter.html',
            controller: [
                '$scope',
                function ($scope) {
                    $scope.locals = {};
                    function init() {
                        $scope.customerTypes = [
                            { text: 'Arbitrary', value: true },
                            { text: 'Corporate', value: false }
                        ];
                    }

                    $scope.setFilterValue = function() {
                        if ($scope.locals.filterValue === true || $scope.locals.filterValue === false) {
                            $scope.report.filters.isArbitrary = $scope.locals.filterValue;
                        } else {
                            delete $scope.report.filters.isArbitrary;
                        }
                    };

                    init();
                }
            ]
        };
    }
]);