﻿angular.module('phoenix.reporting').directive('orderStatusFilter', [
    'phoenix.config',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                report: '='
            },
            templateUrl: config.basePath + 'app/phoenix.reporting/directives/order-status-filter/order-status-filter.html',
            controller: 'OrderStatusFilterCtrl',
            link: function (scope, element, attrs, controller) {
                controller.init();
            }
        };
    }
]);

angular.module('phoenix.reporting').controller('OrderStatusFilterCtrl', [
    '$scope',
    'orderStatuses',
    function ($scope, orderStatuses) {
        'use strict';
        var self = this;
        self.init = function () {
            $scope.orderStatuses = orderStatuses;
        };
    }
]);