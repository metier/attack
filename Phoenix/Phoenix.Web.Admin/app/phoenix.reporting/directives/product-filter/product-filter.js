﻿angular.module('phoenix.reporting').directive('productFilter', [
    'phoenix.config',
    'ProductsService',
    function (config, productsService) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                report: '=',
				valueField: '@',
				bindTo:'@'
            },
            templateUrl: config.basePath + 'app/phoenix.reporting/directives/product-filter/product-filter.html',
            link: function (scope, element, attrs, controller) {
                productsService.getAllActive().then(function (products) {
                    controller.init(products);
                });
                
            },
            controller: [
                '$scope',
                function ($scope) {                    
					var self = this;					
					//if (!$scope.bindTo) $scope.bindTo = "productId";
					self.init = function (products)
					{						
                        $scope.products = _(products).map(function(p) {
                            return { text: p.ProductPath + ' ' + p.Title, value: $scope.valueField == "articlecode" ? p.ProductPath : p.Id };
                        });
                    };
            }]
        };
    }
]);