﻿angular.module('phoenix.reporting').directive('multiselectFilter', ['phoenix.config', '$document', function (config, $document) {
    'use strict';
    return {
        scope: {
            options: '=',
            text: '@',
            name: '@',
            report: '='
        },
        restrict: 'EA',
        templateUrl: config.basePath + 'app/phoenix.reporting/directives/multiselect-filter/multiselect-filter.html',
        link: function (scope, element, attrs, controller) {
            controller.init();

            // Make sure dropdown is hidden when user clicks somewhere else
            $document.bind('click', function (event) {
                var isClickedElementChildOfDropdown = element.find(event.target).length > 0;
                if (isClickedElementChildOfDropdown) {
                    return;
                }
                scope.isOpen = false;
                scope.$apply();
            });
        },
        controller: ['$scope', function ($scope) {
            var self = this;
            
            function addToFilter(option) {
                $scope.report.filters[$scope.name].push(option.value);
            }

            function removeFromFilter(option) {
                var index = $scope.report.filters[$scope.name].indexOf(option.value);
                if (index > -1) {
                    $scope.report.filters[$scope.name].splice(index, 1);
                }
            }

            $scope.dropdownItemClicked = function (option) {
                option.isSelected = !option.isSelected;
                if (option.isSelected) {
                    addToFilter(option);
                } else {
                    removeFromFilter(option);
                }
            };

            $scope.selectBoxRemoved = function (option) {
                option.isSelected = false;
                removeFromFilter(option);
            };
            
            $scope.$on('resetReportFilters', function () {
                $scope.report.filters[$scope.name].splice(0, $scope.report.filters[$scope.name].length);
                _($scope.options).each(function (o) {
                    o.isSelected = false;
                });
            });

            self.init = function () {
                $scope.report.filters[$scope.name] = [];
            };
        }]
    };
}]);