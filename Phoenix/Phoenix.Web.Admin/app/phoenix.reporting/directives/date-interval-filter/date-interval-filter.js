﻿angular.module('phoenix.reporting').directive('dateIntervalFilter', [
    'phoenix.config',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                report: '='
            },
            templateUrl: config.basePath + 'app/phoenix.reporting/directives/date-interval-filter/date-interval-filter.html',
            link: function (scope, element, attrs, controller) {
                controller.init();
            },
            controller: 'DateIntervalFilterCtrl'
        };
    }
]);

angular.module('phoenix.reporting').controller('DateIntervalFilterCtrl', [
    '$scope',
    '$rootScope',
    function ($scope, $rootScope) {
        'use strict';
        var self = this;
        $scope.dateOptions = $rootScope.dateOptions;

        $scope.$on('resetReportFilters', function () {
            self.init();
        });

        self.init = function () {
            $scope.report.filters.fromDate = moment().toDate();
            $scope.report.filters.toDate = null;
        };
    }
]);