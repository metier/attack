﻿angular.module('phoenix.reporting').directive('customerFilter', [
    'phoenix.config',
    function(config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                report: '=',
                reportForm: '=',
                isRequired: '='
            },
            templateUrl: config.basePath + 'app/phoenix.reporting/directives/customer-filter/customer-filter.html',
            controller: 'CustomerFilterCtrl',
            link: function (scope) {
                scope.report.filters.customerId = [];
            }
        };
    }
]);

angular.module('phoenix.reporting').controller('CustomerFilterCtrl', [
    'phoenix.config',
    '$scope',
    'CustomersService',
    function (config, $scope, customersService) {
        'use strict';
        $scope.customerFilter = [];
        $scope.customerId = null;

        $scope.$watch('customerId', function(customerId) {
                if (customerId) {
                    customersService.get(customerId).then(function(result) {
                        $scope.onCustomerSelected(result);
                    });
                };
            }
        );


        function addCustomerToFilter(customer) {
            $scope.report.filters.customerId.push(customer.Id);
            $scope.customerFilter.push(customer);
        }
        
        $scope.removeCustomerFromFilter = function (customer) {
            var index = $scope.report.filters.customerId.indexOf(customer.Id);
            if (index > -1) {
                $scope.report.filters.customerId.splice(index, 1);
            }

            var filered = _($scope.customerFilter).find(function (item) { return item.Id === customer.Id; });
            index = $scope.customerFilter.indexOf(filered);
            if (index > -1) {
                $scope.customerFilter.splice(index, 1);
            }
        };
        
        $scope.onCustomerSelected = function (customer) {
            if (customer) {
                var isAddedToFilter = _($scope.customerFilter).some(function (c) {
                    return c.Id === customer.Id;
                });
                if (!isAddedToFilter) {
                    addCustomerToFilter(customer);
                }
            }
            $scope.customer = null;
        };
        
        $scope.$on('resetReportFilters', function () {
            $scope.report.filters.customerId.splice(0, $scope.report.filters.customerId.length);
            $scope.customerFilter.splice(0, $scope.customerFilter.length);
        });

        $scope.searchCustomer = function (searchString) {
            return customersService.search({ query: searchString, limit: 50, searchAllDistributors: true }).then(function (result) {
                return result.Items;
            });
        };

        $scope.customerSearchTemplateUrl = config.basePath + 'app/phoenix.reporting/directives/customer-filter/customer-search-result-template.html';
    }
]);