﻿angular.module('phoenix.reporting').directive('reportActions', [
    'phoenix.config',
    function(config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                name: '@', // The name of the report as defined by API: /reports/<name>/xlsx
                folder: '@', // The folder below 'reports/' where the report exists 
                report: '=',
                reportForm: '=',
                showExcel: '=',
                showCsv: '='
            },
            templateUrl: config.basePath + 'app/phoenix.reporting/directives/report-actions/report-actions.html',
            controller: 'ReportActionsCtrl'
        };
    }
]);

angular.module('phoenix.reporting').controller('ReportActionsCtrl', [
    '$scope',
    '$rootScope',
    '$location',
    'QueryParameters',
    'phoenix.config',
    function ($scope, $rootScope, $location, queryParameters, config) {
        'use strict';
        
        $scope.resetFilters = function () {
            $scope.$parent.$broadcast('resetReportFilters');
        };

        $scope.getExcelSrc = function () {
            if ($scope.showExcel === false || ($scope.showExcel === undefined && $scope.showCsv !== undefined)) return;
            var queryParamters = $scope.report && $scope.report.filters ? queryParameters.parse($scope.report.filters) : '';

            var folder = '';

            if ($scope.folder != null) {
                folder = $scope.folder;
                while(folder.charAt(0) === '/') 
                    folder = folder.substr(1);

                if (folder.charAt(folder.length - 1) !== '/')
                    folder = folder + '/';
            }
            
            return config.apiPath + 'reports/' + folder + $scope.name + '/xlsx' + queryParamters;
        };
        $scope.getCsvSrc = function () {
            if ($scope.showCsv === false || $scope.showCsv === undefined) return;
            var queryParamters = $scope.report && $scope.report.filters ? queryParameters.parse($scope.report.filters) : '';

            var folder = '';

            if ($scope.folder != null) {
                folder = $scope.folder;
                while (folder.charAt(0) === '/')
                    folder = folder.substr(1);

                if (folder.charAt(folder.length - 1) !== '/')
                    folder = folder + '/';
            }

            return config.apiPath + 'reports/' + folder + $scope.name + '/csv' + queryParamters;
        };
    }
]);