﻿angular.module('phoenix.reporting', [
    'phoenix.config',
    'phoenix.shared'
]);

angular.module('phoenix.reporting').config(['$stateProvider', 'phoenix.config', function ($stateProvider, config) {
    'use strict';

    $stateProvider.state('reports', {
        url: '/reports',
        controller: 'ReportsCtrl',
        templateUrl: config.basePath + 'app/phoenix.reporting/views/report-list.html',
        resolve: {}
    }).state('reports.progressstatus', {
        url: '/progressstatus',
        templateUrl: config.basePath + 'app/phoenix.reporting/views/reports/progress-status.html',
        controller: 'ProgressStatusCtrl'
    }).state('reports.productcoursestatus', {
        url: '/productcoursestatus',
        templateUrl: config.basePath + 'app/phoenix.reporting/views/reports/product-course-status.html',
        controller: 'ProductCourseStatusCtrl'
    }).state('reports.participationsstatuspractivity', {
        url: '/participationsstatuspractivity',
        templateUrl: config.basePath + 'app/phoenix.reporting/views/reports/participation-status-pr-activity.html',
        controller: 'ParticipationStatusPrActivityCtrl'
    }).state('reports.potentialparticipations', {
        url: '/potentialparticipations',
        templateUrl: config.basePath + 'app/phoenix.reporting/views/reports/potential-participation.html',
        controller: 'PotentialParticipationCtrl'
    }).state('reports.allparticipations', {
        url: '/allparticipations',
        templateUrl: config.basePath + 'app/phoenix.reporting/views/reports/all-participation.html',
        controller: 'AllParticipationCtrl'
    }).state('reports.hubspotparticipants', {
        url: '/hubspotparticipants',
        templateUrl: config.basePath + 'app/phoenix.reporting/views/reports/hubspot-participants.html',
        controller: 'HubspotParticipantsCtrl'
    }).state('reports.allparticipationspractivity', {
        url: '/allparticipationspractivity',
        templateUrl: config.basePath + 'app/phoenix.reporting/views/reports/all-participation-pr-activity.html',
        controller: 'AllParticipationPrActivityCtrl'
    }).state('reports.prepaidclassrooms', {
        url: '/prepaidclassrooms',
        templateUrl: config.basePath + 'app/phoenix.reporting/views/reports/prepaid-classroom.html',
        controller: 'PrepaidClassroomCtrl'
    }).state('reports.ordersforinvoicing', {
        url: '/ordersforinvoicing',
        templateUrl: config.basePath + 'app/phoenix.reporting/views/reports/orders-for-invoicing.html',
        controller: 'OrdersForInvoicingCtrl'
    }).state('reports.exams', {
        url: '/exams',
        templateUrl: config.basePath + 'app/phoenix.reporting/views/reports/exams.html',
        controller: 'ExamsReportCtrl'
    }).state('reports.customerspecific_jernbaneverket', {
        url: '/customerspecific/jernbaneverket/safetycourseparticipants',
        templateUrl: config.basePath + 'app/phoenix.reporting/views/reports/customerspecific/jernbaneverket/safetycourse-participants.html',
        controller: 'SafetycourseParticipantsReportCtrl'
    }).state('reports.maillog', {
        url: '/maillog?query&page&limit&orderBy&orderDirection&mailStatus&messageType&fromDate&toDate',
        templateUrl: config.basePath + 'app/phoenix.reporting/views/mail/mail-log-list.html',
        controller: 'MailLogListCtrl',
        resolve: {
            mails: ['MailsService', '$stateParams', function (mailsService, $stateParams) {
                var options = {
                    query: $stateParams.query,
                    page: parseInt($stateParams.page, 10) || 1,
                    limit: parseInt($stateParams.limit, 10) || 15,
                    orderBy: $stateParams.orderBy,
                    orderDirection: $stateParams.orderDirection,
                    mailStatus: $stateParams.mailStatus ? $stateParams.mailStatus.split(',') : null,
                    messageType: $stateParams.messageType ? $stateParams.messageType.split(',') : null,
                    fromDate: $stateParams.fromDate || moment().subtract(1, 'days').toDate(),
                    toDate: $stateParams.toDate || moment().toDate()
                };
                return mailsService.search(options);
            }]
        }
    });
}]);