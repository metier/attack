﻿angular.module('phoenix.reporting').factory('MailsService', [
    '$http',
    'asyncHelper',
    'phoenix.config',
    function ($http, asyncHelper, config) {
        'use strict';
        return {
            search: function (options) {
                var url = config.apiPath + 'mails',
                    parameters;
                
                parameters = {
                    query: options.query,
                    orderBy: options.orderBy,
                    orderDirection: options.orderDirection,
                    messageTypeFilter: options.messageTypeFilter,
                    mailStatus: options.mailStatus,
                    messageType: options.messageType,
                    fromDate: !!options.fromDate ? moment(options.fromDate).toISOString() : null,
                    toDate: !!options.toDate ? moment(options.toDate).toISOString() : null
                };
                
                if (typeof options.limit === 'number') {
                    parameters.limit = options.limit;
                }
                if (typeof options.page === 'number' && options.page > 0) {
                    parameters.skip = (options.page - 1) * parameters.limit;
                }
                
                return asyncHelper.resolveHttp($http.get(url, { params: parameters }));
            },
            send: function (id) {
                var url = config.apiPath + 'mails/' + id + '/send';
                return $http.put(url);
            },
            preview: function (id) {
                var url = config.apiPath + 'mails/' + id + '/preview';
                return asyncHelper.resolveHttp($http.get(url));
            }
        };
    }
]);