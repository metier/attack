﻿/*global angular */
angular.module('phoenix.orders').factory('OrdersService', [
    '$http',
    '$log',
    'phoenix.config',
    'asyncHelper',
    function ($http, $log, config, asyncHelper) {
        'use strict';
        return {
            getById: function (orderId) {
                var url = config.apiPath + 'orders/' + orderId;
                return asyncHelper.resolveHttp($http.get(url));
            },
            getByGroupByKey: function (groupByKey) {
                var url = config.apiPath + 'orders';
                return asyncHelper.resolveHttp($http.get(url, { params: { groupByKey: groupByKey } }));
            },
            getOrderLinesById: function(orderId) {
                var url = config.apiPath + 'orders/orderlines';
                return asyncHelper.resolveHttp($http.get(url, { params: { id: orderId }}));
            },
            getWorkOrders: function (customerId, interceptErrors) {
                var url = config.apiPath + 'orders/workorders';
                return asyncHelper.resolveHttp($http.get(url, { params: { customerId: customerId }, interceptErrors: interceptErrors }));
            },
            create: function (order) {
                var url = config.apiPath + 'orders/';
                return asyncHelper.resolveHttp($http.post(url, order));
            },
            update: function (order) {
                var url = config.apiPath + 'orders/' + order.Id;
                return asyncHelper.resolveHttp($http.put(url, order));
            },
            addParticipants: function (order, participantIds) {
                var url = config.apiPath + 'orders/' + order.Id + '/addParticipants';
                return asyncHelper.resolveHttp($http.put(url, participantIds));
            },
            addActivities: function (order, activityIds) {
                var url = config.apiPath + 'orders/' + order.Id + '/addActivities';
                return asyncHelper.resolveHttp($http.put(url, activityIds));
            },
            removeOrderLines: function (order, orderLineIds) {
                var url = config.apiPath + 'orders/' + order.Id + '/removeOrderLines';
                return asyncHelper.resolveHttp($http.put(url, orderLineIds));
            },
            invoiceByOrderId: function (orderId, workOrder, forwardErrorCodes) {
                var url = config.apiPath + 'orders/invoice';
                return asyncHelper.resolveHttp($http.put(url, null, { params: { id: orderId, workOrder: workOrder }, forwardErrorCodes: forwardErrorCodes }));
            },
            invoiceByGroupByKey: function (groupByKey, workOrder, forwardErrorCodes) {
                var url = config.apiPath + 'orders/invoice';
                return asyncHelper.resolveHttp($http.put(url, null, { params: { groupByKey: groupByKey, workOrder: workOrder }, forwardErrorCodes: forwardErrorCodes }));
            },
            reset: function (orderId) {
                var url = config.apiPath + 'orders/' + orderId + '/reset';
                return asyncHelper.resolveHttp($http.put(url));
            },
            delete: function (orderId) {
                var url = config.apiPath + 'orders/' + orderId;
                return asyncHelper.resolveHttp($http.delete(url));
            },
            createCreditNote: function (orderId) {
                var url = config.apiPath + 'orders/' + orderId + '/creditnote';
                return asyncHelper.resolveHttp($http.put(url));
            },
            credit: function (orderId) {
                var url = config.apiPath + 'orders/' + orderId + '/credit';
                return asyncHelper.resolveHttp($http.put(url));
            },
            search: function (options) {
                var url = config.apiPath + 'orders',
                    parameters;

                parameters = {
                    query: options.query,
                    orderBy: options.orderBy,
                    orderDirection: options.orderDirection,
                    customerTypeFilter: options.customerTypeFilter,
                    statusFilter: options.statusFilter,
                    distributorId: options.distributorId,
                    coordinatorId: options.coordinatorId,   
                    fromDate: !!options.fromDate ? moment(options.fromDate).toISOString() : null,
                    toDate: !!options.toDate ? moment(options.toDate).toISOString() : null
                };

                if (typeof options.limit === 'number') {
                    parameters.limit = options.limit;
                }
                if (typeof options.page === 'number' && options.page > 0) {
                    parameters.skip = (options.page - 1) * parameters.limit;
                }
                return asyncHelper.resolveHttp($http.get(url, { params: parameters }));
            }
        };
}]);