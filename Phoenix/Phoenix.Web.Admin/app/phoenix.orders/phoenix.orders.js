﻿angular.module('phoenix.orders', [
    'phoenix.config',
    'phoenix.shared',
    'phoenix.customers',
    'ui.bootstrap',
    'ui.utils',
    'placeholderShim',
    'ui.router']
    )
.config([
    'phoenix.config',
    '$stateProvider',
    function (config, $stateProvider) {
        'use strict';
        $stateProvider.state('orderlist', {
            url: '/orders?query&page&limit&orderBy&orderDirection&statusFilter&customerTypeFilter&coordinatorId&fromDate&toDate',
            controller: 'OrderListCtrl',
            templateUrl: config.basePath + 'app/phoenix.orders/views/order-list.html',
            resolve: {
                loggedInUser: ['AuthenticationService', function(authenticationService) {
                    return authenticationService.getLoggedInUser();
                }],
                orders: ['OrdersService', 'DistributorsService', '$stateParams', function (ordersService, distributorsService, $stateParams) {
                    var options = {
                        distributorId: distributorsService.get().Id,
                        coordinatorId: $stateParams.coordinatorId,
                        query: $stateParams.query,
                        page: parseInt($stateParams.page, 10) || 1,
                        limit: parseInt($stateParams.limit, 10) || 15,
                        orderBy: $stateParams.orderBy,
                        orderDirection: $stateParams.orderDirection,
                        statusFilter: $stateParams.statusFilter ? $stateParams.statusFilter.split(',') : null,
                        customerTypeFilter: $stateParams.customerTypeFilter ? $stateParams.customerTypeFilter.split(',') : null,
                        fromDate: $stateParams.fromDate || moment({ month: moment().month() - 1, day: 1 }).toDate(),
                        toDate: $stateParams.toDate || moment().toDate()
                    };
                    return ordersService.search(options);
                }]
            }
        });
        
        $stateProvider.state('orderproposed', {
            url: '/orders/proposed/:groupByKey',
            templateUrl: config.basePath + 'app/phoenix.orders/views/order.html',
            abstract: true,
            controller: 'OrderCtrl',
            resolve: {
                data: ['OrdersService', '$stateParams', function (ordersService, $stateParams) {
                    return ordersService.getByGroupByKey($stateParams.groupByKey).then(function (order) {
                        return {
                            order: order,
                            orderLines: order.OrderLines
                        };
                    });
                }],
                codes: ['CodesService', function (codesService) {
                    return codesService.getAll();
                }]
            }
        }).state('orderproposed.view', {
            url: '?selectedTab',
            controller: 'OrderViewCtrl',
            templateUrl: config.basePath + 'app/phoenix.orders/views/order-view.html'
        });
        
        $stateProvider.state('order', {
            templateUrl: config.basePath + 'app/phoenix.orders/views/order.html',
            abstract: true,
            url: '/orders/:orderId',
            controller: 'OrderCtrl',
            resolve: {
                data: ['OrdersService', '$stateParams', '$q', function (ordersService, $stateParams, $q) {
                    return ordersService.getById($stateParams.orderId).then(function (order) {
                        var promises = [ordersService.getWorkOrders(order.CustomerId)];
                        if (order.CreditedOrderId) {
                            promises.push(ordersService.getById(order.CreditedOrderId));
                        }

                        return $q.all(promises).then(function(results) {
                            return {
                                order: order,
                                orderLines: order.OrderLines,
                                creditedOrder: results[1],
                                workOrders: results[0]
                            };
                        });
                    });
                }],
                codes: ['CodesService', function (codesService) {
                    return codesService.getAll();
                }]
            }
        }).state('order.edit', {
            url: '/edit?selectedTab',
            templateUrl: config.basePath + 'app/phoenix.orders/views/order-edit.html',
            controller: 'OrderEditCtrl'
        }).state('order.view', {
            url: '?selectedTab',
            controller: 'OrderViewCtrl',
            templateUrl: config.basePath + 'app/phoenix.orders/views/order-view.html'
        });

    }
]);