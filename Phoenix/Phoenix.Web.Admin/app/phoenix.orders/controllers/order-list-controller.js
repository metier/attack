angular.module('phoenix.orders').controller('OrderListCtrl', [
    'phoenix.config',
    '$scope',
    '$state',
    '$location',
    '$q',
    '$modal',
    'orders',
    'loggedInUser',
    'orderStatuses',
    'customerTypes',
    'OrdersService',
    'NotificationService',
    function (config, $scope, $state, $location, $q, $modal, orders, loggedInUser, orderStatuses, customerTypes, ordersService, notificationService) {
        'use strict';
        var self = this;
        
        $scope.fromDate = !!$location.search().fromDate ? moment($location.search().fromDate).toDate() : moment({ month: moment().month() - 1, day: 1}).toDate();
        $scope.toDate = !!$location.search().toDate ? moment($location.search().toDate).toDate() : moment().toDate();

        $scope.loggedInCoordinatorId = loggedInUser.User.Id;
        $scope.statusFilterValues = orderStatuses;
        $scope.customerTypeFilterValues = customerTypes;

        $scope.orders = orders || { TotalCount: 0, Items: [] };
        $scope.query = $location.search().query;
        $scope.orderBy = $location.search().orderBy || '';
        $scope.orderDirection = $location.search().orderDirection || '';
        $scope.page = $location.search().page;
        $scope.limit = $location.search().limit || 15;
        $scope.maxPages = $scope.orders.TotalCount;
        $scope.selectedOrders = [];

        $scope.navigate = function (page) {
            if (page < 1 || page > $scope.maxPages) {
                return;
            }
            $location.search('page', page);
        };

        $scope.orderSelected = function () {
            var selectableOrders = _($scope.orders.Items).filter(function (o) {
                return $scope.canSelectOrder(o);
            });

            $scope.canSelectAll = selectableOrders.length > 0;
            $scope.selectedOrders = self.getSelectedOrders();

            $scope.isAllOrdersSelected = selectableOrders.length === $scope.selectedOrders.length && selectableOrders.length > 0;
        };

        $scope.toggleSelectAllOrders = function () {
            _($scope.orders.Items).each(function (o) {
                if ($scope.canSelectOrder(o)) {
                    o.isSelected = !!$scope.isAllOrdersSelected;
                }
            });
            $scope.orderSelected();
        };

        $scope.canSelectOrder = function (order) {
            return (order.OrderStatus === 0 || order.OrderStatus === 1) && !order.IsCreditNote;
        };

        function getWorkOrdersForCustomer(customer) {
            var defer = $q.defer();
            ordersService.getWorkOrders(customer.CustomerId, false).then(function (result) {
                var customerWorkOrders = {
                    CustomerId: customer.CustomerId,
                    CustomerName: customer.CustomerName,
                    WorkOrders: result
                };
                defer.resolve(customerWorkOrders);
            }, function () {
                var customerWorkOrders = {
                    CustomerId: customer.CustomerId,
                    CustomerName: customer.CustomerName
                };
                defer.resolve(customerWorkOrders);
            });
            return defer.promise;
        }

        function getSingleWorkOrderPerCustomer(allCustomerWorkOrders) {
            var defer = $q.defer();

            var singleWorkOrderCustomers = _.chain(allCustomerWorkOrders).filter(function (item) {
                return !item.WorkOrders || item.WorkOrders.length < 2;
            }).map(function (item) {
                var workorder;
                if (item.WorkOrders) {
                    workorder = item.WorkOrders[0];
                }

                return { CustomerId: item.CustomerId, WorkOrder: workorder };
            }).value();

            var multipleWorkOrderCustomers = _(allCustomerWorkOrders).filter(function (customerWorkOrders) {
                return !!customerWorkOrders.WorkOrders && customerWorkOrders.WorkOrders.length >= 2;
            });

            if (multipleWorkOrderCustomers.length > 0) {
                $modal.open({
                    backdrop: 'static',
                    templateUrl: config.basePath + 'app/phoenix.orders/views/modals/select-workorders-modal.html',
                    controller: 'SelectWorkordersModalCtrl',
                    resolve: {
                        customerWorkOrdersCollection: function () { return multipleWorkOrderCustomers; }
                    }
                }).result.then(function (workordersSelectResult) {
                    defer.resolve(workordersSelectResult.concat(singleWorkOrderCustomers));
                }, function () {
                    defer.reject();
                });
            } else {
                defer.resolve(singleWorkOrderCustomers);
            }
            
            return defer.promise;
        }

        function getWorkordersForSelectedOrders() {
            var uniqueCustomers = _.uniq(_($scope.selectedOrders).map(function (searchableOrder) {
                return {
                    CustomerId: searchableOrder.CustomerId,
                    CustomerName: searchableOrder.Name
                };
            }), function (item) {
                return item.CustomerId;
            });

            var workOrdersForCustomerPromises = _(uniqueCustomers).map(function (customer) {
                return getWorkOrdersForCustomer(customer);
            });
            return $q.all(workOrdersForCustomerPromises);
        }

        
        function invoiceSelectedOrders(workorderPerCustomer, missingWorkOrders) {
            var invoiceOrderPromises = _($scope.selectedOrders).map(function (o) {
                o.isBusySendingInvoice = true;

                var customerWorkOrder = _(workorderPerCustomer).find(function(element) {
                    return element.CustomerId === o.CustomerId;
                }) || {};

                var invoicePromise = o.OrderStatus === 0 ?
                    ordersService.invoiceByGroupByKey(o.GroupByKey, customerWorkOrder.WorkOrder, 1001) :
                    ordersService.invoiceByOrderId(o.OrderId, customerWorkOrder.WorkOrder, 1001);

                return invoicePromise.then(function(invoiced) {
                    o.isBusySendingInvoice = false;
                    o.isSelected = false;

                    o.GroupByKey = null;
                    o.OrderId = invoiced.Id;
                    o.OrderStatus = invoiced.Status;
                    o.OrderNumber = invoiced.OrderNumber;

                    $scope.orderSelected();
                }, function(error) {
                    o.isBusySendingInvoice = false;
                    $scope.orderSelected();
                    if (error.ErrorCode === 1001) {
                        missingWorkOrders.push(o);
                    }
                });
            });

            return $q.all(invoiceOrderPromises);
        }

        $scope.sendSelectedInvoices = function () {
            if (confirm("Are you sure you want to send the selected orders to invoicing?")) {
                var missingWorkOrders = [];
                $scope.isBusySendingInvoices = true;

                getWorkordersForSelectedOrders()
                    .then(getSingleWorkOrderPerCustomer)
                    .then(function(workOrderPerCustomer) {
                        return invoiceSelectedOrders(workOrderPerCustomer, missingWorkOrders);
                    })
                    .finally(function () {
                        if (missingWorkOrders.length > 0) {
                            var uniqueCustomerNames = _.chain(missingWorkOrders).uniq(function(o) {
                                return o.Name;
                            }).map(function(o) {
                                return o.Name;
                            }).value();
                            var message = uniqueCustomerNames.join(', ') + " has no work orders in Agresso and could not be invoiced";
                            notificationService.alertWarning({
                                message: message,
                                timeOut: 60000
                            });
                        }
                        $scope.isBusySendingInvoices = false;
                    });
            }
        };

        $scope.applyFilter = function () {
            if ($scope.filterByLoggedInCoordinator) {
                $location.search('coordinatorId', $scope.loggedInCoordinatorId);
            } else {
                $location.search('coordinatorId', null);
            }

            if ($scope.fromDate && moment($scope.fromDate).isValid()) {
                $location.search('fromDate', moment($scope.fromDate).format('YYYY-MM-DD'));
            } else {
                $location.search('fromDate', null);
            }

            if ($scope.toDate && moment($scope.toDate).isValid()) {
                $location.search('toDate', moment($scope.toDate).format('YYYY-MM-DD'));
            } else {
                $location.search('toDate', null);
            }

            if ($scope.query) {
                $location.search('query', $scope.query);
            } else {
                $location.search('query', null);
            }
        };
        
        self.getSelectedOrders = function () {
            return _($scope.orders.Items).filter(function (o) {
                return o.isSelected;
            });
        };
        
        self.init = function () {
            var urlCoordinatorId = $location.search().coordinatorId;
            $scope.filterByLoggedInCoordinator = !!urlCoordinatorId && parseInt(urlCoordinatorId, 10) === $scope.loggedInCoordinatorId;
            $scope.orderSelected();
        };

        self.init();
    }
]);