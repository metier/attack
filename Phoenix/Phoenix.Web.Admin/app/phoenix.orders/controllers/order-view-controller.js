﻿angular.module('phoenix.orders').controller('OrderViewCtrl', [
    '$scope',
    '$state',
    'data',
    function ($scope, $state, data) {
        'use strict';
        $scope.order = data.order;
        $scope.orderLines = data.orderLines;
        $scope.workOrders = data.workOrders;
    }
]);