﻿angular.module('phoenix.orders').controller('OrderEditCtrl', [
    '$scope',
    '$state',
    'data',
    'OrdersService',
    function ($scope, $state, data, ordersService) {
        'use strict';
        $scope.order = angular.copy(data.order);
        $scope.orderLines = angular.copy(data.orderLines);
        $scope.isBusySaving = false;
        
        $scope.save = function () {
            $scope.isBusySaving = true;
            ordersService.update($scope.order).then(function (updated) {
                $scope.isBusySaving = false;
                $state.go('order.view', { orderId: updated.Id, selectedTab: $state.params.selectedTab }, { reload: true });
            }, function() {
                $scope.isBusySaving = false;
            });
        };

        $scope.create = function () {
            $scope.isBusySaving = true;
            ordersService.create($scope.order).then(function (created) {
                $scope.isBusySaving = false;
                $state.go('order.view', { orderId: created.Id }, { reload: true });
            }, function() {
                $scope.isBusySaving = false;
            });
        };

        $scope.cancel = function () {
            $state.go('order.view', { selectedTab: $state.params.selectedTab });
        };

        $scope.$on('OrderUpdated', function (event, order) {
            $scope.order.FixedPriceActivities = order.FixedPriceActivities;
            $scope.order.Participants = order.Participants;
            $scope.order.RowVersion = order.RowVersion;
        });
    }
]);