﻿angular.module('phoenix.orders').controller('SelectWorkordersModalCtrl', [
    '$scope',
    '$modalInstance', 'customerWorkOrdersCollection', function($scope, $modalInstance, customerWorkOrdersCollection) {
        'use strict';
        $scope.customerWorkOrdersCollection = customerWorkOrdersCollection;
        $scope.customerWorkOrderCollection = [];

        $scope.onWorkorderSelected = function() {
            $scope.customerWorkOrderCollection = _.chain($scope.customerWorkOrdersCollection).filter(function (customerWorkOrders) {
                return !!customerWorkOrders.SelectedWorkOrder;
            }).map(function(item) {
                return {
                    CustomerId: item.CustomerId,
                    WorkOrder: item.SelectedWorkOrder
                };
            }).value();
        };

        $scope.ok = function() {
            $modalInstance.close($scope.customerWorkOrderCollection);
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    }
]);