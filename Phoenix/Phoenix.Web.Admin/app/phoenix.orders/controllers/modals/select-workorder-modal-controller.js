﻿angular.module('phoenix.orders').controller('SelectWorkorderModalCtrl', ['$scope',
    '$modalInstance', 'workorders', function ($scope, $modalInstance, workorders) {
        'use strict';
        $scope.workOrders = workorders;
		$scope.locals =
		{
			workOrder: workorders[0]
		};
        
        $scope.ok = function () {
            $modalInstance.close($scope.locals.workOrder);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);