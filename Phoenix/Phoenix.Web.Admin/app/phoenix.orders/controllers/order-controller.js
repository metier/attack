angular.module('phoenix.orders').controller('OrderCtrl', [
    '$scope',
    '$state',
    '$modal',
    '$q',
    'data',
    'codes',
    'phoenix.config',
    'OrdersService',
    'ActivityModalHelper',
    'ParticipantsService',
    'LeadingTextsService',
    'CustomersService',
    'DistributorsService',
    'NotificationService',
    'ExamsService',
    function ($scope, $state, $modal, $q, data, codes, config, ordersService, activityModalHelper, participantsService, leadingTextsService, customersService, distributorsService, notificationService, examsService) {
        'use strict';
        var self = this;

        $scope.order = data.order;
        $scope.orderLines = data.orderLines;
        $scope.creditedOrder = data.creditedOrder;
        $scope.codes = codes;

        self.init = function () {
            $scope.orderLinesDisplay = [];
            $scope.totalAmount = 0;
            $scope.currencyCode = '';
            $scope.isBusyDeletingOrderLines = false;
            
            if ($scope.orderLines.length === 0) {
                return;
            }

            _($scope.orderLines).each(function(ol){
                ol.isSelected = false;
            });
            
            // Extract the different types of order lines
            self.fixedPriceActivityOrderLines = _($scope.orderLines).where({ Type: 1 });
            self.unitPriceActivityOrderLines = _($scope.orderLines).where({ Type: 2 });
            self.participantOrderLines = _($scope.orderLines).where({ Type: 3 });

            // Fill with fixed price activities
            _(self.fixedPriceActivityOrderLines).each(function (ol) {
                ol.Quantity = $scope.order.IsCreditNote ? -1 : 1;
                ol.TotalPrice = ol.Price * ol.Quantity;
                $scope.orderLinesDisplay.push(ol);
            });

            // Fill with activities from participants grouped by price
            _(self.unitPriceActivityOrderLines).each(function (activity) {
                var participantPriceGroup = _.chain(self.participantOrderLines).where({ ParentActivityId: activity.Id }).groupBy('Price').value();

                _(participantPriceGroup).each(function (participants) {
                    var activityOrderLine = angular.copy(activity);
                    activityOrderLine.Quantity = participants.length * ($scope.order.IsCreditNote ? -1 : 1);
                    activityOrderLine.Price = participants[0].Price;
                    activityOrderLine.CurrencyCodeId = participants[0].CurrencyCodeId;
                    activityOrderLine.TotalPrice = activityOrderLine.Price * activityOrderLine.Quantity;
//                    activityOrderLine.TotalPrice = _(participants).reduce(function (prev, current) { return prev + (current.Price || 0); }, 0);
                    $scope.orderLinesDisplay.push(activityOrderLine);

                    _(participants).each(function (p) {
                        $scope.orderLinesDisplay.push(p);
                    });
                });
            });

            // Calculate total amount
            $scope.totalAmount = _($scope.orderLinesDisplay).reduce(function (prev, current) {
                return prev + (current.TotalPrice || 0);
            }, 0);

            $scope.isCurrencyCodeAmbigous = self.hasAmbigousCurrencies($scope.orderLinesDisplay);
            $scope.currencyCode = $scope.getCurrencyCode($scope.orderLinesDisplay[0].CurrencyCodeId);
        };

        $scope.editActivity = function (activityId) {
            var promises = activityModalHelper.openEditModal(activityId);
            promises.saved.then(self.refreshOrderLines);
        };

        self.hasAmbigousCurrencies = function (orderLines) {
            var currencyCodes = _.chain(orderLines)
                                 .map(function (ol) { return ol.CurrencyCodeId; })
                                 .uniq()
                                 .value();
            return currencyCodes.length > 1;
        };

        self.refreshOrderLines = function () {
            if ($scope.isProposed) {
                $state.go($state.current, $state.params, { reload: true });
            } else {
                ordersService.getOrderLinesById($state.params.orderId).then(function (orderLines) {
                    $scope.orderLines = orderLines;
                    self.init();
                });
            }
        };

        $scope.editParticipant = function (participantId) {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.customers.activities/views/modals/participant-edit-modal.html',
                controller: 'ParticipantEditModalCtrl',
                resolve: {
                    participant: function () { return participantsService.get(participantId); },
                    customer: function () { return customersService.get($scope.order.CustomerId); },
                    distributors: function () { return distributorsService.getAll(); },
                    currentOrder: function () { return $scope.order; },
                    predefinedLeadingTexts: function () { return leadingTextsService.getAllPredefined(); },
                    codes: function () { return codes; },
                    course: function () { return participantsService.getParticipantCourse(participantId); },
                    examContainer: function () { return participantsService.getParticipantExamContainer(participantId); },
                    exam: function () { return examsService.getExamResult(participantId); },
                    article: function () { return participantsService.getArticle(participantId); },
                    participantGradingComments: ['CommentsService', function (commentsService) { return commentsService.get('participantGrading', participantId); }]
                },
                windowClass: 'modal-wider'
            });
            modalInstance.result.then(self.refreshOrderLines);
        };

        $scope.validateOrderLineCurrency = function (orderLine) {
            var result = {}, children, parent;
            if (orderLine.Type === 2) {
                children = self.getChildren(orderLine);
                result.isAmbigous = self.hasAmbigousCurrencies(children);
            } else if (orderLine.Type === 3) {
                parent = self.getParent(orderLine);
                result.isAmbigous = $scope.validateOrderLineCurrency(parent).isAmbigous;
            } else {
                result.isAmbigous = false;
            }
            return result;
        };

        $scope.getCurrencyCode = function (currencyCodeId) {
            if (!currencyCodeId) {
                return '';
            }
            return _($scope.codes.currencyCodes).find(function (c) { return c.Id === currencyCodeId; }).Value;
        };

        self.getChildren = function(orderLine) {
            if (orderLine.Type === 2) {
                return _($scope.orderLinesDisplay).where({ Type: 3, Price: orderLine.Price, ParentActivityId: orderLine.Id });
            }
            return [];
        };

        self.getParent = function (orderLine) {
            if (orderLine.Type === 3) {
                return  _($scope.orderLinesDisplay).findWhere({ Type: 2, Price: orderLine.Price, Id: orderLine.ParentActivityId });
            }
            return undefined;
        };

        self.setCloseState = function (fromState, fromParams) {
            // Navigate on "Close" to where you came from. Default to order list
            if (!self.closeState) {
                if (!fromState.name || fromState.name.indexOf('order') === 0 || fromState.name.indexOf('login') === 0) {
                    self.closeState = 'orderlist';
                    self.closeParams = fromParams;
                } else {
                    self.closeState = fromState;
                    self.closeParams = fromParams;
                }
            }
        };

        $scope.edit = function () {
            $state.go('order.edit', { selectedTab: $state.params.selectedTab });
        };

        $scope.editProposed = function () {
            $scope.isBusyCreatingProposed = true;
            ordersService.create($scope.order).then(function (created) {
                $scope.isBusyCreatingProposed = false;
                $state.go('order.edit', { orderId: created.Id, selectedTab: $state.params.selectedTab });
            }, function () {
                $scope.isBusyCreatingProposed = false;
            });
        };

        $scope.resetOrder = function () {
            $scope.isBusyResetting = true;
            ordersService.reset($scope.order.Id).then(function (reset) {
                $scope.isBusyResetting = false;
                $scope.order = reset;
                $state.go('order.edit', { orderId: $scope.order.Id, selectedTab: $state.params.selectedTab }, { reload: true });
            }, function () {
                $scope.isBusyResetting = false;
            });
        };

        $scope.removeActivity = function (orderLine) {
            var activity = _($scope.order.FixedPriceActivities).find(function (a) {
                return a === orderLine.Id;
            });
            if (activity) {
                var index = $scope.order.FixedPriceActivities.indexOf(activity);
                $scope.order.FixedPriceActivities.splice(index, 1);
            }
        };
        

        $scope.getSelectedOrderLines = function (orderLineType) {
            if(!orderLineType)
                orderLineType = -1;
                
            return _($scope.orderLines).filter(function (o) {
                return (o.isSelected && (o.Type === orderLineType || orderLineType < 0));
            });
        };

        $scope.getSelectedFixedPriceActivities = function () {
            return $scope.getSelectedOrderLines(1);
        };

        $scope.getSelectedParticipants = function () {
            return $scope.getSelectedOrderLines(3);
        };

        $scope.getSelectedChildren = function (parentOrderLine) {
            var allSelectedParticipants = $scope.getSelectedParticipants();

            return _.filter(allSelectedParticipants, function(participant){
                return participant.ParentActivityId === parentOrderLine.Id;
            });
        };

        $scope.removeSelectedOrderLines = function() {
            if (confirm("Are you sure you want to remove all the selected order lines from the order?")) {
                $scope.isBusyDeletingOrderLines = true;

                var participantsToRemove = $scope.getSelectedParticipants();
                _.each(participantsToRemove, function(participant) {
                    $scope.removeParticipant(participant);
                });

                var activitiesToRemove = $scope.getSelectedFixedPriceActivities();
                _.each(activitiesToRemove, function(activity) {
                    $scope.removeActivity(activity);
                });

                var orderlineIds = _.union(_.pluck(participantsToRemove, 'Id'), _.pluck(activitiesToRemove, 'Id'));

                ordersService.removeOrderLines($scope.order, orderlineIds).then(function (updated) {
                    $scope.order = updated;
                    self.refreshOrderLines();
                    $scope.$broadcast('OrderUpdated', updated);
                    $scope.isBusyDeletingOrderLines = false;
                });
            }
        }

        $scope.toggleSelection = function (orderLine) {
            orderLine.isSelected = !orderLine.isSelected;

            var children = self.getChildren(orderLine);
            _(children).each(function (child) {
                child.isSelected = orderLine.isSelected;
            });

            var parent = self.getParent(orderLine);
            if(parent){
                parent.isSelected = ($scope.getSelectedChildren(parent).length === self.getChildren(parent).length);
            };
        };


        $scope.removeParticipant = function (orderLine) {
                var participant = _($scope.order.Participants).find(function (p) {
                    return p === orderLine.Id;
                });
                if (participant) {
                    var index = $scope.order.Participants.indexOf(participant);
                    $scope.order.Participants.splice(index, 1);
                }
        };

        $scope.addParticipants = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.customers.activities/views/modals/participants-select-modal.html',
                controller: 'ParticipantsSelectModalCtrl',
                resolve: {
                    customerId: function () { return $scope.order.CustomerId; },
                    isInvoiced: function () { return false; }
                },
                windowClass: 'modal-wider'
            });
            modalInstance.result.then(function (selectedParticipants) {
                var participantIds = [];

                _(selectedParticipants).each(function(participant) {
                    participantIds.push(participant.Id);
                });

                return ordersService.addParticipants($scope.order, participantIds).then(function (updated) {
                    $scope.order = updated;
                    self.refreshOrderLines();
                    $scope.$broadcast('OrderUpdated', updated);
                });
            });
            
        };

        $scope.addFixedPriceActivities = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.customers.activities/views/modals/activities-select-modal.html',
                controller: 'ActivitiesSelectModalCtrl',
                resolve: {
                    canSearchAllCustomers: function () { return false; },
                    onlyActivitiesProposedForOrder: function () { return true; },
                    customerId: function () { return $scope.order.CustomerId; }
                },
                windowClass: 'modal-wider'
            });
            modalInstance.result.then(function (selectedActivities) {
                var activityIds = [];

                _(selectedActivities).each(function (activity) {
                    activityIds.push(activity.ActivityId);
                    });

                    return ordersService.addActivities($scope.order, activityIds).then(function (updated) {
                        $scope.order = updated;
                        self.refreshOrderLines();
                        $scope.$broadcast('OrderUpdated', updated);
                });
            });
        };
        
        self.updateOrder = function () {
            return ordersService.update($scope.order).then(function (updated) {
                $scope.order = updated;
                self.refreshOrderLines();
                $scope.$broadcast('OrderUpdated', updated);
            });
        };

        self.updateOrderFromOrderLine = function (orderLine) {
            orderLine.isBusyRemoving = true;
            self.updateOrder().then(function () {
                orderLine.isBusyRemoving = false;
            }, function() {
                orderLine.isBusyRemoving = false;
            });
        };


        function loadWorkOrders(order) {
            var deferred = $q.defer();

            ordersService.getWorkOrders(order.CustomerId, false).then(function (workOrder) {
                $scope.workOrders = workOrder;
            }).finally(function() {
                deferred.resolve();
            });

            return deferred.promise;
        }


        function selectWorkOrderForOrder() {
            var deferred = $q.defer();

            if ($scope.workOrders && $scope.workOrders.length > 0 && !$scope.order.WorkOrder) {
                if ($scope.workOrders.length === 1) {
                    deferred.resolve($scope.workOrders[0]);
                } else {
                    var modalInstance = $modal.open({
                        backdrop: 'static',
                        templateUrl: config.basePath + 'app/phoenix.orders/views/modals/select-workorder-modal.html',
                        controller: 'SelectWorkorderModalCtrl',
                        resolve: {
                            workorders: function () { return $scope.workOrders; }
                        }
                    });

                    modalInstance.result.then(function (result) {
                        deferred.resolve(result);
                    }, function() {
                        deferred.reject();
                    });
                }
            } else {
                deferred.resolve($scope.order.WorkOrder);
            }

            return deferred.promise;
        }

        $scope.invoice = function () {
            if (confirm("Are you sure you want to send the order to invoicing?")) {
                selectWorkOrderForOrder().then(function (workOrder) {
                    $scope.isBusyInvoicing = true;
                    if ($scope.isProposed) {
                        ordersService.invoiceByGroupByKey($state.params.groupByKey, workOrder).then(function (invoiced) {
                            $scope.isBusyInvoicing = false;
                            $state.go('order.view', { orderId: invoiced.Id, selectedTab: $state.params.selectedTab });
                        }, function() {
                            $scope.isBusyInvoicing = false;
                        });
                    } else {
                        ordersService.invoiceByOrderId($scope.order.Id, workOrder).then(function () {
                            $scope.isBusyInvoicing = false;
                            $state.go($state.current, $state.params, { reload: true });
                        }, function() {
                            $scope.isBusyInvoicing = false;
                        });
                    }
                }, function () {
                    notificationService.alertError({
                        header: 'No work order selected',
                        message: 'Work order must be selected for order'
                    });
                });
            }
        };
        
        $scope.editCreditNote = function () {
            $scope.isBusyCreatingCreditNote = true;
            ordersService.createCreditNote($scope.order.Id).then(function (created) {
                $scope.isBusyCreatingCreditNote = false;
                $state.go('order.edit', { orderId: created.Id });
            }, function() {
                $scope.isBusyCreatingCreditNote = false;
            });
        };

        $scope.sendCreditNote = function () {
            $scope.isBusySendingCreditNote = true;
            ordersService.credit($scope.order.Id).then(function (credited) {
                $scope.isBusySendingCreditNote = false;
                $state.go('order.view', { orderId: credited.Id }, { reload: true });
            }, function() {
                $scope.isBusySendingCreditNote = false;
            });
        };

 
        $scope.delete = function () {
            if (confirm("Are you sure you want to delete the order draft?")) {
                $scope.isBusyDeleting = true;
                ordersService.delete($scope.order.Id).then(function () {
                    $scope.isBusyDeleting = false;
                    $state.go(self.closeState, self.closeParams);
                }, function() {
                    $scope.isBusyDeleting = false;
                });
            }
        };

        $scope.close = function () {
            $state.go(self.closeState, self.closeParams);
        };

        $scope.isCommentsLoaded = false;

        $scope.commentsUpdated = function (comments) {
            $scope.isCommentsLoaded = true;
            $scope.commentCount = comments.length;
        };

        $scope.selectTab = function (tabName) {
            $state.go($state.current.name, { selectedTab: tabName });
        };
        
        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            self.setCloseState(fromState, fromParams);
            $scope.isEditing = $state.is('order.edit');
            $scope.isProposed = toState.name.indexOf('orderproposed') === 0;

            $scope.isDetailsTabSelected = $state.params.selectedTab === 'details';
            $scope.isCommentsTabSelected = $state.params.selectedTab === 'comments';

            if (!$state.params.selectedTab) {
                $scope.selectTab('details');
            }
        });

        function init() {
            $scope.orderLinesDisplay = [];
            $scope.totalAmount = 0;
            $scope.currencyCode = '';
            $scope.isLoadingWorkOrders = true;

            loadWorkOrders($scope.order).then(function() {
                $scope.isLoadingWorkOrders = false;
            });

            if ($scope.orderLines.length === 0) {
                return;
    }

            // Extract the different types of order lines
            self.fixedPriceActivityOrderLines = _($scope.orderLines).where({ Type: 1 });
            self.unitPriceActivityOrderLines = _($scope.orderLines).where({ Type: 2 });
            self.participantOrderLines = _($scope.orderLines).where({ Type: 3 });

            // Fill with fixed price activities
            _(self.fixedPriceActivityOrderLines).each(function (ol) {
                ol.Quantity = $scope.order.IsCreditNote ? -1 : 1;
                ol.TotalPrice = ol.Price * ol.Quantity;
                $scope.orderLinesDisplay.push(ol);
            });

            // Fill with activities from participants grouped by price
            _(self.unitPriceActivityOrderLines).each(function (activity) {
                var participantPriceGroup = _.chain(self.participantOrderLines).where({ ParentActivityId: activity.Id }).groupBy('Price').value();

                _(participantPriceGroup).each(function (participants) {
                    var activityOrderLine = angular.copy(activity);
                    activityOrderLine.Quantity = participants.length * ($scope.order.IsCreditNote ? -1 : 1);
                    activityOrderLine.Price = participants[0].Price;
                    activityOrderLine.CurrencyCodeId = participants[0].CurrencyCodeId;
                    activityOrderLine.TotalPrice = activityOrderLine.Price * activityOrderLine.Quantity;
                    //                    activityOrderLine.TotalPrice = _(participants).reduce(function (prev, current) { return prev + (current.Price || 0); }, 0);
                    $scope.orderLinesDisplay.push(activityOrderLine);

                    _(participants).each(function (p) {
                        $scope.orderLinesDisplay.push(p);
                    });
                });
            });

            // Calculate total amount
            $scope.totalAmount = _($scope.orderLinesDisplay).reduce(function (prev, current) {
                return prev + (current.TotalPrice || 0);
            }, 0);

            $scope.isCurrencyCodeAmbigous = self.hasAmbigousCurrencies($scope.orderLinesDisplay);
            $scope.currencyCode = $scope.getCurrencyCode($scope.orderLinesDisplay[0].CurrencyCodeId);
        };


        init();
    }
]);
