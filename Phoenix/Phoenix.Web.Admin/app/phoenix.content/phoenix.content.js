﻿angular.module('content', [
    'ui.router',
    'phoenix.shared',
    'phoenix.exam',
    'phoenix.config'
]).config([
    '$urlRouterProvider',
    '$httpProvider',
    '$stateProvider',
    function ($urlRouterProvider, $httpProvider, $stateProvider) {
        'use strict';
        $stateProvider.state('login', {
            onEnter: ['$window', '$location', 'phoenix.config', function ($window, $location, config) {
                var returnUrl = $location.absUrl();
                $window.location = config.basePath + '#/login?returnUrl=' + returnUrl;
            }]
        });
        $urlRouterProvider.otherwise("/exam/questions/");
        $httpProvider.interceptors.push('errorHandlingHttpInterceptor');
    }
]);