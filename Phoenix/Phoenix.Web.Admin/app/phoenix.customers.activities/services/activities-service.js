﻿angular.module('phoenix.customers').factory('ActivitiesService', [
    '$http',
    'asyncHelper',
    'phoenix.config',
    function ($http, asyncHelper, config) {
        'use strict';
        return {
            // Set searchable to true if searchable types should be returned. Default is false
            get: function (id, searchable) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'activities/' + id, { params: { searchable: searchable || false }}), true);
            },
            getCurrentActivityOrder: function (id) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'activities/' + id + '/currentorder'), false);
            },
            getOrderLine: function (id) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'activities/' + id + '/orderline'), false);
            },
            getActivitySets: function(id) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'activities/' + id + '/activitysets'), false);
            },
            getExamStatuses: function(id) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'activities/' + id + '/examstatuses'), false);
            },
            getEnrollmentConditions: function (id) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'activities/' + id + '/enrollmentconditions'), false);
            },
            saveEnrollmentConditions: function (enrollmentConditions, activityId) {
                _.each(enrollmentConditions, function (condition) {
                    condition.ActivityId = activityId;
                });

                var url = config.apiPath + 'activities' + "/" + activityId + "/enrollmentconditions";
                return asyncHelper.resolveHttp($http.put(url, enrollmentConditions));
            },
            search: function (options) {
                var parameters = {
                    distributorId: options.distributorId,
                    customerId: options.customerId,
                    customerArticleId: options.customerArticleId,
                    activitySetId: options.activitySetId,
                    articleOriginalId: options.articleOriginalId,
                    includeActivitySets: options.includeActivitySets,
                    query: options.query,
                    isproposed: options.isProposed,
                    hasFixedPrice: options.hasFixedPrice,
                    productId: options.productId,
                    limit: options.limit,
                    orderBy: options.orderBy,
                    orderDirection: options.orderDirection
                };

                if (typeof options.page === 'number' && options.page > 0) {
                    parameters.skip = (options.page - 1) * parameters.limit;
                }
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'activities', { params: parameters }));
            },
            create: function (activity) {
                return asyncHelper.resolveHttp($http.post(config.apiPath + 'activities', activity));
            },
            copy: function (activity, copyFromActivityId) {
                return asyncHelper.resolveHttp($http.post(config.apiPath + 'activities/copy/' + copyFromActivityId, activity));
            },
            update: function (activity) {
                return asyncHelper.resolveHttp($http.put(config.apiPath + 'activities/' + activity.Id, activity));
            }
        };
    }
]);