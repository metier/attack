﻿angular.module('phoenix.customers').factory('ParticipantsService', [
    '$http',
    'asyncHelper',
    'phoenix.config',
    function ($http, asyncHelper, config) {
        'use strict';
        return {
            get: function (id, includeOrderInformation) {

                if (includeOrderInformation === true)
                    return asyncHelper.resolveHttp($http.get(config.apiPath + 'participants/' + id + '/includeOrderInformation'), true);
                else 
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'participants/' + id), true);
            },
            getSearchableParticipant: function (id) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'searchableparticipants/' + id), true);
            },
            getSearchableParticipantForEnrollment: function (enrollmentId) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'searchableparticipants/enrollments/' + enrollmentId), true);
            },
            getCurrentParticipantOrder: function(id) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'participants/' + id + '/currentorder'), false);
            },
            getParticipantCourse: function (id) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'participants/' + id + '/course'), false);
            },
            getParticipantExamContainer: function (id) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'participants/' + id + '/examcontainer'), false);
            },
            getArticle: function (id) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'participants/' + id + '/article'), false);
            },
            getExamSubmissions: function (options) {
                var parameters = {
                    includeGraded: options.includeGraded
                };
                if (typeof options.limit === 'number') {
                    parameters.limit = options.limit;
                }
                if (typeof options.page === 'number' && options.page > 0) {
                    parameters.skip = (options.page - 1) * parameters.limit;
                }
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'participants/examsubmissions', { params: parameters }));
            },
            submitExam: function (id, attachment) {
                return asyncHelper.resolveHttp($http.put(config.apiPath + 'participants/' + id + '/submitexam', attachment), false);
            },
            setGrade: function (id, grade) {
                return asyncHelper.resolveHttp($http.put(config.apiPath + 'participants/' + id + '/grade?grade=' + grade), false);
            },
            setStatus: function (id, status) {
                return asyncHelper.resolveHttp($http.put(config.apiPath + 'participants/' + id + '/status/' + status), false);
            },
            move: function(id, activityId, activitySetId) {
                var moveRequest = {
                    activityId: activityId,
                    activitySetId: activitySetId
                };
                return asyncHelper.resolveHttp($http.put(config.apiPath + 'participants/' + id +'/move', moveRequest));
            },
            create: function (participant) {
                return asyncHelper.resolveHttp($http.post(config.apiPath + 'participants', participant));
            },
            update: function (participant) {
                return asyncHelper.resolveHttp($http.put(config.apiPath + 'participants/' + participant.Id, participant));
            },
            search: function (options) {
                var parameters = {
                    query: options.query,
                    customerId: options.customerId,
                    userId: options.userId,
                    isInvoiced: options.isInvoiced,
                    orderBy: options.orderBy,
                    orderDirection: options.orderDirection,
                    limit: options.limit
                };

                if (typeof options.page === 'number' && options.page > 0) {
                    parameters.skip = (options.page - 1) * parameters.limit;
                }
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'participants', { params: parameters }));
            }
        };
    }
]);