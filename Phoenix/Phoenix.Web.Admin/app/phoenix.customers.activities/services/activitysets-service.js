﻿angular.module('phoenix.customers').factory('ActivitySetsService', [
    '$http',
    'asyncHelper',
    'phoenix.config',
    function ($http, asyncHelper, config) {
        'use strict';
        return {
            get: function (id) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'activitysets/' + id), true);
            },
            search: function (options) {
                var parameters,
                    url = config.apiPath + 'activitysets';

                if (!options.customerId) {
                    throw 'Customer ID must be specified!';
                }

                parameters = {
                    customerId: options.customerId,
                    query: options.query,
                    productId: options.productId,
                    limit: options.limit,
                    orderBy: options.orderBy,
                    orderDirection: options.orderDirection
                };

                if (typeof options.page === 'number' && options.page > 0) {
                    parameters.skip = (options.page - 1) * parameters.limit;
                }
                return asyncHelper.resolveHttp($http.get(url, { params: parameters }));
            },
            getCourseCalendar: function(customerId) {
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'activitysets?customerId=' + customerId + '&courseCalendar=true'));
            },
            create: function (activitySet) {
                return asyncHelper.resolveHttp($http.post(config.apiPath + 'activitysets', activitySet));
            },
            update: function (activitySet) {
                return asyncHelper.resolveHttp($http.put(config.apiPath + 'activitysets/' + activitySet.Id, activitySet));
            },
            enroll: function (activitySetId, userId, enrollmentDetails, interceptErrors) {
                return asyncHelper.resolveHttp($http.put(config.apiPath + 'activitysets/' + activitySetId + '/enroll?userId=' + userId, enrollmentDetails, { interceptErrors: interceptErrors }));
            },
            unenroll: function (activitySetId, userId) {
                return asyncHelper.resolveHttp($http.put(config.apiPath + 'activitysets/' + activitySetId + '/unenroll?userId=' + userId));
            },
            addActivity: function (activitySetId, activityId, interceptErrors) {
                return asyncHelper.resolveHttp($http.put(config.apiPath + 'activitysets/' + activitySetId + '/addactivity?activityId=' + activityId, null, { interceptErrors: interceptErrors }));
            },
            removeActivity: function (activitySetId, activityId) {
                return asyncHelper.resolveHttp($http.put(config.apiPath + 'activitysets/' + activitySetId + '/removeactivity?activityId=' + activityId));
            },
            share: function (activitySetId, customerId) {
                return asyncHelper.resolveHttp($http.put(config.apiPath + 'activitysets/' + activitySetId + '/share/' + customerId));
            },
            unshare: function (activitySetId, customerId) {
                return asyncHelper.resolveHttp($http.put(config.apiPath + 'activitysets/' + activitySetId + '/unshare/' + customerId));
            }
        };
    }
]);