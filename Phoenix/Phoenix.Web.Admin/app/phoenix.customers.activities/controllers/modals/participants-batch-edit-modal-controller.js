﻿angular.module('phoenix.customers').controller('ParticipantsBatchEditModalCtrl', [
    '$scope',
    '$modalInstance',
    'codes',
    'participantResults',
    function ($scope, $modalInstance, codes, participantResults) {
        'use strict';
        $scope.values = {};
        $scope.local = {
            isAdditionalTimeIncreasing: true
        };
        
        $scope.participantStatusCodes = codes.participantStatuses;
        $scope.participantResultCodes = angular.copy(participantResults);
        $scope.participantResultCodes.unshift({ value: null, text: '-- Remove --' });
        $scope.currencies = codes.currencyCodes;
        
        $scope.participantBillableSelected = function() {
            if ($scope.values.IsNotBillable === "") {
                delete $scope.values.IsNotBillable;
            }
        };
        
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.save = function () {
            if ($scope.local.AdditiveAdditionalTime > 0) {
                if ($scope.local.isAdditionalTimeIncreasing) {
                    $scope.values.AdditiveAdditionalTime = $scope.local.AdditiveAdditionalTime;
                } else {
                    $scope.values.AdditiveAdditionalTime = -1 * $scope.local.AdditiveAdditionalTime;
                }
            }
            $modalInstance.close($scope.values);
        };
    }
]);