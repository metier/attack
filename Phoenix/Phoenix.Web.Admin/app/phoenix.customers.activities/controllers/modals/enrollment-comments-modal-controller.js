angular.module('phoenix.customers').controller('EnrollmentCommentsModalCtrl', [
    '$scope',
    '$q',
    '$modal',
    '$modalInstance',
    'enrollment',
    function ($scope, $q, $modal, $modalInstance, enrollment) {
        'use strict';
        var self = this;

        $scope.enrollment = {};
        $scope.enrollment = enrollment;

        $scope.close = function () {
            $modalInstance.close($scope.enrollment);
        };
       
    }
]);