﻿angular.module('phoenix.customers').controller('ExamQualificationModalCtrl', [
    '$scope',
    '$modalInstance',
    'activity',
    'participant',
    'participantStatusCodes',
    'ParticipantsService',
    'ActivitiesService',
    function ($scope, $modalInstance, activity, participant, participantStatusCodes, participantsService, activitiesService) {
        'use strict';

        var self = this;
        var isOpen = true;

        var LABEL_SUCCESS = "label label-success";
        var LABEL_WARNING = "label label-warning";
        var LABEL_DANGER = "label label-danger";
  
        function init() {
            $scope.isBusy = false;
            $scope.participant = participant;
            $scope.participantStatusCodes = participantStatusCodes;

            self.getSearchableParticipations(participant.user.Id);

            activitiesService.getEnrollmentConditions(activity.Id)
                .then(function(result) {
                        $scope.enrollmentConditions = result;
                    }
                );
        }

        self.getSearchableParticipations = function (userId) {
            participantsService.search({ userId: userId, limit: -1 }).then(
                function (participations) {
                    $scope.participations = participations.Items;
                }
            );
        }

        $scope.participantStatusChanged = function (participation) {
            participantsService.setStatus(participation.Id, participation.ParticipantStatusCodeValue).then(
                function () {
                    //Retrieve the new QualificationStatus from the database
                    participantsService.get($scope.participant.Id).then(
                        function (p) {
                            $scope.participant.QualificationStatus = p.QualificationStatus;
                        }
                    );
                }
            );
        }

        //Close the modal on navigation
        $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            self.dismissModal();
        });

        $scope.getCssClassForAggregatedStatusCode = function (condition) {

            var participations = $scope.filteredParticipations(condition);

            if (participations.length === 0)
                return LABEL_DANGER;

            var list = _.map(participations, function(p) {
                return self.getCssClassForStatusCode(condition, p);
            });

            _.sortBy(list, function (x) {
                return _.indexOf([LABEL_SUCCESS, LABEL_WARNING, LABEL_DANGER], x);
            });

            return _.first(list);
        }

        self.getCssClassForStatusCode = function (condition, participation) {

            if (!self.CompareArticlePaths(condition, participation)) {
                return false;       //Should never happen as the participants are allready filtered 
            }

            if (
                (!condition.LanguageId || condition.LanguageId === participation.LanguageId) &&
                (!condition.CustomerId || condition.CustomerId === participation.ActivityOwnerCustomerId)
               ) {

                if (_.contains(['COMPLETED', 'DISPENSATION'], participation.ParticipantStatusCodeValue))
                    return LABEL_SUCCESS;
                if (!condition.IsStrictRequirement && _.contains(['ENROLLED', 'INPROGRESS'], participation.ParticipantStatusCodeValue))
                    return LABEL_WARNING;
            }

            return LABEL_DANGER;
        }

        $scope.getCssClassForCustomerLabel = function (condition, participation) {
            if (!condition.CustomerId || condition.CustomerId === participation.ActivityOwnerCustomerId)
                return LABEL_SUCCESS;

            return LABEL_DANGER;
        }

        $scope.getCssClassForArticlePathLabel = function (condition, participation) {
            if (!condition.LanguageId || condition.LanguageId === participation.LanguageId)
                return LABEL_SUCCESS;

            return LABEL_DANGER;
        }

        $scope.getCssClassForParticipantStatusLabel = function(condition, participation) {
            if (_.contains(['COMPLETED', 'DISPENSATION'], participation.ParticipantStatusCodeValue))
                return LABEL_SUCCESS;
            if (!condition.IsStrictRequirement && _.contains(['ENROLLED', 'INPROGRESS'], participation.ParticipantStatusCodeValue))
                return LABEL_WARNING;

            return LABEL_DANGER;
        }

        $scope.filteredParticipations = function(condition) {

            var list = _.filter($scope.participations, function (p) {
                //Deliberately not checking customer or language, 
                //to give the user the full overview of the users related participations.
                return self.CompareArticlePaths(condition, p);      
            });
            
            return list;
        }

        self.CompareArticlePaths = function (condition, participation) {
            var articlePath = condition.Product.ProductPath.trim().concat(condition.ArticleType.CharCode).trim();
            return (participation.ArticlePath.lastIndexOf(articlePath, 0) === 0);   //Can't use .startsWith() as this function is not supported in IE
        }
        
        self.dismissModal = function() {
            if ($modalInstance && isOpen) {
                isOpen = false;
                $modalInstance.dismiss('cancel');
            }
        }

        $scope.cancel = function () {
            self.dismissModal();
        };

        init();
    }
]);