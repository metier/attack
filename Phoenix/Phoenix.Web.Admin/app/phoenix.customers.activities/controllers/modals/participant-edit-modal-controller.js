angular.module('phoenix.customers').controller('ParticipantEditModalCtrl', [
    '$scope',
    '$q',
    '$modal',
    '$modalInstance',
    'ParticipantsService',
    'participant',
    'course',
    'examContainer',
    'exam',
    'article',
    'customer',
    'distributors',
    'currentOrder',
    'predefinedLeadingTexts',
    'codes',
    'participantGradingComments',
    'PerformancesService',
    'CustomersService',
    'UsersService',
    'ExamsService',
    'lessonStatuses',
    'phoenix.config',
    'NotificationService',
    function ($scope, $q, $modal, $modalInstance, participantsService, participant, course, examContainer, exam, article, customer, distributors, currentOrder, predefinedLeadingTexts, codes, participantGradingComments, performancesService, customersService, usersService, examsService, lessonStatuses, config, notificationService) {
        'use strict';
        var self = this;
        self.hasUpdatedPerformances = false;
        $scope.participant = participant;
        $scope.customer = customer;
        $scope.participantStatusCodes = codes.participantStatuses;
        $scope.currencies = codes.currencyCodes;
        $scope.currentOrder = currentOrder;
        $scope.course = course;
        $scope.examContainer = examContainer;
        $scope.exam = exam;
        $scope.lessonStatuses = lessonStatuses;
        $scope.participantGradingComments = participantGradingComments;
        $scope.selectedTab = 'participant';
        $scope.isCommentsLoaded = false;
        $scope.isCommentsUpdated = false;
        $scope.isExamUnsubmitted = false;
        $scope.grades = ['', 'A', 'B', 'C', 'D', 'E', 'F'];

        $scope.commentsUpdated = function (comments) {

            if($scope.isCommentsLoaded && $scope.commentCount !== comments.length)
                $scope.isCommentsUpdated = true;

            $scope.commentCount = comments.length;
            $scope.isCommentsLoaded = true;
        };

        $scope.getParticipantInfoElement = function (customLeadingText) {
            var infoElement = _($scope.participant.ParticipantInfoElements).find(function (e) {
                return e.LeadingTextId === customLeadingText.LeadingTextId;
            });

            if (!infoElement) {
                infoElement = {
                    ParticipantId: $scope.participant.Id,
                    LeadingTextId: customLeadingText.LeadingTextId
                };
                $scope.participant.ParticipantInfoElements.push(infoElement);
            }
            return infoElement;
        };

        self.setCustomLeadingTexts = function () {
            $scope.customLeadingTexts = angular.copy($scope.customer.CustomLeadingTexts);
            
            _($scope.customLeadingTexts).each(function (customLeadingText) {
                customLeadingText.LeadingText = _(predefinedLeadingTexts).find(function (predefined) {
                    return customLeadingText.LeadingTextId === predefined.Id;
                });
            });
        };

        function getDistributor(distributorId) {
            return _(distributors).find(function(distributor) {
                return distributor.Id === distributorId;
            });
        }

        function loadPerformances(userId, activityId) {
            $scope.isPerformancesLoaded = false;
            if (course || examContainer) {
                usersService.getPerformances(userId, { activityId: activityId }).then(function (result) {
                    $scope.performances = result[0].Performances;
                    _($scope.performances).each(function (per) {
                        per.TimeUsedInMilliseconds = per.TimeUsedInSeconds * 1000;
                    });
                    $scope.isPerformancesLoaded = true;
                });
            } else {
                $scope.isPerformancesLoaded = true;
            }
        }
        
        function findLessonRco(rcoId, lessons) {
            for (var i = 0; i < lessons.length; i++) {
                if (lessons[i].Id === rcoId) {
                     return lessons[i];
                }
                var found = findLessonRco(rcoId, lessons[i].Lessons);
                if (found) {
                     return found;
                }
            }
            return null;
        }
        
        $scope.getRco = function (rcoId) {
            if ($scope.course) {
                if ($scope.course.Id === rcoId) {
                    return $scope.course;
                }
                return findLessonRco(rcoId, $scope.course.Lessons);
            }
            if ($scope.examContainer) {
                if ($scope.examContainer.Id === rcoId) {
                    return $scope.examContainer;
                }
                return _($scope.examContainer.Children).find(function (e) { return e.Id === rcoId; });
            }
            return null;
        };
        
        $scope.updatePerformance = function (performance) {
            performance.isUpdating = true;
            return performancesService.register(performance.UserId, performance.RcoId, {
                Score: performance.Score,
                Status: performance.Status,
                CompletedDate: performance.CompletedDate,
                TimeUsedInSeconds: performance.TimeUsedInMilliseconds / 1000
            }).then(function (updated) {
                performance.Score = updated.Score;
                performance.CompletedDate = updated.CompletedDate;
                performance.TimeUsedInSeconds = updated.TimeUsedInSeconds;
                performance.TimeUsedInMilliseconds = updated.TimeUsedInSeconds * 1000;
                performance.Status = updated.Status;

                self.hasUpdatedPerformances = true;

                _($scope.performances).each(function (p) {
                    p.isUpdating = true;
                });
                loadPerformances($scope.participant.UserId, $scope.participant.ActivityId);

                participantsService.get($scope.participant.Id, true).then(function (result) {
                    $scope.participant = result;
                }, function () {
                });
            }, function () {
            });
        };

        $scope.setGrade = function () {
            $scope.isSettingGrade = true;
            participantsService.setGrade($scope.participant.Id, $scope.participant.ExamGrade).then(function (updated) {
                self.hasUpdatedPerformances = true;
                $scope.isSettingGrade = false;
                $scope.participant.RowVersion = updated.ParticipantRowVersion;
                $scope.participant.ExamGrade = updated.ExamGrade || '';
                $scope.participant.ExamGradeDate = updated.ExamGradeDate;
                $scope.participant.StatusCodeValue = updated.StatusCodeValue;
                $scope.participant.Result = updated.Result;
            }, function () {
                $scope.isSettingGrade = false;
            });
        };

        $scope.unsubmitExam = function () {
            $scope.isUnsubmittingExam = true;
            examsService.unsubmitExam($scope.exam.Id).then(function (updatedExam) {
                participantsService.get($scope.participant.Id, true).then(function (updatedParticipant) {

                    self.hasUpdatedPerformances = true;
                    $scope.isExamUnsubmitted = true;

                    $scope.exam = updatedExam;
                    $scope.participant.StatusCodeValue = updatedParticipant.StatusCodeValue;
                    $scope.participant.ExamGrade = updatedParticipant.ExamGrade;
                    $scope.participant.Result = updatedParticipant.Result;
                    $scope.participant.RowVersion = updatedParticipant.RowVersion;
                    $scope.participant.ParticipantOrderLines = updatedParticipant.ParticipantOrderLines;

                    self.initExam();
                    $scope.isUnsubmittingExam = false;
                });
            }, function() {
                $scope.isUnsubmittingExam = false;
            });
        };
        
        $scope.onDocumentUploaded = function (attachment) {
            if (attachment) {
                participantsService.submitExam($scope.participant.Id, attachment).then(function () {
                    $scope.participant.Attachments.push(attachment);
                });
            }
        };

        $scope.searchCustomer = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.customers/views/modals/customer-search-modal.html',
                controller: 'CustomerSearchModalCtrl',
                windowClass: 'modal-wide'
            });
            modalInstance.result.then(function (searchableCustomer) {
                customersService.get(searchableCustomer.Id).then(function (selectedCustomer) {
                    $scope.customer = selectedCustomer;
                    $scope.distributor = getDistributor(selectedCustomer.DistributorId);
                    $scope.participant.CustomerId = selectedCustomer.Id;
                    $scope.participant.ParticipantInfoElements = [];
                    self.setCustomLeadingTexts();
                });
            });
        };

        $scope.getAnswer = function(question) {
            return _($scope.exam.Answers).find(function(answer) {
                return answer.QuestionId === question.Id;
            });
        };

        function isCorrect(answer) {
            var question = _($scope.exam.Questions).find(function(item) {
                return answer.QuestionId === item.Id;
            });

            if (question.Type === 'TF') {
                return answer.BoolAnswer === question.CorrectBoolAnswer;
            }
            if (question.Type === 'M' || question.Type === 'S') {
                var correctOptions = _.chain(question.QuestionOptions).filter(function (item) {
                    return item.IsCorrect;
                }).map(function(item) {
                    return item.Id;
                }).value();
                var correctNumberOfAnswers = correctOptions.length === answer.OptionsAnswers.length;
                if (correctNumberOfAnswers) {
                    return _(answer.OptionsAnswers).every(function(item) {
                        return _(correctOptions).contains(item.Id);
                    });
                }
            }
            return false;

        }

        $scope.isOptionSelected = function(answer, questionOption) {
            var answerOption = _(answer.OptionsAnswers).find(function(item) {
                return item.Id === questionOption.Id;
            });
            return !!answerOption;
        };

        $scope.saveExam = function () {
            $scope.isSavingExam = true;
            examsService.overrideTotalScore($scope.exam.Id, $scope.exam.TotalScore).then(function () {
                var participantPromise = participantsService.get($scope.participant.Id, true);
                var examPromise = examsService.getExamResult($scope.participant.Id);
                return $q.all([participantPromise, examPromise]).then(function (results) {
                    notificationService.alertSuccess({ message: 'Exam results was saved. Grade has been re-calculated.' });
                    self.hasUpdatedPerformances = true;
                    $scope.participant = results[0];
                    $scope.exam = results[1];
                    self.initExam();
                });
            }).finally(function() {
                $scope.isSavingExam = false;
            });
        };

        $scope.onToggleIsNotBillable = function () {
            $scope.participant.IsNotBillable = !$scope.participant.IsNotBillable;
        }

        self.init = function () {
            if (course || examContainer) {
                $scope.isProgressVisible = true;
                loadPerformances($scope.participant.UserId, $scope.participant.ActivityId);
            }

            $scope.participant.ExamGrade = $scope.participant.ExamGrade || '';
            $scope.distributor = getDistributor(customer.DistributorId);
            // Case exams, project assignments and external certifications
            $scope.isCaseExam = article.ArticleTypeId === 4 || article.ArticleTypeId === 5 || article.ArticleTypeId === 9;
            // Multiple choice exams and internal certifications
            $scope.isMultipleChoiceExam = article.ArticleTypeId === 3 || article.ArticleTypeId === 6;
            
            self.setCustomLeadingTexts();
            $scope.participantLockedOnOrder = false;

            if (currentOrder) {
                if (!currentOrder.IsCreditNote) {
                    $scope.participantLockedOnOrder = true;
                }
                if (currentOrder.IsCreditNote && currentOrder.Status === 1) {
                    $scope.participantLockedOnOrder = true;
                }
            }

            self.initExam();
        };

        self.initExam = function () {
            if ($scope.exam.Questions) {
                $scope.maximumScore = _($scope.exam.Questions).reduce(function (memo, question) { return memo + question.Points; }, 0);
            }
            if (!$scope.maximumScore) {
                $scope.maximumScore = 'unknown';
            }
            _($scope.exam.Questions).each(function (q) {
                q.answer = $scope.getAnswer(q);
            });
            _($scope.exam.Answers).each(function (item) {
                item.isCorrect = isCorrect(item);
            });
        };
        
        $scope.cancel = function () {
            if (self.hasUpdatedPerformances || $scope.isCommentsUpdated || $scope.isExamUnsubmitted) {
                $modalInstance.close($scope.participant);
            } else {
                $modalInstance.dismiss('cancel');
            }
        };

        $scope.save = function () {
            $scope.isSaving = true;
            participantsService.update($scope.participant).then(function (updatedParticipant) {
                $scope.isSaving = false;
                $modalInstance.close(updatedParticipant);
            }, function() {
                $scope.isSaving = false;
            });
        };
        
        self.init();
    }
]);