﻿angular.module('phoenix.users').controller('ParticipantsSelectModalCtrl', [
    '$scope',
    '$modalInstance',
    'customerId',
    'isInvoiced',
    'ParticipantsService',
    function ($scope, $modalInstance, customerId, isInvoiced, participantsService) {
        'use strict';
        var self = this;
        $scope.customerId = customerId;

        self.initialData = null;
        $scope.limit = 10;
        $scope.isInitialized = false;
        $scope.lastSearchQuery = '';
        $scope.model = {
            query: '',
            selectedParticipants: []
        };

        $scope.search = function () {
            if (!$scope.model.query) {
                return;
            }
            $scope.isSearching = true;
            $scope.lastSearchQuery = $scope.model.query;
            participantsService.search({ query: $scope.model.query, limit: $scope.limit, page: 1, customerId: customerId, isInvoiced: isInvoiced }).then(function (result) {
                $scope.isSearching = false;
                $scope.participants = result;
            }, function () {
                $scope.isSearching = false;
            });
        };

        $scope.navigate = function (page) {
            participantsService.search({ query: $scope.model.query, limit: $scope.limit, page: page, customerId: customerId, isInvoiced: isInvoiced }).then(function (result) {
                $scope.participants = result;
            });
        };

        $scope.toggleSelect = function (participant) {
            var selectedParticipant = self.getSelectedParticipant(participant.Id),
                index;
            if (!selectedParticipant) {
                $scope.model.selectedParticipants.push(participant);
            } else {
                index = $scope.model.selectedParticipants.indexOf(selectedParticipant);
                if (index > -1) {
                    $scope.model.selectedParticipants.splice(index, 1);
                }
            }
        };

        $scope.isSelected = function (participant) {
            return !!self.getSelectedParticipant(participant.Id);
        };

        self.getSelectedParticipant = function (id) {
            return _($scope.model.selectedParticipants).find(function (a) { return a.Id === id; });
        };

        $scope.onQueryChanged = function () {
            if ($scope.model.query === '') {
                $scope.participants = self.initialData;
            }
        };

        self.init = function () {
            $scope.model.query = '';
            $scope.isSearching = true;
            participantsService.search({ query: $scope.model.query, limit: $scope.limit, page: 1, customerId: customerId, isInvoiced: isInvoiced }).then(function (result) {
                $scope.isInitialized = true;
                $scope.isSearching = false;
                self.initialData = result;
                $scope.participants = result;
            }, function() {
                $scope.isSearching = false;
            });
        };

        self.init();


        $scope.ok = function () {
            $modalInstance.close($scope.model.selectedParticipants);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }
]);