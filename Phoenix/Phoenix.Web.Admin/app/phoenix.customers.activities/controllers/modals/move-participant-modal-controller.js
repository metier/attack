﻿angular.module('phoenix.customers').controller('MoveParticipantModalCtrl', [
    '$scope',
    '$modalInstance',
    '$q',
    'ActivitiesService',
    'DistributorsService',
    'ParticipantsService',
    'customerId',
    'participantId',
    'articleOriginalId',
    function ($scope, $modalInstance, $q, activitiesService, distributorsService, participantsService, customerId, participantId, articleOriginalId) {
        'use strict';
        var self = this;
        self.initialData = null;
        self.customerId = customerId;
        self.participantId = participantId;
        self.articleOriginalId = articleOriginalId;

        $scope.model = {
            selectedActivity: null,
            lastSearchQuery: '',
            searchAllCustomers: false,
            query: '',
            limit: 10
        };
        
        $scope.$watch('model.searchAllCustomers', function () {
            self.init();
        });

        $scope.search = function () {
            $scope.model.selectedActivity = null;
            self.searchActivities(1);
        };
        
        $scope.navigate = function (page) {
            self.searchActivities(page);
        };
        
        $scope.isSelected = function (activity) {
            return !!$scope.model.selectedActivity && activity.ActivityId === $scope.model.selectedActivity.ActivityId && activity.ActivitySetId === $scope.model.selectedActivity.ActivitySetId;
        };

        $scope.onQueryChanged = function () {
            if ($scope.model.query === '') {
                $scope.model.activities = self.initialData;
            }
        };
        
        $scope.ok = function () {
            participantsService.move(self.participantId, $scope.model.selectedActivity.ActivityId, $scope.model.selectedActivity.ActivitySetId)
                                .then(function() {
                                    $modalInstance.close();
                                });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        self.searchActivities = function (page) {
            var deferred = $q.defer(), distributorId;
            distributorId = distributorsService.get().Id;

            $scope.model.isSearching = true;
            $scope.model.lastSearchQuery = $scope.model.query;
            activitiesService.search({
                query: $scope.model.query,
                limit: $scope.model.limit,
                page: page || 1,
                customerId: $scope.model.searchAllCustomers ? null : customerId,
                distributorId: distributorId,
                includeActivitySets: true,
                articleOriginalId: self.articleOriginalId
            }).then(function (result) {
                $scope.model.isSearching = false;
                $scope.model.activities = result;
                deferred.resolve(result);
            }, function () {
                $scope.model.isSearching = false;
                deferred.reject();
            });

            return deferred.promise;
        };

        self.init = function () {
            self.searchActivities(1).then(function (result) {
                self.initialData = result;
            });
        };

        self.init();
    }
]);