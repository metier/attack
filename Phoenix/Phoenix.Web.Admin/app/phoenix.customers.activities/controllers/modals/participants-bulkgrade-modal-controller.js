﻿angular.module('phoenix.customers').controller('ParticipantsBulkGradeModalCtrl', [
    '$scope',
    '$modalInstance',
    '$q',
    'activityParticipants',
    'ParticipantsService',
    'CustomersService',
    'DistributorsService',
    'ValidationHelper',
    function ($scope, $modalInstance, $q, activityParticipants, participantsService, customersService, distributorsService, validationHelper) {
        'use strict';
        var self = this;
        self.numberOfUpdatedParticipants = 0;
        $scope.grades = ['A', 'B', 'C', 'D', 'E', 'F'];
        $scope.locals = {
            participants: [],
            selectedParticipants: []
        };  

        $scope.verifyParticipant = function (participant) {
            participant.isBusyVerifying = true;
            participant.isValid = false;
            participant.emailValidationText = null;
            participant.gradeValidationText = null;

            // Validate required
            if (!participant.Email) {
                participant.isBusyVerifying = false;
                participant.isValid = false;
                participant.emailValidationText = 'E-mail is required';
                $scope.onParticipantSelected();
                return;
            }

            // Validate email
            participant.isValid = validationHelper.isValidEmail(participant.Email);
            if (!participant.isValid) {
                participant.isBusyVerifying = false;
                participant.emailValidationText = 'Invalid e-mail';
                $scope.onParticipantSelected();
                return;
            }
            
            // Validate participant exists
            var foundParticipant = _(activityParticipants).find(function (p) { return p.Email === participant.Email; });
            if (foundParticipant) {
                participant.participant = foundParticipant;
                participant.isBusyVerifying = false;

                if (!_($scope.grades).some(function (g) { return g === participant.Grade; })) {
                    participant.Grade = null;
                }
                if (!participant.Grade) {
                    participant.gradeValidationText = 'Select grade';
                    participant.isValid = false;
                } else {
                    participant.isValid = true;
                }

                $scope.onParticipantSelected();
            } else {
                participant.emailValidationText = 'Participant with e-mail was not found';
                participant.isValid = false;
                participant.isBusyVerifying = false;
                $scope.onParticipantSelected();
            }
        };
        
        $scope.toggleSelectAllParticipants = function () {
            _($scope.locals.participants).each(function (p) {
                if (p.isValid) {
                    p.isSelected = !!$scope.locals.isAllParticipantsSelected;
                }
            });
            $scope.onParticipantSelected();
        };

        $scope.onParticipantSelected = function () {
            var selectableParticipants = _($scope.locals.participants).filter(function (u) {
                return u.isValid;
            });

            $scope.locals.canSelectAll = selectableParticipants.length === 0;
            $scope.locals.selectedParticipants = _($scope.locals.participants).filter(function (u) {
                return u.isSelected;
            });

            $scope.locals.isAllParticipantsSelected = selectableParticipants.length === $scope.locals.selectedParticipants.length && selectableParticipants.length > 0;
        };

        $scope.$watch('locals.selectedFile', function () {
            $scope.locals.participants = [];
            $scope.onParticipantSelected();
            if ($scope.locals.selectedFile && $scope.locals.selectedFile.participants) {
                _($scope.locals.selectedFile.participants).each(function (participant) {
                    $scope.verifyParticipant(participant);
                    $scope.locals.participants.push(participant);
                    $scope.onParticipantSelected();
                });
            }
        });

        function setExamGrade(participant) {
            participant.isBusyUpdating = true;
            participant.isUpdated = false;
            participantsService.setGrade(participant.participant.Id, participant.Grade).then(function () {
                self.numberOfUpdatedParticipants++;
                participant.isBusyUpdating = false;
                participant.isUpdated = true;
            }, function () {
                participant.isBusyUpdating = false;
            });
        }

        $scope.setExamGrades = function () {
            _($scope.locals.selectedParticipants).each(function (particpant) {
                setExamGrade(particpant);
            });
        };
        
        $scope.close = function () {
            if (self.numberOfUpdatedParticipants > 0) {
                $modalInstance.close();
            } else {
                $modalInstance.dismiss('cancel');
            }
        };
    }
]);