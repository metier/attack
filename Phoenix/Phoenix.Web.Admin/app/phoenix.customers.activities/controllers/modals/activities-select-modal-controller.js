﻿angular.module('phoenix.customers').controller('ActivitiesSelectModalCtrl', [
    '$scope',
    '$modalInstance',
    'canSearchAllCustomers',
    'onlyActivitiesProposedForOrder',
    'customerId',
    'DistributorsService',
    'ActivitiesService',
    function ($scope, $modalInstance, canSearchAllCustomers, onlyActivitiesProposedForOrder, customerId, distributorsService, activitiesService) {
        'use strict';
        var self = this;
        self.distributorId = distributorsService.get().Id;
        $scope.customerId = customerId;
        
        self.initialData = null;
        $scope.canSearchAllCustomers = !!canSearchAllCustomers;
        $scope.limit = 10;
        $scope.isInitialized = false;
        $scope.lastSearchQuery = '';
        $scope.selectedArticle = null;
        $scope.model = {
            query: '',
            selectedActivities: [],
            searchAllCustomers: false
        };

        $scope.$watch('model.searchAllCustomers', function () {
            self.init();
        });

        $scope.search = function () {
            var options = {
                query: $scope.model.query,
                limit: $scope.limit,
                page: 1,
                customerId: $scope.model.searchAllCustomers ? null : customerId,
                distributorId: self.distributorId
            };

            if (!$scope.model.query) {
                return;
            }
            $scope.isSearching = true;
            $scope.lastSearchQuery = $scope.model.query;

            if (onlyActivitiesProposedForOrder) {
                options.isProposed = true;
                options.hasFixedPrice = true;
            }

            activitiesService.search(options).then(function (result) {
                $scope.isSearching = false;
                $scope.activities = result;
            }, function () {
                $scope.isSearching = false;
            });
        };

        $scope.navigate = function (page) {
            var options = {
                query: $scope.model.query,
                limit: $scope.limit,
                page: page,
                customerId: $scope.model.searchAllCustomers ? null : customerId,
                distributorId: self.distributorId
            };

            if (onlyActivitiesProposedForOrder) {
                options.isProposed = true;
                options.hasFixedPrice = true;
            }
            
            activitiesService.search(options).then(function (result) {
                $scope.activities = result;
            });
        };

        $scope.toggleSelect = function (activity) {
            var selectedActivity = self.getSelectedActivity(activity.ActivityId), index;
            if (!selectedActivity) {
                $scope.model.selectedActivities.push(activity);
            } else {
                index = $scope.model.selectedActivities.indexOf(selectedActivity);
                if (index > -1) {
                    $scope.model.selectedActivities.splice(index, 1);
                }
            }
        };

        $scope.isSelected = function (activity) {
            return !!self.getSelectedActivity(activity.ActivityId);
        };

        self.getSelectedActivity = function (id) {
            return _($scope.model.selectedActivities).find(function (a) { return a.ActivityId === id; });
        };

        $scope.onQueryChanged = function () {
            if ($scope.model.query === '') {
                $scope.activities = self.initialData;
            }
        };
        
        self.init = function () {
            $scope.model.query = '';
            var options = {
                query: $scope.model.query,
                limit: $scope.limit,
                page: 1,
                customerId: $scope.model.searchAllCustomers ? null : customerId,
                distributorId: self.distributorId
            };

            if (onlyActivitiesProposedForOrder) {
                options.isProposed = true;
                options.hasFixedPrice = true;
            }
            
            activitiesService.search(options).then(function (result) {
                $scope.isInitialized = true;
                self.initialData = result;
                $scope.activities = result;
            });
        };

        self.init();


        $scope.ok = function () {
            $modalInstance.close($scope.model.selectedActivities);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }
]);