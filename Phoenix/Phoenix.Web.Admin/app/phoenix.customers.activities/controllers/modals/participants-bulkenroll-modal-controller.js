﻿angular.module('phoenix.customers').controller('ParticipantsBulkEnrollModalCtrl', [
    '$scope',
    '$modalInstance',
    '$q',
    '$timeout',
    'UsersService',
    'DistributorsService',
    'ValidationHelper',
    'activitySetId',
    'leadingTexts',
    'customer',
    'enrolledUsers',
    'NotificationService',
    'ActivitySetsService',
    function ($scope, $modalInstance, $q, $timeout, usersService, distributorsService, validationHelper, activitySetId, leadingTexts, customer, enrolledUsers, notificationService, activitySetsService) {
        'use strict';
        var self = this;
        self.numberOfEnrolledUsers = 0;
        self.enrolledUsers = angular.copy(enrolledUsers);
        $scope.allEnrollmentsVerified = true;

        if (customer.CustomLeadingTexts.length === 0) {
            var errorMsg = 'Could not find any leading texts for ' + customer.Name + '. Please make sure all the required fields are provided.';
            notificationService.alertError({ message: errorMsg });
            throw errorMsg;
        }

        $scope.locals = {
            enrollments: [],
            selectedEnrollments: [],
            leadingTexts: leadingTexts
        };  

        //Not in use anymore. May be removed...
        $scope.getCustomLeadingText = function (leadingText) {
            return _(customer.CustomLeadingTexts).find(function (clt) {
                return clt.LeadingTextId === leadingText.Id;
            });
        };

        $scope.verifyEnrollment = function (enrollment) {
            enrollment.isEnrolled = false;
            enrollment.isValid = false;
            enrollment.emailValidationText = null;

            // Validate required
            if (!enrollment.Email) {
                enrollment.isValid = false;
                enrollment.emailValidationText = 'E-mail is required';
                $scope.onEnrollmentSelected();
                return $timeout(function () {
                    return;
                });
            }

            // Validate email
            enrollment.isValid = validationHelper.isValidEmail(enrollment.Email);
            if (!enrollment.isValid) {
                enrollment.emailValidationText = 'Invalid e-mail';
                $scope.onEnrollmentSelected();
                return $timeout(function () {
                    return;
                });
            }

            // Validate already enrolled
            var existingEnrolledUser = _(self.enrolledUsers).find(function (user) {
                return enrollment.Email === user.Email;
            });

            if (existingEnrolledUser) {
                enrollment.isValid = true;
                enrollment.isEnrolled = true;
                enrollment.user = existingEnrolledUser;
                $scope.onEnrollmentSelected();
                return $timeout(function () {
                    return;
                });
            }

            var deferred = $q.defer();
            // Validate user exists
            $scope.lookupUserByEmail(enrollment.Email).then(function (user) {
                if (user) {
                    enrollment.isValid = true;
                    enrollment.user = user;
                } else {
                    enrollment.user = null;
                    enrollment.emailValidationText = 'User with e-mail was not found';
                    enrollment.isValid = false;
                }
                $scope.onEnrollmentSelected();
            }, function () {
                enrollment.emailValidationText = 'Failed to look up user by e-mail';
                enrollment.isValid = false;
                $scope.onEnrollmentSelected();
            }).finally(function () {
                deferred.resolve();
            });

            return deferred.promise;
        };

        $scope.lookupUserByEmail = function (email) {
            var deferred = $q.defer();
            usersService.search({
                query: email,
                distributorId: distributorsService.get().Id
            }).then(function (result) {
                deferred.resolve(result.Items[0]);
            }, function () {
                deferred.reject();
            });
            return deferred.promise;
        };

        $scope.toggleSelectAllEnrollments = function () {
            _($scope.locals.enrollments).each(function (e) {
                if (e.isValid && !e.isEnrolled) {
                    e.isSelected = !!$scope.locals.isAllEnrollmentsSelected;
                }
            });
            $scope.onEnrollmentSelected();
        };

        $scope.onEnrollmentSelected = function () {
            var selectableEnrollments = _($scope.locals.enrollments).filter(function (e) {
                return e.isValid && !e.isEnrolled;
            });

            $scope.locals.canSelectAll = selectableEnrollments.length === 0;
            $scope.locals.selectedEnrollments = _($scope.locals.enrollments).filter(function (e) {
                return e.isSelected;
            });

            $scope.locals.isAllEnrollmentsSelected = selectableEnrollments.length === $scope.locals.selectedEnrollments.length && selectableEnrollments.length > 0;
        };

        $scope.$watch('locals.selectedFile', function () {
            var promises = [];
            $scope.locals.enrollments = $scope.locals.selectedFile ? $scope.locals.selectedFile.enrollments : [];
            $scope.allEnrollmentsVerified = false;
            _($scope.locals.enrollments).each(function (enrollment) {
                promises.push($scope.verifyEnrollment(enrollment));
            });

            $q.all(promises).finally(function() {
                $scope.allEnrollmentsVerified = true;
            });
        });

        self.getInfoElements = function (enrollment) {
            var infoElements = [];
            _($scope.locals.leadingTexts).each(function (lt) {
                if (enrollment[lt.Text] !== "") {
                    infoElements.push({
                        LeadingTextId: lt.Id,
                        InfoText: enrollment[lt.Text]
                    });
                }
            });
            return infoElements;
        };

        $scope.enroll = function () {
            // Write info elements to user
            var enrollments = _($scope.locals.selectedEnrollments).map(function (enrollment) {
                enrollment.user.InfoElements = self.getInfoElements(enrollment);
                return enrollment;
            });

            // Enroll in parallel
            $scope.isBusyEnrolling = true;
            $q.all(_(enrollments).map(function (enrollment) {
                enrollment.isBusyEnrolling = true;
                return activitySetsService.enroll(activitySetId, enrollment.user.Id, {"InfoElements" : enrollment.user.InfoElements}).then(function () {
                    self.numberOfEnrolledUsers++;
                    enrollment.isEnrolled = true;
                    enrollment.isSelected = false;
                    enrollment.isBusyEnrolling = false;
                    self.enrolledUsers.push(enrollment.user);
                    $scope.onEnrollmentSelected();
                }, function() {
                    enrollment.isBusyEnrolling = false;
                    $scope.onEnrollmentSelected();
                });
            })).then(function () {
                $scope.isBusyEnrolling = false;
            }, function () {
                $scope.isBusyEnrolling = false;
            });
        };

        $scope.close = function () {
            if (self.numberOfEnrolledUsers > 0) {
                $modalInstance.close(self.enrolledUsers);
            } else {
                $modalInstance.dismiss('cancel');
            }
        };
    }
]);