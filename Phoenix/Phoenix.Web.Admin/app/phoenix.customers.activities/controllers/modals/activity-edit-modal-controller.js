﻿angular.module('phoenix.customers').controller('ActivityEditModalCtrl', [
    '$scope',
    '$modalInstance',
    'customer',
    'customerArticle',
    'editActivity',
    'currentActivityOrder',
    'articleCopy',
    function ($scope, $modalInstance, customerArticle, customer, editActivity, currentActivityOrder, articleCopy) {
        'use strict';
        $scope.isModal = true;
        $scope.customer = customer;
        $scope.customerArticle = customerArticle;
        $scope.articleCopy = articleCopy;
        $scope.activity = editActivity;
        $scope.currentActivityOrder = currentActivityOrder;

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
        
        $scope.save = function () {
            $scope.isSaving = true;
            $scope.$broadcast('saveActivity');
        };

        $scope.$on('ActivitySaved', function (event, savedActivity) {
            $scope.isSaving = false;
            $modalInstance.close(savedActivity);
        });

        $scope.$on('ActivitySaveFailed', function () {
            $scope.isSaving = false;
        });
    }
]);