﻿angular.module('phoenix.customers').controller('ActivityCreateModalCtrl', [
    '$scope',
    '$modalInstance',
    'customer',
    'customerArticle',
    'copyActivity',
    'articleCopy',
    'courseProgram',
    'educationalPartners',
    function ($scope, $modalInstance, customer, customerArticle, copyActivity, articleCopy, courseProgram, educationalPartners) {
        'use strict';
        $scope.customer = customer;
        $scope.customerArticle = customerArticle;
        $scope.articleCopy = articleCopy;
        $scope.educationalPartners = educationalPartners;
        if (copyActivity) {
            var copyOfCopyActivity = angular.copy(copyActivity);
            delete copyOfCopyActivity.Id;
            copyOfCopyActivity.ActivityPeriods = [{}];
            copyOfCopyActivity.Participants = null;
            copyOfCopyActivity.ActivityExamCourses = _(copyOfCopyActivity.ActivityExamCourses).map(function (course) {
                return { CourseRcoId: course.CourseRcoId };
            });
            
            $scope.activity = copyOfCopyActivity;
        } else {
            $scope.activity = {
                Name: articleCopy.Title,
                CustomerArticleId: customerArticle.Id,
                UnitPrice: customerArticle.UnitPrice,
                FixedPrice: customerArticle.FixedPrice,
                UnitPriceNotBillable: customerArticle.UnitPriceNotBillable,
                FixedPriceNotBillable: customerArticle.FixedPriceNotBillable,
                CurrencyCodeId: customerArticle.CurrencyCodeId,
                ActivityPeriods: [{}],
                Resources: [],
                Attachments: articleCopy.Attachments,
                Duration: articleCopy.Duration,
                MomentTimezoneName: "europe_oslo",
                RcoCourseId: articleCopy.RcoCourseId,
                RcoExamContainerId: articleCopy.RcoExamContainerId,
                CaseAttachmentId: articleCopy.CaseAttachmentId,
                EducationalPartnerResourceId: customerArticle.EducationalPartnerId,
                NumberOfQuestions: articleCopy.NumberOfQuestions,
                NumberOfQuestionsPerPage: articleCopy.NumberOfQuestionsPerPage,
                EasyQuestionCount: articleCopy.EasyQuestionCount,
                MediumQuestionCount: articleCopy.MediumQuestionCount,
                HardQuestionCount: articleCopy.HardQuestionCount,
                EasyCustomerSpecificCount: articleCopy.EasyCustomerSpecificCount,
                MediumCustomerSpecificCount: articleCopy.MediumCustomerSpecificCount,
                HardCustomerSpecificCount: articleCopy.HardCustomerSpecificCount,
                ECTS: articleCopy.ECTS,
                ActivityExamCourses: _(articleCopy.ArticleExamCourses).map(function(course) {
                    return { CourseRcoId: course.CourseRcoId };
				}),
                EnrolledUserActivityInformation: articleCopy.EnrolledUserActivityInformation,
                EnrolledUserActivityExpiredInformation: articleCopy.EnrolledUserActivityExpiredInformation,
            };
        }  
        
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if (copyActivity) {
                $scope.$broadcast('saveActivity', { copyFromActivityId: copyActivity.Id });
            } else {
                $scope.$broadcast('saveActivity');
            }
        };

        $scope.$on('ActivitySaved', function (event, createdActivity) {
            $scope.isSaving = false;
            $modalInstance.close(createdActivity);
        });

        $scope.$on('ActivitySaveFailed', function () {
            $scope.isSaving = false;
        });

        $scope.onToggleSelection = function (control) {
            if (control === "FixedPrice")
                $scope.activity.FixedPriceNotBillable = !$scope.activity.FixedPriceNotBillable;

            if (control === "UnitPrice")
                $scope.activity.UnitPriceNotBillable = !$scope.activity.UnitPriceNotBillable;
        }

    }
]);