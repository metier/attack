﻿angular.module('phoenix.customers').controller('ActivitySetCreateModalCtrl', [
    '$scope',
    '$modalInstance',
    'customerId',
    'activityId',
    'ActivitySetsService',
    function ($scope, $modalInstance, customerId, activityId, activitySetsService) {
        'use strict';

        $scope.activitySet = {
            CustomerId: customerId,
            EnrollmentFrom: moment().toDate(),
            IsUnenrollmentAllowed: false,
            IsPublished: false
        };
        
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.save = function () {
            $scope.isSaving = true;
            activitySetsService.create($scope.activitySet).then(function (result) {
                if (activityId) {
                    activitySetsService.addActivity(result.Id, activityId).then(function() {
                        $modalInstance.close(result);
                    });
                } else {
                    $modalInstance.close(result);
                }
            }, function () {
                $scope.isSaving = false;
            });
        };
    }
]);