﻿angular.module('phoenix.customers').controller('CoursecalendarActivitysetDetailCtrl', [
    '$scope',
    'activitySet',
    'activities',
    function ($scope, activitySet, activities) {
        'use strict';
        $scope.activitySet = activitySet;
        $scope.activities = activities.Items;
    }
]);