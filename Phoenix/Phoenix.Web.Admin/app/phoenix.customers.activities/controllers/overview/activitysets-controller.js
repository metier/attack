﻿angular.module('phoenix.customers').controller('OverviewActivitySetsCtrl', [
    '$scope',
    '$state',
    '$location',
    'ActivitySetsService',
    'activitySets',
    'customer',
    function ($scope, $state, $location, activitySetsService, activitySets, customer) {
        'use strict';

        $scope.customer = customer;
        $scope.activitySets = activitySets || { TotalCount: 0, Items: [] };
        $scope.query = $location.search().query;
        $scope.orderBy = $location.search().orderBy || '';
        $scope.orderDirection = $location.search().orderDirection || '';
        $scope.page = $location.search().page;
        $scope.limit = $location.search().limit || 15;
        $scope.maxPages = $scope.activitySets.TotalCount;

        $scope.onQueryChanged = function () {
            if ($scope.query === '') {
                $scope.search();
            }
        };

        $scope.search = function () {
            if (!$state.params.query && $scope.query === '') { return; }
            $state.go('customer.activities.overview.activitysets.list', { query: $scope.query });
        };

        $scope.navigate = function (page) {
            if (page < 1 || page > $scope.maxPages) {
                return;
            }
            $location.search('page', page);
        };

        $scope.delete = function (activitySet) {
            if (confirm("Do you really want to delete the activity set?")) {
                activitySet.isBusyDeleting = true;

                activitySet.IsDeleted = true;
                activitySetsService.update(activitySet).then(function () {
                    activitySet.isBusyDeleting = false;
                    $state.go('customer.activities.overview.activitysets.list', {}, { inherit: true, reload: true });
                }, function () {
                    activitySet.isBusyDeleting = false;
                });
            }
        };
    }
]);