﻿angular.module('phoenix.customers').controller('CustomerActivitiesOverviewCtrl', [
    '$scope',
    '$state',
    function ($scope, $state) {
        'use strict';

        $scope.isActive = function (s) {
            return $state.current.name.indexOf(s) === 0;
        };
    }
]);