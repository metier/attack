﻿angular.module('phoenix.customers').controller('OverviewCourseCalendarCtrl', [
    '$scope',
    '$state',
    '$q',
    'courseCalendar',
    'ActivitySetsService',
    function ($scope, $state, $q, courseCalendar, activitySetsService) {
        'use strict';
        var self = this;

        self.numberIsNotSet = function (number) {
            return number !== 0 && !number;
        };

        self.sortCourseCalendar = function (activitySets) {
            return activitySets.sort(function (a, b) {
                return (self.numberIsNotSet(a.CourseProgramPriority) ? -1 : a.CourseProgramPriority) -
                       (self.numberIsNotSet(b.CourseProgramPriority) ? -1 : b.CourseProgramPriority);
            });
        };

        $scope.sortableOptions = {
            distance: 5
        };

        $scope.revert = function() {
            $scope.courseCalendar = angular.copy(self.originalCourseCalendar);
            self.sortCourseCalendar($scope.courseCalendar);
        };

        $scope.saveOrder = function () {
            var promises = [],
                updatedActivitySets = [];

            $scope.isBusySavingOrder = true;
            _($scope.courseCalendar).each(function (activitySet, index) {
                activitySet.CourseProgramPriority = index;
                promises.push(activitySetsService.update(activitySet));
            });

            $q.all(promises).then(function(results) {
                _(results).each(function(result) {
                    updatedActivitySets.push(result);
                });
                self.originalCourseCalendar = self.sortCourseCalendar(updatedActivitySets);
                $scope.courseCalendar = angular.copy(self.originalCourseCalendar);
                $scope.isBusySavingOrder = false;
            });
        };

        $scope.activitySetSortableOptions = {
            placeholder: 'coursecalendar-list-placeholder'
        };

        $scope.select = function(activitySet) {
            $state.go('customer.activities.overview.coursecalendar.activityset', { activitySetId: activitySet.Id });
        };

        $scope.isSelected = function (activitySet) {
            return parseInt($state.params.activitySetId, 10) === activitySet.Id;
        };

        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState) {
            if (toState.name === 'customer.activities.overview.coursecalendar' && fromState.name !== 'customer.activities.overview.coursecalendar.activityset') {
                if ($scope.courseCalendar.length > 0) {
                    $state.go('customer.activities.overview.coursecalendar.activityset', { activitySetId: $scope.courseCalendar[0].Id });
                }
            }
        });

        self.init = function () {
            $scope.isBusySavingOrder = false;
            self.originalCourseCalendar = self.sortCourseCalendar(courseCalendar);
            $scope.courseCalendar = angular.copy(self.originalCourseCalendar);
        };

        self.init();
    }
]);