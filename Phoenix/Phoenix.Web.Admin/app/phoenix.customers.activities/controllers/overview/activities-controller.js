﻿angular.module('phoenix.customers').controller('OverviewActivitiesCtrl', [
    '$scope',
    '$state',
    '$location',
    'activities',    
    'ActivitiesService',
    function ($scope, $state, $location, activities, activitiesService) {
        'use strict';

        $scope.activities = activities || { TotalCount: 0, Items: [] };
        $scope.query = $location.search().query;
        $scope.orderBy = $location.search().orderBy || '';
        $scope.orderDirection = $location.search().orderDirection || '';
        $scope.page = $location.search().page;
        $scope.limit = $location.search().limit || 15;
        $scope.maxPages = $scope.activities.TotalCount;

        $scope.onQueryChanged = function () {
            if ($scope.query === '') {
                $scope.search();
            }
        };

        $scope.search = function () {
            if (!$state.params.query && $scope.query === '') { return; }
            $state.go('customer.activities.overview.activities.list', { query: $scope.query });
        };

        $scope.navigate = function (page) {
            if (page < 1 || page > $scope.maxPages) {
                return;
            }
            $location.search('page', page);
        };

        $scope.deleteActivity = function (activityInList) {
            if (confirm("Do you really want to delete the activity?")) {
                activityInList.isBusyDeleting = true;
                activitiesService.get(activityInList.ActivityId).then(function (activityToDelete) {
                    activityToDelete.IsDeleted = true;
                    activitiesService.update(activityToDelete).then(function () {
                        activityInList.isBusyDeleting = false;
                        $state.go('customer.activities.overview.activities.list', {}, { inherit: true, reload: true });
                    }, function () {
                        activityInList.isBusyDeleting = false;
                    });
                });
            }
        };
    }
]);