angular.module('phoenix.customers').controller('ActivityEditCtrl', [
    '$scope',
    '$state',
    '$modal',
    '$q',
    'ActivitiesService',
    'customer',
    'data',
    'participantUsers',
    'codes',
    'activitySets',
    'ParticipantsService',
    'LeadingTextsService',
    'CustomersService',
    'DistributorsService',
    'phoenix.config',
    'Timer',
    'ExamsService',
    '$filter',
    'participantResults',
    'RcosService',
    'CommentsService',
    function ($scope, $state, $modal, $q, activitiesService, customer, data, participantUsers, codes, activitySets, participantsService, leadingTextsService, customersService, distributorsService, config, Timer, examsService, $filter, participantResults, rcoService, commentsService) {
        'use strict';
        var self = this;
        $scope.splitHorizontal = true;
        $scope.customer = customer;
        $scope.educationalPartners = data.educationalPartners;
        /*
       * Save related entities by saving the activityWithRelatedItems. 
       * After saving, update the working activity to enable cancelling of edit
       */
        self.saveRelatedItems = function () {
            var activityToSave = angular.copy($scope.activityWithRelatedItems),
                promise;

            promise = activitiesService.update(activityToSave);
            promise.then(function (result) {
                $scope.originalActivity = result;
                $scope.activityWithRelatedItems = angular.copy($scope.originalActivity);
                $scope.activity.Resources = result.Resources;
                $scope.activity.Attachments = result.Attachments;
                $scope.activity.RowVersion = result.RowVersion;
            });
            return promise;
        };

        $scope.isCommentsLoaded = false;
        $scope.isParticipantCommentsLoaded = false;

        $scope.customerArticle = data.customerArticle;
        $scope.articleCopy = data.articleCopy;
        $scope.isExamArticle = $scope.articleCopy.ArticleTypeId === 4 || $scope.articleCopy.ArticleTypeId === 5 || $scope.articleCopy.ArticleTypeId === 9;
        $scope.isExamPlayerArticle = $scope.articleCopy.ArticleTypeId === 3 || $scope.articleCopy.ArticleTypeId === 6;
        $scope.activity = data.activity;
        $scope.currentActivityOrder = data.currentActivityOrder;
        $scope.rcoCourse = data.rcoCourse;
        $scope.rcoExam = data.rcoExam;
        $scope.examRcoCourses = data.examRcoCourses;
        $scope.examStatuses = data.examStatuses;

        $scope.participantUsers = participantUsers.Items || [];
        $scope.activitySets = activitySets;

        $scope.originalActivity = angular.copy($scope.activity);
        $scope.activityWithRelatedItems = angular.copy($scope.originalActivity);

        $scope.participantStatusCodes = codes.participantStatuses;
        $scope.participantResultCodes = participantResults;
        $scope.selectedParticipants = [];
        $scope.participantComments = [-1];

        $scope.isMultipleChoice = function () {
            return $scope.articleCopy.ArticleTypeId === 3;
        };

        $scope.isInternalCertification = function () {
            return $scope.articleCopy.ArticleTypeId === 6;
        };

        self.getCurrentParticipantOrder = function (participant) {

            if(participant.ParticipantOrderLines == null || participant.ParticipantOrderLines.length == 0)
                return null;

            return (_.max(participant.ParticipantOrderLines, function (orderLine) { return orderLine.Id })).Order;
        }

        $scope.getParticipantOrderStatus = function (participant) {
            var currentOrder = participant.CurrentOrder;

            if (currentOrder === null) {
                if (participant.IsNotBillable || moment().diff(participant.EarliestInvoiceDate, 'hours') < 0)
                    return "";
                else 
                    return "Proposed";
            }

            switch (currentOrder.Status) {
            case 1:
                return "Draft";
            case 2:
                return "Transferred";
            case 3:
                return "Not Invoiced";
            case 4:
                if (currentOrder.IsCreditNote)
                    return "Credited";
                else
                    return "Invoiced";
            case 5:
                return "Rejected";
            default:
                return "Failed to retrieve";
            }
        }

        function getAllParticipantComments(){
            $scope.isParticipantCommentsLoaded = false;

            commentsService.getAllParticipantComments($scope.activity.Id, true, true).then(function (data) {
                angular.copy(data, $scope.participantComments);
                $scope.isParticipantCommentsLoaded = true;
            });
        }
        getAllParticipantComments();

        function addUserObjectsToParticipants() {
            _(participantUsers.Items).each(function (u) {
                var participant = _($scope.activityWithRelatedItems.Participants).find(function (p) { return p.UserId === u.Id; });
                if (participant) {
                    participant.user = u;
                    participant.CurrentOrder = self.getCurrentParticipantOrder(participant);
                }

                var filteredParticipant = _($scope.filteredParticipants).find(function (p) { return p.UserId === u.Id; });
                if (filteredParticipant) {
                    filteredParticipant.user = u;
                }
            });
        }
        addUserObjectsToParticipants();


        function initExamStatusTimers() {
            _($scope.examStatuses).each(function (status) {
                initExamStatusTimer(status);
            });
        }
        initExamStatusTimers();

        function initExamStatusTimer(status) {
            status.timer = new Timer(status.TimeRemainingInSeconds);
            setStatusText(status);
            status.timer.onTextUpdated = function () {
                setStatusText(status);
            };
            status.timer.start();
        }

        function setStatusText(status) {
            if (status.IsSubmitted) {
                status.text = 'Submitted';
            } else {
                if (status.IsStarted) {
                    status.text = status.timer.timerText;
                } else {
                    status.text = 'Not started';
                }
            }
        }

        function updateExamStatuses() {
            activitiesService.getExamStatuses($scope.activity.Id).then(function (updatedStatuses) {
                _(updatedStatuses).each(function (updatedStatus) {
                    var status = _($scope.examStatuses).find(function (s) { return s.ParticipantId === updatedStatus.ParticipantId; });
                    if (status) {
                        status.IsSubmitted = updatedStatus.IsSubmitted;
                        status.TimeRemainingInSeconds = updatedStatus.TimeRemainingInSeconds;
                        status.timer.secondsRemaining = updatedStatus.TimeRemainingInSeconds;
                        setStatusText(status);
                    } else {
                        $scope.examStatuses.push(updatedStatus);
                        initExamStatusTimer(updatedStatus);
                    }
                });
            });
        }
        
        if ($scope.activity.Id) {
            if ($scope.articleCopy.ArticleTypeId === 1) {
                $scope.reportUrl = config.apiPath + 'reports/progressstatus/xlsx?activityId=' + $scope.activity.Id;
            } else {
                $scope.reportUrl = config.apiPath + 'reports/activityparticipants/xlsx?activityId=' + $scope.activity.Id;
            }
        }
        
        $scope.getExamStatus = function (participantId) {
            return _($scope.examStatuses).find(function (s) {
                return s.ParticipantId === participantId;
            });
        };

        $scope.getCurrencyCode = function (currencyCodeId) {
            return _(codes.currencyCodes).find(function (c) { return c.Id === currencyCodeId; }).Value;
        };
        
        $scope.save = function () {
            $scope.isBusySaving = true;
            $scope.$broadcast('saveActivity');
        };

        $scope.bulkSetGrades = function () {
            var participants = _($scope.activityWithRelatedItems.Participants).map(function (par) {
                return {
                    Id: par.Id,
                    Email: par.user.Email,
                    FirstName: par.user.FirstName,
                    LastName: par.user.LastName
                };
            });
            $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.customers.activities/views/modals/participants-bulkgrade-modal.html',
                controller: 'ParticipantsBulkGradeModalCtrl',
                resolve: {
                    activityParticipants: function () { return participants; }
                },
                windowClass: 'modal-wide'
            }).result.then(function() {
                $state.go($state.current, $state.params, { reload: true });
            });
        };

        $scope.editSelectedParticipants = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.customers.activities/views/modals/participants-batch-edit-modal.html',
                controller: 'ParticipantsBatchEditModalCtrl',
                resolve: {
                    codes: function () { return codes; },
                    participantResults: function () { return participantResults; }
                },
                windowClass: 'modal-wide'
            });
            modalInstance.result.then(function (values) {
                $scope.isSavingParticipants = true;
                self.applyValuesToParticipants(values, self.getSelectedParticipants()).then(function () {
                    $scope.isSavingParticipants = false;
                    updateExamStatuses();
                }, function () {
                    $scope.isSavingParticipants = false;
                });
            });
        };

        $scope.toggleSelectAllParticipants = function () {
            _($scope.filteredParticipants).each(function (p) {
                p.isSelected = !!$scope.isAllParticipantsSelected;
            });
            $scope.participantSelected();
        };

        $scope.participantSelected = function () {
            $scope.isAllParticipantsSelected = !_($scope.filteredParticipants).some(function (p) {
                return !p.isSelected;
            });
            $scope.selectedParticipants = self.getSelectedParticipants();
        };

        self.getSelectedParticipants = function () {
            return _($scope.filteredParticipants).filter(function (p) {
                return p.isSelected;
            });
        };

        $scope.filterParticipants = function () {
            $scope.filteredParticipants = $filter('filterDeep')($scope.activityWithRelatedItems.Participants, 'user.Email,user.FirstName,user.LastName', $scope.participantFilter);
            $scope.participantSelected();
        };
        $scope.filterParticipants();

        self.applyValuesToParticipants = function (values, participants) {
            var promises = _(participants).map(function (p) {
                return self.applyValuesToParticipant(values, p);
            });

            return $q.all(promises).finally(function() {
                $scope.participantSelected();
                addUserObjectsToParticipants();
            });
        };

        self.addValuesToExamTotalScore = function (participantId, valueToAdd) {
            var updateScoreDeffered = $q.defer();
            if (_.isNumber(valueToAdd) && valueToAdd !== 0) {
                examsService.getExamResult(participantId).then(function (exam) {
                    if (exam && exam.IsSubmitted) {
                        var newTotalScore = (exam.TotalScore || 0) + (valueToAdd);
                        examsService.overrideTotalScore(exam.Id, newTotalScore).then(function () {
                            updateScoreDeffered.resolve();
                        }, function () {
                            updateScoreDeffered.reject();
                        });
                    } else {
                        // Do not update if not submitted
                        updateScoreDeffered.resolve();
                    }
                }, function () {
                    updateScoreDeffered.reject();
                });
            } else {
                // Do not update if not number
                updateScoreDeffered.resolve();
            }
            return updateScoreDeffered.promise;
        };

        self.updateParticipant = function (values, participant) {
            for (var key in values) {
                participant[key] = values[key];
            }

            // Increase or decrease additional time from 'AdditiveAdditionalTime' property
            delete participant.AdditiveAdditionalTime;
            if (_.isNumber(values.AdditiveAdditionalTime)) {
                participant.AdditionalTime += values.AdditiveAdditionalTime;
                if (participant.AdditionalTime < 0) {
                    participant.AdditionalTime = 0;
                }
            }
            return participantsService.update(participant).then(function (result) {
                participant.isBusyEditing = false;
                self.replaceUpdatedParticipant(result, $scope.activityWithRelatedItems.Participants);
                self.replaceUpdatedParticipant(result, $scope.filteredParticipants);
                addUserObjectsToParticipants();
                $scope.participantSelected();
                return result;
            });
        };

        self.applyValuesToParticipant = function (values, participant) {
            var participantBeforeChange;
            participant.isBusyEditing = true;           

            return self.addValuesToExamTotalScore(participant.Id, values.AdditiveTotalScore)
                .then(function () { return participantsService.get(participant.Id); })
                .then(function (p) {
                    participantBeforeChange = angular.copy(p);
                    return self.updateParticipant(values, p);
                })
                .then(function (updatedParticipant) {
                    self.replaceUpdatedParticipant(updatedParticipant, $scope.activityWithRelatedItems.Participants);
                    self.replaceUpdatedParticipant(updatedParticipant, $scope.filteredParticipants);
                    addUserObjectsToParticipants();
                }, function () {
                    self.replaceUpdatedParticipant(participantBeforeChange, $scope.activityWithRelatedItems.Participants);
                    self.replaceUpdatedParticipant(participantBeforeChange, $scope.filteredParticipants);
                    addUserObjectsToParticipants();
                })
                .finally(function () {
                    participant.isBusyEditing = false;
                    $scope.participantSelected();
                });
        };
        
        $scope.$on('ActivitySaved', function (event, savedActivity) {
            $scope.isBusySaving = false;
            $scope.originalActivity = angular.copy(savedActivity);
            $scope.activity = angular.copy($scope.originalActivity);
            $scope.activityWithRelatedItems = angular.copy($scope.originalActivity);
            updateExamStatuses();
            $state.go('customer.activities.overview.activities.item.view', { selectedTab: $state.params.selectedTab });
        });

        $scope.$on('ActivitySaveFailed', function () {
            $scope.isBusySaving = false;
        });

        $scope.cancel = function () {
            $scope.activity = angular.copy($scope.originalActivity);
            $state.go('customer.activities.overview.activities.item.view', { selectedTab: $state.params.selectedTab });
        };

        $scope.edit = function () {
            $scope.activity = angular.copy($scope.originalActivity);
            $scope.$broadcast('init', $scope.activity);
            $state.go('customer.activities.overview.activities.item.edit', { selectedTab: $state.params.selectedTab });
        };

        $scope.selectTab = function (tabName) {
            $state.go($state.current.name, { selectedTab: tabName });
        };

        $scope.commentsUpdated = function (comments) {
            $scope.isCommentsLoaded = true;
            $scope.commentCount = comments.length;
        };

        self.replaceUpdatedParticipant = function (updatedParticipant, participantsArray) {
            var existingParticipant = _(participantsArray).find(function (p) {
                return p.Id === updatedParticipant.Id;
            }), index;
            index = participantsArray.indexOf(existingParticipant);
            if (index > -1) {
                participantsArray[index] = updatedParticipant;
            }
        };

        self.removeParticipant = function (participant, participantArray) {
            var existingParticipant = _(participantArray).find(function (p) {
                return p.Id === participant.Id;
            }), index;
            index = participantArray.indexOf(existingParticipant);
            if (index > -1) {
                participantArray.splice(index, 1);
            }
        };
        
        $scope.getExamRcoCourse = function (rcoId) {
            return _($scope.examRcoCourses).find(function (rco) { return rco.Id === rcoId; });
        };

        $scope.participantStatusChanged = function (participant) {
            self.applyValuesToParticipant({ StatusCodeValue: participant.StatusCodeValue }, participant);
        };

        $scope.participantResultChanged = function (participant) {
            self.applyValuesToParticipant({ Result: participant.Result }, participant);
        };
        
        $scope.editParticipant = function (participant) {
            participant.isBusyEditing = true;
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.customers.activities/views/modals/participant-edit-modal.html',
                controller: 'ParticipantEditModalCtrl',
                resolve: {
                    participant: function () { return participantsService.get(participant.Id); },
                    customer: function () { return customer.Id === participant.CustomerId ? customer : customersService.get(participant.CustomerId); },
                    distributors: function () { return distributorsService.getAll(); },
                    currentOrder: function () { return participantsService.getCurrentParticipantOrder(participant.Id); },
                    predefinedLeadingTexts: function () { return leadingTextsService.getAllPredefined(); },
                    codes: function () { return codes; },
                    course: function () { return participantsService.getParticipantCourse(participant.Id); },
                    examContainer: function () { return participantsService.getParticipantExamContainer(participant.Id); },
                    exam: function () { return examsService.getExamResult(participant.Id); },
                    article: function () { return participantsService.getArticle(participant.Id); },
                    participantGradingComments: ['CommentsService', function (commentsService) { return commentsService.get('participantGrading', participant.Id); }]
                },
                windowClass: 'modal-wider'
            });
            modalInstance.result.then(function (updatedParticipant) {
                self.replaceUpdatedParticipant(updatedParticipant, $scope.activityWithRelatedItems.Participants);
                self.replaceUpdatedParticipant(updatedParticipant, $scope.filteredParticipants);
                addUserObjectsToParticipants();
                updateExamStatuses();
                getAllParticipantComments();
            });
            modalInstance.opened.then(function () {
                participant.isBusyEditing = false;
            });
        };

        $scope.moveParticipant = function (participant) {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.customers.activities/views/modals/move-participant-modal.html',
                controller: 'MoveParticipantModalCtrl',
                resolve: {
                    participantId: function () { return participant.Id; },
                    customerId: function () { return customer.Id; },
                    articleOriginalId: function () { return $scope.customerArticle.ArticleOriginalId; }
                },
                windowClass: 'modal-wider'
            });
            modalInstance.result.then(function () {
                // Remove the participant from all objects
                self.removeParticipant(participant, $scope.activityWithRelatedItems.Participants);
                self.removeParticipant(participant, $scope.activity.Participants);
                self.removeParticipant(participant, $scope.originalActivity.Participants);
                self.removeParticipant(participant, $scope.filteredParticipants);
            });
        };

        $scope.openRcoCourseInfoModal = function (rco) {
            $modal.open({
                templateUrl: config.basePath + 'app/phoenix.products/views/modals/rco-course-info-modal.html',
                controller: 'RcoCourseInfoModalCtrl',
                resolve: {
                    rcoCourse: function () {
                        return rco || $scope.rcoCourse;
                    }
                },
                windowClass: 'modal-wide'
            });
        };

        $scope.openRcoExamInfoModal = function () {
            $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.products/views/modals/rco-exam-info-modal.html',
                controller: 'RcoExamInfoModalCtrl',
                resolve: {
                    rcoExam: function () {
                        return $scope.rcoExam;
                    }
                },
                windowClass: 'modal-wide'
            });
        };

        $scope.createActivitySet = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.customers.activities/views/modals/activityset-create-modal.html',
                controller: 'ActivitySetCreateModalCtrl',
                resolve: {
                    customerId: function () {
                        return customer.Id;
                    },
                    activityId: function() {
                        return $scope.activity.Id;
                    }
                },
                windowClass: 'modal-wide'
            });

            modalInstance.result.then(function (activitySet) {
                $scope.activitySets.push(activitySet);
            });
        };

        $scope.showQualificationPopup = function (participant) {
                $modal.open({
                    backdrop: 'static',
                    templateUrl: config.basePath + 'app/phoenix.customers.activities/views/modals/exam-qualification-modal.html',
                    controller: 'ExamQualificationModalCtrl',
                    resolve: {
                        activity: function() { return $scope.activity; },
                        participant: function() {
                            $scope.selectedParticipant = participant;
                            return $scope.selectedParticipant;
                        },
                        participantStatusCodes: function() { return $scope.participantStatusCodes; }
                    },
                windowClass: 'modal-wider'
            });
        };

        $scope.$on('AttachmentsChanged', function () {
            $scope.isSavingAttachments = true;
            self.saveRelatedItems().then(function() {
                $scope.isSavingAttachments = false;
            }, function() {
                $scope.isSavingAttachments = false;
            });
        });

        $scope.$on('ResourcesChanged', function () {
            $scope.isSavingResources = true;
            self.saveRelatedItems().then(function () {
                $scope.isSavingResources = false;
            }, function () {
                $scope.isSavingResources = false;
            });
        });

        $scope.$on('SaveActivityAutomail', function () {
            $scope.isSavingAutomail = true;
            self.saveRelatedItems().then(function () {
                $scope.isSavingAutomail = false;
                }, function () {
                $scope.isSavingAutomail = false;
            });
        });

        $scope.close = function () {
            $state.go(self.closeState, self.closeParams);
        };
        
        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            // Navigate on "Close" to where you came from. Default to activity list
            if (!self.closeState) {
                if (!fromState.name || fromState.name.indexOf('customer.activities.overview.activities.item') > -1 || fromState.name.indexOf('login') > -1) {
                    self.closeState = 'customer.activities.overview.activities.list';
                    self.closeParams = null;
                } else {
                    self.closeState = fromState;
                    self.closeParams = fromParams;
                }
            }
            
            $scope.isEditing = $state.is('customer.activities.overview.activities.item.edit');
            $scope.isEnrollmentConditionsTabSelected = $state.params.selectedTab === 'enrollmentconditions';
            $scope.isResourcesTabSelected = $state.params.selectedTab === 'resources';
            $scope.isAttachmentsTabSelected = $state.params.selectedTab === 'attachments';
            $scope.isAutomailsTabSelected = $state.params.selectedTab === 'automails';
            $scope.isCommentsTabSelected = $state.params.selectedTab === 'comments';
            $scope.isParticipantsTabSelected = $state.params.selectedTab === 'participants';
            $scope.isExamsTabSelected = $state.params.selectedTab === 'exams';
            $scope.isCustomInformationTabSelected = $state.params.selectedTab === 'customInformation';

            if (!$scope.isEditing) {
                $scope.activity = angular.copy($scope.originalActivity);
            }

            if (!$state.params.selectedTab) {
                $scope.selectTab('participants');
            }
        });
    }
]);