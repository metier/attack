angular.module('phoenix.customers').controller('ActivitySetCreateCtrl', [
    '$scope',
    '$state',
    '$modal',
    '$location',
    '$q',
    'phoenix.config',
    'ActivitiesService',
    'ActivitySetsService',
    'activitySet',
    'activities',
    'enrolledUsers',
    'customer',
    'NotificationService',
    'CustomersService',
    'CommentsService',
    function ($scope, $state, $modal, $location, $q, config, activitiesService, activitySetsService, activitySet, activities, enrolledUsers, customer, notificationService, customersService, commentsService) {
        'use strict';
        var self = this;
        
        $scope.createFormUrl = config.basePath + 'app/phoenix.customers.activities/views/activityset/activityset-create-form.html';
        $scope.activitySet = activitySet;
        $scope.enrolledUsers = enrolledUsers.Items || [];
        $scope.activities = activities.Items || [];
        $scope.originalActivitySet = angular.copy(activitySet);
        $scope.locals = { searchedCustomer: null };
        $scope.getSearchableUser = function (userId) {
            return _($scope.enrolledUsers).find(function (u) {
                return u.Id === userId;
            });
        };

        $scope.$watch('locals.searchedCustomer.Id', function(customerId) {
                if (customerId) {
                    customersService.get(customerId).then(function(result) {
                            $scope.onCustomerShareSelected(result);
                        }
                    );
                }
            }
        );

        $scope.getNumberOfEnrollments = function () {
            return _($scope.activitySet.Enrollments).filter(function (e) { return !e.IsCancelled; }).length;
        };
        
        self.addActivity = function (activityId) {
            return activitySetsService.addActivity($scope.activitySet.Id, activityId, false).then(self.refreshCurrentActivitySet);
        };

        self.removeActivity = function (activityId) {
            return activitySetsService.removeActivity($scope.activitySet.Id, activityId).then(self.refreshCurrentActivitySet);
        };

        self.enroll = function (userId, enrollmentDetails) {
            return activitySetsService.enroll($scope.activitySet.Id, userId, enrollmentDetails, false).then(self.refreshCurrentActivitySet);
        };

        self.unenroll = function (userId) {
            return activitySetsService.unenroll($scope.activitySet.Id, userId).then(self.refreshCurrentActivitySet);
        };

        self.refreshCurrentActivitySet = function (updatedActivitySet) {
            $scope.originalActivitySet = updatedActivitySet;
            $scope.activitySet.Activities = updatedActivitySet.Activities;
            $scope.activitySet.Enrollments = updatedActivitySet.Enrollments;
            $scope.activitySet.ActivitySetShares = updatedActivitySet.ActivitySetShares;
            $scope.activitySet.RowVersion = updatedActivitySet.RowVersion;
        };
        
        $scope.openSelectActivitiesModal = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.customers.activities/views/modals/activities-select-modal.html',
                controller: 'ActivitiesSelectModalCtrl',
                resolve: {
                    canSearchAllCustomers: function () { return true; },
                    onlyActivitiesProposedForOrder: function () { return false; },
                    customerId: function () { return customer.Id; }
                },
                windowClass: 'modal-wider'
            });
            modalInstance.result.then(function (selectedActivities) {
                // Only add activities with ID's that are not already in $scope.activities
                var newSelectedActivities = _(selectedActivities).reject(function (sa) {
                    var existingActivity = _($scope.activities).find(function (ea) { return ea.ActivityId === sa.ActivityId; });
                    return !!existingActivity;
                });
                $scope.activities = $scope.activities.concat(newSelectedActivities);

                $scope.isBusyAddingActivities = true;
                $q.all(_(newSelectedActivities).map(function (activity) {
                    var deferred = $q.defer();
                    self.addActivity(activity.ActivityId).then(function (success) {
                        deferred.resolve(success);
                    }, function (error) {
                        var index = $scope.activities.indexOf(activity);
                        if (index > -1) {
                            $scope.activities.splice(index, 1);
                        }
                        deferred.reject(error);
                    });
                    return deferred.promise;
                })).then(function () {
                    $scope.isBusyAddingActivities = false;
                }, function (error) {
                    $scope.isBusyAddingActivities = false;
                    showValidationErrors(error);
                });
            });
        };
        
        $scope.removeActivity = function (activity) {
            if (confirm("Do you really want to remove the activity from the activity set?")) {
                activity.isBusyRemoving = true;
                self.removeActivity(activity.ActivityId).then(function () {
                    activity.isBusyRemoving = false;
                    var index = $scope.activities.indexOf(activity);
                    if (index > -1) {
                        $scope.activities.splice(index, 1);
                    }
                }, function() {
                    activity.isBusyRemoving = false;
                });
            }
        };

        $scope.openSelectUsersModal = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.users/views/modals/users-select-modal.html',
                controller: 'UsersSelectModalCtrl',
                resolve: {
                    customerId: function () { return customer.Id; }
                },
                windowClass: 'modal-wider'
            });
            modalInstance.result.then(function (selectedUsers) {
                self.enrollUsers(selectedUsers);
            }, function() {
                $scope.isBusyAddingUsers = false;
            });
        };

        $scope.openCommentsModal = function (enrollment) {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.customers.activities/views/modals/enrollment-comments-modal.html',
                controller: 'EnrollmentCommentsModalCtrl',
                resolve: {
                    enrollment: function () { return enrollment; }
                },
                windowClass: 'modal-wider'
            });
            modalInstance.result.then(function () {
                    $scope.getEnrollmentComments(enrollment, true, true);
                }, function() {
                
            });
        };

        $scope.getEnrollmentComments = function(enrollment, includeUserComments, includeParticipantComments) {
            enrollment.isBusyEditing = true;

            commentsService.getEnrollmentComments(enrollment.Id, includeUserComments, includeParticipantComments).then(function (result) {
                    enrollment.comments = result;
                    enrollment.isBusyEditing = false;
            }
            );
        }

        $scope.openBulkEnrollModal = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.customers.activities/views/modals/participants-bulkenroll-modal.html',
                controller: 'ParticipantsBulkEnrollModalCtrl',
                resolve: {
                    activitySetId: function () { return $scope.activitySet.Id; },
                    customer: function () { return customer; },
                    leadingTexts: ['LeadingTextsService', function (leadingTextsService) {
                        return leadingTextsService.getAllPredefined();
                    }],
                    enrolledUsers: function() {
                        return _(_($scope.activitySet.Enrollments).reject(function (e) { return e.IsCancelled; })).map(function(e) {
                            return $scope.getSearchableUser(e.UserId);
                        });
                    }
                },
                windowClass: 'modal-widest'
            });
            modalInstance.result.then(function (users) {
                // Only add users with ID's that are not already in $scope.enrolledUsers
                var newSelectedUsers = _(users).reject(function (su) {
                    var existingUser = _($scope.enrolledUsers).find(function (eu) { return eu.UserId === su.Id; });
                    return !!existingUser;
                });
                $scope.enrolledUsers = $scope.enrolledUsers.concat(newSelectedUsers);

                activitySetsService.get($scope.activitySet.Id).then(self.refreshCurrentActivitySet);
            }, function () {
                $scope.isBusyAddingUsers = false;
            });
        };

        function showValidationErrors(error) {
            if (error.ValidationErrors && error.ValidationErrors.length > 0) {
                _(error.ValidationErrors).each(function (e) {
                    notificationService.alertError({ message: e, header: 'Validation Error' });
                });
            }
        }

        self.enrollUsers = function (users) {
            // Only add users with ID's that are not already in $scope.enrolledUsers
            var newSelectedUsers = _(users).reject(function (su) {
                var existingUser = _($scope.enrolledUsers).find(function (eu) { return eu.UserId === su.Id; });
                return !!existingUser;
            });
            $scope.enrolledUsers = $scope.enrolledUsers.concat(newSelectedUsers);

            $scope.isBusyAddingUsers = true;
            $q.all(_(newSelectedUsers).map(function (user) {
                return self.enroll(user.Id, {"InfoElements": user.InfoElements });
            })).then(function () {
                $scope.isBusyAddingUsers = false;
            }, function (error) {
                showValidationErrors(error);
                $scope.isBusyAddingUsers = false;
            });
        };

        $scope.removeUser = function (enrollment) {
            if (confirm("Do you really want to remove the enrolled user from the activity set?")) {
                enrollment.isBusyRemoving = true;
                self.unenroll(enrollment.UserId).then(function () {
                    enrollment.isBusyRemoving = false;
                    var index = $scope.activitySet.Enrollments.indexOf(enrollment);
                    if (index > -1) {
                        $scope.activitySet.Enrollments.splice(index, 1);
                    }
                }, function () {
                    enrollment.isBusyRemoving = false;
                });
            }
        };

        $scope.searchCustomer = function (searchString) {
            return customersService.search({ query: searchString, limit: 50 }).then(function (result) {
                return result.Items;
            });
        };

        function shareWithCustomer(customerId) {
            return activitySetsService.share($scope.activitySet.Id, customerId);
        }

        $scope.onCustomerShareSelected = function (selectedCustomer) {
            if (selectedCustomer) {
                var isShared = _($scope.activitySet.ActivitySetShares).some(function (c) {
                    return c.CustomerId === selectedCustomer.Id;
                });
                if (!isShared) {
                    shareWithCustomer(selectedCustomer.Id).then(self.refreshCurrentActivitySet);
                }
            }
            $scope.locals.searchedCustomer = null;
        };
        
        $scope.unshareCustomer = function (sharedCustomer) {
            activitySetsService.unshare($scope.activitySet.Id, sharedCustomer.Id).then(self.refreshCurrentActivitySet);
        };

        $scope.selectSubOrganizations = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.customers/views/modals/sub-orgs-select-modal.html',
                controller: 'SubOrgsSelectModalCtrl',
                resolve: {
                    customer: function () { return customer; },
                    subOrganizations: function () { return customersService.search({ childrenOfCustomerId: customer.Id, limit: -1 }); }
                }
            });
            modalInstance.result.then(function (selectedSubOrgs) {
                var promises = _(selectedSubOrgs).map(function (subOrg) {
                    return shareWithCustomer(subOrg.Id);
                });
                $q.all(promises).then(function (resolves) {
                    // Of all successful share-request, find the one with the most CustomerShare (the newest one)
                    var latestUpdate = _(resolves).max(function (as) {
                        return as.ActivitySetShares.length;
                    });
                    self.refreshCurrentActivitySet(latestUpdate);
                });
            });
        };

        $scope.edit = function () {
            $state.go('customer.activities.overview.activitysets.item.edit', { selectedTab: $state.params.selectedTab });
        };

        $scope.save = function () {
            $scope.isBusySaving = true;
            var updateFunc = $scope.isCreating ? activitySetsService.create : activitySetsService.update;
            updateFunc($scope.activitySet).then(function (result) {
                $scope.originalActivitySet = result;
                $scope.activitySet = angular.copy(result);
                if ($scope.isCreating) {
                    $state.go('customer.activities.overview.activitysets.item.view', { activitySetId: result.Id }, { location: 'replace' });
                } else {
                    $state.go('customer.activities.overview.activitysets.item.view', { selectedTab: $state.params.selectedTab });
                }
                $scope.isBusySaving = false;
            }, function() {
                $scope.isBusySaving = false;
            });
        };

        $scope.cancel = function () {
            if ($scope.isCreating) {
                $state.go('customer.activities.overview.activitysets.list');
            } else {
                $scope.activitySet = angular.copy($scope.originalActivitySet);
                $state.go('customer.activities.overview.activitysets.item.view', { selectedTab: $state.params.selectedTab });
            }
        };

        $scope.selectTab = function (tabName) {
            $state.go($state.current.name, { selectedTab: tabName });
        };

        $scope.close = function () {
            $state.go(self.closeState, self.closeParams);
        };

        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            // Navigate on "Close" to where you came from. Default to activity set list
            if (!self.closeState) {
                if (!fromState.name || fromState.name.indexOf('customer.activities.overview.activitysets.item') > -1 || fromState.name.indexOf('login') > -1) {
                    self.closeState = 'customer.activities.overview.activitysets.list';
                    self.closeParams = null;
                } else {
                    self.closeState = fromState;
                    self.closeParams = fromParams;
                }
            }

            $scope.isActivitiesTabSelected = $state.params.selectedTab === 'activities';
            $scope.isEnrollmentsTabSelected = $state.params.selectedTab === 'enrollments';
            $scope.isShareTabSelected = $state.params.selectedTab === 'share';
            $scope.isEditing = $state.is('customer.activities.overview.activitysets.item.edit');
            $scope.isCreating = $state.is('customer.activities.overview.activitysets.create');

            if (!$scope.isEditing) {
                $scope.activitySet = angular.copy($scope.originalActivitySet);
            }

            if (!$scope.isCreating && !$state.params.selectedTab) {
                $scope.selectTab('activities');
            }
        });
    }
]);