﻿angular.module('phoenix.customers').factory('ActivityModalHelper', [
    'phoenix.config',
    '$q',
    '$modal',
    'CustomerArticlesService',
    'CustomersService',
    'ActivitiesService',
    'ArticlesService',
    function (config, $q, $modal, customerArticlesService, customersService, activitiesService, articlesService) {
        'use strict';
        return {
            /**
             * Helper function for all the plumbing required to open an activity edit modal.
             *
             * @return Object with two promises: 'opened' and 'saved'
             *
             * @param activityId {number}
             */
            openEditModal: function (activityId) {
                var saveDeferred = $q.defer(),
                    modalOpenDeferred = $q.defer();

                activitiesService.get(activityId).then(function (activity) {
                    customerArticlesService.get(activity.CustomerArticleId).then(function(customerArticle) {
                        var modalInstance = $modal.open({
                            backdrop: 'static',
                            templateUrl: config.basePath + 'app/phoenix.customers.activities/views/modals/activity-edit-modal.html',
                            controller: 'ActivityEditModalCtrl',
                            resolve: {
                                customer: function () { return customersService.get(customerArticle.CustomerId); },
                                customerArticle: function() { return customerArticle; },
                                editActivity: function () { return activity; },
                                currentActivityOrder: function () { return activitiesService.getCurrentActivityOrder(activityId); },
                                articleCopy: function() { return articlesService.get(customerArticle.ArticleCopyId); }
                            },
                            windowClass: 'modal-wide'
                        });
                        modalInstance.result.then(function(a) {
                            saveDeferred.resolve(a);
                        });

                        modalInstance.opened.then(function() {
                            modalOpenDeferred.resolve();
                        });
                    }, function() {
                        modalOpenDeferred.reject();
                    });
                }, function() {
                    modalOpenDeferred.reject();
                });
                
                return {
                    saved: saveDeferred.promise,
                    opened: modalOpenDeferred.promise
                };
            }
        };
    }
]);