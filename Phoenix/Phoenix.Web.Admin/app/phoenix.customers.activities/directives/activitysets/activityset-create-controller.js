﻿angular.module('phoenix.customers').controller('ActivitysetCreateDirectiveCtrl', [
    '$scope',
    function($scope) {
        'use strict';
        $scope.isUnenrollmentPrevented = !$scope.activitySet.IsUnenrollmentAllowed;

        $scope.isUnenrollmentPreventedChanged = function() {
            if ($scope.isUnenrollmentPrevented) {
                $scope.activitySet.UnenrollmentDeadline = null;
            }
            $scope.activitySet.IsUnenrollmentAllowed = !$scope.isUnenrollmentPrevented;
        };

        $scope.isPublishedChanged = function() {
            if (!$scope.activitySet.IsPublished) {
                $scope.activitySet.PublishFrom = null;
                $scope.activitySet.PublishTo = null;
            } else {
                $scope.activitySet.PublishFrom = moment().toDate();
            }
        };

        $scope.openDatepicker = function($event, datePickerBoolPropertyName) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope[datePickerBoolPropertyName] = true;
        };
    }
]);