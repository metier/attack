﻿angular.module('phoenix.customers').directive('activitysetCreate', [
    'phoenix.config',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            controller: 'ActivitysetCreateDirectiveCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.activities/directives/activitysets/activityset-create.html'
        };
    }
]);