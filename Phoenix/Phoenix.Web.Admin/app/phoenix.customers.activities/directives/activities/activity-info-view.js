angular.module('phoenix.customers').directive('activityInfoView', [
    'phoenix.config',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            templateUrl: config.basePath + 'app/phoenix.customers.activities/directives/activities/activity-info-view.html',
            controller: ['$scope', 'CodesService', function ($scope, codesService) {
                var self = this;

                $scope.$watch('activity', function () {
                    self.init();
                });

                self.getCurrencyCode = function (codeId) {
                    var currency = _(self.currencyCodes).find(function (c) { return c.Id === codeId; });
                    if (currency) {
                        return currency.Value;
                    }
                    return '';
                };

                self.init = function() {
                    if(!$scope.selectedEducationalPartner)
                        $scope.selectedEducationalPartner = { Id: $scope.activity.EducationalPartnerResourceId };

                    if (self.currencyCodes) {
                        $scope.currencyCode = self.getCurrencyCode($scope.activity.CurrencyCodeId);
                    } else {
                        codesService.getAll().then(function (codes) {
                            self.currencyCodes = codes.currencyCodes;
                            $scope.currencyCode = self.getCurrencyCode($scope.activity.CurrencyCodeId);
                        });
                    }
                };
            }]
        };
    }
]);