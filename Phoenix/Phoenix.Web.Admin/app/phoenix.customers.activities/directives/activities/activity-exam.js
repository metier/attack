﻿angular.module('phoenix.customers').directive('activityExam', [
    'phoenix.config',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            controller: 'ActivityExamCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.activities/directives/activities/activity-exam.html'
        };

    }
]);