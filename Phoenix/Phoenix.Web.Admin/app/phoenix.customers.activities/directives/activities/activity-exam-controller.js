﻿angular.module('phoenix.customers').controller('ActivityExamCtrl', [
    '$scope',
    '$q',
    'RcosService',
    function ($scope, $q, rcosService) {
        'use strict';
        $scope.examRcoCourses = [];

        $scope.getExamRcoCourse = function (rcoId) {
            return _($scope.examRcoCourses).find(function (rco) { return rco.Id === rcoId; });
        };

        function setExamCoursesOnScope(activity) {
            var deferred = $q.defer();

            if (activity.ActivityExamCourses && activity.ActivityExamCourses.length > 0) {
                var examCoursePromises = _($scope.activity.ActivityExamCourses).map(function (course) {
                    return rcosService.getCourse(course.CourseRcoId);
                });

                $q.all(examCoursePromises).then(function (rcos) {
                    _(rcos).each(function (rco) {
                        $scope.examRcoCourses.push(rco);
                    });
                }).finally(function () {
                    deferred.resolve();
                });
            } else {
                deferred.resolve();
            }

            return deferred.promise;
        }

        function init() {
            var promises = [];

            promises.push(setExamCoursesOnScope($scope.activity));

            $q.all(promises).finally(function () {
                $scope.isInitialized = true;
            });
        }

        init();
    }
]);