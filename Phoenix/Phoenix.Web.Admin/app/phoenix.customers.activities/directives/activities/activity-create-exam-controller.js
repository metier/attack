﻿angular.module('phoenix.customers').controller('ActivityCreateExamCtrl', [
    '$scope',
    'RcosService',
    '$q',
    '$modal',
    'phoenix.config',
    function ($scope, rcosService, $q, $modal, config) {
        'use strict';
        $scope.isInitialized = false;
        $scope.examRcoCourses = [];

        $scope.getExamRcoCourse = function(rcoId) {
            return _($scope.examRcoCourses).find(function (rco) { return rco.Id === rcoId; });
        };

        $scope.openSelectExamCourseModal = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.products/views/modals/rco-course-select-modal.html',
                controller: 'RcoCourseSelectModalCtrl',
                resolve: {
                },
                windowClass: 'modal-wider'
            });

            modalInstance.result.then(function (selectedRcoCourse) {
                var existing = _($scope.activity.ActivityExamCourses).find(function (course) {
                    return course.CourseRcoId === selectedRcoCourse.Id;
                });
                if (!!existing) {
                    return;
                }

                rcosService.getCourse(selectedRcoCourse.Id).then(function (result) {
                    $scope.examRcoCourses.push(result);
                    $scope.activity.ActivityExamCourses.push({
                        ArticleId: $scope.activity.Id,
                        CourseRcoId: result.Id
                    });
                });
            });
        };

        $scope.removeActivityExamCourse = function(index) {
            if (index > -1) {
                $scope.activity.ActivityExamCourses.splice(index, 1);
            }
        };

        $scope.easyQuestionCountChanged = function () {
            $scope.updateNumberOfQuestions();
            if (!$scope.activity.EasyQuestionCount) {
                $scope.activity.EasyCustomerSpecificCount = null;
            }
            else if ($scope.activity.EasyQuestionCount < $scope.activity.EasyCustomerSpecificCount) {
                $scope.activity.EasyCustomerSpecificCount = $scope.activity.EasyQuestionCount;
            }
        };

        $scope.mediumQuestionCountChanged = function () {
            $scope.updateNumberOfQuestions();
            if (!$scope.activity.MediumQuestionCount) {
                $scope.activity.MediumCustomerSpecificCount = null;
            }
            else if ($scope.activity.MediumQuestionCount < $scope.activity.MediumCustomerSpecificCount) {
                $scope.activity.MediumCustomerSpecificCount = $scope.activity.MediumQuestionCount;
            }
        };

        $scope.hardQuestionCountChanged = function () {
            $scope.updateNumberOfQuestions();
            if (!$scope.activity.HardQuestionCount) {
                $scope.activity.HardCustomerSpecificCount = null;
            }
            else if ($scope.activity.HardQuestionCount < $scope.activity.HardCustomerSpecificCount) {
                $scope.activity.HardCustomerSpecificCount = $scope.activity.HardQuestionCount;
            }
        };

        $scope.updateNumberOfQuestions = function () {
            $scope.activity.NumberOfQuestions = ($scope.activity.EasyQuestionCount || 0) + ($scope.activity.MediumQuestionCount || 0) + ($scope.activity.HardQuestionCount || 0);
        };

        function setExamCoursesOnScope(activity) {
            var deferred = $q.defer();

            if (activity.ActivityExamCourses && activity.ActivityExamCourses.length > 0) {
                var examCoursePromises = _($scope.activity.ActivityExamCourses).map(function(course) {
                    return rcosService.getCourse(course.CourseRcoId);
                });

                $q.all(examCoursePromises).then(function (rcos) {
                    _(rcos).each(function(rco) {
                        $scope.examRcoCourses.push(rco);
                    });
                }).finally(function () {
                    deferred.resolve();
                });
            } else {
                deferred.resolve();
            }

            return deferred.promise;
        }

        function init() {
            var promises = [];

            promises.push(setExamCoursesOnScope($scope.activity));

            $q.all(promises).finally(function () {
                $scope.ects = ['5', '7.5', '10', '15'];     //TODO: Extract Into a config-file or DB. The same 'magic numbers' are duplicated in product-article-create-controller.js
                $scope.isInitialized = true;
            });
        }

        init();
    }
]);