﻿angular.module('phoenix.customers').directive('activityCreateExam', [
    'phoenix.config',
    function(config) {
        'use strict';
        return {
            restrict: 'E',
            controller: 'ActivityCreateExamCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.activities/directives/activities/activity-create-exam.html'
        };

    }
]);