﻿angular.module('phoenix.customers').controller('ActivityCreateDirectiveCtrl', [
    '$scope',
    'ActivitiesService',
    'NotificationService',
    'CodesService',
    'ResourcesService',
    function ($scope) {
        'use strict';
        $scope.selectedTab = 'info';
        $scope.isCommentsLoaded = false;

        $scope.commentsUpdated = function (comments) {
            $scope.isCommentsLoaded = true;
            $scope.commentCount = comments.length;
        };

        $scope.isMultipleChoice = function () {
            return $scope.articleCopy.ArticleTypeId === 3;
        };

        $scope.isInternalCertification = function() {
            return $scope.articleCopy.ArticleTypeId === 6;
        };
    }
]);