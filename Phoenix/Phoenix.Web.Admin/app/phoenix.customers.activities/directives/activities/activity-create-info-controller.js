/*global moment*/
angular.module('phoenix.customers').controller('ActivityCreateInfoCtrl', [
    '$scope',
    'ActivitiesService',
    'NotificationService',
    'CodesService',
    function ($scope, activitiesService, notificationService, codesService) {
        'use strict';
        var self = this;
        self.ARTICLETYPE_ELEARNING = 1;
        self.ARTICLETYPE_CLASSROOM = 2;
        self.ARTICLETYPE_MULTIPLE_CHOICE = 3;
        self.ARTICLETYPE_CASE_EXAM = 4;
        self.ARTICLETYPE_PROJECT_ASSIGNMENT = 5;
        self.ARTICLETYPE_INTERNAL_CERTIFICATION = 6;
        self.ARTICLETYPE_OTHER = 7;
        self.ARTICLETYPE_MOCKEXAM = 8;
        self.ARTICLETYPE_EXTERNAL_CERTIFICATION = 9;
        self.ARTICLETYPE_SUBSCRIPTION = 12;
        $scope.isExam = $scope.articleCopy.ArticleTypeId === self.ARTICLETYPE_MULTIPLE_CHOICE ||
                                $scope.articleCopy.ArticleTypeId === self.ARTICLETYPE_CASE_EXAM ||
                                $scope.articleCopy.ArticleTypeId === self.ARTICLETYPE_EXTERNAL_CERTIFICATION ||
                                $scope.articleCopy.ArticleTypeId === self.ARTICLETYPE_PROJECT_ASSIGNMENT ||
                                $scope.articleCopy.ArticleTypeId === self.ARTICLETYPE_INTERNAL_CERTIFICATION ||
                                $scope.articleCopy.ArticleTypeId === self.ARTICLETYPE_MOCKEXAM;

        $scope.endDateEnabled = $scope.articleCopy.ArticleTypeId !== self.ARTICLETYPE_MULTIPLE_CHOICE &&
                                $scope.articleCopy.ArticleTypeId !== self.ARTICLETYPE_ELEARNING &&
                                $scope.articleCopy.ArticleTypeId !== self.ARTICLETYPE_INTERNAL_CERTIFICATION;

        $scope.supportsMultipleActivityPeriods = $scope.articleCopy.ArticleTypeId !== self.ARTICLETYPE_CASE_EXAM &&
                                                 $scope.articleCopy.ArticleTypeId !== self.ARTICLETYPE_ELEARNING &&
                                                 $scope.articleCopy.ArticleTypeId !== self.ARTICLETYPE_EXTERNAL_CERTIFICATION &&
                                                 $scope.articleCopy.ArticleTypeId !== self.ARTICLETYPE_PROJECT_ASSIGNMENT &&
                                                 $scope.articleCopy.ArticleTypeId !== self.ARTICLETYPE_MULTIPLE_CHOICE &&
                                                 $scope.articleCopy.ArticleTypeId !== self.ARTICLETYPE_INTERNAL_CERTIFICATION;

        $scope.isElearning = $scope.articleCopy.ArticleTypeId === self.ARTICLETYPE_ELEARNING;
        $scope.isSubscription = $scope.articleCopy.ArticleTypeId === self.ARTICLETYPE_SUBSCRIPTION;

        if ($scope.isElearning || $scope.isSubscription){
            $scope.deadlineType = $scope.activity.Duration ? 'Duration' : 'EndDate';
        }
        else {
            $scope.deadlineType = $scope.activity.EndDate ? 'EndDate' : 'Duration';
        }
        
        
        $scope.validateCurrency = function () {
            $scope.isCurrencyRequired = !$scope.activity.FixedPriceNotBillable || !$scope.activity.UnitPriceNotBillable;
        };

        $scope.addPeriod = function () {
            $scope.tempActivityPeriods.push({
                ActivityId: $scope.activity.Id
            });
        };

        $scope.removePeriod = function (index) {
            if (confirm("Are you sure you want to remove the period?")) {
                $scope.tempActivityPeriods.splice(index, 1);
            }
        };

        self.filterEmptyPeriods = function (periods) {
            return _(periods).filter(function (p) { return !!p.Start || !!p.End; });
        };

        self.addTimeAndAdjustToSelectedTimezone = function (date, time) {
            var timeZoneMoment = moment.tz(date, $scope.activity.MomentTimezoneName);
            if (time) {
                var hours = parseInt(time.substring(0, 2), 10) || 0;
                var minutes = parseInt(time.substring(2, 4), 10) || 0;
                timeZoneMoment = timeZoneMoment.hours(hours).minutes(minutes);
            }
            return timeZoneMoment.format();
        };

        self.adjustPeriodsToSelectedTimezones = function (periods) {
            _(periods).each(function (p) {
                if (p.Start) {
                    p.Start = self.addTimeAndAdjustToSelectedTimezone(p.Start, p.startTime);
                }
                if (p.End) {
                    p.End = self.addTimeAndAdjustToSelectedTimezone(p.End, p.endTime);
                }
            });
        };

        self.prepareActivity = function () {
            var updateActivityPeriods = angular.copy($scope.tempActivityPeriods);

            if ($scope.isElearning || $scope.isSubscription) 
            {
                $scope.activity.EndDate = null;                
            }
            else if ($scope.deadlineType === "EndDate") {
                $scope.activity.EndDate = self.addTimeAndAdjustToSelectedTimezone($scope.activity.EndDate, $scope.activity.endTime);
            }

            self.adjustPeriodsToSelectedTimezones(self.filterEmptyPeriods(updateActivityPeriods));
            $scope.activity.ActivityPeriods = angular.copy(updateActivityPeriods);
        }

        $scope.saveActivity = function () {
            var updateFunc = $scope.isEditMode ? activitiesService.update : activitiesService.create;

            self.prepareActivity();

            updateFunc($scope.activity).then(function (saved) {
                $scope.$emit('ActivitySaved', saved);
                $scope.activity = saved;
                self.initActivityPeriods();
                self.initActivityEndTime();
            }, function () {
                $scope.$emit('ActivitySaveFailed');
                self.init();
            });
        };

        $scope.copyActivity = function (copyFromActivityId) {

            self.prepareActivity();

            activitiesService.copy($scope.activity, copyFromActivityId).then(function (saved) {
                $scope.$emit('ActivitySaved', saved);
                $scope.activity = saved;
                self.initActivityPeriods();
                self.initActivityEndTime();
            }, function () {
                $scope.$emit('ActivitySaveFailed');
                self.init();
            });
        };

        $scope.$on('saveActivity', function (evt, params) {
            if (params && params.copyFromActivityId && params.copyFromActivityId > 0)
                $scope.copyActivity(params.copyFromActivityId);
            else
                $scope.saveActivity();
        });

        $scope.$on('init', function () {
            self.init();
        });

        $scope.setTime = function (activityPeriod, timePropertyName) {
            var timeString = activityPeriod[timePropertyName];
            if (!timeString || timeString.length < 4) {
                activityPeriod[timePropertyName] = null;
                return;
            }
            var hours = parseInt(timeString.substring(0, 2), 10) || 0;
            var minutes = parseInt(timeString.substring(2, 4), 10) || 0;
            if (hours > 23) {
                activityPeriod[timePropertyName]= null;
                return;
            }
            if (minutes > 59) {
                activityPeriod[timePropertyName]= null;
                return;
            }
        };
        
        $scope.setActivityEndTime = function () {
            var timeString = $scope.activity.endTime;
            if (!timeString || timeString.length < 4) {
                $scope.activity.endTime = null;
                return;
            }
            var hours = parseInt(timeString.substring(0, 2), 10) || 0;
            var minutes = parseInt(timeString.substring(2, 4), 10) || 0;
            if (hours > 23) {
                $scope.activity.endTime = null;
                return;
            }
            if (minutes > 59) {
                $scope.activity.endTime = null;
                return;
            }
        };

        $scope.onToggleSelection = function (control) {
            if (control === "FixedPrice")
                $scope.activity.FixedPriceNotBillable = !$scope.activity.FixedPriceNotBillable;

            if (control === "UnitPrice")
                $scope.activity.UnitPriceNotBillable = !$scope.activity.UnitPriceNotBillable;

            $scope.validateCurrency();
        }

        $scope.onToggleDuration = function () {
            if ($scope.deadlineType === 'Duration') {
                $scope.activity.Duration = null;
                $scope.deadlineType = 'EndDate';
            } else {
                $scope.tempActivityPeriods[0].End = null;
                $scope.tempActivityPeriods[0].endTime = null;
                $scope.activity.ActivityPeriods[0].End = null;
                $scope.deadlineType = 'Duration';
            }
        }

        $scope.updateEducationalPartner = function() {
            $scope.activity.EducationalPartnerResourceId = $scope.selectedEducationalPartner.Id;
        }

        self.getDateTimePart = function(string) {
            // Remove the time zone part
            // Convert from 2014-07-13T04:00:00+09:00 to 2014-07-13T04:00:00
            return string.substring(0, 19);
        };

        self.initActivityPeriods = function() {
            // Dates will be on the format: 2014-07-13T04:00:00+09:00
            $scope.tempActivityPeriods = angular.copy($scope.activity.ActivityPeriods);
            _($scope.tempActivityPeriods).each(function(p) {
                if (p.Start) {
                    p.Start = self.getDateTimePart(p.Start);
                    p.startTime = moment(p.Start).format('HHmm');
                }
                if (p.End) {
                    p.End = self.getDateTimePart(p.End);
                    p.endTime = moment(p.End).format('HHmm');
                }
            });
        };

        self.initActivityEndTime = function() {
            if ($scope.deadlineType === "EndDate") {
                    if ($scope.activity.EndDate) {
                        var endDate = self.getDateTimePart($scope.activity.EndDate);
                        $scope.activity.endTime = moment(endDate).format('HHmm');
                    }
            }
        }

        self.init = function () {
            
            $scope.isEditMode = !!$scope.activity.Id;
            
            codesService.getAll().then(function (codes) {
                $scope.currencies = codes.currencyCodes;
            });
            
            $scope.activityLockedOnOrder = false;
            $scope.validateCurrency();

            if ($scope.currentActivityOrder) {
                if (!$scope.currentActivityOrder.IsCreditNote) {
                    $scope.activityLockedOnOrder = true;
                }
                if ($scope.currentActivityOrder.IsCreditNote && $scope.currentActivityOrder.Status === 1) {
                    $scope.activityLockedOnOrder = true;
                }
            }

            if(!$scope.selectedEducationalPartner)
                $scope.selectedEducationalPartner = { Id: $scope.activity.EducationalPartnerResourceId };

            self.initActivityPeriods();
            self.initActivityEndTime();
        };
        
        self.init();
    }
]);