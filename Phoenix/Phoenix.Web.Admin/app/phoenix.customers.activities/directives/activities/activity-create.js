﻿angular.module('phoenix.customers').directive('activityCreate', [
    'phoenix.config',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            controller: 'ActivityCreateDirectiveCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.activities/directives/activities/activity-create.html'
        };
    }
]);