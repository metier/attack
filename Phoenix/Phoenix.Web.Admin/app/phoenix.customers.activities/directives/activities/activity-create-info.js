﻿angular.module('phoenix.customers').directive('activityCreateInfo', [
    'phoenix.config',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            controller: 'ActivityCreateInfoCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.activities/directives/activities/activity-create-info.html'
        };
    }
]);