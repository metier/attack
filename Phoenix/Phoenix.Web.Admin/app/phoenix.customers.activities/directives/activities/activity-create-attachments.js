﻿angular.module('phoenix.customers').directive('activityCreateAttachments', [
    'phoenix.config',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                activity: '='  
            },
            templateUrl: config.basePath + 'app/phoenix.customers.activities/directives/activities/activity-create-attachments.html',
            controller: ['$scope', function ($scope) {
                $scope.onDocumentUploaded = function (attachment) {
                    if (attachment) {
                        $scope.activity.Attachments.push(attachment);
                        $scope.$emit('AttachmentsChanged');
                    }
                };

                $scope.deleteAttachment = function (index) {
                    if (confirm("Are you sure you want to remove the attachment?")) {
                        $scope.activity.Attachments.splice(index, 1);
                        $scope.$emit('AttachmentsChanged');
                    }
                };
            }]
        };
    }
]);