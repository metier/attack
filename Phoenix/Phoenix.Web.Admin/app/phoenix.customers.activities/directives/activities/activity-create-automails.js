﻿angular.module('phoenix.customers').directive('activityCreateAutomails', [
    'phoenix.config',
    function(config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                activity: '=',
                customer: '=',
                article: '=',
                isSaving: '=',
                dateOptions: '='
            },
            templateUrl: config.basePath + 'app/phoenix.customers.activities/directives/activities/activity-create-automails.html',
            controller: ['$scope', function ($scope) {
                $scope.showElearningCheckbox = $scope.article.ArticleTypeId === 1;
                $scope.showReminderCheckbox = $scope.article.ArticleTypeId === 2 ||
                                              $scope.article.ArticleTypeId === 3 ||
                                              $scope.article.ArticleTypeId === 4 ||
                                              $scope.article.ArticleTypeId === 5 ||
                                              $scope.article.ArticleTypeId === 6 ||
                                              $scope.article.ArticleTypeId === 9;
                $scope.showExamSubmissionCheckbox = $scope.article.ArticleTypeId === 4 || $scope.article.ArticleTypeId === 5;
                
                function init() {
                    $scope.deadlineType = $scope.activity.CompletionTargetDate? 'EndDate' : 'Duration';

                    if (!$scope.activity.Id) {
                        var automailDefinition = $scope.customer.AutomailDefinition;

                        if (automailDefinition) {
                            $scope.activity.SendProgressAutomail = automailDefinition.IsProgressNotificationElearning || false;
                            $scope.activity.SendReminderAutomail = automailDefinition.IsReminder || false;
                            $scope.activity.SendExamSubmissionAutomail = automailDefinition.IsExamSubmissionNotification || false;
                            $scope.activity.SendEnrollmentConfirmationAutomail = automailDefinition.IsEnrollmentNotification || false;
                            $scope.activity.ActivityAutomailText = {
                                Text: automailDefinition.AutomailText
                            };
                        }

                        if (!$scope.activity.UserRegistrationDate) {
                            $scope.activity.UserRegistrationDate = moment().toDate();
                        }
                        if (!$scope.activity.EnrollmentConfirmationDate) {
                            $scope.activity.EnrollmentConfirmationDate = moment().toDate();
                        }
                    } else {
                        if (!$scope.activity.ActivityAutomailText) {
                            $scope.activity.ActivityAutomailText = { Text: null };
                        }
                    }
                }

                $scope.saveAutomail = function () {
                    $scope.$emit('SaveActivityAutomail');
                };

                init();
            }]
        };
    }
]);