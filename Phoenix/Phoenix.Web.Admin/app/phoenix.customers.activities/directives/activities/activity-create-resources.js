﻿angular.module('phoenix.customers').directive('activityCreateResources', [
    'phoenix.config',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                activity: '='
            },
            templateUrl: config.basePath + 'app/phoenix.customers.activities/directives/activities/activity-create-resources.html',
            controller: ['$scope', 'ResourcesService', function ($scope, resourcesService) {
                $scope.resourcesSearchTemplateUrl = config.basePath + 'app/phoenix.resources/views/templates/resource-search-result-template.html';

                $scope.searchResources = function (searchString) {
                    return resourcesService.search({ query: searchString, searchColumn: ['name'], limit: 20 }).then(function (result) {
                        return result.Items;
                    });
                };

                $scope.onResourceSelected = function (resource) {
                    if (!_($scope.activity.Resources).find(function (item) { return item.Id === resource.Id; })) {
                        $scope.activity.Resources.push(resource);
                        $scope.$emit('ResourcesChanged');
                    }
                    $scope.selectedResource = null;
                };

                $scope.removeResource = function (index) {
                    if (confirm("Are you sure you want to remove the resource?")) {
                        $scope.activity.Resources.splice(index, 1);
                        $scope.$emit('ResourcesChanged');
                    }
                };
            }]
        };
    }
]);