﻿/*global angular, confirm */
/*jslint maxlen: 200 */
angular.module('phoenix').controller('NavigationCtrl', [
    '$scope',
    '$rootScope',
    '$location',
    '$state',
    '$q',
    'DistributorsService',
    'AuthenticationService',
    'phoenix.config',
    function ($scope, $rootScope, $location, $state, $q, distributorsService, authenticationService, config) {
        'use strict';
        $scope.selectDistributor = function (distributor) {
            var selectedDistributor = distributorsService.get();
            if (!$rootScope.selectedDistributor ||
                (selectedDistributor.Id !== distributor.Id && confirm('Do you really want to change the distributor at this point? All unsaved data will be lost.'))) {
                distributorsService.set(distributor);
                $rootScope.selectedDistributor = distributorsService.get();
                $state.go(config.defaultState, null, { reload: true });
            }
        };
        
        $scope.isActive = function (route) {
            return $location.path().indexOf(route) === 0;
        };

        $scope.signOut = function() {
            authenticationService.logout().then(function() {
                $state.go('login');
            });
        };

        function initApp() {
            $rootScope.isLoading = true;
            var promises = [
                distributorsService.getAll(),
                authenticationService.getLoggedInUser()
            ];

            $scope.isTestEnvironment = $location.$$host.indexOf('mymetier.net') !== 0;

            $q.all(promises).then(function (results) {
                $rootScope.distributors = results[0];
                $rootScope.loggedInUser = results[1];
                $rootScope.selectedDistributor = distributorsService.get();
            }).finally(function () {
                $rootScope.isLoading = false;
            });
        }
        
        $rootScope.$on('userSaved', function (event, result) {
            if ($rootScope.loggedInUser && result.Username === $rootScope.loggedInUser.Username) {
                initApp();
            }
        });

        $rootScope.$on('userLoggedOut', function () {
            $rootScope.loggedInUser = null;
            $rootScope.selectedDistributor = null;
        });
        
        initApp();
    }
]);