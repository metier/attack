﻿angular.module('phoenix').controller('ResetPasswordCtrl', [
    '$scope',
    '$location',
    '$state',
    'UsersService',
    'AuthenticationService',
    'phoenix.config',
    function ($scope, $location, $state, usersService, authenticationService, config) {
        'use strict';

        function init() {
            $scope.token = $location.search().token || '';
            $scope.model = {};
        }

        $scope.resetPassword = function () {
            $scope.isBusy = true;
            usersService.resetPassword($scope.token, $scope.model.email, $scope.model.password).then(function() {
                authenticationService.getLoggedInUser().then(function(result) {
                    $scope.isBusy = false;
                    if (result) {
                        $state.go(config.defaultState);
                    } else {
                        $state.go('login');
                    }
                }, function() {
                    $scope.isBusy = false;
                    $state.go('login');
                });
            }, function() {
                $scope.isBusy = false;
            });
        };

        init();
    }
]);