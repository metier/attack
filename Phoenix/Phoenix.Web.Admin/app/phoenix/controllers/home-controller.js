﻿/*global angular */
angular.module('phoenix').controller('HomeCtrl', ['$scope', 'DistributorsService', function ($scope, distributorsService) {
    'use strict';
    $scope.isSuperUsersVisible = distributorsService.get().Id === 1194;
}]);
