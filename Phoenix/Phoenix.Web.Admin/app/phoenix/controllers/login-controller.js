﻿/*global angular */
angular.module('phoenix').controller('LoginCtrl', [
    '$scope',
    '$rootScope',
    '$state',
    '$window',
    '$location',
    'AuthenticationService',
    'DistributorsService',
    'phoenix.config',
    function ($scope, $rootScope, $state, $window, $location, authenticationService, distributorsService, config) {
        'use strict';

        function getReturnUrl() {
            var urlParts = $location.url().split('returnUrl=');
            return urlParts.length >= 2 ? decodeURIComponent(urlParts[1]) : null;
        }

        $scope.login = function () {
            if (!$scope.username || !$scope.password) {
                return;
            }
            $scope.isBusy = true;
            authenticationService.login($scope.username, $scope.password).then(function (userContext) {
                distributorsService.set(userContext.DefaultDistributor);
                var returnUrl = getReturnUrl();
                if (returnUrl) {
                    $window.location = returnUrl;
                } else {
                    $state.go(config.defaultState, null, { location: 'replace' });
                }
            }).finally(function() {
                $scope.isBusy = false;
            });
        };
    }
]);