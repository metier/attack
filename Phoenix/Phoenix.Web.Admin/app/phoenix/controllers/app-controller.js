﻿angular.module('phoenix').controller('AppCtrl', [
    '$scope',
    '$state',
    '$rootScope',
    'promiseTracker',
    function ($scope, $state, $rootScope, promiseTracker) {
        'use strict';
        var tracker = promiseTracker('tracker'),
            currentPromise;

        function resolvePromise() {
            if (currentPromise) {
                currentPromise.resolve();
                currentPromise = null;
            }
        }

        $rootScope.dateOptions = {
            'year-format': "'yyyy'",
            'starting-day': 1
        };

        $rootScope.textAngularOpts = {
            toolbar: [['p','h1', 'h2', 'h3', 'bold', 'italics', 'ul', 'ol'], ['insertLink']],
            classes: {
                toolbar: "btn-toolbar",
                toolbarGroup: "btn-group",
                toolbarButton: "btn btn-default",
                toolbarButtonActive: "active",
                textEditor: 'form-control',
                htmlEditor: 'form-control'
            }
        };

        $rootScope.$on('$stateChangeStart', function () {
            if (!currentPromise) {
                currentPromise = tracker.createPromise();
            }
        });

        $rootScope.$on('$stateChangeError', function () {
            resolvePromise();
        });

        $rootScope.$on('$stateNotFound', function () {
            resolvePromise();
        });

        $rootScope.$on('$stateChangeSuccess', function () {
            $scope.isLoggedIn = !$state.is('login');
            $scope.isResettingPassword = $state.is('iforgot');
            resolvePromise();
        });
    }
]);