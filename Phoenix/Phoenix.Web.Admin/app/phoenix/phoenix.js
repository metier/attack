﻿angular.module('phoenix', [
    'phoenix.products',
    'phoenix.customers',
    'phoenix.users',
    'phoenix.orders',
    'phoenix.resources',
    'phoenix.reporting',
    'phoenix.examiner',
    'phoenix.config',
    'phoenix.shared',
    'ui.router',
    'ui.bootstrap',
    'textAngular',
    'ngCookies',
    'ngAnimate',
    'ajoslin.promise-tracker',
    'cgBusy'
]).config([
    '$stateProvider',
    '$urlRouterProvider',
    'phoenix.config',
    '$httpProvider',
    '$uiViewScrollProvider',
    function ($stateProvider, $urlRouterProvider, config, $httpProvider, $uiViewScrollProvider) {
        'use strict';
        $stateProvider.state('login', {
            url: '/login?returnUrl',
            templateUrl: config.basePath + 'app/phoenix/views/login.html',
            controller: 'LoginCtrl'
        }).state('iforgot', {
            url: '/iforgot?token',
            templateUrl: config.basePath + 'app/phoenix/views/iforgot.html',
            controller: 'ResetPasswordCtrl'
        }).state('home', {
            url: '/home',
            templateUrl: config.basePath + 'app/phoenix/views/home.html',
            controller: 'HomeCtrl'
        }).state('notfound', {
            url: '/404',
            templateUrl: config.basePath + 'app/phoenix/views/404.html'
        });

        $uiViewScrollProvider.useAnchorScroll();
        $urlRouterProvider.otherwise(config.defaultUrl || '/home');
        $httpProvider.interceptors.push('errorHandlingHttpInterceptor');
    }
]);