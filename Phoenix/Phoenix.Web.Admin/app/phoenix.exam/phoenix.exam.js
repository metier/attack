﻿angular.module('phoenix.exam', [
    'phoenix.config',
    'phoenix.shared',
    'phoenix.products',
    'phoenix.customers',
    'ui.bootstrap',
    'ui.utils',
    'placeholderShim',
    'ui.router',
    'ngCookies'
]).config([
    'phoenix.config',
    '$stateProvider',
    function(config, $stateProvider) {
        'use strict';
        $stateProvider.state('exam', {
            url: '/exam',
            templateUrl: config.basePath + 'app/phoenix.exam/views/exam-home.html',
            controller: 'ExamHomeCtrl'
        }).state('exam.questions', {
            url: '/questions',
            abstract: true,
            template: '<ui-view />',
            resolve: {
                products: ['ProductsService', function (productsService) {
                    return productsService.getAll().then(function (products) {
                        return _(products).map(function (p) {
                            return { text: p.ProductPath + ' ' + p.Title, value: p.Id, labelText: p.ProductPath };
                        });
                    });
                }],
                languages: ['LanguagesService', function(languagesService) {
                    return languagesService.getAll().then(function(languages) {
                        return languages;
                    });
                }]
            }
        }).state('exam.questions.list', {
            url: '/?query&page&limit&orderBy&orderDirection&tagId&productId&rcoId&difficulty&status&languageId&customerSpecificFilterValue&customerId&lastUsedFromDate&lastUsedToDate',
            controller: 'QuestionListCtrl',
            templateUrl: config.basePath + 'app/phoenix.exam/views/questions/question-list.html',
            resolve: {
                questions: ['QuestionsService', '$stateParams', function (questionsService, $stateParams) {
                    if ($stateParams.status === null) {
                        $stateParams.status = "1,2";
                    }
                    var options = {
                        query: $stateParams.query,
                        page: parseInt($stateParams.page, 10) || 1,
                        limit: parseInt($stateParams.limit, 10) || 15,
                        orderBy: $stateParams.orderBy,
                        orderDirection: $stateParams.orderDirection,
                        lastUsedFromDate: $stateParams.lastUsedFromDate,
                        lastUsedToDate: $stateParams.lastUsedToDate,
                        tagId: $stateParams.tagId ? $stateParams.tagId.split(',') : null,
                        productId: $stateParams.productId ? $stateParams.productId.split(',') : null,
                        rcoId: $stateParams.rcoId ? $stateParams.rcoId.split(',') : null,
                        difficulty: $stateParams.difficulty ? $stateParams.difficulty.split(',') : null,
                        status: $stateParams.status ? $stateParams.status.split(',') : null,
                        languageId: $stateParams.languageId ? $stateParams.languageId.split(',') : null,
                        customerSpecificFilterValue: $stateParams.customerSpecificFilterValue ? $stateParams.customerSpecificFilterValue : 0,
                        customerId: $stateParams.customerId ? $stateParams.customerId.split(',') : null
                    };
                    
                    return questionsService.search(options);
                }],
                selectedTags: ['TagsService', '$stateParams', function (tagsService, $stateParams) {
                    if (!$stateParams.tagId) {
                        return [];
                    }
                    return tagsService.search({
                        tagIds: $stateParams.tagId.split(','),
                        limit: 100
                    }).then(function(result) {
                        return result.Items || [];
                    }, function () {
                        return [];
                    });
                }],
                selectedCustomers: ['CustomersService', '$stateParams', function (customersService, $stateParams) {
                    if (!$stateParams.customerId) {
                        return [];
                    }
                    return customersService.search({
                        customerIds: $stateParams.customerId.split(','),
                        limit: 100,
                        searchAllDistributors: true
                    }).then(function (result) {
                        return result.Items || [];
                    }, function () {
                        return [];
                    });
                }],
                selectedVersions: ['RcosService', '$stateParams', function (rcosService, $stateParams) {
                    if (!$stateParams.rcoId) {
                        return [];
                    }
                    return rcosService.search({
                        rcoIds: $stateParams.rcoId.split(','),
                        limit: 100
                    }).then(function(result) {
                        return result.Items || [];
                    }, function() {
                        return [];
                    });
                }],
                selectedFilter: ['questionCustomerSpecificFilters', '$stateParams', function (questionCustomerSpecificFilters, $stateParams) {
                    var selected = _.find(questionCustomerSpecificFilters, function (filter) {
                        return filter.value === $stateParams.customerSpecificFilterValue;
                    });

                    return selected;

                }]
            }
        }).state('exam.questions.create', {
            url: '/create',
            controller: 'QuestionCreateCtrl',
            templateUrl: config.basePath + 'app/phoenix.exam/views/questions/question-create.html',
            resolve: {
                question: ['questionTypes', function (questionTypes) {
                    return {
                        Points: 1,
                        Type: questionTypes[1].value,
                        CorrectBoolAnswer: true,
                        IsOptionsRandomized: false,
                        QuestionOptions: [{ IsCorrect: false, Text: '' }]
                    };
                }],
                earlierVersions: function () { return null; },
                newestVersion: function () { return null; }
            }
        }).state('exam.questions.import', {
            url: '/import',
            controller: 'QuestionsImportCtrl',
            templateUrl: config.basePath + 'app/phoenix.exam/views/questions/questions-import.html'
        }).state('exam.questions.edit', {
            url: '/:questionId',
            controller: 'QuestionCreateCtrl',
            templateUrl: config.basePath + 'app/phoenix.exam/views/questions/question-create.html',
            resolve: {
                question: ['QuestionsService', 'UsersService', '$stateParams', '$q', function (questionsService, usersService, $stateParams, $q) {
                    return questionsService.get($stateParams.questionId).then(function (result) {
                        var promises = [
                            usersService.getUserAccountByUserId(result.CreatedById)
                        ];
                        if (result.ModifiedById) {
                            promises.push(usersService.getUserAccountByUserId(result.ModifiedById));
                        }
                        return $q.all(promises).then(function(results) {
                            result.CreatedBy = results[0];
                            result.ModifiedBy = result.ModifiedById ? results[1] : null;
                            return result;
                        });
                    });
                }],
                earlierVersions: ['QuestionsService', '$stateParams', function (questionsService, $stateParams) {
                    return questionsService.getEarlierVersions($stateParams.questionId);
                }],
                newestVersion: ['QuestionsService', '$stateParams', function (questionsService, $stateParams) {
                    return questionsService.getNewestVersion($stateParams.questionId).then(function (newest) {
                        if (newest === 'null') {
                            return null;
                        }
                        return newest;
                    });
                }]
            }
        });
    }
]);