﻿angular.module('phoenix.exam').controller('QuestionCreateCtrl', [
    'phoenix.config',
    '$scope',
    'QuestionsService',
    'products',
    'languages',
    '$state',
    'question',
    'earlierVersions',
    'newestVersion',
    'NotificationService',
    function (config, $scope, questionsService, products, languages, $state, question, earlierVersions, newestVersion, notificationService) {
        'use strict';
        var backState = null;

        $scope.isEditMode = $state.current.name === 'exam.questions.edit';
        
        $scope.products = products;
        $scope.languages = languages;
        $scope.question = question;
        $scope.earlierVersions = earlierVersions;
        $scope.newestVersion = newestVersion;
        
        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            if (fromState.name === 'exam.questions.list') {
                backState = { name: 'exam.questions.list', params: fromParams };
            }
        });

        function navigateBack() {
            if (backState) {
                $state.go(backState.name, backState.params);
            } else {
                $state.go('exam.questions.list');
            }
        }
        
        function createNewQuestion(nextState, reloadState) {
            questionsService.create($scope.question).then(function () {
                if (reloadState) {
                    $state.go(nextState, null, { reload: true });
                } else {
                    $state.go(nextState);
                }
            }).finally(function () {
                $scope.isCreating = false;
                $scope.isCreatingAddNew = false;
            });
        }
        
        $scope.create = function () {
            $scope.isCreating = true;
            createNewQuestion('exam.questions.list');
        };

        $scope.createAndAddNew = function () {
            $scope.isCreatingAddNew = true;
            createNewQuestion('exam.questions.create', true);
        };

        $scope.update = function () {
            $scope.isSaving = true;
            questionsService.update($scope.question).then(function (updated) {
                if ($scope.question.Id !== updated.Id) {
                    notificationService.alertSuccess({ message: 'New version created!' });
                } else {
                    notificationService.alertSuccess({ message: 'Question saved!' });
                }
                navigateBack();
            }).finally(function() {
                $scope.isSaving = false;
            });
        };

        $scope.cancel = function () {
            navigateBack();
        };
    }
]);