﻿angular.module('phoenix.exam').controller('QuestionListCtrl', [
    '$scope',
    '$stateParams',
    '$state',
    '$q',
    '$window',
    '$modal',
    'questions',
    'products',
    'languages',
    'selectedTags',
    'selectedVersions',
    'selectedCustomers',
    'difficultyLevels',
    'questionStatuses',
    'questionCustomerSpecificFilters',
    'selectedFilter',
    'QueryParameters',
    'phoenix.config',
    'QuestionsService',
    'NotificationService',
function ($scope, $stateParams, $state, $q, $window, $modal, questions, products, languages, selectedTags, selectedVersions, selectedCustomers, difficultyLevels, questionStatuses, questionCustomerSpecificFilters, selectedFilter, queryParameters, config, questionsService, notificationService) {
        'use strict';
        var defaultLimit = 15, defaultPage = 1;
        $scope.isLoading = true;

        $scope.$on('$stateChangeStart', function () {
            $scope.isLoading = true;
        });

        $scope.$on('$stateChangeSuccess', function () {
            $scope.isInitializing = true;
            $scope.isLoading = false;

            $scope.limit = $stateParams.limit || defaultLimit;
            $scope.page = $stateParams.page || defaultPage;
            $scope.query = $stateParams.query;
            $scope.questions = questions;
            $scope.languages = _(languages).map(function (l) {
                return { text: l.Name, value: l.Id, labelText: l.Name };
            });

            $scope.products = products;
            $scope.difficulties = difficultyLevels;
            $scope.questionStatuses = questionStatuses;
            $scope.customerSpecificFilter = questionCustomerSpecificFilters;

            if (moment($stateParams.lastUsedFromDate).isValid()) {
                $scope.lastUsedFromDate = moment($stateParams.lastUsedFromDate).toDate();
            }

            if (moment($stateParams.lastUsedToDate).isValid()) {
                $scope.lastUsedToDate = moment($stateParams.lastUsedToDate).toDate();
            }
            
            $scope.selectedProducts = setSelectedItemsFromParams($stateParams.productId, $scope.products);
            $scope.selectedLanguages = setSelectedItemsFromParams($stateParams.languageId, $scope.languages);
            $scope.selectedVersions = selectedVersions;
            $scope.selectedDifficulties = setSelectedItemsFromParams($stateParams.difficulty, $scope.difficulties);
            $scope.selectedStatuses = setSelectedItemsFromParams($stateParams.status, $scope.questionStatuses);
            $scope.selectedTags = selectedTags;
            $scope.selectedFilter = selectedFilter;
            $scope.selectedCustomers = selectedCustomers;

            $scope.isInitializing = false;
        });

        function splitParams(param) {
            return param ? param.split(','): [];
        }

        function setSelectedItemsFromParams(params, listOfItems) {
            _(listOfItems).each(function(i) {
                i.isSelected = false;
            });
            return _.chain(splitParams(params))
                .map(function(paramValue) {
                    var p = _(listOfItems).find(function(item) { return item.value === parseInt(paramValue, 10); });
                    if (p) { p.isSelected = true; }
                    return p;
                }).filter(function(item) {
                    return !!item;
                }).value();
        }

        function getSelectedFilter(filters) {
            if (filters) {

                var selectedCustomerFilter = _.find(filters, function (filter) {
                    return filter.isSelected === true;
                });

                if (!selectedCustomerFilter) {
                    filters[0].isSelected = true;
                    selectedCustomerFilter = filters[0];
                }

                return selectedCustomerFilter;
            }
            return null;
        }
        
        function resolveStateParams() {
            var queryOptions = {
                query: $scope.query
            };
            if ((!$stateParams.limit && $scope.limit !== defaultLimit) || ($stateParams.limit && $scope.limit !== $stateParams.limit)) {
                queryOptions.limit = $scope.limit;
            }
            if ((!$stateParams.page && $scope.page !== defaultPage) || ($stateParams.page && $scope.page !== $stateParams.page)) {
                queryOptions.page = $scope.page;
            }

            if ($scope.lastUsedFromDate) {
                queryOptions.lastUsedFromDate = moment($scope.lastUsedFromDate).toISOString();
            } else {
                queryOptions.lastUsedFromDate = null;
            }

            if ($scope.lastUsedToDate) {
                queryOptions.lastUsedToDate = moment($scope.lastUsedToDate).toISOString();
            } else {
                queryOptions.lastUsedToDate = null;
            }

            if ($scope.selectedProducts.length > 0 || splitParams($stateParams.productId).length > 0) {
                queryOptions.productId = _($scope.selectedProducts).pluck('value');
            }

            if ($scope.selectedLanguages.length > 0 || splitParams($stateParams.languageId).length > 0) {
                queryOptions.languageId = _($scope.selectedLanguages).pluck('value');
            }

            if ($scope.selectedDifficulties.length > 0 || splitParams($stateParams.difficulty).length > 0) {
                queryOptions.difficulty = _($scope.selectedDifficulties).pluck('value');
            }

            if ($scope.selectedStatuses.length > 0 || splitParams($stateParams.status).length > 0) {
                queryOptions.status = _($scope.selectedStatuses).pluck('value');
            }

            if ($scope.selectedVersions.length > 0 || splitParams($stateParams.rcoId).length > 0) {
                queryOptions.rcoId = _($scope.selectedVersions).pluck('Id');
            }

            if ($scope.selectedTags.length > 0 || splitParams($stateParams.tagId).length > 0) {
                queryOptions.tagId = _($scope.selectedTags).pluck('Id');
            }

            var selectedCustomerFilter = getSelectedFilter($scope.customerSpecificFilter);
            if (selectedCustomerFilter) {
                queryOptions.customerSpecificFilterValue = selectedCustomerFilter.value;
            }

            if ($scope.selectedCustomers.length > 0 || splitParams($stateParams.customerId).length > 0) {
                queryOptions.customerId = _($scope.selectedCustomers).pluck('Id');
            }
            return queryOptions;
        }

    $scope.navigate = function (page) {
            $scope.page = page;
            var queryOptions = resolveStateParams();
            $state.go('exam.questions.list', queryOptions);
        };

        $scope.exportQuestions = function() {
            var queryOptions = resolveStateParams();
            var queryParams = queryParameters.parse(queryOptions);
            var url = config.apiPath + 'reports/questions/xlsx' + queryParams;
            $window.location.href = url;
        };

        $scope.searchQuestions = function() {
            var queryOptions = resolveStateParams();
            $state.go('exam.questions.list', queryOptions);
        };

        $scope.refetchQuestions = function () {
            var queryOptions = resolveStateParams();
            return $state.go('exam.questions.list', queryOptions, { reload: true });
        };

        $scope.getLanguageText = function(languageId) {
            var language = _($scope.languages).find(function (l) {
                return l.value === languageId;
            });
            return language ? language.text : '';
        };

        $scope.onQueryChanged = function() {
            if ($scope.query === '') {
                $scope.searchQuestions();
            }
        };



        $scope.bulkEditQuestions = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: config.basePath + 'app/phoenix.exam/views/modals/questions-bulk-edit-modal.html',
                controller: 'QuestionsBulkEditModalCtrl',
                resolve: {
                        questions: function() { return $scope.questions; },
                        difficulties: function () { return $scope.difficulties; },
                        statusLevels: function () { return $scope.questionStatuses; },
                        languages: function () { return languages; },
                        products: function () { return $scope.products; },
                        versions: function() { return $scope.selectedVersions; }
                },
                windowClass: 'modal-wide'
            });
            modalInstance.result.then(function (values) {
                $scope.isSavingQuestions = true;

                values.questionIds = _(questions.Items).map(function (q) {
                    return q.Id;
                });

                questionsService.batchUpdate(values).finally(function () {
                    $scope.refetchQuestions().finally(function() {
                        $scope.isSavingQuestions = false;
                        notificationService.alertSuccess({ message: ('Questions saved!') });
                    });
               });
            });
        };


        $scope.$watch('selectedProducts', function () {
            if (!$scope.isInitializing) {
                $scope.searchQuestions();
            }
        });

        $scope.$watch('selectedLanguages', function () {
            if (!$scope.isInitializing) {
                $scope.searchQuestions();
            }
        });

        $scope.$watch('selectedDifficulties', function () {
            if (!$scope.isInitializing) {
                $scope.searchQuestions();
            }
        });

        $scope.$watch('selectedStatuses', function () {
            if (!$scope.isInitializing) {
                $scope.searchQuestions();
            }
        });

        $scope.$watch('selectedVersions', function () {
            if (!$scope.isInitializing) {
                $scope.searchQuestions();
            }
        }, true);

        $scope.$watch('selectedTags', function () {
            if (!$scope.isInitializing) {
                $scope.searchQuestions();
            }
        }, true);

        $scope.$watch('customerSpecificFilter', function () {
            if (!$scope.isInitializing) {
                $scope.searchQuestions();
            }
        }, true);

        $scope.$watch('selectedCustomers', function () {
            if (!$scope.isInitializing) {
                $scope.searchQuestions();
            }
        }, true);
    }
]);