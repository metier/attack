﻿angular.module('phoenix.exam').controller('QuestionsImportCtrl', [
    'phoenix.config',
    '$scope',
    'QuestionsService',
    'QuestionsImporter',
    'products',
    'languages',
    '$state',
    function (config, $scope, questionsService, questionsImporter, products, languages, $state) {
        'use strict';
        var backState = null;
        
        $scope.model = {
            isParsing: false,
            parseErrorMsg: null
        };

        $scope.fileSelectModel = {
            selectedFile: null
        };
        $scope.products = products;
        $scope.languages = languages;
        $scope.isParsing = false;

        function navigateBack() {
            if (backState) {
                $state.go(backState.name, backState.params, { location: 'replace' });
            } else {
                $state.go('exam.questions.list');
            }
        }

        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            if (fromState.name === 'exam.questions.list') {
                backState = { name: 'exam.questions.list', params: fromParams };
            }
        });
        
        $scope.$watch('fileSelectModel.selectedFile', function () {
            $scope.model.parseErrorMsg = null;

            if ($scope.fileSelectModel.selectedFile && $scope.fileSelectModel.selectedFile.questions) {
                $scope.model.isParsing = true;
                questionsImporter.parseQuestionsFromExcel($scope.fileSelectModel.selectedFile.questions).then(function(questions) {
                    $scope.questions = questions;
                },function(error) {
                    $scope.model.parseErrorMsg = error;
                }).finally(function () {
                    $scope.model.isParsing = false;
                });
            } else {
                $scope.questions = [];
            }
        });

        $scope.import = function () {
            $scope.isImporting = true;
            $scope.model.showImportSuccessMsg = false;
            $scope.model.showImportErrorMsg = false;

            questionsService.batchCreate($scope.questions).then(function () {
                $scope.questions = [];
                $scope.model.showImportSuccessMsg = true;
            }, function() {
                $scope.model.showImportErrorMsg = true;
            }).finally(function() {
                $scope.isImporting = false;
            });
        };

        $scope.cancel = function () {
            navigateBack();
        };
    }
]);