﻿angular.module('phoenix.exam').controller('ExamHomeCtrl', ['$scope', '$state', function ($scope, $state) {
    'use strict';
    $scope.isActive = function (state, absolute) {
        if (absolute) {
            return $state.current.name === state;
        }
        return $state.current.name.indexOf(state) === 0;
    };
}]);