﻿angular.module('phoenix.exam').controller('QuestionsBulkEditModalCtrl', [
    '$scope',
    '$modalInstance',
    'questions',
    'difficulties',
    'statusLevels',
    'languages',
    'products',
    'CustomersService',
    'phoenix.config',
    function ($scope, $modalInstance, questions, difficulties, statusLevels, languages, products, customerService, config) {
        'use strict';


        function clearSelectedOnArray(array) {
            _(array).each(function (a) {
                    a.isSelected = false;
                }
            );
            return array;
        }

        $scope.customerSearchTemplateUrl = config.basePath + 'app/phoenix.exam/views/templates/customer-search-result-template.html';
        $scope.searchCustomer = function (query) {
            return customerService.search({ query: query, limit: 20, searchAllDistributors: true }).then(function (result) {
                return result.Items;
            });
        };

        $scope.onCustomerSelected = function (customer) {
            $scope.values.customerId = customer.Id;
            $scope.userSelectedCustomer = customer;
        };

        $scope.setValidSelectedCustomer = function () {
            if ($scope.selectedCustomer) {
                $scope.selectedCustomer = $scope.userSelectedCustomer;
            } else {
                //$scope.values.customerId = null;
                $scope.selectedCustomer = null;
                $scope.userSelectedCustomer = null;
            }
        };

        $scope.values = {};

        $scope.questionCount = questions.TotalCount;
        $scope.difficulties = difficulties;
        $scope.statusLevels = statusLevels;
        $scope.languages = languages;
        $scope.productsToAdd = clearSelectedOnArray(angular.copy(products));
        $scope.productsToRemove = clearSelectedOnArray(angular.copy(products));
        $scope.selectedProductsToAdd = [];
        $scope.selectedProductsToRemove = [];
        $scope.selectedVersionsToAdd = [];
        $scope.selectedVersionsToRemove = [];
        $scope.selectedTagsToAdd = [];
        $scope.selectedTagsToRemove = [];

        $scope.values.isChangeCustomerTailoredSetting = false;

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.save = function () {

            $scope.values.productIdsToAdd = _($scope.values.selectedProductsToAdd).map(function (p) {
                return p.value;
            });

            $scope.values.productIdsToRemove = _($scope.values.selectedProductsToRemove).map(function (p) {
                return p.value;
            });

            $scope.values.versionIdsToAdd = _($scope.selectedVersionsToAdd).map(function (v) {
                return v.Id;
            });

            $scope.values.versionIdsToRemove = _($scope.selectedVersionsToRemove).map(function (v) {
                return v.Id;
            });

            $scope.values.tagIdsToAdd = _($scope.selectedTagsToAdd).map(function (t) {
                return t.Id;
            });

            $scope.values.tagIdsToRemove = _($scope.selectedTagsToRemove).map(function (t) {
                return t.Id;
            });

            $modalInstance.close($scope.values);
        };

}]);
