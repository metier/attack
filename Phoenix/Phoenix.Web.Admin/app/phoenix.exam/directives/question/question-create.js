﻿angular.module('phoenix.exam').directive('questionCreate', [
    'phoenix.config',
    function (config) {
        "use strict";
        return {
            restrict: 'E',
            scope: {
                isEditMode: '=',
                question: '=',
                inLanguages: '=languages',
                inProducts: '=products'
            },
            templateUrl: config.basePath + 'app/phoenix.exam/directives/question/question-create.html',
            controller: [
                '$scope',
                'difficultyLevels',
                'questionStatuses',
                'questionTypes',
                'CustomersService',
                'Guid',
                function ($scope, difficultyLevels, questionStatuses, questionTypes, customerService, guid) {
                    function configureProductFilter() {
                        _($scope.products).each(function (item) {
                            item.isSelected = false;
                        });
                        $scope.selectedProducts = _($scope.question.Products).map(function (p) {
                            return { text: p.ProductPath + ' ' + p.Title, value: p.Id, labelText: p.ProductPath };
                        }) || [];

                        _($scope.selectedProducts).each(function (selected) {
                            var listProduct = _($scope.products).find(function (product) {
                                return product.value === selected.value;
                            });
                            if (listProduct) {
                                listProduct.isSelected = true;
                            }
                        });
                    }

                    $scope.$watch('model.selectedSingleOptionIndex', function () {
                        if ($scope.model.selectedSingleOptionIndex > -1) {
                            _($scope.question.QuestionOptions).each(function (option, index) {
                                option.IsCorrect = $scope.model.selectedSingleOptionIndex === index;
                            });
                        }
                    });

                    $scope.questionTypeSelected = function () {
                        $scope.model.selectedSingleOptionIndex = -1;
                        _($scope.question.QuestionOptions).each(function (option) {
                            option.IsCorrect = false;
                        });
                    };

                    $scope.$watch('selectedProducts', function () {
                        $scope.question.Products = _($scope.selectedProducts).map(function (p) {
                            return { Id: p.value };
                        });
                    });

                    $scope.$watch('selectedVersions', function () {
                        $scope.question.Rcos = $scope.selectedVersions;
                    }, true);

                    $scope.$watch('selectedTags', function () {
                        $scope.question.Tags = $scope.selectedTags;
                    }, true);

                    $scope.searchCustomer = function (query) {
                        return customerService.search({ query: query, limit: 20, searchAllDistributors: true }).then(function (result) {
                            return result.Items;
                        });
                    };

                    $scope.onCustomerSelected = function (customer) {
                        $scope.question.CustomerId = customer.Id;
                        $scope.userSelectedCustomer = customer;
                    };

                    $scope.setValidSelectedCustomer = function () {
                        if ($scope.selectedCustomer) {
                            $scope.selectedCustomer = $scope.userSelectedCustomer;
                        } else {
                            $scope.question.CustomerId = null;
                            $scope.selectedCustomer = null;
                            $scope.userSelectedCustomer = null;
                        }
                    };

                    $scope.addQuestionOption = function () {
                        $scope.question.QuestionOptions.push({ IsCorrect: false, Text: '' });
                    };

                    $scope.removeQuestionOption = function (option) {
                        var index = $scope.question.QuestionOptions.indexOf(option);
                        if (index > -1) {
                            $scope.question.QuestionOptions.splice(index, 1);
                        }
                    };

                    function init() {
                        $scope.model = {
                            selectedSingleOptionIndex: -1
                        };
                        $scope.difficultyLevels = difficultyLevels;
                        $scope.questionStatuses = questionStatuses;
                        $scope.questionTypes = questionTypes;

                        // Make copies of arrays so we can mess around with them without affecting others
                        $scope.products = angular.copy($scope.inProducts);
                        $scope.languages = angular.copy($scope.inLanguages);

                        // Generate unique name for radio button group
                        $scope.singleOptionRadioButtonName = guid.newGuid();

                        $scope.customerSearchTemplateUrl = config.basePath + 'app/phoenix.exam/views/templates/customer-search-result-template.html';
                        
                        if ($scope.question.Type === 'S') {
                            var correctAnswerOption = _($scope.question.QuestionOptions).find(function (option) {
                                return option.IsCorrect === true;
                            });
                            $scope.model.selectedSingleOptionIndex = $scope.question.QuestionOptions.indexOf(correctAnswerOption);
                        }
                                                
                        if ($scope.question.CustomerId) {
                            customerService.get($scope.question.CustomerId).then(function (result) {
                                $scope.selectedCustomer = result;
                            });
                        }

                        configureProductFilter();
                        $scope.selectedVersions = $scope.question.Rcos || [];
                        $scope.selectedTags = $scope.question.Tags || [];
                    }

                    init();
                }
            ]
        };
    }
]);