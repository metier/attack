﻿angular.module('phoenix.exam').directive('examVersionFilter', [
    'phoenix.config',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                selectedItems: '=',
                label: '@'
            },
            templateUrl: config.basePath + 'app/phoenix.exam/directives/exam-version-filter/exam-version-filter.html',
            controller: [
                '$scope',
                'RcosService',
                function ($scope, rcosService) {
                    $scope.searchVersion = function(query) {
                        return rcosService.getCourses({ query: query, limit: 20, orderBy: 'version', orderDirection: 'asc' }).then(function (result) {
                            return _(result.Items).filter(function (item) {
                                return !!item.Version;
                            });
                        });
                    };

                    $scope.onVersionSelected = function(rco) {
                        if (rco) {
                            var isAddedToFilter = _($scope.selectedItems).some(function (r) {
                                return r.Id === rco.Id;
                            });
                            if (!isAddedToFilter) {
                                $scope.selectedItems.push(rco);
                            }
                        }
                        $scope.version = null;
                    };

                    $scope.remove = function(rco) {
                        var index = $scope.selectedItems.indexOf(rco);
                        if (index > -1) {
                            $scope.selectedItems.splice(index, 1);
                        }
                    };
                }]
        };
    }
]);