﻿angular.module('phoenix.exam').directive('examCustomersFilter', [
    'phoenix.config',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                selectedCustomers: '=',
                filterOptions: '=',
                selectedFilter: '=',
                label: '@'
            },
            templateUrl: config.basePath + 'app/phoenix.exam/directives/exam-customers-filter/exam-customers-filter.html',
            controller: [
                '$scope',
                'CustomersService',
                function ($scope, customersService) {
                    $scope.customerSearchTemplateUrl = config.basePath + 'app/phoenix.exam/views/templates/customer-search-result-template.html';

                    $scope.isCustomerControlDisabled = function () {
                        if($scope.selectedFilter){
                            return $scope.selectedFilter.value !== '2';
                        } else {
                            return true;
                        }
                    }

                    function addCustomerToSelection(customer) {
                        if (customer) {
                            var isAddedToFilter = _($scope.selectedCustomers).some(function (c) {
                                return c.Id === customer.Id;
                            });
                            if (!isAddedToFilter) {
                                $scope.selectedCustomers.push(customer);
                            }
                        }
                    }

                    $scope.searchCustomers = function (query) {
                        $scope.currentSearchTerm = query;
                        return customersService.search({ query: query, limit: 20, searchAllDistributors: true }).then(function (result) {
                            return result.Items;
                        });
                    };

                    $scope.onCustomerSelected = function (customer) {
                        addCustomerToSelection(customer);
                        $scope.customer = null;
                    };

                    $scope.remove = function (customer) {
                        var index = $scope.selectedCustomers.indexOf(customer);
                        if (index > -1) {
                            $scope.selectedCustomers.splice(index, 1);
                        }
                    };
                }]
        };
    }
]);