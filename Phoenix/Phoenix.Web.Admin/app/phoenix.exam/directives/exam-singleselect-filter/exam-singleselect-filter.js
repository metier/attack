angular.module('phoenix.exam').directive('examSingleselectFilter', [
    '$document',
    'phoenix.config',
    function ($document, config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                selectedItem: '=',
                options: '=',
                label: '@',
                labelType: '@'
            },
            templateUrl: config.basePath + 'app/phoenix.exam/directives/exam-singleselect-filter/exam-singleselect-filter.html',
            link: function (scope, element) {
                // Make sure dropdown is hidden when user clicks somewhere else
                $document.bind('click', function (event) {
                    var isClickedElementChildOfDropdown = element.find(event.target).length > 0;
                    if (isClickedElementChildOfDropdown) {
                        scope.isOpen = false;
                        return;
                    }
                    scope.isOpen = false;
                    scope.$apply();
                });
            },
            controller: [
                '$scope',
                function ($scope) {

                    function updateSelection(option) {
                        _.each($scope.options, function(item) {
                            item.isSelected = false;
                        });

                        option.isSelected = true;
                        $scope.selectedItem = option;
                    }

                    $scope.dropdownItemClicked = function (option) {
                        updateSelection(option);
                    };

//                    updateSelection($scope.selectedItem);
                }]
        };
    }
]);