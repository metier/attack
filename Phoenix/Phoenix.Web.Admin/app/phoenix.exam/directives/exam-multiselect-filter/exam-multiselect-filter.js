﻿angular.module('phoenix.exam').directive('examMultiselectFilter', [
    '$document',
    'phoenix.config',
    function ($document, config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                selectedItems: '=',
                options: '=',
                placeholder: '@',
                label: '@',
                labelType: '@'
            },
            templateUrl: config.basePath + 'app/phoenix.exam/directives/exam-multiselect-filter/exam-multiselect-filter.html',
            link: function (scope, element) {
                // Make sure dropdown is hidden when user clicks somewhere else
                $document.bind('click', function (event) {
                    var isClickedElementChildOfDropdown = element.find(event.target).length > 0;
                    if (isClickedElementChildOfDropdown) {
                        return;
                    }
                    scope.isOpen = false;
                    scope.$apply();
                });
            },
            controller: [
                '$scope',
                function ($scope) {
                    function updateSelection(option) {
                        option.isSelected = !option.isSelected;
                        $scope.selectedItems = _($scope.options).filter(function (item) {
                            return item.isSelected;
                        });
                    }

                    $scope.dropdownItemClicked = function (option) {
                        updateSelection(option);
                    };

                    $scope.remove = function(option) {
                        updateSelection(option);
                    };

                    $scope.getLabelType = function() {
                        if ($scope.labelType === 'success') {
                            return "label-success";
                        }
                        return 'label-default';
                    };
                }]
        };
    }
]);