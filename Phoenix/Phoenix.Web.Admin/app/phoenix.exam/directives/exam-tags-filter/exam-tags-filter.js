﻿angular.module('phoenix.exam').directive('examTagsFilter', [
    'phoenix.config',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                selectedItems: '=',
                canCreate: '@',
                label: '@'
            },
            templateUrl: config.basePath + 'app/phoenix.exam/directives/exam-tags-filter/exam-tags-filter.html',
            controller: [
                '$scope',
                'TagsService',
                function ($scope, tagsService) {
                    $scope.isCreateEnabled = $scope.canCreate === 'true';

                    function addTagToSelection(tag) {
                        if (tag) {
                            var isAddedToFilter = _($scope.selectedItems).some(function (t) {
                                return t.Id === tag.Id;
                            });
                            if (!isAddedToFilter) {
                                $scope.selectedItems.push(tag);
                            }
                        }
                    }

                    $scope.searchTags = function (query) {
                        $scope.currentSearchTerm = query;
                        return tagsService.search({ query: query, limit: 20 }).then(function (result) {
                            $scope.isEmptySearchResult = !result.Items || result.Items.length === 0;
                            return result.Items;
                        });
                    };

                    $scope.onTagSelected = function (tag) {
                        addTagToSelection(tag);
                        $scope.tag = null;
                    };

                    $scope.createNewFromQuery = function () {
                        if ($scope.isEmptySearchResult && $scope.currentSearchTerm && $scope.isCreateEnabled) {
                            tagsService.create({ Text: $scope.currentSearchTerm }).then(function(result) {
                                addTagToSelection(result);
                                $scope.currentSearchTerm = null;
                            });
                        }
                    };

                    $scope.remove = function(tag) {
                        var index = $scope.selectedItems.indexOf(tag);
                        if (index > -1) {
                            $scope.selectedItems.splice(index, 1);
                        }
                    };

                    $scope.$watch('tag', function() {
                        if (!$scope.currentSearchTerm) {
                            $scope.isEmptySearchResult = false;
                        }
                    });
                }]
        };
    }
]);