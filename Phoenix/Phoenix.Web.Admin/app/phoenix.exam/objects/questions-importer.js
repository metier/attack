﻿angular.module('phoenix.exam').factory('QuestionsImporter', [
    '$q',
    'LanguagesService',
    'ProductsService',
    'TagsService',
    'RcosService',
    function ($q, languagesService, productsService, tagsService, rcosService) {
        "use strict";
        var languages;
        var products;
        var tags = [];
        var rcos = [];
        var columns = {
            question: 'Question',
            type: 'Type (S, M, TF)',
            correctBool: 'Correct TF (T,F)',
            correctAlternatives: 'Correct Alternative(s)(1,2,3 or 4)',
            alternative: 'Alternative ', // With preceding number: 'Alternative 4'
            language: 'Language',
            products: 'Products (comma separated)',
            versions: 'Versions (comma separated)',
            difficulty: 'Level of difficulty (1,2 og 3)',
            tags: 'Tags (comma separated)',
            author: 'Author',
            customerId: 'Customer Id',
            status: 'Status (D, P, O)'
        };

        function splitComma(str) {
            str = str + "";
            return str.split(',');
        }
        function trim(str) { return str.trim(); }
        function notEmpty(str) {
            if (_.isNumber(str) && !_.isNaN(str)) {
                str = '' + str;
            }
            return _.isString(str) && str !== '';
        }
        function parseNumber(str) { return parseInt(str, 10); }
        function areEqualIgnoreCase(str1, str2) { return str1.toLowerCase() === str2.toLowerCase(); }

        function getStringArrayFromCommaSeparated(commaSeparated) {
            return _.chain(splitComma(commaSeparated)).map(trim).filter(notEmpty).value();
        }
        
        function getNumberArrayFromCommaSeparated(commaSeparated) {
            return _.chain(splitComma(commaSeparated)).map(trim).filter(notEmpty).map(parseNumber).value();
        }
        
        function getAlternativeNumber(str) {
            return parseInt(str.substring(columns.alternative.length, str.length), 10);
        }

        function isAlternativeColumn(str) {
            return str.indexOf(columns.alternative) === 0 && !_.isNaN(getAlternativeNumber(str));
        }

        function getAlternatives(row) {
            return _.chain(_.keys(row))
                .filter(isAlternativeColumn)
                .filter(function (colName) { return notEmpty(row[colName]);} )
                .map(function(colName) { return { key: colName, value: row[colName], number: getAlternativeNumber(colName) }; })
                .value();
        }
        
        function parseOptions(row) {
            var correctAlternatives = getNumberArrayFromCommaSeparated(row[columns.correctAlternatives]);
            var type = row[columns.type];
            if (type === 'S') {
                if (correctAlternatives.length !== 1) {
                    throw 'Single choice question must have exactly one correct alternative';
                }
            }
            if (type === 'M') {
                if (correctAlternatives.length <= 0) {
                    throw 'Multiple choice question must have at least one correct alternative';
                }
            }
            if (type === 'TF') {
                if (correctAlternatives.length > 0) {
                    throw 'True/False question cannot have any alternatives';
                }
            }
            var alternatives = getAlternatives(row);
            return _(alternatives).map(function (alt) {
                return {
                    Text: "" + alt.value,
                    IsCorrect: _(correctAlternatives).some(function (correctNumber) { return correctNumber === alt.number; })
                };
            });
        }

        function getLanguageId(excelString) {
            if (!excelString) {
                return null;
            }
            var foundLanguage = _(languages).find(function (lan) { return areEqualIgnoreCase(lan.Identifier, excelString); });
            if (!foundLanguage) {
                throw 'Could not find language ' + excelString;
            }
            return foundLanguage.Id;
        }

        function getProduct(productNumber) {
            var product = _(products).find(function(p) {
                return areEqualIgnoreCase(p.ProductPath, productNumber) && p.IsActive;
            });
            if (!product) {
                throw 'Could not find product ' + productNumber;
            }
            return product;
        }

        function getProducts(excelString) {
            return _(getStringArrayFromCommaSeparated(excelString)).map(getProduct);
        }

        function getDifficulty(excelString) {
            return parseInt(excelString, 10);
        }
        
        function getTags(excelString) {
            var tagPromises = _(getStringArrayFromCommaSeparated(excelString)).map(function(tag) {
                 return getTag(tag);
            });
            return $q.all(tagPromises);
        }

        function getTag(tag) {
            var deferred = $q.defer();
            var loadedTag = _(tags).find(function (item) { return areEqualIgnoreCase(item.Text, tag); });
            if (loadedTag) {
                deferred.resolve(loadedTag);
            } else {
                return tagsService.search({ query: tag, limit: 20 }).then(function (result) {
                    if (result.Items && result.Items.length > 0) {
                        var exactMatch = _(result.Items).find(function (item) { return areEqualIgnoreCase(item.Text, tag); });
                        if (exactMatch) {
                            tags.push(result.Items[0]);
                            return result.Items[0];
                        }
                    }
                    throw 'Could not find tag ' + tag;
                });
            }
            return deferred.promise;
        }
        
        function getRcos(excelString) {
            var rcoPromises = _(getStringArrayFromCommaSeparated(excelString)).map(function (rco) { return getRco(rco); });
            return $q.all(rcoPromises);
        }

        function getRco(version) {
            var deferred = $q.defer();
            var loadedRco = _(rcos).find(function (item) { return areEqualIgnoreCase(item.Version, version); });
            if (loadedRco) {
                deferred.resolve(loadedRco);
            } else {
                return rcosService.getCourses({ query: version, limit: 1000 }).then(function (result) {
                    if (result.Items && result.Items.length > 0) {
                        var exactMatch = _(result.Items).find(function (item) { return areEqualIgnoreCase(item.Version, version); });
                        if (exactMatch) {
                            rcos.push(exactMatch);
                            return exactMatch;
                        }
                    }
                    throw 'Could not find rco version ' + version;
                });
            }
            return deferred.promise;
        }
        
        function getCorrectBoolAnswer(row) {
            if (row[columns.type] === 'TF') {
                var excelString = row[columns.correctBool];
                if (excelString === 'T') {
                    return true;
                }
                if (excelString === 'F') {
                    return false;
                }
            }
            return null;
        }

        function getQuestionStatus(excelString) {
            if (excelString === 'D') {
                return 1;
            }
            if (excelString === 'P') {
                return 2;
            }
            if (excelString === 'O') {
                return 9;
            }
            return null;
        }

        function parseQuestionFromExcel(row) {
            var deferred = $q.defer();
            var question = {
                IsOptionsRandomized: true,
                Points: 1,
                Status: getQuestionStatus(row[columns.status]),
                Text: row[columns.question],
                Type: row[columns.type].toUpperCase(),
                CorrectBoolAnswer: getCorrectBoolAnswer(row),
                QuestionOptions: parseOptions(row),
                LanguageId: getLanguageId(row[columns.language]),
                Products: getProducts(row[columns.products] || ''),
                Difficulty: getDifficulty(row[columns.difficulty]),
                Author: row[columns.author],
                CustomerId: row[columns.customerId]
            };
            
            var promises = [];
            promises.push(getTags(row[columns.tags] || '').then(function (result) {
                question.Tags = result;
            }));

            promises.push(getRcos(row[columns.versions] || '').then(function (result) {
                
                question.Rcos = result;
            }));
            
            $q.all(promises).then(function() {
                deferred.resolve(question);
            }, deferred.reject);

            return deferred.promise;
        }

        function parseQuestionsFromExcel(excelQuestions) {
            // Parse all questions in sequence to allow for re-use of fetched (cached) values for tags and rcos
            var deferred = $q.defer();
            var parsed = [];
            _(excelQuestions).reduce(function (current, next) {
                return current.then(function () {
                    return parseQuestionFromExcel(next).then(function (question) {
                        parsed.push(question);
                    });
                });
            }, $q.when(1)).then(function () {
                deferred.resolve(parsed);
            }, deferred.reject);

            return deferred.promise;
        }

        function loadLanguages() {
            return languagesService.getAll().then(function (result) { languages = result; });
        }

        function loadProducts() {
            return productsService.getAllActive().then(function (result) { products = result; });
        }

        return {
            parseQuestionsFromExcel: function (excelQuestions) {
                var preLoads = [loadLanguages(), loadProducts()];
                return $q.all(preLoads).then(function () { return parseQuestionsFromExcel(excelQuestions); });
            }
        };
    }
]);