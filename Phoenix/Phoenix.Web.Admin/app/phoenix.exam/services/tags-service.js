﻿angular.module('phoenix.exam').factory('TagsService', [
    '$http',
    'phoenix.config',
    'asyncHelper',
    function ($http, config, asyncHelper) {
        'use strict';
        return {
            search: function (options) {
                var parameters = {
                    query: options.query,
                    tagIds: options.tagIds,
                    orderBy: options.orderBy,
                    orderDirection: options.orderDirection
                },
                url = config.apiPath + 'tags';

                if (typeof options.limit === 'number') {
                    parameters.limit = options.limit;
                }
                if (typeof options.page === 'number' && options.page > 0) {
                    parameters.skip = (options.page - 1) * parameters.limit;
                }

                return asyncHelper.resolveHttp($http.get(url, { params: parameters }), false);
            },
            create: function (tag) {
                var url = config.apiPath + 'tags';
                return asyncHelper.resolveHttp($http.post(url, tag), false);
            }
        };
    }]);