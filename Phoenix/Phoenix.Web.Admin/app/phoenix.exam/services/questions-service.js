﻿angular.module('phoenix.exam').factory('QuestionsService', [
    '$http',
    'phoenix.config',
    'asyncHelper',
    function ($http, config, asyncHelper) {
        'use strict';
        return {
            get: function (id) {
                var url = config.apiPath + 'questions/' + id;
                return asyncHelper.resolveHttp($http.get(url));
            },
            getEarlierVersions: function (id) {
                var url = config.apiPath + 'questions/' + id + '/earlierversions';
                return asyncHelper.resolveHttp($http.get(url));
            },
            getNewestVersion: function (id) {
                var url = config.apiPath + 'questions/' + id + '/newestversion';
                return asyncHelper.resolveHttp($http.get(url));
            },
            create: function (question) {
                var url = config.apiPath + 'questions/';
                return asyncHelper.resolveHttp($http.post(url, question));
            },
            batchCreate: function (questions) {
                var url = config.apiPath + 'questions/batch';
                return asyncHelper.resolveHttp($http.post(url, questions));
            },
            update: function (question) {
                var url = config.apiPath + 'questions/' + question.Id;
                return asyncHelper.resolveHttp($http.put(url, question));
            },
            batchUpdate: function (questionsChangeSet) {
                var url = config.apiPath + 'questions/batch';
                return asyncHelper.resolveHttp($http.put(url, questionsChangeSet));
            },
            search: function (options) {
                var url = config.apiPath + 'questions';

                var parameters = {
                    query: options.query,
                    orderBy: options.orderBy,
                    orderDirection: options.orderDirection,
                    lastUsedFromDate: options.lastUsedFromDate,
                    lastUsedToDate: options.lastUsedToDate,
                    tagId: options.tagId, // array
                    productId: options.productId, // array
                    rcoId: options.rcoId, // array
                    difficulty: options.difficulty, // array
                    status: options.status, // array
                    languageId: options.languageId, // array
                    customerSpecificFilterValue: options.customerSpecificFilterValue,
                    customerId: options.customerId // array
                };

                if (typeof options.limit === 'number') {
                    parameters.limit = options.limit;
                }
                if (typeof options.page === 'number' && options.page > 0) {
                    parameters.skip = (options.page - 1) * parameters.limit;
                }
                return asyncHelper.resolveHttp($http.get(url, { params: parameters }));
            }
        };
    }]);