﻿/*global angular, moment */
angular.module('phoenix.shared').filter('localDate', [function () {
    'use strict';
    return function (date, format, timezoneName) {
        if (!date) {
            return '';
        }
        
        var momentInTime = moment.utc(date);
        if (timezoneName) {
            momentInTime.tz(timezoneName);
        } else {
            momentInTime = momentInTime.local();
        }

        if (!format) {
            return momentInTime.format('DD.MM.YYYY HH:mm');
        }
        return momentInTime.format(format);
    };
}]);