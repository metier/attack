﻿/*global angular */
/*jslint maxlen: 150 */
angular.module('phoenix.shared').filter('imageRef', ['phoenix.config', function (config) {
    'use strict';
    return function (relativePath) {
        if (relativePath) {
            return config.basePath + 'app/assets/img/' + relativePath;
        }
        return null;
    };
}]);