﻿angular.module('phoenix.shared').filter('capitalize', function () {
    'use strict';
    return function (input) {
        if (input != null) {
            input = input.toString();
            input = input.toLowerCase();
            return input.substring(0, 1).toUpperCase() + input.substring(1);
        }
        return input;
    };
});