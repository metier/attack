﻿angular.module('phoenix.shared').filter('questionStatus', ['questionStatuses', function (questionStatuses) {
    'use strict';
    return function (questionStatusValue) {
        return _(questionStatuses).find(function (qs) {
            return qs.value === questionStatusValue;
        }).text;
    };
}]);