﻿angular.module('phoenix.shared').filter('participantResult', ['participantResults', function (participantResults) {
    'use strict';
    return function (participantResultValue) {
        var participantResult = _(participantResults).find(function (pr) {
            return pr.value === participantResultValue;
        });
        return participantResult ? participantResult.text : '';
    };
}]);