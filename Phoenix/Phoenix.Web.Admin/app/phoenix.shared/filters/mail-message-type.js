﻿angular.module('phoenix.shared').filter('mailMessageType', ['mailMessageTypes', function (mailMessageTypes) {
    'use strict';
    return function (messageTypeValue) {
        return _(mailMessageTypes).find(function (ct) {
            return ct.value === messageTypeValue;
        }).text;
    };
}]);