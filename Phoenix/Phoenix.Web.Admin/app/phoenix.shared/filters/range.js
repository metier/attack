﻿/*global angular */
angular.module('phoenix.shared').filter('range', [function () {
    'use strict';
    return function (input, min, max) {
        var i;
        min = parseInt(min, 10);
        max = parseInt(max, 10);
        for (i = min; i < max; i += 1) {
            input.push(i);
        }
        return input;
    };
}]);