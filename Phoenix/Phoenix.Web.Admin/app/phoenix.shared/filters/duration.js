﻿/*global angular */
angular.module('phoenix.shared').filter('duration', ['dateHelper', function (dateHelper) {
    'use strict';
    return function (milliseconds) {
        return dateHelper.formatDurationString(milliseconds);
    };
}]);