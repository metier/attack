﻿/*global angular */
/*jslint maxlen: 150 */
angular.module('phoenix.shared').filter('attachmentPath', ['phoenix.config', function (config) {
    'use strict';
    return function (attachment) {
        if (attachment) {
            return config.filesPath + attachment.FileId;
        }
        return null;
    };
}]);