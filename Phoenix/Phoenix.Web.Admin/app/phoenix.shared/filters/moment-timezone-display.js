﻿/*global angular, moment */
/* 
Usage:
momentTimezoneName should be on the format: "europe_oslo" (or something that is parsable to a moment timezone) 
*/
angular.module('phoenix.shared').filter('momentTimezoneDisplay', [function () {
    'use strict';
    var self = this;
    
    self.getNumber = function (theNumber) {
        if (theNumber >= 0) {
            return "+" + theNumber;
        }
        return theNumber.toString();
    };

    return function (momentTimezoneName) {
        if (!momentTimezoneName) {
            return "";
        }

        var zoneSet = moment.tz(momentTimezoneName),
            offset = self.getNumber((-1) * zoneSet.zone() / 60),
            zoneName = zoneSet.tz().replace(/_/g, ' ');
        return zoneName + ' (UTC' + offset + ')';
    };
}]);