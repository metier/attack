﻿angular.module('phoenix.shared').filter('orderStatus', ['orderStatuses', function (orderStatuses) {
    'use strict';
    return function (orderStatusValue) {
        return _(orderStatuses).find(function (os) {
            return os.value === orderStatusValue;
        }).text;
    };
}]);