﻿angular.module('phoenix.shared').filter('assetRef', ['phoenix.config', function (config) {
    'use strict';
    return function (relativePath) {
        if (relativePath) {
            return config.basePath + 'app/assets/' + relativePath;
        }
        return null;
    };
}]);