﻿angular.module('phoenix.shared').filter('educationPartnerSchemaType', ['educationPartnerSchemaTypes', function (educationPartnerSchemaTypes) {
    'use strict';
    return function (educationPartnerSchemaTypeValue) {
        return _(educationPartnerSchemaTypes).find(function (et) {
            return et.value === educationPartnerSchemaTypeValue;
        }).text;
    };
}]);