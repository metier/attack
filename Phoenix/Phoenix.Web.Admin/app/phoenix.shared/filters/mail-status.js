﻿angular.module('phoenix.shared').filter('mailStatus', ['mailStatuses', function (mailStatuses) {
    'use strict';
    return function (mailStatusValue) {
        return _(mailStatuses).find(function (os) {
            return os.value === mailStatusValue;
        }).text;
    };
}]);