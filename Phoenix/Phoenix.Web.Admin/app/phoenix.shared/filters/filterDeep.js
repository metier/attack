﻿angular.module('phoenix.shared').filter('filterDeep', function () {
    'use strict';

    function getValue(element, propertyArray) {
        var value = element;

        _.forEach(propertyArray, function (property) {
            value = value[property];
        });

        return value;
    }

    function isMatch(value, target) {
        if (!value) {
            return false;
        }
        return value.toLowerCase().indexOf(target.toLowerCase()) >= 0;
    }
    // Will filter on words deep inside an object:
    // Usage: ng-repeat="item in items | filterDeep:'child.property,child.property2':searchText
    return function (array, propertyPathsCsvString, target) {
        var propertyPaths = propertyPathsCsvString.split(',');
        var propertyPathsMap = _(propertyPaths).map(function (propertyString) {
            return { properties: propertyString.split(".") };
        });

        return _.filter(array, function (item) {
            var values = _(propertyPathsMap).map(function (p) {
                return getValue(item, p.properties);
            });

            if (!target) {
                return true;
            }

            var words = target.trim().split(' ');

            return _(words).all(function (word) {
                return _(values).some(function (value) {
                    return isMatch(value, word);
                });
            });
        });
    };
});