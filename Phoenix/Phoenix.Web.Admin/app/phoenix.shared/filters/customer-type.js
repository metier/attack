﻿angular.module('phoenix.shared').filter('customerType', ['customerTypes', function (customerTypes) {
    'use strict';
    return function (customerTypeValue) {
        return _(customerTypes).find(function (ct) {
            return ct.value === customerTypeValue;
        }).text;
    };
}]);