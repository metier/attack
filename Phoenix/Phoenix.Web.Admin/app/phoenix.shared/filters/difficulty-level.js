﻿angular.module('phoenix.shared').filter('difficultyLevel', ['difficultyLevels', function (difficultyLevels) {
    'use strict';
    return function (difficultyLevelValue) {
        return _(difficultyLevels).find(function (dl) {
            return dl.value === difficultyLevelValue;
        }).text;
    };
}]);