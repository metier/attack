﻿angular.module('phoenix.shared').filter('percentage', ['$filter', function ($filter) {
    'use strict';
    return function (input, decimals) {
        return $filter('number')(input * 100, decimals) + '%';
    };
}]);