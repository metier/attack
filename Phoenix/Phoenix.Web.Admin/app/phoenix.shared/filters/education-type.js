﻿angular.module('phoenix.shared').filter('educationType', ['educationTypes', function (educationTypes) {
    'use strict';
    return function (educationTypeValue) {
        return _(educationTypes).find(function (ct) {
            return ct.value === educationTypeValue;
        }).text;
    };
}]);