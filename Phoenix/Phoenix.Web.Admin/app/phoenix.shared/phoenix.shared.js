﻿/*global angular */
angular.module('phoenix.shared', ['phoenix.config', 'ui.bootstrap', 'ui.utils', 'placeholderShim', 'ngUpload', 'textAngular', 'ngSanitize', 'ui.select2']);