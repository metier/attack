﻿angular.module('phoenix.shared').constant('examTypes', [
    { value: 0, text: 'Random' },
    { value: 1, text: 'Fixed' }
]);