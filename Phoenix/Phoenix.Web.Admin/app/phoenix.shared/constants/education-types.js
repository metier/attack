﻿angular.module('phoenix.shared').constant('educationTypes', [
    { value: 0, text: 'None' },
    { value: 1, text: 'University' },
    { value: 2, text: 'University college' },
    { value: 3, text: 'High school/General study competence' }
]);