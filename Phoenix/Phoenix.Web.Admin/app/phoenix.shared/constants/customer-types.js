﻿angular.module('phoenix.shared').constant('customerTypes', [
    { value: 0, text: 'Arbitrary' },
    { value: 1, text: 'Corporate' }
]);