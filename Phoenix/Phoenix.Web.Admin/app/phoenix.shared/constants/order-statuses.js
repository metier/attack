﻿angular.module('phoenix.shared').constant('orderStatuses', [
    { value: 0, text: 'Proposed' },
    { value: 1, text: 'Draft' },
    { value: 2, text: 'Transferred' },
    { value: 3, text: 'Not Invoiced' },
    { value: 4, text: 'Invoiced' },
    { value: 5, text: 'Rejected' }
]);