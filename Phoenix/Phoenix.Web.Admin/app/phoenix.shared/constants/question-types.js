﻿angular.module('phoenix.shared').constant('questionTypes', [
    { value: 'TF', text: 'True/false' },
    { value: 'S', text: 'Multiple choice, single answer' },
    { value: 'M', text: 'Multiple choice, multiple answers' }
]);