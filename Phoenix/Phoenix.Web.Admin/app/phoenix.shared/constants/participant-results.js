﻿angular.module('phoenix.shared').constant('participantResults', [
    { value: 'PASSED', text: 'Passed' },
    { value: 'FAILED', text: 'Failed' }
]);