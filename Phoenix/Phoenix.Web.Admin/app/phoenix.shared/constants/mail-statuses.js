﻿angular.module('phoenix.shared').constant('mailStatuses', [
    { value: 0, text: 'Created' },
    { value: 1, text: 'Sent' },
    { value: 2, text: 'Failed' }
]);