﻿angular.module('phoenix.shared').constant('questionCustomerSpecificFilters', [
    { value: '0', text: 'Show all (No filter)' },
    { value: '1', text: 'Show only generic questions' },
    { value: '2', text: 'Show only custom questions' }
]);