﻿angular.module('phoenix.shared').constant('difficultyLevels', [
    { value: 1, text: 'Easy' },
    { value: 2, text: 'Medium' },
    { value: 3, text: 'Hard' }
]);