﻿angular.module('phoenix.shared').constant('mailMessageTypes', [
    { value: 0, text: 'Password reset request' },
    { value: 1, text: 'User registration notification' },
    { value: 2, text: 'Enrollment confirmation' },
    { value: 3, text: 'Reminder (workshop, exam)' },
    { value: 4, text: 'Reminder (e-learning)' },
    { value: 5, text: 'E-learning completed' },
    { value: 6, text: 'Enrollment confirmation (customer)' },
    { value: 7, text: 'Not qualified for exam' },
    { value: 8, text: 'Examiner reminder' },
    { value: 9, text: 'Candidate survey' },
    { value: 10, text: 'Exam submitted' }
]);