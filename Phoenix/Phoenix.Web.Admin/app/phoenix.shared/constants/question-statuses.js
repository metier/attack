﻿angular.module('phoenix.shared').constant('questionStatuses', [
    { value: 1, text: 'Draft' },
    { value: 2, text: 'Published' },
    { value: 9, text: 'Obsolete' }
]);