﻿angular.module('phoenix.shared').factory('QueryParameters', [
    function () {
        'use strict';
        function formatQueryString(name, value) {
            if (_.isDate(value)) {
                return { name: name, value: moment(value).format('YYYY-MM-DD') };
            }
            else {
                return { name: name, value: value };
            }
        }

        return {
            // Will generate query parameters from the properties and arrays of an object
            // Returns: '?property1=value&array1=value1&array1=value2'
            parse: function (object) {
                var parameters = [];
                if (object) {
                    for (var key in object) {
                        if (object.hasOwnProperty(key)) {
                            if (_.isArray(object[key])) {
                                var array = object[key];
                                for (var i = 0; i < array.length; i++) {
                                    parameters.push(formatQueryString(key, array[i]));
                                }
                            }
                            else {
                                if (object[key] || object[key] === false) {
                                    parameters.push(formatQueryString(key, object[key]));
                                }
                            }
                        }
                    }
                }
                if (parameters.length === 0) { return ''; }
                return _(parameters).reduce(function (memo, item, index) {
                    return memo + item.name + '=' + item.value + (index < parameters.length - 1 ? '&' : '');
                }, '?');
            }
        };
    }
]);