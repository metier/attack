﻿/*global moment*/
angular.module('phoenix.shared').factory('dateHelper', [function () {
    'use strict';
    return {
        formatDurationString: function (milliseconds) {
            var days, hours, minutes, resultString = '', duration;
        
            duration = moment.duration(milliseconds);

            days = Math.floor(duration.asDays());
            if (days) {
                resultString += days + 'd ';
                duration.subtract(days, 'd');
            }
            hours = Math.floor(duration.asHours());
            if (hours) {
                resultString += hours + 'h ';
                duration.subtract(hours, 'h');
            }
            minutes = Math.floor(duration.asMinutes());
            if (minutes) {
                resultString += minutes + 'm ';
                duration.subtract(minutes, 'm');
            }
            return resultString;
        }
    };
}]);