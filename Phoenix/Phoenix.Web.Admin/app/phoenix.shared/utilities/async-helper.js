﻿angular.module('phoenix.shared').factory('asyncHelper', [
    '$q',
    '$state',
    function($q, $state) {
        'use strict';
        return {
            resolveHttp: function(httpPromise, redirectOn404) {
                var deferred = $q.defer();
                httpPromise.success(function(result) {
                    deferred.resolve(result);
                }).error(function (error, status) {
                    deferred.reject(error);
                    if (redirectOn404 && status === 404) {
                        $state.go('notfound');
                    }
                });
                return deferred.promise;
            }
        };
    }
]);