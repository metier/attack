﻿angular.module('phoenix.shared').factory('Timer', [
    '$interval',
    function ($interval) {
        'use strict';

        function twoDigits(number) {
            return ("0" + number).slice(-2);
        }

        function getTimerText(secondsRemaining) {
            var dur = moment.duration({ seconds: secondsRemaining });
            return twoDigits(dur.hours()) + ':' + twoDigits(dur.minutes()) + ':' + twoDigits(dur.seconds());
        }

        function Timer(initialValue) {
            var self = this;
            self.secondsRemaining = initialValue;
            self.isRunning = false;
            self.timerText = getTimerText(self.secondsRemaining);

            // Tick the timer every second
            $interval(function () {
                if (self.isRunning) {
                    if (self.secondsRemaining > 0) {
                        self.secondsRemaining--;
                        self.timerText = getTimerText(self.secondsRemaining);
                        self.onTextUpdated();

                        if (self.secondsRemaining === 0) {
                            self.stop();
                        }
                    }
                }
            }, 1000);
        }

        Timer.prototype = {
            start: function () {
                this.isRunning = true;
            },
            stop: function () {
                this.isRunning = false;
                this.onStopped();
            },
            onTextUpdated: function () { },
            onStopped: function () { }
        };

        return Timer;
    }
]);