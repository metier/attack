﻿angular.module('phoenix.shared').directive('placeholderImage', ['phoenix.config', function (config) {
    'use strict';
    return {
        restrict: 'E',
        template: '<img src="' + config.basePath + 'app/assets/img/placeholder_300x300.png" class="img-thumbnail" />'
    };
}]);