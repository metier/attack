﻿angular.module('phoenix.shared').directive('qualificationStatus', [
    'phoenix.config', function(config) {
        'use strict';
        return {
            scope: {
                status: '@'
            },
            restrict: 'E',
            templateUrl: config.basePath + 'app/phoenix.shared/directives/qualification-status/qualification-status.html',
            transclude: true
            ,
            link: function (scope, element, attrs) {

                scope.getColorCodingForQualificationStatus = function() {

                    switch (scope.status) {
                    case 'PARTIALLYQUALIFIED':
                        return 'yellow';
                    case 'QUALIFIED':
                        return 'lightgreen';
                    default:
                        return 'red';
                    }
                };
            }
        };
    }
]);