﻿angular.module('phoenix.shared').directive('userSearch', [
    'phoenix.config',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            templateUrl: config.basePath + 'app/phoenix.shared/directives/user-search/user-search.html',
            controller: 'UserSearchCtrl',
            scope: {
                model: '=ngModel',
                userSelected: '&onUserSelected',
                inputBlur: '&onBlur',
                distributorId: '='          //Will search across all distributors if not provided
            },
            link: function (scope, element, attrs) {
                scope.$watch('model', function () {
                    scope.$eval(attrs.ngModel + ' = model');
                });

                scope.$watch(attrs.ngModel, function (val) {
                    if (val !== scope.user) {
                        scope.user = val;
                    }
                });
            }
        };
    }
]);

angular.module('phoenix.shared').controller('UserSearchCtrl', [
    'phoenix.config',
    '$scope',
    'UsersService',
    function (config, $scope, usersService) {
        'use strict';

        var hasChanged = false;

        $scope.hasblurred = false;
        $scope.isSearching = false;
        $scope.user = $scope.model;

        $scope.searchUser = function (searchString) {
            $scope.isSearching = true;

            return usersService.search({ distributorId: $scope.distributorId, query: searchString, limit: 50 }).then(function (result) {
                return result.Items;
            }).finally(function () {
                $scope.isSearching = false;
            });
        };

        $scope.userSearchTemplateUrl = config.basePath + 'app/phoenix.shared/directives/user-search/user-search-result-template.html';

        $scope.onUserSelected = function(selectedUser) {
            if (selectedUser) {
                $scope.model = selectedUser;
                hasChanged = true;
            }
        };

        $scope.setValidSelectedUser = function () {
            if (!$scope.user) {
                $scope.model = null;
            } else {
                $scope.user = $scope.model;
                hasChanged = true;
            }
            $scope.hasblurred = true;
        };

        $scope.$watch(function() {
            return $scope.hasblurred;
        }, function () {
            if ($scope.hasblurred) {
                $scope.inputBlur();
                $scope.hasblurred = false;
            }
        });

        $scope.$watch(function() {
            return $scope.model;
        }, function () {
            if (hasChanged) {
                $scope.userSelected();
                hasChanged = false;
            }
        });
    }
]);