﻿/*global angular */
/*jslint maxlen: 150 */
angular.module('phoenix.shared').controller('HelpTextModalCtrl', [
    '$scope',
    'HelpTextsService',
    '$modalInstance',
    'options',
    'helpText',
    function ($scope, helpTextsService, $modalInstance, options, helpText) {
        'use strict';
        $scope.title = options.title;
        $scope.section = options.section;
        $scope.name = options.name;

        $scope.helpText = helpText || {
            Name: $scope.name,
            Section: $scope.section,
            Text: ''
        };
        
        $scope.edit = function () {
            $scope.helpTextEdit = angular.copy($scope.helpText);
            $scope.isEditing = true;
        };
        
        $scope.save = function () {
            var saveFunction;
            if ($scope.helpTextEdit.Id) {
                saveFunction = helpTextsService.update;
            } else {
                saveFunction = helpTextsService.create;
            }
            
            saveFunction($scope.helpTextEdit).then(function (result) {
                $scope.helpText = result;
                $scope.isEditing = false;
            }, function () {
                $scope.isEditing = false;
            });            
        };
        
        $scope.cancel = function () {
            $scope.isEditing = false;
        };

        $scope.close = function () {    
            $modalInstance.dismiss();
        };
    }
]);