﻿/*global angular */
/*jslint maxlen: 150 */
angular.module('phoenix.shared').directive('help', [function () {
    'use strict';
    return {
        restrict: 'EA',
        scope: {
            section: '@',
            name: '@',
            title: '@'
        },
        template: '<i class="fa fa-question-circle clickable" ng-click="openHelpDialog()" title="Get help"></i>',
        controller: ['$scope', '$modal', 'phoenix.config', 'HelpTextsService', function ($scope, $modal, config, helpTextsService) {
            $scope.openHelpDialog = function () {
                $modal.open({
                    backdrop: 'static',
                    templateUrl: config.basePath + 'app/phoenix.shared/directives/help/help-text-modal.html',
                    controller: 'HelpTextModalCtrl',
                    resolve: {
                        options: function() {
                            return {
                                title: $scope.title,
                                section: $scope.section,
                                name: $scope.name
                            };
                        },
                        helpText: function() {
                            return helpTextsService.get($scope.section, $scope.name);
                        }
                    }
                });
            };
        }]
    };
}]);