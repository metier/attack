﻿angular.module('phoenix.shared').directive('pageLimitSelector', [
    'phoenix.config',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {},
            templateUrl: config.basePath + 'app/phoenix.shared/directives/page-limit-selector/page-limit-selector.html',
            controller: ['$scope', '$location', function ($scope, $location) {
                $scope.limits = [15, 30, 50, 100];
                $scope.selectedLimit = parseInt($location.search().limit, 10) || $scope.limits[0];

                $scope.limitSelected = function () {
                    $location.search('limit', $scope.selectedLimit);
                    $location.search('page', 1);
                };
            }]
        };
    }
]);