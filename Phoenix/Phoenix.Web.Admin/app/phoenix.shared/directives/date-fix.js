﻿/* 
* Directive to fix european formats on dates.
* It will also convert dates to UTC+0 (zulu time)
* Inspired by (but modified): http://developer.the-hideout.de/?p=119
*/
angular.module('phoenix.shared').directive('dateFix', ['dateFilter', function (dateFilter) {
    'use strict';
    // return the directive link function. (compile function not needed)
    return {
        restrict: 'EA',
        require: 'ngModel', // get a hold of NgModelController

        link: function (scope, element, attrs, ngModel) {

            var format = attrs.datepickerPopup;
            var maxDate = scope[attrs.max];
            var minDate = scope[attrs.min];
            var model = ngModel;

            ngModel.$parsers.push(function (viewValue) {
                var newDate = model.$viewValue;
                var date = null;

                if (!newDate) {
                    newDate = "";
                }
                // pass through if we clicked date from popup
                if (newDate === "") {
                    return newDate;
                }
                if (typeof newDate === "object") {
                    var myMoment = moment(newDate);
                    return moment.utc({
                        day: myMoment.date(),
                        month: myMoment.month(),
                        year: myMoment.year()
                    }).toDate();
                }
                
                // build a new date according to initial localized date format
                if (format === "dd.MM.yyyy") {
                    // extract day, month and year
                    var splitted = newDate.split('.');

                    var month = parseInt(splitted[1]) - 1;
                    date = moment.utc({
                        day: splitted[0],
                        month: month,
                        year: splitted[2]
                    }).toDate();

                    // if maxDate,minDate is set make sure we do not allow greater values
                    if (maxDate && date > maxDate) {
                        date = maxDate;
                    }
                    if (minDate && date < minDate) {
                        date = minDate;
                    }

                    model.$setValidity('date', true);
                    model.$setViewValue(date);
                }
                return date ? date : viewValue;
            });

            element.on('keydown', { scope: scope, varOpen: attrs.isOpen }, function (e) {
                var response = true;
                // the scope of the date control
                var scope = e.data.scope;
                // the variable name for the open state of the popup (also controls it!)
                var openId = e.data.varOpen;

                switch (e.keyCode) {
                    case 13: // ENTER
                        scope[openId] = !scope[openId];
                        // update manually view
                        if (!scope.$$phase) {
                            scope.$apply();
                        }
                        response = false;
                        break;

                    case 9: // TAB
                        scope[openId] = false;
                        // update manually view
                        if (!scope.$$phase) {
                            scope.$apply();
                        }
                        break;
                }

                return response;
            });

            // set input to the value set in the popup, which can differ if input was manually!
            element.on('blur', { scope: scope }, function () {
                // the value is an object if date has been changed! Otherwise it was set as a string.
                if (typeof model.$viewValue === "object") {
                    element.context.value = isNaN(model.$viewValue) ? "" : dateFilter(model.$viewValue, format);
                }
            });
        }
    };
}]);