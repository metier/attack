﻿angular.module('phoenix.shared').directive('htmlBlock', [function () {
    'use strict';

    function getDocument(iFrameElement) {
        return iFrameElement.contentDocument || iFrameElement.contentWindow.document;
    }

    return {
        replace: true,
        scope: {
            html: '@'
        },
        restrict: 'E',
        template: '<iframe frameborder="0" scrolling="no"></iframe>',
        link: function (scope, element, iAttrs) {
            var iFrameElement = element[0];

            iFrameElement.onload = function () {
                var iframeDocument = getDocument(iFrameElement);
                if (iframeDocument.body) {
                    iFrameElement.style.height = iframeDocument.body.scrollHeight + 'px';
                    iFrameElement.height = iframeDocument.body.scrollHeight + 'px';
                }
            };
            
            var doc = getDocument(iFrameElement);
            doc.open();
            doc.write(iAttrs.html);
            doc.close();

            iFrameElement.style.width = '100%';
        }
    };
}]);