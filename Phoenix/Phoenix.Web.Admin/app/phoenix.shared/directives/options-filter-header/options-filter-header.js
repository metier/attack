﻿angular.module('phoenix.shared').directive('optionsFilterHeader', ['phoenix.config', '$document', '$location', function (config, $document, $location) {
    'use strict';
    return {
        scope: {
            options: '=',
            queryParam: '@'
        },
        restrict: 'EA',
        replace: true,
        templateUrl: config.basePath + 'app/phoenix.shared/directives/options-filter-header/options-filter-header.html',
        link: function (scope, element) {
            scope.isDropdownVisible = false;
            scope.selectedValues = [];

            // Make sure dropdown is hidden when user clicks somewhere else
            $document.bind('click', function(event){
                var isClickedElementChildOfDropdown = element.find(event.target).length > 0;
                if (isClickedElementChildOfDropdown) {
                    return;
                }    
                scope.isDropdownVisible = false;
                scope.$apply();
            });
            
            scope.toggleFilter = function () {
                scope.setSelectedFromUrl();
                scope.isDropdownVisible = !scope.isDropdownVisible;
            };

            scope.applyFilter = function () {
                scope.selectedValues = scope.getSelectedOptionValues();
                $location.search(scope.queryParam, scope.selectedValues);
                scope.isDropdownVisible = false;
            };

            scope.resetFilter = function () {
                $location.search(scope.queryParam, null);
                scope.selectedValues = [];
                scope.isDropdownVisible = false;
            };

            scope.getSelectedOptionValues = function () {
                var selectedOptions = _(scope.options).filter(function (opt) {
                    return opt.isSelected;
                });
                return _(selectedOptions).map(function (opt) {
                    return opt.value;
                });
            };

            scope.setSelectedFromUrl = function () {
                var queryParamValues = $location.search()[scope.queryParam];
                _(scope.options).each(function (opt) {
                    var matchingQueryParams = _(queryParamValues).filter(function (q) {
                        return q.toString() === opt.value.toString();
                    });
                    opt.isSelected = matchingQueryParams.length > 0;
                });
                scope.selectedValues = scope.getSelectedOptionValues();
            };

            scope.setSelectedFromUrl();
        }
    };
}]);