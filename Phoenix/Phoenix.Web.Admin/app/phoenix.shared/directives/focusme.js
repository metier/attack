﻿/*global angular */
/*jslint maxlen: 150 */
angular.module('phoenix.shared').directive('focusMe', ['$parse', '$timeout', function ($parse, $timeout) {
    'use strict';
    return {
        link: function (scope, element, attrs) {
            var model = $parse(attrs.focusMe);
            scope.$watch(model, function (value) {
                if (value === true) {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            });
        }
    };
}]);