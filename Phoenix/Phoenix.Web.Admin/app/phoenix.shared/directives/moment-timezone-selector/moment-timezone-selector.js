﻿/*global moment,_*/
angular.module('phoenix.shared').directive('momentTimezoneSelector', [
    'phoenix.config',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                zonesetSelected: '&',
                selectedZonesetName: '='
            },
            templateUrl: config.basePath + 'app/phoenix.shared/directives/moment-timezone-selector/moment-timezone-selector.html',
            controller: ['$scope', function ($scope) {
                var self = this;
                $scope.timezones = moment.tz.zones();

                self.matchTimezones = function (term, text, option) {
                    return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || 
                        option[0].value.toUpperCase().indexOf(term.toUpperCase()) >= 0;
                };

                $scope.timezones = _($scope.timezones).filter(function (timeZone) {
                    var place = timeZone.displayName.split('/')[1];
                    return !!place;
                });

                $scope.timezones = _($scope.timezones).uniq(function (timeZone) {
                    var place = timeZone.displayName.split('/')[1];
                    return place;
                });

                $scope.regions = _($scope.timezones).groupBy(function (timeZone) {
                    timeZone.region = timeZone.displayName.split('/')[0];
                    timeZone.place = timeZone.displayName.split('/')[1].replace(/_/g, ' ');

                    return timeZone.region;
                });

                $scope.selectOptions = {
                    matcher: self.matchTimezones
                };
                
                $scope.$watch('selectedZonesetName', function () {
                    var zoneSet;
                    if ($scope.selectedZonesetName) {
                        zoneSet = _($scope.timezones).find(function (zone) {
                            return zone.name === $scope.selectedZonesetName;
                        });
                        
                        $scope.zonesetSelected({ zone: zoneSet });
                        $scope.utcTime = moment.tz($scope.localTime, $scope.selectedZonesetName).utc();
                    }
                });                
            }]
        };
    }
]);