﻿/*global xlsx, FileReader*/
angular.module('phoenix.shared').directive('xlsxFileSelect', [
    '$parse',
    'NotificationService',
    function ($parse, notificationService) {
        'use strict';
        var self = {};

        self.onFileReadError = function () {
            notificationService.alertError({ header: 'Error reading file', message: 'An error occurred when trying to read the file.' });
        };

        self.parseXlsx = function (xlsx) {
            var parsed = {}, col, row, worksheet, worksheetRows, currentRow, currentCell, colHeader;
            for (var i = 0; i < xlsx.worksheets.length; i++) {
                worksheet = xlsx.worksheets[i];
                worksheetRows = [];
                for (row = 1; row < worksheet.data.length; row++) {
                    currentRow = {};
                    for (col = 0; col < worksheet.data[row].length; col++) {
                        colHeader = worksheet.data[0][col].value;
                        if (!colHeader) {
                            throw { message: 'Header is not set!' };
                        }
                        currentCell = worksheet.data[row][col];
                        currentRow[colHeader] = currentCell ? worksheet.data[row][col].value : '';
                    }
                    
                    var isEmptyRow = true;
                    for (col = 0; col < worksheet.data[row].length; col++) {
                        if (currentRow[worksheet.data[0][col].value]) {
                            isEmptyRow = false;
                            break;
                        }
                    }

                    if (!isEmptyRow) {
                        worksheetRows.push(currentRow);
                    }
                }
                parsed[worksheet.name] = worksheetRows;
            }
            
            return parsed;
        };

        return {
            restrict: 'A',
            link: function (scope, elm, attrs) {
                var model = $parse(attrs.xlsxFileSelect);

                elm.bind('change', function (evt) {
                    var fileReader = new FileReader();

                    fileReader.addEventListener('load', function (event) {
                        var base64Data = event.target.result.split('base64,')[1],
                            xlsxFile,
                            parsed;
                        try {
                            xlsxFile = xlsx(base64Data);
                            parsed = self.parseXlsx(xlsxFile);
                        } catch (e) {
                            self.onFileReadError();
                            throw e;
                        }
                        if (parsed) {
                            scope.$apply(model.assign(scope, parsed));
                        }
                    });

                    fileReader.addEventListener('error', function () {
                        self.onFileReadError();
                    });

                    fileReader.readAsDataURL(evt.target.files[0]);
                });

                scope.$watch(attrs.xlsxFileSelect, function () {
                    if (!model(scope)) {
                        elm.val(null);
                    }
                });
            }
        };
    }
]);