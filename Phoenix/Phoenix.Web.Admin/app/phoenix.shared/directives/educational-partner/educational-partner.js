﻿angular.module('phoenix.shared').directive('educationalPartner', [
    'phoenix.config',
    function(config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                label: '@',
                selectedEducationalPartner: '=',
                readonly: '=',
                selectionChanged: '&'
            },
            templateUrl: config.basePath + 'app/phoenix.shared/directives/educational-partner/educational-partner.html',
            controller: 'EducationalPartnerCtrl'
        };
    }
]);