﻿/*global angular*/
angular.module('phoenix.shared').controller('EducationalPartnerCtrl', [
    '$scope',
    '$q',
    'ResourcesService',
    function ($scope, $q, resourcesService) {
        'use strict';
        $scope.isInitialized = false;
        $scope.educationalPartners = [];
        var self = this;

        self.getSelectedPartnerName = function () {
            var name = "";

            _.find($scope.educationalPartners, function(partner) {
                if(partner.Id === $scope.selectedEducationalPartner.Id){
                    name = partner.Name;
                }
            });

            return name;
        };

        $scope.educationalPartnerChanged = function() {
            $scope.selectedEducationalPartner.Name = self.getSelectedPartnerName();
            $scope.selectionChanged();
        }

        function init() {
            resourcesService.getEducationalPartners({}).then(function (result) {
                $scope.educationalPartners = result.Items;
                $scope.selectedEducationalPartner.Name = self.getSelectedPartnerName();
                $scope.isInitialized = true;
            });
        }

        init();
    }
]);