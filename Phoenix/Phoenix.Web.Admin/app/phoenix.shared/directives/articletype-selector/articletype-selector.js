angular.module('phoenix.shared').directive('articletypeSelector', [
    'phoenix.config',
    function(config) {
        'use strict';
        return {
            restrict: 'E',
            templateUrl: config.basePath + 'app/phoenix.shared/directives/articletype-selector/articletype-selector.html',
            controller: 'ArticletypeSelectorCtrl',
            scope: {
                selectedArticletypeId: '=?'
            },
            link: function(scope, element, attrs){
                scope.isRequired = false;
                scope.clearOnSelect = false;

                if (attrs.clearOnSelect === "" || attrs.clearOnSelect === "true")
                    scope.clearOnSelect = true;

                if (attrs.isRequired === "" || attrs.isRequired === "true")
                    scope.isRequired = true;
            }
        };
    }
]);

angular.module('phoenix.shared').controller('ArticletypeSelectorCtrl', [
    'phoenix.config',
    '$scope',
    'ArticlesService',
    function (config, $scope, articlesService) {
        'use strict';

        $scope.isSearching = false;

        self.init = function () {

            if ($scope.selectedArticletypeId) {
                self.loadArticletype($scope.selectedArticletypeId);
            };
        };

        self.loadArticletype = function (articletypeId) {
            if (articletypeId) {
                articlesService.getArticletype(articletypeId).then(function (result) {
                    $scope.articletype = result;
                });
            }
            else{
                $scope.articletype = null;
            };
        };

        $scope.searchArticletype = function (searchString) {
            $scope.isSearching = true;
            return articlesService.searchArticletypes(searchString).then(function (result) {
                return result;
            }).finally(function() {
                $scope.isSearching = false;
            });
        };


        $scope.articletypeSelectorTemplateUrl = config.basePath + 'app/phoenix.shared/directives/articletype-selector/articletype-selector-result-template.html';

        $scope.onArticletypeSelected = function () {
            $scope.selectedArticletypeId = $scope.articletype.Id;
            if($scope.clearOnSelect === true){
                $scope.articletype = null;
            }
        }

        $scope.setValidSelectedArticletype = function () {
            if (!$scope.articletype || $scope.articletype === "") {
                $scope.selectedArticletypeId = null;
            }
            else if (!$scope.articletype.Id) {
                self.loadArticletype($scope.selectedArticletypeId);
            }
            else {
                $scope.selectedArticletypeId = $scope.articletype.Id;
            }
        }

        self.init();
    }
]);