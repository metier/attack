﻿angular.module('phoenix.shared').directive('changeLog', [
    'phoenix.config',
    function(config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                entityId: '@',
                entityType: '@'
            },
            templateUrl: config.basePath + 'app/phoenix.shared/directives/change-log/change-log.html',
            controller: 'ChangeLogCtrl'
        };
    }
]);