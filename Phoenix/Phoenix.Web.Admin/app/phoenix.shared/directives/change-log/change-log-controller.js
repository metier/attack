﻿/*global angular*/
angular.module('phoenix.shared').controller('ChangeLogCtrl', [
    '$scope',
    '$q',
    'ChangeLogService',
    function ($scope, $q, changeLogService) {
        'use strict';
        var numberOfEntries = 10,
            currentPage = 1,
            entityType = parseInt($scope.entityType, 10),
            entityId = parseInt($scope.entityId, 10);

        $scope.isInitialized = false;
        $scope.isLoadingNextEntries = false;
        $scope.hasMoreEntries = false;
        
        $scope.loadMoreEntries = function () {
            if (!$scope.hasMoreEntries) {
                return;
            }
            $scope.isLoadingNextEntries = true;
            changeLogService.get({
                entityType: entityType,
                entityId: entityId,
                limit: numberOfEntries,
                page: currentPage + 1
            }).then(function (result) {
                currentPage++;
                $scope.entries = $scope.entries.concat(result.Items);
                $scope.hasMoreEntries = $scope.entries.length < result.TotalCount;
            }).finally(function() {
                $scope.isLoadingNextEntries = false;
            });
        };

        function init() {
            changeLogService.get({
                entityType: entityType,
                entityId: entityId,
                limit: numberOfEntries,
                page: 1
            }).then(function (result) {
                currentPage = 1;
                $scope.entries = result.Items;
                $scope.hasMoreEntries = $scope.entries.length < result.TotalCount;
                $scope.isInitialized = true;
            });
        }

        init();
    }
]);