﻿angular.module('phoenix.shared').directive('toggleEdit', ['phoenix.config', '$parse', function (config, $parse) {
    'use strict';
    return {
        scope: {
            isMultiline: '=',
            hideCancelButton: '=',
            textChanged: '&',
            rows: '@'
        },
        restrict: 'E',
        templateUrl: config.basePath + 'app/phoenix.shared/directives/toggle-edit/toggle-edit.html',
        transclude: true,
        link: function (scope, element, attrs) {
            var model = $parse(attrs.ngModel);
            if (!attrs.rows) {
                scope.rows = '3';
            }

            scope.isEditing = false;
            scope.editText = model(scope.$parent);
            scope.isCancelling = false;

            scope.edit = function () {
                scope.isEditing = true;
                scope.isCancelling = false;
                scope.editText = model(scope.$parent);
            };

            scope.save = function () {
                if (!scope.isCancelling && scope.isEditing) {
                    model.assign(scope.$parent, scope.editText);
                    scope.isEditing = false;
                    scope.textChanged();
                }
            };

            scope.cancel = function () {
                scope.isCancelling = true;
                scope.isEditing = false;
            };
        }
    };
}]);