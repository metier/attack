﻿angular.module('phoenix.shared').directive('duration', ['$parse', 'dateHelper', function ($parse, dateHelper) {
    'use strict';
    var self = this || {};
    self.getMillisecondsFromString = function (timeString) {
        var seconds = 0, days, hours, minutes;
        days = timeString.match(/(\d+)\s*d/);
        hours = timeString.match(/(\d+)\s*h/);
        minutes = timeString.match(/(\d+)\s*m/);
        if (days) { seconds += parseInt(days[1], 10) * 86400; }
        if (hours) { seconds += parseInt(hours[1], 10) * 3600; }
        if (minutes) { seconds += parseInt(minutes[1], 10) * 60; }
        return seconds * 1000;
    };

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {

            ctrl.$parsers.unshift(function (viewValue) {
                if (viewValue) {
                    var milliseconds = self.getMillisecondsFromString(viewValue);
                    if (milliseconds > 0) {
                        ctrl.$setValidity('duration', true);
                        return milliseconds;
                    } else {
                        ctrl.$setValidity('duration', false);
                        return undefined;
                    }
                } else {
                    ctrl.$setValidity('duration', true);
                    return undefined;
                }
            });

            ctrl.$formatters.unshift(function (value) {
                return dateHelper.formatDurationString(value);
            });
            
            element[0].placeholder = 'e.g. 1d 2h 30m';
        }
    };
}]);