﻿angular.module('phoenix.shared').directive('selectOnFocus', function () {
    'use strict';
    return function (scope, element) {
        element.bind('focus', function () {
            this.select();
        });
    };
});