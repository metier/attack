angular.module('phoenix.shared').directive('productSelector', [
    'phoenix.config',
    function(config) {
        'use strict';
        return {
            restrict: 'E',
            templateUrl: config.basePath + 'app/phoenix.shared/directives/product-selector/product-selector.html',
            controller: 'PoductSelectorCtrl',
            scope: {
                selectedProductId: '=?'
            },
            link: function(scope, element, attrs){
                scope.isRequired = false;
                scope.clearOnSelect = false;

                if (attrs.clearOnSelect === "" || attrs.clearOnSelect === "true")
                    scope.clearOnSelect = true;

                if (attrs.isRequired === "" || attrs.isRequired === "true")
                    scope.isRequired = true;
            }
        };
    }
]);

angular.module('phoenix.shared').controller('PoductSelectorCtrl', [
    'phoenix.config',
    '$scope',
    'ProductsService',
    function (config, $scope, productsService) {
        'use strict';

        $scope.isSearching = false;

        self.init = function () {

            if ($scope.selectedProductId) {
                self.loadProduct($scope.selectedProductId);
            };
        };

        self.loadProduct = function(productId){
            if(productId){
                productsService.get(productId).then(function (result) {
                    $scope.product = result;
                });
            }
            else{
                $scope.product = null;
            };
        };

        $scope.searchProduct = function (searchString) {
            $scope.isSearching = true;
            return productsService.search(searchString).then(function (result) {
                return result;
            }).finally(function() {
                $scope.isSearching = false;
            });
        };


        $scope.productSelectorTemplateUrl = config.basePath + 'app/phoenix.shared/directives/product-selector/product-selector-result-template.html';

        $scope.onProductSelected = function () {
            $scope.selectedProductId = $scope.product.Id;
            if($scope.clearOnSelect === true){
                $scope.product = null;
            }
        }

        $scope.setValidSelectedProduct = function () {
            if (!$scope.product || $scope.product === "") {
                $scope.selectedProductId = null;
            }
            else if (!$scope.product.Id) {
                self.loadProduct($scope.selectedProductId);
            }
            else {
                $scope.selectedProductId = $scope.product.Id;
            }
        }

        self.init();
    }
]);