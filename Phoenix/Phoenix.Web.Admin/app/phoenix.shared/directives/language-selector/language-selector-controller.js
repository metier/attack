﻿/*global angular*/
angular.module('phoenix.shared').controller('LanguageSelectorCtrl', [
    '$scope',
    '$q',
    'LanguagesService',
    function ($scope, $q, languagesService) {
        'use strict';
        $scope.isInitialized = false;
        $scope.languages = [];
        $scope.languageId = -1;
        $scope.selectedLanguageName = "";

        var self = this;

        self.getLanguageName = function (languageId) {
            var name = "";
            languageId = Number(languageId); //Make sure the provided value is a number, not a string

            _.find($scope.languages, function (language) {
                if (language.Id === languageId) {
                    name = language.Name;
                }
            });

            return name;
        };

        $scope.selectedLanguageChanged = function (languageId) {
            languageId = Number(languageId);

            if (languageId <= 0)
                languageId = null;
            else {
                self.getLanguageName(languageId);
            }

            $scope.selectedLanguageId = languageId;
        }

        function init() {
            $scope.languageId = $scope.selectedLanguageId;
            languagesService.getAll({}).then(function (result) {
                $scope.languages = result;
                $scope.selectedLanguageName = self.getLanguageName($scope.selectedLanguageId);
                $scope.isInitialized = true;
            });
        }

        init();
    }
]);;