﻿angular.module('phoenix.shared').directive('languageSelector', [
    'phoenix.config',
    function(config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                selectedLanguageId: '=?',
                readonly: '='
            },
            templateUrl: config.basePath + 'app/phoenix.shared/directives/language-selector/language-selector.html',
            controller: 'LanguageSelectorCtrl'
        };
    }
]);