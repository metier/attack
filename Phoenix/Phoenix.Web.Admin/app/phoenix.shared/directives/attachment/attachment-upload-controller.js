﻿/*global angular */
/*jslint maxlen: 150 */
angular.module('phoenix.shared').controller('AttachmentUploadDirectiveCtrl', ['$scope', 'phoenix.config', function ($scope, config) {
    'use strict';
    var self = this;

    $scope.filesUploadUrl = config.apiPath + "files";
    $scope.model = {};
    $scope.editTitle = $scope.editTitle === 'true';
    $scope.editDescription = $scope.editDescription === 'true';

    self.getFileNameFromPath = function (path) {
        return path.replace(/^.*[\\\/]/, '');
    };
    
    $scope.onfileUpload = function (content, completed) {
        if (completed && content) {
            if (!content.Title) {
                content.Title = self.originalFilename;
            }

            var fileUrl = config.apiPath + 'files/' + content.FileId;
            $scope.model.title = '';
            $scope.model.description = '';
            $scope.fileUploaded({ attachment: content, fileUrl: fileUrl });
        }
    };

    $scope.onImageSelected = function (imageValue) {
        $scope.selectedImagePath = imageValue;
        self.originalFilename = self.getFileNameFromPath(imageValue);
    };
   
}]);