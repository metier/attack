﻿/*global angular*/
angular.module('phoenix.shared').directive('comments', [
    'phoenix.config',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                entity: '@',
                id: '@',
                excludeHeader: '=',
                commentsUpdated: '&'
            },
            templateUrl: config.basePath + 'app/phoenix.shared/directives/comments/comments.html',
            controller: 'CommentsCtrl'
        };
    }
]);