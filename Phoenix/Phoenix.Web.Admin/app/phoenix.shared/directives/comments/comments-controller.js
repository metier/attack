﻿/*global angular*/
angular.module('phoenix.shared').controller('CommentsCtrl', [
    '$scope',
    '$q',
    'CommentsService',
    'AuthenticationService',
    function ($scope, $q, commentsService, authenticationService) {
        'use strict';
        var self = this;
        $scope.isInitialized = false;
        $scope.isBusySaving = false;
        
        $scope.addComment = function () {
            $scope.isBusySaving = true;
            commentsService.create($scope.newComment).then(function (result) {
                $scope.isBusySaving = false;
                $scope.comments = result;
                $scope.commentsUpdated({ comments: $scope.comments });
                $scope.newComment = self.emptyComment();
            }, function () {
                $scope.isBusySaving = false;
            });
        };
        
        $scope.deleteComment = function (comment) {
            if (confirm("Are you sure you want to delete the comment?")) {
                commentsService.delete(comment).then(function (result) {
                    $scope.comments = result;
                    $scope.commentsUpdated({ comments: $scope.comments });
                });
            }
        };
        
        self.emptyComment = function () {
            return {
                Entity: $scope.entity,
                EntityId: $scope.id,
                UserId: $scope.currentUser.User.Id
            };
        };

        self.init = function () {

            var promises = [
                commentsService.get($scope.entity, $scope.id),
                authenticationService.getLoggedInUser()
            ];

            $q.all(promises).then(function(results) {
                $scope.comments = results[0];
                $scope.currentUser = results[1];

                $scope.commentsUpdated({ comments: $scope.comments });
                $scope.newComment = self.emptyComment();

                $scope.isInitialized = true;
            });
        };
        
        self.init();
    }
]);