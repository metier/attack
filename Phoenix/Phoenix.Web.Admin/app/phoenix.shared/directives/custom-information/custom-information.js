﻿angular.module('phoenix.shared').directive('customInformation', [
    'phoenix.config',
    function(config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                label: '@',
                entityId: '@',
                entityType: '@',
                onSavedEvent: '&',
                onCancelledEvent: '&'
            },
            templateUrl: config.basePath + 'app/phoenix.shared/directives/custom-information/custom-information.html',
            controller: 'CustomInformationCtrl'
        };
    }
]);