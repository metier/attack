/*global angular*/
angular.module('phoenix.shared').controller('CustomInformationCtrl', [
    '$scope',
    '$q',
    'CustomInformationService',
    function($scope, $q, customInformationService) {
        'use strict';
        $scope.isInitialized = false;
        $scope.customInformationSet = [];
        var self = this;

        $scope.cancel = function () {
            self.getCustomInformation(self.setIsBusy);
            $scope.onCancelledEvent();
        }

        $scope.deleteRow = function(customInfoId) {
            var index = _.findIndex($scope.customInformationSet, function(infoElement) { return infoElement.Id === customInfoId });
            $scope.customInformationSet.splice(index, 1);
        }

        $scope.deleteAll = function() {
            $scope.isBusy = true;

            customInformationService.deleteAll($scope.entityType, $scope.entityId).then(function() {
                    $scope.customInformationSet = [];
                    $scope.isBusy = false;
                }, function() {
                    $scope.isBusy = false;
                }
            );
        }

        $scope.addRow = function() {
            var newId = ($scope.customInformationSet.length + 1) * (-1);
            $scope.customInformationSet.push({ Id: newId, EntityType: $scope.entityType, EntityId: $scope.entityId });
        }

        $scope.save = function() {
            $scope.isBusy = true;

            _.each($scope.customInformationSet, function(customInfo) {
                if (customInfo.Id < 0) {
                    customInfo.Id = null;
                }
            });

            customInformationService.save($scope.customInformationSet, $scope.entityType, $scope.entityId).then(function(result) {
                    $scope.customInformationSet = result;
                    $scope.isBusy = false;
                    $scope.onSavedEvent();

                }, function() {
                    $scope.isBusy = false;
                }
            );
        }

        self.getCustomInformation = function (isWorking) {
            isWorking(true);

            customInformationService.get($scope.entityType, $scope.entityId).then(function(result) {
                    $scope.customInformationSet = result;
                    isWorking(false);
                },
                function() {
                    isWorking(false);
                });
        }

        self.init = function() {
            self.getCustomInformation(self.setIsInitialized);
        }

        self.setIsBusy = function (value) {
            $scope.isBusy = value;
        }

        self.setIsInitialized = function(value)
        {
            $scope.isInitialized = !value;
        }

        self.init();
    }
]);