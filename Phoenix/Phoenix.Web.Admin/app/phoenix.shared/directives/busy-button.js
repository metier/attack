﻿angular.module('phoenix.shared').directive('busyButton', function () {
    'use strict';
    return {
        restrict: 'A',
        scope: {
            isBusy: '=',
            busyText: '@'
        },
        link: function (scope, el, attrs) {
            var html = el[0].innerHTML;
            scope.$watch('isBusy', function (newValue) {
                if (angular.isDefined(attrs.busyText)) {
                    if (newValue === true) {
                        el[0].innerHTML = attrs.busyText;
                        el.addClass('disabled');
                    } else {
                        el[0].innerHTML = html;
                        el.removeClass('disabled');
                    }
                }    
            });
        }
    };
});