﻿/*global angular*/
angular.module('phoenix.shared').controller('UserCommentsCtrl', [
    '$scope',
    '$q',
    'CommentsService',
    'AuthenticationService',
    'ParticipantsService',
    function ($scope, $q, commentsService, authenticationService, participantsService) {
        'use strict';
        var self = this;
        $scope.isInitialized = false;
        $scope.isBusySaving = false;

        function getComments() {
            if ($scope.entity === 'user')
                return commentsService.getUserComments($scope.id, true, true);

            if ($scope.entity === 'enrollment')
                return commentsService.getEnrollmentComments($scope.id, true, true);

            return commentsService.getParticipationComments($scope.id, true, true);
        }

        function getParticipations() {
            if ($scope.entity === 'user')
                return participantsService.search({ userId: $scope.id, limit: -1 }); 

            if ($scope.entity === 'participant')
                return participantsService.getSearchableParticipant($scope.id);

            if ($scope.entity === 'enrollment')
                return participantsService.getSearchableParticipantForEnrollment($scope.id);

            return null;
        }

        $scope.getActivity = function (comment) {
            if(comment.Entity === "participant"){
                var participant = _.findWhere($scope.participations, { Id: comment.EntityId });
                var activity = {};
                activity.CustomerId = participant.ActivityOwnerCustomerId;
                activity.ActivityId = participant.ActivityId;
                activity.Name = participant.ActivityName;

                return activity;
            }

            return null;
        }

        $scope.getActivitySet = function (comment) {
            if (comment.Entity === "enrollment") {
                var participant = _.findWhere($scope.participations, { EnrollmentId: comment.EntityId });
                var activityset = {};
                activityset.Id = participant.EnrolledActivitySetId;
                activityset.Name = participant.EnrolledActivitySetName;

                return activityset;
            }

            return null;
        }

        $scope.addComment = function () {
            $scope.isBusySaving = true;
            commentsService.create($scope.newComment).then(function () {
                $scope.isBusySaving = false;
                getComments().then(function(result){
                    $scope.comments = result;
                    $scope.commentsUpdated({ comments: $scope.comments });
                    $scope.newComment = self.emptyComment();
                });
            }, function () {
                $scope.isBusySaving = false;
            });
        };
        
        $scope.deleteComment = function (comment) {
            if (confirm("Are you sure you want to delete the comment?")) {
                commentsService.delete(comment).then(function () {
                    getComments().then(function (result) {
                        $scope.comments = result;
                        $scope.commentsUpdated({ comments: $scope.comments });
                    });
                });
            }
        };
        
        self.emptyComment = function () {
            return {
                Entity: $scope.entity,
                EntityId: $scope.id,
                UserId: $scope.currentUser.User.Id
            };
        };

        $scope.close = function () {
            $scope.modalClosed();
        };


        self.init = function () {

            var promises = [
                getComments(),
                authenticationService.getLoggedInUser(),
                getParticipations()
            ];

            $q.all(promises).then(function(results) {
                $scope.comments = results[0];
                $scope.currentUser = results[1];

                if ($scope.entity === "user")
                    $scope.participations = (results[2].Items);
                else if ($scope.entity === "enrollment")
                    $scope.participations = (results[2]);
                else if ($scope.entity === "participant")
                    $scope.participations = ([results[2]]);

                $scope.commentsUpdated({ comments: $scope.comments });
                $scope.newComment = self.emptyComment();

                $scope.isInitialized = true;
            });
        };
        
        self.init();
    }
]);