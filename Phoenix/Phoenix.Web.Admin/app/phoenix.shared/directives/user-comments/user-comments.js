﻿/*global angular*/
angular.module('phoenix.shared').directive('usercomments', [
    'phoenix.config',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                entity: '@',
                id: '@',
                header:'@',
                showCloseButton: '@',
                modalClosed: '&',
                commentsUpdated: '&'
            },
            templateUrl: config.basePath + 'app/phoenix.shared/directives/user-comments/user-comments.html',
            controller: 'UserCommentsCtrl'
        };
    }
]);