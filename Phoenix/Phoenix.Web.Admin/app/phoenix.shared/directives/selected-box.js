﻿angular.module('phoenix.shared').directive('selectedBox', [function () {
    'use strict';
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            onRemove: '&'  
        },
        template: '<div class="selected-box">' +
                    '<span ng-transclude></span>' +
                    '   <i class="fa fa-times clickable" title="Remove" ng-click="onRemove()"></i>' +
                  '</div>'
    };
}]);