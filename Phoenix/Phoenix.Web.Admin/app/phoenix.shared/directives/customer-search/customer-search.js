angular.module('phoenix.shared').directive('customerSearch', [
    'phoenix.config',
    function(config) {
        'use strict';
        return {
            restrict: 'E',
            templateUrl: config.basePath + 'app/phoenix.shared/directives/customer-search/customer-search.html',
            controller: 'CustomerSearchCtrl',
            scope: {
                selectedCustomerId: '=?'
            },
            link: function(scope, element, attrs){
                
                scope.tooltipPlacement = "right";
                scope.isRequired = false;
                scope.searchAllDistributors = false;
                scope.clearOnSelect = false;

                if (attrs.tooltipAlignLeft === "" || attrs.tooltipAlignLeft === "left")
                    scope.tooltipPlacement = "left";

                if (attrs.clearOnSelect === "" || attrs.clearOnSelect === "true")
                    scope.clearOnSelect = true;

                if (attrs.searchAllDistributors === "" || attrs.searchAllDistributors === "true")
                    scope.searchAllDistributors = true;

                if (attrs.isRequired === "" || attrs.isRequired === "true")
                    scope.isRequired = true;
            }
        };
    }
]);

angular.module('phoenix.shared').controller('CustomerSearchCtrl', [
    'phoenix.config',
    '$scope',
    'CustomersService',
    function (config, $scope, customersService) {
        'use strict';

        $scope.isSearching = false;
        var customerSearchItemTooltipTemplateUrl = ['<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div>',
            '<div class="tooltip-inner"></div>',
            '</div>'].join('');

        self.init = function() {

            if ($scope.selectedCustomerId) {
                self.loadCustomer($scope.selectedCustomerId);
            };
        };

        self.loadCustomer = function(customerId){
            if(customerId){
                customersService.get(customerId).then(function (result) {
                    $scope.customer = result;
                });
            }
            else{
                $scope.customer = null;
            };
        };

        $scope.searchCustomer = function (searchString) {
            $scope.isSearching = true;
            return customersService.search({ query: searchString, limit: 50, searchAllDistributors: $scope.searchAllDistributors }).then(function (result) {
                _(result.Items).each(function(item) {
                    item.customerSearchItemTooltipTemplateUrl = customerSearchItemTooltipTemplateUrl;
                    item.tooltipPlacement = $scope.tooltipPlacement;
                    item.tooltipContent = ['<div style="white-space: nowrap;text-align: left;"><div>ID: ',
                        item.Id, '</div>',
                        '<div>Accounting ID: ', item.ExternalCustomerId ,'</div>',
                        '<div>Language: ', item.Language, '</div></div>'].join('');
                });
                return result.Items;
            }).finally(function() {
                $scope.isSearching = false;
            });
        };


        $scope.customerSearchTemplateUrl = config.basePath + 'app/phoenix.shared/directives/customer-search/customer-search-result-template.html';

        $scope.onCustomerSelected = function () {
            $scope.selectedCustomerId = $scope.customer.Id;
            if($scope.clearOnSelect === true){
                $scope.customer = null;
            }
        }

        $scope.setValidSelectedCustomer = function () {
            if (!$scope.customer || $scope.customer === "") {
                $scope.selectedCustomerId = null;
            }
            else if (!$scope.customer.Id) {
                self.loadCustomer($scope.selectedCustomerId);
            }
            else {
                $scope.selectedCustomerId = $scope.customer.Id;
            }
        }

        self.init();
    }
]);