﻿/*global angular */
/*jslint maxlen: 150 */
angular.module('phoenix.shared').directive('sortableHeader', ['phoenix.config', function (config) {
    'use strict';
    return {
        scope: {
            title: '@',
            orderBy: '@',
            currentOrderBy: '=',
            currentOrderDirection: '='
        },
        restrict: 'EA',
        replace: true,
        templateUrl: config.basePath + 'app/phoenix.shared/directives/sortable-header/sortable-header.html',
        controller: ['$scope', '$location', function ($scope, $location) {
            var self = this;
            
            $scope.orderDescending = function () {
                self.order('desc');
            };

            $scope.orderAscending = function () {
                self.order('asc');
            };
            
            $scope.isOrderedDescending = function () {
                return self.isOrderedBy($scope.orderBy, 'desc');
            };

            $scope.isOrderedAscending = function () {
                return self.isOrderedBy($scope.orderBy, 'asc');
            };
            
            $scope.isNotOrdered = function () {
                return !$scope.isOrderedAscending() && !$scope.isOrderedDescending();
            };
            
            self.isOrderedBy = function (orderBy, orderDirection) {
                return $scope.currentOrderBy === orderBy && $scope.currentOrderDirection === orderDirection;
            };
            
            self.order = function (orderDirection) {
                $location.search('orderBy', $scope.orderBy);
                $location.search('orderDirection', orderDirection);
            };
        }]
    };
}]);