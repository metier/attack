﻿angular.module('phoenix.shared').directive('enrollmentConditions', [
    'phoenix.config',
    'ActivitiesService',
    function(config, enrollmentConditionsService) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                entityId: '=activityId'
            },
            templateUrl: config.basePath + 'app/phoenix.shared/directives/enrollment-conditions/enrollment-conditions.html',
            controller: 'EnrollmentConditionsCtrl',
            link: function (scope, element, attrs, controller, transcludeFn) {
                controller.init(enrollmentConditionsService);
            }
        };
    }
]);