/*global angular*/
angular.module('phoenix.shared').controller('EnrollmentConditionsCtrl', [
    '$scope',
    '$q',
    function ($scope, $q) {
        'use strict';
        var enrollmentConditionsService;
        $scope.enrollmentConditions = [];
        $scope.isInitialized = false;
        $scope.isEditMode = false;
        var self = this;

        $scope.save = function() {
            $scope.isBusy = true;

			var validationErrors = "";
            _.each($scope.enrollmentConditions, function(condition) {
                condition.Language = null;
                condition.ArticleType = null;
                condition.Product = null;
                condition.Customer = null;

                if (condition.Id < 0) {
                    condition.Id = null;
				}
				if (!condition.Title) validationErrors += "\n* Title is required";
				if (!condition.ProductId) validationErrors += "\n* Product must be selected";
				if (!condition.ArticleTypeId) validationErrors += "\n* Article type must be selected";				
            });

			if (validationErrors)
			{
				alert("Enrollment conditions could not be saved. Please correct the issues below and try saving again.\n" + validationErrors);
				self.setIsBusy(false);
				return;
			}

            enrollmentConditionsService.saveEnrollmentConditions($scope.enrollmentConditions, $scope.entityId).then(function() {
                self.getEnrollmentConditions($scope.entityId, self.setIsBusy);
                    $scope.setEditMode(false);
                }, function() {
                    self.setIsBusy(false);
                }
            );
        }

        $scope.deleteRow = function(conditionId) {
            var index = _.findIndex($scope.enrollmentConditions, function(condition) { return condition.Id === conditionId });
            $scope.enrollmentConditions.splice(index, 1);
        }

        $scope.cancel = function () {
            self.getEnrollmentConditions($scope.entityId, self.setIsBusy);
            $scope.setEditMode(false);
        }

        $scope.addRow = function() {
            var newId = ($scope.enrollmentConditions.length + 1) * (-1);
            $scope.enrollmentConditions.push({ Id: newId });
        }

        $scope.setEditMode = function(editMode) {
            if (!editMode)
                $scope.isEditMode = false;
            else
                $scope.isEditMode = editMode;
        }

        self.getEnrollmentConditions = function (entityId, isWorking) {

            if (entityId) {
                isWorking(true);
                enrollmentConditionsService.getEnrollmentConditions(entityId).then(function (result) {
                        $scope.enrollmentConditions = result;
                        isWorking(false);
                    },
                    function() {
                        isWorking(false);
                    });
            }
        }

        self.init = function (service) {
            $scope.setEditMode($scope.isEditMode);
            enrollmentConditionsService = service;
            self.getEnrollmentConditions($scope.entityId, self.setIsInitialized);

            $scope.isInitialized = true;
        }

        self.setIsBusy = function (value) {
            $scope.isBusy = value;
        }

        self.setIsInitialized = function(value)
        {
            $scope.isInitialized = !value;
        }

    }
]);