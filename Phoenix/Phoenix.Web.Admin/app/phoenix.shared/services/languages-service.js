﻿/*global angular */
/*jslint maxlen: 250 */
angular.module('phoenix.shared').factory('LanguagesService', ['$http', '$q', 'phoenix.config', '$cacheFactory', function ($http, $q, config, $cacheFactory) {
    'use strict';
    var self = this;
    self.cache = $cacheFactory("languagescache");
    self.getAll = function () {
        var deferred = $q.defer(),
            cached = self.cache.get('languages');
        if (cached) {
            deferred.resolve(cached);
        } else {
            $http.get(config.apiPath + 'languages').success(function (result) {
                self.cache.put('languages', result);
                deferred.resolve(result);
            }).error(function (error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    };
    
    return {
        getAll: self.getAll
    };
}]);