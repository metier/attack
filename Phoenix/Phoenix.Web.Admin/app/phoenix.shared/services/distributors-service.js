﻿/*global $ */
angular.module('phoenix.shared').factory('DistributorsService', [
    '$q',
    '$http',
    'phoenix.config',
    'asyncHelper',
    function ($q, $http, config, asyncHelper) {
        'use strict';
        var cookieKey = 'distributor';
        var myMetierCookieKey = 'DistributorId';
        var myMetierCookieObject = 'distributor-mymetier';
        return {
            get: function() {
                var cookie = $.cookie(cookieKey);
                var myMetierCookie = this.getDistributorObject();
                if (cookie) {
                    return JSON.parse(cookie);
                } else if (myMetierCookie) {
                    return JSON.parse(myMetierCookie);
                }
                return null;
            },
            getDistributorID: function () {
                try {
                    var myMetierCookie = $.cookie(myMetierCookieKey);
                    if (myMetierCookie) {
                        return JSON.parse(myMetierCookie);
                    }
                    return null;
                } catch (ex) {
                    return null;
                }
            },
            getDistributorObject: function () {
                try {
                    var myMetierCookie = $.cookie(myMetierCookieObject);
                    if (myMetierCookie) {
                        return myMetierCookie;
                    }
                    return null;
                } catch (ex) {
                    return null;
                }
            },
            set: function(distributor) {
                $.cookie(cookieKey, JSON.stringify(distributor), { path: '/' });
            },
            getAll: function() {
                var deferred = $q.defer(),
                    url = config.apiPath + 'distributors';

                asyncHelper.resolveHttp($http.get(url)).then(function(distributors) {
                    deferred.resolve(distributors);
                });

                return deferred.promise;
            }
        };
    }
]);