﻿/*global angular */
angular.module('phoenix.shared').factory('AuthenticationService', [
    '$http',
    '$rootScope',
    'phoenix.config',
    'asyncHelper',
    function ($http, $rootScope, config, asyncHelper) {
        'use strict';

        return {
            login: function(username, password) {
                var url = config.apiPath + 'accounts/' + username + '/logon';
                return asyncHelper.resolveHttp($http.post(url, { Password: password }));
            },
            logout: function() {
                var url = config.apiPath + 'accounts/logout',
                    promise = asyncHelper.resolveHttp($http.post(url));

                $rootScope.$broadcast('userLoggedOut');
                return promise;
            },
            getLoggedInUser: function () {
                var url = config.apiPath + 'accounts?username=';
                return asyncHelper.resolveHttp($http.get(url));
            }
        };
    }
]);