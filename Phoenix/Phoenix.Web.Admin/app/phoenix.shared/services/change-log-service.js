﻿/*global angular */
angular.module('phoenix.shared').factory('ChangeLogService', [
    '$http',
    'phoenix.config',
    'asyncHelper',
    function ($http, config, asyncHelper) {
        'use strict';
        return {
            get: function (options) {
                var url = config.apiPath + 'changelog',
                    parameters;

                parameters = {
                    entityId: options.entityId,
                    entityType: options.entityType
                };

                if (typeof options.limit === 'number') {
                    parameters.limit = options.limit;
                }
                if (typeof options.page === 'number' && options.page > 0) {
                    parameters.skip = (options.page - 1) * parameters.limit;
                }
                return asyncHelper.resolveHttp($http.get(url, { params: parameters }));
            }
        };
}]);