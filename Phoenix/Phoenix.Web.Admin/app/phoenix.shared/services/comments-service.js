﻿/*global angular */
angular.module('phoenix.shared').factory('CommentsService', [
    '$http',
    'phoenix.config',
    'asyncHelper',
    function ($http, config, asyncHelper) {
        'use strict';
        
        return {
            get: function (entity, entityId) {
                var url = config.apiPath + 'comments',
                    parameters;
                
                parameters = {
                    entity: entity,
                    entityId: entityId
                };
                
                return asyncHelper.resolveHttp($http.get(url, { params: parameters }));
            },
            getAll: function (entity, entityIds) {
                var url = config.apiPath + 'comments';
                
                var parameters = {
                    entity: entity,
                    entityIds: entityIds
                };
                
                return asyncHelper.resolveHttp($http.get(url, { params: parameters }));
            },
            getUserComments: function (userId, includeEnrollmentComments, includeParticipantComments) {
                var url = config.apiPath + 'usercomments';
                console.log(url);
                var parameters = {
                    userId: userId,
                    includeEnrollmentComments: includeEnrollmentComments,
                    includeParticipantComments: includeParticipantComments
                };
                return asyncHelper.resolveHttp($http.get(url, { params: parameters }));
            },
            getParticipationComments: function (participantId, includeUserComments, includeEnrollmentComments) {
                var url = config.apiPath + 'comments';
                
                var parameters = {
                    participantId: participantId,
                    includeUserComments: includeUserComments,
                    includeEnrollmentComments: includeEnrollmentComments
                };
                
                return asyncHelper.resolveHttp($http.get(url, { params: parameters }));
            },
            getEnrollmentComments: function (enrollmentId, includeUserComments, includeParticipantComments) {
                var url = config.apiPath + 'enrollmentcomments';
                
                var parameters = {
                    enrollmentId: enrollmentId,
                    includeUserComments: includeUserComments,
                    includeParticipantComments: includeParticipantComments
                };
                
                return asyncHelper.resolveHttp($http.get(url, { params: parameters }));
            },
            getAllParticipantComments: function (activityId, includeUserComments, includeEnrollmentComments) {
                var url = config.apiPath + 'comments';
                
                var parameters = {
                    activityId: activityId,
                    includeUserComments: includeUserComments,
                    includeEnrollmentComments: includeEnrollmentComments
                };
                
                return asyncHelper.resolveHttp($http.get(url, { params: parameters }));
            },
            getAllEnrollmentComments: function (activitySetId, includeUserComments, includeParticipantComments) {
                var url = config.apiPath + 'activitysetcomments';

                var parameters = {
                    activitySetId: activitySetId,
                    includeUserComments: includeUserComments,
                    includeParticipantComments: includeParticipantComments
                };

                return asyncHelper.resolveHttp($http.get(url, { params: parameters }));
            },
            create: function (comment) {
                var url = config.apiPath + 'comments';
                return asyncHelper.resolveHttp($http.post(url, comment));
            },
            delete: function (comment) {
                var url = config.apiPath + 'comments/' + comment.Id;
                return asyncHelper.resolveHttp($http.delete(url));
            }
        };
    }
]);