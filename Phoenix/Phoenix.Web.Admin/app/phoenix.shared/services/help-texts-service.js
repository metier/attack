﻿/*global angular */
/*jslint maxlen: 150 */
angular.module('phoenix.shared').factory('HelpTextsService', [
    '$http',
    '$q',
    'phoenix.config',
    'asyncHelper',
    function ($http, $q, config, asyncHelper) {
        'use strict';
        return {
            get: function (section, name) {
                var deferred = $q.defer(),
                    url = config.apiPath + 'helptexts',
                    parameters = {
                        section: section,
                        name: name
                    };
            
                $http.get(url, { params: parameters }).success(function (result) {
                    deferred.resolve(result);
                }).error(function (error, statusCode) {
                    if (statusCode === 404) {
                        deferred.resolve();
                    } else {
                        deferred.reject(error);
                    }
                });
                return deferred.promise;
            },
            create: function (helpText) {
                var url = config.apiPath + 'helptexts';
                return asyncHelper.resolveHttp($http.post(url, helpText));
            },
            update: function (helpText) {
                var url = config.apiPath + 'helptexts/' + helpText.Id;
                return asyncHelper.resolveHttp($http.put(url, helpText));
            }
        };
}]);