﻿angular.module('phoenix.shared').factory('ExamsService', [
    '$http',
    'phoenix.config',
    'asyncHelper',
    function($http, config, asyncHelper) {
        'use strict';
        return {
            getExamResult: function(participantId) {
                var url = config.apiPath + 'exams/result?participantId=' + participantId;
                return asyncHelper.resolveHttp($http.get(url));
            },
            unsubmitExam: function (id) {
                return asyncHelper.resolveHttp($http.put(config.apiPath + 'exams/' + id + '/unsubmit'), false);
            },
            setTotalScore: function (examId, options) {
                var url = config.apiPath + 'exams/' + examId + '/score';
                var params = {
                    totalScore: options.totalScore,
                    isOverridden: options.isOverridden
                };
                return asyncHelper.resolveHttp($http.put(url, null, {params: params}));
            },
            overrideTotalScore: function(examId, totalScore) {
                return this.setTotalScore(examId, { totalScore: totalScore, isOverridden: true });
            },
            resetTotalScoreToCalculated: function(examId) {
                return this.setTotalScore(examId, { isOverridden: false });
            }
        };
    }
]);