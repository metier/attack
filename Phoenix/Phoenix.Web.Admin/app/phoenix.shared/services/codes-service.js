﻿/*global angular */
/*jslint maxlen: 170 */
angular.module('phoenix.shared').factory('CodesService', [
    '$http',
    '$q',
    '$log',
    '$cacheFactory',
    'phoenix.config',
    'asyncHelper',
    function ($http, $q, $log, $cacheFactory, config, asyncHelper) {
        'use strict';
        var self = this;
        self.cache = $cacheFactory("codescache");

        self.getCodes = function (type) {
            var url = config.apiPath + 'codes';
            return asyncHelper.resolveHttp($http.get(url, { params: { type: type } }));
        };

        return {
            getAll: function () {
                var defer = $q.defer(),
                    cachedCodes = self.cache.get('codes');
                if (cachedCodes) {
                    defer.resolve(cachedCodes);
                } else {
                    $q.all([
                        self.getCodes('articlestatuses'),
                        self.getCodes('countrycodes'),
                        self.getCodes('currencycodes'),
                        self.getCodes('participantstatuses')
                    ]).then(function (data) {
                        var codes = {
                            articleStatuses: data[0],
                            countryCodes: data[1],
                            currencyCodes: data[2],
                            participantStatuses: data[3]
                        };
                        self.cache.put('codes', codes);
                        defer.resolve(codes);
                    }, function (error) {
                        defer.reject(error);
                    });
                }
                return defer.promise;
            }
        };
}]);