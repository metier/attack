﻿/*global angular, toastr */
/*jslint maxlen: 150 */
angular.module('phoenix.shared').factory('NotificationService', [function () {
    'use strict';
    function overrideOptions(options) {
        toastr.options.timeOut = options.timeOut || 5000;
        toastr.options.closeButton = options.closeButton || true;
        toastr.options.positionClass = options.positionClass || 'toast-bottom-right';
    }

    return {
        // Options: header, message
        alertError: function (options) {
            overrideOptions(options);
            toastr.error(options.message, options.header);
        },
        // Options: header, message
        alertWarning: function (options) {
            overrideOptions(options);
            toastr.warning(options.message, options.header);
        },
        // Options: header, message
        alertInfo: function (options) {
            overrideOptions(options);
            toastr.info(options.message, options.header);
        },
        // Options: header, message
        alertSuccess: function (options) {
            overrideOptions(options);
            toastr.success(options.message, options.header);
        }
    };
}]);