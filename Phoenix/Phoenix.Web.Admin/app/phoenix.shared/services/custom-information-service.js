﻿/*global angular */
angular.module('phoenix.shared').factory('CustomInformationService', [
    '$http',
    'phoenix.config',
    'asyncHelper',
    function ($http, config, asyncHelper) {
        'use strict';
        
        return {
            get: function (entityType, entityId) {

                var url = config.apiPath + 'custominformation';
                
                var parameters = {
                    entitytype: entityType,
                    entityid: entityId
                };
                
                return asyncHelper.resolveHttp($http.get(url, { params: parameters }));
            }
            ,
            save: function (customInformationSet, entityType, entityId) {
                var url = config.apiPath + 'custominformation' + "/" + entityType + "/" + entityId;

                return asyncHelper.resolveHttp($http.put(url, customInformationSet));
            },
            delete: function (customInformationId) {
                var url = config.apiPath + 'custominformation/' + customInformationId;
                return asyncHelper.resolveHttp($http.delete(url));
            },
            deleteAll: function (entityType, entityId) {

                var url = config.apiPath + 'custominformation';

                var parameters = {
                    entitytype: entityType,
                    entityid: entityId
                };

                return asyncHelper.resolveHttp($http.delete(url, { params: parameters }));
            }
        };
    }
]);