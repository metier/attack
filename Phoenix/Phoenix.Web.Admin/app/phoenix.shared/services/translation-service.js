﻿angular.module('phoenix.shared').factory('TranslationService', [
    '$http',
    '$q',
    'phoenix.config',
    function ($http, $q, config) {
        'use strict';
        var self = this;
        self.defaultLanguage = 'en';
        self.cache = [];

        function getTranslationUrl(language) {
            return config.basePath + 'app/assets/translations/translations_' + language + '.json';
        }

        function getCachedLanguage(language) {
            return _(self.cache).find(function(c) {
                return c.language === language.toLowerCase();
            });
        }

        function getTranslation(key, language) {
            var defered = $q.defer();
            var cached = getCachedLanguage(language);
            if (cached) {
                defered.resolve(cached.data);
            } else {
                
                $http.get(getTranslationUrl(language.toLowerCase())).success(function (result) {
                    self.cache.push({ language: language.toLowerCase(), data: result });
                    defered.resolve(result);
                }).error(function (error, status) {
                    if (status === 404 && language.toLowerCase() !== self.defaultLanguage) {
                        // Language file was not found. Retry with default language.
                        var defaultCached = getCachedLanguage(self.defaultLanguage);
                        if (defaultCached) {
                            defered.resolve(defaultCached.data);
                        } else {
                            $http.get(getTranslationUrl(self.defaultLanguage)).success(function (result) {
                                self.cache.push({ language: self.defaultLanguage, data: result });
                                defered.resolve(result);
                            }).error(function() {
                                defered.reject();
                            });
                        }

                    } else {
                        defered.reject();
                    }
                });
            }

            return defered.promise;
        }
        
        return {
            translate: function (key, language) {
                var deferred = $q.defer();
                
                getTranslation(key, language).then(function (result) {
                    deferred.resolve(result[key]);
                }, function () {
                    deferred.reject();
                });

                return deferred.promise;
            }
        };
    }
]);