﻿angular.module('phoenix.shared').factory('errorHandlingHttpInterceptor', [
    '$q',
    '$injector',
    '$location',
    'NotificationService',
    function ($q, $injector, $location, notificationService) {
        'use strict';
        return {
            'responseError': function (rejection) {
                var $state = $injector.get('$state');

                if (rejection.status === 401) {
                    if (!$state.is('login') && !$state.is('iforgot')) {
                        $state.go('login', { returnUrl: $location.absUrl() });
                    }
                }

                if (rejection.config.interceptErrors !== undefined && !rejection.config.interceptErrors) {
                    return $q.reject(rejection);
                }

                // If "forwardErrorCodes" is defined (array or number), check if it matches 
                // the rejection's error code and then stop interception
                if (rejection.config.forwardErrorCodes !== undefined) {
                    var errorCode = rejection.data.ErrorCode;
                    if (angular.isArray(rejection.config.forwardErrorCodes)) {
                        if (_(rejection.config.forwardErrorCodes).some(function(ec) { return ec === errorCode; })) {
                            return $q.reject(rejection);
                        }
                    } else if (angular.isNumber(rejection.config.forwardErrorCodes)) {
                        if (rejection.config.forwardErrorCodes === errorCode) {
                            return $q.reject(rejection);
                        }
                    }
                }
                
                // Notify on bad requests with validation errors
                if (rejection.status === 400) {
                    if (rejection.data.ValidationErrors && rejection.data.ValidationErrors.length > 0) {
                        _(rejection.data.ValidationErrors).each(function(error) {
                            notificationService.alertError({ message: error, header: 'Validation Error' });
                        });
                    } else if (rejection.data.Message) {
                        notificationService.alertError({ message: rejection.data.Message });
                    }
                }

                if (rejection.status === 409) {
                    notificationService.alertError({
                        message: 'Your data is out of date and cannot be saved. Please refresh and try to save again.',
                        header: 'Old data'
                    });
                }

                if (rejection.status === 500) {
                    notificationService.alertError({
                        message: 'An unexpected error occured!'
                    });
                }

                return $q.reject(rejection);
            }
        };
    }
]);