﻿angular.module('phoenix.customers').controller('CustomerArticleEditCtrl', [
    '$scope',
    '$state',
    'data',
    'codes',
    'CustomerArticlesService',
    'ArticlesService',
    function ($scope, $state, data, codes, customerArticlesService, articlesService) {
        'use strict';

        $scope.customerArticle = angular.copy(data.customerArticle);
        $scope.article = data.article;
        $scope.currencies = codes.currencyCodes;
        $scope.busySaving = false;
        $scope.selectedEducationalPartner = { Id: $scope.customerArticle.EducationalPartnerId };

        $scope.onArticleSaved = function () {
            $scope.busySaving = true;
            customerArticlesService.update($scope.customerArticle).then(function (customerArticle) {
                data.customerArticle = customerArticle;
                $scope.customerArticle = angular.copy(data.customerArticle);
                articlesService.get($scope.customerArticle.ArticleCopyId).then(function (article) {
                    data.article = article;
                    $scope.article = data.article;

                    $scope.busySaving = false;
                    history.back();
                });
            }, function () {
                $scope.busySaving = false;
            });

        };

        $scope.educationalPartnerChanged = function() {
            $scope.customerArticle.EducationalPartnerId = $scope.selectedEducationalPartner.Id;
        }

        $scope.onToggleSelection = function (control) {
            if (control === "FixedPrice")
                $scope.customerArticle.FixedPriceNotBillable = !$scope.customerArticle.FixedPriceNotBillable;

            if (control === "UnitPrice")
                $scope.customerArticle.UnitPriceNotBillable = !$scope.customerArticle.UnitPriceNotBillable;
        }


        $scope.onArticleCancelled = function () {
            $scope.customerArticle = angular.copy(data.customerArticle);
            history.back();
        };

        $scope.getCurrencyCode = function (id) {
            return _($scope.currencies).find(function (currency) {
                return currency.Id === id;
            }).Value;
        };

        $scope.$on('$stateChangeSuccess', function () {
            if ($state.is('customer.articles.item.view')) {
                $scope.customerArticle = angular.copy(data.customerArticle);
            }
            $scope.isEditing = $state.is('customer.articles.item.edit');
        });
    }
]);