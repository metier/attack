/*jslint nomen: true*/
/*global angular, _ */
/*jslint maxlen: 150 */
angular.module('phoenix.customers').controller('CustomerContactPersonsCtrl', ['$scope', 'UsersService', function ($scope, usersService) {
    'use strict';
    var self = this;
    $scope.addCustomerContactPerson = function () {
        $scope.customer.CustomerContactPersons.push({ IsDeleted: false });
    };

    $scope.removeCustomerContactPerson = function (contactPerson) {
        if (contactPerson.Id) {
            contactPerson.IsDeleted = true;
        } else {
            var index = $scope.customer.CustomerContactPersons.indexOf(contactPerson);
            if (index > -1) {
                $scope.customer.CustomerContactPersons.splice(index, 1);
            }
        }
    };

    $scope.searchUser = function (searchString) {
        return usersService.search({ query: searchString, limit: 20, phoenixUsersOnly: true }).then(function (result) {
            return result.Items;
        });
    };

    $scope.onUserSelected = function (user, metierContactType) {
        if (user) {
            var metierContactPerson = self.resolveMetierContactPerson(metierContactType);

            if (!metierContactPerson) {
                metierContactPerson = {
                    ContactType: metierContactType
                };
                $scope.customer.MetierContactPersons.push(metierContactPerson);
            }

            metierContactPerson.UserId = user.Id;
            metierContactPerson.User = user;
            metierContactPerson.IsDeleted = false;
        }
    };

    $scope.setValidUser = function (user, metierContactType) {
        var metierContactPerson = self.resolveMetierContactPerson(metierContactType);

        if (!user && metierContactPerson) {
            if (!metierContactPerson.Id) {
                var index = $scope.customer.MetierContactPersons.indexOf(metierContactPerson);
                $scope.customer.MetierContactPersons.splice(index, 1);
            } else {
                metierContactPerson.IsDeleted = true;
            }
        }

        switch (metierContactType) {
            case 0:
                $scope.accountManager = user ? metierContactPerson.User : null;
                break;
            case 1:
                $scope.programCoordinator = user ? metierContactPerson.User : null;
                break;
            case 2:
                $scope.examCoordinator = user ? metierContactPerson.User : null;
                break;
            case 3:
                $scope.subjectMatterExpert = user ? metierContactPerson.User : null;
                break;
        }
    };
    
    self.resolveMetierContactPerson = function(metierContactType) {
        return _($scope.customer.MetierContactPersons).find(function(contactPerson) {
            return (contactPerson.ContactType === metierContactType && !contactPerson.IsDeleted);
        });
    };

    self.setMetierContactPersonToProperty = function (metierContactType) {
        var metierContactPerson = self.resolveMetierContactPerson(metierContactType);

        if (metierContactPerson) {
            metierContactPerson.User = null;
            usersService.getUserAccountByUserId(metierContactPerson.UserId).then(function (result) {
                metierContactPerson.User = result.User;
                switch (metierContactType) {
                    case 0:
                        $scope.accountManager = result.User;
                        break;
                    case 1:
                        $scope.programCoordinator = result.User;
                        break;
                    case 2:
                        $scope.examCoordinator = result.User;
                        break;
                    case 3:
                        $scope.subjectMatterExpert = result.User;
                        break;
                }
            });
        }
    };

    self.init = function () {
        var contactPersons = _.where($scope.customer.CustomerContactPersons, { IsDeleted: false });

        if (contactPersons.length === 0) {
            $scope.addCustomerContactPerson();
        }

        self.setMetierContactPersonToProperty(0);
        self.setMetierContactPersonToProperty(1);
        self.setMetierContactPersonToProperty(2);
        self.setMetierContactPersonToProperty(3);
    };

    self.init();
}]);