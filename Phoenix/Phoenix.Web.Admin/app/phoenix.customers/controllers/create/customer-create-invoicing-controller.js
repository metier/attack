/*global angular */
/*jslint maxlen: 150 */
angular.module('phoenix.customers').controller('CustomerInvoicingCtrl', ['$scope', function ($scope) {
    'use strict';
    var self = this;
    
    $scope.groupByOrderRefChanged = function (option) {
        $scope.customer.CustomerInvoicing.GroupByOrderRef = option === 1;
        $scope.customer.CustomerInvoicing.GroupByParticipants = option === 2;
        $scope.customer.CustomerInvoicing.BundledBilling = option === 3;
    };

    $scope.groupByParticipantChanged = function () {
        $scope.customer.CustomerInvoicing.BundledBilling = !$scope.isGroupByParticipant;

        if (!$scope.customer.CustomerInvoicing.BundledBilling) {
            _($scope.customer.CustomLeadingTexts).each(function (customLeadingText) {
                customLeadingText.IsGroupByOnInvoice = false;
            });
        } else {
            $scope.customer.CustomerInvoicing.OrderRefLeadingTextId = null;
            $scope.customer.CustomerInvoicing.YourRefLeadingTextId = null;
        }

        self.fillDropDowns();
    };

    $scope.invoiceGroupingChanged = function (customLeadingText) {
        if ($scope.customer.CustomerInvoicing.OrderRefLeadingTextId === customLeadingText.LeadingTextId) {
            $scope.customer.CustomerInvoicing.OrderRefLeadingTextId = null;
        }
        if ($scope.customer.CustomerInvoicing.YourRefLeadingTextId === customLeadingText.LeadingTextId) {
            $scope.customer.CustomerInvoicing.YourRefLeadingTextId = null;
        }
        self.fillDropDowns();
    };

    $scope.orderRefChanged = function () {
        if ($scope.customer.CustomerInvoicing.OrderRefLeadingTextId !== 'CustomText') {
            $scope.customer.CustomerInvoicing.OrderRefCustomText = null;
        }
    };

    $scope.yourRefChanged = function() {
        if ($scope.customer.CustomerInvoicing.YourRefLeadingTextId !== 'CustomText') {
            $scope.customer.CustomerInvoicing.YourRefCustomText = null;
        }
    };

    self.fillDropDowns = function () {
        $scope.invoiceRefs = [];
        _($scope.customer.CustomLeadingTexts).each(function (customLeadingText) {
            if (customLeadingText.IsVisible) {
                if (!$scope.customer.CustomerInvoicing.BundledBilling || customLeadingText.IsGroupByOnInvoice) {
                    $scope.invoiceRefs.push({
                        Display: customLeadingText.Text,
                        Value: customLeadingText.LeadingTextId
                    });
                }
            }
        });

        $scope.invoiceRefs.push({
            Display: '<Custom Text>',
            Value: 'CustomText'
        });

        if (!$scope.customer.CustomerInvoicing.OrderRefLeadingTextId) {
            $scope.customer.CustomerInvoicing.OrderRefLeadingTextId = 'CustomText';
        }

        if (!$scope.customer.CustomerInvoicing.YourRefLeadingTextId) {
            $scope.customer.CustomerInvoicing.YourRefLeadingTextId = 'CustomText';
        }
    };

    self.init = function () {
        $scope.isGroupByParticipant = !$scope.customer.CustomerInvoicing.BundledBilling;
        self.fillDropDowns();  
    };

    self.init();
}]);
