/*jslint nomen: true*/
/*global angular, _ */
/*jslint maxlen: 150 */
angular.module('phoenix.customers').controller('CustomerCreateAdvancedSettingsCtrl', ['$scope', function ($scope) {
    'use strict';
    var self = this;
    $scope.addEmailAddress = function () {
        $scope.customer.ParticipantInfoDefinition.AcceptedEmailAddresses.push({ IsDeleted: false });
    };
    $scope.removeEmailAddress = function (emailAddress) {
        if (emailAddress.Id) {
            emailAddress.IsDeleted = true;
        } else {
            var index = $scope.customer.ParticipantInfoDefinition.AcceptedEmailAddresses.indexOf(emailAddress);
            if (index > -1) {
                $scope.customer.ParticipantInfoDefinition.AcceptedEmailAddresses.splice(index, 1);
            }
        }
    };

    $scope.suggestLeadingTextEditMode = function (predefinedLeadingText) {
        predefinedLeadingText.isSuggestionVisible = true;
    };

    $scope.discardLeadingTextEditModeSuggestion = function (predefinedLeadingText) {
        predefinedLeadingText.isSuggestionVisible = false;
    };

    $scope.startLeadingTextEditMode = function (predefinedLeadingText) {
        predefinedLeadingText.isEditing = true;
    };

    $scope.stopLeadingTextEdits = function (predefinedLeadingText) {
        predefinedLeadingText.isEditing = false;
    };

    $scope.revertToOrignialText = function (predefinedLeadingText) {
        predefinedLeadingText.customLeadingText.Text = predefinedLeadingText.Text;
    };

    $scope.isTextEdited = function (predefinedLeadingText) {
        return predefinedLeadingText.Text !== predefinedLeadingText.customLeadingText.Text;
    };
	$scope.onDiplomaLogoUploaded = function (attachment)
	{
		$scope.customer.DiplomaLogoAttachment = attachment;
	};
	$scope.onDiplomaSignatureUploaded = function (attachment)
	{
		$scope.customer.DiplomaSignatureAttachment = attachment;
	};

	$scope.deleteDiplomaSignature = function ()
	{
		$scope.customer.DiplomaSignatureAttachment = null;
		$scope.customer.DiplomaSignatureAttachmentId = null;
	};

	$scope.deleteDiplomaLogo = function ()
	{
		$scope.customer.DiplomaLogoAttachment = null;
		$scope.customer.DiplomaLogoAttachmentId = null;
	};

    $scope.updateCustomLeadingText = function(customLeadingText) {
        if (!customLeadingText.IsVisible) {
            customLeadingText.IsVisibleInvoiceLine = false;
            customLeadingText.IsGroupByOnInvoice = false;
        }
    };

    self.getCustomLeadingText = function (predefinedLeadingText) {
        return _.find($scope.customer.CustomLeadingTexts, function (item) {
            return item.LeadingTextId === predefinedLeadingText.Id;
        });
    };

    self.initCustomLeadingTexts = function () {
        _.each($scope.predefinedLeadingTexts, function (predefinedLeadingText) {
            var customLeadingText = self.getCustomLeadingText(predefinedLeadingText);
            if (!customLeadingText) {
                customLeadingText = {
                    LeadingTextId: predefinedLeadingText.Id,
                    Text: predefinedLeadingText.Text,
                    IsMandatory: predefinedLeadingText.IsMandatory,
                    IsVisible: predefinedLeadingText.IsVisible
                };
                $scope.customer.CustomLeadingTexts.push(customLeadingText);
            }
            predefinedLeadingText.customLeadingText = customLeadingText;
        });
    };

    self.init = function () {
        self.initCustomLeadingTexts();
        var emailAddresses = _.where($scope.customer.ParticipantInfoDefinition.AcceptedEmailAddresses, { IsDeleted: false });
        if (emailAddresses.length === 0) {
            $scope.addEmailAddress();
        }
    };

    self.init();
}]);