angular.module('phoenix.customers').controller('CustomerCreateCtrl', [
                   '$scope',
                   'phoenix.config',
                   '$location',
                   'CustomersService',
                   'NotificationService',
                    '$stateParams',
                    '$state',
                    'customer',
                    'codes',
                    'languages',
                    'predefinedLeadingTexts',
                    'DistributorsService',
                    'distributor',
    function ($scope, config, $location, customerService, notificationService, $stateParams, $state, customer, codes, languages, predefinedLeadingTexts, distributorsService, distributor) {
    'use strict';
        var self = this;
        $scope.isBusy = false;
        $scope.isEditMode = !!customer.Id;
        $scope.distributor = distributor;
        
        $scope.codes = codes || {};
        $scope.languages = languages || [];
        $scope.predefinedLeadingTexts = predefinedLeadingTexts || [];
        
        $scope.templates = [
            {
                url: config.basePath + 'app/phoenix.customers/views/create/customer-create-info.html',
                name: 'Customer Info',
                queryParam: 'basic'
            },
            {
                url: config.basePath + 'app/phoenix.customers/views/create/customer-create-contact-persons.html',
                name: 'Contact Persons',
                queryParam: 'contactpersons'
            },
            {
                url: config.basePath + 'app/phoenix.customers/views/create/customer-create-advanced.html',
                name: 'Advanced Settings',
                queryParam: 'advanced'
            },
            {
                url: config.basePath + 'app/phoenix.customers/views/create/customer-create-invoicing.html',
                name: 'Invoice Conditions',
                queryParam: 'invoicing'
            },
            {
                url: config.basePath + 'app/phoenix.customers/views/create/customer-create-automails.html',
                name: 'Automails',
                queryParam: 'automails'
            },
            {
                url: config.basePath + 'app/phoenix.customers/views/details/customer-comments.html',
                name: 'Comments',
                queryParam: 'comments'
            }
        ];

        $scope.isFirstTemplate = function (template) {
            return $scope.templates.indexOf(template) === 0;
        };

        $scope.isLastTemplate = function (template) {
            return $scope.templates.indexOf(template) === $scope.templates.length - 1;
        };

        $scope.setActiveTemplate = function (template) {
            $scope.activeTemplate = template;
            $state.go('customer.overview.edit.section', { section: template.queryParam });
        };

        $scope.isActiveTemplate = function (template) {
            var section = $location.search().section || 'basic';
            return template.queryParam === section;
        };

        $scope.saveAndClose = function () {
            $scope.isBusy = true;
            customerService.update($scope.customer).then(function (result) {
                $scope.isBusy = false;
                $scope.customer = angular.copy(result);
                self.close(true);
            }, self.handleError);
        };

        $scope.deleteAndClose = function () {
            if (confirm("Do you really want to delete this customer?")) {
                $scope.isBusy = true;
                customerService.deleteCustomer($scope.customer.Id).then(function() {
                    $scope.isBusy = false;
                    $state.go('customerlist');
                    notificationService.alertSuccess({
                        header: $scope.customer.Name,
                        message: 'Customer successfully deleted from the system'
                    });

                }, self.handleError);
            };
        };

        $scope.createCustomer = function () {
            $scope.isBusy = true;
            customerService.create($scope.customer).then(function (result) {
                $scope.isBusy = false;
                $state.go('customer.overview.edit.section', { customerId: result.Id, section: 'contactpersons' }, { location: 'replace' });
            }, self.handleError);
        };

        $scope.cancel = function () {
            self.close(false);
        };

        self.close = function (reload) {
            if ($scope.customer.Id) {
                if (reload) {
                    $state.go('customer.overview.details.section', { section: $scope.activeTemplate.queryParam }, { reload: true });
                } else {
                    $state.go('customer.overview.details.section', { section: $scope.activeTemplate.queryParam });
                }
            } else {
                $state.go('customerlist');
            }
        };
        
        self.setActiveTemplateFromUrl = function () {
            var section = $location.search().section || 'basic',
                template = _($scope.templates).find(function (t) { return t.queryParam === section; });
            $scope.activeTemplate = template;
        };

        self.handleError = function () {
            $scope.isBusy = false;
        };

        $scope.commentsUpdated = function (comments) {
            $scope.isCommentsLoaded = true;
            $scope.commentCount = comments.length;
        };
        
        self.initCustomer = function (cust) {
            $scope.isCommentsLoaded = false;

            $scope.customer = angular.copy(cust) || {};
            $scope.customer.AutomailDefinition = $scope.customer.AutomailDefinition || {};
            $scope.customer.CustomerContactPersons = $scope.customer.CustomerContactPersons || [];
            $scope.customer.Addresses = $scope.customer.Addresses || [];
            $scope.customer.ParticipantInfoDefinition = $scope.customer.ParticipantInfoDefinition || {};
            $scope.customer.ParticipantInfoDefinition.AcceptedEmailAddresses = $scope.customer.ParticipantInfoDefinition.AcceptedEmailAddresses || [];
            $scope.customer.CustomerInvoicing = $scope.customer.CustomerInvoicing || {};
            $scope.customer.CustomLeadingTexts = $scope.customer.CustomLeadingTexts || [];
        };

        self.initCustomer(customer);
        self.setActiveTemplateFromUrl();
}]);