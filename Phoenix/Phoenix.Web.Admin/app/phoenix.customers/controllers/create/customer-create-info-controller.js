/*jslint nomen: true*/
/*global angular, _ */
/*jslint maxlen: 170 */
angular.module('phoenix.customers').controller('CustomerCreateInfoCtrl', [
    '$scope',
    'CustomersService',
    'ExternalCustomersService',
    'NotificationService',  
    'DistributorsService',
    function ($scope, customerService, externalCustomerService, notificationService, distributorsService) {
        'use strict';
        var self = this, ADDRESSTYPE_INVOICE = 0, ADDRESSTYPE_POST = 1, searchExternalCustomerPlaceholder = 'Search external customer...';

        $scope.searchParentCustomer = function (searchString) {
            return customerService.search({ query: searchString, limit: 20 }).then(function (result) {
                return result.Items;
            });
        };

        $scope.searchExternalCustomer = function (searchString) {
            $scope.existingCustomerId = null;
            if ($scope.customer.DistributorId) {
                return externalCustomerService.search(searchString, $scope.customer.DistributorId).then(function (result) {
                    return result;
                });
            }
        };

        self.setCustomerSearchBusy = function (placeholderText) {
            $scope.searchExternalCustomerBusy = true;
            $scope.selectedExternalCustomer = null;
            $scope.searchCustomerPlaceholder = placeholderText;
        };

        self.restoreCustomerSearch = function (externalCustomer) {
            $scope.searchExternalCustomerBusy = false;
            $scope.selectedExternalCustomer = externalCustomer;
            $scope.searchCustomerPlaceholder = searchExternalCustomerPlaceholder;
        };

        $scope.onExternalCustomerSelected = function (externalCustomer) {
            self.setCustomerSearchBusy('Verifying selected customer..');
            
            customerService.getByExternalCustomerId(externalCustomer.Id, distributorsService.get().Id).then(function (result) {
                self.restoreCustomerSearch($scope.userSelectedExternalCustomer);
                $scope.existingCustomerId = result.Id;
            }, function (error) {
                if (error.notFound) {
                    self.restoreCustomerSearch(externalCustomer);
                    self.setExternalCustomerValues(externalCustomer);
                    $scope.userSelectedExternalCustomer = externalCustomer;
                } else {
                    notificationService.alertError({ message: 'An unexpected error occurred while verifying selected customer' });
                    self.restoreCustomerSearch($scope.userSelectedExternalCustomer);
                }
            });
        };

        $scope.onCompanyRegistrationNumberEntered = function(companyRegistrationNumber) {
            customerService.getByCompanyRegistrationNumber(companyRegistrationNumber, $scope.customer.IsArbitrary, distributorsService.get().Id).then(function(result) {
                    $scope.existingCustomerIdByCompanyRegistrationNumber = result.Id;
                }
            );
        };

        $scope.setValidSelectedExternalCustomer = function () {
            if ($scope.selectedExternalCustomer) {
                $scope.selectedExternalCustomer = $scope.userSelectedExternalCustomer;
            } else {
                self.resetExternalCustomerValues();
            }
        };

        $scope.copyToMailAddress = function () {
            if ($scope.useInvoiceAddressAsMailAddess) {
                $scope.mailAddress.AddressStreet1 = $scope.invoiceAddress.AddressStreet1;
                $scope.mailAddress.AddressStreet2 = $scope.invoiceAddress.AddressStreet2;
                $scope.mailAddress.ZipCode        = $scope.invoiceAddress.ZipCode;
                $scope.mailAddress.City           = $scope.invoiceAddress.City;
                $scope.mailAddress.Country        = $scope.invoiceAddress.Country;
            }
        };

        self.setExternalCustomerValues = function (externalCustomer) {
            $scope.customer.Name = externalCustomer.Name;
            $scope.customer.ExternalCustomerId = externalCustomer.Id;
            $scope.customer.CompanyRegistrationNumber = externalCustomer.CompanyRegistrationNumber;
            $scope.invoiceAddress.AddressStreet1 = externalCustomer.AddressStreet1;
            $scope.invoiceAddress.AddressStreet2 = externalCustomer.AddressStreet2;
            $scope.invoiceAddress.ZipCode = externalCustomer.ZipCode;
            $scope.invoiceAddress.City = externalCustomer.City;
            $scope.invoiceAddress.Country = externalCustomer.Country;
            $scope.copyToMailAddress();
        };

        self.resetExternalCustomerValues = function () {
            $scope.customer.Name = null;
            $scope.customer.ExternalCustomerId = null;
            $scope.customer.CompanyRegistrationNumber = null;
            $scope.invoiceAddress.AddressStreet1 = null;
            $scope.invoiceAddress.AddressStreet2 = null;
            $scope.invoiceAddress.ZipCode = null;
            $scope.invoiceAddress.City = null;
            $scope.invoiceAddress.Country = null;

            $scope.selectedExternalCustomer = null;
            $scope.userSelectedExternalCustomer = null;
            $scope.useInvoiceAddressAsMailAddess = false;
        };

        self.isAddressesEqual = function (a1, a2) {
            return a1.AddressStreet1 === a2.AddressStreet1 &&
                   a1.AddressStreet2 === a2.AddressStreet2 &&
                   a1.ZipCode === a2.ZipCode &&
                   a1.City === a2.City &&
                   a1.Country === a2.Country;
        };

        self.initCustomerAddress = function (customer, type) {
            var address = _.find(customer.Addresses, function (a) {
                return a.AddressType === type;
            });

            if (!address) {
                address = { AddressType: type, CustomerId: customer.Id };
                customer.Addresses.push(address);
            }
            return address;
        };

        self.initExternalCustomer = function () {
            $scope.selectedExternalCustomer = { Name: $scope.customer.Name };
            $scope.userSelectedExternalCustomer = $scope.selectedExternalCustomer;
        };

        self.canEditCustomerName = function () {

            if (!$scope.distributor.HasDefaultInvoiceProvider) {
                if (!!$scope.customer.ExternalCustomerId)               //Could've checked for $scope.isEditMode as well (indicates that an existing customer is in edit mode), 
                    return false;                                       //but a customer can't have an ExternalCustomerId when it's about to be created (i.e. a new customer).
            }

            return true;
        }

        self.canEditInvoiceAddress = function () {

            if (!$scope.distributor.HasDefaultInvoiceProvider) {
                if (!!$scope.customer.ExternalCustomerId)
                    return false;

                if (!$scope.customer.IsArbitrary) {
                    return false;
                }
            }

            return true;
        }

        self.init = function () {
            $scope.searchCustomerPlaceholder = searchExternalCustomerPlaceholder;
            $scope.invoiceAddress = self.initCustomerAddress($scope.customer, ADDRESSTYPE_INVOICE);
            $scope.mailAddress = self.initCustomerAddress($scope.customer, ADDRESSTYPE_POST);

            $scope.showSearchExternalCustomerComponent = (!$scope.distributor.HasDefaultInvoiceProvider && !$scope.isEditMode && !$scope.customer.IsArbitrary);
            $scope.canEditCustomerName = self.canEditCustomerName();
            $scope.canEditInvoiceAddress = self.canEditInvoiceAddress();

            if ($scope.isEditMode) {
                $scope.useInvoiceAddressAsMailAddess = self.isAddressesEqual($scope.mailAddress, $scope.invoiceAddress);
                self.initExternalCustomer();
            } else {
                $scope.useInvoiceAddressAsMailAddess = false;
            }
        };

        self.init();
}]);