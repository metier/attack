﻿/*global angular */
angular.module('phoenix.customers').controller('CustomerDetailsCtrl', [
    '$scope',
    '$rootScope',
    '$state',
    '$location',
    'customer',
    'parentCustomer',
    'languages',
    'phoenix.config',
    'UsersService',
    'DistributorsService',
    function ($scope, $rootScope, $state, $location, customer, parentCustomer, languages, config, usersService, distributorsService) {
        'use strict';
        var self = this;

        $scope.templates = [
            {
                url: config.basePath + 'app/phoenix.customers/views/details/customer-info.html',
                name: 'Customer Info',
                queryParam: 'basic'
            },
            {
                url: config.basePath + 'app/phoenix.customers/views/details/customer-contact-persons.html',
                name: 'Contact Persons',
                queryParam: 'contactpersons'
            },
            {
                url: config.basePath + 'app/phoenix.customers/views/details/customer-advanced.html',
                name: 'Advanced Settings',
                queryParam: 'advanced'
            },
            {
                url: config.basePath + 'app/phoenix.customers/views/details/customer-invoicing.html',
                name: 'Invoice Conditions',
                queryParam: 'invoicing'
            },
            {
                url: config.basePath + 'app/phoenix.customers/views/details/customer-automails.html',
                name: 'Automails',
                queryParam: 'automails'
            },
            {
                url: config.basePath + 'app/phoenix.customers/views/details/customer-comments.html',
                name: 'Comments',
                queryParam: 'comments'
            }
        ];

        $scope.setActiveTemplate = function(template) {
            $scope.activeTemplate = template;
            $state.go('customer.overview.details.section', { section: template.queryParam });
        };

        self.setActiveTemplateFromUrl = function() {
            var section = $location.search().section || 'basic',
                template = _($scope.templates).find(function(t) { return t.queryParam === section; });
            $scope.activeTemplate = template;
        };

        $scope.isActiveTemplate = function(template) {
            var section = $location.search().section || 'basic';
            return template.queryParam === section;
        };

        $scope.isActive = function(s) {
            return $state.current.name.indexOf(s) === 0;
        };

        $scope.getLanguageDescription = function(languageId) {
            var language = _($scope.languages).find(function(languageCandidate) {
                return languageCandidate.Id === languageId;
            });
            return language.Name;
        };

        $scope.getCompetitionModeDescription = function(competitionModeValue) {
            switch (competitionModeValue) {
                case 0:
                    return 'Full (see classmates and their scores)';
                case 1:
                    return 'See classmates, but not their scores';
                case 2:
                    return 'None (don\'t see classmates)';
            }
        };

        $scope.getHeaderReferenceText = function (leadingTextId) {

            var customLeadingText = _($scope.customer.CustomLeadingTexts).find(function(customLeadingText) {
                return customLeadingText.LeadingTextId === leadingTextId;
            });

            if (!customLeadingText) {
                return 'Custom Text';
            } else {
                return customLeadingText.Text;
            }
        };

        $scope.commentsUpdated = function (comments) {
            $scope.isCommentsLoaded = true;
            $scope.commentCount = comments.length;
        };

        self.init = function (initCustomer) {
            var accountManager, programCoordinator, examCoordinator, subjectMatterExpert;

            $scope.isCommentsLoaded = false;

            $scope.customer = initCustomer || {};
            $scope.parentCustomer = parentCustomer || {};
            if (!$scope.customer.CustomerInvoicing) {
                $scope.customer.CustomerInvoicing = {};
            }
            $scope.invoiceAddress = _($scope.customer.Addresses).find(function (address) {
                return address.AddressType === 0;
            });
            $scope.mailAddress = _($scope.customer.Addresses).find(function (address) {
                return address.AddressType === 1;
            });
            $scope.groupByParticipants = !$scope.customer.CustomerInvoicing.BundledBilling;
            $scope.languages = languages;

            accountManager = _($scope.customer.MetierContactPersons).find(function(metierContactPerson) {
                return (metierContactPerson.ContactType === 0 && !metierContactPerson.IsDeleted);
            });

            if (accountManager) {
                usersService.getUserAccountByUserId(accountManager.UserId).then(function (result) {
                    $scope.accountManager = result.User;
                });
            }

            programCoordinator = _($scope.customer.MetierContactPersons).find(function (metierContactPerson) {
                return (metierContactPerson.ContactType === 1 && !metierContactPerson.IsDeleted);
            });

            if (programCoordinator) {
                usersService.getUserAccountByUserId(programCoordinator.UserId).then(function (result) {
                    $scope.programCoordinator = result.User;
                });
            }

            examCoordinator = _($scope.customer.MetierContactPersons).find(function (metierContactPerson) {
                return (metierContactPerson.ContactType === 2 && !metierContactPerson.IsDeleted);
            });

            if (examCoordinator) {
                usersService.getUserAccountByUserId(examCoordinator.UserId).then(function (result) {
                    $scope.examCoordinator = result.User;
                });
            }

            subjectMatterExpert = _($scope.customer.MetierContactPersons).find(function (metierContactPerson) {
                return (metierContactPerson.ContactType === 3 && !metierContactPerson.IsDeleted);
            });

            if (subjectMatterExpert) {
                usersService.getUserAccountByUserId(subjectMatterExpert.UserId).then(function (result) {
                    $scope.subjectMatterExpert = result.User;
                });
            }
        };
        
        self.init(customer);
        self.setActiveTemplateFromUrl();
    }
]);