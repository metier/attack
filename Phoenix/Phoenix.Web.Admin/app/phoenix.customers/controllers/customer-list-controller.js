﻿/*global angular */
angular.module('phoenix.customers').controller('CustomerListCtrl', ['$scope',
        '$location',
        '$state',
        'customers', function ($scope, $location, $state, customers) {
    'use strict';

    $scope.customers = customers || { TotalCount: 0, Items: [] };
    $scope.query = $location.search().query;
    $scope.orderBy = $location.search().orderBy || '';
    $scope.orderDirection = $location.search().orderDirection || '';
    $scope.page = $location.search().page;
    $scope.limit = $location.search().limit || 15;
    $scope.maxPages = $scope.customers.TotalCount;

    $scope.onQueryChanged = function () {
        if ($scope.query === '') {
            $scope.search();
        }
    };

    $scope.search = function () {
        if (!$state.params.query && $scope.query === '') { return; }
        $state.go('customerlist', { query: $scope.query, page: 1 });
    };

    $scope.navigate = function (page) {
        if (page < 1 || page > $scope.maxPages) {
            return;
        }
        $location.search('page', page);
    };
}]);
