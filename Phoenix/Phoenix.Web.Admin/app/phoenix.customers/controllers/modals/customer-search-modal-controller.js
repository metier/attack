﻿angular.module('phoenix.customers').controller('CustomerSearchModalCtrl', [
    '$scope',
    '$modalInstance',
    'CustomersService',
    function ($scope, $modalInstance, customersService) {
        'use strict';

        var self = this;
        $scope.isInitialized = false;
        $scope.lastSearchQuery = '';
        $scope.limit = 20;
        $scope.maxPages = 0;
        $scope.model = {
            query: '',
            searchAllDistributors: false
        };

        $scope.onQueryChanged = function() {
            if ($scope.model.query === '') {
                $scope.users = self.initialData;
            }
        };

        $scope.search = function(page) {
            $scope.isSearching = true;
            $scope.lastSearchQuery = $scope.model.query;
            customersService.search({ query: $scope.model.query, limit: $scope.limit, page: page, searchAllDistributors: $scope.model.searchAllDistributors }).then(function (result) {
                $scope.isSearching = false;
                $scope.customers = result;
                $scope.maxPages = result.TotalCount;
            }, function () {
                $scope.isSearching = false;
            });
        };

        $scope.select = function(customer) {
            $scope.model.selectedCustomer = customer;
        };

        $scope.isSelected = function(customer) {
            return $scope.model.selectedCustomer && $scope.model.selectedCustomer.Id === customer.Id;
        };

        $scope.navigate = function (page) {
            if (page < 1 || page > $scope.maxPages) {
                return;
            }
            $scope.search(page);
            $scope.page = page;
        };

        $scope.ok = function() {
            $modalInstance.close($scope.model.selectedCustomer);
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        function init() {
            $scope.model.query = '';
            customersService.search({ query: $scope.model.query, limit: $scope.limit, page: 1, searchAllDistributors: $scope.model.searchAllDistributors }).then(function (result) {
                $scope.maxPages = result.TotalCount;
                $scope.isInitialized = true;
                self.initialData = result;
                $scope.customers = result;
            });
        }

        init();
    }
]);