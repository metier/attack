﻿angular.module('phoenix.customers').controller('SubOrgsSelectModalCtrl', [
    '$scope',
    '$modalInstance',
    'customer',
    'subOrganizations',
    function ($scope, $modalInstance, customer, subOrganizations) {
        'use strict';
        var self = this;
        $scope.customer = customer;
        $scope.subOrganizations = subOrganizations.Items;

        $scope.model = {
            selectedSubOrganizations: [],
            isAllSelected: false
        };

        $scope.toggleSelect = function (subOrg) {
            var selectedSubOrg = self.getSelectedSubOrg(subOrg.Id);
            if (!selectedSubOrg) {
                self.selectSubOrg(subOrg, true);
            } else {
                self.selectSubOrg(selectedSubOrg, false);
            }
        };

        self.selectSubOrg = function (subOrg, isSelected) {
            var selectedSubOrg = self.getSelectedSubOrg(subOrg.Id);
            if (isSelected) {
                if (!selectedSubOrg) {
                    $scope.model.selectedSubOrganizations.push(subOrg);
                }
            } else {
                if (selectedSubOrg) {
                    var index = $scope.model.selectedSubOrganizations.indexOf(selectedSubOrg);
                    if (index > -1) {
                        $scope.model.selectedSubOrganizations.splice(index, 1);
                    }
                }
            }
            $scope.model.isAllSelected = $scope.model.selectedSubOrganizations.length === $scope.subOrganizations.length;
        };

        $scope.isSelected = function (subOrg) {
            return !!self.getSelectedSubOrg(subOrg.Id);
        };

        $scope.toggleSelectAll = function () {
            var isAllSelected = $scope.model.isAllSelected;
            _($scope.subOrganizations).each(function (subOrg) { self.selectSubOrg(subOrg, isAllSelected); });
        };

        self.getSelectedSubOrg = function (id) {
            return _($scope.model.selectedSubOrganizations).find(function (o) { return o.Id === id; });
        };

        $scope.ok = function () {
            $modalInstance.close($scope.model.selectedSubOrganizations);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }
]);