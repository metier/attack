﻿/*global angular */
angular.module('phoenix.customers').controller('CustomerCtrl', ['$scope', '$state', 'customer', function ($scope, $state, customer) {
    'use strict';
    $scope.customer = customer || {};
    
    $scope.navigateToCourseProgram = function () {
        // Do not navigate if we're already here
        if ($state.current.name.indexOf('customer.courseprograms') > -1) {
            return;
        }

        // Navigate back to self before proceeding
        $state.go('customer', { customerId: $scope.customer.Id }).then(function () {
            $state.go('customer.courseprograms', { customerId: $scope.customer.Id });
        });
    };

    $scope.isActive = function (s) {
        return $state.current.name.indexOf(s) === 0;
    };
}]);