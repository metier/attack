﻿/*global angular, _*/
angular.module('phoenix.customers', [
        'phoenix.config',
        'phoenix.users',
        'phoenix.shared',
        'phoenix.products',
        'ui.bootstrap',
        'ui.utils',
        'placeholderShim',
        'ui.sortable',
        'ui.router']
).config([
    '$stateProvider',
    'phoenix.config',
    '$locationProvider',
    function ($stateProvider, config) {
        'use strict';
        $stateProvider.state('customerlist', {
            url: '/customers?query&page&limit&orderBy&orderDirection',
            controller: 'CustomerListCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers/views/customer-list.html',
            resolve: {
                customers: ['CustomersService', 'DistributorsService', '$stateParams', function (customerService, distributorsService, $stateParams) {
                    var options = {
                        distributorId: distributorsService.get().Id,
                        query: $stateParams.query,
                        page: parseInt($stateParams.page, 10) || 1,
                        limit: parseInt($stateParams.limit, 10) || 15,
                        orderBy: $stateParams.orderBy,
                        orderDirection: $stateParams.orderDirection
                    };
                    return customerService.search(options);
                }]
            }
        });

        $stateProvider.state('customercreate', {
            url: '/customers/create/:isArbitrary',
            controller: 'CustomerCreateCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers/views/create/customer-create.html',
            reloadOnSearch: false,
            resolve: {

                codes: ['CodesService', function (codesService) {
                    return codesService.getAll();
                }],
                languages: ['LanguagesService', function (languagesService) {
                    return languagesService.getAll();
                }],
                predefinedLeadingTexts: ['LeadingTextsService', function (leadingTextService) {
                    return leadingTextService.getAllPredefined();
                }],
                customer: ['DistributorsService', '$stateParams', function (distributorsService, $stateParams) {
                    var distributor = distributorsService.get();
                    if (!distributor) {
                        throw 'No distributor selected!';
                    }
                    return {
                        DistributorId: distributor.Id,
                        IsArbitrary: $stateParams.isArbitrary === 'true',
                        CustomerInvoicing: {
                            BundledBilling: true,
                            ParticipantsBillable: true
                        }
                    };
                }],
                distributor: ['DistributorsService', function (distributorsService) {
                    return distributorsService.get();
                }]
            }
        }).state('customercreate.section', {
            url: '/?section'
        });

        $stateProvider.state('customer', {
            url: '/customers/:customerId',
            controller: 'CustomerCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers/views/customer.html',
            resolve: {
                codes: ['CodesService', function (codesService) {
                    return codesService.getAll();
                }],
                languages: ['LanguagesService', function (languagesService) {
                    return languagesService.getAll();
                }],
                customer: ['CustomersService', '$stateParams', function (customerService, $stateParams) {
                    return customerService.get($stateParams.customerId);
                }]
            }
        }).state('customer.overview', {
            url: '/',
            template: '<div ui-view></div>',
            abstract: true
        }).state('customer.overview.details', {
            url: '',
            controller: 'CustomerDetailsCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers/views/customer-details.html',
            resolve: {
                parentCustomer: ['CustomersService', 'customer', function (customerService, customer) {
                    if (customer.ParentId) {
                        return customerService.get(customer.ParentId);
                    }
                    return null;
                }]
            }
        }).state('customer.overview.details.section', {
            url: '/?section'
        }).state('customer.overview.edit', {
            url: 'edit',
            controller: 'CustomerCreateCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers/views/create/customer-create.html',
            resolve: {
                predefinedLeadingTexts: ['LeadingTextsService', function (leadingTextService) {
                    return leadingTextService.getAllPredefined();
                }],
                distributor: ['DistributorsService', 'customer', function (distributorsService, customer) {
                    return distributorsService.getAll().then(function (distributors) {
                        return _(distributors).find(function(distributor) {
                            return distributor.Id == customer.DistributorId;
                        });
                    });
                }]
            }
        }).state('customer.overview.edit.section', {
            url: '/?section'
        });

        // User states
        $stateProvider.state('customer.users', {
            url: '/users?query&page&limit&orderBy&orderDirection',
            controller: 'UserListCtrl',
            templateUrl: config.basePath + 'app/phoenix.users/views/user-list.html',
            resolve: {
                users: ['UsersService', '$stateParams', function (userService, $stateParams) {
                    var options = {
                        customerId: parseInt($stateParams.customerId, 10),
                        query: $stateParams.query,
                        page: parseInt($stateParams.page, 10) || 1,
                        limit: parseInt($stateParams.limit, 10) || 15,
                        orderBy: $stateParams.orderBy,
                        orderDirection: $stateParams.orderDirection
                    };
                    return userService.search(options);
                }]
            }
        });
        $stateProvider.state('customer.usercreate', {
            url: '/users/create',
            abstract: true,
            controller: 'UserCreateCtrl',
            templateUrl: config.basePath + 'app/phoenix.users/views/user-create.html',
            resolve: {
                userAccount: function () {
                    return {
                        User: {
                            IsAllowUserEcts: true,
                            UserNotificationType: 0
                        },
                        Roles: ['LearningPortalUser']
                    };
                },
                languages: ['LanguagesService', function (languagesService) {
                    return languagesService.getAll();
                }],
                codes: ['CodesService', function (codesService) {
                    return codesService.getAll();
                }],
                predefinedLeadingTexts: ['LeadingTextsService', function (leadingTextService) {
                    return leadingTextService.getAllPredefined();
                }],
                participants: function () { return null;},
                loggedInUser: function () { return null; },
                competence: function () { return null; }
            }
        }).state('customer.usercreate.edit', {
            controller: 'UserEditCtrl',
            templateUrl: config.basePath + 'app/phoenix.users/views/edit/user-edit.html',
            url: '',
            resolve: {
                diplomas: ['UsersService', '$stateParams', function (usersService, $stateParams) {
                    return $stateParams && $stateParams.userId ? usersService.getDiplomas($stateParams.userId) : [];
                }]
            }
        }).state('customer.usercreate.edit.section', {
            url: '/?section'
        });

        $stateProvider.state('customer.user', {
            url: '/users/:userId',
            templateUrl: config.basePath + 'app/phoenix.customers/views/user/user.html',
            controller: 'UserCtrl',
            resolve: {
                userAccount: ['UsersService', 'customer', '$stateParams', function (usersService, customer, $stateParams) {
                    return usersService.getUserAccountByUserId($stateParams.userId).then(function (userAccount) {
                        userAccount.User.Customer.CustomLeadingTexts = customer.CustomLeadingTexts;
                        return userAccount;
                    });
                }],
                participants: ['ParticipantsService', '$stateParams', function (participantsService, $stateParams) {
                    return participantsService.search({ userId: $stateParams.userId, limit: -1 });
                }],
                competence: ['UsersService', '$stateParams', function (usersService, $stateParams) {
                    return usersService.getCompetence($stateParams.userId);
                }]
            }
        }).state('customer.user.overview', {
            url: '/',
            template: '<div ui-view></div>',
            abstract: true
        }).state('customer.user.overview.details', {
            url: '/?section',
            templateUrl: config.basePath + 'app/phoenix.users/views/view/user-view.html',
            controller: 'UserViewCtrl',
            resolve: {
                diplomas: ['UsersService', '$stateParams', function (usersService, $stateParams) {
                    return usersService.getDiplomas($stateParams.userId);
                }]
            }
        }).state('customer.user.overview.edit', {
            url: 'edit',
            controller: 'UserEditCtrl',
            templateUrl: config.basePath + 'app/phoenix.users/views/edit/user-edit.html',
            resolve: {
                userAccount: ['UsersService', '$stateParams', function (usersService, $stateParams) {
                    return usersService.getUserAccountByUserId($stateParams.userId);
                }],
                languages: ['LanguagesService', function (languagesService) {
                    return languagesService.getAll();
                }],
                codes: ['CodesService', function (codesService) {
                    return codesService.getAll();
                }],
                predefinedLeadingTexts: ['LeadingTextsService', function (leadingTextService) {
                    return leadingTextService.getAllPredefined();
                }],
                participants: ['ParticipantsService', '$stateParams', function (participantsService, $stateParams) {
                    return participantsService.search({ userId: $stateParams.userId, limit: -1 });
                }],
                loggedInUser: ['AuthenticationService', function (authenticationService) {
                    return authenticationService.getLoggedInUser();
                }],
                diplomas: ['UsersService', '$stateParams', function (usersService, $stateParams) {
                    return $stateParams && $stateParams.userId ? usersService.getDiplomas($stateParams.userId) : [];
                }]
            }
        }).state('customer.user.overview.edit.section', {
            url: '/?section'
        });


        // Course program states
        $stateProvider.state('customer.courseprograms', {
            url: '/courseprograms',
            controller: 'CourseProgramsCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.courseprograms/views/course-programs.html',
            resolve: {
                coursePrograms: [
                    'CourseProgramsService', '$stateParams', function(courseProgramsService, $stateParams) {
                        return courseProgramsService.getByCustomerId($stateParams.customerId);
                    }
                ]
            }
        }).state('customer.courseprograms.create', {
            url: '/create',
            controller: 'CourseProgramCreateCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.courseprograms/views/course-program-create.html',
            resolve: {
                courseProgram: ['coursePrograms', '$stateParams', function(coursePrograms, $stateParams) {
                    return {
                        CustomerId: parseInt($stateParams.customerId, 10),
                        Steps: []
                    };
                }]
            }
        }).state('customer.courseprograms.item', {
            url: '/:courseProgramId',
            resolve: {
                courseProgram: ['coursePrograms', 'CourseProgramsService', '$stateParams', function(coursePrograms, courseProgramsService, $stateParams) {
                    return _(coursePrograms).find(function (cp) {
                        return String(cp.Id) === $stateParams.courseProgramId;
                    });
                }]
            },
            abstract: true,
            template: '<ui-view />'
        }).state('customer.courseprograms.item.view', {
            url: '',
            controller: 'CourseProgramViewCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.courseprograms/views/course-program-view.html',
            resolve: {
                educationalPartners: ['ResourcesService', function (resourcesService) {
                    return resourcesService.search({ limit: -1, type: 4 });
                }]
            }
        }).state('customer.courseprograms.item.view.cpitem', {
            url: '/?itemId&type',
            controller: 'CourseProgramItemCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.courseprograms/views/course-program-item.html',
            resolve: {
                customerArticles: [
                    '$stateParams', 'CustomerArticlesService', function($stateParams, customerArticlesService) {
                        var func = $stateParams.type === 'exam' ? customerArticlesService.getByExamId : customerArticlesService.getByModuleId;
                        return func($stateParams.itemId);
                    }
                ]
            }
        }).state('customer.courseprograms.item.view.cpitem.customerarticle', {
            url: '/?customerArticleId',
            controller: 'CustomerArticleCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.courseprograms/views/customer-article.html',
            resolve: {
                data: [
                    '$stateParams', 'customerArticles', 'ArticlesService', function ($stateParams, customerArticles, articlesService) {
                        var customerArticle = _(customerArticles).find(function(ca) {
                            return String(ca.Id) === $stateParams.customerArticleId;
                        });
                        return articlesService.get(customerArticle.ArticleCopyId).then(function(article) {
                            return {
                                customerArticle: customerArticle,
                                customerArticleArticle: article
                            };
                        });
                    }
                ]
            }
        }).state('customer.courseprograms.item.view.cpitem.customerarticle.activities', {
            url: '/activities?orderBy&orderDirection&limit',
            templateUrl: config.basePath + 'app/phoenix.customers.courseprograms/views/customer-article-activities.html',
            controller: 'CustomerArticleActivitiesCtrl',
            resolve: {
                activities: [
                    'ActivitiesService', '$stateParams', function(activitiesService, $stateParams) {
                        return activitiesService.search({
                            customerArticleId: $stateParams.customerArticleId,
                            limit: parseInt($stateParams.limit, 10) || 20,
                            orderBy: $stateParams.orderBy,
                            orderDirection: $stateParams.orderDirection
                        });
                    }
                ]
            }
        }).state('customer.courseprograms.item.edit', {
            url: '/edit',
            controller: 'CourseProgramCreateCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.courseprograms/views/course-program-create.html'
        });

        // Customer article states
        $stateProvider.state('customer.articles', {
            url: '/articles',
            abstract: true,
            template: '<ui-view />'
        }).state('customer.articles.item', {
            url: '/:customerArticleId',
            abstract: true,
            controller: 'CustomerArticleEditCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers/views/customer-article-edit.html',
            resolve: {
                data: [
                    '$stateParams', 'CustomerArticlesService', 'ArticlesService', function($stateParams, customerArticlesService, articlesService) {
                        return customerArticlesService.get($stateParams.customerArticleId).then(function(customerArticle) {
                            return articlesService.get(customerArticle.ArticleCopyId).then(function(articleCopy) {
                                return {
                                    customerArticle: customerArticle,
                                    article: articleCopy
                                };
                            });
                        });
                    }
                ],
                codes: ['CodesService', function(codesService) {
                    return codesService.getAll();
                }]
            }
        }).state('customer.articles.item.view', {
            url: '/'
        }).state('customer.articles.item.edit', {
            url: '/edit'
        });

        // Activity states
        $stateProvider.state('customer.activities', {
            url: '/activities',
            abstract: 'true',
            template: '<ui-view />'
        }).state('customer.activities.overview', {
            controller: 'CustomerActivitiesOverviewCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.activities/views/overview/overview.html'
        }).state('customer.activities.overview.activities', {
            url: '/activities',
            abstract: 'true',
            template: '<ui-view />'
        }).state('customer.activities.overview.activities.list', {
            url: '/list?query&page&limit&orderBy&orderDirection',
            controller: 'OverviewActivitiesCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.activities/views/overview/activities.html',
            resolve: {
                activities: ['$stateParams', 'ActivitiesService', function ($stateParams, activitiesService) {
                    return activitiesService.search({
                        customerId: $stateParams.customerId,
                        query: $stateParams.query,
                        page: parseInt($stateParams.page, 10) || 1,
                        limit: parseInt($stateParams.limit, 10) || 15,
                        orderBy: $stateParams.orderBy,
                        orderDirection: $stateParams.orderDirection
                    });
                }]
            }
        }).state('customer.activities.overview.activities.item', {
            url: '/:activityId',
            abstract: true,
            controller: 'ActivityEditCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.activities/views/activity/activity-edit.html',
            resolve: {
                data: [
                    '$stateParams',
                    '$q',
                    'CustomerArticlesService',
                    'ActivitiesService',
                    'ArticlesService',
                    'RcosService',
                    'ResourcesService',
                    function ($stateParams, $q, customerArticlesService, activitiesService, articlesService, rcosService, resourcesService) {
                        return activitiesService.get($stateParams.activityId).then(function (activity) {
                            var activityPromises = [
                                activitiesService.getCurrentActivityOrder($stateParams.activityId).then(function (activityOrder) {
                                    return customerArticlesService.get(activity.CustomerArticleId).then(function (customerArticle) {
                                        return articlesService.get(customerArticle.ArticleCopyId).then(function (articleCopy) {
                                            return {
                                                activity: activity,
                                                currentActivityOrder: activityOrder,
                                                customerArticle: customerArticle,
                                                articleCopy: articleCopy
                                            };
                                        });
                                    });
                                })
                            ];

                            activityPromises.push(resourcesService.search({ limit: -1, type: 4 }));
                            if (activity.RcoCourseId) {
                                activityPromises.push(rcosService.getCourse(activity.RcoCourseId));
                            }
                            else if (activity.RcoExamContainerId) {
                                activityPromises.push(rcosService.getExam(activity.RcoExamContainerId));
                            }
                            
                            activityPromises.push(activitiesService.getExamStatuses($stateParams.activityId));

                            var activityExamCoursePromises = [];
                            if (activity.ActivityExamCourses) {
                                _(activity.ActivityExamCourses).each(function (course) {
                                    activityExamCoursePromises.push(rcosService.getCourse(course.CourseRcoId));
                                });
                            }

                            return $q.all(activityPromises).then(function (resolves) {
                                return $q.all(activityExamCoursePromises).then(function (examCourseResolves) {
                                    return {
                                        activity: resolves[0].activity,
                                        currentActivityOrder: resolves[0].activityOrder,
                                        customerArticle: resolves[0].customerArticle,
                                        articleCopy: resolves[0].articleCopy,
                                        educationalPartners: resolves[1],
                                        rcoCourse: activity.RcoCourseId ? resolves[2] : null,
                                        rcoExam: activity.RcoExamContainerId ? resolves[2] : null,
                                        examRcoCourses: examCourseResolves,
                                        examStatuses: resolves[resolves.length - 1]
                                    };
                                });                                
                            });
                        });
                    }
                ],
                participantUsers: ['UsersService', '$stateParams', function (usersService, $stateParams) {
                    return usersService.search({ activityId: $stateParams.activityId, limit: -1 });
                }],
                codes: ['CodesService', function (codesService) {
                    return codesService.getAll();
                }],
                activitySets: ['ActivitiesService', '$stateParams', function (activitiesService, $stateParams) {
                    return activitiesService.getActivitySets($stateParams.activityId);
                }]
            }
        }).state('customer.activities.overview.activities.item.view', {
            url: '/?selectedTab'
        }).state('customer.activities.overview.activities.item.edit', {
            url: '/edit?selectedTab'
        }).state('customer.activities.overview.activitysets', {
            url: '/activitysets',
            abstract: 'true',
            template: '<ui-view />'
        }).state('customer.activities.overview.activitysets.list', {
            url: '/list?query&page&limit&orderBy&orderDirection',
            controller: 'OverviewActivitySetsCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.activities/views/overview/activitysets.html',
            resolve: {
                activitySets: ['$stateParams', 'ActivitySetsService', function ($stateParams, activitySetsService) {
                    return activitySetsService.search({
                        customerId: $stateParams.customerId,
                        query: $stateParams.query,
                        page: parseInt($stateParams.page, 10) || 1,
                        limit: parseInt($stateParams.limit, 10) || 15,
                        orderBy: $stateParams.orderBy,
                        orderDirection: $stateParams.orderDirection
                    });
                }]
            }
        }).state('customer.activities.overview.activitysets.create', {
            url: '/create',
            controller: 'ActivitySetCreateCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.activities/views/activityset/activityset-create.html',
            resolve: {
                activitySet: ['$stateParams', function ($stateParams) {
                    return {
                        CustomerId: $stateParams.customerId,
                        EnrollmentFrom: moment().toDate(),
                        IsUnenrollmentAllowed: false,
                        IsPublished: false
                    };
                }],
                activities: function () { return {}; },
                enrolledUsers: function () { return {}; }
            }
        }).state('customer.activities.overview.activitysets.item', {
            url: '/:activitySetId',
            abstract: true,
            controller: 'ActivitySetCreateCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.activities/views/activityset/activityset-create.html',
            resolve: {
                activitySet: ['$stateParams', 'ActivitySetsService', function ($stateParams, activitySetsService) {
                    return activitySetsService.get($stateParams.activitySetId);
                }],
                activities: ['$stateParams', 'ActivitiesService', function ($stateParams, activitiesService) {
                    return activitiesService.search({ activitySetId: $stateParams.activitySetId, limit: -1 });
                }],
                enrolledUsers: ['UsersService', '$stateParams', function (usersService, $stateParams) {
                    return usersService.search({ activitySetId: $stateParams.activitySetId, limit: -1 });
                }]
            }
        }).state('customer.activities.overview.activitysets.item.view', { 
            url: '/?selectedTab'
        }).state('customer.activities.overview.activitysets.item.edit', { 
            url: '/edit?selectedTab'
        }).state('customer.activities.overview.coursecalendar', {
            url: '/coursecalendar',
            controller: 'OverviewCourseCalendarCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.activities/views/overview/coursecalendar.html',
            resolve: {
                courseCalendar: ['$stateParams', 'ActivitySetsService', function ($stateParams, activitySetsService) {
                    return activitySetsService.getCourseCalendar($stateParams.customerId);
                }]
            }
        }).state('customer.activities.overview.coursecalendar.activityset', {
            url: '/:activitySetId',
            controller: 'CoursecalendarActivitysetDetailCtrl',
            templateUrl: config.basePath + 'app/phoenix.customers.activities/views/coursecalendar/activityset-detail.html',
            resolve: {
                activitySet: ['$stateParams', 'courseCalendar', function($stateParams, courseCalendar) {
                    return _(courseCalendar).find(function (as) {
                        return String(as.Id) === $stateParams.activitySetId;
                    });
                }],
                activities: ['ActivitiesService', '$stateParams', function (activitiesService, $stateParams) {
                    var activitySetId = $stateParams.activitySetId;
                    function isNumeric( obj ) {
                        return !jQuery.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
                    }

                    if (!isNumeric($stateParams.activitySetId)) {
                        activitySetId = -1;
                    }
                    return activitiesService.search({
                        activitySetId: activitySetId,
                        limit: -1
                    });
                }]
            }
        });
    }
]);