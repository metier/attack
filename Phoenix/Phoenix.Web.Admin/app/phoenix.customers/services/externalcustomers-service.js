﻿/*global angular */
angular.module('phoenix.customers').factory('ExternalCustomersService', [
    '$http',
    '$log',
    'phoenix.config',
    'asyncHelper',
    function ($http, $log, config, asyncHelper) {
        'use strict';
        return {
            search: function (searchQuery, distributorId) {
                var url = config.apiPath + 'externalcustomers';
                return asyncHelper.resolveHttp($http.get(url, { params: { name: searchQuery, distributorId: distributorId } }));
            },
            hasExternalCustomersProvider: function (distributorId) {
                var url = config.apiPath + 'externalcustomers/hasexternalcustomersprovider';
                return asyncHelper.resolveHttp($http.get(url, { params: { distributorId: distributorId } }));
            }
        };
}]);