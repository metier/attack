﻿/*global angular */
angular.module('phoenix.customers').factory('CustomersService', [
    '$http',
    '$log',
    '$q',
    'phoenix.config',
    'asyncHelper',
    'DistributorsService',
    function ($http, $log, $q, config, asyncHelper, distributorsService) {
        'use strict';
        return {
            search: function (options) {
                var parameters = {
                    query: options.query,
                    customerIds: options.customerIds,
                    childrenOfCustomerId: options.childrenOfCustomerId,
                    limit: options.limit,
                    orderBy: options.orderBy,
                    orderDirection: options.orderDirection
                };

                if (!options.searchAllDistributors) {
                    parameters.distributorId = distributorsService.get().Id;
                }
                
                if (typeof options.page === 'number' && options.page > 0) {
                    parameters.skip = (options.page - 1) * parameters.limit;
                }
                return asyncHelper.resolveHttp($http.get(config.apiPath + 'customers', { params: parameters }));
            },
            get: function (id) {
                var url = config.apiPath + 'customers/' + id;    
                return asyncHelper.resolveHttp($http.get(url), true);
            },
            getLeadingTexts: function(id) {
                var url = config.apiPath + 'customers/' + id + '/leadingtexts';
                return asyncHelper.resolveHttp($http.get(url), false);
            },
            getByExternalCustomerId: function (externalCustomerId, distributorId) {
                var url = config.apiPath + 'customers',
                    deferred = $q.defer(),
                    parameters;
                if (!distributorId) {
                    throw 'No distributor selected!';
                }
                parameters = {
                    distributorId: distributorId,
                    externalCustomerId: externalCustomerId
                };

                $http.get(url, { params: parameters }).success(function (result) {
                    deferred.resolve(result);
                }).error(function (error, statusCode) {
                    if (statusCode === 404) {
                        deferred.reject({
                            notFound: true,
                            message: error.Message
                        });
                    } else {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },
            getByCompanyRegistrationNumber: function (companyRegistrationNumber, isArbitrary, distributorId) {
                var url = config.apiPath + 'customers',
                    deferred = $q.defer(),
                    parameters;

                if (!distributorId) {
                    throw 'No distributor selected!';
                }
                parameters = {
                    distributorId: distributorId,
                    isArbitrary: isArbitrary,
                    companyRegistrationNumber: companyRegistrationNumber
                };

                $http.get(url, { params: parameters }).success(function (result) {
                    deferred.resolve(result);
                }).error(function (error, statusCode) {
                    if (statusCode === 404) {
                        deferred.reject({
                            notFound: true,
                            message: error.Message
                        });
                    } else {
                        deferred.reject(error);
                    }
                });

                return deferred.promise;
            },
            update: function (customer) {
                var url = config.apiPath + 'customers/' + customer.Id;
                return asyncHelper.resolveHttp($http.put(url, customer));
            },
            create: function (customer) {
                var url = config.apiPath + 'customers/';
                return asyncHelper.resolveHttp($http.post(url, customer));
            },
            deleteCustomer: function (customerId) {
                var url = config.apiPath + 'customers/' + customerId + '/delete';
                return asyncHelper.resolveHttp($http.put(url));
            }
        };
}]);