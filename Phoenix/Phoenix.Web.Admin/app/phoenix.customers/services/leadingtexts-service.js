﻿/*global angular */
angular.module('phoenix.customers').factory('LeadingTextsService', [
    '$http',
    '$log',
    'phoenix.config',
    'asyncHelper',
    function ($http, $log, config, asyncHelper) {
        'use strict';
        return {
            getAllPredefined: function () {
                var url = config.apiPath + 'leadingtexts';
                return asyncHelper.resolveHttp($http.get(url));
            }
        };
}]);