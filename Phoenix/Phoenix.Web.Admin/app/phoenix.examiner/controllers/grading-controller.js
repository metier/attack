﻿angular.module('phoenix.examiner').controller('GradingCtrl', [
    '$scope',
    '$state',
    '$stateParams',
    'ParticipantsService',
    'NotificationService',
    'examSubmissions',
    function($scope, $state, $stateParams, participantsService, notificationService, examSubmissions) {
        'use strict';

        function init() {
            $scope.grades = ['', 'A', 'B', 'C', 'D', 'E', 'F'];
            _(examSubmissions.Items).each(function(es) {
                es.ExamGrade = es.ExamGrade || '';
                es.PreviousExamGrade = es.ExamGrade;
            });
            $scope.examSubmissions = examSubmissions;
            $scope.includeGraded = $stateParams.includeGraded === 'true';
            $scope.page = $stateParams.page;
            $scope.limit = $stateParams.limit || 15;
        }

        function reloadCurrentState() {
            $state.go($state.current, $state.params, { reload: true });
        }

        $scope.isGraded = function(examSubmission) {
            return !!examSubmission.ExamGrade;
        };

        $scope.navigate = function(page) {
            if (page < 1) {
                return;
            }
            $state.params.page = page;
            reloadCurrentState();
        };

        $scope.includeGradedChecked = function() {
            $state.params.includeGraded = $scope.includeGraded;
            reloadCurrentState();
        };

        $scope.setGrade = function(examSubmission) {
            examSubmission.isSettingGrade = true;
            participantsService.setGrade(examSubmission.ParticipantId, examSubmission.ExamGrade).then(function(updatedParticipant) {
                examSubmission.ExamGradeDate = updatedParticipant.ExamGradeDate;
                examSubmission.ExamGrade = updatedParticipant.ExamGrade;
                examSubmission.PreviousExamGrade = updatedParticipant.ExamGrade;
                notificationService.alertSuccess({ message: 'Grade was set!' });
            }, function() {
                examSubmission.ExamGrade = examSubmission.PreviousExamGrade;
            }).finally(function() {
                examSubmission.isSettingGrade = false;
            });
        };

        init();
    }
]);