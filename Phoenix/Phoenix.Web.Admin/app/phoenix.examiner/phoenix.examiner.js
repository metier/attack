﻿angular.module('phoenix.examiner', [
    'ui.router',
    'phoenix.config'
]).config([
    '$stateProvider',
    'phoenix.config',
    function ($stateProvider, config) {
        'use strict';
        $stateProvider.state('grading', {
            url: '/grading?page&limit&includeGraded',
            templateUrl: config.basePath + 'app/phoenix.examiner/views/grading.html',
            controller: 'GradingCtrl',
            resolve: {
                examSubmissions: ['ParticipantsService', '$stateParams', function (participantsService, $stateParams) {
                    var options = {
                        page: parseInt($stateParams.page, 10) || 1,
                        limit: parseInt($stateParams.limit, 10) || 15,
                        includeGraded: $stateParams.includeGraded === 'true'
                    };
                    return participantsService.getExamSubmissions(options);
                }]
            }
        });
    }
]);