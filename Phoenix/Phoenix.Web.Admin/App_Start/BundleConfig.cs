﻿using System.Web.Optimization;

namespace Metier.Phoenix.Web.Admin.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/app/phoenix/js")
                        .IncludeDirectory("~/app/phoenix.shared", "*.js", true)
                        .IncludeDirectory("~/app/phoenix", "*.js", true)
                        .IncludeDirectory("~/app/phoenix.customers", "*.js", true)
                        .IncludeDirectory("~/app/phoenix.users", "*.js", true)
                        .IncludeDirectory("~/app/phoenix.resources", "*.js", true)
                        .IncludeDirectory("~/app/phoenix.products", "*.js", true)
                        .IncludeDirectory("~/app/phoenix.orders", "*.js", true)
                        .IncludeDirectory("~/app/phoenix.customers.courseprograms", "*.js", true)
                        .IncludeDirectory("~/app/phoenix.customers.activities", "*.js", true)
                        .IncludeDirectory("~/app/phoenix.reporting", "*.js", true)
                        .IncludeDirectory("~/app/phoenix.exam", "*.js", true)
                        .IncludeDirectory("~/app/phoenix.examiner", "*.js", true)
                        .IncludeDirectory("~/app/phoenix.content", "*.js", true));
            
            bundles.Add(new StyleBundle("~/app/phoenixcss").Include(
                "~/app/bower_components/toastr/toastr.css",
                "~/app/bower_components/angular-busy/dist/angular-busy.css"
                ));

            bundles.Add(new ScriptBundle("~/app/bower/lib/js").Include(
               "~/app/bower_components/jquery/dist/jquery.js",
               "~/app/bower_components/jquery-ui/ui/jquery.ui.core.js",
               "~/app/bower_components/jquery-ui/ui/jquery.ui.widget.js",
               "~/app/bower_components/jquery-ui/ui/jquery.ui.mouse.js",
               "~/app/bower_components/jquery-ui/ui/jquery.ui.sortable.js",
               "~/app/bower_components/jquery-cookie/jquery.cookie.js",
               "~/app/bower_components/angular/angular.js",
               "~/app/bower_components/angular-animate/angular-animate.js",
               "~/app/bower_components/angular-sanitize/angular-sanitize.js",
               "~/app/bower_components/angular-cookies/angular-cookies.js",
               "~/app/bower_components/toastr/toastr.js",
               "~/app/bower_components/bootstrap/dist/js/bootstrap.js",
               "~/app/bower_components/underscore/underscore.js",
               "~/app/bower_components/momentjs/moment.js",
               "~/app/bower_components/moment-timezone/moment-timezone.js",
               "~/app/bower_components/angular-ui-utils/ui-utils.js",
               "~/app/bower_components/angular-ui-utils/ui-utils-ieshiv.js",
               "~/app/bower_components/angular-ui-sortable/sortable.js",
               "~/app/bower_components/angular-bootstrap/ui-bootstrap.js",
               "~/app/bower_components/angular-bootstrap/ui-bootstrap-tpls.js",
               "~/app/bower_components/ngUpload/ng-upload.js",
               "~/app/bower_components/angular-promise-tracker/promise-tracker.js",
               "~/app/bower_components/angular-busy/dist/angular-busy.js",
               "~/app/bower_components/angular-ui-router/release/angular-ui-router.js",
               "~/app/bower_components/select2/select2.min.js",
               "~/app/bower_components/angular-ui-select2/src/select2.js"

               ));

            bundles.Add(new ScriptBundle("~/app/lib/js").Include(
                "~/app/lib/shims/jquery.html5-placeholder-shim.js",
                "~/app/lib/shims/placeholder.js"
                ));

            bundles.Add(new ScriptBundle("~/app/assets/data/js").Include(
                "~/app/assets/data/angular-locale_no.js",
                "~/app/assets/data/moment-timezone-data.js"));
        }
    }
}