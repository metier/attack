﻿using System.Web.Mvc;

namespace Metier.Phoenix.Web.Admin.App_Start
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}