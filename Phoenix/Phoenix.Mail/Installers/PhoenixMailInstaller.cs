﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Metier.Phoenix.Mail.Installers
{
    public class PhoenixMailInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Classes.FromThisAssembly().Pick().WithServiceDefaultInterfaces().Configure(s => s.IsFallback()).LifestyleTransient()
            );
        }
    }
}