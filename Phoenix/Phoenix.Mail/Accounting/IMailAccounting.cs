﻿using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Mail.Accounting
{
    public interface IMailAccounting
    {
        int CreateMail(AutoMailMessage autoMailMessage);
        void UpdateStatus(int mailId, MailStatusType status);
        Core.Data.Entities.Mail GetMail(int mailId);
    }
}