﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Newtonsoft.Json;

namespace Metier.Phoenix.Mail.Accounting
{
    public class MailAccounting : IMailAccounting
    {
        private readonly MetierLmsContext _context;

        public MailAccounting(MetierLmsContext context)
        {
            _context = context;
        }

        public int CreateMail(AutoMailMessage autoMailMessage)
        {
            var createdTime = DateTime.UtcNow;
            var mail = new Core.Data.Entities.Mail
            {
                MessageType = autoMailMessage.MessageType,
                Created = createdTime,
                RecipientEmail = autoMailMessage.RecipientEmail,
                Subject = autoMailMessage.Subject,
                MailStatuses = new List<MailStatus> { new MailStatus { Status = MailStatusType.Created, StatusTime = createdTime } },
                SerializedMailMessage = new SerializedMailMessage{ SerializedMessage = JsonConvert.SerializeObject(autoMailMessage) },
                UserId = autoMailMessage.UserId,
                ActivityId = autoMailMessage.ActivityId
            };

            if (autoMailMessage.ParticipantId.HasValue)
                mail.ParticipantId = autoMailMessage.ParticipantId;

            _context.Mails.Add(mail);
            _context.SaveChanges();

            return mail.Id;
        }

        public void UpdateStatus(int mailId, MailStatusType status)
        {
            var mail = _context.Mails.Include(m => m.MailStatuses).FirstOrDefault(m => m.Id == mailId);

            if (mail == null)
            {
                throw new ArgumentException("No mail found for specified ID", "mailId");
            }

            mail.MailStatuses.Add(new MailStatus{Status = status, StatusTime = DateTime.UtcNow});

            _context.SaveChanges();
        }

        public Core.Data.Entities.Mail GetMail(int mailId)
        {
            return _context.Mails.Include(m => m.SerializedMailMessage).FirstOrDefault(m => m.Id == mailId);
        }
    }
}