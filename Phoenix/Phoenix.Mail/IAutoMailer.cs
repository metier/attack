﻿using System;

namespace Metier.Phoenix.Mail
{
    public interface IAutoMailer
    {
        /// <summary>
        /// Creates a new Mail message and tries to send it
        /// </summary>
        /// <param name="autoMailMessage"></param>
        /// <returns>Whether the sending was successful</returns>
        bool Send(AutoMailMessage autoMailMessage);

        /// <summary>
        /// Tries to send an existing Mail message
        /// </summary>
        /// <param name="mailId"></param>
        /// <returns>Whether the sending was successful</returns>
        bool Send(int mailId);

        /// <summary>
        /// Gets mail message for a given <see cref="mailId"/>
        /// </summary>
        /// <param name="mailId"></param>
        /// <returns></returns>
        AutoMailMessage Get(int mailId);

        //bool SendInvitation(AutoMailMessage autoMailMessage, DateTime start, DateTime end, string location);
    }
}