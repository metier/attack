﻿using System.Collections.Generic;

namespace Metier.Phoenix.Mail.FieldFactories
{
    public interface IUserDiplomasFieldsFactory
    {
        Dictionary<string, string> CreateFields(int userDiplomasId); 
    }
}