using System.Collections.Generic;

namespace Metier.Phoenix.Mail.FieldFactories
{
    public interface IParticipantFieldsFactory
    {
        Dictionary<string, string> CreateFields(int participantId, string languageIdentifier, params string[] fieldNames);
    }
}