﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;

namespace Metier.Phoenix.Mail.FieldFactories
{
    public class UserDiplomasFieldsFactory : IUserDiplomasFieldsFactory
    {
        private const string HideElementInlineCss = "display:none !important; font-size: 0; max-height: 0; line-height: 0; mso-hide: all;";

        private readonly MetierLmsContext _context;

        public UserDiplomasFieldsFactory(MetierLmsContext context)
        {
            _context = context;
        }

        public Dictionary<string, string> CreateFields(int userDiplomaId)
        {

            var userDiploma = _context.UserDiplomas.First(u => u.Id == userDiplomaId);
         
			var fields = new Dictionary<string, string>
            {
                { "diploma-title", HttpUtility.HtmlEncode(userDiploma.Title) },
                { "diploma-created", HttpUtility.HtmlEncode(userDiploma.Created) },

            };
            return fields;
        }
    }
}