﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Metier.Phoenix.Core.Data;

namespace Metier.Phoenix.Mail.FieldFactories
{
    public class CustomerFieldsFactory : ICustomerFieldsFactory
    {
        private readonly MetierLmsContext _context;

        public CustomerFieldsFactory(MetierLmsContext context)
        {
            _context = context;
        }

        public Dictionary<string, string> CreateFields(int customerId)
        {
            var customer = _context.Customers.Include(c => c.ParticipantInfoDefinition).First(c => c.Id == customerId);
            var hasCustomerPortalUrl = customer.ParticipantInfoDefinition != null && !string.IsNullOrEmpty(customer.ParticipantInfoDefinition.CustomerPortalUrl);

            var distributor = _context.Distributors.First(d => d.Id == customer.DistributorId);
            var hasDistributorPortalUrl = !string.IsNullOrEmpty(distributor.LoginFormUrl);

            var portalUrl = "";

            if (hasCustomerPortalUrl)
                portalUrl = customer.ParticipantInfoDefinition.CustomerPortalUrl;
            else if (hasDistributorPortalUrl)
                portalUrl = distributor.LoginFormUrl;
            else
                portalUrl = ConfigurationManager.AppSettings["MyMetierDefaultLogin"];

            var fields = new Dictionary<string, string>
            {
                { "customer-portal", portalUrl},
                { "customer-name", HttpUtility.HtmlEncode(customer.Name) }
            };
            return fields;
        }
    }
}