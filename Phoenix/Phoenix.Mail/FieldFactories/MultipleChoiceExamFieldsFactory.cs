﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;

namespace Metier.Phoenix.Mail.FieldFactories
{
    public interface IMultipleChoiceExamFieldsFactory
    {
        Dictionary<string, string> CreateFields(int examActivityId, int userId);
    }

    public class MultipleChoiceExamFieldsFactory : IMultipleChoiceExamFieldsFactory
    {
        private readonly MetierLmsContext _context;

        public MultipleChoiceExamFieldsFactory(MetierLmsContext context)
        {
            _context = context;
        }

        public Dictionary<string, string> CreateFields(int examActivityId, int userId)
        {
            var activity = _context.Activities.First(a => a.Id == examActivityId);

            var fields = new Dictionary<string, string>();

            var multiplechoiceCourseContent = GetCourseContent(activity);
            fields.Add("multiplechoice-course-content", multiplechoiceCourseContent);

            return fields;
        }

        private int GetNumberOfCompletedCourses(int userId, IEnumerable<Rco> qualifyingRcos)
        {
            var rcoIds = qualifyingRcos.Select(r => r.Id).ToList();
            return _context.Participants.Where(p => p.UserId == userId &&
                                                    (p.StatusCodeValue == ParticipantStatusCodes.Completed ||
                                                     p.StatusCodeValue == ParticipantStatusCodes.Dispensation))
                .Join(_context.Activities, p => p.ActivityId, a => a.Id, (p, a) => a)
                .Count(a => a.RcoCourseId.HasValue && rcoIds.Contains(a.RcoCourseId.Value));
        }

        private static string GetFormattedUserExamCourseEnrollmentsString(IOrderedEnumerable<Rco> qualifyingRcos)
        {
            var sb = new StringBuilder();
            sb.Append("<ul>");

            foreach (var rco in qualifyingRcos)
            {
                sb.Append(string.Format("<li>{0} ({1})</li>", rco.Name, rco.Version));
            }

            sb.Append("</ul>");
            var userExamCourseEnrollments = sb.ToString();
            return userExamCourseEnrollments;
        }

        private string GetCourseContent(Activity activity)
        {
            var rcoIds = activity.ActivityExamCourses.Select(a => a.CourseRcoId).ToList();

            if (rcoIds.Count == 0)
            {
                return string.Empty;
            }

            var rcos = _context.Rcos.Where(r => rcoIds.Contains(r.Id)).OrderBy(r => r.Name);

            var sb = new StringBuilder();
            sb.Append("<ul>");

            foreach (var rco in rcos)
            {
                sb.Append(string.Format("<li>{0} ({1})</li>", rco.Name, rco.Version));
            }

            sb.Append("</ul>");

            return sb.ToString();
        }
    }
}