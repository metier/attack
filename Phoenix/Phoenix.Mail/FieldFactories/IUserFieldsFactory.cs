﻿using System.Collections.Generic;

namespace Metier.Phoenix.Mail.FieldFactories
{
    public interface IUserFieldsFactory
    {
        Dictionary<string, string> CreateFields(int userId); 
    }
}