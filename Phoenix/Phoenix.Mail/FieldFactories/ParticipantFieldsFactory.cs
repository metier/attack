﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Metier.Phoenix.Core.Business;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Mail.Formatters;
using Metier.Phoenix.Mail.Helpers;
using Metier.Phoenix.Mail.Resources;

namespace Metier.Phoenix.Mail.FieldFactories
{
    public class ParticipantFieldsFactory : IParticipantFieldsFactory
    {
        private const string HideElementInlineCss = "display:none !important; font-size: 0; max-height: 0; line-height: 0; mso-hide: all;";

        private readonly MetierLmsContext _context;
        private readonly IAddressFormatter _addressFormatter;
        private readonly IElearningCourseHelper _elearningCourseHelper;
        private readonly IParticipantBusiness _participantBusiness;

        public ParticipantFieldsFactory(MetierLmsContext context,
            IAddressFormatter addressFormatter,
            IElearningCourseHelper elearningCourseHelper,
            IParticipantBusiness participantBusiness)
        {
            _context = context;
            _addressFormatter = addressFormatter;
            _elearningCourseHelper = elearningCourseHelper;
            _participantBusiness = participantBusiness;
        }

        public Dictionary<string, string> CreateFields(int participantId, string languageIdentifier, params string[] fieldNames)
        {
            var participant = _context.Participants.First(p => p.Id == participantId);
            var activitySet = _context.ActivitySets.First(aset => aset.Id == participant.ActivitySetId);
            var activity = _context.Activities.Include(a => a.ActivityAutomailText).Include(a => a.ActivityPeriods).Include("CustomerArticle.ArticleCopy").Include("Resources.ResourceType").Include("Resources.Addresses").First(a => a.Id == participant.ActivityId);
            var activityPeriodStart = activity.ActivityPeriods.OrderBy(ap => ap.Start).FirstOrDefault();
            var activityPeriodEnd = activity.ActivityPeriods.OrderByDescending(ap => ap.End).FirstOrDefault();

            var fields = new Dictionary<string, string>();

            Add(fields, "activityset-title", HttpUtility.HtmlEncode(activitySet.Name), fieldNames);
            Add(fields, "activity-title", HttpUtility.HtmlEncode(activity.Name), fieldNames);
            Add(fields, "activity-start-date", activityPeriodStart != null ? (activityPeriodStart.Start.HasValue ? activityPeriodStart.Start.Value.ToString("dd.MM.yyyy") : string.Empty) : string.Empty, fieldNames);
            Add(fields, "activity-end-date", activityPeriodEnd != null ? (activityPeriodEnd.End.HasValue ? activityPeriodEnd.End.Value.ToString("dd.MM.yyyy") : string.Empty) : string.Empty, fieldNames);
            Add(fields, "activity-start-time", activityPeriodStart != null ? (activityPeriodStart.Start.HasValue ? activityPeriodStart.Start.Value.ToString("HH:mm") : string.Empty) : string.Empty, fieldNames);
            Add(fields, "activity-end-time", activityPeriodEnd != null ? (activityPeriodEnd.End.HasValue ? activityPeriodEnd.End.Value.ToString("HH:mm") : string.Empty) : string.Empty, fieldNames);
			Add(fields, "dates-visibility", activity.HideActivityDates ? HideElementInlineCss : string.Empty, null);//fieldNames
			Add(fields, "customer-article-name", activity.CustomerArticle != null && activity.CustomerArticle.ArticleCopy != null ? activity.CustomerArticle.ArticleCopy.Title : string.Empty, null); //fieldNames


			if (IncludeField(fieldNames, "article-language"))
            {
                var articleLanguage = _context.CustomerArticles.Where(c => c.Id == activity.CustomerArticleId)
                                    .Join(_context.Articles, c => c.ArticleCopyId, a => a.Id, (c, a) => a)
                                    .Join(_context.Languages, a => a.LanguageId, l => l.Id, (a, l) => l)
                                    .FirstOrDefault();

                Add(fields, "article-language", articleLanguage == null ? string.Empty : articleLanguage.Name, fieldNames);        
            }

            if (IncludeField(fieldNames, "due-date") || IncludeField(fieldNames, "due-time"))
            {
                var dueDate = _participantBusiness.GetDueDate(participantId);
                if (dueDate != null)
                {
                    var dueDateInLocalTime = (activityPeriodStart != null && activityPeriodStart.Start.HasValue) ?  dueDate.Value.AddHours(activityPeriodStart.Start.Value.Offset.TotalHours): dueDate;

                    Add(fields, "due-date", dueDateInLocalTime.Value.ToString("dd.MM.yyyy"), fieldNames);
					Add(fields, "due-date-if-different-than-activity-start-date", (dueDateInLocalTime.Value.ToString("dd.MM.yyyy") != (activityPeriodStart.Start.HasValue ? activityPeriodStart.Start.Value.ToString("dd.MM.yyyy") : "") ? dueDateInLocalTime.Value.ToString("dd.MM.yyyy") : ""), fieldNames);
					Add(fields, "due-time", dueDateInLocalTime.Value.ToString("HH:mm"), fieldNames);
                    Add(fields, "due-time-visibility", activity.EndDate.HasValue ? HideElementInlineCss : string.Empty, fieldNames);
                }
            }


            if (IncludeField(fieldNames, "activity-address-visibility") || IncludeField(fieldNames, "activity-address"))
            {
                var locationResource = activity.Resources.FirstOrDefault(r => r.ResourceType.Name == "Location");
                var locationInfo = string.Empty;

                if(locationResource != null)
                {
                    locationInfo = locationResource.Name == null ? string.Empty : locationResource.Name.Trim();
                    var locationAddress = _addressFormatter.GetAddressFromResource(locationResource);

                    if (string.IsNullOrEmpty(locationInfo))
                        locationInfo = locationAddress;
                    else if(!String.IsNullOrWhiteSpace(locationAddress))
                        locationInfo += ", " + locationAddress;
                }

                Add(fields, "activity-address-visibility", string.IsNullOrEmpty(locationInfo) ? HideElementInlineCss : string.Empty, fieldNames);
                Add(fields, "activity-address", string.IsNullOrEmpty(locationInfo) ? string.Empty : HttpUtility.HtmlEncode(locationInfo), fieldNames);
            }

            if (IncludeField(fieldNames, "instructor-visibility") || IncludeField(fieldNames, "instructor"))
            {
                var instructorResource = activity.Resources.FirstOrDefault(r => r.ResourceType.Name == "Instructor");
                Add(fields, "instructor-visibility", instructorResource == null ? HideElementInlineCss : string.Empty, fieldNames);
                Add(fields, "instructor", instructorResource != null ? HttpUtility.HtmlEncode(instructorResource.Name) : string.Empty, fieldNames);
            }

            if (IncludeField(fieldNames, "activity-periods"))
            {
                var activityPeriods = GetActivityPeriods(activity.ActivityPeriods, languageIdentifier);
                fields.Add("activity-periods", activityPeriods);
            }


			var ecoachResource = activity.Resources.FirstOrDefault(r => r.ResourceType.Name == "E-Coach");
			if (IncludeField(fieldNames, "e-coach-visibility") || IncludeField(fieldNames, "e-coach"))
            {                
                Add(fields, "e-coach-visibility", ecoachResource == null ? HideElementInlineCss : string.Empty, fieldNames);
                Add(fields, "e-coach", ecoachResource != null ? HttpUtility.HtmlEncode(ecoachResource.Name) : string.Empty, fieldNames);
            }
			
			if (IncludeField(fieldNames, "e-learning-deadline"))
            {
                var elearningDeadline = _elearningCourseHelper.GetElearningDeadline(participant, activity);
                fields.Add("e-learning-deadline", elearningDeadline.HasValue ? elearningDeadline.Value.ToString("dd.MM.yyyy H:mm") : string.Empty);
            }
            
            if (IncludeField(fieldNames, "e-learning-schedule"))
            {
                var elearningSchedule = _elearningCourseHelper.GetElearningSchedule(participant, activity, languageIdentifier);
                fields.Add("e-learning-schedule", elearningSchedule);
            }

            if (IncludeField(fieldNames, "enrollment-conditions-visibility") || IncludeField(fieldNames, "enrollment-conditions"))
            {
                var hasEnrollmentConditions = activity.EnrollmentConditions?.Count > 0;
                Add(fields, "enrollment-conditions-visibility", hasEnrollmentConditions ? string.Empty : HideElementInlineCss, fieldNames);
                Add(fields, "enrollment-conditions", hasEnrollmentConditions ? activity.ActivityAutomailText.Text : string.Empty, fieldNames);
            }

            if (IncludeField(fieldNames, "additional-information-visibility") || IncludeField(fieldNames, "additional-information"))
            {
                var hasAdditionalText = activity.ActivityAutomailText != null &&
                                        !string.IsNullOrWhiteSpace(RemoveHtmlTags(RemoveHtmlEncoding(activity.ActivityAutomailText.Text)).Replace(System.Environment.NewLine, string.Empty));
                Add(fields, "additional-information-visibility", hasAdditionalText ? string.Empty : HideElementInlineCss, fieldNames);
                Add(fields, "additional-information", hasAdditionalText ? activity.ActivityAutomailText.Text : string.Empty, fieldNames);
            }

            return fields;
        }

        private string RemoveHtmlEncoding(string html)
        {
            return System.Net.WebUtility.HtmlDecode(html);
        }

        private string RemoveHtmlTags(string html)
        {
            return string.IsNullOrEmpty(html) ? "" : Regex.Replace(html, "<.+?>", string.Empty);
        }

        private void Add(Dictionary<string, string> fields, string fieldName, string value, string[] fieldNames)
        {
            if (IncludeField(fieldNames, fieldName))
            {
				//Overwrite existing value, if it exists
				fields[fieldName] = value;
            }
        }

        private bool IncludeField(string[] fieldNames, string fieldName)
        {
            return fieldNames == null || !fieldNames.Any() || fieldNames.Contains(fieldName);
        }

        private string GetTerm(string term, string languageIdentifier)
        {
            var resourceManager = new ResourceManager(typeof (Terms));
            var culture = LanguageToCultureMapper.GetCulture(languageIdentifier);
            return resourceManager.GetString(term, culture);
        }

        private string GetActivityPeriods(IEnumerable<ActivityPeriod> activityPeriods, string languageIdentifier)
        {
            var dateString = GetTerm("Date", languageIdentifier);
            var timeString = GetTerm("Time", languageIdentifier);

            var sb = new StringBuilder();
            foreach (var activityPeriod in activityPeriods.OrderBy(a => a.Start))
            {
                sb.Append("<p>");
                sb.Append("<strong>");
                sb.Append(dateString + ":");
                sb.Append("</strong> ");
				var toAndFromDateAreDifferent = !activityPeriod.Start.HasValue || !activityPeriod.End.HasValue || activityPeriod.Start.Value.Date != activityPeriod.End.Value.Date;
				
				if (activityPeriod.Start.HasValue)
                {
                    sb.Append(activityPeriod.Start.Value.ToString("dd.MM.yyyy"));
                }

				if (toAndFromDateAreDifferent)
				{
					sb.Append(" - ");
				}
                if (activityPeriod.End.HasValue && toAndFromDateAreDifferent)
                {
                    sb.Append(activityPeriod.End.Value.ToString("dd.MM.yyyy"));
                }
                sb.Append("<br/>");

                sb.Append("<strong>");
                sb.Append(timeString + ":");
                sb.Append("</strong> ");

                if (activityPeriod.Start.HasValue)
                {
                    sb.Append(activityPeriod.Start.Value.ToString("HH:mm"));
                }
                sb.Append(" - ");
                if (activityPeriod.End.HasValue)
                {
                    sb.Append(activityPeriod.End.Value.ToString("HH:mm"));
                }

                sb.Append("</p>");
            }
            return sb.ToString();
        }
    }
}