﻿using System.Collections.Generic;

namespace Metier.Phoenix.Mail.FieldFactories
{
    public interface ICustomerFieldsFactory
    {
        Dictionary<string, string> CreateFields(int customerId); 
    }
}