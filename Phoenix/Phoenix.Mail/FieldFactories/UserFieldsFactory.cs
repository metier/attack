﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;

namespace Metier.Phoenix.Mail.FieldFactories
{
    public class UserFieldsFactory : IUserFieldsFactory
    {
        private const string HideElementInlineCss = "display:none !important; font-size: 0; max-height: 0; line-height: 0; mso-hide: all;";

        private readonly MetierLmsContext _context;

        public UserFieldsFactory(MetierLmsContext context)
        {
            _context = context;
        }

        public Dictionary<string, string> CreateFields(int userId)
        {

            var user = _context.Users.Include("UserInfoElements.LeadingText").Include(u=>u.Customer).First(u => u.Id == userId);
            var searchableUser = _context.SearchableUsers.First(u => u.Id == userId);
            var supervisor = user.UserInfoElements.Where(ue => ue.LeadingTextId == LeadingTextIds.Supervisor)
                                                  .Select(ue => ue.InfoText).FirstOrDefault() ?? string.Empty;
			
			var contactEmail = ""; 			
			var distributorId = user.Customer?.DistributorId;
			if (distributorId.HasValue)
			{
				var distributor = _context.Distributors.First(d => d.Id == distributorId.Value);
				if (distributor != null && !string.IsNullOrEmpty(distributor.ContactEmail))
					contactEmail = distributor.ContactEmail;
			}
			
						
			var fields = new Dictionary<string, string>
            {
                { "first-name", HttpUtility.HtmlEncode(user.FirstName) },
                { "last-name", HttpUtility.HtmlEncode(user.LastName) },
                { "username", searchableUser.UserName },
                { "user-fullname", HttpUtility.HtmlEncode(string.Format("{0} {1}", user.FirstName, user.LastName)) },
                { "user-email", searchableUser.Email },
                { "participant-supervisor-visibility", string.IsNullOrEmpty(supervisor) ? HideElementInlineCss : string.Empty },
                { "participant-supervisor", HttpUtility.HtmlEncode(supervisor) },
				{ "contact-email", contactEmail }
			};
            return fields;
        }
    }
}