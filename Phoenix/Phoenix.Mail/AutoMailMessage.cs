﻿using Metier.Phoenix.Core.Data.Enums;
using System;

namespace Metier.Phoenix.Mail
{
    public class AutoMailMessage
    {
        public MessageType MessageType { get; set; }
        public string SenderEmail { get; set; }
        public string RecipientEmail { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string DistributorLogoUrl { get; set; }
        public int? UserId { get; set; }
        public int? ParticipantId { get; set; }
        public int? ActivityId { get; set; }

        public CalendarInvitation CalendarInvitation { get; set; }
    }
}