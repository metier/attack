﻿using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Mail.Formatters
{
    public interface IAddressFormatter
    {
        string GetAddressFromResource(Resource resource);
    }
}