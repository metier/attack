﻿using System.Linq;
using System.Text;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Mail.Formatters
{
    public class AddressFormatter : IAddressFormatter        
    {
        private const int CountryCodeType = 4;
        private readonly MetierLmsContext _context;

        public AddressFormatter(MetierLmsContext context)
        {
            _context = context;
        }

        public string GetAddressFromResource(Resource resource)
        {
            var sb = new StringBuilder();

            if (resource != null)
            {
                var address = resource.Addresses.FirstOrDefault();
                if (address != null)
                {
                    var streetAddress = GetStreetAddress(address);
                    if (!string.IsNullOrEmpty(streetAddress))
                    {
                        sb.Append(streetAddress + ", ");
                    }

                    var cityAddress = GetCityAddress(address);
                    if (!string.IsNullOrEmpty(cityAddress))
                    {
                        sb.Append(cityAddress + ", ");
                    }
                    
                    var countryCode = _context.Codes.FirstOrDefault(c => c.CodeTypeId == CountryCodeType && c.Value == address.Country);
                    if (countryCode != null)
                    {
                        sb.Append(countryCode.Description);
                    }
                }    
            }
            
            return sb.ToString().TrimEnd(new[] { ' ', ',' });
        }

        private string GetCityAddress(ResourceAddress address)
        {
            if (string.IsNullOrEmpty(address.ZipCode) && string.IsNullOrEmpty(address.City))
            {
                return string.Empty;
            }
            if (string.IsNullOrEmpty(address.ZipCode))
            {
                return address.City;
            }
            if (string.IsNullOrEmpty(address.City))
            {
                return address.ZipCode;
            }
            return address.ZipCode + " " + address.City;
        }

        private string GetStreetAddress(ResourceAddress address)
        {
            if (string.IsNullOrEmpty(address.StreetAddress) && string.IsNullOrEmpty(address.StreetAddress2))
            {
                return string.Empty;
            }
            if (string.IsNullOrEmpty(address.StreetAddress2))
            {
                return address.StreetAddress;
            }
            if (string.IsNullOrEmpty(address.StreetAddress))
            {
                return address.StreetAddress2;
            }
            return address.StreetAddress + ", " + address.StreetAddress2;
        }
    }
}