﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Web;
using Castle.Core.Logging;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Mail.Configuration;
using Metier.Phoenix.Mail.Configuration.ConfigurationSections;
using Metier.Phoenix.Mail.Resources;
using NLog;

namespace Metier.Phoenix.Mail.Templates
{
    public class MailMessageFactory : IMailMessageFactory
    {
        private readonly Dictionary<string, string> _generalFields = new Dictionary<string, string>();
		private const string HideElementInlineCss = "display:none !important; font-size: 0; max-height: 0; line-height: 0; mso-hide: all;";
        //private readonly Logger _logger;
        //, Logger logger

        public MailMessageFactory(IAutoMailConfiguration autoMailConfiguration)
        {
            //_logger = logger;
            var metierLogo = autoMailConfiguration.Urls.OfType<UrlsConfigurationElement>().FirstOrDefault(u => u.Key == "metierLogo");
            if (metierLogo != null)
            {
                _generalFields.Add("metier-logo-url", metierLogo.Url);    
            }


			var mailBanner = autoMailConfiguration.Urls.OfType<UrlsConfigurationElement>().FirstOrDefault(u => u.Key == "mailBanner");
			if (mailBanner != null)
			{
				_generalFields.Add("mailbilde-url", mailBanner.Url);
			}
		}

        public string GetSubject(string resourceName, string languageIdentifier, Distributor distributor = null, Dictionary<string, string> fields = null)
        {
            var subject = "";

            var resourceManager = new ResourceManager(typeof (SubjectTitles));

            if(distributor!= null)
            {
                //I just learned that a resourcename can not start with a digit, so adding an underscore to the beginning of the name.
                var distributorSpecificResourceName = "_" + distributor.Id + "_" + resourceName;
                subject = resourceManager.GetString(distributorSpecificResourceName, LanguageToCultureMapper.GetCulture(languageIdentifier));
            }

            if(string.IsNullOrWhiteSpace(subject))
                subject = resourceManager.GetString(resourceName, LanguageToCultureMapper.GetCulture(languageIdentifier));


            if (fields != null)
            {
                subject = HttpUtility.HtmlDecode(ApplyKeywordValuesAndReturnString(subject, fields));
            }
            return subject;
        }

        public string GetBody(string htmlResourcePath, Dictionary<string, string> fields, Distributor distributor = null, string languageIdentifier = null)
        {
			//Default e-coach-visibility to Hide
			if (fields?.ContainsKey("e-coach-visibility") == false)
				fields.Add("e-coach-visibility", HideElementInlineCss);

			if (fields?.ContainsKey("additional-information-visibility") == false)
				fields.Add("additional-information-visibility", HideElementInlineCss);


			var distributorId = 0;
            if (distributor != null)
            {
                distributorId = distributor.Id;
                var logoUrlKey = distributorId + "-logo-url";

                if (!string.IsNullOrWhiteSpace(distributor.LogoUrl) && !_generalFields.ContainsKey(logoUrlKey))
                    _generalFields.Add(logoUrlKey, distributor.LogoUrl);
            }

            using (var stream = ResolveResourceStream(htmlResourcePath, distributorId, languageIdentifier))
            using (var reader = new StreamReader(stream))
            {
                var templateContent = reader.ReadToEnd();
                return ApplyKeywordValuesAndReturnString(templateContent, fields);
            }
        }

        private string ApplyKeywordValuesAndReturnString(string keywordedString, Dictionary<string, string> keywordValues)
        {
            try
            {
                //_logger.Error(string.Join("; ", keywordValues.Select(z => ((!string.IsNullOrEmpty(z.Value) ? z.Value : "Empty") + " -- " + (!string.IsNullOrEmpty(z.Key) ? z.Key : "KeyEmpty")))));
                return keywordValues.Concat(_generalFields).Aggregate(keywordedString,
               (memo, keywordEntry) => memo.Replace("{" + keywordEntry.Key + "}", keywordEntry.Value));
            }
            catch (Exception e)
            {
                //_logger.Error(e);
                return keywordedString;
            }
        }

        private Stream ResolveResourceStream(string htmlResourcePath, int distributorId, string languageIdentifier)
        {
            var assembly = Assembly.GetAssembly(typeof (MailMessageFactory));
            var distributorSpecificResourcePath = ConstructDistributorSpecificResourcePath(htmlResourcePath, distributorId);

            //First check for a matching resource on this format: <distributorId>_<resourcePath>_<languageIdentifierName>.html
            //Note: If languageIdentifier is an empty string, the resourcePath will be on this format:  <distributorId>_<resourcePath>.html,
            //so no need to specifically try to retrieve the corresponding resource for this path.
            var languageSpecificResourcePath = ConstructLanguageSpecificResourcePath(distributorSpecificResourcePath, languageIdentifier);
            if (assembly.GetManifestResourceNames().Contains(languageSpecificResourcePath))
                return assembly.GetManifestResourceStream(languageSpecificResourcePath);

            //If no language specific template exists for this distributor, check for a matching resource on this format: <distributorId>_<resourcePath>.html
            if (assembly.GetManifestResourceNames().Contains(distributorSpecificResourcePath))
                return assembly.GetManifestResourceStream(distributorSpecificResourcePath);

            //If no match, check for a matching resource on this format: <resourcePath>_<languageIdentifierName>.html
            //Note: If languageIdentifier is an empty string, the resourcePath will be on this format: <resourcePath>.html,
            languageSpecificResourcePath = ConstructLanguageSpecificResourcePath(htmlResourcePath, languageIdentifier);
            if (assembly.GetManifestResourceNames().Contains(languageSpecificResourcePath))
                return assembly.GetManifestResourceStream(languageSpecificResourcePath);

            //If no language specific template exists, check for a matching resource on this format: <resourcePath>.html
            if (assembly.GetManifestResourceNames().Contains(htmlResourcePath))
                return assembly.GetManifestResourceStream(htmlResourcePath);

            throw new ArgumentException(string.Format("The resource {0} does not exist in assembly {1}.", htmlResourcePath, assembly.FullName), htmlResourcePath);
        }

        private string ConstructDistributorSpecificResourcePath(string htmlResourcePath, int distributorId)
        {
            return htmlResourcePath.Insert(htmlResourcePath.Replace(".html", "").LastIndexOf(".") + 1, distributorId + "_");
        }
        private string ConstructLanguageSpecificResourcePath(string htmlResourcePath, string languageIdentifier)
        {
            if(string.IsNullOrWhiteSpace(languageIdentifier))
                return htmlResourcePath;

            return htmlResourcePath.Insert(htmlResourcePath.Length - ".html".Length,
                "_" + LanguageToCultureMapper.GetCulture(languageIdentifier).Name);

        }

    }
}