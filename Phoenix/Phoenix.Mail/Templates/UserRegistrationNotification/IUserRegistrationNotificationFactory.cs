﻿using System;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Mail.Templates.UserRegistrationNotification
{
    public interface IUserRegistrationNotificationFactory
    {
        AutoMailMessage CreateUserRegistrationNotificationMessage(User user, string passwordResetFormUrl, Guid? resetToken);
    }
}