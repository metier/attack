﻿using System;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Mail.FieldFactories;
using Metier.Phoenix.Mail.Helpers;
using Metier.Phoenix.Core.Utilities;

namespace Metier.Phoenix.Mail.Templates.UserRegistrationNotification
{
    public class UserRegistrationNotificationFactory : IUserRegistrationNotificationFactory
    {
        private const string ResourceName = "UserRegistrationNotification";
        private readonly string _templateResourceName = typeof(UserRegistrationNotificationFactory).Namespace + ".UserRegistrationNotificationMessage.html";

        private readonly IMailMessageFactory _mailMessageFactory;
        private readonly MetierLmsContext _context;
        private readonly ILanguageHelper _languageHelper;
        private readonly IUserFieldsFactory _userFieldsFactory;
        private readonly ICustomerFieldsFactory _customerFieldsFactory;

        public UserRegistrationNotificationFactory(IMailMessageFactory mailMessageFactory, MetierLmsContext context, ILanguageHelper languageHelper, 
            IUserFieldsFactory userFieldsFactory, ICustomerFieldsFactory customerFieldsFactory)
        {
            _mailMessageFactory = mailMessageFactory;
            _context = context;
            _languageHelper = languageHelper;
            _userFieldsFactory = userFieldsFactory;
            _customerFieldsFactory = customerFieldsFactory;
        }

        public AutoMailMessage CreateUserRegistrationNotificationMessage(User user, string passwordResetFormUrl, Guid? resetToken)
        {
            var searchableUser = _context.SearchableUsers.First(u => u.Id == user.Id);
            var customer = _context.Customers.Include(c => c.ParticipantInfoDefinition).First(c => c.Id == searchableUser.CustomerId);
            var distributor = _context.Distributors.First(d => d.Id == customer.DistributorId);
            var languageIdentifier = _languageHelper.GetPreferredLanguageIdentifier(user, customer);

            var userFields = _userFieldsFactory.CreateFields(user.Id);
            var customerFields = _customerFieldsFactory.CreateFields(user.CustomerId);
			
			var templateResourceName = _templateResourceName;
			if(!string.IsNullOrEmpty(passwordResetFormUrl) && resetToken.HasValue)
			{				
				userFields.Add("password-reset-url", GetPasswordResetUrl(passwordResetFormUrl, resetToken.ToString()));
				templateResourceName = typeof(UserRegistrationNotificationFactory).Namespace + ".UserRegistrationNotificationMessageWithPassword.html";
			}
			
			var fields = userFields.Concat(customerFields).ToDictionary(f => f.Key, f => f.Value);

            var message = new AutoMailMessage
            {
                MessageType = MessageType.UserRegistrationNotification,
                SenderEmail = distributor.AutomailSender,
                RecipientEmail = searchableUser.Email,
                Subject = _mailMessageFactory.GetSubject(ResourceName, languageIdentifier, distributor),
                Body = _mailMessageFactory.GetBody(templateResourceName, fields , distributor, languageIdentifier),
                UserId = user.Id
            };
            return message;
        }

		private string GetPasswordResetUrl(string urlBase, string token)
		{
			return new UrlBuilder(urlBase).AddParameter("token", token).ToString();
		}
	}
}