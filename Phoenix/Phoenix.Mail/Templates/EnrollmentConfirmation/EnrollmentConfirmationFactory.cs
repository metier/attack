﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Mail.FieldFactories;
using Metier.Phoenix.Mail.Helpers;
using NLog;

namespace Metier.Phoenix.Mail.Templates.EnrollmentConfirmation
{
    public class EnrollmentConfirmationFactory : IEnrollmentConfirmationFactory
    {
        private readonly IMailMessageFactory _mailMessageFactory;
        private readonly IParticipantFieldsFactory _participantFieldsFactory;
        private readonly ICustomerFieldsFactory _customerFieldsFactory;
        private readonly IUserFieldsFactory _userFieldsFactory;
        private readonly IMultipleChoiceExamFieldsFactory _multipleChoiceExamFieldsFactory;
        private readonly ILanguageHelper _languageHelper;
        private readonly MetierLmsContext _context;
        private const string ResourceName = "EnrollmentConfirmation";
        private readonly string _templateResourceName = typeof(EnrollmentConfirmationFactory).Namespace + ".EnrollmentConfirmationMessage.html";
        private readonly string _templateResourceNameElearning = typeof(EnrollmentConfirmationFactory).Namespace + ".EnrollmentConfirmationMessageElearning.html";
        private readonly string _templateResourceNameElearningNoProgressSchedule = typeof(EnrollmentConfirmationFactory).Namespace + ".EnrollmentConfirmationMessageElearningNoProgressSchedule.html";
        private readonly string _templateResourceNameCaseExamProjectAssignment = typeof(EnrollmentConfirmationFactory).Namespace + ".EnrollmentConfirmationMessageCaseExamProjectAssignment.html";
        private readonly string _templateResourceNameMultipleChoice = typeof(EnrollmentConfirmationFactory).Namespace + ".EnrollmentConfirmationMessageMultipleChoice.html";
        private readonly string _templateResourceNameExternalCertification = typeof(EnrollmentConfirmationFactory).Namespace + ".EnrollmentConfirmationExternalCertification.html";
        private readonly string _templateResourceNameInternalCertification = typeof(EnrollmentConfirmationFactory).Namespace + ".EnrollmentConfirmationInternalCertification.html";
        private readonly string _emptyTemplateResourceName = typeof(EnrollmentConfirmationFactory).Namespace + ".EmptyEnrollmentConfirmationMessage.html";
        private readonly Logger _logger;

        public EnrollmentConfirmationFactory(IMailMessageFactory mailMessageFactory, 
                                            IParticipantFieldsFactory participantFieldsFactory, 
                                            ICustomerFieldsFactory customerFieldsFactory, 
                                            IUserFieldsFactory userFieldsFactory,
                                            IMultipleChoiceExamFieldsFactory multipleChoiceExamFieldsFactory,
                                            ILanguageHelper languageHelper,
                                            MetierLmsContext context)
        {
            _mailMessageFactory = mailMessageFactory;
            _participantFieldsFactory = participantFieldsFactory;
            _customerFieldsFactory = customerFieldsFactory;
            _userFieldsFactory = userFieldsFactory;
            _multipleChoiceExamFieldsFactory = multipleChoiceExamFieldsFactory;
            _languageHelper = languageHelper;
            _context = context;
            _logger = LogManager.GetCurrentClassLogger();

        }

        public IEnumerable<AutoMailMessage> CreateMultipleMessage(Participant participant, bool includeCalendarInvitation) {
            var user = _context.Users.Include(u => u.Customer).First(u => u.Id == participant.UserId);
            var customer = _context.Customers.Include(c => c.AutomailDefinition).First(c => c.Id == user.CustomerId);
            var distributor = _context.Distributors.First(d => d.Id == customer.DistributorId);

            var languageIdentifier = _languageHelper.GetPreferredLanguageIdentifier(user, user.Customer);

            var searchableUser = _context.SearchableUsers.First(u => u.Id == participant.UserId);

            var participantFields = _participantFieldsFactory.CreateFields(participant.Id, languageIdentifier);
            var userFields = _userFieldsFactory.CreateFields(participant.UserId);
            var customerFields = _customerFieldsFactory.CreateFields(participant.CustomerId);

            var fields = participantFields.Concat(userFields).Concat(customerFields).ToDictionary(f => f.Key, f => f.Value);

            var articleType = GetArticleType(participant);
            var templateName = GetTemplateName(articleType, customer.AutomailDefinition);
            var activity = _context.Activities.Where(c => c.Id == participant.ActivityId).FirstOrDefault();
            var activitySet = _context.ActivitySets.Where(c => c.Id == participant.ActivitySetId).FirstOrDefault();

            string enumName = typeof(ArticleTypes).GetEnumName(articleType);

            if (templateName == _templateResourceNameMultipleChoice)
            {
                var multipleChoiceExamFields = _multipleChoiceExamFieldsFactory.CreateFields(participant.ActivityId, participant.UserId);
                fields = fields.Concat(multipleChoiceExamFields).ToDictionary(f => f.Key, f => f.Value);
            }
            if (articleType == (int)ArticleTypes.Classroom ||
                articleType == (int)ArticleTypes.CaseExam)
            {
                var locationField = string.Empty;
                var hasLocationField = string.Empty;
                var articleName = string.Empty;
                fields.TryGetValue("activity-address", out locationField);
                fields.TryGetValue("activity-address-visibility", out hasLocationField);
                fields.TryGetValue("customer-article-name", out articleName);
                foreach (var period in activity.ActivityPeriods)
                {
                    _logger.Error($" Periods S: {(period.Start.HasValue ? period.Start.ToString() : "Empty")}, E: {(period.End.HasValue ? period.End.ToString() : "Empty")} ");
                }

                return activity.ActivityPeriods.Select(p => new AutoMailMessage {
                    MessageType = MessageType.EnrollmentConfirmation,
                    SenderEmail = distributor.AutomailSender,
                    RecipientEmail = searchableUser.Email,
                    Subject = _mailMessageFactory.GetSubject(ResourceName, languageIdentifier, distributor, fields),
                    Body = _mailMessageFactory.GetBody(templateName, fields, distributor, languageIdentifier),
                    UserId = participant.UserId,
                    ParticipantId = participant.Id,
                    CalendarInvitation = new CalendarInvitation
                    {
                        IncludeCalendarInvitation = true,
                        CalendarTitle = activitySet.Name,
                        CalendarDescription = $"{articleName} {activitySet.Name}",
                        EndTime = p.End.Value.DateTime,
                        StartTime = p.Start.Value.DateTime,
                        Location = locationField,
                        ReceiverName = $"{participant.User.FirstName} {participant.User.LastName}",
                        OrganizerName = "Metier"
                    }
                });
            }
            else
            {
                return new List<AutoMailMessage>();
            }
        }

        public AutoMailMessage CreateMessage(Participant participant, bool includeCalendarInvitation)
        {
            var user = _context.Users.Include(u => u.Customer).First(u => u.Id == participant.UserId);
            var customer = _context.Customers.Include(c => c.AutomailDefinition).First(c => c.Id == user.CustomerId);
            var distributor = _context.Distributors.First(d => d.Id == customer.DistributorId);
            
            var languageIdentifier = _languageHelper.GetPreferredLanguageIdentifier(user, user.Customer);

            var searchableUser = _context.SearchableUsers.First(u => u.Id == participant.UserId);

            var participantFields = _participantFieldsFactory.CreateFields(participant.Id, languageIdentifier);
            var userFields = _userFieldsFactory.CreateFields(participant.UserId);
            var customerFields = _customerFieldsFactory.CreateFields(participant.CustomerId);

            var fields = participantFields.Concat(userFields).Concat(customerFields).ToDictionary(f => f.Key, f => f.Value);

            var articleType = GetArticleType(participant);
            var templateName = GetTemplateName(articleType, customer.AutomailDefinition);
            var activity = _context.Activities.Where(c => c.Id == participant.ActivityId).FirstOrDefault();
            var activitySet = _context.ActivitySets.Where(c => c.Id == participant.ActivitySetId).FirstOrDefault();

            string enumName = typeof(ArticleTypes).GetEnumName(articleType);

            if (templateName == _templateResourceNameMultipleChoice)
            {
                var multipleChoiceExamFields = _multipleChoiceExamFieldsFactory.CreateFields(participant.ActivityId, participant.UserId);
                fields = fields.Concat(multipleChoiceExamFields).ToDictionary(f => f.Key, f => f.Value);
            }
            if(articleType == (int) ArticleTypes.Classroom ||
                articleType == (int)ArticleTypes.CaseExam)
            {
                var locationField = string.Empty;
                var hasLocationField = string.Empty;
                var articleName = string.Empty;
                fields.TryGetValue("activity-address", out locationField);
                fields.TryGetValue("activity-address-visibility", out hasLocationField);
                fields.TryGetValue("customer-article-name", out articleName);
                
                return new AutoMailMessage
                {
                    MessageType = MessageType.EnrollmentConfirmation,
                    SenderEmail = distributor.AutomailSender,
                    RecipientEmail = searchableUser.Email,
                    Subject = _mailMessageFactory.GetSubject(ResourceName, languageIdentifier, distributor, fields),
                    Body = _mailMessageFactory.GetBody(templateName, fields, distributor, languageIdentifier),
                    UserId = participant.UserId,
                    ParticipantId = participant.Id,
                    CalendarInvitation = new CalendarInvitation
                    {
                        IncludeCalendarInvitation = true,
                        CalendarTitle = activitySet.Name,
                        CalendarDescription = $"{articleName} {activitySet.Name}",
                        EndTime = activity.ActivityPeriods.First().Start.Value.DateTime,
                        StartTime = activity.ActivityPeriods.First().End.Value.DateTime,
                        Location = locationField,
                        ReceiverName = $"{participant.User.FirstName} {participant.User.LastName}",
                        OrganizerName = "Metier"
                    }
                };
            } else
            {
                return new AutoMailMessage
                {
                    MessageType = MessageType.EnrollmentConfirmation,
                    SenderEmail = distributor.AutomailSender,
                    RecipientEmail = searchableUser.Email,
                    Subject = _mailMessageFactory.GetSubject(ResourceName, languageIdentifier, distributor, fields),
                    Body = _mailMessageFactory.GetBody(templateName, fields, distributor, languageIdentifier),
                    UserId = participant.UserId,
                    ParticipantId = participant.Id,
                };
            }
            
        }

        private string GetTemplateName(int articleType, AutomailDefinition automailDefinition)
        {
            if (articleType == (int) ArticleTypes.ELearning && !automailDefinition.IsExcludeProgressSchedule)
            {
                return _templateResourceNameElearning;
            }
            if (articleType == (int)ArticleTypes.ELearning && automailDefinition.IsExcludeProgressSchedule)               
            {
                return _templateResourceNameElearningNoProgressSchedule;
            }
            if (articleType == (int)ArticleTypes.CaseExam || articleType == (int)ArticleTypes.ProjectAssignment)
            {
                return _templateResourceNameCaseExamProjectAssignment;
            }
            if (articleType == (int)ArticleTypes.ExternalCertification)
            {
                return _templateResourceNameExternalCertification;
            }
            if (articleType == (int)ArticleTypes.InternalCertification)
            {
                return _templateResourceNameInternalCertification;
            }
            if (articleType == (int) ArticleTypes.MultipleChoice)
            {
                return _templateResourceNameMultipleChoice;
            }
            if (articleType == (int) ArticleTypes.Seminar)
            {
                return _emptyTemplateResourceName;
            }
            if (articleType == (int) ArticleTypes.Webinar)
            {
                return _emptyTemplateResourceName;
            }
            return _templateResourceName;
        }

        private int GetArticleType(Participant participant)
        {
            var articleType = _context.Participants.Where(p => p.Id == participant.Id)
                                .Join(_context.Activities, p => p.ActivityId, a => a.Id, (p, a) => a)
                                .Join(_context.CustomerArticles, a => a.CustomerArticleId, ca => ca.Id, (a, ca) => ca)
                                .Join(_context.Articles, ca => ca.ArticleCopyId, a => a.Id, (ca, a) => a)
                                .Select(a => a.ArticleTypeId)
                                .FirstOrDefault();
            return articleType;
        }
    }
}