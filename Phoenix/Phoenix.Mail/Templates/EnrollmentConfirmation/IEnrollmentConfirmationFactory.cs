﻿using Metier.Phoenix.Core.Data.Entities;
using System.Collections.Generic;

namespace Metier.Phoenix.Mail.Templates.EnrollmentConfirmation
{
    public interface IEnrollmentConfirmationFactory
    {
        AutoMailMessage CreateMessage(Participant participant, bool includeCalendarInvitation);
        IEnumerable<AutoMailMessage> CreateMultipleMessage(Participant participant, bool includeCalendarInvitation);
    }
}