﻿using System.Data.Entity;
using System.Linq;
using Castle.Core.Resource;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Mail.FieldFactories;
using Metier.Phoenix.Mail.Helpers;

namespace Metier.Phoenix.Mail.Templates.UserDiplomas
{
    public class UserDiplomasMessageFactory : IUserDiplomasMessageFactory
    {
        private readonly IMailMessageFactory _mailMessageFactory;
        private readonly IParticipantFieldsFactory _participantFieldsFactory;
        private readonly IUserFieldsFactory _userFieldsFactory;
        private readonly IUserDiplomasFieldsFactory _userDiplomasFieldsFactory;
        private readonly ICustomerFieldsFactory _customerFieldsFactory;
        private readonly ILanguageHelper _languageHelper;
        private readonly MetierLmsContext _context;
        private const string ResourceName = "UserDiplomasMessage";
        private readonly string _templateResourceName = typeof(UserDiplomasMessageFactory).Namespace + ".UserDiplomasMessage.html";

        public UserDiplomasMessageFactory(IMailMessageFactory mailMessageFactory, 
                                            IParticipantFieldsFactory participantFieldsFactory, 
                                            IUserFieldsFactory userFieldsFactory,
                                            IUserDiplomasFieldsFactory userDiplomasFieldsFactory,
                                            ICustomerFieldsFactory customerFieldsFactory,
                                            ILanguageHelper languageHelper,
                                            MetierLmsContext context)
        {
            _mailMessageFactory = mailMessageFactory;
            _participantFieldsFactory = participantFieldsFactory;
            _userFieldsFactory = userFieldsFactory;
            _userDiplomasFieldsFactory = userDiplomasFieldsFactory;
            _customerFieldsFactory = customerFieldsFactory;
            _languageHelper = languageHelper;
            _context = context;
        }

        public AutoMailMessage CreateMessage(User user, UserDiploma userDiploma)
        {
            var languageIdentifier = _languageHelper.GetPreferredLanguageIdentifier(user, user.Customer);
            var distributor = _context.Distributors.FirstOrDefault(d => d.Id == user.Customer.DistributorId);

            var searchableUser = _context.SearchableUsers.First(u => u.Id == user.Id);
            var participant = _context.Participants.FirstOrDefault(p => p.UserId == user.Id);
           

            var userFields = _userFieldsFactory.CreateFields(user.Id);
            var userDiplomasFields = _userDiplomasFieldsFactory.CreateFields(userDiploma.Id);
            var fields = userFields.Concat(userDiplomasFields).ToDictionary(f => f.Key, f => f.Value);
            if (participant != null)
            {
                var customerFields = _customerFieldsFactory.CreateFields(participant.CustomerId);
                fields = fields.Concat(customerFields).ToDictionary(f => f.Key, f => f.Value);
            }
           

            return new AutoMailMessage
            {
                MessageType = MessageType.ExamSubmitted,
                SenderEmail = distributor == null ? string.Empty : distributor.AutomailSender,
                RecipientEmail = searchableUser.Email,
                Subject = _mailMessageFactory.GetSubject(ResourceName, languageIdentifier, distributor, fields),
                Body = _mailMessageFactory.GetBody(_templateResourceName, fields, distributor, languageIdentifier),
                UserId = user.Id
            };
        }
    }
}