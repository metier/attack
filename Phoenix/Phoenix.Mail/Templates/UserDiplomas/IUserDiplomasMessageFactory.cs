using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Mail.Templates.UserDiplomas
{
    public interface IUserDiplomasMessageFactory
    {
        AutoMailMessage CreateMessage(User user, UserDiploma userDiploma);
    }
}