﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Mail.Helpers;
using Metier.Phoenix.Mail.FieldFactories;

namespace Metier.Phoenix.Mail.Templates.PasswordResetRequest
{
    public class PasswordResetRequestMessageFactory : IPasswordResetRequestMessageFactory
    {
        private const string ResourceName = "PasswordReset";
        private readonly string _templateResourceName = typeof(PasswordResetRequestMessageFactory).Namespace + ".PasswordResetRequestMessage.html";
        private readonly MetierLmsContext _context;
        private readonly IMailMessageFactory _mailMessageFactory;
		private readonly IUserFieldsFactory _userFieldsFactory;
		private readonly ILanguageHelper _languageHelper;
		
		public PasswordResetRequestMessageFactory(MetierLmsContext context, IMailMessageFactory mailMessageFactory, IUserFieldsFactory userFieldsFactory, ILanguageHelper languageHelper)
        {
            _context = context;
            _mailMessageFactory = mailMessageFactory;
            _languageHelper = languageHelper;
			_userFieldsFactory = userFieldsFactory;
		}

        public AutoMailMessage CreatePasswordResetMessage(Guid resetToken, string recipientEmail, int userId, string passwordResetFormUrl)
        {
            var user = _context.Users.WithUserInfoElementsWithLeadingTexts().First(u => u.Id == userId);
            var customer = _context.Customers.Include(c => c.ParticipantInfoDefinition).First(c => c.Id == user.CustomerId);
            var distributor = _context.Distributors.First(d => d.Id == customer.DistributorId);
            var languageIdentifier = _languageHelper.GetPreferredLanguageIdentifier(user, customer);

			var userFields = _userFieldsFactory.CreateFields(user.Id);
			var passwordFields = new Dictionary<string, string>
									{
										{ "password-reset-url", GetUrl(passwordResetFormUrl, resetToken.ToString()) },										
									};

			var fields = userFields.Concat(passwordFields).ToDictionary(f => f.Key, f => f.Value);

			var message = new AutoMailMessage
            {
                MessageType = MessageType.PasswordResetRequest,
                SenderEmail = distributor.AutomailSender,
                RecipientEmail = recipientEmail,
                Subject = _mailMessageFactory.GetSubject(ResourceName, languageIdentifier, distributor),
                Body = _mailMessageFactory.GetBody(_templateResourceName, fields, distributor, languageIdentifier),
                UserId = userId
            };
            return message;
        }

        private string GetUrl(string urlBase, string token)
        {
            return new UrlBuilder(urlBase).AddParameter("token", token).ToString();
        }
    }
}