﻿using System;

namespace Metier.Phoenix.Mail.Templates.PasswordResetRequest
{
    public interface IPasswordResetRequestMessageFactory
    {
        AutoMailMessage CreatePasswordResetMessage(Guid resetToken, string recipientEmail, int userId, string passwordResetFormUrl); 
    }
}