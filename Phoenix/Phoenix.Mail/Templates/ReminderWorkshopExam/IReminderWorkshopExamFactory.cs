﻿using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Mail.Templates.ReminderWorkshopExam
{
    public interface IReminderWorkshopExamFactory
    {
        AutoMailMessage CreateMessage(Participant participant); 
    }
}