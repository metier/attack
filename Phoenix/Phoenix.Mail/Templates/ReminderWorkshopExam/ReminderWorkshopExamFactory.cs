﻿using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Mail.FieldFactories;
using Metier.Phoenix.Mail.Helpers;

namespace Metier.Phoenix.Mail.Templates.ReminderWorkshopExam
{
    public class ReminderWorkshopExamFactory : IReminderWorkshopExamFactory
    {
        private readonly IMailMessageFactory _mailMessageFactory;
        private readonly IParticipantFieldsFactory _participantFieldsFactory;
        private readonly ICustomerFieldsFactory _customerFieldsFactory;
        private readonly IUserFieldsFactory _userFieldsFactory;
        private readonly ILanguageHelper _languageHelper;
        private readonly MetierLmsContext _context;
        private const string ResourceName = "ReminderWorkshopExam";
        private readonly string _templateResourceName = typeof(ReminderWorkshopExamFactory).Namespace + ".ReminderWorkshopExamMessage.html";
        private readonly string _templateResourceNameCaseExamProjectAssignment = typeof(ReminderWorkshopExamFactory).Namespace + ".ReminderWorkshopExamMessageCaseExamProjectAssignment.html";
        private readonly string _templateResourceNameMultipleChoice = typeof(ReminderWorkshopExamFactory).Namespace + ".ReminderWorkshopExamMessageMultipleChoice.html";
        private readonly string _templateResourceNameExternalCertification = typeof(ReminderWorkshopExamFactory).Namespace + ".ReminderWorkshopExamMessageExternalCertification.html";
        private readonly string _templateResourceNameInternalCertification = typeof(ReminderWorkshopExamFactory).Namespace + ".ReminderWorkshopExamMessageInternalCertification.html";

        public ReminderWorkshopExamFactory(IMailMessageFactory mailMessageFactory, 
                                            IParticipantFieldsFactory participantFieldsFactory, 
                                            ICustomerFieldsFactory customerFieldsFactory, 
                                            IUserFieldsFactory userFieldsFactory,
                                            ILanguageHelper languageHelper,
                                            MetierLmsContext context)
        {
            _mailMessageFactory = mailMessageFactory;
            _participantFieldsFactory = participantFieldsFactory;
            _customerFieldsFactory = customerFieldsFactory;
            _userFieldsFactory = userFieldsFactory;
            _languageHelper = languageHelper;
            _context = context;
        }

        public AutoMailMessage CreateMessage(Participant participant)
        {
            var user = _context.Users.Include(u => u.Customer).First(u => u.Id == participant.UserId);
            var languageIdentifier = _languageHelper.GetPreferredLanguageIdentifier(user, user.Customer);
            var distributor = _context.Distributors.First(d => d.Id == user.Customer.DistributorId);

            var searchableUser = _context.SearchableUsers.First(u => u.Id == participant.UserId);

            var participantFields = _participantFieldsFactory.CreateFields(participant.Id, languageIdentifier);
            var userFields = _userFieldsFactory.CreateFields(participant.UserId);
            var customerFields = _customerFieldsFactory.CreateFields(participant.CustomerId);

            var fields = participantFields.Concat(userFields).Concat(customerFields).ToDictionary(f => f.Key, f => f.Value);
            
            var articleType = GetArticleType(participant);
            var templateName = GetTemplateName(articleType);

            return new AutoMailMessage
            {
                MessageType = MessageType.ReminderWorkshopExam,
                SenderEmail = distributor.AutomailSender,
                RecipientEmail = searchableUser.Email,
                Subject = _mailMessageFactory.GetSubject(ResourceName, languageIdentifier, distributor, fields),
                Body = _mailMessageFactory.GetBody(templateName, fields, distributor, languageIdentifier),
                UserId = participant.UserId,
                ParticipantId = participant.Id
            };
        }

        private string GetTemplateName(int articleType)
        {
            if (articleType == (int)ArticleTypes.CaseExam || articleType == (int)ArticleTypes.ProjectAssignment)
            {
                return _templateResourceNameCaseExamProjectAssignment;
            }
            if (articleType == (int)ArticleTypes.MultipleChoice)
            {
                return _templateResourceNameMultipleChoice;
            }
            if (articleType == (int)ArticleTypes.ExternalCertification)
            {
                return _templateResourceNameExternalCertification;
            }
            if (articleType == (int)ArticleTypes.InternalCertification)
            {
                return _templateResourceNameInternalCertification;
            }
            return _templateResourceName;
        }

        private int GetArticleType(Participant participant)
        {
            var articleType = _context.Participants.Where(p => p.Id == participant.Id)
                                .Join(_context.Activities, p => p.ActivityId, a => a.Id, (p, a) => a)
                                .Join(_context.CustomerArticles, a => a.CustomerArticleId, ca => ca.Id, (a, ca) => ca)
                                .Join(_context.Articles, ca => ca.ArticleCopyId, a => a.Id, (ca, a) => a)
                                .Select(a => a.ArticleTypeId)
                                .FirstOrDefault();
            return articleType;
        }
    }
}