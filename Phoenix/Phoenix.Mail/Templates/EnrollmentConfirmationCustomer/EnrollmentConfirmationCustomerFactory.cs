﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Utilities.Validation;
using Metier.Phoenix.Mail.FieldFactories;
using Metier.Phoenix.Mail.Helpers;

namespace Metier.Phoenix.Mail.Templates.EnrollmentConfirmationCustomer
{
    public class EnrollmentConfirmationCustomerFactory : IEnrollmentConfirmationCustomerFactory
    {
        private const string ResourceName = "EnrollmentConfirmationCustomer";
        private readonly string _templateResourceName = typeof(EnrollmentConfirmationCustomerFactory).Namespace + ".EnrollmentConfirmationCustomerMessage.html";
        private readonly MetierLmsContext _context;
        private readonly IUserFieldsFactory _userFieldsFactory;
        private readonly ICustomerFieldsFactory _customerFieldsFactory;
        private readonly IParticipantFieldsFactory _participantFieldsFactory;
        private readonly IMailMessageFactory _mailMessageFactory;
        private readonly ILanguageHelper _languageHelper;

        public EnrollmentConfirmationCustomerFactory(MetierLmsContext context, 
            IUserFieldsFactory userFieldsFactory,
            ICustomerFieldsFactory customerFieldsFactory,
            IParticipantFieldsFactory participantFieldsFactory,
            IMailMessageFactory mailMessageFactory,
            ILanguageHelper languageHelper)
        {
            _context = context;
            _userFieldsFactory = userFieldsFactory;
            _customerFieldsFactory = customerFieldsFactory;
            _participantFieldsFactory = participantFieldsFactory;
            _mailMessageFactory = mailMessageFactory;
            _languageHelper = languageHelper;
        }

        public IEnumerable<AutoMailMessage> CreateMessages(Participant participant)
        {
            var customer = _context.Customers.Include(c => c.ParticipantInfoDefinition).First(c => c.Id == participant.CustomerId);
            var user = _context.Users.WithUserInfoElementsWithLeadingTexts().First(u => u.Id == participant.UserId);
            var distributor = _context.Distributors.First(d => d.Id == customer.DistributorId);
            var recipients = FindRecipients(user, customer);
            var languageIdentifier = _languageHelper.GetPreferredLanguageIdentifier(user, customer);
            var fields = GetFields(participant, languageIdentifier);

            return recipients.Select(recipient => new AutoMailMessage
            {
                MessageType = MessageType.EnrollmentConfirmationCustomer,
                SenderEmail = distributor.AutomailSender,
                RecipientEmail = recipient,
                Subject = _mailMessageFactory.GetSubject(ResourceName, languageIdentifier, distributor, fields),
                Body = _mailMessageFactory.GetBody(_templateResourceName, fields, distributor, languageIdentifier),
                UserId = participant.UserId,
                ParticipantId = participant.Id
            });
        }

        private Dictionary<string,string> GetFields(Participant participant, string languageIdentifier)
        {
            var userFields = _userFieldsFactory.CreateFields(participant.UserId);
            var customerFields = _customerFieldsFactory.CreateFields(participant.CustomerId);
            var activityTitleFields = _participantFieldsFactory.CreateFields(participant.Id, languageIdentifier, "activityset-title", "customer-article-name");

            return userFields.Concat(customerFields).Concat(activityTitleFields).ToDictionary(f => f.Key, f => f.Value);
        }

        private IEnumerable<string> FindRecipients(User user, Customer customer)
        {
            var recipients = new List<string>();
            var emailConfirmation = customer.ParticipantInfoDefinition != null ? customer.ParticipantInfoDefinition.EmailConfirmationAddress : string.Empty;
            if (!string.IsNullOrEmpty(emailConfirmation) && EmailValidation.IsValidEmail(emailConfirmation))
            {
                recipients.Add(emailConfirmation);
            }

            var userConfirmationEmail = user.UserInfoElements
                                            .Where(ue => ue.LeadingText != null && 
                                                         ue.LeadingText.Id == LeadingTextIds.ConfirmationEmail)
                                            .Select(ue => ue.InfoText).FirstOrDefault() ?? string.Empty;

            if (!string.IsNullOrEmpty(userConfirmationEmail) && EmailValidation.IsValidEmail(userConfirmationEmail))
            {
                recipients.Add(userConfirmationEmail);
            }
            return recipients;
        }
    }
}