﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Mail.Templates.EnrollmentConfirmationCustomer
{
    public interface IEnrollmentConfirmationCustomerFactory
    {
        IEnumerable<AutoMailMessage> CreateMessages(Participant participant); 
    }
}