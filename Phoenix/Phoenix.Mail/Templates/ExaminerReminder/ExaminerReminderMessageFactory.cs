﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Mail.Helpers;

namespace Metier.Phoenix.Mail.Templates.ExaminerReminder
{
    public class ExaminerReminderMessageFactory : IExaminerReminderMessageFactory
    {
        private const string ResourceName = "ExaminerReminder";
        private readonly string _templateResourceName = typeof(ExaminerReminderMessageFactory).Namespace + ".ExaminerReminderMessage.html";

        private readonly IMailMessageFactory _mailMessageFactory;
        private readonly ILanguageHelper _languageHelper;

        public ExaminerReminderMessageFactory(IMailMessageFactory mailMessageFactory, ILanguageHelper languageHelper)
        {
            _mailMessageFactory = mailMessageFactory;
            _languageHelper = languageHelper;
        }

        public AutoMailMessage CreateMessage(UserResource examiner, Activity activity, Distributor distributor)
        {
            
            var languageIdentifier = _languageHelper.GetPreferredLanguageIdentifier(examiner.User, examiner.User.Customer);

            var fields = new Dictionary<string, string>
            {
                { "activity-title", activity.Name },
                { "first-name", examiner.User.FirstName },
                { "examiner-portal-url", ConfigurationManager.AppSettings["ExaminerPortalUrl"] }
            };

            var mailMessage = new AutoMailMessage
            {
                ActivityId = activity.Id,
                UserId = examiner.UserId,
                MessageType = MessageType.ReminderExaminer,
                SenderEmail = distributor == null ? string.Empty:distributor.AutomailSender,
                RecipientEmail = examiner.ContactEmail,
                Subject = _mailMessageFactory.GetSubject(ResourceName, languageIdentifier, distributor, fields),
                Body = _mailMessageFactory.GetBody(_templateResourceName, fields, distributor, languageIdentifier)
            };

            return mailMessage;
        }
    }
}