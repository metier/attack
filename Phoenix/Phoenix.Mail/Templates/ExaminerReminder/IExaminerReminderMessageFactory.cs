﻿using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Mail.Templates.ExaminerReminder
{
    public interface IExaminerReminderMessageFactory
    {
        AutoMailMessage CreateMessage(UserResource examiner, Activity activity, Distributor distributor);
    }
}