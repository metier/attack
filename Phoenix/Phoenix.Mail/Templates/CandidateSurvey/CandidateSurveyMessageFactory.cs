using System;
using System.Collections.Generic;
using Metier.Phoenix.Core.Configuration;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Mail.Templates.CandidateSurvey
{
    public class CandidateSurveyMessageFactory : ICandidateSurveyMessageFactory
    {
        private readonly ICoreConfiguration _configuration;
        private readonly IMailMessageFactory _mailMessageFactory;

        private const string ResourceName = "CandidateSurvey";
        private readonly string _templateResourceName = typeof(CandidateSurveyMessageFactory).Namespace + ".CandidateSurveyMessage.html";

        public CandidateSurveyMessageFactory(ICoreConfiguration configuration, IMailMessageFactory mailMessageFactory)
        {
            _configuration = configuration;
            _mailMessageFactory = mailMessageFactory;
        }

        public AutoMailMessage CreateCandidateSurveyMessage(Guid sharedFileGuid, Resource educationalPartner)
        {
            var fields = GetCandidateSurveyFields(sharedFileGuid);

            return new AutoMailMessage
            {
                MessageType = MessageType.CandidateSurvey,
                RecipientEmail = educationalPartner.ContactEmail,
                Subject = _mailMessageFactory.GetSubject(ResourceName, string.Empty),
                Body = _mailMessageFactory.GetBody(_templateResourceName, fields)
            };
        }

        private Dictionary<string, string> GetCandidateSurveyFields(Guid sharedFileGuid)
        {
            var fields = new Dictionary<string, string>();
            var candidateSurveyLink = _configuration.PhoenixApiUrl + "files/shared?fileId=" + sharedFileGuid;
            fields.Add("candidate-survey-link", candidateSurveyLink);
            return fields;
        }
    }
}