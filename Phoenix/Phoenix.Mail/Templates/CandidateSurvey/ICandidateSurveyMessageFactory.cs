﻿using System;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Mail.Templates.CandidateSurvey
{
    public interface ICandidateSurveyMessageFactory
    {
        AutoMailMessage CreateCandidateSurveyMessage(Guid sharedFileGuid, Resource educationalPartner);
    }
}