﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Mail.Templates
{
    public interface IMailMessageFactory
    {
        string GetSubject(string resourceName, string languageIdentifier, Distributor distributor = null, Dictionary<string, string> fields = null);
        string GetBody(string htmlResourcePath, Dictionary<string, string> fields, Distributor distributor = null, string languageIdentifier = null);
    }
}