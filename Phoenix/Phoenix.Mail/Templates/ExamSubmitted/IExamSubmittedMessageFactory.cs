using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Mail.Templates.ExamSubmitted
{
    public interface IExamSubmittedMessageFactory
    {
        AutoMailMessage CreateMessage(Participant participant);
    }
}