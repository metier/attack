﻿using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Mail.FieldFactories;
using Metier.Phoenix.Mail.Helpers;

namespace Metier.Phoenix.Mail.Templates.ExamSubmitted
{
    public class ExamSubmittedMessageFactory : IExamSubmittedMessageFactory
    {
        private readonly IMailMessageFactory _mailMessageFactory;
        private readonly IParticipantFieldsFactory _participantFieldsFactory;
        private readonly IUserFieldsFactory _userFieldsFactory;
        private readonly ILanguageHelper _languageHelper;
        private readonly MetierLmsContext _context;
        private const string ResourceName = "ExamSubmitted";
        private readonly string _templateResourceName = typeof(ExamSubmittedMessageFactory).Namespace + ".ExamSubmittedMessage.html";

        public ExamSubmittedMessageFactory(IMailMessageFactory mailMessageFactory, 
                                            IParticipantFieldsFactory participantFieldsFactory, 
                                            IUserFieldsFactory userFieldsFactory,
                                            ILanguageHelper languageHelper,
                                            MetierLmsContext context)
        {
            _mailMessageFactory = mailMessageFactory;
            _participantFieldsFactory = participantFieldsFactory;
            _userFieldsFactory = userFieldsFactory;
            _languageHelper = languageHelper;
            _context = context;
        }

        public AutoMailMessage CreateMessage(Participant participant)
        {
            var user = _context.Users.Include(u => u.Customer).First(u => u.Id == participant.UserId);
            var languageIdentifier = _languageHelper.GetPreferredLanguageIdentifier(user, user.Customer);
            var distributor = _context.Distributors.FirstOrDefault(d => d.Id == user.Customer.DistributorId);

            var searchableUser = _context.SearchableUsers.First(u => u.Id == participant.UserId);

            var participantFields = _participantFieldsFactory.CreateFields(participant.Id, languageIdentifier);
            var userFields = _userFieldsFactory.CreateFields(participant.UserId);
            
            var fields = participantFields.Concat(userFields).ToDictionary(f => f.Key, f => f.Value);

            return new AutoMailMessage
            {
                MessageType = MessageType.ExamSubmitted,
                SenderEmail = distributor == null ? string.Empty : distributor.AutomailSender,
                RecipientEmail = searchableUser.Email,
                Subject = _mailMessageFactory.GetSubject(ResourceName, languageIdentifier, distributor, fields),
                Body = _mailMessageFactory.GetBody(_templateResourceName, fields, distributor, languageIdentifier),
                UserId = participant.UserId,
                ParticipantId = participant.Id
            };
        }
    }
}