﻿using System;

namespace Metier.Phoenix.Mail.Templates.MissingExamCompetency
{
    public interface IMissingExamCompetencyMessageFactory
    {
        AutoMailMessage CreateMissingExamCompetencyMessage(int userId); 
    }
}