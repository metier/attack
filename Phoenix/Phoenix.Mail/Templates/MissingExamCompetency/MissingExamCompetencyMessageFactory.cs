﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Mail.Helpers;

namespace Metier.Phoenix.Mail.Templates.MissingExamCompetency
{
    public class MissingExamCompetencyMessageFactory : IMissingExamCompetencyMessageFactory
    {        
        private readonly string _templateResourceName = typeof(MissingExamCompetencyMessageFactory).Namespace + ".MissingExamCompetencyMessage.html";
        private readonly MetierLmsContext _context;
        private readonly IMailMessageFactory _mailMessageFactory;
        private readonly ILanguageHelper _languageHelper;

        public MissingExamCompetencyMessageFactory(MetierLmsContext context, IMailMessageFactory mailMessageFactory, ILanguageHelper languageHelper)
        {
            _context = context;
            _mailMessageFactory = mailMessageFactory;
            _languageHelper = languageHelper;
        }

        public AutoMailMessage CreateMissingExamCompetencyMessage(int userId)
        {
            var user = _context.Users.WithUserInfoElementsWithLeadingTexts().First(u => u.Id == userId);
            var customer = _context.Customers.Include(c => c.AutomailDefinition).First(c => c.Id == user.CustomerId);
            var distributor = _context.Distributors.FirstOrDefault(d => d.Id == customer.DistributorId);

            var participationsWithCompetenceRequirement = _context.SearchableParticipants.Where(p => p.UserId == userId && p.IsUserCompetenceInfoRequired && p.ParticipantStatusCodeValue == Metier.Phoenix.Core.Data.Codes.ParticipantStatusCodes.Enrolled).ToList();
            var eCoaches = participationsWithCompetenceRequirement.SelectMany(p => _context.Activities.First(a => a.Id == p.ActivityId).Resources.Where(r => r.ResourceTypeId == 3).ToList()).ToList();
            var eCoachEmails = eCoaches.Select(e => e.ContactEmail).Distinct().ToList();
            var searchableUser = _context.SearchableUsers.First(u => u.Id == userId);

            if (eCoachEmails.Count == 0) eCoachEmails.Add(distributor.AutomailSender);
            //Todo: can toEmail be commaseparated list?

            var message = new AutoMailMessage
            {
                MessageType = MessageType.MissingExamCompetency,
                SenderEmail = distributor.AutomailSender,
                RecipientEmail = string.Join(",", eCoachEmails),
                Subject = "User indicates he/she does not meet requirements for accredited practical competence - " + user.MembershipUserId,
                Body = _mailMessageFactory.GetBody(_templateResourceName, 
                                            new Dictionary<string, string>
                                            {                                                
                                                { "username", user.MembershipUserId },
                                                { "email", searchableUser.Email }
                                            }, distributor),
                UserId = userId
            };
            return message;
        }        
    }
}