﻿using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Mail.FieldFactories;
using Metier.Phoenix.Mail.Helpers;

namespace Metier.Phoenix.Mail.Templates.EnrollmentNotQualifiedForExam
{
    public interface IEnrollmentNotQualifiedForExamFactory
    {
        AutoMailMessage CreateMessage(Participant participant);
    }

    public class EnrollmentNotQualifiedForExamFactory : IEnrollmentNotQualifiedForExamFactory
    {
        private const string ResourceName = "EnrollmentNotQualifiedForExam";
        private readonly string _templateResourceName = typeof(EnrollmentNotQualifiedForExamFactory).Namespace + ".EnrollmentNotQualifiedForExamMessage.html";

        private readonly MetierLmsContext _context;
        private readonly IParticipantFieldsFactory _participantFieldsFactory;
        private readonly IUserFieldsFactory _userFieldsFactory;
        private readonly ILanguageHelper _languageHelper;
        private readonly IMailMessageFactory _mailMessageFactory;

        public EnrollmentNotQualifiedForExamFactory(MetierLmsContext context,
                                                    IParticipantFieldsFactory participantFieldsFactory,
                                                    IUserFieldsFactory userFieldsFactory,
                                                    ILanguageHelper languageHelper,
                                                    IMailMessageFactory mailMessageFactory)
        {
            _context = context;
            _participantFieldsFactory = participantFieldsFactory;
            _userFieldsFactory = userFieldsFactory;
            _languageHelper = languageHelper;
            _mailMessageFactory = mailMessageFactory;
        }

        public AutoMailMessage CreateMessage(Participant participant)
        {
            var user = _context.Users.Include(u => u.Customer).First(u => u.Id == participant.UserId);
            var searchableUser = _context.SearchableUsers.First(u => u.Id == participant.UserId);
            var distributor = _context.Distributors.First(d => d.Id == user.Customer.DistributorId);

            var languageIdentifier = _languageHelper.GetPreferredLanguageIdentifier(user, user.Customer);

            var participantFields = _participantFieldsFactory.CreateFields(participant.Id, languageIdentifier);
            var userFields = _userFieldsFactory.CreateFields(participant.UserId);

            var fields = participantFields.Concat(userFields).ToDictionary(f => f.Key, f => f.Value);

            return new AutoMailMessage
            {
                MessageType = MessageType.EnrollmentNotQualifiedForExam,
                SenderEmail = distributor.AutomailSender,
                RecipientEmail = searchableUser.Email,
                Subject = _mailMessageFactory.GetSubject(ResourceName, languageIdentifier, distributor, fields),
                Body = _mailMessageFactory.GetBody(_templateResourceName, fields, distributor, languageIdentifier),
                UserId = participant.UserId,
                ParticipantId = participant.Id
            };
        }
    }
}