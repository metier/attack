﻿using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Mail.Templates.ReminderElearning
{
    public interface IReminderElearningFactory
    {
        AutoMailMessage CreateAheadMessage(Participant participant);
        AutoMailMessage CreateBehindMessage(Participant participant);
        AutoMailMessage CreateCompletedMessage(Participant participant);
    }
}