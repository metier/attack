﻿using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Mail.FieldFactories;
using Metier.Phoenix.Mail.Helpers;

namespace Metier.Phoenix.Mail.Templates.ReminderElearning
{
    public class ReminderElearningFactory : IReminderElearningFactory
    {
        private const string ResourceAheadName = "ResourceAheadName";
        private const string ResourceBehindName = "ResourceBehindName";
        private const string ResourceCompleteName = "ResourceCompleteName";
        private readonly string _templateResourceAheadName = typeof(ReminderElearningFactory).Namespace + ".ReminderElearningAheadMessage.html";
        private readonly string _templateResourceBehindName = typeof(ReminderElearningFactory).Namespace + ".ReminderElearningBehindMessage.html";
        private readonly string _templateResourceCompleteName = typeof(ReminderElearningFactory).Namespace + ".ReminderElearningCompletedMessage.html";

        private readonly MetierLmsContext _context;
        private readonly IUserFieldsFactory _userFieldsFactory;
        private readonly IParticipantFieldsFactory _participantFieldsFactory;
        private readonly ILanguageHelper _languageHelper;
        private readonly IMailMessageFactory _mailMessageFactory;

        public ReminderElearningFactory(MetierLmsContext context, 
            IUserFieldsFactory userFieldsFactory, 
            IParticipantFieldsFactory participantFieldsFactory,
            ILanguageHelper languageHelper,
            IMailMessageFactory mailMessageFactory)
        {
            _context = context;
            _userFieldsFactory = userFieldsFactory;
            _participantFieldsFactory = participantFieldsFactory;
            _languageHelper = languageHelper;
            _mailMessageFactory = mailMessageFactory;
        }

        public AutoMailMessage CreateAheadMessage(Participant participant)
        {
            return CreateMessage(participant, ResourceAheadName, _templateResourceAheadName, MessageType.ReminderElearning);
        }

        public AutoMailMessage CreateBehindMessage(Participant participant)
        {
            return CreateMessage(participant, ResourceBehindName, _templateResourceBehindName, MessageType.ReminderElearning);
        }

        public AutoMailMessage CreateCompletedMessage(Participant participant)
        {
            return CreateMessage(participant, ResourceCompleteName, _templateResourceCompleteName, MessageType.ElearningCompleted);
        }

        internal AutoMailMessage CreateMessage(Participant participant, string resourceName, string resourceTemplate, MessageType messageType)
        {
            var searchableUser = _context.SearchableUsers.First(u => u.Id == participant.UserId);
            var distributor = _context.Distributors.First(d => d.Id == searchableUser.DistributorId);
            var languageIdentifider = GetLanguageIdentifider(participant);
            var fields = GetFieldsForTemplate(participant, languageIdentifider);

            return new AutoMailMessage
            {
                MessageType = messageType,
                SenderEmail = distributor.AutomailSender,
                RecipientEmail = searchableUser.Email,
                Subject = _mailMessageFactory.GetSubject(resourceName, languageIdentifider, distributor, fields),
                Body = _mailMessageFactory.GetBody(resourceTemplate, fields, distributor, languageIdentifider),
                UserId = participant.UserId,
                ParticipantId = participant.Id
            };
        }

        private string GetLanguageIdentifider(Participant participant)
        {
            var user = _context.Users.First(u => u.Id == participant.UserId);
            var customer = _context.Customers.First(c => c.Id == user.CustomerId);

            var languageIdentifider = _languageHelper.GetPreferredLanguageIdentifier(user, customer);
            return languageIdentifider;
        }

        private Dictionary<string, string> GetFieldsForTemplate(Participant participant, string languageIdentifider)
        {
            var userFields = _userFieldsFactory.CreateFields(participant.UserId);
            var participantFields = _participantFieldsFactory.CreateFields(participant.Id, languageIdentifider);

            return userFields.Concat(participantFields).ToDictionary(f => f.Key, f => f.Value);
        }
    }
}