﻿using System;
using System.Net.Mail;

namespace Metier.Phoenix.Mail
{
    public interface ISmtpClientProxy
    {
        void Send(MailMessage mailMessage);
        void SendInvitation(MailMessage mailMessage, CalendarInvitation calendarInvitation);

    }
}