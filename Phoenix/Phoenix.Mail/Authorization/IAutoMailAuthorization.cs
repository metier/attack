﻿namespace Metier.Phoenix.Mail.Authorization
{
    public interface IAutoMailAuthorization
    {
        bool CanSend(string recipientEmail);
        bool IsParticipantAutomailable(int participantId);
    }
}