﻿using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Mail.Configuration;
using Metier.Phoenix.Mail.Configuration.ConfigurationSections;

namespace Metier.Phoenix.Mail.Authorization
{
    public class AutoMailAuthorization : IAutoMailAuthorization
    {
        private readonly IAutoMailConfiguration _configuration;
        private readonly MetierLmsContext _context;

        public AutoMailAuthorization(IAutoMailConfiguration configuration, MetierLmsContext context)
        {
            _configuration = configuration;
            _context = context;
        }

        public bool CanSend(string recipientEmail)
        {
            if (!_configuration.AutoMailEnabled)
            {
                return false;
            }

            if (_configuration.AutoMailWhiteList == null)
            {
                return false;
            }

            if (!IsValidEmailAddresses(recipientEmail))
            {
                return false;
            }

            var emails = recipientEmail.Split(',').Where(email=>email.Trim()!="").Select(email=>email.Trim()).ToList();            

            return emails.All(email => _configuration.AutoMailWhiteList.Cast<WhiteListConfigurationElement>().Any(whiteListItem => IsWhiteListMatch(whiteListItem.Email, email)));
        }

        public bool IsParticipantAutomailable(int participantId)
        {
            var productCategory = _context.Participants.Where(p => p.Id == participantId)
                                               .Join(_context.Activities, p => p.ActivityId, a => a.Id, (p, a) => a)
                                               .Join(_context.CustomerArticles, a => a.CustomerArticleId, ca => ca.Id, (a, ca) => ca)
                                               .Join(_context.Articles, ca => ca.ArticleCopyId, a => a.Id, (ca, a) => a)
                                               .Join(_context.Products, a => a.ProductId, p => p.Id, (a, p) => p)
                                               .Join(_context.ProductCategories, p => p.ProductCategoryId, pc => pc.Id, (p, pc) => pc)
                                               .FirstOrDefault();
            if (productCategory != null)
            {
                return productCategory.ProductCodePrefix != "TE" && productCategory.ProductCodePrefix != "TPM";
            }
            return true;
        }

        internal bool IsValidEmailAddresses(string emailAddresses)
        {
            try
            {
                emailAddresses
                    .Split(',')
                    .ToList()
                    .ForEach(email =>
                    {
                        if (email.Trim() != "")
                            new MailAddress(email.Trim());
                    });

                return true;
            }
            catch
            {
                return false;
            }
        }

        internal bool IsWhiteListMatch(string whiteListAddress, string emailAddress)
        {
            var whiteListRegex = "^" + whiteListAddress.Replace("*", ".*") + "$";
            return Regex.IsMatch(emailAddress, whiteListRegex);
        }
    }
}