﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Mail;
using System.Transactions;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Mail.Accounting;
using Metier.Phoenix.Mail.Authorization;
using Metier.Phoenix.Mail.Configuration;
using Newtonsoft.Json;
using NLog;
using System.Threading;

namespace Metier.Phoenix.Mail
{
    public class AutoMailer : IAutoMailer
    {
        private readonly ISmtpClientProxy _smtpClientProxy;
        private readonly IAutoMailAuthorization _autoMailAuthorization;
        private readonly IMailAccounting _mailAccounting;
        private readonly IAutoMailConfiguration _autoMailConfiguration;
        
        private readonly Logger _logger;

        private readonly List<Tuple<int, string, MailMessage>> _deferredMessages = new List<Tuple<int, string, MailMessage>>(); 
        
        public AutoMailer(ISmtpClientProxy smtpClientProxy, IAutoMailAuthorization autoMailAuthorization, IMailAccounting mailAccounting, IAutoMailConfiguration autoMailConfiguration)
        {
            _smtpClientProxy = smtpClientProxy;
            _autoMailAuthorization = autoMailAuthorization;
            _mailAccounting = mailAccounting;
            _autoMailConfiguration = autoMailConfiguration;
            _logger = LogManager.GetCurrentClassLogger();
        }

        public bool Send(AutoMailMessage autoMailMessage)
        {
            if (autoMailMessage.ParticipantId.HasValue && !_autoMailAuthorization.IsParticipantAutomailable(autoMailMessage.ParticipantId.Value))
            {
                // If participant is not automailable, do not send, but pretend everything went OK.
                return true;
            }

            var mailMessage = GetMailMessage(autoMailMessage);
            var mailId = _mailAccounting.CreateMail(autoMailMessage);

            if(autoMailMessage.CalendarInvitation != null && autoMailMessage.CalendarInvitation.IncludeCalendarInvitation.HasValue)
            {
                _logger.Error("SendWithCalendarInvitation: ");
                return SendWithCalendarInvitation(mailMessage, autoMailMessage.RecipientEmail, mailId, autoMailMessage.CalendarInvitation);
            } else
            {
                return Send(mailMessage, autoMailMessage.RecipientEmail, mailId);
            }
        }

        public bool Send(int mailId)
        {
            var mail = _mailAccounting.GetMail(mailId);
            if (mail == null || mail.SerializedMailMessage == null || mail.SerializedMailMessage.SerializedMessage == null)
            {
                _logger.Error(string.Format("Mail with ID {0} has no serialized message", mailId));
                return false;
            }

            AutoMailMessage autoMailMessage;
            try
            {
                autoMailMessage = JsonConvert.DeserializeObject<AutoMailMessage>(mail.SerializedMailMessage.SerializedMessage);
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("Mail with ID {0} could not be serialized", mailId), e);
                return false;
            }

            var mailMessage = GetMailMessage(autoMailMessage);
            return Send(mailMessage, autoMailMessage.RecipientEmail, mailId);
        }

      
        //public bool SendInvitation(AutoMailMessage autoMailMessage, DateTime start, DateTime end, string location)
        //{
           
        //    try
        //    {
        //        var mailMessage = GetMailMessage(autoMailMessage);
        //        _smtpClientProxy.SendInvitation(mailMessage, start, end, location);
        //    }
        //    catch (Exception e)
        //    {
        //        _logger.Error("Invitation cant be sent", e);
        //        return false;
        //    }

        //    return true;
        //}

        public AutoMailMessage Get(int mailId)
        {
            var mail = _mailAccounting.GetMail(mailId);
            return JsonConvert.DeserializeObject<AutoMailMessage>(mail.SerializedMailMessage.SerializedMessage);
        }
        
        internal bool Send(MailMessage mailMessage, string recipientEmail, int mailId)
        {
            if (!_autoMailAuthorization.CanSend(recipientEmail))
            {
                _logger.Info(string.Format("Mail address {0} is blocked", recipientEmail));
                _mailAccounting.UpdateStatus(mailId, MailStatusType.Failed);
                return false;
            }
            
            // If mails are sent as part of a transaction,
            // they will be queued up and sent once the transaction completes
            if (Transaction.Current != null)
            {
                _deferredMessages.Add(new Tuple<int, string, MailMessage>(mailId, recipientEmail, mailMessage));
                Transaction.Current.TransactionCompleted -= SendSmtpWhenTransactionComplete;
                Transaction.Current.TransactionCompleted += SendSmtpWhenTransactionComplete;
                return true;
            }

            // If not part of transaction, send right away
            return SendSmtp(mailMessage, recipientEmail, mailId);
        }


        internal bool SendWithCalendarInvitation(MailMessage mailMessage, string recipientEmail, int mailId, CalendarInvitation calendarInvitation)
        {
            if (!_autoMailAuthorization.CanSend(recipientEmail))
            {
                _logger.Info(string.Format("Mail address {0} is blocked", recipientEmail));
                _mailAccounting.UpdateStatus(mailId, MailStatusType.Failed);
                return false;
            }

            // If mails are sent as part of a transaction,
            // they will be queued up and sent once the transaction completes
            //if (Transaction.Current != null)
            //{
            //    _deferredMessages.Add(new Tuple<int, string, MailMessage>(mailId, recipientEmail, mailMessage));
            //    Transaction.Current.TransactionCompleted -= SendSmtpWhenTransactionComplete;
            //    Transaction.Current.TransactionCompleted += SendSmtpWhenTransactionComplete;
            //    return true;
            //}

            // If not part of transaction, send right away
            return SendSmtpWithInvitation(mailMessage, recipientEmail, mailId, calendarInvitation);
        }

        private bool SendSmtpWithRetry(MailMessage mailMessage, string recipientEmail, int mailId, int attempt)
        {
            if (attempt > 2)
                return false;
            try
            {
                Thread.Sleep(1000 * 10);
                _mailAccounting.UpdateStatus(mailId, attempt == 1 ? MailStatusType.ResendingAttempt2 : MailStatusType.ResendingAttempt3);
                _smtpClientProxy.Send(mailMessage);
                _mailAccounting.UpdateStatus(mailId, MailStatusType.Sent);
                return true;
            }
            catch (SmtpException e)
            {
                if (e.Message.Contains("The operation has timed out."))
                {
                    _logger.Warn(string.Format("Failed to send email to {0}. Next attempt will be executed in 10 seconds.", recipientEmail), e);
                    return SendSmtpWithRetry(mailMessage, recipientEmail, mailId, attempt + 1);
                }
                else
                {
                    _logger.Error(string.Format("Failed to send email to {0}", recipientEmail), e);
                    _mailAccounting.UpdateStatus(mailId, MailStatusType.Failed);
                    return false;
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("Failed to send email to {0}", recipientEmail), e);
                _mailAccounting.UpdateStatus(mailId, MailStatusType.Failed);
                return false;
            }
        }

        private bool SendSmtp(MailMessage mailMessage, string recipientEmail, int mailId)
        {
            try
            {
                _smtpClientProxy.Send(mailMessage);
                _mailAccounting.UpdateStatus(mailId, MailStatusType.Sent);
                return true;
            } catch (SmtpException e)
            {
                if (e.Message.Contains("The operation has timed out."))
                {
                    return SendSmtpWithRetry(mailMessage, recipientEmail, mailId, 1);
                }
                else
                    throw e;
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("Failed to send email to {0}", recipientEmail), e);
                _mailAccounting.UpdateStatus(mailId, MailStatusType.Failed);
                return false;
            }
        }


        private bool SendSmtpWithInvitation(MailMessage mailMessage, string recipientEmail, int mailId, CalendarInvitation calendarInvitation)
        {
            try
            {
                _smtpClientProxy.SendInvitation(mailMessage, calendarInvitation);
                _mailAccounting.UpdateStatus(mailId, MailStatusType.Sent);
                return true;
            }
            catch (SmtpException e)
            {
                if (e.Message.Contains("The operation has timed out."))
                {
                    return SendSmtpWithRetry(mailMessage, recipientEmail, mailId, 1);
                }
                else
                    throw e;
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("Failed to send email to {0}", recipientEmail), e);
                _mailAccounting.UpdateStatus(mailId, MailStatusType.Failed);
                return false;
            }
        }
        private void SendSmtpWhenTransactionComplete(object sender, TransactionEventArgs e)
        {
            if (e.Transaction.TransactionInformation.Status == TransactionStatus.Committed)
            {
                foreach (var deferredMessage in _deferredMessages)
                {
                    var mailId = deferredMessage.Item1;
                    var recipientEmail = deferredMessage.Item2;
                    var mailMessage = deferredMessage.Item3;

                    SendSmtp(mailMessage, recipientEmail, mailId);
                }
            }
            else
            {
                _logger.Info("Transaction failed. {0} mail messages was dropped", _deferredMessages.Count);
            }
            _deferredMessages.Clear();

            if (Transaction.Current != null)
            {
                Transaction.Current.TransactionCompleted -= SendSmtpWhenTransactionComplete;    
            }
        }

        private MailMessage GetMailMessage(AutoMailMessage autoMailMessage)
        {
            var sender = string.IsNullOrWhiteSpace(autoMailMessage.SenderEmail) ? _autoMailConfiguration.Sender : autoMailMessage.SenderEmail;

            var mailMessage = new MailMessage()
            {
                Subject = autoMailMessage.Subject,
                Body = autoMailMessage.Body,
                IsBodyHtml = true,
                From = new MailAddress(sender)
            };

            if (autoMailMessage.RecipientEmail != null)
            {
                autoMailMessage.RecipientEmail
                    .Split(',')
                    .ToList()
                    .ForEach(email => {
                        if(email.Trim()!="")
                            mailMessage.To.Add(email);
                    });            
            }

            return mailMessage;
        }

        
    }
}