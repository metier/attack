﻿using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Web.Configuration;
using System.Web;
using Metier.Phoenix.Mail.Configuration;
using Newtonsoft.Json;
using System;
using NLog;
using Castle.Core.Logging;
using System.Net.Mime;

namespace Metier.Phoenix.Mail
{
    public class SmtpClientProxy : ISmtpClientProxy
    {
        private readonly SmtpClient _smtpClient;
        private readonly Logger _logger;

        public SmtpClientProxy(IAutoMailConfiguration configuration)
        {
            _smtpClient = ConfigureSmtpClient(configuration);
            _logger = LogManager.GetCurrentClassLogger();

        }

        private SmtpClient ConfigureSmtpClient(IAutoMailConfiguration configuration)
        {
            var smtpConfiguration = configuration.SmtpClientConfiguration;
            var smtpClient = new SmtpClient
            {
                Host = smtpConfiguration.Host,
                EnableSsl = smtpConfiguration.EnableSsl
            };

            if (!string.IsNullOrEmpty(smtpConfiguration.UserName))
            {
                smtpClient.Credentials = new NetworkCredential(smtpConfiguration.UserName, smtpConfiguration.Password);
            }
            return smtpClient;
        }

        public void Send(MailMessage mailMessage)
        {
            _logger.Info("Sending normal email");
            _smtpClient.Send(mailMessage);
        }

        private string ToUniversalIso8601(DateTime dateTime, bool includeZ = true)
        {
            var r =  dateTime.ToUniversalTime().ToString("u").Replace(" ", "T").Replace("-","").Replace(":", "").Trim();
            if (!includeZ) {
                return r.Replace("Z", "");
            }
            return r;
        }

        private string CleanText(string text)
        {
            return text.Replace(":", "");
        }


        //private string GetTextCalendarInvitation(CalendarInvitation calendarInvitation)
        //{
        //    string bodyText = "Type:Single Meeting\r\nOrganizer: {0}\r\nStart Time:{1}\r\nEnd Time:{2}\r\nTime Zone:{3}\r\nLocation: {4}\r\n\r\n*~*~*~*~*~*~*~*~*~*\r\n\r\n{5}";
        //    bodyText = string.Format(bodyText,
        //        calendarInvitation.OrganizerName,
        //        calendarInvitation.StartTime.Value.ToLongDateString() + " " + calendarInvitation.StartTime.Value.ToLongTimeString(),
        //        calendarInvitation.EndTime.Value.ToLongDateString() + " " + calendarInvitation.EndTime.Value.ToLongTimeString(),
        //        System.TimeZone.CurrentTimeZone.StandardName,
        //        calendarInvitation.Location,
        //        calendarInvitation.CalendarDescription);
        //    return bodyText;
        //}

        private string GetCalendarInvitation(System.Net.Mail.MailMessage mailMessage, CalendarInvitation calendarInvitation)
        {
            StringBuilder str = new StringBuilder();
            str.AppendLine("BEGIN:VCALENDAR");
            str.AppendLine("METHOD:REQUEST");
            str.AppendLine("PRODID:Microsoft Exchange Server 2010");
            str.AppendLine("VERSION:2.0");
            str.AppendLine("BEGIN:VTIMEZONE");
            str.AppendLine("TZID:W. Europe Standard Time");
            str.AppendLine("BEGIN:STANDARD");
            str.AppendLine("DTSTART:16010101T030000");
            str.AppendLine("TZOFFSETFROM:+0200");
            str.AppendLine("TZOFFSETTO:+0100");
            str.AppendLine("RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=-1SU;BYMONTH=10");
            str.AppendLine("END:STANDARD");
            str.AppendLine("BEGIN:DAYLIGHT");
            str.AppendLine("DTSTART:16010101T020000");
            str.AppendLine("TZOFFSETFROM:+0100");
            str.AppendLine("TZOFFSETTO:+0200");
            str.AppendLine("RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=-1SU;BYMONTH=3");
            str.AppendLine("END:DAYLIGHT");
            str.AppendLine("END:VTIMEZONE");

          
                str.AppendLine("BEGIN:VEVENT");
                str.AppendLine($"ORGANIZER;CN={calendarInvitation.OrganizerName}:mailto:{mailMessage.From.Address}");
                str.AppendLine($"ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN={calendarInvitation.ReceiverName}:mailto:{mailMessage.To[0].Address}");
                str.AppendLine($"ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN={calendarInvitation.OrganizerName}:mailto:{mailMessage.From.Address}");
                str.AppendLine($"UID:{Guid.NewGuid().ToString("B")}");
                str.AppendLine($"SUMMARY;LANGUAGE=en-US:{CleanText(calendarInvitation.CalendarTitle)}");
                str.AppendLine($"DTSTART;TZID=W. Europe Standard Time:{ToUniversalIso8601(calendarInvitation.StartTime, false)}");
                str.AppendLine($"DTEND;TZID=W. Europe Standard Time:{ToUniversalIso8601(calendarInvitation.EndTime, false)}");
                str.AppendLine("CLASS:PUBLIC");
                str.AppendLine("PRIORITY:5");
                str.AppendLine($"DTSTAMP:{ToUniversalIso8601(DateTime.Now)}");
                str.AppendLine("TRANSP:OPAQUE");
                str.AppendLine("STATUS:CONFIRMED");
                str.AppendLine($"SUMMARY:{calendarInvitation.CalendarDescription}");

                str.AppendLine("SEQUENCE:0");
                str.AppendLine($"LOCATION;LANGUAGE=en-US:{calendarInvitation.Location}");

                str.AppendLine("BEGIN:VALARM");
                str.AppendLine("DESCRIPTION:REMINDER");
                str.AppendLine("TRIGGER;RELATED=START:-PT15M");
                str.AppendLine("ACTION:DISPLAY");
                str.AppendLine("END:VALARM");
                str.AppendLine("END:VEVENT");
         

            
            str.AppendLine("END:VCALENDAR");

            _logger.Info("----");
             _logger.Info(str.ToString());
            _logger.Info("----");

            return str.ToString();
        }

        public void SendInvitation(MailMessage mailMessage, CalendarInvitation calendarInvitation)
        {
            _logger.Info("Sending invitation email");
           

            System.Net.Mime.ContentType textType = new System.Net.Mime.ContentType("text/plain");
            System.Net.Mime.ContentType calendarType = new System.Net.Mime.ContentType("text/calendar");
            System.Net.Mime.ContentType HTMLType = new System.Net.Mime.ContentType("text/html");
            calendarType.Parameters.Add("method", "REQUEST");
            calendarType.Parameters.Add("name", "meeting.ics");
            //AlternateView textView = AlternateView.CreateAlternateViewFromString(GetTextCalendarInvitation(calendarInvitation), textType);
            //mailMessage.AlternateViews.Add(textView);

            AlternateView HTMLView = AlternateView.CreateAlternateViewFromString(mailMessage.Body, HTMLType);
            mailMessage.AlternateViews.Add(HTMLView); 
            
            
            AlternateView calendarView = AlternateView.CreateAlternateViewFromString(GetCalendarInvitation(mailMessage, calendarInvitation), calendarType);
            calendarView.TransferEncoding = TransferEncoding.SevenBit;

            mailMessage.AlternateViews.Add(calendarView);

            mailMessage.Subject = calendarInvitation.CalendarDescription;
            mailMessage.Body = mailMessage.Body;
            _smtpClient.Send(mailMessage);
        }
    }
}