﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metier.Phoenix.Mail
{
    public class CalendarInvitation
    {
        public bool? IncludeCalendarInvitation { get; set; }
        public string CalendarTitle { get; set; }
        public string CalendarDescription { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Location { get; set; }
        public string OrganizerName { get; set; }
        public string ReceiverName { get; set; }
    }
}
