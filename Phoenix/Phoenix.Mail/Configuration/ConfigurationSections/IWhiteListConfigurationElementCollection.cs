﻿using System.Collections;

namespace Metier.Phoenix.Mail.Configuration.ConfigurationSections
{
    public interface IWhiteListConfigurationElementCollection : IEnumerable
    {
        WhiteListConfigurationElement this[int index] { get; set; }
    }
}