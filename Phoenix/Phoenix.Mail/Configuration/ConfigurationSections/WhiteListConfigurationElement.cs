﻿using System.Configuration;

namespace Metier.Phoenix.Mail.Configuration.ConfigurationSections
{
    public class WhiteListConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("email", IsRequired = true)]
        public virtual string Email
        {
            get
            {
                return this["email"] as string;
            }
        }
    }
}