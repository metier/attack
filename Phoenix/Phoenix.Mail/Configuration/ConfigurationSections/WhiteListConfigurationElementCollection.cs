﻿using System.Configuration;

namespace Metier.Phoenix.Mail.Configuration.ConfigurationSections
{
    public class WhiteListConfigurationElementCollection : ConfigurationElementCollection, IWhiteListConfigurationElementCollection
    {
        public WhiteListConfigurationElement this[int index]
        {
            get
            {
                return BaseGet(index) as WhiteListConfigurationElement;
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new WhiteListConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((WhiteListConfigurationElement)element).Email;
        }
    }
}