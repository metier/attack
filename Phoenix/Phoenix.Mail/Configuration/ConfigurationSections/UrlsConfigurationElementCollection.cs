﻿using System.Configuration;

namespace Metier.Phoenix.Mail.Configuration.ConfigurationSections
{
    public class UrlsConfigurationElementCollection : ConfigurationElementCollection, IUrlsConfigurationElementCollection
    {
        public UrlsConfigurationElement this[int index]
        {
            get
            {
                return BaseGet(index) as UrlsConfigurationElement;
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new UrlsConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((UrlsConfigurationElement)element).Key;
        }
    }
}