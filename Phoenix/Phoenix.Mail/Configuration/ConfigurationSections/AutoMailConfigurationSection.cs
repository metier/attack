﻿using System.Configuration;

namespace Metier.Phoenix.Mail.Configuration.ConfigurationSections
{
    public class AutoMailConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("disabled", IsRequired = false)]
        public bool Disabled
        {
            get
            {
                var configuration = this["disabled"] as bool?;
                return configuration.HasValue && configuration.Value;
            }
        }

        [ConfigurationProperty("sender", IsRequired = true)]
        public virtual string Sender
        {
            get
            {
                return this["sender"] as string;
            }
        }

        [ConfigurationProperty("smtp")]
        public SmtpConfigurationElement Smtp
        {
            get
            {
                return this["smtp"] as SmtpConfigurationElement;
            }
        }

        [ConfigurationProperty("whitelist")]
        public WhiteListConfigurationElementCollection WhiteList
        {
            get
            {
                return this["whitelist"] as WhiteListConfigurationElementCollection;
            }
        }

        [ConfigurationProperty("urls")]
        public UrlsConfigurationElementCollection Urls
        {
            get
            {
                return this["urls"] as UrlsConfigurationElementCollection;
            }
        }
    }
}