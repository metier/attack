﻿using System.Configuration;

namespace Metier.Phoenix.Mail.Configuration.ConfigurationSections
{
    public class UrlsConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("key", IsRequired = true)]
        public virtual string Key
        {
            get
            {
                return this["key"] as string;
            }
        }

        [ConfigurationProperty("url", IsRequired = true)]
        public virtual string Url
        {
            get
            {
                return this["url"] as string;
            }
        }
    }
}