﻿using System.Collections;

namespace Metier.Phoenix.Mail.Configuration.ConfigurationSections
{
    public interface IUrlsConfigurationElementCollection : IEnumerable
    {
        UrlsConfigurationElement this[int index] { get; set; }
    }
}