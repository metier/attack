﻿using System.Configuration;

namespace Metier.Phoenix.Mail.Configuration.ConfigurationSections
{
    public class SmtpConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("host", IsRequired = true)]
        public virtual string Host
        {
            get
            {
                return this["host"] as string;
            }
        }

        [ConfigurationProperty("username", IsRequired = false)]
        public virtual string UserName
        {
            get
            {
                return this["username"] as string;
            }
        }

        [ConfigurationProperty("password", IsRequired = false)]
        public virtual string Password
        {
            get
            {
                return this["password"] as string;
            }
        }

        [ConfigurationProperty("enableSsl", IsRequired = false)]
        public virtual bool EnableSsl
        {
            get
            {
                var enableSsl = this["enableSsl"] as bool?;
                return enableSsl.GetValueOrDefault();
            }
        }
    }
}