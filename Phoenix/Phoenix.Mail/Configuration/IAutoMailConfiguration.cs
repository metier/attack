﻿using Metier.Phoenix.Mail.Configuration.ConfigurationSections;

namespace Metier.Phoenix.Mail.Configuration
{
    public interface IAutoMailConfiguration
    {
        SmtpConfigurationElement SmtpClientConfiguration { get; }
        IWhiteListConfigurationElementCollection AutoMailWhiteList { get; }
        IUrlsConfigurationElementCollection Urls { get; }
        bool AutoMailEnabled { get; }
        string Sender { get; }
    }
}