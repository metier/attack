﻿using System.Configuration;
using Metier.Phoenix.Mail.Configuration.ConfigurationSections;

namespace Metier.Phoenix.Mail.Configuration
{
    public class AutoMailConfiguration : IAutoMailConfiguration
    {
        private readonly AutoMailConfigurationSection _autoMailConfigurationSection;

        public AutoMailConfiguration()
        {
            _autoMailConfigurationSection = (AutoMailConfigurationSection)ConfigurationManager.GetSection("autoMail");
        }

        public SmtpConfigurationElement SmtpClientConfiguration
        {
            get
            {
                if (_autoMailConfigurationSection != null)
                {
                    return _autoMailConfigurationSection.Smtp;
                }
                return null;
            }
        }

        public IWhiteListConfigurationElementCollection AutoMailWhiteList
        {
            get
            {
                if (_autoMailConfigurationSection != null)
                {
                    return _autoMailConfigurationSection.WhiteList;
                }
                return null;
            }
        }
        
        public IUrlsConfigurationElementCollection Urls
        {
            get
            {
                if (_autoMailConfigurationSection != null)
                {
                    return _autoMailConfigurationSection.Urls;
                }
                return null;
            }
        }

        public bool AutoMailEnabled
        {
            get
            {
                if (_autoMailConfigurationSection != null)
                {
                    return !_autoMailConfigurationSection.Disabled;
                }
                return false;
            }
        }

        public string Sender
        {
            get
            {
                if (_autoMailConfigurationSection != null)
                {
                    return _autoMailConfigurationSection.Sender;
                }
                return string.Empty;
            }
        }
    }
}