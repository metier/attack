﻿using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Mail.Helpers
{
    public interface ILanguageHelper
    {
        string GetPreferredLanguageIdentifier(User user, Customer userCustomer);
    }
}