﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Resources;
using System.Text;
using System.Web;
using Metier.Phoenix.Core.Business;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Core.Utilities.Extensions;
using Metier.Phoenix.Core.Utilities.Sorting;
using Metier.Phoenix.Mail.Resources;
using Metier.Phoenix.Core.Business.DateAndTime;

namespace Metier.Phoenix.Mail.Helpers
{
    public class ElearningCourseHelper : IElearningCourseHelper
    {
        private readonly MetierLmsContext _context;
        private readonly ISortNumberedStrings _sortNumberedStrings;

        private ParticipantBusiness participantBusiness;

        public ElearningCourseHelper(MetierLmsContext context, ISortNumberedStrings sortNumberedStrings)
        {
            _context = context;
            _sortNumberedStrings = sortNumberedStrings;
            participantBusiness = new ParticipantBusiness(_context);
        }

        public bool IsAheadOfSchedule(Participant participant)
        {
            var activity = _context.Activities.First(a => a.Id == participant.ActivityId);
            var lessons = _context.Rcos.Where(r => r.ParentId == activity.RcoCourseId && r.Context == "lesson");
            var lessonIds = lessons.Select(l => l.Id).ToList();
            var completedLessons = _context.ScormPerformances.Where(p => lessonIds.Contains(p.RcoId) &&
                                                                         p.UserId == participant.UserId &&
                                                                         p.Status == ScormLessonStatus.Complete).ToList();

            var weeklyLessons = GetWeeklyLessons(participant, activity);
            var expectedNumberOfCompletedLessons = weeklyLessons.Where(lsn => lsn.Key < IsoWeek.GetIsoWeekFromDate(DateTime.UtcNow))
                                                                .Sum(lsn => lsn.Value.Count);

            return completedLessons.Count >= expectedNumberOfCompletedLessons;
        }

        public string GetElearningSchedule(Participant participant, Activity activity, string languageIdentifier)
        {
            var weeklyLessons = GetWeeklyLessons(participant, activity);
            var weekString = GetTerm("Week", languageIdentifier);

            var sb = new StringBuilder();
            foreach (var weeklyLesson in weeklyLessons)
            {
                sb.AppendFormat("<p><strong>{0} {1}:</strong><br />", weekString, weeklyLesson.Key.WeekNumber);

                foreach (var lesson in weeklyLesson.Value)
                {
                    sb.Append(" " + HttpUtility.HtmlEncode(lesson.Name) + "<br />");
                }

                sb.Append("</p>");
            }
            return sb.ToString();
        }

        public DateTime GetElearningStartDate(Participant participant, Activity activity)
        {
            return participantBusiness.GetParticipantStart(participant, activity);
        }


        //private DateTime? GetElearningDeadline(Activity activity, DateTime startDate, long additionalTime = 0)
        //{
        //    if (activity.CompletionTargetDate.HasValue)
        //    {
        //        var deadline = activity.CompletionTargetDate;
        //        deadline = deadline + TimeSpan.FromMilliseconds(additionalTime);

        //        return deadline;
        //    }
        //    if (activity.CompletionTargetDuration.HasValue)
        //    {
        //        var deadline = startDate + TimeSpan.FromMilliseconds(activity.CompletionTargetDuration.Value);
        //        deadline = deadline + TimeSpan.FromMilliseconds(additionalTime);

        //        return deadline;
        //    }
        //    return null;
        //}

        public  DateTime? GetElearningDeadline(Participant participant, Activity activity)
        {
            var customerArticle = _context.CustomerArticles.Include(ca=> ca.ArticleCopy).First(ca => ca.Id == activity.CustomerArticleId);
            return participantBusiness.GetDueDate(participant, activity, customerArticle.ArticleCopy);

            //var startDate = business.GetParticipantStart(participant, activity);

            //var startDate = GetElearningStartDate(participant, activity);

            //return GetElearningDeadline(activity, startDate, participant.AdditionalTime.GetValueOrDefault());
        }

        internal Dictionary<IsoWeek, List<Rco>> GetWeeklyLessons(List<Rco> lessons, int numberOfWeeks, DateTime startDate)
        {
            if (lessons.Count == 0)
            {
                return new Dictionary<IsoWeek, List<Rco>>();
            }
            var partitionedLessons = GetPartitionedLessons(lessons, numberOfWeeks);
            var result = partitionedLessons.Select((lessns, idx) => new { Week = ConstructIsoWeek(startDate, idx), Lessons = lessns })
                                           .Where(x => x.Lessons.Count > 0).OrderBy(w => w.Week).ToDictionary(x => x.Week, x => x.Lessons);

            return result;
        }

        private IsoWeek ConstructIsoWeek(DateTime startDate, int index)
        {
            var dateInWeek = startDate.AddDays(index*7);
            return IsoWeek.GetIsoWeekFromDate(dateInWeek);
        }

        private Dictionary<IsoWeek, List<Rco>> GetWeeklyLessons(Participant participant, Activity activity)
        {
            var weeklyLessons = new Dictionary<IsoWeek, List<Rco>>();

            var startDate = GetElearningStartDate(participant, activity);
            var deadline = GetElearningDeadline(participant, activity);
            //var deadline = GetElearningDeadline(activity, startDate, participant.AdditionalTime.GetValueOrDefault());

            if (!deadline.HasValue)
            {
                return weeklyLessons;
            }

            var timeSpan = deadline.Value - startDate;

            if (timeSpan.Ticks < 0)
            {
                return weeklyLessons;
            }

            var numberOfWeeks = (int) Math.Ceiling(timeSpan.TotalDays/7);

            var lessons = GetLessonsForActivity(activity.Id).OrderBy(l => _sortNumberedStrings.PadInitialNumbersForSorting(l.Name, l.IsFinalTest ?? false)).ToList();
            weeklyLessons = GetWeeklyLessons(lessons, numberOfWeeks, startDate);

            return weeklyLessons;
        }

        private string GetTerm(string term, string languageIdentifier)
        {
            var resourceManager = new ResourceManager(typeof(Terms));
            var culture = LanguageToCultureMapper.GetCulture(languageIdentifier);
            return resourceManager.GetString(term, culture);
        }

        private static IEnumerable<List<Rco>> GetPartitionedLessons(List<Rco> lessons, int numberOfWeeks)
        {
            if (lessons.Count >= numberOfWeeks)
            {
                return lessons.Compress(numberOfWeeks).ToList();
            }
            return lessons.Stretch(numberOfWeeks).ToList();
        }

        private List<Rco> GetLessonsForActivity(int activityId)
        {
            return _context.Activities.Where(a => a.Id == activityId)
                            .Join(_context.Rcos, a => a.RcoCourseId, r => r.ParentCourseId, (a, r) => r)
                            .Where(r => r.Context == "lesson" && r.ParentContext == "course").ToList();
        }
    }
}