﻿using System;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Mail.Helpers
{
    public interface IElearningCourseHelper
    {
        bool IsAheadOfSchedule(Participant participant);

        string GetElearningSchedule(Participant participant, Activity activity, string languageIdentifier);
        DateTime? GetElearningDeadline(Participant participant, Activity activity);
        DateTime GetElearningStartDate(Participant participant, Activity activity);
    }
}