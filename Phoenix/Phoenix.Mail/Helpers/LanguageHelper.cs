﻿using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Mail.Helpers
{
    public class LanguageHelper : ILanguageHelper
    {
        private readonly MetierLmsContext _context;

        public LanguageHelper(MetierLmsContext  context)
        {
            _context = context;
        }

        public string GetPreferredLanguageIdentifier(User user, Customer userCustomer)
        {
            var languageId = user.PreferredLanguageId ?? userCustomer.LanguageId ?? 0;
            return languageId != 0 ? _context.Languages.First(l => l.Id == languageId).Identifier : "EN";
        }
    }
}