﻿namespace Metier.Phoenix.Tasks
{
    public interface ITaskRunner
    {
        void RunTask(string name);
    }
}