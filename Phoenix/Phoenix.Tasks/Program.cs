﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Metier.Phoenix.Core.Business;
using Metier.Phoenix.Core.Configuration;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Tasks.Configuration;
using Metier.Phoenix.Tasks.Registration;
using NLog;

namespace Metier.Phoenix.Tasks
{
    public class Program
    {
        private static readonly string HelpPageResourceName = typeof(Program).Namespace + ".help.txt";

        public static void Main(string[] args)
        {
            var logger = LogManager.GetCurrentClassLogger();
            
            if (args == null || args.Length == 0)
            {
                Console.WriteLine("No options specified. See \"{0} -help\" for information about how to use the console application.", Assembly.GetExecutingAssembly().GetName().Name);
                logger.Debug("Phoenix.Tasks invoked without arguments");
                return;
            }

            logger.Info("Running Phoenix.Tasks with arguments: {0}", string.Join(" ", args));

            switch (args[0])
            {
                case "-register":
                    RegisterTasks.RegisterFromConfig();
                    break;
                case "-run":
                    if (string.IsNullOrEmpty(args[1]))
                    {
                        Console.WriteLine("No task was specified. See \"{0} -help\" for information about how to use the console application.", Assembly.GetExecutingAssembly().GetName().Name);
                    }
                    var container = BootstrapContainer();
                    container.Resolve<ITaskRunner>().RunTask(args[1]);
                    break;
                case "-help":
                    PrintHelpPage();
                    break;
                default:
                    Console.WriteLine("Unrecognized option: {0}. See \"{1} -help\" for information about how to use the console application.", args[0], Assembly.GetExecutingAssembly().GetName().Name);
                    break;
            }
        }

        private static WindsorContainer BootstrapContainer()
        {
            var container = new WindsorContainer();
            container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel));
            container.Register(Component.For<ICoreConfiguration, IPhoenixTasksConfiguration>().ImplementedBy<PhoenixTasksConfiguration>().LifestyleTransient());
            container.Install(FromAssembly.Containing<IAutoMailer>());
            container.Install(FromAssembly.Containing<ITaskRunner>());
            container.Install(FromAssembly.Containing<IParticipantBusiness>());
            return container;
        }

        private static void PrintHelpPage()
        {
            var assembly = Assembly.GetAssembly(typeof(Program));
            using (var stream = assembly.GetManifestResourceStream(HelpPageResourceName))
            using (var reader = new StreamReader(stream))
            {
                var helpFileString = reader.ReadToEnd();
                Console.Write(helpFileString);
                Console.WriteLine();
            }
        }
    }
}
