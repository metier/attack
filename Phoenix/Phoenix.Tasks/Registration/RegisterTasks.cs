﻿using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using Metier.Phoenix.Tasks.Configuration;
using Microsoft.Win32.TaskScheduler;
using NLog;

namespace Metier.Phoenix.Tasks.Registration
{
    internal static class RegisterTasks
    {
        internal static void RegisterFromConfig()
        {
            var logger = LogManager.GetCurrentClassLogger();

            try
            {
                var taskDefinitions = ((TasksConfigurationSection)ConfigurationManager.GetSection("tasks")).TaskDefinitions;
                using (var taskService = new TaskService())
                {
                    foreach (TaskDefinitionConfigurationElement taskDefinitionSpec in taskDefinitions)
                    {
                        RegisterTask(taskService, taskDefinitionSpec);
                    }
                }
            }
            catch (Exception e)
            {
                logger.ErrorException("An exception occurred while registering tasks", e);
            }
        }

        private static void RegisterTask(TaskService taskService, TaskDefinitionConfigurationElement taskDefinitionSpec)
        {
            if (taskService.RootFolder.Tasks.Exists(taskDefinitionSpec.Name))
            {
                taskService.RootFolder.DeleteTask(taskDefinitionSpec.Name);
            }

            var taskDefinition = taskService.NewTask();
            taskDefinition.Principal.LogonType = TaskLogonType.ServiceAccount;
            taskDefinition.Principal.UserId = "SYSTEM";
            taskDefinition.RegistrationInfo.Description = taskDefinitionSpec.Description;
            taskDefinition.Actions.Add(new ExecAction(GetConsolePath(), string.Format("-run {0}", taskDefinitionSpec.Name.Split(new[] { "#" }, StringSplitOptions.None)[0])));
            taskDefinition.Settings.StartWhenAvailable = true;

            if (taskDefinitionSpec.TriggerType == TriggerType.Interval)
            {
                var registrationTrigger = (RegistrationTrigger) taskDefinition.Triggers.Add(new RegistrationTrigger());
                registrationTrigger.Repetition.Interval = TimeSpan.FromMinutes(taskDefinitionSpec.Timespan);
            }
            if (taskDefinitionSpec.TriggerType == TriggerType.Daily)
            {
                var configurationTime = taskDefinitionSpec.Time;
                var hour = Convert.ToInt32(string.IsNullOrEmpty(configurationTime.Substring(0, 2).TrimStart('0')) ? "0" : configurationTime.Substring(0, 2).TrimStart('0'));
                var minute = Convert.ToInt32(string.IsNullOrEmpty(configurationTime.Substring(2).TrimStart('0')) ? "0" : configurationTime.Substring(2).TrimStart('0'));
                var startBoundary = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, hour, minute, 0);
                
                taskDefinition.Triggers.Add(new DailyTrigger {StartBoundary = startBoundary});
            }

            taskService.RootFolder.RegisterTaskDefinition(taskDefinitionSpec.Name, taskDefinition);
        }

        internal static string GetConsolePath()
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            return Path.GetFullPath(path);
        }
    }
}