﻿namespace Metier.Phoenix.Tasks.Configuration
{
    public enum TriggerType
    {
        Interval,
        Daily
    }
}