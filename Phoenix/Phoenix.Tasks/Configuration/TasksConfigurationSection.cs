﻿using System.Configuration;

namespace Metier.Phoenix.Tasks.Configuration
{
    public class TasksConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("taskDefinition")]
        public TaskDefinitionConfigurationElementCollection TaskDefinitions
        {
            get
            {
                return this["taskDefinition"] as TaskDefinitionConfigurationElementCollection;
            }
        }
    }
}