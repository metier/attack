﻿using System.Configuration;

namespace Metier.Phoenix.Tasks.Configuration
{
    public class PhoenixTasksConfiguration : IPhoenixTasksConfiguration
    {
        public string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["MetierLmsContext"].ConnectionString; }
        }

        public string McsConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["MetierMcsContext"].ConnectionString; }
        }

        public string PhoenixApiUrl
        {
            get { return ConfigurationManager.AppSettings["PhoenixApiUrl"]; }
        }

        public string GetPasswordResetFormUrl(string source)
        {
            var settingString = "PasswordResetFormUrl-" + source;
            return ConfigurationManager.AppSettings[settingString];
        }
    }
}