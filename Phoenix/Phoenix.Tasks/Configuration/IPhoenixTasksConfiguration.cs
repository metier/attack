﻿using Metier.Phoenix.Core.Configuration;

namespace Metier.Phoenix.Tasks.Configuration
{
    public interface IPhoenixTasksConfiguration : ICoreConfiguration
    {
        string McsConnectionString { get; }
    }
}