using System;
using System.Configuration;

namespace Metier.Phoenix.Tasks.Configuration
{
    public class TaskDefinitionConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public virtual string Name
        {
            get
            {
                return this["name"] as string;
            }
        }

        [ConfigurationProperty("description", IsRequired = true)]
        public virtual string Description
        {
            get
            {
                return this["description"] as string;
            }
        }

        [ConfigurationProperty("triggerType", IsRequired = true)]
        public virtual TriggerType TriggerType
        {
            get
            {
                TriggerType tmp;
                return (TriggerType) this["triggerType"];
            }
        }

        [ConfigurationProperty("time", IsRequired = false)]
        public string Time
        {
            get { return this["time"] as string; }
        }

        [ConfigurationProperty("timespanInMinutes", IsRequired = false)]
        public virtual int Timespan
        {
            get
            {
                var timespan = this["timespanInMinutes"] as int?;
                return timespan.HasValue ? timespan.Value : 599940; // 599940 is max value for scheduled tasks
            }
        }
    }
}