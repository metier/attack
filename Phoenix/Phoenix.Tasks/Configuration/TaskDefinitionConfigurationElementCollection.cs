﻿using System.Configuration;

namespace Metier.Phoenix.Tasks.Configuration
{
    public class TaskDefinitionConfigurationElementCollection : ConfigurationElementCollection
    {
        public TaskDefinitionConfigurationElement this[int index]
        {
            get
            {
                return BaseGet(index) as TaskDefinitionConfigurationElement;
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new TaskDefinitionConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((TaskDefinitionConfigurationElement)element).Name;
        }
    }
}