﻿using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Tasks.Services;
using Metier.Phoenix.Tasks.Services.Agresso;
using Metier.Phoenix.Tasks.Services.Agresso.Configuration.Norway;
using NLog;

namespace Metier.Phoenix.Tasks.Tasks.UpdateCustomers
{
    public class UpdateMetierAcademyNorwayAgressoCustomers : ITask
    {
        private readonly IAgressoNorwayConfiguration _agressoNorwayConfiguration;
        private readonly IAgressoService _agressoService;
        private readonly MetierLmsContext _context;
        private Logger _logger;

        public UpdateMetierAcademyNorwayAgressoCustomers(IAgressoNorwayConfiguration agressoNorwayConfiguration, IAgressoService agressoService, MetierLmsContext context)
        {
            _logger = LogManager.GetCurrentClassLogger();
            _agressoNorwayConfiguration = agressoNorwayConfiguration;
            _agressoService = agressoService;
            _context = context;
        }

        public string Name { get { return "Phoenix-UpdateMetierAcademyNorwayAgressoCustomers"; } }

        public void Run()
        {
            var externalCustomers = _agressoService.GetCustomers(_agressoNorwayConfiguration).ToList();
            _logger.Debug("Retrieved {0} Metier Academy Norway customers from Agresso for update", externalCustomers.Count);

            var externalCustomerIds = externalCustomers.Select(e => e.Id).ToList();
            var customers = _context.Customers.Include(c => c.Addresses).Where(c => c.DistributorId == 1194 && externalCustomerIds.Contains(c.ExternalCustomerId)).ToList();

            var numberOfCustomersWithChanges = 0;

            foreach (var customer in customers)
            {
                var externalCustomer = externalCustomers.First(e => e.Id == customer.ExternalCustomerId);
                if (HasChanges(customer, externalCustomer))
                {
                    numberOfCustomersWithChanges++;
                    _logger.Debug("{0} (ID: {1}) has changes. Updating.", customer.Name, customer.Id);

                    customer.Name = externalCustomer.Name;
                    customer.CompanyRegistrationNumber = externalCustomer.CompanyRegistrationNumber;

                    var invoiceAddress = customer.InvoicingAddress;
                    if (invoiceAddress != null)
                    {
                        invoiceAddress.AddressStreet1 = externalCustomer.AddressStreet1;
                        invoiceAddress.AddressStreet2 = externalCustomer.AddressStreet2;
                        invoiceAddress.ZipCode = externalCustomer.ZipCode;
                        invoiceAddress.City = externalCustomer.City;
                        invoiceAddress.Country = externalCustomer.Country;    
                    }
                    _context.SaveChanges();
                }
            }

            _logger.Debug("Updated {0}  Metier Academy Norway customers.", numberOfCustomersWithChanges);
        }

        private bool HasChanges(Customer customer, ExternalCustomer externalCustomer)
        {
            if (externalCustomer.Name != customer.Name || externalCustomer.CompanyRegistrationNumber != customer.CompanyRegistrationNumber)
            {
                return true;
            }

            var invoiceAddress = customer.InvoicingAddress;
            if (invoiceAddress == null)
            {
                return false;
            }
            if (externalCustomer.AddressStreet1 != invoiceAddress.AddressStreet1)
            {
                return true;
            }
            if (externalCustomer.AddressStreet2 != invoiceAddress.AddressStreet2)
            {
                return true;
            }
            if (externalCustomer.City != invoiceAddress.City)
            {
                return true;
            }
            if (externalCustomer.Country != invoiceAddress.Country)
            {
                return true;
            }
            if (externalCustomer.ZipCode != invoiceAddress.ZipCode)
            {
                return true;
            }
            return false;
        }
    }
}