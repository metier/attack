﻿namespace Metier.Phoenix.Tasks.Tasks
{
    public interface ITask
    {
        string Name { get; }
        void Run();
    }
}