﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Templates.EnrollmentConfirmation;

namespace Metier.Phoenix.Tasks.Tasks.Automail
{
    public class SendEnrollmentConfirmations : ITask
    {
        private readonly MetierLmsContext _context;
        private readonly IEnrollmentConfirmationFactory _enrollmentConfirmationFactory;
        private readonly IAutoMailer _autoMailer;

        public SendEnrollmentConfirmations(MetierLmsContext context, IEnrollmentConfirmationFactory enrollmentConfirmationFactory, IAutoMailer autoMailer)
        {
            _context = context;
            _enrollmentConfirmationFactory = enrollmentConfirmationFactory;
            _autoMailer = autoMailer;
        }

        public string Name { get { return "Phoenix-SendEnrollmentConfirmations"; } }

        public void Run()
        {
            var participantsForEnrollmentConfirmation = RemoveParticipipantsWhereReminderDisabledForCustomer(GetParticipantsToNotify());
            
            foreach (var participant in participantsForEnrollmentConfirmation)
            {
                var autoMailMessage = _enrollmentConfirmationFactory.CreateMessage(participant, true);
                _autoMailer.Send(autoMailMessage);
            }
        }

        private IEnumerable<Participant> GetParticipantsToNotify()
        {
            var participants = _context.Activities
                .Where(a => a.EnrollmentConfirmationDate.HasValue && a.EnrollmentConfirmationDate.Value < DateTime.UtcNow)
                .Where(a => a.SendEnrollmentConfirmationAutomail.HasValue && a.SendEnrollmentConfirmationAutomail.Value)
                .Join(_context.CustomerArticles, a => a.CustomerArticleId, ca => ca.Id, (activity, customerArticle) => new {Activity = activity, CustomerArticle = customerArticle})
                .Join(_context.Articles, x => x.CustomerArticle.ArticleCopyId, a => a.Id, (x, article) => new {x.Activity, Article = article})
                .Where(x => x.Article.ArticleTypeId != (int) ArticleTypes.Other && x.Article.ArticleTypeId != (int) ArticleTypes.Mockexam)
                .Join(_context.Participants, x => x.Activity.Id, p => p.ActivityId, (activity, participant) => participant)
                .Where(p => !p.IsDeleted && p.StatusCodeValue == ParticipantStatusCodes.Enrolled)
                .GroupJoin(_context.Mails, p => p.Id, m => m.ParticipantId, (p, m) => new {Participant = p, Mail = m})
                .Where(x => x.Mail.All(m => !m.MessageType.HasValue || m.MessageType.Value != MessageType.EnrollmentConfirmation))
                .Select(x => x.Participant)
                .Join(_context.Users, p => p.UserId, u => u.Id, (p, u) => new {Participant = p, User = u})
                .Where(x => x.User.UserNotificationType != UserNotificationTypes.No)
                .Where(x => !x.User.ExpiryDate.HasValue || x.User.ExpiryDate.Value > DateTime.UtcNow)
                .Select(x => x.Participant);

            return participants;
        }

        private IEnumerable<Participant> RemoveParticipipantsWhereReminderDisabledForCustomer(IEnumerable<Participant> participants)
        {
            foreach (var participant in participants)
            {
                var customer = _context.Customers.Include(c => c.AutomailDefinition).First(c => c.Id == participant.CustomerId);
                if (customer.AutomailDefinition != null && customer.AutomailDefinition.IsEnrollmentNotification.HasValue && customer.AutomailDefinition.IsEnrollmentNotification.Value)
                {
                    yield return participant;
                }
            }
        }
    }
}