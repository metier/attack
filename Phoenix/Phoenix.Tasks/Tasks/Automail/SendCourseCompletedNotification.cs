﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Templates.ReminderElearning;

namespace Metier.Phoenix.Tasks.Tasks.Automail
{
    public class SendCourseCompletedNotification : ITask
    {
        private readonly IReminderElearningFactory _reminderElearningFactory;
        private readonly MetierLmsContext _context;
        private readonly IAutoMailer _autoMailer;

        public SendCourseCompletedNotification(IReminderElearningFactory reminderElearningFactory, MetierLmsContext context,
            IAutoMailer autoMailer)
        {
            _reminderElearningFactory = reminderElearningFactory;
            _context = context;
            _autoMailer = autoMailer;
        }

        public string Name { get { return "Phoenix-SendCourseCompletedNotification"; } }

        public void Run()
        {
            var autoMailMessages = GetReminderMessagesForParticipantsCompleted();

            foreach (var autoMailMessage in autoMailMessages)
            {
                _autoMailer.Send(autoMailMessage);
            }
        }

        private IEnumerable<AutoMailMessage> GetReminderMessagesForParticipantsCompleted()
        {
            var participantsCompleted = FindParticipantsCompletedElearning();
            var participantsCompletedToRemind = RemoveParticipipantsWhereEnrollmentConfirmationDisabledForCustomer(participantsCompleted);

            return participantsCompletedToRemind.Select(p => _reminderElearningFactory.CreateCompletedMessage(p));
        }

        private IEnumerable<Participant> RemoveParticipipantsWhereEnrollmentConfirmationDisabledForCustomer(IEnumerable<Participant> participants)
        {
            foreach (var participant in participants)
            {
                var customer = _context.Customers.Include(c => c.AutomailDefinition).First(c => c.Id == participant.CustomerId);
                if (customer.AutomailDefinition != null && customer.AutomailDefinition.IsEnrollmentNotification.HasValue && customer.AutomailDefinition.IsEnrollmentNotification.Value)
                {
                    yield return participant;
                }
            }
        }

        private IEnumerable<Participant> FindParticipantsCompletedElearning()
        {
            var cutOffDate = DateTime.UtcNow.AddDays(-3);
            
            var participants = _context.Articles.Where(a => !a.IsDeleted && (a.ArticleTypeId == (int)ArticleTypes.ELearning))
                    .Join(_context.CustomerArticles, a => a.Id, ca => ca.ArticleCopyId, (a, ca) => ca).Where(ca => !ca.IsDeleted)
                    .Join(_context.Activities, ca => ca.Id, a => a.CustomerArticleId, (ca, a) => a).Where(a => !a.IsDeleted)
                    .Join(_context.Participants, a => a.Id, p => p.ActivityId, (a, p) => new { Activity = a, Participant = p })
                    .Where(x => !x.Participant.IsDeleted && x.Participant.StatusCodeValue == ParticipantStatusCodes.Completed)
                    .Join(_context.Users, x => x.Participant.UserId, u => u.Id, (x, u) => new { x.Activity, x.Participant, User = u })
                    .Where(x => x.User.UserNotificationType == UserNotificationTypes.Yes && (!x.User.ExpiryDate.HasValue || x.User.ExpiryDate.Value > DateTime.UtcNow))
                    .GroupJoin(_context.Mails, x => x.Participant.Id, m => m.ParticipantId, (x, m) => new { x.Activity, x.Participant, Mail = m })
                    // Avoid spamming - filter out participants without completion date or completed more than some days ago
                    .Where(x => x.Participant.CompletedDate.HasValue && x.Participant.CompletedDate.Value > cutOffDate)
                    // Filter out participants already reminded of course completion
                    .Where(x => x.Mail.All(m => !m.ParticipantId.HasValue ||
                                           m.ParticipantId.Value != x.Participant.Id ||
                                           !m.MessageType.HasValue ||
                                           m.MessageType.Value != MessageType.ElearningCompleted))
                    .Select(x => x.Participant);

            return participants.ToList();
        }
    }
}