﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using Metier.Phoenix.Core.Business;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Utilities.Sorting;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Helpers;
using Metier.Phoenix.Mail.Templates.ReminderElearning;
using Metier.Phoenix.Tasks.Tasks.Automail;

namespace Metier.Phoenix.Tasks.Tasks.Automail
{
    public class SendRemindersElearning : ITask
    {
        private readonly IReminderElearningFactory _reminderElearningFactory;
        private readonly IAutoMailer _autoMailer;
        private readonly MetierLmsContext _context;
        private readonly IElearningCourseHelper _elearningCourseHelper;
        private readonly IParticipantBusiness _participantBusiness;

        public SendRemindersElearning(IReminderElearningFactory reminderElearningFactory, 
            IAutoMailer autoMailer, 
            MetierLmsContext context,
            IElearningCourseHelper elearningCourseHelper,
            IParticipantBusiness participantBusiness)
        {
            _reminderElearningFactory = reminderElearningFactory;
            _autoMailer = autoMailer;
            _context = context;
            _elearningCourseHelper = elearningCourseHelper;
            _participantBusiness = participantBusiness;
        }

        public string Name { get { return "Phoenix-SendRemindersElearning"; } }

        public void Run()
        {
            var autoMailMessages = GetReminderMessagesForParticipantsInProgress();
            
            foreach (var autoMailMessage in autoMailMessages)
            {
                _autoMailer.Send(autoMailMessage);
            }
        }

        private IEnumerable<AutoMailMessage> GetReminderMessagesForParticipantsInProgress()
        {
            var participantsInProgress = FindParticipantsQualifyingForReminder().ToList();
            var participantsInProgressToRemind = RemoveParticipipantsPastDueDate(participantsInProgress).ToList();

            var participantsBehind = GetParticipantsBehind(participantsInProgressToRemind);
            var participantsAhead = GetParticipantsAhead(participantsInProgressToRemind);

            var autoMailMessages = participantsBehind.Select(p => _reminderElearningFactory.CreateBehindMessage(p))
                .Concat(participantsAhead.Select(p => _reminderElearningFactory.CreateAheadMessage(p)));
            return autoMailMessages;
        }

        private IEnumerable<Participant> FindParticipantsQualifyingForReminder()
        {

            return ElearningArticles()
                .Join(GetCustomerArticles(), a => a.Id, ca => ca.ArticleCopyId, (a, ca) => ca)
                .Join(Activities(), ca => ca.Id, a => a.CustomerArticleId, (ca, a) => a)
                .Join(GetParticipants(), a => a.Id, p => p.ActivityId, (a, p) => new {Activity = a, Participant = p})
                .Join(GetUsers(), x => x.Participant.UserId, u => u.Id,
                    (x, u) => new {x.Activity, x.Participant, User = u})
                .Join(GetCustomers(), x => x.Participant.CustomerId, c => c.Id,
                    (x, c) => new {x.Activity, x.Participant, x.User, Customer = c})
                .GroupJoin(GetElearningReminderMails(), x => x.Participant.Id, m => m.ParticipantId,
                    (x, m) => new {x.Activity, x.Participant, Mail = m})
                .Where(x => new List<int?>
                {
                    DbFunctions.DiffDays(
                        (x.Mail.Select(m => m.Created)
                            .Concat(
                                new List<DateTime>
                                {
                                    (x.Activity.EnrollmentConfirmationDate.HasValue &&
                                     x.Activity.EnrollmentConfirmationDate.Value > x.Participant.Created)
                                        ? x.Activity.EnrollmentConfirmationDate.Value
                                        : x.Participant.Created
                                }
                            ).Max()), DateTime.UtcNow)
                }.FirstOrDefault(days => days > 13 && days < 60) != null)
                .Select(x => x.Participant);
        }

        private IQueryable<Core.Data.Entities.Mail> GetElearningReminderMails()
        {
            return _context.Mails.Where(m =>
                m.MessageType.HasValue &&
                m.MessageType.Value == MessageType.ReminderElearning);
        }
        private IQueryable<Activity> Activities()
        {
            return _context.Activities.Where(a => !a.IsDeleted && a.SendProgressAutomail.HasValue && a.SendProgressAutomail.Value && (a.ActivityPeriods == null || a.ActivityPeriods.Count == 0 || ((!a.ActivityPeriods[0].Start.HasValue || a.ActivityPeriods[0].Start < DateTime.UtcNow) && (!a.ActivityPeriods[0].End.HasValue || a.ActivityPeriods[0].End > DateTime.UtcNow))));
        }

        private IQueryable<Participant> GetParticipants()
        {
            return _context.Participants.Where(p => !p.IsDeleted && (
                p.StatusCodeValue == ParticipantStatusCodes.Enrolled
                ||
                p.StatusCodeValue == ParticipantStatusCodes.InProgress
                ));
        }

        private IQueryable<Article> ElearningArticles()
        {            
            return _context.Articles.Where(a => !a.IsDeleted && a.ArticleType.Id == (int)ArticleTypes.ELearning).Include(a => a.ArticleType);
        }

        private IQueryable<CustomerArticle> GetCustomerArticles()
        {
            return _context.CustomerArticles.Where(ca => !ca.IsDeleted);
        }
        private IQueryable<User> GetUsers()
        {
            return _context.Users.Where(u => u.UserNotificationType == UserNotificationTypes.Yes &&
                        (!u.ExpiryDate.HasValue || DbFunctions.DiffDays(u.ExpiryDate.Value, DateTime.UtcNow) < 0));
        }
        private IQueryable<Customer> GetCustomers()
        {
            return _context.Customers.Where(c =>
                c.AutomailDefinition != null &&
                c.AutomailDefinition.IsProgressNotificationElearning.HasValue &&
                c.AutomailDefinition.IsProgressNotificationElearning.Value); 
        }
        private IEnumerable<Participant> RemoveParticipipantsPastDueDate(IEnumerable<Participant> participants)
        {
            return participants.Where(p => !_participantBusiness.IsPastDueDate(p.Id));
        }

        private IEnumerable<Participant> GetParticipantsBehind(IEnumerable<Participant> participants)
        {
            return participants.Where(p => !_elearningCourseHelper.IsAheadOfSchedule(p));
        }

        private IEnumerable<Participant> GetParticipantsAhead(IEnumerable<Participant> participants)
        {
            return participants.Where(p => _elearningCourseHelper.IsAheadOfSchedule(p));
        }
    }
}