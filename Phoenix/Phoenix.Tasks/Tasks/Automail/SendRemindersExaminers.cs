﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Business;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Templates.ExaminerReminder;

namespace Metier.Phoenix.Tasks.Tasks.Automail
{
    public class SendRemindersExaminers : ITask
    {
        private readonly IExaminerReminderMessageFactory _examinerReminderMessageFactory;
        private readonly IAutoMailer _autoMailer;
        private readonly MetierLmsContext _context;
        private readonly IParticipantBusiness _participantBusiness;

        public SendRemindersExaminers(
            IExaminerReminderMessageFactory examinerReminderMessageFactory, 
            IAutoMailer autoMailer,
            MetierLmsContext context, 
            IParticipantBusiness participantBusiness)
        {
            _examinerReminderMessageFactory = examinerReminderMessageFactory;
            _autoMailer = autoMailer;
            _context = context;
            _participantBusiness = participantBusiness;
        }

        public string Name { get { return "Phoenix-SendRemindersExaminers"; } }

        public void Run()
        {
            var examiners = _context.Resources.OfType<UserResource>().Include("User.Customer").Where(r => r.ResourceTypeId == 5).ToList();

            foreach (var examiner in examiners)
            {
                var distributor = _context.Distributors.FirstOrDefault(d => d.Id == examiner.User.Customer.DistributorId);
                var activitiesPastDueDate = GetCompletedExamActivities(examiner);

                foreach (var activity in activitiesPastDueDate)
                {
                    var message =_examinerReminderMessageFactory.CreateMessage(examiner, activity, distributor);
                    _autoMailer.Send(message);
                }
            }
        }

        private IEnumerable<Activity> GetCompletedExamActivities(UserResource examiner)
        {
            var notReminded = 
                // Select CaseExam/ProjectAssignment participants and activities assiegned to the examiner
                _context.Participants.Where(p => !p.IsDeleted && p.ExaminerResourceId == examiner.Id)
                .Join(_context.Activities, p => p.ActivityId, a => a.Id, (p, a) => new { Participant = p, Activity = a })
                .Join(_context.CustomerArticles, x => x.Activity.CustomerArticleId, ca => ca.Id, (x, ca) => new {CustomerArticle = ca, x.Activity, x.Participant})
                .Join(_context.Articles, x => x.CustomerArticle.ArticleCopyId, a => a.Id, (x, a) => new {Article = a, x.Activity, x.Participant})
                .Where(x => x.Article.ArticleTypeId == (int) ArticleTypes.CaseExam || x.Article.ArticleTypeId == (int) ArticleTypes.ProjectAssignment)

                // Filter out those where mails have already been sent
                .GroupJoin(_context.Mails, x => x.Activity.Id, m => m.ActivityId, (x, m) => new {x.Participant, x.Activity, x.Article, Mail = m})
                .Where(x => x.Mail.All(m => !m.MessageType.HasValue || m.MessageType.Value != MessageType.ReminderExaminer || m.UserId != examiner.UserId))

                .Select(x => new {x.Participant, x.Activity, x.Article})
                .Distinct()
                .ToList();

            // Load activity periods so that due date can be calculated
            foreach (var activity in notReminded.Select(x => x.Activity))
            {
                _context.Entry(activity).Collection(a => a.ActivityPeriods).Load();
            }

            return notReminded.Where(x => _participantBusiness.IsPastDueDate(x.Participant, x.Activity, x.Article))
                                                   .Select(x => x.Activity)
                                                   .Distinct()
                                                   .ToList();
        }
    }
}