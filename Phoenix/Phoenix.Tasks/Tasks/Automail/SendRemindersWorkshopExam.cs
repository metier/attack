﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Templates.ReminderWorkshopExam;

namespace Metier.Phoenix.Tasks.Tasks.Automail
{
    public class SendRemindersWorkshopExam : ITask
    {
        private readonly MetierLmsContext _context;
        private readonly IReminderWorkshopExamFactory _reminderWorkshopExamFactory;
        private readonly IAutoMailer _autoMailer;

        public SendRemindersWorkshopExam(MetierLmsContext context, IReminderWorkshopExamFactory reminderWorkshopExamFactory,
            IAutoMailer autoMailer)
        {
            _context = context;
            _reminderWorkshopExamFactory = reminderWorkshopExamFactory;
            _autoMailer = autoMailer;
        }

        public string Name { get { return "Phoenix-SendRemindersWorkshopExam"; } }

        public void Run()
        {
            var participantsToRemind = RemoveParticipipantsWhereReminderDisabledForCustomer(FindParticipantsToRemind());
            foreach (var participant in participantsToRemind)
            {
                var mailMessage = _reminderWorkshopExamFactory.CreateMessage(participant);
                _autoMailer.Send(mailMessage);
            }
        }

        private IEnumerable<Participant> FindParticipantsToRemind()
        {
            var articleTypes = new[]
            {
                (int)ArticleTypes.CaseExam,
                (int)ArticleTypes.InternalCertification,
                (int)ArticleTypes.Classroom,
                (int)ArticleTypes.MultipleChoice,
                (int)ArticleTypes.ProjectAssignment,
                (int)ArticleTypes.ExternalCertification
            }; 

            return _context.Articles.Include(a => a.ArticleType).Where(a => !a.IsDeleted && articleTypes.Contains(a.ArticleType.Id))
            .Join(_context.CustomerArticles, a => a.Id, ca => ca.ArticleCopyId, (a, ca) => ca).Where(ca => !ca.IsDeleted)
            .Join(_context.Activities, ca => ca.Id, a => a.CustomerArticleId, (ca, a) => a).Include(a => a.ActivityPeriods)
            .Where(a => !a.IsDeleted && a.SendReminderAutomail.HasValue && a.SendReminderAutomail.Value)
            .Join(_context.Participants, a => a.Id, p => p.ActivityId, (a, p) => new { Activity = a, Participant = p })
            .Where(x => !x.Participant.IsDeleted && x.Participant.StatusCodeValue == ParticipantStatusCodes.Enrolled)
            .Join(_context.Users, x => x.Participant.UserId, u => u.Id, (x, u) => new { x.Activity, x.Participant, User = u })
            .Where(x => x.User.UserNotificationType == UserNotificationTypes.Yes && (!x.User.ExpiryDate.HasValue || DbFunctions.DiffDays(x.User.ExpiryDate.Value, DateTime.UtcNow) < 0))
            // Find activities with Start Date at 14 or 2 days from now
            .Where(x => x.Activity.ActivityPeriods.Count > 0 && x.Activity.ActivityPeriods.Any(ap => ap.Start.HasValue &&
                        (
                        DbFunctions.DiffDays(DateTime.UtcNow, x.Activity.ActivityPeriods.Where(ap2 => ap2.Start.HasValue).OrderBy(ap2 => ap2.Start).Select(ap2 => ap2.Start).FirstOrDefault()) == 14 ||
                        DbFunctions.DiffDays(DateTime.UtcNow, x.Activity.ActivityPeriods.Where(ap2 => ap2.Start.HasValue).OrderBy(ap2 => ap2.Start).Select(ap2 => ap2.Start).FirstOrDefault()) == 2
                        )))
            .GroupJoin(_context.Mails, x => x.Participant.Id, m => m.ParticipantId, (x, m) => new { Mail = m, x.Participant })
            // Filter out participants already reminded today
            .Where(x => x.Mail.All(m => !m.ParticipantId.HasValue ||
                                        m.ParticipantId.Value != x.Participant.Id ||
                                        !m.MessageType.HasValue ||
                                        m.MessageType.Value != MessageType.ReminderWorkshopExam ||
                                        DbFunctions.DiffDays(DateTime.UtcNow, m.Created) != 0))
            .Select(x => x.Participant);
        }

        private IEnumerable<Participant> RemoveParticipipantsWhereReminderDisabledForCustomer(IEnumerable<Participant> participants)
        {
            foreach (var participant in participants)
            {
                var customer = _context.Customers.Include(c => c.AutomailDefinition).First(c => c.Id == participant.CustomerId);
                
                if (customer.AutomailDefinition != null && 
                    customer.AutomailDefinition.IsReminder.HasValue && 
                    customer.AutomailDefinition.IsReminder.Value)
                {
                    yield return participant;
                }
            }
        }
    }
}