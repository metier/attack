﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Transactions;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Core.Security;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Templates.PasswordResetRequest;
using Metier.Phoenix.Mail.Templates.UserRegistrationNotification;
using Metier.Phoenix.Tasks.Configuration;

namespace Metier.Phoenix.Tasks.Tasks.Automail
{
    public class SendUserRegistrationNotifications : ITask
    {
        private readonly MetierLmsContext _context;
        private readonly IUserRegistrationNotificationFactory _userRegistrationNotificationFactory;
        private readonly IAutoMailer _autoMailer;
        private readonly IPhoenixTasksConfiguration _configuration;
        private readonly IPasswordResetRequestMessageFactory _passwordResetRequestMessageFactory;
        private readonly IPasswordResetRequestFactory _passwordResetRequestFactory;

        public SendUserRegistrationNotifications(MetierLmsContext context, IUserRegistrationNotificationFactory userRegistrationNotificationFactory,
            IAutoMailer autoMailer, IPhoenixTasksConfiguration configuration, IPasswordResetRequestMessageFactory passwordResetRequestMessageFactory, 
            IPasswordResetRequestFactory passwordResetRequestFactory)
        {
            _context = context;
            _userRegistrationNotificationFactory = userRegistrationNotificationFactory;
            _autoMailer = autoMailer;
            _configuration = configuration;
            _passwordResetRequestMessageFactory = passwordResetRequestMessageFactory;
            _passwordResetRequestFactory = passwordResetRequestFactory;
        }

        public string Name { get { return "Phoenix-SendUserRegistrationNotifications"; } }

        public void Run()
        {
            var usersToNotify = RemoveUsersWhereReminderDisabledForCustomer(GetUsersToNotify());
            foreach (var user in usersToNotify)
            {								

				var customer = _context.Customers.FirstOrDefault(c => c.Id == user.CustomerId);
				var distributor = _context.Distributors.FirstOrDefault(d => d.Id == customer.DistributorId);
				var sendSeparatePasswordResetEmail = distributor.SendSeparatePasswordResetEmail;
				var sendPasswordResetInstructions = user.IsPasswordGeneratedOnCreate.HasValue && user.IsPasswordGeneratedOnCreate.Value;

				Guid? resetToken = null;
				string passwordResetFormUrl = null;

				using (var scope = new TransactionScope())
				{					
					if (!sendSeparatePasswordResetEmail && sendPasswordResetInstructions)
					{

						passwordResetFormUrl = string.IsNullOrWhiteSpace(distributor.PasswordResetFormUrl) ? _configuration.GetPasswordResetFormUrl("LearningPortal") : distributor.PasswordResetFormUrl;

						if (string.IsNullOrEmpty(passwordResetFormUrl))
						{
							throw new ValidationException("Invalid source.");
						}

						resetToken = _passwordResetRequestFactory.CreatePasswordResetRequest(user).ResetToken;
					}


					var autoMailMessage = _userRegistrationNotificationFactory.CreateUserRegistrationNotificationMessage(user, passwordResetFormUrl, resetToken);
					_autoMailer.Send(autoMailMessage);
				}

				if (!resetToken.HasValue)
				{
                    SendPasswordResetRequest(user);
                }
            }
        }

        private void SendPasswordResetRequest(User user)
        {
            var email = GetEmailForUser(user);

            var customer = _context.Customers.FirstOrDefault(c => c.Id == user.CustomerId);
            var distributor = _context.Distributors.FirstOrDefault(d => d.Id == customer.DistributorId);

            var resetPasswordFormUrl = string.IsNullOrWhiteSpace(distributor.PasswordResetFormUrl) ? _configuration.GetPasswordResetFormUrl("LearningPortal"): distributor.PasswordResetFormUrl;

            if (string.IsNullOrEmpty(resetPasswordFormUrl))
            {
                throw new ValidationException("Invalid source.");
            }

            using (var scope = new TransactionScope())
            {
                var passwordResetRequest = _passwordResetRequestFactory.CreatePasswordResetRequest(user);
                var autoMailMessage = _passwordResetRequestMessageFactory.CreatePasswordResetMessage(passwordResetRequest.ResetToken, email, user.Id, resetPasswordFormUrl);

                var sendSuccess = _autoMailer.Send(autoMailMessage);

                if (!sendSuccess)
                {
                    throw new MailSendException("Failed to send mail");
                }
                scope.Complete();
            }
        }

        private string GetEmailForUser(User user)
        {
            using (var conn = new SqlConnection(_configuration.ConnectionString))
            {
                var sqlCommand = new SqlCommand(string.Format("SELECT m.Email FROM [aspnet_Users] u INNER JOIN [aspnet_Membership] m ON u.UserId = m.UserId WHERE LoweredUserName = '{0}'", user.MembershipUserId.ToLower()), conn);
                conn.Open();
                return Convert.ToString(sqlCommand.ExecuteScalar());
            }
        }

        private IEnumerable<User> GetUsersToNotify()
        {
            var tomorrow = DateTime.Today.AddDays(1);
            var dateLimit = DateTime.Today.AddDays(-3);

            return _context.Users
                .Where(u => u.UserRegistrationNotificationDate.HasValue && u.UserRegistrationNotificationDate.Value < tomorrow)
                .Where(u => u.UserNotificationType == UserNotificationTypes.Yes && (!u.ExpiryDate.HasValue || u.ExpiryDate.Value > tomorrow))
                // Avoid spam -  do not send registration notification if notification date are not within the last week
                .Where(u => u.UserRegistrationNotificationDate.Value >= dateLimit)
                .GroupJoin(_context.Mails, u => u.Id, m => m.UserId, (user, mail) => new { User = user, Mail = mail })
                .Where(x => x.Mail.All(m => !m.MessageType.HasValue || m.MessageType.Value != MessageType.UserRegistrationNotification))
                .Select(x => x.User);
        }

        private IEnumerable<User> RemoveUsersWhereReminderDisabledForCustomer(IEnumerable<User> users)
        {
            foreach (var user in users)
            {
                // Avoid spam -  do not send registration notification if notification date are not within the last 3 days
                if (user.UserRegistrationNotificationDate.HasValue && DateTime.UtcNow.Subtract(user.UserRegistrationNotificationDate.Value).Days <= 3)
                {
                    //Only send mail to users that are not deleted/expired.
                    if (!user.ExpiryDate.HasValue || DateTime.UtcNow.Subtract(user.ExpiryDate.Value).Days <= 0)
                    {
                        var customer =
                            _context.Customers.Include(c => c.AutomailDefinition).First(c => c.Id == user.CustomerId);
                        if (customer.AutomailDefinition != null &&
                            customer.AutomailDefinition.IsWelcomeNotification.HasValue &&
                            customer.AutomailDefinition.IsWelcomeNotification.Value)
                        {
                            yield return user;
                        }
                    }
                }
            }
        }
    }
}