﻿using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using Metier.Phoenix.Tasks.Configuration;

namespace Metier.Phoenix.Tasks.Tasks.CopyRcoCmsObjects
{
    public class CopyRcoCmsObjectsTask : ITask
    {
        private readonly IPhoenixTasksConfiguration _configuration;

        public CopyRcoCmsObjectsTask(IPhoenixTasksConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string Name { get { return "Phoenix-CopyRcoCmsObjects"; } }

        private DataTable GetRcos()
        {
            var ds = new DataSet();

            var source = new SqlConnection(_configuration.McsConnectionString);
            source.Open();
            
            var cmd = new SqlCommand("SELECT * FROM v_RCOs", source);
            var adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            
            source.Close(); 

            return ds.Tables[0];
        }

        public void Run()
        {
            var rcos = GetRcos();
            var destination = new SqlConnection(_configuration.ConnectionString);

            using (var transaction = new TransactionScope())
            {
                var cmd = new SqlCommand("SET XACT_ABORT ON", destination);
                destination.Open();
                cmd.ExecuteNonQuery();

                cmd = new SqlCommand("DELETE FROM Rcos", destination);
                cmd.ExecuteNonQuery();
                
                var bulkData = new SqlBulkCopy(destination);
                bulkData.DestinationTableName = "Rcos";
                bulkData.WriteToServer(rcos);

                bulkData.Close();
                destination.Close();

                transaction.Complete();
            }
        }
    }
}