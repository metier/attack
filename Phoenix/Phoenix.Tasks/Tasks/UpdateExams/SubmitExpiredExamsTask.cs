﻿using System;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Business;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Exams;
using NLog;

namespace Metier.Phoenix.Tasks.Tasks.UpdateExams
{
    public class SubmitExpiredExamsTask : ITask
    {
        private readonly MetierLmsContext _context;
        private readonly IExamGrader _examGrader;
        private readonly IParticipantBusiness _participantBusiness;
        private Logger _logger;

        public SubmitExpiredExamsTask(MetierLmsContext context, IExamGrader examGrader, IParticipantBusiness participantBusiness)
        {
            _context = context;
            _examGrader = examGrader;
            _participantBusiness = participantBusiness;
            _logger = LogManager.GetCurrentClassLogger();
        }

        public string Name { get { return "Phoenix-SubmitExpiredExams"; } }

        public void Run()
        {
            var unsubmittedExams = _context.Exams.Include("Questions.QuestionOptions").Include(e => e.Answers).Where(e => !e.IsSubmitted).ToList();

            foreach (var unsubmittedExam in unsubmittedExams)
            {
                var examDataObjects = _context.Exams.Where(e => e.Id == unsubmittedExam.Id)
                    .Join(_context.Participants, e => e.ParticipantId, p => p.Id, (e, p) => p)
                    .Join(_context.Activities, p => p.ActivityId, a => a.Id, (p, a) => new {Participant = p, Activity = a})
                    .Join(_context.CustomerArticles,
                        anon => anon.Activity.CustomerArticleId,
                        ca => ca.Id,
                        (anon, ca) => new {anon.Activity, anon.Participant, CustomerArticle = ca})
                    .Join(_context.Articles.Where(art => art.ArticleTypeId == 3),
                        anon => anon.CustomerArticle.ArticleCopyId,
                        a => a.Id,
                        (anon, a) => new {anon.Activity, anon.Participant, Article = a}).ToList();

                if (examDataObjects.Count == 0)
                {
                    _logger.Error($"Could not resolve participant, activities and articles for exam with ID {unsubmittedExam.Id}. Unable to verify if exam has expired.");
                    continue;
                }

                var participant = examDataObjects[0].Participant;
                var activity = examDataObjects[0].Activity;
                var examArticle = examDataObjects[0].Article as ExamArticle;

				//JvT: Not able to get this to work in Linq above. Hackish fix below:
				activity.ActivityPeriods = _context.ActivityPeriods.Where(ap => ap.ActivityId == activity.Id).ToList();

                if (HasExpired(unsubmittedExam, participant, activity, examArticle))
                {
                    SubmitExam(unsubmittedExam, participant, examArticle);
                }
            }
        }

        internal bool HasExpired(Exam unsubmittedExam, Participant participant, Activity activity, ExamArticle examArticle)
        {
            var dueDate = _participantBusiness.GetDueDate(participant, activity, examArticle, unsubmittedExam);

            if (!dueDate.HasValue)
            {
                _logger.Error($"Could not determine due date for exam with ID {unsubmittedExam.Id} and thus unable to expire exam");
                return false;
            }

            return dueDate.Value < (DateTime.UtcNow - TimeSpan.FromMinutes(5));
        }

        private void SubmitExam(Exam exam, Participant participant, ExamArticle article)
        {
            exam.IsSubmitted = true;
            exam.SubmittedAt = DateTime.UtcNow;
            participant.StatusCodeValue = ParticipantStatusCodes.Completed;
            _examGrader.SetGrade(exam, article, participant);
            _context.SaveChanges();
        }
    }
}