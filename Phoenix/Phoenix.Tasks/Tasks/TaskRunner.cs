﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Core.Utilities.Logging;
using NLog;

namespace Metier.Phoenix.Tasks.Tasks
{
    public class TaskRunner : ITaskRunner
    {
        private readonly IEnumerable<ITask> _tasks;
        private readonly Logger _logger;

        public TaskRunner(IEnumerable<ITask> tasks)
        {
            _logger = LogManager.GetCurrentClassLogger();
            _tasks = tasks;
            _logger.Info("TaskRunner initialized with following tasks: {0}", string.Join(",", _tasks.Select(t => t.Name).ToList()));
        }

        public void RunTask(string name)
        {
            try
            {
                var task = _tasks.FirstOrDefault(t => t.Name.ToLower() == name.ToLower());
                if (task != null)
                {
                    _logger.Info("Running task: {0}", task.Name);
                    using (new TimingLogScope(_logger, string.Format("Completed task: {0}", task.Name), LogLevel.Info))
                    {
                        task.Run();
                    }
                    
                }
            }
            catch (Exception e)
            {
                _logger.ErrorException(string.Format("An error occurred while running task {0}", name), e);
            }
        }
    }
}