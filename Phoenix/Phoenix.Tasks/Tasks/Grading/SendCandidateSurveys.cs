﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Metier.Phoenix.Core.Business.ChangeLog;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Factories.GradingLists;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Templates.CandidateSurvey;

namespace Metier.Phoenix.Tasks.Tasks.Grading
{
    public class SendCandidateSurveys : ITask
    {
        private readonly ICandidateSurveyFactory _candidateSurveyFactory;
        private readonly ICandidateSurveyMessageFactory _candidateSurveyMessageFactory;
        private readonly IAutoMailer _autoMailer;
        private readonly IChangeLogProvider _changeLogProvider;
        private readonly MetierLmsContext _context;

        public SendCandidateSurveys(ICandidateSurveyFactory candidateSurveyFactory, 
            ICandidateSurveyMessageFactory candidateSurveyMessageFactory, 
            IAutoMailer autoMailer,
            IChangeLogProvider changeLogProvider,
            MetierLmsContext context)
        {
            _candidateSurveyFactory = candidateSurveyFactory;
            _candidateSurveyMessageFactory = candidateSurveyMessageFactory;
            _autoMailer = autoMailer;
            _changeLogProvider = changeLogProvider;
            _context = context;
        }

        public string Name { get { return "Phoenix-SendCandidateSurveys"; } }

        public void Run()
        {
            var educationalPartners = GetEducationalPartners().ToList();

            foreach (var educationalPartner in educationalPartners)
            {
                using (var scope = new TransactionScope())
                {
                    var participants = GetParticipantsForCandidateSurvey(educationalPartner).ToList();
                    if (participants.Any())
                    {
                        RegisterDateForCandidateSurvey(participants);
                        var sharedFile = _candidateSurveyFactory.Create(educationalPartner, participants);
                        if (sharedFile == null)
                        {
                            continue;
                        }
                        var autoMail = _candidateSurveyMessageFactory.CreateCandidateSurveyMessage(sharedFile.ShareGuid, educationalPartner);
                        _autoMailer.Send(autoMail);
                        _context.SaveChanges();

                        WriteToChangeLog(educationalPartner, participants);
                    }
                    scope.Complete();
                }
            }
        }

        private void WriteToChangeLog(Resource educationalPartner, IEnumerable<Participant> participants)
        {
            foreach (var participant in participants)
            {
                _changeLogProvider.Write(participant.Id, ChangeLogEntityTypes.Participant, ChangeLogTypes.CandidateSurveySent, "Candidate survey sent to {0}", educationalPartner.Name);
            }
        }

        private void RegisterDateForCandidateSurvey(IEnumerable<Participant> participants)
        {
            var gradingListDate = DateTime.UtcNow;
            foreach (var participant in participants)
            {
                participant.GradingListDate = gradingListDate;
            }
        }

        private IEnumerable<Participant> GetParticipantsForCandidateSurvey(Resource educationalPartner)
        {
            return _context.Articles.OfType<ExamArticle>()
                .Where(a => !a.IsDeleted)
                .OfType<Article>()
                .Union(
                    _context.Articles.OfType<CaseExamArticle>()
                        .Where(a => !a.IsDeleted))
                .Join(_context.CustomerArticles, ea => ea.Id, ca => ca.ArticleCopyId, (ea, ca) => ca)
                .Where(ca => !ca.IsDeleted)
                .Join(_context.Activities, ca => ca.Id, a => a.CustomerArticleId, (ca, a) => a)
                .Where(
                    a =>
                        a.EducationalPartnerResourceId.HasValue &&
                        a.EducationalPartnerResourceId.Value == educationalPartner.Id && !a.IsDeleted &&
                        !string.IsNullOrEmpty(a.ECTS))
                .Join(_context.Participants, a => a.Id, p => p.ActivityId, (a, p) => p)
                .Where(
                    p =>
                        !string.IsNullOrEmpty(p.ExamGrade) && 
                        p.GradingListDate == null && 
                        !p.IsDeleted &&
                        p.Created.Year > 2014 &&                        //Restricting participants to those that are created after Phoenix go-live 
                        p.StatusCodeValue == ParticipantStatusCodes.Completed)
                .Join(_context.Users, p => p.UserId, u => u.Id, (p, u) => new {User = u, Participant = p})
                .Where(x => x.User.IsAllowUserEcts.HasValue && x.User.IsAllowUserEcts.Value)
                .Select(x => x.Participant);
        }

        private IEnumerable<Resource> GetEducationalPartners()
        {
            return _context.Resources.WithAll().Where(r => r.ResourceType.Name == "Partners");
        }
    }
}