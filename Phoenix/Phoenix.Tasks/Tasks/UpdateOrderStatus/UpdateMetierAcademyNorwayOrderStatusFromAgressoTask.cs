﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Tasks.Services.Agresso;
using Metier.Phoenix.Tasks.Services.Agresso.Configuration.Norway;
using NLog;

namespace Metier.Phoenix.Tasks.Tasks.UpdateOrderStatus
{
    public class UpdateMetierAcademyNorwayOrderStatusFromAgressoTask : ITask
    {
        private readonly MetierLmsContext _context;
        private readonly IAgressoService _agressoService;
        private readonly IAgressoNorwayConfiguration _agressoNorwayConfiguration;
        private readonly Logger _logger;

        public UpdateMetierAcademyNorwayOrderStatusFromAgressoTask(MetierLmsContext context, IAgressoService agressoService, IAgressoNorwayConfiguration agressoNorwayConfiguration)
        {
            _context = context;
            _agressoService = agressoService;
            _agressoNorwayConfiguration = agressoNorwayConfiguration;
            _logger = LogManager.GetCurrentClassLogger();
        }

        public string Name { get { return "Phoenix-UpdateMetierAcademyNorwayOrderStatusAgresso"; } }

        public void Run()
        {
            var orders = GetOrdersWithTemporaryStatus().ToList();

            _logger.Debug("Updating {0} Metier Academy Norway orders with temporary status", orders.Count());

            var numberOfUpdatedOrders = UpdateOrderStatusFromAgresso(orders);

            _logger.Info("Changed status for {0} Metier Academy Norway orders.", numberOfUpdatedOrders);
        }

        private IEnumerable<Order> GetOrdersWithTemporaryStatus()
        {
            return _context.Orders.Where(o => o.Status == OrderStatus.Transferred || o.Status == OrderStatus.NotInvoiced)
                           .Join(_context.Customers, o => o.CustomerId, c => c.Id, (o, c) => new { Order = o, Customer = c })
                           .Where(x => x.Customer.DistributorId == 1194)
                           .Select(x => x.Order);
        }

        private int UpdateOrderStatusFromAgresso(IEnumerable<Order> orders)
        {
            var numberOfUpdatedOrders = 0;
            foreach (var order in orders)
            {
				try
				{
					_logger.Debug("Checking status for order number {0}", order.OrderNumber);

					var currentStatus = order.Status;
					var agressoValuesForOrder = _agressoService.GetCurrentValuesInAgresso(_agressoNorwayConfiguration, order);

					_logger.Debug("Retrieved updated status for order number {0}. (Previous: {1}, Updated: {2})", order.OrderNumber, order.Status, agressoValuesForOrder.Status);

					if (agressoValuesForOrder.Status != currentStatus)
					{
						_logger.Debug("Status changed for order number {0}. Updating status.", order.OrderNumber);

						numberOfUpdatedOrders++;
						order.LastModified = DateTime.UtcNow;
						order.Status = agressoValuesForOrder.Status;
						order.InvoiceNumber = agressoValuesForOrder.InvoiceNumber;
					}
				}
				catch (Exception e)
				{
					_logger.Error($"Order number {order.OrderNumber} failed to update", e);
				}
            }
            _context.SaveChanges();

            return numberOfUpdatedOrders;
        }
    }
}