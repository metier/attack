﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Tasks.Services.Agresso;
using Metier.Phoenix.Tasks.Services.Agresso.Configuration.Sweden;
using NLog;

namespace Metier.Phoenix.Tasks.Tasks.UpdateOrderStatus
{
    public class UpdateMetierAcademySwedenOrderStatusFromAgressoTask : ITask
    {
        private readonly MetierLmsContext _context;
        private readonly IAgressoService _agressoService;
        private readonly IAgressoSwedenConfiguration _agressoSwedenConfiguration;
        private readonly Logger _logger;

        public UpdateMetierAcademySwedenOrderStatusFromAgressoTask(MetierLmsContext context, IAgressoService agressoService, IAgressoSwedenConfiguration agressoSwedenConfiguration)
        {
            _context = context;
            _agressoService = agressoService;
            _agressoSwedenConfiguration = agressoSwedenConfiguration;
            _logger = LogManager.GetCurrentClassLogger();
        }

        public string Name { get { return "Phoenix-UpdateMetierAcademySwedenOrderStatusAgresso"; } }

        public void Run()
        {
            var orders = GetOrdersWithTemporaryStatus().ToList();

            _logger.Debug("Updating {0} Metier Academy Sweden orders with temporary status", orders.Count());

            var numberOfUpdatedOrders = UpdateOrderStatusFromAgresso(orders);

            _logger.Info("Changed status for {0} Metier Academy Sweden orders.", numberOfUpdatedOrders);
        }

        private IEnumerable<Order> GetOrdersWithTemporaryStatus()
        {
            return _context.Orders.Where(o => o.Status == OrderStatus.Transferred || o.Status == OrderStatus.NotInvoiced)
                           .Join(_context.Customers, o => o.CustomerId, c => c.Id, (o, c) => new { Order = o, Customer = c })
                           .Where(x => x.Customer.DistributorId == 1195)
                           .Select(x => x.Order);
        }

        private int UpdateOrderStatusFromAgresso(IEnumerable<Order> orders)
        {
            var numberOfUpdatedOrders = 0;
            foreach (var order in orders)
            {
                _logger.Debug("Checking status for order number {0}", order.OrderNumber);

                var currentStatus = order.Status;
                var agressoValuesForOrder = _agressoService.GetCurrentValuesInAgresso(_agressoSwedenConfiguration, order);

                _logger.Debug("Retrieved updated status for order number {0}. (Previous: {1}, Updated: {2})", order.OrderNumber, order.Status, agressoValuesForOrder.Status);

                if (agressoValuesForOrder.Status != currentStatus)
                {
                    _logger.Debug("Status changed for order number {0}. Updating status.", order.OrderNumber);

                    numberOfUpdatedOrders++;
                    order.LastModified = DateTime.UtcNow;
                    order.Status = agressoValuesForOrder.Status;
                    order.InvoiceNumber = agressoValuesForOrder.InvoiceNumber;
                }
            }
            _context.SaveChanges();

            return numberOfUpdatedOrders;
        }
    }
}