﻿using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Tasks.Services.Agresso
{
    public  class AgressoInvoiceQueryData
    {
        public int OrderNo { get; set; }
        public OrderStatus Status { get; set; }
        public int? InvoiceNo { get; set; }
    }
}