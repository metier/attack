﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Utilities.Logging;
using Metier.Phoenix.Tasks.Agresso.CustomerService;
using Metier.Phoenix.Tasks.Agresso.QueryEngine;
using Metier.Phoenix.Tasks.Services.Agresso.Configuration;
using NLog;
using WSCredentials = Metier.Phoenix.Tasks.Agresso.QueryEngine.WSCredentials;

namespace Metier.Phoenix.Tasks.Services.Agresso
{
    public class AgressoService : IAgressoService
    {
        private readonly QueryEngineV201101Soap _agressoQueryService;
        private readonly CustomerSoap _customerService;
        private readonly Logger _logger;

        public AgressoService(QueryEngineV201101Soap agressoQueryService, CustomerSoap customerService)
        {
            _agressoQueryService = agressoQueryService;
            _customerService = customerService;
            _logger = LogManager.GetCurrentClassLogger();
        }

        public IEnumerable<ExternalCustomer> GetCustomers(IAgressoConfiguration configuration)
        {
            var credentials = GetCustomerServiceCredentials(configuration);
            var customerObject = new CustomerObject { CustomerName = "%", Company = configuration.Client };
            GetCustomersResponse result;
            using (new TimingLogScope(_logger, string.Format("Query for all customers from Agresso client {0} completed", configuration.Client)))
            {
                result = _customerService.GetCustomers(new GetCustomersRequest(new GetCustomersRequestBody(customerObject, false, credentials)));
            }
            return result.Body.GetCustomersResult.CustomerTypeList.Select(CreateExternalCustomer).ToList();
        }

        public AgressoOrderData GetCurrentValuesInAgresso(IAgressoConfiguration configuration, Order order)
        {
            if (order == null || !order.OrderNumber.HasValue)
            {
                return null;
            }

            TemplateResultAsXML agressoInvoiceQueryResponse;
            using (new TimingLogScope(_logger, string.Format("Query for order status from Agresso client {0} completed", configuration.Client)))
            {
                agressoInvoiceQueryResponse = _agressoQueryService.GetTemplateResultAsXML(new InputForTemplateResult
                {
                    TemplateId = configuration.InvoiceQueryTemplateId,
                    SearchCriteriaPropertiesList = new[]{new SearchCriteriaProperties
                {
                    ColumnName = "order_id",
                    FromValue = order.OrderNumber.Value.ToString(CultureInfo.InvariantCulture),
                    RestrictionType = "=",
                    DataType = 10
                }}
                }, GetCredentials(configuration));
            }

            var matchingAgressoOrder = ParseInvoiceQueryResponse(agressoInvoiceQueryResponse)
                                       .FirstOrDefault(iq => iq.OrderNo == order.OrderNumber.Value);

            var result = new AgressoOrderData
            {
                Status = order.Status
            };

            if (matchingAgressoOrder != null)
            {
                result.Status = matchingAgressoOrder.Status;
                result.InvoiceNumber = matchingAgressoOrder.InvoiceNo;
            }
            return result;
        }

        private ExternalCustomer CreateExternalCustomer(CustomerObject co)
        {
            var address = co.AddressList.FirstOrDefault() ?? new AddressUnitType();

            return new ExternalCustomer
            {
                Id = co.CustomerID,
                Name = co.CustomerName,
                CompanyRegistrationNumber = TrimCompanyRegistrationNumber(co.CompanyRegistrationNumber),
                AccountNumber = string.Format("{0}-{1}", co.CustomerGroupID, co.CustomerID),
                AddressStreet1 = address.Address,
                ZipCode = address.ZipCode,
                City = address.Place,
                Country = address.CountryCode
            };
        }

        private string TrimCompanyRegistrationNumber(string companyRegistrationNumber)
        {
            if(companyRegistrationNumber == null || companyRegistrationNumber.Replace(" ", "").Length == 0)
                return null;
            
            return companyRegistrationNumber.Replace(" ", "");
        }

        private IEnumerable<AgressoInvoiceQueryData> ParseInvoiceQueryResponse(TemplateResultAsXML result)
        {
            var invoiceSearchResult = XDocument.Parse(result.TemplateResult);
            return invoiceSearchResult.Descendants("AgressoQE").Where(aqe => aqe.Descendants("_section").First().Value == "D").Select(ConvertToAgressoInvoiceQueryData).ToList();
        }

        private AgressoInvoiceQueryData ConvertToAgressoInvoiceQueryData(XElement aqe)
        {
            return new AgressoInvoiceQueryData
            {
                Status = MapToOrderStatus(aqe.Descendants("status").First().Value),
                OrderNo = !string.IsNullOrEmpty(aqe.Descendants("order_id").First().Value) ? Convert.ToInt32(aqe.Descendants("order_id").First().Value) : 0,
                InvoiceNo = !string.IsNullOrEmpty(aqe.Descendants("voucher_no").First().Value) ? Convert.ToInt32(aqe.Descendants("voucher_no").First().Value) : 0,
            };
        }

        private OrderStatus MapToOrderStatus(string agressoStatusValue)
        {
            switch (agressoStatusValue)
            {
                case "N":
                    return OrderStatus.NotInvoiced;
                case "F":
                    return OrderStatus.Invoiced;
                case "P":
                    return OrderStatus.Rejected;
                default:
                    return OrderStatus.Transferred;
            }
        }

        private Phoenix.Tasks.Agresso.CustomerService.WSCredentials GetCustomerServiceCredentials(IAgressoConfiguration configuration)
        {
            return new Phoenix.Tasks.Agresso.CustomerService.WSCredentials
            {
                Username = configuration.Username,
                Password = configuration.Password,
                Client = configuration.Client
            };
        }

        private WSCredentials GetCredentials(IAgressoConfiguration configuration)
        {
            var credentials = new WSCredentials
            {
                Username = configuration.Username,
                Password = configuration.Password,
                Client = configuration.Client
            };
            return credentials;
        }
    }
}