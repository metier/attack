﻿using System;
using System.Configuration;

namespace Metier.Phoenix.Tasks.Services.Agresso.Configuration.Sweden
{
    public class AgressoSwedenConfiguration : IAgressoSwedenConfiguration
    {
        public string Username
        {
            get { return ConfigurationManager.AppSettings["Sweden.Agresso.Username"]; }
        }

        public string Password
        {
            get { return ConfigurationManager.AppSettings["Sweden.Agresso.Password"]; }
        }

        public string Client
        {
            get { return ConfigurationManager.AppSettings["Sweden.Agresso.Client"]; }
        }

        public int InvoiceQueryTemplateId
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["Sweden.Agresso.InvoiceQueryTemplateId"]); }
        }        
    }
}