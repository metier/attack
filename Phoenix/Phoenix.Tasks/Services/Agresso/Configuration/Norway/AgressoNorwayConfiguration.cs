﻿using System;
using System.Configuration;

namespace Metier.Phoenix.Tasks.Services.Agresso.Configuration.Norway
{
    public class AgressoNorwayConfiguration : IAgressoNorwayConfiguration
    {
        public string Username
        {
            get { return ConfigurationManager.AppSettings["Norway.Agresso.Username"]; }
        }

        public string Password
        {
            get { return ConfigurationManager.AppSettings["Norway.Agresso.Password"]; }
        }

        public string Client
        {
            get { return ConfigurationManager.AppSettings["Norway.Agresso.Client"]; }
        }

        public int InvoiceQueryTemplateId
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["Norway.Agresso.InvoiceQueryTemplateId"]); }
        }
    }
}