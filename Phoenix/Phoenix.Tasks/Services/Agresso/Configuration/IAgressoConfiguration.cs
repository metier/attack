﻿namespace Metier.Phoenix.Tasks.Services.Agresso.Configuration
{
    public interface IAgressoConfiguration
    {
        string Username { get; }
        string Password { get; }
        string Client { get; }
        int InvoiceQueryTemplateId { get; }
    }
}