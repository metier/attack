﻿using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Tasks.Services.Agresso
{
    public class AgressoOrderData
    {
        public OrderStatus Status { get; set; }
        public int? InvoiceNumber { get; set; }
    }
}