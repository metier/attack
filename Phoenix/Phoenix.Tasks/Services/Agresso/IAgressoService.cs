﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Tasks.Services.Agresso.Configuration;

namespace Metier.Phoenix.Tasks.Services.Agresso
{
    public interface IAgressoService
    {
        IEnumerable<ExternalCustomer> GetCustomers(IAgressoConfiguration configuration);
        AgressoOrderData GetCurrentValuesInAgresso(IAgressoConfiguration configuration, Order order);
    }
}