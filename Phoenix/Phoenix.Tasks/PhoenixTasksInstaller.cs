﻿using System.Linq;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Tasks.Tasks;

namespace Metier.Phoenix.Tasks
{
    public class PhoenixTasksInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<MetierLmsContext>().DependsOn(Dependency.OnValue("connectionString", "Name=MetierLmsContext")).LifestyleTransient()
                , Classes.FromThisAssembly().BasedOn<ITask>().WithService.FromInterface().LifestyleTransient()
                , Classes.FromThisAssembly()
                         .Where(a => a.GetInterfaces().All(i => i != typeof(ITask)) &&
                                     a != typeof(MetierLmsContext))
                         .WithService.DefaultInterfaces().LifestyleTransient()
                );
        }
    }
}