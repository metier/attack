﻿using System;
using Metier.Phoenix.Api.Agresso.CustomerService;
using Metier.Phoenix.Api.Services.Agresso.Configuration.Sweden;
using Metier.Phoenix.Api.Services.Agresso.Customers;
using Metier.Phoenix.Api.Services.ExternalCustomers;
using Metier.Phoenix.Api.Services.ExternalCustomers.Providers.Agresso.Sweden;

namespace Phoenix.Api.IntegrationTests.Api.Agresso
{
    public class FakeAgressoSwedenCustomerProvider : AgressoSwedenCustomerProvider
    {
        public FakeAgressoSwedenCustomerProvider(IAgressoSwedenConfiguration configuration, CustomerSoap agressoCustomerService, IExternalCustomerFactory externalCustomerFactory) : base(configuration, agressoCustomerService, externalCustomerFactory)
        {
        }

        public override string Create(ExternalCustomerCreateRequest createRequest)
        {
            return new Guid().ToString();
        }
    }
}