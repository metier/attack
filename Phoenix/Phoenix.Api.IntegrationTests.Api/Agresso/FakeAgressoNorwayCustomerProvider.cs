﻿using System;
using Metier.Phoenix.Api.Agresso.CustomerService;
using Metier.Phoenix.Api.Services.Agresso.Configuration.Norway;
using Metier.Phoenix.Api.Services.Agresso.Customers;
using Metier.Phoenix.Api.Services.ExternalCustomers;
using Metier.Phoenix.Api.Services.ExternalCustomers.Providers.Agresso.Norway;

namespace Phoenix.Api.IntegrationTests.Api.Agresso
{
    public class FakeAgressoNorwayCustomerProvider : AgressoNorwayCustomerProvider
    {
        public FakeAgressoNorwayCustomerProvider(IAgressoNorwayConfiguration configuration, CustomerSoap agressoCustomerService, IExternalCustomerFactory externalCustomerFactory) : base(configuration, agressoCustomerService, externalCustomerFactory)
        {
        }

        public override string Create(ExternalCustomerCreateRequest createRequest)
        {
            return new Guid().ToString();
        }
    }
}