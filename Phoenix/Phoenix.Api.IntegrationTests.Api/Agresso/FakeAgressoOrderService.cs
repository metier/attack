﻿using System;
using System.Collections.Generic;
using Metier.Phoenix.Api.Services.Agresso.Invoicing;
using Metier.Phoenix.Api.Services.Invoices;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;

namespace Phoenix.Api.IntegrationTests.Api.Agresso
{
    public class FakeAgressoOrderService : IAgressoOrderService
    {
        private static int _seq = 1;

        public OrderRequest PrepareOrderRequest(OrderRequest orderRequest)
        {
            orderRequest.Order.OrderNumber = ++_seq;
            orderRequest.Order.Status = OrderStatus.Transferred;
            orderRequest.Order.OrderDate = DateTime.UtcNow;

            return orderRequest;
        }
 

        public Order Create(OrderRequest orderRequest)
        {
            return orderRequest.Order;
        }

        public IEnumerable<string> GetWorkOrders(WorkOrderRequest workOrderRequest)
        {
            return new List<string> {"Fake work order"};
        }

        public Order Update(OrderRequest orderRequest)
        {
            return orderRequest.Order;
        }
    }
}