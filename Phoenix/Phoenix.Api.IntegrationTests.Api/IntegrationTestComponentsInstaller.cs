using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Metier.Phoenix.Api.Configuration;
using Metier.Phoenix.Api.Services.Agresso.Invoicing;
using Metier.Phoenix.Api.Services.ExternalCustomers.Providers.Agresso.Norway;
using Metier.Phoenix.Api.Services.ExternalCustomers.Providers.Agresso.Sweden;
using Metier.Phoenix.Mail;
using Phoenix.Api.IntegrationTests.Api.Agresso;

namespace Phoenix.Api.IntegrationTests.Api
{
    public class IntegrationTestComponentsInstaller : ComponentsInstaller
    {
        public override void Install(IWindsorContainer container, IConfigurationStore store)
        {
            base.Install(container, store);
            
            container.Register(
                    Component.For<IAgressoOrderService>().ImplementedBy<FakeAgressoOrderService>().LifestyleTransient()
                    , Component.For<IAgressoNorwayCustomerProvider>().ImplementedBy<FakeAgressoNorwayCustomerProvider>().LifestyleTransient()
                    , Component.For<IAgressoSwedenCustomerProvider>().ImplementedBy<FakeAgressoSwedenCustomerProvider>().LifestyleTransient()
                    , Component.For<IAutoMailer>().ImplementedBy<FakeAutoMailer>().LifestyleTransient()
                );
        }
    }
}