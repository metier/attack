using Metier.Phoenix.Mail;
using System;

namespace Phoenix.Api.IntegrationTests.Api
{
    public class FakeAutoMailer : IAutoMailer
    {
        public bool Send(AutoMailMessage autoMailMessage)
        {
            return true;
        }

        public bool Send(int mailId)
        {
            return true;
        }

        public AutoMailMessage Get(int mailId)
        {
            return new AutoMailMessage();
        }

        public bool SendInvitation(AutoMailMessage autoMailMessage, DateTime start, DateTime end, string location)
        {
            throw new System.NotImplementedException();
        }
    }
}