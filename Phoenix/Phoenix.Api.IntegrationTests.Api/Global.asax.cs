﻿using Metier.Phoenix.Api;

namespace Phoenix.Api.IntegrationTests.Api
{
    public class WebApiApplication : MvcApplication
    {
        public WebApiApplication() : base(new IntegrationTestComponentsInstaller())
        {
        }
    }
}
