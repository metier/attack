﻿using System;
using System.Net;
using Metier.Phoenix.Api.Security;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Api;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Security
{
    [TestFixture]
    public class AuthorizationTests : ApiTestBase
    {
        [Test]
        public void GetDistributors_Unauthroized_ShouldReturnUnauthorized()
        {
            var request = CreateWebRequest("distributors", "GET");
            var response = GetWebResponse(request);
            
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public void GetDistributors_AsAuthorizedUser_ShouldReturnOk()
        {
            LogOn(_sqlInitializer.IntegrationTestPhoenixUserName);

            var request = CreateWebRequest("distributors", "GET");
            var response = GetWebResponse(request);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
        
        [Test]
        public void GetDistributors_WithValidAuthToken_ShouldReturnOk()
        {
            const string username = "testuserwithauthtoken";
            var authToken = SqlWriteHelper.InsertMembershipTokenUser(username);
            _sqlInitializer.CreatedUsernames.Add(username);

            var request = CreateWebRequest("distributors", "GET");
            request.Headers.Add("PhoenixAuthToken", authToken);
            var response = GetWebResponse(request);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public void GetDistributors_WithInvalidAuthToken_ShouldReturnUnauthorized()
        {
            const string authToken = "whatever"; // Should be invalid

            var request = CreateWebRequest("distributors", "GET");
            request.Headers.Add("PhoenixAuthToken", authToken);
            var response = GetWebResponse(request);

            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
    }
}