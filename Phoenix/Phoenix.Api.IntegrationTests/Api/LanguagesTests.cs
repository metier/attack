﻿using System.Collections.Generic;
using System.Net;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data.Entities;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class LanguagesTests : AuthorizedApiTestBase
    {
        // GET: languages
        [Test]
        public void GetLanguages_MustReturnListOfLanguages()
        {
            var request = CreateWebRequest("languages", "GET");
            using (var response = GetWebResponse(request))
            {
                var responseValue = GetResponseString(response);
                var result = JsonConvert.DeserializeObject<List<Language>>(responseValue);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.IsNotNull(result);
                Assert.IsTrue(result.Count > 0);
            }
        }
    }
}