﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class ArticlesTests : AuthorizedApiTestBase
    {
        internal class SerializableArticle : Article
        {
            
        }
        
        [Test]
        public void GetArticle_ArticlePathShouldBeEqualInQueryAndForSingleArticle()
        {
            var articleId = _sqlInitializer.CreatedArticleIds.First();
            var singleRequest = CreateWebRequest("articles/" + articleId, "GET");
            using (var singleResponse = GetWebResponse(singleRequest))
            {
                var singleArticle = GetResponseContent<SerializableArticle>(singleResponse);

                Assert.IsNotNull(singleArticle);
                Assert.IsNotNullOrEmpty(singleArticle.ArticlePath);

                var queryRequest = CreateWebRequest("articles?limit=999999&query=" + singleArticle.ArticlePath, "GET");
                using (var queryResponse = GetWebResponse(queryRequest))
                {
                    var articles = GetResponseContent<PartialList<SearchableArticle>>(queryResponse);
                    var queryArticle = articles.Items.FirstOrDefault(a => a.Id == singleArticle.Id);

                    Assert.IsNotNull(queryArticle);
                    Assert.IsNotNullOrEmpty(queryArticle.Path);

                    Assert.AreEqual(singleArticle.ArticlePath, queryArticle.Path);
                }
            }
        }

        [Test]
        public void GetArticle_GivenValidId_ShouldReturnArticle()
        {
            var articleId = _sqlInitializer.CreatedArticleIds.First();
            var request = CreateWebRequest("articles/" + articleId, "GET");
            using (var response = GetWebResponse(request))
            {
                var article = GetResponseContent<SerializableArticle>(response);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(articleId, article.Id);
            }
        }
        
        [Test]
        public void GetArticle_GivenInvalidId_ShouldReturnNotFound()
        {
            const int articleId = int.MaxValue;
            var request = CreateWebRequest("articles/" + articleId, "GET");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
            }
        }
 
        [Test]
        public void GetArticles_GivenProductId_ShouldArticles()
        {
            var productId = _sqlInitializer.ProductIdWithDescriptionAndArticle;
            var request = CreateWebRequest("articles?productId=" + productId, "GET");
            using (var response = GetWebResponse(request))
            {
                var articles = GetResponseContent<PartialList<SearchableArticle>>(response);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.GreaterOrEqual(articles.Items.Count, 1);
            }
        }
        
        [Test]
        public void CreateArticle_GivenValidArticle_ShouldCreateArticle()
        {
            var validArticle = CreateValidArticle();
            var request = CreateWebRequest("articles/", "POST", validArticle);
            using (var response = GetWebResponse(request))
            {
                var article = GetResponseContent<SerializableArticle>(response);

                _sqlInitializer.CreatedArticleIds.Add(article.Id);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            }
        }
        
        [Test]
        public void CreateArticle_GivenValidElearningArticle_ShouldCreateArticle()
        {
            var validArticle = CreateValidElearningArticle();
            var request = CreateWebRequest("articles/", "POST", validArticle);
            using (var response = GetWebResponse(request))
            {
                var article = GetResponseContent<ElearningArticle>(response);

                _sqlInitializer.CreatedArticleIds.Add(article.Id);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            }
        }

        [Test]
        public void CreateArticle_GivenValidClassroomArticle_ShouldCreateArticle()
        {
            var validArticle = CreateValidClassroomArticle();
            var request = CreateWebRequest("articles/", "POST", validArticle);
            using (var response = GetWebResponse(request))
            {
                var article = GetResponseContent<ClassroomArticle>(response);

                _sqlInitializer.CreatedArticleIds.Add(article.Id);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            }
        }

        [Test]
        public void CreateArticle_GivenValidMultipleChoiceArticle_ShouldCreateArticle()
        {
            var validArticle = CreateValidMultipleChoiceArticle();
            var request = CreateWebRequest("articles/", "POST", validArticle);
            using (var response = GetWebResponse(request))
            {
                var article = GetResponseContent<ExamArticle>(response);

                _sqlInitializer.CreatedArticleIds.Add(article.Id);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            }
        }

        [Test]
        public void CreateArticle_GivenValidCertificationArticle_ShouldCreateArticle()
        {
            var validArticle = CreateValidCertificationArticle();
            var request = CreateWebRequest("articles/", "POST", validArticle);
            using (var response = GetWebResponse(request))
            {
                var article = GetResponseContent<ExamArticle>(response);

                _sqlInitializer.CreatedArticleIds.Add(article.Id);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            }
        }

        [Test]
        public void CreateArticle_GivenValidCaseExamArticle_ShouldCreateArticle()
        {
            var validArticle = CreateValidCaseExamArticle();
            var request = CreateWebRequest("articles/", "POST", validArticle);
            using (var response = GetWebResponse(request))
            {
                var article = GetResponseContent<CaseExamArticle>(response);

                _sqlInitializer.CreatedArticleIds.Add(article.Id);
                _sqlInitializer.CreatedAttachmentIds.Add(article.CaseAttachmentId.Value);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            }
        }

        [Test]
        public void CreateArticle_GivenValidProjectAssignmentArticle_ShouldCreateArticle()
        {
            var validArticle = CreateValidProjectAssignmentArticle();
            var request = CreateWebRequest("articles/", "POST", validArticle);
            using (var response = GetWebResponse(request))
            {
                var article = GetResponseContent<CaseExamArticle>(response);

                _sqlInitializer.CreatedArticleIds.Add(article.Id);
                _sqlInitializer.CreatedAttachmentIds.Add(article.CaseAttachmentId.Value);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            }
        }
        
        [Test]
        public void CreateArticle_GivenValidArticleWithMultipleAttachments_ShouldCreateArticle()
        {
            var validArticle = CreateValidArticle();
            validArticle.Attachments = new List<Attachment>
            {
                new Attachment { Title = "file1", FileId = _sqlInitializer.CreatedFileIds.First() },
                new Attachment { Title = "file2", FileId = _sqlInitializer.CreatedFileIds.First()},
            };

            var request = CreateWebRequest("articles/", "POST", validArticle);
            using (var response = GetWebResponse(request))
            {
                var article = GetResponseContent<SerializableArticle>(response);

                _sqlInitializer.CreatedArticleIds.Add(article.Id);
                _sqlInitializer.CreatedAttachmentIds.AddRange(article.Attachments.Select(a => a.Id));
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
                Assert.AreEqual(2, article.Attachments.Count);
            }
        }

        [Test]
        public void UpdateArticle_ShouldUpdateArticle()
        {
            var article = GetArticle<SerializableArticle>(_sqlInitializer.CreatedArticleIds.First());
            article.Title = "New Title";

            var request = CreateWebRequest("articles/" + article.Id, "PUT", article);
            using (var response = GetWebResponse(request))
            {
                var updated = GetResponseContent<SerializableArticle>(response);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual("New Title", updated.Title);
            }
        }

        [Test]
        public void UpdateElearningArticle_ShouldUpdateElearningArticle()
        {
            var article = GetArticle<ElearningArticle>(_sqlInitializer.CreatedArticleIds.First());
            article.PDU = "New PDU";

            var request = CreateWebRequest("articles/" + article.Id, "PUT", article);
            using (var response = GetWebResponse(request))
            {
                var updated = GetResponseContent<ElearningArticle>(response);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual("New PDU", updated.PDU);
            }
        }

        [Test]
        public void UpdateClassroomArticle_ShouldUpdateClassroomArticle()
        {
            var article = GetArticle<ClassroomArticle>(_sqlInitializer.CreatedArticleIds.ElementAt(1));
            article.Title = "New Title";

            var request = CreateWebRequest("articles/" + article.Id, "PUT", article);
            using (var response = GetWebResponse(request))
            {
                var updated = GetResponseContent<ClassroomArticle>(response);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual("New Title", updated.Title);
            }
        }

        [Test]
        public void UpdateArticle_GivenOldArticle_ShouldReturnConflict()
        {
            var initialArticle = GetArticle<SerializableArticle>(_sqlInitializer.CreatedArticleIds.First());
            initialArticle.Title = "First title";

            var request = CreateWebRequest("articles/" + initialArticle.Id, "PUT", initialArticle);
            var response = GetWebResponse(request);

            GetResponseContent<SerializableArticle>(response);

            initialArticle.Title = "Second title";

            request = CreateWebRequest("articles/" + initialArticle.Id, "PUT", initialArticle);
            response = GetWebResponse(request);

            Assert.AreEqual(HttpStatusCode.Conflict, response.StatusCode);
        }

        [Test]
        public void UpdateArticle_GivenValidArticleWithDeletedAttachments_ShouldDeleteAttachments()
        {
            var initialArticle = GetArticle<SerializableArticle>(_sqlInitializer.CreatedArticleIds.First());
            initialArticle.Attachments = new List<Attachment>
            {
                new Attachment { Title = "file1", FileId = _sqlInitializer.CreatedFileIds.First() },
                new Attachment { Title = "file2", FileId = _sqlInitializer.CreatedFileIds.First() },
            };

            var request = CreateWebRequest("articles/" + initialArticle.Id, "PUT", initialArticle);
            var response = GetWebResponse(request);

            var updatedArticle = GetResponseContent<SerializableArticle>(response);
            _sqlInitializer.CreatedAttachmentIds.AddRange(updatedArticle.Attachments.Select(a => a.Id));
            
            updatedArticle.Attachments.RemoveAt(0);

            request = CreateWebRequest("articles/" + updatedArticle.Id, "PUT", updatedArticle);
            response = GetWebResponse(request);
            updatedArticle = GetResponseContent<SerializableArticle>(response);

            _sqlInitializer.CreatedAttachmentIds.AddRange(updatedArticle.Attachments.Select(a => a.Id));

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(1, updatedArticle.Attachments.Count);
            Assert.AreEqual("file2", updatedArticle.Attachments[0].Title);
        }

        [Test]
        public void UpdateArticle_GivenValidArticleWithAddedAndDeletedAttachments_ShouldDeleteAndAddAttachments()
        {
            var initialArticle = GetArticle<SerializableArticle>(_sqlInitializer.CreatedArticleIds.First());
            initialArticle.Attachments = new List<Attachment>
            {
                new Attachment { Title = "file1", FileId = _sqlInitializer.CreatedFileIds.First() },
                new Attachment { Title = "file2", FileId = _sqlInitializer.CreatedFileIds.First() },
            };

            var request = CreateWebRequest("articles/" + initialArticle.Id, "PUT", initialArticle);
            var response = GetWebResponse(request);

            var updatedArticle = GetResponseContent<SerializableArticle>(response);
            _sqlInitializer.CreatedAttachmentIds.AddRange(updatedArticle.Attachments.Select(a => a.Id));

            updatedArticle.Attachments.RemoveAt(0);
            updatedArticle.Attachments.Add(new Attachment { Title = "file3", FileId = _sqlInitializer.CreatedFileIds.First() });
            
            request = CreateWebRequest("articles/" + updatedArticle.Id, "PUT", updatedArticle);
            response = GetWebResponse(request);
            updatedArticle = GetResponseContent<SerializableArticle>(response);

            _sqlInitializer.CreatedAttachmentIds.AddRange(updatedArticle.Attachments.Select(a => a.Id));

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(2, updatedArticle.Attachments.Count);
            Assert.AreEqual("file2", updatedArticle.Attachments[0].Title);
            Assert.AreEqual("file3", updatedArticle.Attachments[1].Title);
        }
        
        [Test]
        public void UpdateArticle_GivenValidArticleWithAddedAttachments_ShouldAddAttachments()
        {
            var initialArticle = GetArticle<SerializableArticle>(_sqlInitializer.CreatedArticleIds.First());
            initialArticle.Attachments = new List<Attachment>
            {
                new Attachment { Title = "file1", FileId = _sqlInitializer.CreatedFileIds.First() },
                new Attachment { Title = "file2", FileId = _sqlInitializer.CreatedFileIds.First() },
            };

            var request = CreateWebRequest("articles/" + initialArticle.Id, "PUT", initialArticle);
            var response = GetWebResponse(request);

            var updatedArticle = GetResponseContent<SerializableArticle>(response);
            updatedArticle.Attachments.Add(new Attachment { Title = "file3", FileId = _sqlInitializer.CreatedFileIds.First() });

            request = CreateWebRequest("articles/" + updatedArticle.Id, "PUT", updatedArticle);
            response = GetWebResponse(request);

            updatedArticle = GetResponseContent<SerializableArticle>(response);

            _sqlInitializer.CreatedAttachmentIds.AddRange(updatedArticle.Attachments.Select(a => a.Id));

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(3, updatedArticle.Attachments.Count);
            Assert.AreEqual("file1", updatedArticle.Attachments[0].Title);
            Assert.AreEqual("file2", updatedArticle.Attachments[1].Title);
            Assert.AreEqual("file3", updatedArticle.Attachments[2].Title);
        }
        
        private T GetArticle<T>(int articleId) where T : Article
        {
            var request = CreateWebRequest("articles/" + articleId, "GET");

            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<T>(response);
            }
        }

        private ElearningArticle CreateValidElearningArticle()
        {
            var article = new ElearningArticle
            {
                ProductId = _sqlInitializer.ProductIdWithDescriptionAndArticle,
                ProductDescriptionId = _sqlInitializer.CreatedProductDescriptionIds.First(),
                ArticleTypeId = SqlReadHelper.GetArticleTypeId("E-Learning"),
                StatusCodeId = SqlReadHelper.GetFirstArticleCodeId(),
                LanguageId = SqlReadHelper.GetFirstLanguageId(),
                IsDeleted = false,
                PDU = "some PDU"
            };
            return article;
        } 
        
        private ClassroomArticle CreateValidClassroomArticle()
        {
            var article = new ClassroomArticle
            {
                ProductId = _sqlInitializer.ProductIdWithDescriptionAndArticle,
                ProductDescriptionId = _sqlInitializer.CreatedProductDescriptionIds.First(),
                ArticleTypeId = SqlReadHelper.GetArticleTypeId("Classroom"),
                StatusCodeId = SqlReadHelper.GetFirstArticleCodeId(),
                LanguageId = SqlReadHelper.GetFirstLanguageId(),
                IsDeleted = false,
            };
            return article;
        }

        private ExamArticle CreateValidMultipleChoiceArticle()
        {
            var article = new ExamArticle
            {
                ProductId = _sqlInitializer.ProductIdWithDescriptionAndArticle,
                ProductDescriptionId = _sqlInitializer.CreatedProductDescriptionIds.First(),
                ArticleTypeId = SqlReadHelper.GetArticleTypeId("Multiple Choice"),
                StatusCodeId = SqlReadHelper.GetFirstArticleCodeId(),
                LanguageId = SqlReadHelper.GetFirstLanguageId(),
                IsDeleted = false,
                NumberOfQuestions = 12
            };
            return article;
        }

        private CaseExamArticle CreateValidCaseExamArticle()
        {
            var article = new CaseExamArticle
            {
                ProductId = _sqlInitializer.ProductIdWithDescriptionAndArticle,
                ProductDescriptionId = _sqlInitializer.CreatedProductDescriptionIds.First(),
                ArticleTypeId = SqlReadHelper.GetArticleTypeId("Case Exam"),
                StatusCodeId = SqlReadHelper.GetFirstArticleCodeId(),
                LanguageId = SqlReadHelper.GetFirstLanguageId(),
                IsDeleted = false,
                CaseAttachment = new Attachment { Title = "file1", FileId = _sqlInitializer.CreatedFileIds.First() }
            };
            return article;
        }

        private CaseExamArticle CreateValidProjectAssignmentArticle()
        {
            var article = new CaseExamArticle
            {
                ProductId = _sqlInitializer.ProductIdWithDescriptionAndArticle,
                ProductDescriptionId = _sqlInitializer.CreatedProductDescriptionIds.First(),
                ArticleTypeId = SqlReadHelper.GetArticleTypeId("Project Assignment"),
                StatusCodeId = SqlReadHelper.GetFirstArticleCodeId(),
                LanguageId = SqlReadHelper.GetFirstLanguageId(),
                IsDeleted = false,
                CaseAttachment = new Attachment { Title = "file1", FileId = _sqlInitializer.CreatedFileIds.First() }
            };
            return article;
        }

        private ExamArticle CreateValidCertificationArticle()
        {
            var article = new ExamArticle
            {
                ProductId = _sqlInitializer.ProductIdWithDescriptionAndArticle,
                ProductDescriptionId = _sqlInitializer.CreatedProductDescriptionIds.First(),
                ArticleTypeId = SqlReadHelper.GetArticleTypeId("Internal Certification"),
                StatusCodeId = SqlReadHelper.GetFirstArticleCodeId(),
                LanguageId = SqlReadHelper.GetFirstLanguageId(),
                IsDeleted = false,
                NumberOfQuestions = 12
            };
            return article;
        }

        private SerializableArticle CreateValidArticle()
        {
            var article = new SerializableArticle
            {
                ProductId = _sqlInitializer.ProductIdWithDescriptionAndArticle,
                ProductDescriptionId = _sqlInitializer.CreatedProductDescriptionIds.First(),
                ArticleTypeId = SqlReadHelper.GetFirstArticleTypeId(),
                StatusCodeId = SqlReadHelper.GetFirstArticleCodeId(),
                LanguageId = SqlReadHelper.GetFirstLanguageId(),
                IsDeleted = false
            };
            return article;
        }
    }
}