﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class UsersTests : AuthorizedApiTestBase
    {
        [Test]
        public void GetPerformances_GivenUserIdAndArticleId_ShouldReturnCoursePerformanceStructure()
        {
            var userId = _sqlInitializer.CreatedUserIds.First();
            var article = CreateElearningArticle(23864);

            var request = CreateWebRequest("users/" + userId +"/performances?articleId=" + article.Id, "GET");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                var performances = GetResponseContent<List<PerformanceInfo>>(response);
                Assert.Greater(performances.Count, 0);
            }
        }

        [Test]
        public void Search_MustReturnPartialListOfSearchableUsers()
        {
            var request = CreateWebRequest("users", "GET");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                var users = GetResponseContent<PartialList<SearchableUser>>(response);
                Assert.Greater(users.Items.Count, 0);
            }
        }

        private ElearningArticle CreateElearningArticle(int rcoCourseId)
        {
            var validArticle = CreateValidElearningArticle(rcoCourseId);
            var request = CreateWebRequest("articles/", "POST", validArticle);
            using (var response = GetWebResponse(request))
            {
                var article = GetResponseContent<ElearningArticle>(response);
                _sqlInitializer.CreatedArticleIds.Add(article.Id);
                return article;
            }
        }

        private ElearningArticle CreateValidElearningArticle(int rcoCourseId)
        {
            var article = new ElearningArticle
            {
                ProductId = _sqlInitializer.ProductIdWithDescriptionAndArticle,
                ProductDescriptionId = _sqlInitializer.CreatedProductDescriptionIds.First(),
                ArticleTypeId = SqlReadHelper.GetArticleTypeId("E-Learning"),
                StatusCodeId = SqlReadHelper.GetFirstArticleCodeId(),
                LanguageId = SqlReadHelper.GetFirstLanguageId(),
                IsDeleted = false,
                PDU = "some PDU",
                RcoCourseId = rcoCourseId
            };
            return article;
        } 
    }
}