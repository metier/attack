﻿using System.Linq;
using System.Net;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;
using NUnit.Framework;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class RcosTests : AuthorizedApiTestBase
    {
        [Test]
        public void SearchCourses_MustReturnCourses()
        {
            var request = CreateWebRequest("rcos/courses?query=Norwegian", "GET");
            var response = GetWebResponse(request);
            
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var courses = GetResponseContent<PartialList<SearchableRco>>(response);

            Assert.Greater(courses.TotalCount, 0);
            Assert.Greater(courses.Items.Count, 0);
        }

        [Test]
        public void GetRco_GivenValidId_MustReturnRcoWithLessons()
        {
            const int id = 23864; // Has Three levels: Lessons with Lessons
            var request = CreateWebRequest("rcos/courses/" + id, "GET");
            var response = GetWebResponse(request);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var course = GetResponseContent<RcoCourse>(response);
            Assert.IsNotNull(course);
            Assert.Greater(course.Lessons.Count, 0);

            // Verify than some of the lessons have sub-lessons
            Assert.IsTrue(course.Lessons.Any(l => l.Lessons.Any()));
        }

        [Test]
        public void GetRco_GivenInvalidId_MustReturnNotFound()
        {
            const int id = int.MaxValue;
            var request = CreateWebRequest("rcos/courses/" + id, "GET");
            var response = GetWebResponse(request);

            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}