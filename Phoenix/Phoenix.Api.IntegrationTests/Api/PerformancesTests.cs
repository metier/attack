﻿using System;
using System.Linq;
using System.Net;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class PerformancesTests : AuthorizedApiTestBase
    {
        [Test]
        public void RegisterPerformance_GivenUserIdAndRcoId_ShouldRegisterPerformance()
        {
            var userId = _sqlInitializer.CreatedUserIds.First();
            var rcoId = SqlReadHelper.GetFirstLessonRcoId();

            var registerRequest = new ScormPerformanceRegisterRequest
            {
                CompletedDate = DateTime.UtcNow,
                Score = 1337,
                Status = ScormLessonStatus.Complete,
                TimeUsedInSeconds = 42
            };

            var request = CreateWebRequest("performances/register/" + userId + "/" + rcoId, "PUT", registerRequest);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                var performance = GetResponseContent<ScormPerformance>(response);
                _sqlInitializer.CreatedScormPerformanceIds.Add(performance.Id);
                Assert.Greater(performance.Id, 0);
                Assert.AreEqual(registerRequest.CompletedDate, performance.CompletedDate);
                Assert.AreEqual(registerRequest.Score, performance.Score);
                Assert.AreEqual(registerRequest.Status, performance.Status);
                Assert.AreEqual(registerRequest.TimeUsedInSeconds, performance.TimeUsedInSeconds);
            }
        }
    }
}