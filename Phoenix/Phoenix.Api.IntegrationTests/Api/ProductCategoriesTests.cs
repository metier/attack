﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;
using NUnit.Framework;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class ProductCategoriesTests : AuthorizedApiTestBase
    {
        [Test]
        public void GetAllProductCategories()
        {
            var request = CreateWebRequest("productcategories", "GET");
            var response = GetWebResponse(request);

            var productCategories = GetResponseContent<List<ProductCategory>>(response);

            Assert.GreaterOrEqual(productCategories.Count, 1);
        }
    }
}