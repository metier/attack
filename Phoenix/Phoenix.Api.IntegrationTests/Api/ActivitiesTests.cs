﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Facade.Codes;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class ActivitiesTests : AuthorizedApiTestBase
    {

        [Test]
        public void GetActivity_GivenValidId_ShouldReturnActivity()
        {
            var activityId = _sqlInitializer.CreatedActivityIds.First();
            var request = CreateWebRequest("activities/" + activityId, "GET");
            using (var response = GetWebResponse(request))
            {
                var activity = GetResponseContent<Activity>(response);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(activityId, activity.Id);
            }
        } 

        [Test]
        public void GetActivity_GivenValidIdWithSearchableParameter_ShouldReturnSearchableActivity()
        {
            var activityId = _sqlInitializer.CreatedActivityIds.First();
            var request = CreateWebRequest("activities/" + activityId + "?searchable=true", "GET");
            using (var response = GetWebResponse(request))
            {
                var activity = GetResponseContent<NonAbstractSearchableActivity>(response);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(activityId, activity.ActivityId);
            }
        } 
        
        [Test]
        public void GetActivities_GivenValidCustomerArticleId_ShouldReturnActivities()
        {
            var customerArticleId = _sqlInitializer.CreatedCustomerArticleIds.First();
            var request = CreateWebRequest("activities?distributorId="+_sqlInitializer.DistributorIdWithoutExternalCustomers+"&customerArticleId=" + customerArticleId, "GET");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                var activities = GetResponseContent<PartialList<NonAbstractSearchableActivity>>(response);
                Assert.GreaterOrEqual(activities.Items.Count(), 1);
            }
        }

        [Test]
        public void GetActivities_GivenActivitySetFlag_ShouldReturnActivities()
        {
            var activitySet = GetActivitySetFromService();
            var activity = GetActivityFromService(_sqlInitializer.CreatedActivityIds.First());
            activitySet = AddActivityToActivitySet(activitySet.Id, activity.Id);
            
            var request = CreateWebRequest("activities?includeActivitySets=1&activitySetId=" + activitySet.Id, "GET");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                var activities = GetResponseContent<PartialList<SearchableActivityWithActivitySet>>(response);
                Assert.AreEqual(activities.Items.Count(), 1);
                Assert.AreEqual(activitySet.Name, activities.Items[0].ActivitySetName);
                Assert.AreEqual(activitySet.Id, activities.Items[0].ActivitySetId);
            }
        }

        [Test]
        public void CurrentOrder_GivenActivityWithOrder_ShouldReturnOrderInstance()
        {
            var activity = GetActivityFromService(_sqlInitializer.CreatedActivityIds.First());
            var order = CreateOrder(GetValidOrder(activity));

            using (var response = GetWebResponse(CreateWebRequest("activities/" + activity.Id + "/currentorder", "GET")))
            {
                var result = GetResponseContent<Order>(response);
                Assert.AreEqual(order.Id, result.Id);
            }
        }

        [Test]
        public void CurrentOrder_GivenActivityWithMultipleOrders_ShouldReturnLatestOrderInstance()
        {
            var activity = GetActivityFromService(_sqlInitializer.CreatedActivityIds.First());
            var order = CreateOrder(GetValidOrder(activity, true));
            _sqlInitializer.SetOrderStatus(order.Id, OrderStatus.Invoiced);
            order = CreateOrder(GetValidOrder(activity));

            using (var response = GetWebResponse(CreateWebRequest("activities/" + activity.Id + "/currentorder", "GET")))
            {
                var result = GetResponseContent<OrderInfo>(response);
                Assert.AreEqual(order.Id, result.Id);
            }
        }

        [Test]
        public void CurrentOrder_GivenActivityWithoutOrders_ShouldReturnNoContent()
        {
            var activity = GetActivityFromService(_sqlInitializer.CreatedActivityIds.First());
            using (var response = GetWebResponse(CreateWebRequest("activities/" + activity.Id + "/currentorder", "GET")))
            {
                Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
            }
        }
        
        [Test]
        public void CreateActivity_GivenValidActivity_ShouldCreateActivity()
        {
            var activity = GetValidActivity(ArticleTypes.ELearning);
            var request = CreateWebRequest("activities/", "POST", activity);
            using (var response = GetWebResponse(request))
            {
                var createdActivity = GetResponseContent<Activity>(response);

                _sqlInitializer.CreatedActivityIds.Add(createdActivity.Id);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
                Assert.Greater(createdActivity.Id, 0);
            }
        }

        [Test]
        public void CreateActivity_GivenCustomerArticleWithCustomInformation_ShouldCreateActivityWithInheritedCustomInformation()
        {
            var activity = GetValidActivity(ArticleTypes.ELearning);
            SqlWriteHelper.AddCustomInformationToEntity("CustomerArticle", activity.CustomerArticleId, "Key", "field #1", "field #2", "field #3", "field #4", "field #5");

            var request = CreateWebRequest("activities/", "POST", activity);
            using (var response = GetWebResponse(request))
            {
                var createdActivity = GetResponseContent<Activity>(response);

                _sqlInitializer.CreatedActivityIds.Add(createdActivity.Id);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
                Assert.Greater(createdActivity.Id, 0);

                string sql = string.Format("select count(*) from CustomInformation where EntityType='activity' and entityId={0} and Keyfield='Key' and Field1='field #1' and Field2='field #2' and Field3='field #3' and Field4='field #4' and Field5='field #5'", createdActivity.Id);
                var result = SqlClient.ExecuteScalar(sql);
                Assert.AreEqual(1, result, "Expected custom information to be iherited from the customerarticle to the activity");
            }
        }

        [Test]
        public void CopyActivity_GivenActivityWithCustomInformation_ShouldCreateActivityWithEqualCustomInformation()
        {
            var sourceActivityId = _sqlInitializer.CreatedActivityIds.First();
            SqlWriteHelper.AddCustomInformationToEntity("Activity", sourceActivityId, "Key", "field #1", "field #2", "field #3", "field #4", "field #5");

            var activity = GetValidActivity(ArticleTypes.ELearning);

            var request = CreateWebRequest("activities/copy/" + sourceActivityId, "POST", activity);
            using (var response = GetWebResponse(request))
            {
                var createdActivity = GetResponseContent<Activity>(response);

                _sqlInitializer.CreatedActivityIds.Add(createdActivity.Id);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
                Assert.Greater(createdActivity.Id, 0);

                string sql = string.Format("select count(*) from CustomInformation where EntityType='activity' and entityId={0} and Keyfield='Key' and Field1='field #1' and Field2='field #2' and Field3='field #3' and Field4='field #4' and Field5='field #5'", createdActivity.Id);
                var result = SqlClient.ExecuteScalar(sql);
                Assert.AreEqual(1, result, "Expected custom information to be cloned from the sourceactivity to the new activity");
            }
        }

        [Test]
        public void CreateActivity_GivenValidActivityWithExistingResources_ShouldCreateActivity()
        {
            var resource = GetFirstResource();
            var activity = GetValidActivity(ArticleTypes.ELearning);
            activity.Resources = new List<Resource> { resource };
            var request = CreateWebRequest("activities/", "POST", activity);
            using (var response = GetWebResponse(request))
            {
                var createdActivity = GetResponseContent<Activity>(response);

                _sqlInitializer.CreatedActivityIds.Add(createdActivity.Id);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
                Assert.Greater(createdActivity.Id, 0);
                Assert.AreEqual(1, createdActivity.Resources.Count);
                Assert.AreEqual(resource.Id, createdActivity.Resources[0].Id);
            }
        }

        [Test]
        public void CreateActivity_GivenValidActivityWithPeriods_ShouldCreateActivity()
        {
            var activity = GetValidActivity(ArticleTypes.Classroom);
            activity.ActivityPeriods = new List<ActivityPeriod>
            {
                new ActivityPeriod { Start = DateTime.UtcNow, End = DateTime.UtcNow.AddDays(2) },
                new ActivityPeriod { Start = DateTime.UtcNow.AddDays(2), End = DateTime.UtcNow.AddDays(4) }
            };

            var request = CreateWebRequest("activities/", "POST", activity);
            using (var response = GetWebResponse(request))
            {
                var createdActivity = GetResponseContent<Activity>(response);
                
                _sqlInitializer.CreatedActivityIds.Add(createdActivity.Id);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
                Assert.Greater(createdActivity.Id, 0);
                Assert.AreEqual(createdActivity.ActivityPeriods.Count, 2);
            }
        }

        [Test]
        public void CreateActivity_GivenActivityWithInvalidPeriods_ShouldReturnBadRequest()
        {
            var activity = GetValidActivity(ArticleTypes.Classroom);
            activity.ActivityPeriods = new List<ActivityPeriod>
            {
                new ActivityPeriod { Start = DateTime.UtcNow.AddDays(1), End = DateTime.UtcNow }
            };

            var request = CreateWebRequest("activities/", "POST", activity);
            using (var response = GetWebResponse(request))
            {
                GetResponseContent<Activity>(response);
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        [Test]
        public void UpdateActivity_GivenExistingValidActivity_ShouldUpdateActivity()
        {
            var existingActivity = GetActivityFromService(_sqlInitializer.CreatedActivityIds.First());
            existingActivity.Name = "Updated name";
            var request = CreateWebRequest("activities/" + existingActivity.Id, "PUT", existingActivity);
            using (var response = GetWebResponse(request))
            {
                var updated = GetActivityFromService(existingActivity.Id);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual("Updated name", updated.Name);
            }
        }
        
        [Test]
        public void UpdateActivity_GivenOldActivity_ShouldReturnConflict()
        {
            var existingActivity = GetActivityFromService(_sqlInitializer.CreatedActivityIds.First());

            existingActivity.Name = "First name";
            var request = CreateWebRequest("activities/" + existingActivity.Id, "PUT", existingActivity);
            var response = GetWebResponse(request);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            existingActivity.Name = "Second name";
            request = CreateWebRequest("activities/" + existingActivity.Id, "PUT", existingActivity);
            response = GetWebResponse(request);
            Assert.AreEqual(HttpStatusCode.Conflict, response.StatusCode);
        }

        [Test]
        public void UpdateActivity_GivenValidActivityWithAddedPeriods_ShouldAddPeriods()
        {
            var customerArticleId = SqlReadHelper.GetFirstCustomerArticleId(ArticleTypes.Classroom);
            var activityId = SqlReadHelper.GetFirstActivityOnCustomerArticle(customerArticleId); 
            var existingActivity = GetActivityFromService(activityId);

            existingActivity.ActivityPeriods = new List<ActivityPeriod>
            {
                new ActivityPeriod { Start = DateTime.UtcNow, End = DateTime.UtcNow.AddMinutes(1) }
            };

            var request = CreateWebRequest("activities/" + existingActivity.Id, "PUT", existingActivity);
            using (var response = GetWebResponse(request))
            {
                var updated = GetActivityFromService(existingActivity.Id);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(1, updated.ActivityPeriods.Count);
            }
        }
        
        [Test]
        public void UpdateActivity_ShouldAddPeriods()
        {
            var customerArticleId = SqlReadHelper.GetFirstCustomerArticleId(ArticleTypes.Classroom);
            var activityId = SqlReadHelper.GetFirstActivityOnCustomerArticle(customerArticleId); 
            var existingActivity = GetActivityFromService(activityId);

            existingActivity.ActivityPeriods = new List<ActivityPeriod>
            {
                new ActivityPeriod { Start = DateTime.UtcNow, End = DateTime.UtcNow.AddMinutes(1) }
            };

            var request = CreateWebRequest("activities/" + existingActivity.Id, "PUT", existingActivity);
            using (var response = GetWebResponse(request))
            {
                var updated = GetResponseContent<Activity>(response);
                updated.ActivityPeriods.Add(new ActivityPeriod { Start = DateTime.UtcNow, End = DateTime.UtcNow.AddMinutes(1) });
                updated.ActivityPeriods.Add(new ActivityPeriod { Start = DateTime.UtcNow, End = DateTime.UtcNow.AddMinutes(1) });

                var request2 = CreateWebRequest("activities/" + updated.Id, "PUT", updated);
                using (var response2 = GetWebResponse(request2))
                {
                    var result = GetActivityFromService(updated.Id);

                    Assert.AreEqual(HttpStatusCode.OK, response2.StatusCode);
                    Assert.AreEqual(3, result.ActivityPeriods.Count);
                }
            }
        }

        [Test]
        public void UpdateActivity_ShouldDeletePeriods()
        {
            var customerArticleId = SqlReadHelper.GetFirstCustomerArticleId(ArticleTypes.Classroom);
            var activityId = SqlReadHelper.GetFirstActivityOnCustomerArticle(customerArticleId); 
            var existingActivity = GetActivityFromService(activityId);

            existingActivity.ActivityPeriods = new List<ActivityPeriod>
            {
                new ActivityPeriod { Start = DateTime.UtcNow, End = DateTime.UtcNow.AddMinutes(1) },
                new ActivityPeriod { Start = DateTime.UtcNow, End = DateTime.UtcNow.AddMinutes(1) },
            };

            var request = CreateWebRequest("activities/" + existingActivity.Id, "PUT", existingActivity);
            using (var response = GetWebResponse(request))
            {
                var updated = GetResponseContent<Activity>(response);
                updated.ActivityPeriods.RemoveAt(1);

                var request2 = CreateWebRequest("activities/" + updated.Id, "PUT", updated);
                using (var response2 = GetWebResponse(request2))
                {
                    var result = GetActivityFromService(updated.Id);

                    Assert.AreEqual(HttpStatusCode.OK, response2.StatusCode);
                    Assert.AreEqual(1, result.ActivityPeriods.Count);
                }
            }
        }
        
        [Test]
        public void UpdateActivity_GivenValidActivityWithAddedAttachments_ShouldAddAttachments()
        {
            var existingActivity = GetActivityFromService(_sqlInitializer.CreatedActivityIds.First());
            existingActivity.Attachments = new List<Attachment>
            {
                new Attachment { Title = "Something", FileId = _sqlInitializer.CreatedFileIds.First() }
            };

            var request = CreateWebRequest("activities/" + existingActivity.Id, "PUT", existingActivity);
            using (var response = GetWebResponse(request))
            {
                var updated = GetActivityFromService(existingActivity.Id);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(1, updated.Attachments.Count);
                _sqlInitializer.CreatedAttachmentIds.Add(updated.Attachments[0].Id);
            }
        }

        [Test]
        public void UpdateActivity_ShouldAddAttachments()
        {
            var existingActivity = GetActivityFromService(_sqlInitializer.CreatedActivityIds.First());
            existingActivity.Attachments = new List<Attachment>
            {
                new Attachment { Title = "Something", FileId = _sqlInitializer.CreatedFileIds.First() }
            };

            var request = CreateWebRequest("activities/" + existingActivity.Id, "PUT", existingActivity);
            using (var response = GetWebResponse(request))
            {
                var updated = GetResponseContent<Activity>(response);
                updated.Attachments.Add(new Attachment { Title = "Something", FileId = _sqlInitializer.CreatedFileIds.First() });
                updated.Attachments.Add(new Attachment { Title = "Something", FileId = _sqlInitializer.CreatedFileIds.First() });

                var request2 = CreateWebRequest("activities/" + updated.Id, "PUT", updated);
                using (var response2 = GetWebResponse(request2))
                {
                    var result = GetActivityFromService(updated.Id);
                    Assert.AreEqual(HttpStatusCode.OK, response2.StatusCode);
                    Assert.AreEqual(3, result.Attachments.Count);

                    _sqlInitializer.CreatedAttachmentIds.Add(result.Attachments[0].Id);
                    _sqlInitializer.CreatedAttachmentIds.Add(result.Attachments[1].Id);
                    _sqlInitializer.CreatedAttachmentIds.Add(result.Attachments[2].Id);
                }
            }
        }

        [Test]
        public void UpdateActivity_ShouldDeleteAttachments()
        {
            var existingActivity = GetActivityFromService(_sqlInitializer.CreatedActivityIds.First());
            existingActivity.Attachments = new List<Attachment>
            {
                new Attachment { Title = "Something", FileId = _sqlInitializer.CreatedFileIds.First() },
                new Attachment { Title = "Something", FileId = _sqlInitializer.CreatedFileIds.First() }
            };

            var request = CreateWebRequest("activities/" + existingActivity.Id, "PUT", existingActivity);
            using (var response = GetWebResponse(request))
            {
                var updated = GetResponseContent<Activity>(response);
                updated.Attachments.RemoveAt(1);

                var request2 = CreateWebRequest("activities/" + updated.Id, "PUT", updated);
                using (var response2 = GetWebResponse(request2))
                {
                    var result = GetActivityFromService(updated.Id);

                    Assert.AreEqual(HttpStatusCode.OK, response2.StatusCode);
                    Assert.AreEqual(1, result.Attachments.Count);

                    _sqlInitializer.CreatedAttachmentIds.Add(result.Attachments[0].Id);
                }
            }
        }

        [Test]
        public void UpdateActivity_GivenValidActivityWithExistingResource_ShouldAddResource()
        {
            var resource = GetFirstResource();
            var existingActivity = GetActivityFromService(_sqlInitializer.CreatedActivityIds.First());
            existingActivity.Resources = new List<Resource> { resource };

            var request = CreateWebRequest("activities/" + existingActivity.Id, "PUT", existingActivity);
            using (var response = GetWebResponse(request))
            {
                var updated = GetActivityFromService(existingActivity.Id);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(1, updated.Resources.Count);
                Assert.AreEqual(resource.Id, updated.Resources[0].Id);
            }
        }

        [Test]
        public void UpdateActivity_ShouldDeleteResources()
        {
            var resource = GetFirstResource();
            var existingActivity = GetActivityFromService(_sqlInitializer.CreatedActivityIds.First());
            existingActivity.Resources = new List<Resource>
            {
                resource
            };
            
            var request = CreateWebRequest("activities/" + existingActivity.Id, "PUT", existingActivity);
            using (var response = GetWebResponse(request))
            {
                var updated = GetResponseContent<Activity>(response);
                updated.Resources.RemoveAt(0);

                var request2 = CreateWebRequest("activities/" + updated.Id, "PUT", updated);
                using (var response2 = GetWebResponse(request2))
                {
                    var result = GetActivityFromService(updated.Id);

                    Assert.AreEqual(HttpStatusCode.OK, response2.StatusCode);
                    Assert.AreEqual(0, result.Resources.Count);
                }
            }
        }

        [Test]
        public void UpdateActivity_GivenValidActivityWithAddedParticipant_ShouldAddParticipant()
        {
            var existingActivity = GetActivityFromService(_sqlInitializer.CreatedActivityIds.First());
            existingActivity.Participants = new List<Participant>
            {
                GetValidParticipant()
            };

            var request = CreateWebRequest("activities/" + existingActivity.Id, "PUT", existingActivity);
            using (var response = GetWebResponse(request))
            {
                var updated = GetActivityFromService(existingActivity.Id);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(2, updated.Participants.Count);
            }
        }

        [Test]
        public void UpdateActivity_ShouldNotDeleteParticipants()
        {
            var existingActivity = GetActivityFromService(_sqlInitializer.CreatedActivityIds.First());
            existingActivity.Participants = new List<Participant>
            {
                GetValidParticipant(),
            };

            var request = CreateWebRequest("activities/" + existingActivity.Id, "PUT", existingActivity);
            using (var response = GetWebResponse(request))
            {
                var updated = GetActivityFromService(existingActivity.Id);
                updated.Participants.RemoveAt(0);
                
                var request2 = CreateWebRequest("activities/" + updated.Id, "PUT", updated);
                using (var response2 = GetWebResponse(request2))
                {
                    var result = GetActivityFromService(updated.Id);

                    Assert.AreEqual(HttpStatusCode.OK, response2.StatusCode);
                    Assert.AreEqual(2, result.Participants.Count);
                }
            }
        }

        [Test]
        public void OrderLine_GivenActivityId_ShouldReturnOrderLine()
        {
            var activityId = _sqlInitializer.CreatedActivityIds.First();
            using (var response = GetWebResponse(CreateWebRequest("activities/" + activityId + "/orderline", "GET")))
            {
                var orderLine = GetResponseContent<OrderLine>(response);

                Assert.IsNotNull(orderLine);
            }
        }

        [Test]
        public void OrderLine_GivenNonExistingActivityId_ShouldReturnNotFound()
        {
            const int activityId = int.MaxValue;
            using (var response = GetWebResponse(CreateWebRequest("activities/" + activityId + "/orderline", "GET")))
            {
                Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
            }
        }

        [Test]
        public void GetPerformanceSummaryByActivity_ValidActivity_ReturnsSummaryForAllParticipants()
        {
            var courseId = SqlReadHelper.GetFirstCourseRcoId();
            var courseLesson = SqlReadHelper.GetFirstLessonRcoId(courseId);

            var activitySet = GetActivitySetFromService();
            AddActivityToActivitySet(activitySet.Id, _sqlInitializer.CreatedActivityIds.First());
            EnrollUser(activitySet.Id, SqlReadHelper.GetFirstUserId());

            var activity = GetActivityFromService(_sqlInitializer.CreatedActivityIds.First());
            activity.RcoCourseId = courseId;
            GetWebResponse(CreateWebRequest("activities/" + activity.Id, "PUT", activity));

            var registerRequest = new ScormPerformanceRegisterRequest
            {
                CompletedDate = DateTime.UtcNow,
                Score = 1337,
                Status = ScormLessonStatus.Complete,
                TimeUsedInSeconds = 42
            };
            GetWebResponse(CreateWebRequest("performances/register/" + activity.Participants.First().UserId + "/" + courseLesson, "PUT", registerRequest));

            var response = GetWebResponse(CreateWebRequest(string.Format("activities/{0}/performances", activity.Id), "GET"));

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            var performanceSummaries = GetResponseContent<List<PerformanceSummary>>(response);
            Assert.IsNotNull(performanceSummaries);
            Assert.AreEqual(2, performanceSummaries.Count);

            var performanceSummary = performanceSummaries.First();
            Assert.AreEqual(activity.Participants.First().UserId, performanceSummary.UserId);
            Assert.IsNotNullOrEmpty(performanceSummary.FirstName);
            Assert.IsNotNullOrEmpty(performanceSummary.LastName);
            Assert.AreEqual(1337, performanceSummary.TotalScore);
        }

        [Test]
        public void GetActivitySets_MustReturnAllActivitySetsThatContainsActivity()
        {
            var activity = GetActivityFromService(_sqlInitializer.CreatedActivityIds.First());
            var activitySet1 = GetActivitySetFromService();
            var activitySet2 = GetActivitySetFromService();
            AddActivityToActivitySet(activitySet1.Id, activity.Id);
            AddActivityToActivitySet(activitySet2.Id, activity.Id);

            var response = GetWebResponse(CreateWebRequest(string.Format("activities/{0}/activitysets", activity.Id), "GET"));
            
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            
            var activitySets = GetResponseContent<List<ActivitySet>>(response);

            Assert.AreEqual(2, activitySets.Count);
            Assert.AreEqual(activitySet1.Id, activitySets[0].Id);
            Assert.AreEqual(activitySet2.Id, activitySets[1].Id);
        }

        private ActivitySet EnrollUser(int activitySetId, int? userId = null)
        {
            if (userId == null)
            {
                userId = _sqlInitializer.CreateUser();
            }
            var request = CreateWebRequest("activitysets/" + activitySetId + "/enroll?userId=" + userId.Value, "PUT");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                return GetResponseContent<ActivitySet>(response);
            }
        }
        
        private Participant GetValidParticipant()
        {
            var activityId = SqlWriteHelper.InsertActivity(_sqlInitializer.CreatedCustomerArticleIds.First());
            _sqlInitializer.CreatedActivityIds.Add(activityId);

            return new Participant
            {
                UserId = SqlReadHelper.GetFirstUserId(),
                CustomerId = SqlReadHelper.GetFirstCustomerId(),
                ActivityId = activityId,
                ActivitySetId = _sqlInitializer.CreatedActivitySetIds.First(),
                StatusCodeValue = ParticipantStatusCodes.Enrolled,
                EarliestInvoiceDate = DateTime.UtcNow,
                ParticipantInfoElements = new List<ParticipantInfoElement>()
            };
        }

        private Resource GetFirstResource()
        {
            var request = CreateWebRequest("resources?", "GET");
            using (var response = GetWebResponse(request))
            {
                var resources = GetResponseContent<PartialList<Resource>>(response);
                return resources.Items.First();
            }   
        }

        private Activity GetActivityFromService(int id)
        {
            var request = CreateWebRequest("activities/" + id, "GET");
            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<Activity>(response);
            }
        }

        private Activity GetValidActivity(ArticleTypes articleType)
        {
            var customerArticleId = SqlReadHelper.GetFirstCustomerArticleId(articleType);

            return new Activity
            {
                Name = "Test activity",
                Duration = 1234,
                EarliestInvoiceDate = DateTime.UtcNow,
                CustomerArticleId = customerArticleId,
                MomentTimezoneName = "europe_oslo",
                FixedPriceNotBillable = true,
                UnitPriceNotBillable = false,
                UnitPrice = 1000,
                CurrencyCodeId = 262,
                ActivityPeriods = new List<ActivityPeriod> { new ActivityPeriod {Start = DateTimeOffset.UtcNow } }
            };         
        }

        private ActivitySet GetActivitySetFromService(int? id = null)
        {
            if (id == null)
            {
                id = SqlWriteHelper.InsertActivitySet(_sqlInitializer.CreatedCustomerIds.First());
                _sqlInitializer.CreatedActivitySetIds.Add(id.Value);
            }
            var request = CreateWebRequest("activitysets/" + id, "GET");
            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<ActivitySet>(response);
            }
        }

        private ActivitySet AddActivityToActivitySet(int activitySetId, int activityId)
        {
            var request = CreateWebRequest("activitysets/" + activitySetId + "/addactivity?activityId=" + activityId, "PUT");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                return GetResponseContent<ActivitySet>(response);
            }
        }

        private OrderInfo GetValidOrder(Activity activity, bool? isCreditNote = false)
        {
            var customerId = _sqlInitializer.CreatedCustomerIds.First();
            var userId = _sqlInitializer.CreatedUserIds.First();

            return new OrderInfo
            {
                Title = "TestOrder",
                CoordinatorId = userId,
                CustomerId = customerId,
                OrderNumber = 3000000,
                OurRef = "TestOrder OurRef",
                YourOrder = "TestOrder your order",
                YourRef = "Test Your Ref",
                InvoiceNumber = 12345,
                IsCreditNote = isCreditNote.Value,
                FixedPriceActivities = new List<int> { activity.Id }
            };
        }

        private OrderInfo CreateOrder(OrderInfo order)
        {
            var request = CreateWebRequest("orders/", "POST", order);
            using (var response = GetWebResponse(request))
            {
                var createdOrder = GetResponseContent<OrderInfo>(response);
                _sqlInitializer.CreatedOrderIds.Add(createdOrder.Id);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                return createdOrder;
            }
        }

        // Class used for deserialization
        private class NonAbstractSearchableActivity : SearchableActivity 
        {
            
        }
    }
}