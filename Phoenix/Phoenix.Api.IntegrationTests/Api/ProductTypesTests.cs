﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;
using NUnit.Framework;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class ProductTypesTests : AuthorizedApiTestBase
    {
        [Test]
        public void GetAllProductTypes()
        {
            var request = CreateWebRequest("producttypes", "GET");
            var response = GetWebResponse(request);

            var productTypes = GetResponseContent<List<ProductType>>(response);

            Assert.GreaterOrEqual(productTypes.Count, 1);
        }
    }
}