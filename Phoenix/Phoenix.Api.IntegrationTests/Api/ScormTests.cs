﻿using System.Net;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class ScormTests : AuthorizedApiTestBase
    {
        [Test]
        public void Create_MustInitiateSessionAndReturnsessionId()
        {
            var rcoId = SqlReadHelper.GetFirstLessonRcoId();
            var sessionId = CreateSession(rcoId);

            Assert.IsNotNullOrEmpty(sessionId);
            Assert.AreNotEqual("00000000-0000-0000-0000-000000000000", sessionId);
        }

        [Test]
        public void GetSession_MustReturnScormSession()
        {
            var rcoId = SqlReadHelper.GetFirstLessonRcoId();
            var sessionId = CreateSession(rcoId);

            var result = GetSession(sessionId);

            Assert.IsNotNull(result);
        }

        [Test]
        public void UpdateStatus_MustUpdateStatus()
        {
            var rcoId = SqlReadHelper.GetFirstLessonRcoId();
            var sessionId = CreateSession(rcoId);

            using (var response = GetWebResponse(CreateWebRequest(string.Format("scorm/{0}/status/{1}", sessionId, ScormLessonStatus.Passed), "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
            }
        }

        [Test]
        public void UpdateScore_MustUpdateScore()
        {
            var rcoId = SqlReadHelper.GetFirstLessonRcoId();
            var sessionId = CreateSession(rcoId);

            using (var response = GetWebResponse(CreateWebRequest(string.Format("scorm/{0}/score/{1}", sessionId, 42), "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
            }
        }

        [Test]
        public void UpdateSuspendData_MustUpdateSuspendData()
        {
            var rcoId = SqlReadHelper.GetFirstLessonRcoId();
            var sessionId = CreateSession(rcoId);

            using (var response = GetWebResponse(CreateWebRequest(string.Format("scorm/{0}/suspenddata", sessionId), "PUT", "this is suspend data")))
            {
                Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
            }
        }

        [Test]
        public void Terminate_MustTerminateActiveSession()
        {
            var rcoId = SqlReadHelper.GetFirstLessonRcoId();
            var sessionId = CreateSession(rcoId);

            using (var response = GetWebResponse(CreateWebRequest(string.Format("scorm/{0}/terminate", sessionId), "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
            }
        }

        private string CreateSession(int rcoId)
        {
            using (var response = GetWebResponse(CreateWebRequest(string.Format("scorm/create?rcoId={0}", rcoId), "POST")))
            {
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
                var sessionId = GetResponseContent<string>(response);
                _sqlInitializer.CreatedScormSessionIds.Add(sessionId);
                return sessionId;
            }
        }

        private ScormAttempt GetSession(string sessionId)
        {
            using (var response = GetWebResponse(CreateWebRequest(string.Format("scorm/{0}", sessionId), "GET")))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                return GetResponseContent<ScormAttempt>(response);
            }
        }
    }
}
