﻿using System.Collections.Generic;
using System.Net;
using Metier.Phoenix.Core.Data.Entities;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class LeadingTextsTests : AuthorizedApiTestBase
    {
        // GET leadingtexts/
        [Test]
        public void GetLeadingTexts_MustReturnListOfLeadingTexts()
        {
            var request = CreateWebRequest("leadingtexts/", "GET");
            using (var response = GetWebResponse(request))
            {
                var responseValue = GetResponseString(response);
                var result = JsonConvert.DeserializeObject<List<LeadingText>>(responseValue);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.GreaterOrEqual(result.Count, 0);
            }
        }
    }
}