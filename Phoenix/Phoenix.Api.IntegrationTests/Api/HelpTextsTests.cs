﻿using System.Net;
using Metier.Phoenix.Core.Data.Entities;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class HelpTextsTests : AuthorizedApiTestBase
    {
        [Test]
        public void GetHelpText_GivenSectionAndName_ShouldReturnHelpText()
        {
            const string section = "test-section";
            const string name = "test-name";
            const string text = "test-text";

            _sqlInitializer.CreatedHelpTextIds.Add(SqlWriteHelper.InsertHelpText(section, name, text));

            var request = CreateWebRequest(string.Format("helptexts?section={0}&name={1}", section, name), "GET");
            using (var response = GetWebResponse(request))
            {
                var helpText = GetResponseContent<HelpText>(response);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(text, helpText.Text);
            }
        }

        [Test]
        public void GetHelpText_GivenNonexistingParameters_ShouldReturnNoContent()
        {
            const string section = "something non existing";
            
            var request = CreateWebRequest(string.Format("helptexts?section={0}&name={1}", section, "whatever"), "GET");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
            }
        }
        
        [Test]
        public void CreateHelpText_GivenValidHelpText_ShouldCreateHelpText()
        {
            var helpText = new HelpText
            {
                Name = "some uniqueness",
                Section = "more uniqueness",
                Text = "lots of text"
            };

            var request = CreateWebRequest("helptexts", "POST", helpText);
            using (var response = GetWebResponse(request))
            {
                var createdHelpText = GetResponseContent<HelpText>(response);
                
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
                Assert.AreNotEqual(0, createdHelpText.Id);

                _sqlInitializer.CreatedHelpTextIds.Add(createdHelpText.Id);
            }
        }  

        [Test]
        public void UpdateHelpText_GivenValidHelpText_ShouldUpdateHelpText()
        {
            const string section = "test-section";
            const string name = "test-name";
            const string text = "test-text";
            const string updatedText = "updated text";

            _sqlInitializer.CreatedHelpTextIds.Add(SqlWriteHelper.InsertHelpText(section, name, text));

            var existing = GetHelpText(section, name);

            existing.Text = updatedText;
            var request = CreateWebRequest("helptexts/" + existing.Id, "PUT", existing);
            using (var response = GetWebResponse(request))
            {
                var updated = GetResponseContent<HelpText>(response);
                
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(existing.Id, updated.Id);
                Assert.AreEqual(updatedText, updated.Text);
            }
        }

        private HelpText GetHelpText(string section, string name)
        {
            var request = CreateWebRequest(string.Format("helptexts?section={0}&name={1}", section, name), "GET");
            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<HelpText>(response);
            }
        }
    }
}