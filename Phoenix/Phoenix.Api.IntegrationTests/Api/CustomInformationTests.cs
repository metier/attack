﻿using System.Collections.Generic;
using System.Net;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data.Entities;
using Newtonsoft.Json;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class CustomInformationTests : AuthorizedApiTestBase
    {
        [SetUp]
        public void CustomInformationSetup()
        {
            SqlWriteHelper.AddCustomInformationToEntity("activity", 100, "myKey", "Field1", "Field2", "Field3", "Field4", "Field5");
        }

        [TearDown]
        public void CustomInformationTeardown()
        {
            SqlWriteHelper.DeleteCustomInformation("activity", 100);
        }


        [Test]
        public void GetCustomInformation_MustReturnCustomInformationSet()
        {
            var request = CreateWebRequest(string.Format("custominformation?entitytype={0}&entityid={1}", "activity", 100), "GET");

            using (var response = GetWebResponse(request))
            {
                var responseValue = GetResponseString(response);
                var result = JsonConvert.DeserializeObject<List<CustomInformation>>(responseValue);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.IsNotNull(result);
                Assert.IsTrue(result.Count > 0);
            }
        }
    }
}