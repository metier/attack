﻿using System.Net.Http;
using System.Text;
using Metier.Phoenix.Core.Data.Entities;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class FilesTest : AuthorizedApiTestBase
    {
        [Test]
        public async void File_GivenFormWithFileAndMetaData_ShouldSaveToDisk()
        {
            using (var handler = new HttpClientHandler() {CookieContainer = AuthCookies})
            using (var client = new HttpClient(handler))
            {

                var content1 = new StringContent("Hello World1", Encoding.UTF8, "text/plain");

                var formData = new MultipartFormDataContent();
                formData.Add(content1, "file", "hello1.txt");
                formData.Add(new StringContent("The title"), "title");
                formData.Add(new StringContent("The description"), "description");

                HttpResponseMessage postResponse = await client.PostAsync(ApiRoot + "files", formData);

                postResponse.EnsureSuccessStatusCode();
                var postResult = await postResponse.Content.ReadAsStringAsync();

                var attachment = JsonConvert.DeserializeObject<Attachment>(postResult);

                Assert.AreEqual("The title", attachment.Title);
                Assert.AreEqual("The description", attachment.Description);

                _sqlInitializer.CreatedFileIds.Add(attachment.FileId);
            }
        }
    }
}