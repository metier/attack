﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using Metier.Phoenix.Core.Data.Entities;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class CourseProgramsTests : AuthorizedApiTestBase
    {
        [Test]
        public void GetCoursePrograms_GivenValidCustomerId_ShouldReturnCoursePrograms()
        {
            var customerId = SqlWriteHelper.InsertCustomer(_sqlInitializer.DistributorIdWithoutExternalCustomers, SqlReadHelper.GetFirstLanguageId());
            _sqlInitializer.CreatedCustomerIds.Add(customerId);
            var courseProgramId1 = SqlWriteHelper.InsertCourseProgram("program 1", customerId);
            var courseProgramId2 = SqlWriteHelper.InsertCourseProgram("program 2", customerId);

            _sqlInitializer.CreatedCourseProgramIds.Add(courseProgramId1);
            _sqlInitializer.CreatedCourseProgramIds.Add(courseProgramId2);

            var request = CreateWebRequest("courseprograms?customerId=" + customerId, "GET");
            var response = GetWebResponse(request);
            var coursePrograms = GetResponseContent<List<CourseProgram>>(response);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(2, coursePrograms.Count);
            Assert.AreEqual("program 1", coursePrograms[0].Name);
            Assert.AreEqual("program 2", coursePrograms[1].Name);
        }

        [Test]
        public void GetCourseProgram_GivenValidId_ShouldReturnCourseProgramWithStepsWithExamsAndModules()
        {
            var exams = new List<CourseExam> 
            { 
                new CourseExam { Name = "Exam 1"},
            };
            var modules = new List<CourseModule>
            {
                new CourseModule { Name = "Module 1", Order = 1 },
            };
            var steps = new List<CourseStep>
            {
                new CourseStep 
                { 
                    Name = "Step 1", 
                    Order = 1, 
                    Exams = exams,
                    Modules = modules
                }
            };

            var courseProgramId = SqlWriteHelper.InsertCourseProgram("program", SqlReadHelper.GetFirstCustomerId(), steps: steps);
            _sqlInitializer.CreatedCourseProgramIds.Add(courseProgramId);

            var request = CreateWebRequest("courseprograms/" + courseProgramId, "GET");
            var response = GetWebResponse(request);
            var courseProgram = GetResponseContent<CourseProgram>(response);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            Assert.AreEqual(1, courseProgram.Steps.Count, "CourseProgram does not have any steps");
            Assert.AreEqual(1, courseProgram.Steps[0].Exams.Count, "Step does not have any exams");
            Assert.AreEqual(1, courseProgram.Steps[0].Modules.Count, "Step does not have any modules");
            Assert.AreEqual(courseProgramId, courseProgram.Id);
        }

        [Test]
        public void GetCourseProgram_GivenValidIdOfDeletedItem_ShouldReturnNotFound()
        {
            var courseProgramId = SqlWriteHelper.InsertCourseProgram("program", SqlReadHelper.GetFirstCustomerId(), isDeleted: true);
            _sqlInitializer.CreatedCourseProgramIds.Add(courseProgramId);

            var request = CreateWebRequest("courseprograms/" + courseProgramId, "GET");
            var response = GetWebResponse(request);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public void GetProductsInCourseProgram_GivenValidCustomerId_ShouldReturnAllProductsInCourseProgram()
        {
            var courseProgramId = _sqlInitializer.CreatedCourseProgramIds[0];
 
            var request = CreateWebRequest("productsincourseprogram?courseprogramId=" + courseProgramId, "GET");
            var response = GetWebResponse(request);
            var products = GetResponseContent<List<Product>>(response);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(1, products.Count);
            Assert.AreEqual("The product", products[0].Title);
        }
        

        [Test]
        public void CreateCourseProgram_GivenValidCourseProgram_ShouldCreateCourseProgramWithStepsWithModuleAndExams()
        {
            var exams = new List<CourseExam> 
            { 
                new CourseExam { Name = "Exam 1"},
            };
            var modules = new List<CourseModule>
            {
                new CourseModule { Name = "Module 1", Order = 1 },
            };
            var steps = new List<CourseStep>
            {
                new CourseStep 
                { 
                    Name = "Step 1", 
                    Order = 1, 
                    Exams = exams,
                    Modules = modules
                }
            };

            var courseProgram = new CourseProgram
            {
                Name = "Course program",
                CustomerId = SqlReadHelper.GetFirstCustomerId(),
                Steps = steps
            };

            var request = CreateWebRequest("courseprograms", "POST", courseProgram);

            var response = GetWebResponse(request);
            var created = GetResponseContent<CourseProgram>(response);
            _sqlInitializer.CreatedCourseProgramIds.Add(created.Id);

            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);

            Assert.AreNotEqual(0, created.Id, "CourseProgram's ID was not created");

            Assert.AreEqual(1, created.Steps.Count, "CourseProgram does not have any steps");
            Assert.AreEqual(created.Id, created.Steps[0].CourseProgramId, "Step's CourseProgramId does not match that of CourseProgram");
                
            Assert.AreEqual(1, created.Steps[0].Exams.Count, "Step does not have any exams");
            Assert.AreEqual(1, created.Steps[0].Modules.Count, "Step does not have any modules");

            Assert.AreEqual(created.Steps[0].Id, created.Steps[0].Exams[0].CourseStepId, "CourseExam's StepId does not match that of Step");
            Assert.AreEqual(created.Steps[0].Id, created.Steps[0].Modules[0].CourseStepId, "CourseModule's StepId does not match that of Step");   
        }

        [Test]
        public void UpdateCourseProgram_GivenCourseProgramWithRemovedSteps_ShouldDeleteSteps()
        {
            var steps = new List<CourseStep>
            {
                new CourseStep 
                { 
                    Name = "Step 1", 
                    Order = 1,
                },
                new CourseStep 
                { 
                    Name = "Step 2", 
                    Order = 2
                },
            };

            var courseProgramId = SqlWriteHelper.InsertCourseProgram("course program", SqlReadHelper.GetFirstCustomerId(), steps: steps);
            _sqlInitializer.CreatedCourseProgramIds.Add(courseProgramId);

            var existingCourseProgram = GetCourseProgram(courseProgramId);
            existingCourseProgram.Steps.RemoveAt(0);

            var request = CreateWebRequest("courseprograms/"+existingCourseProgram.Id, "PUT", existingCourseProgram);
            var response = GetWebResponse(request);
            GetResponseContent<CourseProgram>(response);
            
            var updatedCourseProgram = GetCourseProgram(courseProgramId);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(1, updatedCourseProgram.Steps.Count);
            Assert.AreEqual("Step 2", updatedCourseProgram.Steps[0].Name);
        }

        [Test]
        public void UpdateCourseProgram_GivenCourseProgramWithSteps_ShouldUpdateSteps()
        {
            var steps = new List<CourseStep>
            {
                new CourseStep 
                { 
                    Name = "Step 1", 
                    Order = 1,
                }
            };

            var courseProgramId = SqlWriteHelper.InsertCourseProgram("course program", SqlReadHelper.GetFirstCustomerId(), steps: steps);
            _sqlInitializer.CreatedCourseProgramIds.Add(courseProgramId);

            var existingCourseProgram = GetCourseProgram(courseProgramId);
            existingCourseProgram.Steps[0].Name = "New step name";

            var request = CreateWebRequest("courseprograms/" + existingCourseProgram.Id, "PUT", existingCourseProgram);
            var response = GetWebResponse(request);
            GetResponseContent<CourseProgram>(response);

            var updatedCourseProgram = GetCourseProgram(courseProgramId);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual("New step name", updatedCourseProgram.Steps[0].Name);
        }
        
        [Test]
        public void UpdateCourseProgram_GivenCourseProgramWithStepsWithRemovedExams_ShouldDeleteExams()
        {
            var steps = new List<CourseStep>
            {
                new CourseStep 
                { 
                    Name = "Step 1", 
                    Order = 1,
                    Exams = new List<CourseExam>
                    {
                        new CourseExam { Name = "Exam 1"},
                        new CourseExam { Name = "Exam 2"}
                    }
                }
            };

            var courseProgramId = SqlWriteHelper.InsertCourseProgram("course program", SqlReadHelper.GetFirstCustomerId(), steps: steps);
            _sqlInitializer.CreatedCourseProgramIds.Add(courseProgramId);

            var existingCourseProgram = GetCourseProgram(courseProgramId);
            existingCourseProgram.Steps[0].Exams.RemoveAt(0);

            var request = CreateWebRequest("courseprograms/" + existingCourseProgram.Id, "PUT", existingCourseProgram);
            var response = GetWebResponse(request);
            GetResponseContent<CourseProgram>(response);

            var updatedCourseProgram = GetCourseProgram(courseProgramId);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(1, updatedCourseProgram.Steps[0].Exams.Count);
            Assert.AreEqual("Exam 2", updatedCourseProgram.Steps[0].Exams[0].Name);
        }

        [Test]
        public void UpdateCourseProgram_GivenCourseProgramWithStepsWithRemovedModules_ShouldDeleteModules()
        {
            var steps = new List<CourseStep>
            {
                new CourseStep 
                { 
                    Name = "Step 1", 
                    Order = 1,
                    Modules = new List<CourseModule>
                    {
                        new CourseModule { Name = "Module 1", Order = 1 },
                        new CourseModule { Name = "Module 2", Order = 2 },
                    }
                }
            };

            var courseProgramId = SqlWriteHelper.InsertCourseProgram("course program", SqlReadHelper.GetFirstCustomerId(), steps: steps);
            _sqlInitializer.CreatedCourseProgramIds.Add(courseProgramId);

            var existingCourseProgram = GetCourseProgram(courseProgramId);
            existingCourseProgram.Steps[0].Modules.RemoveAt(0);

            var request = CreateWebRequest("courseprograms/" + existingCourseProgram.Id, "PUT", existingCourseProgram);
            var response = GetWebResponse(request);
            GetResponseContent<CourseProgram>(response);

            var updatedCourseProgram = GetCourseProgram(courseProgramId);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(1, updatedCourseProgram.Steps[0].Modules.Count);
            Assert.AreEqual("Module 2", updatedCourseProgram.Steps[0].Modules[0].Name);
        }

        [Test]
        public void UpdateCourseProgram_GivenCourseProgramWithStepsWithExams_ShouldUpdateExams()
        {
            var steps = new List<CourseStep>
            {
                new CourseStep 
                { 
                    Name = "Step 1", 
                    Order = 1,
                    Exams = new List<CourseExam>
                    {
                        new CourseExam { Name = "Exam 1"}
                    }
                }
            };

            var courseProgramId = SqlWriteHelper.InsertCourseProgram("course program", SqlReadHelper.GetFirstCustomerId(), steps: steps);
            _sqlInitializer.CreatedCourseProgramIds.Add(courseProgramId);

            var existingCourseProgram = GetCourseProgram(courseProgramId);
            existingCourseProgram.Steps[0].Exams[0].Name = "New exam name";

            var request = CreateWebRequest("courseprograms/" + existingCourseProgram.Id, "PUT", existingCourseProgram);
            var response = GetWebResponse(request);
            GetResponseContent<CourseProgram>(response);

            var updatedCourseProgram = GetCourseProgram(courseProgramId);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual("New exam name", updatedCourseProgram.Steps[0].Exams[0].Name);
        }

        [Test]
        public void UpdateCourseProgram_GivenCourseProgramWithStepsWithModules_ShouldUpdateModules()
        {
            var steps = new List<CourseStep>
            {
                new CourseStep 
                { 
                    Name = "Step 1", 
                    Order = 1,
                    Modules = new List<CourseModule>
                    {
                        new CourseModule { Name = "Module 1", Order = 1 }
                    }
                }
            };

            var courseProgramId = SqlWriteHelper.InsertCourseProgram("course program", SqlReadHelper.GetFirstCustomerId(), steps: steps);
            _sqlInitializer.CreatedCourseProgramIds.Add(courseProgramId);

            var existingCourseProgram = GetCourseProgram(courseProgramId);
            existingCourseProgram.Steps[0].Modules[0].Name = "New module name";

            var request = CreateWebRequest("courseprograms/" + existingCourseProgram.Id, "PUT", existingCourseProgram);
            var response = GetWebResponse(request);
            GetResponseContent<CourseProgram>(response);

            var updatedCourseProgram = GetCourseProgram(courseProgramId);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual("New module name", updatedCourseProgram.Steps[0].Modules[0].Name);
        }

        [Test]
        public void UpdateCourseProgram_GivenCourseProgramWithAddedSteps_ShouldAddSteps()
        {
            var steps = new List<CourseStep>
            {
                new CourseStep 
                { 
                    Name = "Step 1", 
                    Order = 1,
                }
            };

            var courseProgramId = SqlWriteHelper.InsertCourseProgram("course program", SqlReadHelper.GetFirstCustomerId(), steps: steps);
            _sqlInitializer.CreatedCourseProgramIds.Add(courseProgramId);

            var existingCourseProgram = GetCourseProgram(courseProgramId);
            existingCourseProgram.Steps.Add(new CourseStep { Name = "Step 2", Order = 2 });

            var request = CreateWebRequest("courseprograms/" + existingCourseProgram.Id, "PUT", existingCourseProgram);
            var response = GetWebResponse(request);
            GetResponseContent<CourseProgram>(response);

            var updatedCourseProgram = GetCourseProgram(courseProgramId);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(2, updatedCourseProgram.Steps.Count);
            Assert.AreEqual("Step 2", updatedCourseProgram.Steps[1].Name);
        }

        [Test]
        public void UpdateCourseProgram_GivenCourseProgramWithStepsWithAddedExams_ShouldAddExams()
        {
            var steps = new List<CourseStep>
            {
                new CourseStep 
                { 
                    Name = "Step 1", 
                    Order = 1,
                    Exams = new List<CourseExam>
                    {
                        new CourseExam { Name = "Exam 1"},
                    }
                }
            };

            var courseProgramId = SqlWriteHelper.InsertCourseProgram("course program", SqlReadHelper.GetFirstCustomerId(), steps: steps);
            _sqlInitializer.CreatedCourseProgramIds.Add(courseProgramId);

            var existingCourseProgram = GetCourseProgram(courseProgramId);
            existingCourseProgram.Steps[0].Exams.Add(new CourseExam { Name = "Exam 2" });

            var request = CreateWebRequest("courseprograms/" + existingCourseProgram.Id, "PUT", existingCourseProgram);
            var response = GetWebResponse(request);
            GetResponseContent<CourseProgram>(response);

            var updatedCourseProgram = GetCourseProgram(courseProgramId);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(2, updatedCourseProgram.Steps[0].Exams.Count);
            Assert.AreEqual("Exam 2", updatedCourseProgram.Steps[0].Exams[1].Name);
        }

        [Test]
        public void UpdateCourseProgram_GivenCourseProgramWithStepsWithAddedModules_ShouldAddModules()
        {
            var steps = new List<CourseStep>
            {
                new CourseStep 
                { 
                    Name = "Step 1", 
                    Order = 1,
                    Modules = new List<CourseModule>
                    {
                        new CourseModule { Name = "Module 1", Order = 1 },
                    }
                }
            };

            var courseProgramId = SqlWriteHelper.InsertCourseProgram("course program", SqlReadHelper.GetFirstCustomerId(), steps: steps);
            _sqlInitializer.CreatedCourseProgramIds.Add(courseProgramId);

            var existingCourseProgram = GetCourseProgram(courseProgramId);
            existingCourseProgram.Steps[0].Modules.Add(new CourseModule { Name = "Module 2", Order = 2 });

            var request = CreateWebRequest("courseprograms/" + existingCourseProgram.Id, "PUT", existingCourseProgram);
            var response = GetWebResponse(request);
            GetResponseContent<CourseProgram>(response);

            var updatedCourseProgram = GetCourseProgram(courseProgramId);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(2, updatedCourseProgram.Steps[0].Modules.Count);
            Assert.AreEqual("Module 2", updatedCourseProgram.Steps[0].Modules[1].Name);
        }

        [Test]
        public void UpdateCourseProgram_GivenCourseProgramContainingCustomerArticle_ShouldNotBeAbleToDelete()
        {
            var steps = new List<CourseStep>
            {
                new CourseStep 
                { 
                    Name = "Step 1", 
                    Order = 1,
                    Modules = new List<CourseModule>
                    {
                        new CourseModule { Name = "Module 1", Order = 1 },
                    }
                }
            };

            var courseProgramId = SqlWriteHelper.InsertCourseProgram("course program", SqlReadHelper.GetFirstCustomerId(), steps: steps);
            _sqlInitializer.CreatedCourseProgramIds.Add(courseProgramId);

            var existingCourseProgram = GetCourseProgram(courseProgramId);
            existingCourseProgram.IsDeleted = true;

            CustomerArticle customerArticle = GetCustomerArticle(_sqlInitializer.CreatedCustomerArticleIds.First());
            customerArticle.ModuleId = existingCourseProgram.Steps.First().Modules.First().Id;
            UpdateCustomerArticle(customerArticle);

            var request = CreateWebRequest("courseprograms/" + existingCourseProgram.Id, "PUT", existingCourseProgram);
            var response = GetWebResponse(request);

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        private void UpdateCustomerArticle(CustomerArticle customerArticle)
        {
            var request = CreateWebRequest("customerarticles/" + customerArticle.Id, "PUT", customerArticle);
            var response = GetWebResponse(request);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private CustomerArticle GetCustomerArticle(int customerArticleId)
        {
            var request = CreateWebRequest("customerarticles/" + customerArticleId, "GET");
            var response = GetWebResponse(request);
            return GetResponseContent<CustomerArticle>(response);
        }


        private CourseProgram GetCourseProgram(int id)
        {
            var request = CreateWebRequest("courseprograms/" + id, "GET");
            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<CourseProgram>(response);
            }
        }
    }
}