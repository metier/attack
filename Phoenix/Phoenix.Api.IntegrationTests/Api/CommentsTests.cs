﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Metier.Phoenix.Core.Data.Entities;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class CommentsTests : AuthorizedApiTestBase
    {
        private readonly int entityId = 12345;

        [Test]
        public void GetComments_GivenExistingCommentsForEntityId_ReturnsListOfComments()
        {
            var request = CreateWebRequest(string.Format("comments?entity={0}&entityId={1}", "integrationtest", 1), "GET");
            using (var response = GetWebResponse(request))
            {
                var comments = GetResponseContent<List<Comment>>(response);
                Assert.GreaterOrEqual(comments.Count, 1);
            }
        }

        [Test]
        public void GetComments_GivenExistingCommentsForEntityId_ReturnsStatusOk()
        {
            var request = CreateWebRequest(string.Format("comments?entity={0}&entityId={1}", "integrationtest", 1), "GET");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }
        }

        [Test]
        public void CreateComment_GivenValidComment_ReturnsCreatedStatusCode()
        {
            var comment = GetValidComment();
            var request = CreateWebRequest("comments/", "POST", comment);
            using (var response = GetWebResponse(request))
            {
                var comments = GetResponseContent<List<Comment>>(response);
                var createdComment = comments.First(c => c.EntityId == comment.EntityId);

                _sqlInitializer.CreatedCommentIds.Add(createdComment.Id);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            }
        }

        [Test]
        public void CreateComment_GivenValidComment_NewCommentInReturnedList()
        {
            var comment = GetValidComment();
            comment.CommentText = Guid.NewGuid().ToString();
            var request = CreateWebRequest("comments/", "POST", comment);
            using (var response = GetWebResponse(request))
            {
                var comments = GetResponseContent<List<Comment>>(response);
                var createdComment = comments.First(c => c.EntityId == comment.EntityId);
                _sqlInitializer.CreatedCommentIds.Add(createdComment.Id);
                Assert.AreEqual(comment.CommentText, createdComment.CommentText);
            }
        }

        [Test]
        public void UpdateComment_UpdatesExistingComment_ReturnsListWithChangedComment()
        {
            var existingComment = GetComment();
            existingComment.CommentText = Guid.NewGuid().ToString();
            var request = CreateWebRequest("comments/", "PUT", existingComment);

            using (var response = GetWebResponse(request))
            {
                var comments = GetResponseContent<List<Comment>>(response);
                var createdComment = comments.First(c => c.EntityId == existingComment.EntityId);
                Assert.AreEqual(existingComment.CommentText, createdComment.CommentText);
            }
        }

        [Test]
        public void UpdateComment_UpdatesExistingComment_ReturnsStatusOk()
        {
            var existingComment = GetComment();
            existingComment.CommentText = Guid.NewGuid().ToString();
            var request = CreateWebRequest("comments/", "PUT", existingComment);

            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }
        }

        private Comment GetComment()
        {
            var request = CreateWebRequest(string.Format("comments?entity={0}&entityId={1}", "integrationtest", 1), "GET");
            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<List<Comment>>(response).First();
            }
        }

        private int NextEntityId
        {
            get
            {
                return entityId + 1;
            }
        }

        private Comment GetValidComment()
        {
            return new Comment
            {
                Entity = "IntegrationTests",
                EntityId = NextEntityId,
                CommentText = "Some comment",
                UserId = _sqlInitializer.CreatedUserIds.First()
            };
        }
    }
}