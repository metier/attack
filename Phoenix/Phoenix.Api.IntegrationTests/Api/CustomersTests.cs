﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Utilities.Collections;
using Newtonsoft.Json;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    [Category(TestCategories.ExternalDependencies)]
    public class CustomersTests : AuthorizedApiTestBase
    {
        // PUT customers/:id
        [Test]
        public void UpdateCustomer_GivenValidCustomer_MustReturnUpdatedCustomer()
        {
            var inputCustomer = GetCustomer(_sqlInitializer.CreatedCustomerIds.First());
            inputCustomer.Name = "New name";
            var request = CreateWebRequest("customers/" + inputCustomer.Id, "PUT", inputCustomer);

            using (var response = GetWebResponse(request))
            {
                var responseValue = GetResponseString(response);
                var responseCustomer = JsonConvert.DeserializeObject<Customer>(responseValue);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(inputCustomer.Id, responseCustomer.Id);
                Assert.AreEqual(inputCustomer.Name, responseCustomer.Name);
            }            
        }

        // PUT customers/:id
        [Test]
        public void UpdateCustomer_GivenChangedCustomer_MustUpdateLastModified()
        {
            var inputCustomer = GetCustomer(_sqlInitializer.CreatedCustomerIds.First());
            inputCustomer.Name = "This is a new name for the customer";
            var request = CreateWebRequest("customers/" + inputCustomer.Id, "PUT", inputCustomer);

            using (var response = GetWebResponse(request))
            {
                var responseValue = GetResponseString(response);
                var responseCustomer = JsonConvert.DeserializeObject<Customer>(responseValue);

                Assert.AreEqual(responseCustomer.Created, inputCustomer.Created);
                Assert.Greater(responseCustomer.LastModified, inputCustomer.LastModified);
                Assert.IsFalse(responseCustomer.RowVersion.SequenceEqual(inputCustomer.RowVersion));
            }
        }

        // PUT customers/:id
        [Test]
        public void UpdateCustomer_GivenSameCustomer_MustNotUpdateLastModifiedAndRowVersion()
        {
            var inputCustomer = GetCustomer(_sqlInitializer.CreatedCustomerIds.First());
            var request = CreateWebRequest("customers/" + inputCustomer.Id, "PUT", inputCustomer);

            using (var response = GetWebResponse(request))
            {
                var responseValue = GetResponseString(response);
                var responseCustomer = JsonConvert.DeserializeObject<Customer>(responseValue);

                Assert.AreEqual(responseCustomer.Created, inputCustomer.Created);
                Assert.AreEqual(responseCustomer.LastModified, inputCustomer.LastModified);
                Assert.IsTrue(responseCustomer.RowVersion.SequenceEqual(inputCustomer.RowVersion));
            }
        }

        // PUT customers/:id
        [Test]
        public void UpdateCustomer_GivenInvalidCustomer_MustReturnBadRequest()
        {
            var inputCustomer = GetCustomer(_sqlInitializer.CreatedCustomerIds.First());
            inputCustomer.LanguageId = null;  // Make customer invalid

            var request = CreateWebRequest("customers/" + inputCustomer.Id, "PUT", inputCustomer);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        // PUT customers/:id
        [Test]
        public void UpdateCustomer_GivenOldCustomer_MustReturnConflict()
        {
            var originalCustomer = GetCustomer(_sqlInitializer.CreatedCustomerIds.First());
            var staleCustomer = GetCustomer(originalCustomer.Id);
            
            staleCustomer.Name = "First change";

            var request = CreateWebRequest("customers/" + staleCustomer.Id, "PUT", staleCustomer);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }

            staleCustomer.Name = "Second change without refresh";

            request = CreateWebRequest("customers/" + staleCustomer.Id, "PUT", staleCustomer);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.Conflict, response.StatusCode);
            }
        }

        [Test]
        public void UpdateCustomer_GivenMismatchingIds_MustReturnBadRequest()
        {
            var customer = GetCustomer(_sqlInitializer.CreatedCustomerIds.First());
            customer.Name = "CustomerName change";

            var request = CreateWebRequest("customers/" + 1, "PUT", customer);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        // PUT customers/:id
        [Test]
        public void UpdateCustomer_GivenCustomerWithDuplicateLeadingTexts_MustNotReturnOK()
        {
            var leadingtexts = GetLeadingTexts();
            var inputCustomer = GetCustomer(_sqlInitializer.CreatedCustomerIds.First());

            inputCustomer.CustomLeadingTexts = new List<CustomLeadingText> 
                { 
                    new CustomLeadingText { LeadingTextId = leadingtexts[0].Id },
                    new CustomLeadingText { LeadingTextId = leadingtexts[0].Id }
                };

            var request = CreateWebRequest("customers/" + inputCustomer.Id, "PUT", inputCustomer);
            using (var response = GetWebResponse(request))
            {
                Assert.AreNotEqual(HttpStatusCode.OK, response.StatusCode);
            }
        }                
        
        // POST customers/
        [Test]
        public void CreateCustomer_MustReturnCreatedCustomer()
        {
            var inputCustomer = CreateValidCustomer();
            
            var request = CreateWebRequest("customers/", "POST", inputCustomer);

            using (var response = GetWebResponse(request))
            {
                var responseValue = GetResponseString(response);
                var responseCustomer = JsonConvert.DeserializeObject<Customer>(responseValue);

                _sqlInitializer.CreatedCustomerIds.Add(responseCustomer.Id);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
                Assert.IsNotNull(responseCustomer.Id);
            }    
        }

        [Test]
        public void CreateCustomer_SetsLastModifiedAndCreatedToSameValue()
        {
            var inputCustomer = CreateValidCustomer();

            var request = CreateWebRequest("customers/", "POST", inputCustomer);

            using (var response = GetWebResponse(request))
            {
                var responseValue = GetResponseString(response);
                var responseCustomer = JsonConvert.DeserializeObject<Customer>(responseValue);

                _sqlInitializer.CreatedCustomerIds.Add(responseCustomer.Id);
                Assert.AreEqual(responseCustomer.LastModified, responseCustomer.Created);
            }
        }

        // POST customers/
        [Test]
        public void CreateCustomer_GivenInvalidCustomer_MustReturnBadRequest()
        {
            var inputCustomer = CreateValidCustomer();
            
            inputCustomer.LanguageId = null;  // Make customer invalid

            var request = CreateWebRequest("customers/", "POST", inputCustomer);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }                
        }

        // GET customers/:customerId
        [Test]
        public void GetCustomer_MustReturnExistingCustomer()
        {
            var existingCustomerId = _sqlInitializer.CreatedCustomerIds.First();
            var request = CreateWebRequest("customers/" + existingCustomerId, "GET");
            using (var response = GetWebResponse(request))
            {
                var responseValue = GetResponseString(response);
                var customer = JsonConvert.DeserializeObject<Customer>(responseValue);

                Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
                Assert.IsNotNull(customer);
                Assert.AreEqual(existingCustomerId, customer.Id);
            }
        }

        // GET customers/:customerId
        [Test]
        public void GetCustomer_RequestingNonExistingId_MustReturnNotFound()
        {
            const int nonExistingId = int.MaxValue;
            var request = CreateWebRequest("customers/" + nonExistingId, "GET");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(response.StatusCode, HttpStatusCode.NotFound);
            }
        }

        // GET customers
        [Test]
        public void SearchCustomers_SearchWithoutParameters_MustReturnListOf20CustomersAndTotalCount()
        {
            InsertCustomers();
            var request = CreateWebRequest(string.Format("customers?distributorId={0}", _sqlInitializer.DistributorIdWithoutExternalCustomers), "GET");
            using (var response = GetWebResponse(request))
            {
                var responseContent = GetResponseString(response);
                var result = JsonConvert.DeserializeObject<PartialList<Customer>>(responseContent);

                Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
                Assert.IsNotNull(result);
                Assert.IsTrue(result.TotalCount > 0);
                Assert.AreEqual(20, result.Items.Count);
            }
        }

        // GET customers?limit=15
        [Test]
        public void SearchCustomers_SearchWithLimit_ReturnsSameNumberOfItemsAsSetByLimit()
        {
            InsertCustomers();
            var request = CreateWebRequest(string.Format("customers?limit=15&distributorId={0}", _sqlInitializer.DistributorIdWithoutExternalCustomers), "GET");
            using (var response = GetWebResponse(request))
            {
                var responseContent = GetResponseString(response);
                var result = JsonConvert.DeserializeObject<PartialList<Customer>>(responseContent);

                Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
                Assert.AreEqual(15, result.Items.Count);
            }
        }

        // GET customers?limit=15&skip=5

        [Test]
        public void SearchCustomers_SearchWithSkip_ReturnsCustomersSkippingTheSpecifiedNumberOfCustomersInCustomerTable()
        {
            InsertCustomers();
            var noSkipRequest = CreateWebRequest(string.Format("customers?limit=15&distributorId={0}", _sqlInitializer.DistributorIdWithoutExternalCustomers), "GET");
            PartialList<Customer> noSkipResult;
            using (var response = GetWebResponse(noSkipRequest))
            {
                var responseContent = GetResponseString(response);
                noSkipResult = JsonConvert.DeserializeObject<PartialList<Customer>>(responseContent);
            }

            var skipRequest = CreateWebRequest(string.Format("customers?limit=15&skip=5&distributorId={0}", _sqlInitializer.DistributorIdWithoutExternalCustomers), "GET");
            using (var response = GetWebResponse(skipRequest))
            {
                var responseContent = GetResponseString(response);
                var skipResult = JsonConvert.DeserializeObject<PartialList<Customer>>(responseContent);

                Assert.AreEqual(noSkipResult.Items[5].Id, skipResult.Items[0].Id);
            }
        }

        // GET customers?limit=15&query=MeTi

        [Test]
        public void SearchCustomers_SearchWithQuery_ReturnsOnlyCustomersContainingQueryInName()
        {
            const string query = "MeTi";
            var request = CreateWebRequest(string.Format("customers?limit=4&query={0}&distributorId={1}", query, _sqlInitializer.DistributorIdWithoutExternalCustomers), "GET");
            using (var response = GetWebResponse(request))
            {
                var responseContent = GetResponseString(response);
                var result = JsonConvert.DeserializeObject<PartialList<Customer>>(responseContent);

                Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
                foreach (var customer in result.Items)
                {
                    Assert.IsTrue(customer.Name.ToLower().Contains(query.ToLower()));
                }
            }
        }

        // GET customers?limit=15&query=ben%20lux

        [Test]
        public void SearchCustomers_SearchWithQuery_ReturnsOnlyCustomersContainingQueryValuesInName()
        {
            const string query = "ben";
            const string query2 = "lux";
            var request = CreateWebRequest(string.Format("customers?limit=4&query={0}%20{1}&distributorId={2}", query, query2, _sqlInitializer.DistributorIdWithoutExternalCustomers), "GET");
            using (var response = GetWebResponse(request))
            {
                var responseContent = GetResponseString(response);
                var result = JsonConvert.DeserializeObject<PartialList<Customer>>(responseContent);

                Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
                foreach (var customer in result.Items)
                {
                    Assert.IsTrue(customer.Name.ToLower().Contains(query.ToLower()));
                    Assert.IsTrue(customer.Name.ToLower().Contains(query2.ToLower()));
                }
            }
        }

        // PUT customers/:id/delete
        [Test]
        public void DeleteCustomer_GivenSuccessfulValidation_ShouldDeleteCustomer()
        {
            int customerId = SqlWriteHelper.InsertCustomer(1194, SqlReadHelper.GetFirstLanguageId(), false);
            _sqlInitializer.CreatedCustomerIds.Add(customerId);

            SqlWriteHelper.InsertCustomerAddress(customerId, "somestreet", "1234", "Somewhere", "NO");
            SqlWriteHelper.InsertMetierContactPerson(customerId, _sqlInitializer.CreatedUserIds.First());
            SqlWriteHelper.InsertCustomerContactPerson(customerId, "Some Name (integration testing)");

            var request = CreateWebRequest("customers/" + customerId + "/delete", "PUT");

            using (GetWebResponse(request))
            {
                var deleted = GetCustomer(customerId);
                Assert.AreEqual(0, deleted.Id);
            }
        }

        // PUT customers/:id/delete
        [Test]
        public void DeleteCustomer_GivenFailedValidation_ShouldReturnBadRequest()
        {
            int customerId = SqlWriteHelper.InsertCustomer(1194, SqlReadHelper.GetFirstLanguageId());
            _sqlInitializer.CreatedCustomerIds.Add(customerId);

            var request = CreateWebRequest("customers/" + customerId + "/delete", "PUT");

            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }


        [Test]
        public void GetPerformanceSummaryByCustomer_ValidCustomer_ReturnsSummaryForUsers()
        {
            var courseId = SqlReadHelper.GetFirstCourseRcoId();
            var courseLesson = SqlReadHelper.GetFirstLessonRcoId(courseId);
            var userId = SqlReadHelper.GetFirstUserId();
            var customerId = SqlReadHelper.GetCustomerForUser(userId);

            var activitySet = GetActivitySetFromService();
            AddActivityToActivitySet(activitySet.Id, _sqlInitializer.CreatedActivityIds.First());
            EnrollUser(activitySet.Id, userId);

            var activity = GetActivityFromService(_sqlInitializer.CreatedActivityIds.First());
            activity.RcoCourseId = courseId;
            GetWebResponse(CreateWebRequest("activities/" + activity.Id, "PUT", activity));

            var registerRequest = new ScormPerformanceRegisterRequest
            {
                CompletedDate = DateTime.UtcNow,
                Score = 1337,
                Status = ScormLessonStatus.Complete,
                TimeUsedInSeconds = 42
            };
            GetWebResponse(CreateWebRequest("performances/register/" + userId + "/" + courseLesson, "PUT", registerRequest));

            var response = GetWebResponse(CreateWebRequest(string.Format("customers/{0}/performances", customerId), "GET"));

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            var performanceSummaries = GetResponseContent<List<PerformanceSummary>>(response);
            Assert.IsNotNull(performanceSummaries);

            var performanceSummary = performanceSummaries.First();
            Assert.AreEqual(userId, performanceSummary.UserId);
            Assert.IsNotNullOrEmpty(performanceSummary.FirstName);
            Assert.IsNotNullOrEmpty(performanceSummary.LastName);
        }

        private void InsertCustomers()
        {
            for (int i = 0; i < 20; i++)
            {
                _sqlInitializer.CreatedCustomerIds.Add(SqlWriteHelper.InsertCustomer(_sqlInitializer.DistributorIdWithoutExternalCustomers, SqlReadHelper.GetFirstLanguageId()));
            }
        }

        private List<LeadingText> GetLeadingTexts()
        {
            var request = CreateWebRequest("leadingtexts", "GET");
            using (var response = GetWebResponse(request))
            {
                var responseValue = GetResponseString(response);
                return JsonConvert.DeserializeObject<List<LeadingText>>(responseValue);
            }
        }

        private Customer GetCustomer(int id)
        {
            var request = CreateWebRequest("customers/" + id, "GET");
            using (var response = GetWebResponse(request))
            {
                var responseValue = GetResponseString(response);
                return JsonConvert.DeserializeObject<Customer>(responseValue);
            }
        }

        private static Customer CreateValidCustomer()
        {
            return new Customer
            {
                Name = "Testkunde",
                DistributorId = 1234,
                LanguageId = 5678
            };
        }

        private ActivitySet GetActivitySetFromService(int? id = null)
        {
            if (id == null)
            {
                id = SqlWriteHelper.InsertActivitySet(_sqlInitializer.CreatedCustomerIds.First());
                _sqlInitializer.CreatedActivitySetIds.Add(id.Value);
            }
            var request = CreateWebRequest("activitysets/" + id, "GET");
            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<ActivitySet>(response);
            }
        }

        private ActivitySet AddActivityToActivitySet(int activitySetId, int activityId)
        {
            var request = CreateWebRequest("activitysets/" + activitySetId + "/addactivity?activityId=" + activityId, "PUT");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                return GetResponseContent<ActivitySet>(response);
            }
        }

        private ActivitySet EnrollUser(int activitySetId, int? userId = null)
        {
            if (userId == null)
            {
                userId = _sqlInitializer.CreateUser();
            }
            var request = CreateWebRequest("activitysets/" + activitySetId + "/enroll?userId=" + userId.Value, "PUT");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                return GetResponseContent<ActivitySet>(response);
            }
        }

        private Activity GetActivityFromService(int id)
        {
            var request = CreateWebRequest("activities/" + id, "GET");
            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<Activity>(response);
            }
        }
    }
}