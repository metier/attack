﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Facade.Participant;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Utilities;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class ParticipantTests : AuthorizedApiTestBase
    {

        #region Retrieve Participant

        [Test]
        public void Get_GivenIdToExistingParticipant_ShouldReturnParticipant()
        {
            var participantId = _sqlInitializer.CreatedParticipantIds.First();
            var request = CreateWebRequest("participants/" + participantId, "GET");
            using (var response = GetWebResponse(request))
            {
                var participant = GetResponseContent<Participant>(response);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(participantId, participant.Id);
            }
        }
        
        [Test]
        public void Get_GivenIdToNonExistingParticipant_ShouldReturnNotFound()
        {
            const int nonExistingParticipantId = int.MaxValue;
            var request = CreateWebRequest("participants/" + nonExistingParticipantId, "GET");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
            }
        }

        [Test]
        public void Get_GivenIdToDeletedParticipant_ShouldReturnNotFound()
        {
            var participantId = _sqlInitializer.CreatedParticipantIds.First();
            _sqlInitializer.SetParticipantDeleted(participantId);

            var request = CreateWebRequest("participants/" + participantId, "GET");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
            }
        }

        #endregion

        #region Create Participant

        [Test]
        public void Create_GivenValidParticipantWithInfoElements_ShouldCreateParticipant()
        {
            var infoElements = new List<ParticipantInfoElement>();
            infoElements.Add(GetValidParticipantInfoElement("Some text 1", LeadingTexts.Customfield1));
            infoElements.Add(GetValidParticipantInfoElement("Some text 2", LeadingTexts.Customfield2));

            var participant = GetValidParticipant(null, infoElements.ToArray());
            
            var request = CreateWebRequest("participants", "POST", participant);
            using (var response = GetWebResponse(request))
            {
                var created = GetResponseContent<Participant>(response);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
                Assert.Greater(created.Id, 0);
                
                Assert.AreEqual(2, created.ParticipantInfoElements.Count);
                Assert.AreEqual("Some text 1", created.ParticipantInfoElements[0].InfoText);
                Assert.AreEqual("Some text 2", created.ParticipantInfoElements[1].InfoText);

                _sqlInitializer.CreatedParticipantIds.Add(created.Id);
            }
        }

        [Test]
        public void Create_GivenValidParticipantWithAttachment_ShouldCreateParticipant_And_IgnoreAttachment()
        {
            var participant = GetValidParticipant();
            participant.Attachments.Add(new Attachment{ FileId = _sqlInitializer.CreatedFileIds.First() });
            
            var request = CreateWebRequest("participants", "POST", participant);
            using (var response = GetWebResponse(request))
            {
                var created = GetResponseContent<Participant>(response);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
                
                Assert.AreEqual(0, created.Attachments.Count);

                _sqlInitializer.CreatedParticipantIds.Add(created.Id);
            }
        }

        #endregion 

        #region Update participant

        [Test]
        public void Update_GivenParticipantToBeUpdate_ShouldUpdateParticipant()
        {
            var participant = CreateParticipant();
            participant.UnitPrice = 1337;
            
            var request = CreateWebRequest("participants/" + participant.Id, "PUT", participant);
            using (var response = GetWebResponse(request))
            {
                var updated = GetResponseContent<Participant>(response);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(1337, updated.UnitPrice);
            }
        }

        [Test]
        public void Update_GivenParticipantWithAttachment_ShouldUpdateParticipant_And_IgnoreAttachment()
        {
            var participant = CreateParticipant();
            participant.Attachments.Add(new Attachment { FileId = _sqlInitializer.CreatedFileIds.First() });

            var request = CreateWebRequest("participants/" + participant.Id, "PUT", participant);
            using (var response = GetWebResponse(request))
            {
                var updated = GetResponseContent<Participant>(response);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(0, updated.Attachments.Count);
            }
        }

        [Test]
        public void Update_GivenOldParticipant_ShouldReturnConflict()
        {
            var participant = CreateParticipant();
            participant.UnitPrice = 1337;
            var request = CreateWebRequest("participants/" + participant.Id, "PUT", participant);
            GetWebResponse(request);
            request = CreateWebRequest("participants/" + participant.Id, "PUT", participant);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.Conflict, response.StatusCode);
            }
        }

        [Test]
        public void Update_GivenParticipantWithAddedInfoElements_ShouldUpdateParticipant()
        {
            var infoElements = new List<ParticipantInfoElement>();
            infoElements.Add(GetValidParticipantInfoElement("Some text 1", LeadingTexts.Customfield1));

            var participant = CreateParticipant(null, infoElements.ToArray());

            var request = CreateWebRequest("participants/" + participant.Id, "PUT", participant);
            using (var response = GetWebResponse(request))
            {
                var updated = GetResponseContent<Participant>(response);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                Assert.AreEqual(1, updated.ParticipantInfoElements.Count);
                Assert.AreEqual("Some text 1", updated.ParticipantInfoElements[0].InfoText);
            }
        }

        [Test]
        public void Update_GivenParticipantWithDeletedInfoElements_ShouldUpdateParticipant()
        {
            var participant = CreateParticipant(null, GetValidParticipantInfoElement("Some text 1", LeadingTexts.Customfield1), GetValidParticipantInfoElement("Some text 2", LeadingTexts.Customfield2));
            participant.ParticipantInfoElements.RemoveAt(0);
            var request = CreateWebRequest("participants/" + participant.Id, "PUT", participant);
            using (var response = GetWebResponse(request))
            {
                var updated = GetResponseContent<Participant>(response);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                Assert.AreEqual(1, updated.ParticipantInfoElements.Count);
                Assert.AreEqual("Some text 2", updated.ParticipantInfoElements[0].InfoText);
            }
        }

        #endregion

        #region Move participant

        [Test]
        public void Move_GivenParticipantId_AndMoveRequestWithExistingActivityIdAndActivitySetId_ShouldMoveParticipant()
        {
            var participant = CreateParticipant();
            var newActivity = GetActivityFromService();
            var newActivitySet = GetActivitySetFromService();

            var moveRequest = new ParticipantMoveRequest
            {
                ActivityId = newActivity.Id, 
                ActivitySetId = newActivitySet.Id
            };

            var request = CreateWebRequest("participants/" + participant.Id + "/move", "PUT", moveRequest);
            using (var response = GetWebResponse(request))
            {
                var updated = GetResponseContent<Participant>(response);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                Assert.AreEqual(newActivity.Id, updated.ActivityId);
                Assert.AreEqual(newActivitySet.Id, updated.ActivitySetId);
            }
        }

        [Test]
        public void Move_GivenNonExistingActivity_ShouldNotMoveParticipant()
        {
            var participant = CreateParticipant();
            var newActivitySet = GetActivitySetFromService();

            var moveRequest = new ParticipantMoveRequest
            {
                ActivityId = int.MaxValue,
                ActivitySetId = newActivitySet.Id
            };

            var request = CreateWebRequest("participants/" + participant.Id + "/move", "PUT", moveRequest);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        [Test]
        public void Move_GivenNonExistingActivitySet_ShouldNotMoveParticipant()
        {
            var participant = CreateParticipant();
            var newActivity = GetActivityFromService();

            var moveRequest = new ParticipantMoveRequest
            {
                ActivityId = newActivity.Id,
                ActivitySetId = int.MaxValue
            };

            var request = CreateWebRequest("participants/" + participant.Id + "/move", "PUT", moveRequest);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        [Test]
        public void Move_GivenFullActivity_ShouldNotMoveParticipant()
        {
            var participant = CreateParticipant();
            
            var newActivity = GetActivityFromService();
            newActivity.MaxParticipants = 0;
            newActivity = UpdateActivity(newActivity);

            var newActivitySet = GetActivitySetFromService();

            var moveRequest = new ParticipantMoveRequest
            {
                ActivityId = newActivity.Id,
                ActivitySetId = newActivitySet.Id
            };

            var request = CreateWebRequest("participants/" + participant.Id + "/move", "PUT", moveRequest);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        [Test]
        public void Move_GivenActivityWithoutMaxParticipantLimit_ShouldMoveParticipant()
        {
            var participant = CreateParticipant();
            var newActivity = GetActivityFromService();
            newActivity.MaxParticipants = null;
            newActivity = UpdateActivity(newActivity);
            var newActivitySet = GetActivitySetFromService();

            var moveRequest = new ParticipantMoveRequest
            {
                ActivityId = newActivity.Id,
                ActivitySetId = newActivitySet.Id
            };

            var request = CreateWebRequest("participants/" + participant.Id + "/move", "PUT", moveRequest);
            using (var response = GetWebResponse(request))
            {
                var updated = GetResponseContent<Participant>(response);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                Assert.AreEqual(newActivity.Id, updated.ActivityId);
                Assert.AreEqual(newActivitySet.Id, updated.ActivitySetId);
            }
        }

        #endregion

        #region Invoicing
        [Test]
        public void CurrentOrder_GivenParticipantWithOrder_ShouldReturnOrderInstance()
        {
            var participant = CreateParticipant();
            var order = CreateOrder(GetValidOrder(participant));

            using (var response = GetWebResponse(CreateWebRequest("participants/" + participant.Id + "/currentorder", "GET")))
            {
                var result = GetResponseContent<OrderInfo>(response);
                Assert.AreEqual(order.Id, result.Id);
            }
        }

        [Test]
        public void CurrentOrder_GivenParticipantWithMultipleOrders_ShouldReturnLatestOrderInstance()
        {
            var participant = CreateParticipant();
            var order = CreateOrder(GetValidOrder(participant, true));
            _sqlInitializer.SetOrderStatus(order.Id, OrderStatus.Invoiced);
            order = CreateOrder(GetValidOrder(participant));

            using (var response = GetWebResponse(CreateWebRequest("participants/" + participant.Id + "/currentorder", "GET")))
            {
                var result = GetResponseContent<OrderInfo>(response);
                Assert.AreEqual(order.Id, result.Id);
            }
        }

        [Test]
        public void CurrentOrder_GivenParticipantWithoutOrders_ShouldReturnNoContent()
        {
            var participant = CreateParticipant();
            using (var response = GetWebResponse(CreateWebRequest("participants/" + participant.Id + "/currentorder", "GET")))
            {
                Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
            }
        }

        #endregion


        #region Various tests

        [Test]
        public void DueDate_GivenParticipantWithExamActivityContainingDuration_ShouldReturnDateTimeWithDurationAddedToExamCreatedDate()
        {
            var activityId = _sqlInitializer.InsertMultipleChoiceExamActivity();
            var participant = CreateParticipant(activityId);
            var activity = GetActivityFromService(activityId);

            activity.Duration = (long)TimeSpan.FromDays(2).TotalMilliseconds;
            UpdateActivity(activity);

            var examCreated = DateTime.Now;
            var expectedDateTime = examCreated + TimeSpan.FromMilliseconds((double)activity.Duration);

            _sqlInitializer.InsertExam(participant.Id, examCreated);

            var request = CreateWebRequest($"participants/{participant.Id}/duedate", "GET");
            using (var response = GetWebResponse(request))
            {
                var dueDate = GetResponseContent<DateTime>(response);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.IsTrue(Math.Abs((expectedDateTime - dueDate).TotalMinutes) < 5); //Accepting 5 minutes offset...
            }
        }

        [Test]
        public void DueDate_GivenParticipantWithElearningActivityContainingCompletionTargetDuration_ShouldReturnDateTimeWithDurationAddedToParticipantCreatedDate()
        {
            var participant = CreateParticipant();
            var activity = GetActivityFromService(participant.ActivityId);
            
            activity.CompletionTargetDuration = (long)TimeSpan.FromDays(2).TotalMilliseconds;
            activity.CompletionTargetDate = null;
            UpdateActivity(activity);

            var request = CreateWebRequest($"participants/{participant.Id}/duedate", "GET");
            using (var response = GetWebResponse(request))
            {
                var dueDate = GetResponseContent<DateTime>(response);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(participant.Created + TimeSpan.FromMilliseconds((double)activity.CompletionTargetDuration), dueDate);
            }
        }

        [Test]
        public void DueDate_GivenParticipantWithElearningActivityContainingCompletionTargetDate_ShouldReturnCompletionTargetDateAsDueDate()
        {
            var expectedDueDate = DateTime.Today + TimeSpan.FromDays(2);

            var participant = CreateParticipant();
            var activity = GetActivityFromService(participant.ActivityId);

            activity.CompletionTargetDuration = null;
            activity.CompletionTargetDate = expectedDueDate;
            UpdateActivity(activity);

            var request = CreateWebRequest($"participants/{participant.Id}/duedate", "GET");
            using (var response = GetWebResponse(request))
            {
                var dueDate = GetResponseContent<DateTime>(response);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(expectedDueDate, dueDate);
            }
        }

        [Test]
        public void SubmitExam_GivenAttachment_ShouldAttachToParticipantAndSetStatusCompleted()
        {
            var participant = CreateParticipant(_sqlInitializer.CaseExamActivityId);
            var attachment = new Attachment { Title = "My exam", FileId = _sqlInitializer.CreatedFileIds.First() };
            using (var response = GetWebResponse(CreateWebRequest("participants/" + participant.Id + "/submitexam", "PUT", attachment)))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }

            participant = GetParticipantFromService(participant.Id);
            Assert.AreEqual(1, participant.Attachments.Count);
            Assert.AreEqual(attachment.FileId, participant.Attachments[0].FileId);
            Assert.AreEqual(attachment.Title, participant.Attachments[0].Title);
            Assert.AreEqual(ParticipantStatusCodes.Completed, participant.StatusCodeValue);

            _sqlInitializer.CreatedAttachmentIds.Add(participant.Attachments[0].Id);
        }

        #endregion

        #region Supporting Methods

        private Participant CreateClassroomParticipant(params ParticipantInfoElement[] participantInfoElements)
        {
            var customerArticleId = SqlReadHelper.GetFirstCustomerArticleId(ArticleTypes.Classroom);
            var activityId = SqlReadHelper.GetFirstActivityOnCustomerArticle(customerArticleId);
            if (activityId == 0)
            {
                activityId = SqlWriteHelper.InsertActivity(customerArticleId);
                _sqlInitializer.CreatedActivityIds.Add(activityId);
            }

            return CreateParticipant(activityId, participantInfoElements);
        }

        private Participant CreateParticipant(int? activityId = null, params ParticipantInfoElement[] participantInfoElements)
        {
            var participant = GetValidParticipant(activityId, participantInfoElements);
            var request = CreateWebRequest("participants", "POST", participant);
            using (var response = GetWebResponse(request))
            {
                var created = GetResponseContent<Participant>(response);
                _sqlInitializer.CreatedParticipantIds.Add(created.Id);
                return created;
            }
        }

        private enum LeadingTexts
        {
            Customfield1 = 11,
            Customfield2 = 12,
            EmployeeId = 13,
            Department = 14,
            Position = 15,
            Supervisor = 16,
            ConfirmationEmail = 17
        }

        private ParticipantInfoElement GetValidParticipantInfoElement(string infoText, LeadingTexts leadingText)
        {
            return new ParticipantInfoElement
            {
                LeadingTextId = (int)leadingText,
                InfoText = infoText
            };
        }
        
        private Participant GetValidParticipant(int? activityId = null, params ParticipantInfoElement[] participantInfoElements)
        {
            activityId = activityId ?? SqlWriteHelper.InsertActivity(_sqlInitializer.CreatedCustomerArticleIds.First());
            _sqlInitializer.CreatedActivityIds.Add(activityId.Value);

            var activitySetId = _sqlInitializer.CreatedActivitySetIds.First();
            var userId = SqlReadHelper.GetFirstUserId();

            return new Participant
            {
                UserId = userId,
                CustomerId = SqlReadHelper.GetFirstCustomerId(),
                ActivityId = activityId.Value,
                ActivitySetId = activitySetId,
                StatusCodeValue = ParticipantStatusCodes.Enrolled,
                EarliestInvoiceDate = DateTime.UtcNow,
                ParticipantInfoElements = participantInfoElements.Any() ? participantInfoElements.ToList() : GetDummyParticipantInfoElements(SqlReadHelper.GetFirstCustomerId()).ToList(),
                UnitPrice = 1000,
                CurrencyCodeId = 262,
                EnrollmentId = SqlReadHelper.GetFirstEnrollmentIdForUser(userId, activitySetId),
                Created = DateTime.UtcNow
            };
        }

        private ParticipantInfoElement[] GetDummyParticipantInfoElements(int customerId)
        {
            int startLeadingTextId = 11;
            
            int minId = SqlClient.ExecuteScalar(string.Format("select min(id) from CustomLeadingTexts where CustomerId = {0}", customerId));
            int maxId = SqlClient.ExecuteScalar(string.Format("select max(id) from CustomLeadingTexts where CustomerId = {0}", customerId));

            if (maxId - minId != 6)
                throw new Exception(string.Format("Customleadingtexts not properly configured for customer with id={0}", customerId));

            var elements = new List<ParticipantInfoElement>();

            elements.Add(new ParticipantInfoElement { LeadingTextId = startLeadingTextId, InfoText = "" });
            elements.Add(new ParticipantInfoElement { LeadingTextId = startLeadingTextId + 1, InfoText = "" });
            elements.Add(new ParticipantInfoElement { LeadingTextId = startLeadingTextId + 2, InfoText = "" });
            elements.Add(new ParticipantInfoElement { LeadingTextId = startLeadingTextId + 3, InfoText = "" });
            elements.Add(new ParticipantInfoElement { LeadingTextId = startLeadingTextId + 4, InfoText = "" });
            elements.Add(new ParticipantInfoElement { LeadingTextId = startLeadingTextId + 5, InfoText = "" });
            elements.Add(new ParticipantInfoElement { LeadingTextId = startLeadingTextId + 6, InfoText = "" });

            return elements.ToArray();
        }

        private ActivitySet GetActivitySetFromService(int? id = null)
        {
            if (id == null)
            {
                id = SqlWriteHelper.InsertActivitySet(_sqlInitializer.CreatedCustomerIds.First());
                _sqlInitializer.CreatedActivitySetIds.Add(id.Value);
            }
            var request = CreateWebRequest("activitysets/" + id, "GET");
            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<ActivitySet>(response);
            }
        }

        private Activity GetActivityFromService(int? id = null)
        {
            if (id == null)
            {
                id = SqlWriteHelper.InsertActivity(_sqlInitializer.CreatedCustomerArticleIds.First());
                _sqlInitializer.CreatedActivityIds.Add(id.Value);
            }
            var request = CreateWebRequest("activities/" + id, "GET");
            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<Activity>(response);
            }
        }

        private Participant GetParticipantFromService(int id)
        {
            var request = CreateWebRequest("participants/" + id, "GET");
            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<Participant>(response);
            }
        }

        private Activity UpdateActivity(Activity activity)
        {
            var request = CreateWebRequest("activities/" + activity.Id, "PUT", activity);
            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<Activity>(response);
            }
        }

        private OrderInfo GetValidOrder(Participant participant, bool? isCreditNote = false)
        {
            var customerId = participant.CustomerId;
            var userId = _sqlInitializer.CreatedUserIds.First();

            return new OrderInfo
            {
                Title = "TestOrder",
                CoordinatorId = userId,
                CustomerId = customerId,
                OrderNumber = 3000000,
                OurRef = "TestOrder OurRef",
                YourOrder = "TestOrder your order",
                YourRef = "Test Your Ref",
                InvoiceNumber = 12345,
                IsCreditNote = isCreditNote.Value,
                Participants = new List<int> { participant.Id }
            };
        }

        private OrderInfo CreateOrder(OrderInfo order)
        {
            var request = CreateWebRequest("orders/", "POST", order);
            using (var response = GetWebResponse(request))
            {
                var createdOrder = GetResponseContent<OrderInfo>(response);
                _sqlInitializer.CreatedOrderIds.Add(createdOrder.Id);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                return createdOrder;
            }
        }

        #endregion
    }
}