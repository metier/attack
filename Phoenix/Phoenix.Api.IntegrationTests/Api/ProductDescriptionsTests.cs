﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Metier.Phoenix.Core.Data.Entities;
using Newtonsoft.Json;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class ProductDescriptionsTests : AuthorizedApiTestBase
    {
        [Test]
        public void GetProductDescription_UsingValidId_ReturnsSingleProductDescription()
        {
            var productDescriptionId = SqlReadHelper.GetFirstProductDescriptionId();
            var request = CreateWebRequest("productdescriptions/" + productDescriptionId, "GET");

            using (var response = GetWebResponse(request))
            {
                var responseValue = GetResponseString(response);
                var result = JsonConvert.DeserializeObject<ProductDescription>(responseValue);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(productDescriptionId, result.Id);
            }         
        }

        [Test]
        public void GetProductDescription_UsingNotExistingId_ReturnsNotFoundStatusCode()
        {
            const int id = Int32.MaxValue;

            var request = CreateWebRequest("productdescriptions/" + id, "GET");

            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
            }
        }

        [Test]
        public void CreateProductDescription_CreatingProductDescriptionForExistingProduct_CreatesProductDescription()
        {
            var productDescription = CreateValidProductDescription();
            productDescription = PostProductDescription(productDescription);
            _sqlInitializer.CreatedProductDescriptionIds.Add(productDescription.Id);
            Assert.GreaterOrEqual(productDescription.Id, 0);
        }

        [Test]
        public void CreateProductDescription_CreatingProductDescriptionTwiceForSameProductAndCustomerAndLanguage_ShouldReturnBadRequest()
        {
            var productDescription = CreateValidProductDescription();
            
            productDescription = PostProductDescription(productDescription);
            _sqlInitializer.CreatedProductDescriptionIds.Add(productDescription.Id);

            var productDescription2 = CreateValidProductDescription();
            productDescription2.LanguageId = productDescription.LanguageId;
            productDescription2.ProductId = productDescription.ProductId;
            productDescription2.CustomerId = productDescription.CustomerId;
                
            var request = CreateWebRequest("productdescriptions/", "POST", productDescription2);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }
        
        [Test]
        public void UpdateProductDescription_ChangingExistingProductDescription_UpdatesValues()
        {
            var productDescription = GetProductDescription(_sqlInitializer.CreatedProductDescriptionIds.First());
            productDescription.Title = "Changed product description title";
            
            var request = CreateWebRequest("productdescriptions/" + productDescription.Id, "PUT", productDescription);

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                var responseValue = GetResponseString(response);
                var responseProductDescription = JsonConvert.DeserializeObject<ProductDescription>(responseValue);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(productDescription.Id, responseProductDescription.Id);
                Assert.AreEqual(productDescription.Title, responseProductDescription.Title);
            }
        }

        [Test]
        public void UpdateProductDescription_GivenInvalidProductDescription_MustReturnBadRequest()
        {
            var productDescription = GetProductDescription(_sqlInitializer.CreatedProductDescriptionIds.First());
            // Make product description invalid
            productDescription.Language = null;  
            productDescription.LanguageId = 999999;

            var request = CreateWebRequest("productdescriptions/" + productDescription.Id, "PUT", productDescription);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        [Test]
        public void UpdateProductDescription_GivenOldCustomer_MustReturnConflict()
        {
            var productDescription = GetProductDescription(_sqlInitializer.CreatedProductDescriptionIds.First());
            var staleproductDescription = GetProductDescription(productDescription.Id);

            staleproductDescription.Title = "First change";

            var request = CreateWebRequest("productdescriptions/" + staleproductDescription.Id, "PUT", staleproductDescription);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }

            staleproductDescription.Title = "Second change without refresh";

            request = CreateWebRequest("productdescriptions/" + staleproductDescription.Id, "PUT", staleproductDescription);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.Conflict, response.StatusCode);
            }
        }

        private ProductDescription GetProductDescription(int id)
        {
            var request = CreateWebRequest("productdescriptions/" + id, "GET");

            using (var response = GetWebResponse(request))
            {
                var responseValue = GetResponseString(response);
                return JsonConvert.DeserializeObject<ProductDescription>(responseValue);
            }
        }

        private ProductDescription PostProductDescription(ProductDescription data)
        {
            var request = CreateWebRequest("productdescriptions/", "POST", data);
            ProductDescription productDescription;

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                var responseValue = GetResponseString(response);
                productDescription = JsonConvert.DeserializeObject<ProductDescription>(responseValue);
            }
            return productDescription;
        }

        private ProductDescription CreateValidProductDescription()
        {
            var productId = _sqlInitializer.ProductIdEmpty;
            var templatePartId = SqlReadHelper.GetFirstTemplatePartId(productId);
            var languageId = SqlReadHelper.GetFirstLanguageId();
            var customerId = SqlReadHelper.GetFirstCustomerId();

            var productDescription = new ProductDescription
            {
                ProductId = productId,
                LanguageId = languageId,
                CustomerId = customerId,
                Title = "Test product description",
                ProductDescriptionTexts = new List<ProductDescriptionText>
                {
                    new ProductDescriptionText
                    {
                        Text = "Testing a product description",
                        ProductTemplatePartId = templatePartId
                    }
                }
            };
            return productDescription;
        }
    }
}