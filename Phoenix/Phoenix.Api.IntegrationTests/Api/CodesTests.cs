﻿using System.Collections.Generic;
using System.Net;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Facade.Codes;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class CodesTests : AuthorizedApiTestBase
    {
        // GET: codes/:id
        [Test]
        [TestCase(CodeTypes.ArticleStatuses)]
        [TestCase(CodeTypes.CountryCodes)]
        [TestCase(CodeTypes.CurrencyCodes)]
        public void GetCodes_MustReturnListOfCodes(string codeType)
        {
            var request = CreateWebRequest("codes?type=" + codeType, "GET");
            using (var response = GetWebResponse(request))
            {
                var responseValue = GetResponseString(response);
                var result = JsonConvert.DeserializeObject<List<Code>>(responseValue);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.IsNotNull(result);
                Assert.IsTrue(result.Count > 0);
            }
        }
    }
}