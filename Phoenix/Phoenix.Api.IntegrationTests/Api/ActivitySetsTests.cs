﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Facade.Participant;
using Metier.Phoenix.Api.Facade.Enrollment;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    public class ActivitySetsTests : AuthorizedApiTestBase
    {

        [Test]
        public void GetActivitySet_GivenValidId_ShouldReturnActivitySet()
        {
            var activitySetId = _sqlInitializer.CreatedActivitySetIds.First();
            var request = CreateWebRequest("activitysets/" + activitySetId, "GET");
            using (var response = GetWebResponse(request))
            {
                var activitySet = GetResponseContent<ActivitySet>(response);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(activitySetId, activitySet.Id);
            }
        }

        [Test]
        public void GetActivitySet_GivenInvalidId_ShouldReturnNotFoundStatusCode()
        {
            const int activitySetId = Int32.MaxValue;
            var request = CreateWebRequest("activitysets/" + activitySetId, "GET");
            using (var response = GetWebResponse(request))
            {
                GetResponseContent<ActivitySet>(response);
                Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
            }
        }

        [Test]
        public void GetActivitySetsByCustomerId_GivenValidCustomerId_ShouldReturnListOfActivitySets()
        {
            int customerId;
            var activitySetId = _sqlInitializer.CreatedActivitySetIds.First();
            var request = CreateWebRequest("activitysets/" + activitySetId, "GET");
            using (var response = GetWebResponse(request))
            {
                var activitySet = GetResponseContent<ActivitySet>(response);
                customerId = activitySet.CustomerId;
            }

            request = CreateWebRequest("activitysets?customerId=" + customerId, "GET");
            using (var response = GetWebResponse(request))
            {
                var activitySets = GetResponseContent<PartialList<ActivitySet>>(response);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.Greater(activitySets.Items.Count(), 0);
            }
        }

        [Test]
        public void GetActivitySetsByCustomerId_GivenCourseCalendarProperty_ShouldIncludePublishedWithCurrentTimeframe()
        {
            var activitySet = GetActivitySetFromService();
            activitySet.IsPublished = true;
            activitySet.PublishFrom = DateTime.UtcNow - TimeSpan.FromDays(2);
            activitySet.PublishTo = DateTime.UtcNow + TimeSpan.FromDays(2);

            var request = CreateWebRequest("activitysets/" + activitySet.Id, "PUT", activitySet);
            using (GetWebResponse(request)) { }

            request = CreateWebRequest(string.Format("activitysets?customerId={0}&courseCalendar=true", activitySet.CustomerId), "GET");
            using (var response = GetWebResponse(request))
            {
                var activitySets = GetResponseContent<IEnumerable<ActivitySet>>(response).ToList();

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.IsTrue(activitySets.Any(a => a.Id == activitySet.Id));
            }
        }

        [Test]
        public void GetActivitySetsByCustomerId_GivenCourseCalendarProperty_ShouldIncludePublishedWithOpenEndedEndDate()
        {
            var activitySet = GetActivitySetFromService();
            activitySet.IsPublished = true;
            activitySet.PublishFrom = DateTime.UtcNow - TimeSpan.FromDays(2);
            activitySet.PublishTo = null;

            var request = CreateWebRequest("activitysets/" + activitySet.Id, "PUT", activitySet);
            using (GetWebResponse(request)) { }

            request = CreateWebRequest(string.Format("activitysets?customerId={0}&courseCalendar=true", activitySet.CustomerId), "GET");
            using (var response = GetWebResponse(request))
            {
                var activitySets = GetResponseContent<IEnumerable<ActivitySet>>(response).ToList();

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.IsTrue(activitySets.Any(a => a.Id == activitySet.Id));
            }
        }

        [Test]
        public void GetActivitySetsByCustomerId_GivenCourseCalendarProperty_ShouldNotIncludeNotPublishedWithCurrentTimeframe()
        {
            var activitySet = GetActivitySetFromService();
            activitySet.IsPublished = false;
            activitySet.PublishFrom = DateTime.UtcNow - TimeSpan.FromDays(2);
            activitySet.PublishTo = DateTime.UtcNow + TimeSpan.FromDays(2);

            var request = CreateWebRequest("activitysets/" + activitySet.Id, "PUT", activitySet);
            using (GetWebResponse(request)) { }

            request = CreateWebRequest(string.Format("activitysets?customerId={0}&courseCalendar=true", activitySet.CustomerId), "GET");
            using (var response = GetWebResponse(request))
            {
                var activitySets = GetResponseContent<IEnumerable<ActivitySet>>(response).ToList();

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.IsFalse(activitySets.Any(a => a.Id == activitySet.Id));
            }
        }

        [Test]
        public void GetActivitySetsByCustomerId_GivenCourseCalendarProperty_ShouldNotIncludePublishedWithFutureTimeframe()
        {
            var activitySet = GetActivitySetFromService();
            activitySet.IsPublished = true;
            activitySet.PublishFrom = DateTime.UtcNow + TimeSpan.FromDays(2);
            activitySet.PublishTo = DateTime.UtcNow + TimeSpan.FromDays(4);

            var request = CreateWebRequest("activitysets/" + activitySet.Id, "PUT", activitySet);
            using (GetWebResponse(request)) { }

            request = CreateWebRequest(string.Format("activitysets?customerId={0}&courseCalendar=true", activitySet.CustomerId), "GET");
            using (var response = GetWebResponse(request))
            {
                var activitySets = GetResponseContent<IEnumerable<ActivitySet>>(response).ToList();

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.IsFalse(activitySets.Any(a => a.Id == activitySet.Id));
            }
        }

        [Test]
        public void GetActivitySetsByCustomerId_GivenCourseCalendarProperty_ShouldNotIncludePublishedWithPastTimeframe()
        {
            var activitySet = GetActivitySetFromService();
            activitySet.IsPublished = true;
            activitySet.PublishFrom = DateTime.UtcNow - TimeSpan.FromDays(4);
            activitySet.PublishTo = DateTime.UtcNow - TimeSpan.FromDays(2);

            var request = CreateWebRequest("activitysets/" + activitySet.Id, "PUT", activitySet);
            using (GetWebResponse(request)) { }

            request = CreateWebRequest(string.Format("activitysets?customerId={0}&courseCalendar=true", activitySet.CustomerId), "GET");
            using (var response = GetWebResponse(request))
            {
                var activitySets = GetResponseContent<IEnumerable<ActivitySet>>(response).ToList();

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.IsFalse(activitySets.Any(a => a.Id == activitySet.Id));
            }
        }

        [Test]
        public void CreateActivitySet_GivenValidActivitySet_ShouldCreateActivitySet()
        {
            var activitySet = GetValidActivitySet();
            var request = CreateWebRequest("activitysets/", "POST", activitySet);
            using (var response = GetWebResponse(request))
            {
                var createdActivitySet = GetResponseContent<ActivitySet>(response);

                _sqlInitializer.CreatedActivitySetIds.Add(createdActivitySet.Id);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
                Assert.Greater(createdActivitySet.Id, 0);
            }
        }

        [Test]
        public void CreateActivity_GivenValidActivitySetWithExistingActivity_ShouldIgnoreActivity()
        {
            var activity = GetActivityFromService();
            var activitySet = GetValidActivitySet();
            activitySet.Activities= new List<Activity> { activity };
            var request = CreateWebRequest("activitysets/", "POST", activitySet);
            using (var response = GetWebResponse(request))
            {
                var createdActivitySet = GetResponseContent<ActivitySet>(response);

                _sqlInitializer.CreatedActivitySetIds.Add(createdActivitySet.Id);
                Assert.AreEqual(0, createdActivitySet.Activities.Count);
            }
        }
        
        [Test]
        public void CreateActivitySet_GivenInvalidActivitySet_ShouldReturnBadRequestStatusCode()
        {
            var activitySet = GetValidActivitySet();
            activitySet.EnrollmentFrom = null;
            var request = CreateWebRequest("activitysets/", "POST", activitySet);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        [Test]
        public void CreateActivitySet_GivenValidActivitySetWithEnrollments_ShouldIgnoreEnrollments()
        {
            var activitySet = GetValidActivitySet();
            activitySet.Enrollments.Add(GetValidEnrollment());
            
            var request = CreateWebRequest("activitysets/", "POST", activitySet);
            using (var response = GetWebResponse(request))
            {
                var createdActivitySet = GetResponseContent<ActivitySet>(response);

                _sqlInitializer.CreatedActivitySetIds.Add(createdActivitySet.Id);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
                Assert.AreEqual(0, createdActivitySet.Enrollments.Count);
            }
        }

        [Test]
        public void UpdateActivitySet_GivenExistingValidActivitySet_ShouldUpdateActivitySet()
        {
            var existingActivitySet = GetActivitySetFromService();
            var updatedName = Guid.NewGuid().ToString();
            existingActivitySet.Name = updatedName;
            var request = CreateWebRequest("activitysets/" + existingActivitySet.Id, "PUT", existingActivitySet);
            using (var response = GetWebResponse(request))
            {
                var updated = GetActivitySetFromService(existingActivitySet.Id);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(updatedName, updated.Name);
            }
        }

        [Test]
        public void UpdateActivitySet_GivenInvalidActivitySet_ShouldReturnBadRequest()
        {
            var existingActivitySet = GetActivitySetFromService();
            existingActivitySet.EnrollmentFrom = null;
            var request = CreateWebRequest("activitysets/" + existingActivitySet.Id, "PUT", existingActivitySet);
            using (var response = GetWebResponse(request))
            {
                var updated = GetActivitySetFromService(existingActivitySet.Id);
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
                Assert.AreEqual(existingActivitySet.RowVersion, updated.RowVersion);
            }
        }

        [Test]
        public void UpdateActivitySet_GivenOldActivitySet_ShouldReturnConflict()
        {
            var existingActivitySet = GetActivitySetFromService();

            existingActivitySet.Name = Guid.NewGuid().ToString();
            var request = CreateWebRequest("activitysets/" + existingActivitySet.Id, "PUT", existingActivitySet);
            var response = GetWebResponse(request);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            existingActivitySet.Name = Guid.NewGuid().ToString();
            request = CreateWebRequest("activitysets/" + existingActivitySet.Id, "PUT", existingActivitySet);
            response = GetWebResponse(request);
            Assert.AreEqual(HttpStatusCode.Conflict, response.StatusCode);
        }

        [Test]
        public void UpdateActivitySet_GivenActivitySetWithAddedActivity_ShouldIgnoreAddedActivity()
        {
            var activitySet = GetActivitySetFromService();
            var activity = GetActivityFromService();
            activitySet.Activities.Add(activity);

            var updated = UpdateActivitySet(activitySet);

            Assert.AreEqual(0, updated.Activities.Count);
        }

        [Test]
        public void UpdateActivitySet_GivenActivitySetWithDeletedActivity_ShouldIgnoreDeletedActivity()
        {
            var activitySet = GetActivitySetFromService();
            var activity = GetActivityFromService();
            activitySet = AddActivityToActivitySet(activitySet.Id, activity.Id);
            activitySet.Activities.RemoveAt(0);

            var updated = UpdateActivitySet(activitySet);

            Assert.AreEqual(1, updated.Activities.Count);
        }

        [Test]
        public void UpdateActivitySet_GivenActivitySetWithAddedEnrollment_ShouldIgnoreAddedEnrollment()
        {
            var activitySet = GetActivitySetFromService();
            activitySet.Enrollments.Add(GetValidEnrollment());

            var updated = UpdateActivitySet(activitySet);

            Assert.AreEqual(0, updated.Enrollments.Count);
        }

        [Test]
        public void UpdateActivitySet_GivenActivitySetWithDeletedEnrollment_ShouldIgnoreDeletedEnrollment()
        {
            var activitySet = GetActivitySetFromService();
            activitySet = EnrollUser(activitySet.Id);
            activitySet.Enrollments.RemoveAt(0);

            var updated = UpdateActivitySet(activitySet);

            Assert.AreEqual(1, updated.Enrollments.Count);
        }

        [Test]
        public void Enroll_EnrollingAsLearningPortalUserForOtherUser_ShouldReturnOk()
        {
            var activitySet = GetActivitySetFromService();
            var username = _sqlInitializer.IntegrationTestLearningPortalTokenUserName;
            var otherUserId = SqlReadHelper.GetUserIdForUsername(_sqlInitializer.IntegrationTestPhoenixUserName);

            LogOn(username);

            var request = CreateWebRequest("activitysets/" + activitySet.Id + "/enroll?userId=" + otherUserId, "PUT");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }
        }

        [Test]
        public void Enroll_EnrollingAsLearningPortalUserForSelfUser_ShouldEnroll()
        {
            var activitySet = GetActivitySetFromService();
            var username = _sqlInitializer.IntegrationTestLearningPortalTokenUserName;
            var userId = SqlReadHelper.GetUserIdForUsername(username);

            LogOn(username);

            var request = CreateWebRequest("activitysets/" + activitySet.Id + "/enroll?userId=" + userId, "PUT");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }
        }

        [Test]
        public void Enroll_ShouldAddEnrollments()
        {
            var activitySet = GetActivitySetFromService();
            activitySet = EnrollUser(activitySet.Id);
            
            Assert.AreEqual(1, activitySet.Enrollments.Count);
        }

        [Test]
        public void Enroll_ShouldSetCustomParticipantPrice_WhenCustomParticipantPriceIsSpecified()
        {
            // Create an activity sets with two activities
            var activitySet = GetActivitySetFromService();
            
            var userId = _sqlInitializer.CreateUser();
            
            var activity1 = GetActivityFromService();
            var activity2 = GetActivityFromService();
            var activity1OverridePrice = 987654;

            AddActivityToActivitySet(activitySet.Id, activity1.Id);
            AddActivityToActivitySet(activitySet.Id, activity2.Id);

            var enrollmentDetails = new EnrollmentDetails {
                 ParticipantPayments=new List<ParticipantPayment> {
                     new ParticipantPayment
                     {
                         ActivityId = activity1.Id,
                         Price = activity1OverridePrice
                     }
                 }
            };

            activitySet = EnrollUser(activitySet.Id, userId, enrollmentDetails);

            activity1 = GetActivityFromService(activity1.Id);
            activity2 = GetActivityFromService(activity2.Id);

            //_sqlInitializer.CreatedParticipantIds.AddRange(activity.Participants.Select(p => p.Id));

            // Assert that price is correct for activity1
            Assert.AreEqual(activity1OverridePrice, activity1.Participants.Find(x => x.UserId == userId).UnitPrice);
            Assert.AreEqual(activity2.UnitPrice, activity2.Participants.Find(x => x.UserId == userId).UnitPrice);

        }

        [Test]
        public void Enroll_ShouldSetDefaultActivityPrice_WhenNoCustomParticipantPriceIsSpecified()
        {
            // Create an activity sets with two activities
            var activitySet = GetActivitySetFromService();

            var userId = _sqlInitializer.CreateUser();

            var activity1 = GetActivityFromService();
            var activity2 = GetActivityFromService();

            AddActivityToActivitySet(activitySet.Id, activity1.Id);
            AddActivityToActivitySet(activitySet.Id, activity2.Id);
            
            activitySet = EnrollUser(activitySet.Id, userId, null);

            activity1 = GetActivityFromService(activity1.Id);
            activity2 = GetActivityFromService(activity2.Id);

            //_sqlInitializer.CreatedParticipantIds.AddRange(activity.Participants.Select(p => p.Id));

            // Assert that price is correct for activity1
            Assert.AreEqual(activity1.UnitPrice, activity1.Participants.Find(x => x.UserId == userId).UnitPrice);
            Assert.AreEqual(activity2.UnitPrice, activity2.Participants.Find(x => x.UserId == userId).UnitPrice);

        }

        //[Test]
        //public void Enroll_TestWithPaymentReference()
        //{
        //    var url = "activitysets/" + 7782 + "/enroll?userId=" + 921 + "&paymentReference=testing ordering";
        //    var request = CreateWebRequest(url, "PUT");
        //    using (var response = GetWebResponse(request))
        //    {
        //        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        //        var content = GetResponseContent<ActivitySet>(response);
        //    }
        //}

        [Test]
        public void Enroll_ShouldCreateParticipantsForAllActivities()
        {
            // Create initial activity set with one activity
            var activitySet = GetActivitySetFromService();
            var activity1 = GetActivityFromService();
            var activity2 = GetActivityFromService();
            activitySet = AddActivityToActivitySet(activitySet.Id, activity1.Id);
            activitySet = AddActivityToActivitySet(activitySet.Id, activity2.Id);
            
            // Enroll one user
            var userId = _sqlInitializer.CreateUser();
            activitySet = EnrollUser(activitySet.Id, userId);
            
            // Retrieve activities
            activity1 = GetActivityFromService(activity1.Id);
            activity2 = GetActivityFromService(activity2.Id);
            _sqlInitializer.CreatedParticipantIds.AddRange(activity1.Participants.Select(p => p.Id));
            _sqlInitializer.CreatedParticipantIds.AddRange(activity2.Participants.Select(p => p.Id));

            // Assert that user was enrolled to activitis
            Assert.AreEqual(1, activity1.Participants.Count);
            Assert.AreEqual(ParticipantStatusCodes.Enrolled, activity1.Participants[0].StatusCodeValue);
            Assert.AreEqual(userId, activity1.Participants[0].UserId);
            
            Assert.AreEqual(1, activity2.Participants.Count);
            Assert.AreEqual(ParticipantStatusCodes.Enrolled, activity2.Participants[0].StatusCodeValue);
            Assert.AreEqual(userId, activity2.Participants[0].UserId);
        }

        [Test]
        public void Enroll_ShouldSaveUserInfoElementsFromRequest()
        {
            // Create initial activity set with one activity
            var activitySet = GetActivitySetFromService();
            var activity = GetActivityFromService();
            activitySet = AddActivityToActivitySet(activitySet.Id, activity.Id);

            // Enroll user with one custom info element
            var userId = _sqlInitializer.CreateUser();
            var leadingTextId = 11;
            var customLeadingTextId = _sqlInitializer.CreatedCustomLeadingTextIds.First();
            var customerId = SqlClient.ExecuteScalar("Select customerId from CustomLeadingTexts where id = " + customLeadingTextId);

            const string infoText = "Custom text";
            var enrollmentDetails =
                new EnrollmentDetails
                {
                     InfoElements = new List<UserEnrollInfoElement> { new UserEnrollInfoElement { LeadingTextId = leadingTextId, InfoText = infoText } }
                };
                

            EnrollUser(activitySet.Id, userId, enrollmentDetails, null, customerId);

            // Retrieve activities
            activity = GetActivityFromService(activity.Id);
            _sqlInitializer.CreatedParticipantIds.AddRange(activity.Participants.Select(p => p.Id));

            var participant = GetParticipant(activity.Participants[0].Id);
            var participantInfoElement = participant.ParticipantInfoElements.FirstOrDefault(pie => pie.LeadingTextId == leadingTextId);

            // Assert that user was enrolled to activities
            Assert.GreaterOrEqual(participant.ParticipantInfoElements.Count, 1);
            Assert.IsNotNull(participantInfoElement);
            Assert.AreEqual(infoText, participantInfoElement.InfoText);
        }

        [Test]
        public void Enroll_ShouldCopyInfoElementsFromUserProfile()
        {
            // Create initial activity set with one activity
            var activitySet = GetActivitySetFromService();
            var activity = GetActivityFromService();
            activitySet = AddActivityToActivitySet(activitySet.Id, activity.Id);
            
            // Enroll user with one custom info element
            var userId = _sqlInitializer.CreateUser();

            var leadingTextId = 17;
            var customLeadingTextId = _sqlInitializer.CreatedCustomLeadingTextIds.First();
            var customerId = SqlClient.ExecuteScalar("Select customerId from CustomLeadingTexts where id = " + customLeadingTextId);

            var sql = string.Format(" from UserInfoElements where UserId = {0} and LeadingTextId != {1} order by id", userId, leadingTextId);

            var userInfoElementInfoText = SqlClient.ExecuteScalarReturnString("select top 1 InfoText" + sql);
            var userInfoElementLeadingTextId = SqlClient.ExecuteScalar("select top 1 LeadingTextId" + sql);

            //The user should have at least one userInfoElement where leadingtextId is different from 17
            Assert.IsNotNullOrEmpty(userInfoElementInfoText);

            const string infoText = "Custom text";
            var enrollmentDetails = new EnrollmentDetails
            {
                InfoElements = new List<UserEnrollInfoElement> { new UserEnrollInfoElement { LeadingTextId = leadingTextId, InfoText = infoText } }
            };

            EnrollUser(activitySet.Id, userId, enrollmentDetails, null, customerId);

            // Retrieve activities
            activity = GetActivityFromService(activity.Id);
            _sqlInitializer.CreatedParticipantIds.AddRange(activity.Participants.Select(p => p.Id));

            var participant = GetParticipant(activity.Participants[0].Id);
            var participantInfoElement = participant.ParticipantInfoElements.FirstOrDefault(pie => pie.LeadingTextId == userInfoElementLeadingTextId);

            // Assert that user was enrolled to activities
            Assert.GreaterOrEqual(participant.ParticipantInfoElements.Count, 2);
            Assert.IsNotNull(participantInfoElement);
            Assert.AreEqual(userInfoElementInfoText, participantInfoElement.InfoText);
        }
        
        [Test]
        public void Enroll_ShouldNotCreateMulitplePartcipantsForSameUserAndActivity()
        {
            // Create two activity sets with the same activity
            var activity = GetActivityFromService();
            var activitySet1 = GetActivitySetFromService();
            var activitySet2 = GetActivitySetFromService();
            activitySet1 = AddActivityToActivitySet(activitySet1.Id, activity.Id);
            activitySet2 = AddActivityToActivitySet(activitySet2.Id, activity.Id);
            
            // Enroll user to both activity sets
            var userId = _sqlInitializer.CreateUser();
            activitySet1 = EnrollUser(activitySet1.Id, userId);
            activitySet2 = EnrollUser(activitySet2.Id, userId);
            
            // Retrieve activity
            activity = GetActivityFromService(activity.Id);
            _sqlInitializer.CreatedParticipantIds.AddRange(activity.Participants.Select(p => p.Id));

            // Assert that user was enrolled to activity
            Assert.AreEqual(1, activity.Participants.Count);
            Assert.AreEqual(ParticipantStatusCodes.Enrolled, activity.Participants[0].StatusCodeValue);
            Assert.AreEqual(userId, activity.Participants[0].UserId);
        }

        [Test]
        public void Enroll_ShouldEnrollToMultipleActivitySetsInOneOperation()
        {
            var activity1 = GetActivityFromService();
            var activity2 = GetActivityFromService();
            var activitySet1 = GetActivitySetFromService();
            var activitySet2 = GetActivitySetFromService();
            activitySet1 = AddActivityToActivitySet(activitySet1.Id, activity1.Id);
            activitySet2 = AddActivityToActivitySet(activitySet2.Id, activity2.Id);

            // Enroll user to both activity sets in one operation
            var userId = _sqlInitializer.CreateUser();
            EnrollUserToMultipleActivitySets(new[] { activitySet1.Id, activitySet2.Id }, userId);


            // Retrieve activities
            activity1 = GetActivityFromService(activity1.Id);
            activity2 = GetActivityFromService(activity2.Id);
            _sqlInitializer.CreatedParticipantIds.AddRange(activity1.Participants.Select(p => p.Id));
            _sqlInitializer.CreatedParticipantIds.AddRange(activity2.Participants.Select(p => p.Id));

            // Assert that user was enrolled to activities
            Assert.AreEqual(1, activity1.Participants.Count);
            Assert.AreEqual(ParticipantStatusCodes.Enrolled, activity1.Participants[0].StatusCodeValue);
            Assert.AreEqual(userId, activity1.Participants[0].UserId);

            Assert.AreEqual(1, activity2.Participants.Count);
            Assert.AreEqual(ParticipantStatusCodes.Enrolled, activity2.Participants[0].StatusCodeValue);
            Assert.AreEqual(userId, activity2.Participants[0].UserId);
        }

        [Test]
        public void Unenroll__ShouldCancelEnrollment()
        {
            var activitySet = GetActivitySetFromService();
            var userId = _sqlInitializer.CreateUser();
            activitySet = EnrollUser(activitySet.Id, userId);

            activitySet = UnenrollUser(activitySet.Id, userId);
            Assert.IsTrue(activitySet.Enrollments[0].IsCancelled);
        }

        [Test]
        public void Unenroll_ShouldUnenrollFromAllActivities()
        {
            // Create activity set with two activities
            var activitySet = GetActivitySetFromService();           
            var activity1 = GetActivityFromService();
            activitySet = AddActivityToActivitySet(activitySet.Id, activity1.Id);
            
            // Enroll user to activity set
            var userId = _sqlInitializer.CreateUser();
            activitySet = EnrollUser(activitySet.Id, userId, GetEnrollmentDetails());
            activitySet = UnenrollUser(activitySet.Id, userId);
            
            // Retrieve activities
            activity1 = GetActivityFromService(activity1.Id);
            _sqlInitializer.CreatedParticipantIds.AddRange(activity1.Participants.Select(p => p.Id));

            Assert.AreEqual(0, activity1.Participants.Count);
        }

        [Test]
        public void Unenroll_UnenrollingUserFromActivitySetWith_UnenrollmentDeadline_InThePast_ShouldBeAllowed()
        {
            // Create activityset with one enrollment
            var activitySet = GetActivitySetFromService();
            var userId = _sqlInitializer.CreateUser();
            activitySet = EnrollUser(activitySet.Id, userId);
            
            // Set deadline to yesterday
            activitySet.UnenrollmentDeadline = DateTime.Now.AddDays(-1);
            activitySet.IsUnenrollmentAllowed = true;
            activitySet = UpdateActivitySet(activitySet);

            // Unenroll
            activitySet = UnenrollUser(activitySet.Id, userId);
            Assert.IsTrue(activitySet.Enrollments[0].IsCancelled);
        }

        [Test]
        public void Unenroll_UnenrollingUserFromActivitySetWith_IsUnerollmentAllowed_False_ShouldBeAllowed()
        {
            // Create activityset with one enrollment
            var activitySet = GetActivitySetFromService();
            var userId = _sqlInitializer.CreateUser();
            activitySet = EnrollUser(activitySet.Id, userId);

            // Set deadline to yesterday
            activitySet.UnenrollmentDeadline = DateTime.Now.AddDays(1);
            activitySet.IsUnenrollmentAllowed = false;
            activitySet = UpdateActivitySet(activitySet);

            // Unenroll
            activitySet = UnenrollUser(activitySet.Id, userId);
            Assert.IsTrue(activitySet.Enrollments[0].IsCancelled);
        }

        [Test]
        public void Unenroll_UnenrollingUserFromActivitySetWith_IsUnerollmentAllowed_True_WithoutDate_ShouldBeAllowed()
        {
            // Create activityset with one enrollment
            var activitySet = GetActivitySetFromService();
            var userId = _sqlInitializer.CreateUser();
            activitySet = EnrollUser(activitySet.Id, userId);

            // Set deadline to yesterday
            activitySet.UnenrollmentDeadline = null;
            activitySet.IsUnenrollmentAllowed = true;
            activitySet = UpdateActivitySet(activitySet);

            // Unenroll
            activitySet = UnenrollUser(activitySet.Id, userId);
            Assert.IsTrue(activitySet.Enrollments[0].IsCancelled);
        }

        [Test]
        public void Unenroll_UnenrollingUserAddedToOrder_OrderIsNotCreditNote_ShouldGetValidationError()
        {
            // Create activityset with one enrollment
            var activitySet = GetActivitySetFromService();
            var activity = GetActivityFromService();
            activitySet = AddActivityToActivitySet(activitySet.Id, activity.Id);
            var userId = _sqlInitializer.CreateUser();
            activitySet = EnrollUser(activitySet.Id, userId);

            // Add participant to order
            activity = GetActivityFromService(activitySet.Activities.First().Id);
            var participant = activity.Participants.First(p => p.UserId == userId);
            CreateOrder(GetValidOrder(participant));

            var request = CreateWebRequest("activitysets/" + activitySet.Id + "/unenroll?userId=" + userId, "PUT");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        [Test]
        public void Unenroll_UnenrollingUserAddedToOrder_OrderIsCreditNoteAsDraft_ShouldGetValidationError()
        {
            // Create activityset with one enrollment
            var activitySet = GetActivitySetFromService();
            var activity = GetActivityFromService();
            activitySet = AddActivityToActivitySet(activitySet.Id, activity.Id);
            var userId = _sqlInitializer.CreateUser();
            activitySet = EnrollUser(activitySet.Id, userId);

            // Add participant to order
            activity = GetActivityFromService(activitySet.Activities.First().Id);
            var participant = activity.Participants.First(p => p.UserId == userId);

            var order = CreateOrder(GetValidOrder(participant, true));
            
            var request = CreateWebRequest("activitysets/" + activitySet.Id + "/unenroll?userId=" + userId, "PUT");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        [Test]
        [TestCase(OrderStatus.Transferred)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Invoiced)]
        [TestCase(OrderStatus.Rejected)]
        public void Unenroll_UnenrollingUserAddedToOrder_OrderIsTransferredCreditNote_ShouldAllowUnenrolling(OrderStatus orderStatus)
        {
            // Create activityset with one enrollment
            var activitySet = GetActivitySetFromService();
            var activity = GetActivityFromService();
            activitySet = AddActivityToActivitySet(activitySet.Id, activity.Id);
            var userId = _sqlInitializer.CreateUser();
            activitySet = EnrollUser(activitySet.Id, userId);

            // Add participant to order
            activity = GetActivityFromService(activitySet.Activities.First().Id);
            var participant = activity.Participants.First(p => p.UserId == userId);
            
            var order = CreateOrder(GetValidOrder(participant, true));
            _sqlInitializer.SetOrderStatus(order.Id, orderStatus);

            activitySet = UnenrollUser(activitySet.Id, userId);
            Assert.IsTrue(activitySet.Enrollments[0].IsCancelled);
        }

        [Test]
        public void Unenroll_UnenrollingAsLearningPortalUserForOtherUser_ShouldReturnOK()
        {
            var username = _sqlInitializer.IntegrationTestLearningPortalTokenUserName;
            var userId = SqlReadHelper.GetUserIdForUsername(_sqlInitializer.IntegrationTestLearningPortalTokenUserName);
            var otherUserId = SqlReadHelper.GetUserIdForUsername(_sqlInitializer.IntegrationTestPhoenixUserName);
            
            // Create activityset with one enrollment
            var activitySet = GetActivitySetFromService();
            var activity = GetActivityFromService();
            activitySet = AddActivityToActivitySet(activitySet.Id, activity.Id);
            activitySet = EnrollUser(activitySet.Id, userId);
            
            LogOn(username);

            var request = CreateWebRequest("activitysets/" + activitySet.Id + "/unenroll?userId=" + otherUserId, "PUT");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }
        }

        [Test]
        public void Unenroll_UnenrollingAsLearningPortalUserForSelfUser_ShouldUnenroll()
        {
            var username = _sqlInitializer.IntegrationTestLearningPortalTokenUserName;
            var userId = SqlReadHelper.GetUserIdForUsername(_sqlInitializer.IntegrationTestLearningPortalTokenUserName);
            
            // Create activityset with one enrollment
            var activitySet = GetActivitySetFromService();
            var activity = GetActivityFromService();
            activitySet = AddActivityToActivitySet(activitySet.Id, activity.Id);
            activitySet = EnrollUser(activitySet.Id, userId);

            LogOn(username);

            activitySet = UnenrollUser(activitySet.Id, userId);
            Assert.IsTrue(activitySet.Enrollments[0].IsCancelled);
        }

        [Test]
        public void Unenroll_UnenrollingAsLearningPortalUser_IsUnenrollmentAllowedIsFalse_ShouldReturnBadRequest()
        {
            var username = _sqlInitializer.IntegrationTestLearningPortalTokenUserName;
            var userId = SqlReadHelper.GetUserIdForUsername(username);

            var activitySet = GetActivitySetFromService();
            var activity = GetActivityFromService();
            activitySet.IsUnenrollmentAllowed = false;
            activitySet = UpdateActivitySet(activitySet);
            activitySet = AddActivityToActivitySet(activitySet.Id, activity.Id);
            activitySet = EnrollUser(activitySet.Id, userId);

            LogOn(username);

            var request = CreateWebRequest("activitysets/" + activitySet.Id + "/unenroll?userId=" + userId, "PUT");
            using (var response = GetWebResponse(request))
            {
                var fault = GetResponseContent<Fault>(response);
                Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
                Assert.AreEqual("Unenrollment is not allowed", fault.Message);
            }
        }

        [Test]
        public void Unenroll_UnenrollingAsLearningPortalUser_WithAuthenticaitonToken_ShouldReturnOK()
        {
            const string authenticationTokenUsername = "testuserwithauthtokenforunenrollment";
            var authenticationToken = SqlWriteHelper.InsertMembershipTokenUser(authenticationTokenUsername, PhoenixRoles.LearningPortalTokenUser);
            var otherUserId = SqlReadHelper.GetUserIdForUsername(_sqlInitializer.IntegrationTestPhoenixUserName);

            // Create activityset with one enrollment
            var activitySet = GetActivitySetFromService();
            var activity = GetActivityFromService();
            activitySet = AddActivityToActivitySet(activitySet.Id, activity.Id);
            activitySet = EnrollUser(activitySet.Id, otherUserId);

            _sqlInitializer.CreatedUsernames.Add(authenticationTokenUsername);

            // "Log out"
            AuthCookies = null;

            var request = CreateWebRequest("activitysets/" + activitySet.Id + "/unenroll?userId=" + otherUserId, "PUT");
            request.Headers.Add("PhoenixAuthToken", authenticationToken);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }
        }

        [Test]
        public void Unenroll_UnenrollingAsPhoenixUser_IsUnenrollmentAllowedIsFalse_ShouldUnenroll()
        {
            var username = _sqlInitializer.IntegrationTestLearningPortalTokenUserName;
            var userId = SqlReadHelper.GetUserIdForUsername(username);

            var activitySet = GetActivitySetFromService();
            var activity = GetActivityFromService();
            activitySet.IsUnenrollmentAllowed = false;
            activitySet = UpdateActivitySet(activitySet);
            activitySet = AddActivityToActivitySet(activitySet.Id, activity.Id);
            activitySet = EnrollUser(activitySet.Id, userId);


            activitySet = UnenrollUser(activitySet.Id, userId);

            activitySet = UnenrollUser(activitySet.Id, userId);
            Assert.IsTrue(activitySet.Enrollments[0].IsCancelled);
        }

        [Test]
        public void AddActivity_ShouldAddActivities()
        {
            var activitySet = GetActivitySetFromService();
            var activity = GetActivityFromService();

            var updated = AddActivityToActivitySet(activitySet.Id, activity.Id);
            Assert.AreEqual(1, updated.Activities.Count);
        }

        [Test]
        public void AddActivity_ShouldEnrollExistingUsers()
        {
            // Create activity set with two users enrolled
            var activitySet = GetActivitySetFromService();
            
            activitySet = EnrollUser(activitySet.Id);
            activitySet = EnrollUser(activitySet.Id);
            
            // Add an activity to the activity set
            var activity = GetActivityFromService();
            activitySet = AddActivityToActivitySet(activitySet.Id, activity.Id);
            
            // Retrive activity
            activity = GetActivityFromService(activity.Id);
            _sqlInitializer.CreatedParticipantIds.AddRange(activity.Participants.Select(p => p.Id));

            // Assert that enrolled users is participants in activity
            Assert.AreEqual(2, activity.Participants.Count);
            Assert.AreEqual(ParticipantStatusCodes.Enrolled, activity.Participants[0].StatusCodeValue);
            Assert.AreEqual(ParticipantStatusCodes.Enrolled, activity.Participants[1].StatusCodeValue);
            Assert.AreEqual(activitySet.Enrollments[0].UserId, activity.Participants[0].UserId);
            Assert.AreEqual(activitySet.Enrollments[1].UserId, activity.Participants[1].UserId);
        }

        [Test]
        public void RemoveActivity_ShouldUnenrollAllUsers()
        {
            // Create activity set with one user enrolled to one activity

            var activitySet = GetActivitySetFromService();
            activitySet = EnrollUser(activitySet.Id, enrollmentDetails: GetEnrollmentDetails());
            
            var activity = GetActivityFromService();
            activitySet = AddActivityToActivitySet(activitySet.Id, activity.Id);
            
            // Remove the activity
            RemoveActivityFromActivitySet(activitySet.Id, activity.Id);
            
            activity = GetActivityFromService(activity.Id);
            _sqlInitializer.CreatedParticipantIds.AddRange(activity.Participants.Select(p => p.Id));

            // Assert that the user is unenrolled from the activity
            Assert.AreEqual(0, activity.Participants.Count);
        }

        [Test]
        public void RemoveActivity_ShouldNotUnenrollUsersWithStatusEnrolled_WhenActivityIsInAntoherActivitySetWhereUserIsEnrolled()
        {
            // Create two activity sets with same activity and same user enrolled
            var activitySet1 = GetActivitySetFromService();
            var activitySet2 = GetActivitySetFromService();

            var userId = _sqlInitializer.CreateUser();
            activitySet1 = EnrollUser(activitySet1.Id, userId);
            activitySet2 = EnrollUser(activitySet2.Id, userId);

            var activity = GetActivityFromService();
            AddActivityToActivitySet(activitySet1.Id, activity.Id);
            AddActivityToActivitySet(activitySet2.Id, activity.Id);
            
            // Remove the activity from activity set 1
            RemoveActivityFromActivitySet(activitySet1.Id, activity.Id);
            
            activity = GetActivityFromService(activity.Id);
            _sqlInitializer.CreatedParticipantIds.AddRange(activity.Participants.Select(p => p.Id));

            // Assert that user is still enrolled to the activity
            Assert.AreEqual(1, activity.Participants.Count);
            Assert.AreEqual(ParticipantStatusCodes.Enrolled, activity.Participants[0].StatusCodeValue);
        }

        [Test]
        public void RemoveActivity_ShouldRemoveActivities()
        {
            var activitySet = GetActivitySetFromService();
            var activity = GetActivityFromService();

            activitySet = AddActivityToActivitySet(activitySet.Id, activity.Id);
            Assert.AreEqual(1, activitySet.Activities.Count);

            activitySet = RemoveActivityFromActivitySet(activitySet.Id, activity.Id);
            Assert.AreEqual(0, activitySet.Activities.Count);
        }

        [Test]
        public void RemoveActivity_ShouldNotDeleteActivity()
        {
            var activitySet = GetActivitySetFromService();
            var activity = GetActivityFromService();

            AddActivityToActivitySet(activitySet.Id, activity.Id);
            RemoveActivityFromActivitySet(activitySet.Id, activity.Id);
            
            var activityAfterActivitySetRemoval = GetActivityFromService(activity.Id);
            Assert.AreEqual(activity.Id, activityAfterActivitySetRemoval.Id);
        }

        [Test]
        public void Share_ShouldShareWithCustomer()
        {
            var activitySet = GetActivitySetFromService();
            var customerId = _sqlInitializer.CreatedCustomerIds.Last();
            var request = CreateWebRequest("activitysets/" + activitySet.Id + "/share/" + customerId, "PUT");
            using (var response = GetWebResponse(request))
            {
                var result = GetResponseContent<ActivitySet>(response);
                Assert.AreEqual(1, result.ActivitySetShares.Count);
                Assert.AreEqual(customerId, result.ActivitySetShares[0].CustomerId);
            }
        }

        [Test]
        public void Share_UpdateShouldNotChangeShares()
        {
            var activitySet = GetActivitySetFromService();
            var customerId = _sqlInitializer.CreatedCustomerIds.Last();
            var request = CreateWebRequest("activitysets/" + activitySet.Id + "/share/" + customerId, "PUT");
            using (var response = GetWebResponse(request))
            {
                var result = GetResponseContent<ActivitySet>(response);
                result.ActivitySetShares.Clear();
                result = UpdateActivitySet(result);

                Assert.AreEqual(1, result.ActivitySetShares.Count);
                Assert.AreEqual(customerId, result.ActivitySetShares[0].CustomerId);
            }
        }

        private EnrollmentDetails GetEnrollmentDetails(string infoText = "some text")
        {
            return new EnrollmentDetails
            {
                InfoElements = new List <UserEnrollInfoElement>
            {
                    new UserEnrollInfoElement { LeadingTextId = 11, InfoText = infoText }
                }
            };
        }
                
        private ActivitySet EnrollUser(int activitySetId, int? userId = null, EnrollmentDetails enrollmentDetails = null, string paymentReference = null, int? customerId = null )
        {
            if (userId == null)
            {
                userId = _sqlInitializer.CreateUser();
            }
            var url = "activitysets/" + activitySetId + "/enroll?userId=" + userId.Value;

            if (!string.IsNullOrEmpty(paymentReference))
            {
                url = url + "&paymentReference=" + paymentReference;
            }

            if (customerId != null)
            {
                url = url + "&customerId=" + customerId;
            }

            var request = CreateWebRequest(url, "PUT", enrollmentDetails);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                return GetResponseContent<ActivitySet>(response);
            }
        }

        private IEnumerable<ActivitySet> EnrollUserToMultipleActivitySets(int[] activitySetId, int? userId = null, EnrollmentDetails enrollmentDetails = null, string paymentReference = null)
        {
            if (userId == null)
            {
                userId = _sqlInitializer.CreateUser();
            }
            var url = "activitysets/enroll?userId=" + userId.Value;
            if (!string.IsNullOrEmpty(paymentReference))
            {
                url = url + "&paymentReference=" + paymentReference;
            }
            foreach (var id in activitySetId)
            {
                url = url + "&activitySetId=" + id;
            }
            var request = CreateWebRequest(url, "PUT", enrollmentDetails);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                return GetResponseContent<IEnumerable<ActivitySet>>(response);
            }
        }
        
        private ActivitySet UnenrollUser(int activitySetId, int userId)
        {
            var request = CreateWebRequest("activitysets/" + activitySetId + "/unenroll?userId=" + userId, "PUT");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                return GetResponseContent<ActivitySet>(response);
            }
        }


        private ActivitySet AddActivityToActivitySet(int activitySetId, int activityId)
        {
            var request = CreateWebRequest("activitysets/" + activitySetId + "/addactivity?activityId=" + activityId, "PUT");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                return GetResponseContent<ActivitySet>(response);
            }
        }
        
        private ActivitySet RemoveActivityFromActivitySet(int activitySetId, int activityId)
        {
            var request = CreateWebRequest("activitysets/" + activitySetId + "/removeactivity?activityId=" + activityId, "PUT");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                return GetResponseContent<ActivitySet>(response);
            }
        }

        private ActivitySet UpdateActivitySet(ActivitySet activitySet)
        {
            var request = CreateWebRequest("activitysets/" + activitySet.Id, "PUT", activitySet);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                return GetResponseContent<ActivitySet>(response);
            }
        }
        
        private Enrollment GetValidEnrollment(int? userId = null)
        {
            if (userId == null)
            {
                userId = _sqlInitializer.CreateUser();
            }
            return new Enrollment
            {
                UserId = userId.Value
            };
        }

        private ActivitySet GetActivitySetFromService(int? id = null)
        {
            if (id == null)
            {
                id = SqlWriteHelper.InsertActivitySet(_sqlInitializer.CreatedCustomerIds.First());
                _sqlInitializer.CreatedActivitySetIds.Add(id.Value);
            }
            var request = CreateWebRequest("activitysets/" + id, "GET");
            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<ActivitySet>(response);
            }
        }

        private Activity GetActivityFromService(int? id = null)
        {
            if (id == null)
            {
                id = SqlWriteHelper.InsertActivity(_sqlInitializer.CreatedCustomerArticleIds.First());
                _sqlInitializer.CreatedActivityIds.Add(id.Value);
            }
            var request = CreateWebRequest("activities/" + id, "GET");
            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<Activity>(response);
            }
        }

        private Participant GetParticipant(int id)
        {
            var request = CreateWebRequest("participants/" + id, "GET");
            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<Participant>(response);
            }
        }

        private PartialList<SearchableParticipant> GetParticipantsForUserFromService(int userId)
        {
            var request = CreateWebRequest("participants?userId=" + userId, "GET");
            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<PartialList<SearchableParticipant>>(response);
            }
        }

        private ActivitySet GetValidActivitySet()
        {
            var customerId = _sqlInitializer.CreatedCustomerIds.First();
            return new ActivitySet
            {
                Name = "TestActivitySet_" + Guid.NewGuid(),
                CustomerId = customerId,
                IsPublished = true,
                PublishFrom = DateTime.UtcNow,
                PublishTo = DateTime.UtcNow + TimeSpan.FromDays(100),
                EnrollmentFrom = DateTime.UtcNow + TimeSpan.FromDays(7),
                EnrollmentTo = DateTime.UtcNow + TimeSpan.FromDays(21),
                IsUnenrollmentAllowed = true,
                UnenrollmentDeadline = DateTime.UtcNow + TimeSpan.FromDays(28)
            };
        }

        private OrderInfo GetValidOrder(Participant participant, bool? isCreditNote = false)
        {
            var customerId = participant.CustomerId;
            var userId = _sqlInitializer.CreatedUserIds.First();

            return new OrderInfo
            {
                Title = "TestOrder",
                CoordinatorId = userId,
                CustomerId = customerId,
                OrderNumber = 3000000,
                OurRef = "TestOrder OurRef",
                YourOrder = "TestOrder your order",
                YourRef = "Test Your Ref",
                InvoiceNumber = 12345,
                IsCreditNote = isCreditNote.Value,
                Participants = new List<int> { participant.Id }
            };
        }

        private OrderInfo CreateOrder(OrderInfo order)
        {
            var request = CreateWebRequest("orders/", "POST", order);
            using (var response = GetWebResponse(request))
            {
                var createdOrder = GetResponseContent<OrderInfo>(response);
                _sqlInitializer.CreatedOrderIds.Add(createdOrder.Id);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                return createdOrder;
            }
        }
    }
}