﻿using System.Linq;
using System.Net;
using Metier.Phoenix.Core.Data.Entities;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    public class UserCompetencesTests : AuthorizedApiTestBase
    {
        [Test]
        public void GetUserCompetence_ShouldGetCreatedUserCompetence()
        {
            var created = CreateUserCompetence();
            using (var response = GetWebResponse(CreateWebRequest("users/" + created.UserId + "/competence", "GET")))
            {
                var result = GetResponseContent<UserCompetence>(response);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(created.Id, result.Id);
            }
        }

        [Test]
        public void CreateUserCompetence_ShouldCreateUserCompetenceWithAttachments()
        {
            var competence = CreateUserCompetenceWithAttachment();
            Assert.AreEqual(1, competence.Attachments.Count);
            _sqlInitializer.CreatedAttachmentIds.AddRange(competence.Attachments.Select(a => a.Id));
        }
        
        [Test]
        public void CreateUserCompetence_GivenSocialSecurityNumber_ShouldReturnPlaintext()
        {
            var competence = GetValidUserCompetence();
            competence.SocialSecurityNumber = "12345678901";
            var created = CreateUserCompetence(competence);

            Assert.AreEqual(competence.SocialSecurityNumber, created.SocialSecurityNumber);
        }

        [Test]
        public void UpdateUserCompetence_ShouldUpdateUserCompetenceWithAttachments()
        {
            var competence = CreateUserCompetenceWithAttachment();
            var existingAttachmentId = competence.Attachments[0].Id;
            competence.Attachments.Add(new Attachment { FileId = GetNewFileId() });

            using (var response = GetWebResponse(CreateWebRequest("users/" + competence.UserId + "/competence", "PUT", competence)))
            {
                var updated = GetResponseContent<UserCompetence>(response);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(2, updated.Attachments.Count);
                Assert.AreEqual(existingAttachmentId, updated.Attachments[0].Id);

                _sqlInitializer.CreatedAttachmentIds.AddRange(updated.Attachments.Select(a => a.Id));
            }
        }

        private UserCompetence CreateUserCompetence()
        {
            return CreateUserCompetence(GetValidUserCompetence());
        }

        private UserCompetence CreateUserCompetenceWithAttachment()
        {
            var competence = GetValidUserCompetence();
            competence.Attachments.Add(new Attachment { FileId = GetNewFileId() });
            return CreateUserCompetence(competence);
        }

        private UserCompetence CreateUserCompetence(UserCompetence competence)
        {
            using (var response = GetWebResponse(CreateWebRequest("users/" + competence.UserId + "/competence", "POST", competence)))
            {
                var userCompetence = GetResponseContent<UserCompetence>(response);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
                _sqlInitializer.CreatedUserCompetenceIds.Add(userCompetence.Id);
                return userCompetence;
            }
        }

        private int GetNewFileId()
        {
            var id = SqlWriteHelper.InsertFile();
            _sqlInitializer.CreatedFileIds.Add(id);
            return id;
        }

        private UserCompetence GetValidUserCompetence()
        {
            return new UserCompetence
            {
                UserId = _sqlInitializer.CreatedUserIds.First()
            };
        }
    }
}