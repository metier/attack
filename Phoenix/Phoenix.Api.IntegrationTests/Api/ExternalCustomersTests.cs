﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Services.ExternalCustomers;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    [Category(TestCategories.ExternalDependencies)]
    public class ExternalCustomersTests : AuthorizedApiTestBase
    {
        [Test]
        [TestCase(1, false, Description = "Unknown")]
        [TestCase(1194, true, Description = "Metier Norway")]
        public void HasExternalCustomers_GivenDistributorId(int distributorId, bool expected)
        {
            var request = CreateWebRequest("externalcustomers/hasexternalcustomersprovider?distributorid=" + distributorId, "GET");
            using (var response = GetWebResponse(request))
            {
                var responseValue = GetResponseString(response);
                var result = JsonConvert.DeserializeObject<bool>(responseValue);

                Assert.AreEqual(expected, result);
            }
        }
        
        [Test]
        public void GetExternalCustomers_GivenMetierNorwayDistributorId_ShouldReturnExternalCustomers()
        {
            const int metierNorwayDistributorId = 1194;
            const string searchName = "";
            var request = CreateWebRequest("externalcustomers?distributorid=" + metierNorwayDistributorId + "&name=" + searchName, "GET");
            using (var response = GetWebResponse(request))
            {
                var responseValue = GetResponseString(response);
                var result = JsonConvert.DeserializeObject<List<ExternalCustomer>>(responseValue);

                Assert.GreaterOrEqual(result.Count, 2);
            }
        }
    }
}