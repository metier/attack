﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Facade.Account;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class AccountsTests : AuthorizedApiTestBase
    {
        #region Get User Account

        [Test]
        public void GetUserAccount_UsingUserId_ReturnsOkResponse()
        {
            int userId;
            var userName = SqlWriteHelper.InsertMembershipUser(Guid.NewGuid().ToString(), SqlWriteHelper.HashedImportedUserPassword, SqlWriteHelper.ImportedUserPasswordSalt, out userId);
            _sqlInitializer.CreatedUsernames.Add(userName);

            var request = CreateWebRequest("accounts?userId=" + userId, "GET");
            var response = GetWebResponse(request);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public void GetUserAccount_UsingUserId_ReturnsUserAccuntWithUser()
        {
            int userId;
            var userName = SqlWriteHelper.InsertMembershipUser(Guid.NewGuid().ToString(), SqlWriteHelper.HashedImportedUserPassword, SqlWriteHelper.ImportedUserPasswordSalt, out userId);
            _sqlInitializer.CreatedUsernames.Add(userName);

            var request = CreateWebRequest("accounts?userId=" + userId, "GET");
            var response = GetWebResponse(request);
            var userAccount = GetResponseContent<UserAccount>(response);

            Assert.IsNotNull(userAccount);
            Assert.IsNotNull(userAccount.User);
        }

        [Test]
        public void GetUserAccount_UsingNonExistingUserId_ReturnsNotFoundResponse()
        {
            int userId = Int32.MaxValue;

            var request = CreateWebRequest("accounts?userId=" + userId, "GET");
            var response = GetWebResponse(request);

            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        #endregion

        #region Create Arbitrary Account

        [Test]
        public void CreateArbitraryAccount_GivenUnknownCustomer_ShouldCreateUserAndCustomer()
        {
            var request = CreateNewArbitraryAccountRequest();
            var webRequest = CreateWebRequest("accounts/arbitrary", "POST", request);
            var webResponse = GetWebResponse(webRequest);

            Assert.AreEqual(HttpStatusCode.Created, webResponse.StatusCode);
            var response = GetResponseContent<ArbitraryAccountResponse>(webResponse);

            Assert.IsTrue(response.UserId > 0);
            Assert.IsTrue(response.ArbitraryCustomerId > 0);

            var numberOfUserInfoElements = SqlClient.ExecuteScalar("Select count(*) from userinfoelements where userid = " + response.UserId);
            Assert.AreEqual(7, numberOfUserInfoElements);

            _sqlInitializer.CreatedUserIds.Add(response.UserId);
            _sqlInitializer.CreatedCustomerIds.Add(response.ArbitraryCustomerId);
        }

        [Test]
        public void CreateArbitraryAccount_ShouldReuseCustomerWithSameAddress()
        {
            var request1 = CreateNewArbitraryAccountRequest();
            request1.CompanyName = "firmaet mitt";
            request1.City = "oslo";
            request1.ZipCode = "1234";
            request1.Country = "NO";
            request1.StreetAddress1 = "testveien 4";
            var webRequest = CreateWebRequest("accounts/arbitrary", "POST", request1);
            var webResponse = GetWebResponse(webRequest);
            var response1 = GetResponseContent<ArbitraryAccountResponse>(webResponse);


            var request2 = CreateNewArbitraryAccountRequest();
            request2.CompanyName = "Firmaet mitt  ";
            request2.City = "  OSLO ";
            request2.ZipCode = "  1234";
            request2.Country = "NO ";
            request2.StreetAddress1 = "  TestVeien 4 ";
            webRequest = CreateWebRequest("accounts/arbitrary", "POST", request2);
            webResponse = GetWebResponse(webRequest);
            var response2 = GetResponseContent<ArbitraryAccountResponse>(webResponse);
            try
            {
                Assert.AreEqual(response1.ArbitraryCustomerId, response2.ArbitraryCustomerId);
                Assert.AreNotEqual(response1.UserId, response2.UserId);
            }
            finally
            {
                _sqlInitializer.CreatedUserIds.Add(response1.UserId);
                _sqlInitializer.CreatedUserIds.Add(response2.UserId);
                _sqlInitializer.CreatedCustomerIds.Add(response1.ArbitraryCustomerId);
                _sqlInitializer.CreatedCustomerIds.Add(response2.ArbitraryCustomerId);
            }
        }
        
        [Test]
        public void CreateArbitraryAccount_ShouldNotReuseCustomerWithSameAddress()
        {
            var request1 = CreateNewArbitraryAccountRequest();
            var webRequest = CreateWebRequest("accounts/arbitrary", "POST", request1);
            var webResponse = GetWebResponse(webRequest);
            var response1 = GetResponseContent<ArbitraryAccountResponse>(webResponse);


            var request2 = CreateNewArbitraryAccountRequest();
            webRequest = CreateWebRequest("accounts/arbitrary", "POST", request2);
            webResponse = GetWebResponse(webRequest);
            var response2 = GetResponseContent<ArbitraryAccountResponse>(webResponse);
            try
            {
                Assert.AreNotEqual(response1.ArbitraryCustomerId, response2.ArbitraryCustomerId);
                Assert.AreNotEqual(response1.UserId, response2.UserId);
            }
            finally
            {
                _sqlInitializer.CreatedUserIds.Add(response1.UserId);
                _sqlInitializer.CreatedUserIds.Add(response2.UserId);
                _sqlInitializer.CreatedCustomerIds.Add(response1.ArbitraryCustomerId);
                _sqlInitializer.CreatedCustomerIds.Add(response2.ArbitraryCustomerId);
            }
        }

        [Test]
        public void CreateArbitraryAccount_GivenMatchingCompanyRegistrationNumbers_ShouldReuseCustomer()
        {
            var request1 = CreateNewArbitraryAccountRequest();
            request1.CompanyName = "firmaet mitt";
            request1.CompanyRegistrationNumber = "999999";
            request1.City = "oslo";
            request1.ZipCode = "1234";
            request1.Country = "NO";
            request1.StreetAddress1 = "testveien 4";
            var webRequest = CreateWebRequest("accounts/arbitrary", "POST", request1);
            var webResponse = GetWebResponse(webRequest);
            var response1 = GetResponseContent<ArbitraryAccountResponse>(webResponse);


            var request2 = CreateNewArbitraryAccountRequest();
            request2.CompanyName = "Mitt Firma ";
            request2.CompanyRegistrationNumber = "999999";
            request2.City = "  Drøbak ";
            request2.ZipCode = "  1233";
            request2.Country = "NO ";
            request2.StreetAddress1 = "  TestVeien 4 ";
            webRequest = CreateWebRequest("accounts/arbitrary", "POST", request2);
            webResponse = GetWebResponse(webRequest);
            var response2 = GetResponseContent<ArbitraryAccountResponse>(webResponse);
            try
            {
                Assert.AreEqual(response1.ArbitraryCustomerId, response2.ArbitraryCustomerId);
                Assert.AreNotEqual(response1.UserId, response2.UserId);
            }
            finally
            {
                _sqlInitializer.CreatedUserIds.Add(response1.UserId);
                _sqlInitializer.CreatedUserIds.Add(response2.UserId);
                _sqlInitializer.CreatedCustomerIds.Add(response1.ArbitraryCustomerId);
                _sqlInitializer.CreatedCustomerIds.Add(response2.ArbitraryCustomerId);
            }
        }

        [Test]
        public void CreateArbitraryAccount_GivenNoProvidedCompanyRegistrationNumber_ShouldReuseCustomerIfMatchesOtherCriteria()
        {
            var request1 = CreateNewArbitraryAccountRequest();
            request1.CompanyName = "mitt firma";
            request1.CompanyRegistrationNumber = "888888";
            request1.City = "Akershus";
            request1.ZipCode = "1234";
            request1.Country = "NO";
            request1.StreetAddress1 = "testveien 8";
            var webRequest = CreateWebRequest("accounts/arbitrary", "POST", request1);
            var webResponse = GetWebResponse(webRequest);
            var response1 = GetResponseContent<ArbitraryAccountResponse>(webResponse);


            var request2 = CreateNewArbitraryAccountRequest();
            request2.CompanyName = "mitt firma ";
            request2.CompanyRegistrationNumber = String.Empty;
            request2.City = "  Akershus ";
            request2.ZipCode = "  1234";
            request2.Country = "NO ";
            request2.StreetAddress1 = "  TestVeien 8 ";
            webRequest = CreateWebRequest("accounts/arbitrary", "POST", request2);
            webResponse = GetWebResponse(webRequest);
            var response2 = GetResponseContent<ArbitraryAccountResponse>(webResponse);
            try
            {
                Assert.AreEqual(response1.ArbitraryCustomerId, response2.ArbitraryCustomerId);
                Assert.AreNotEqual(response1.UserId, response2.UserId);
            }
            finally
            {
                _sqlInitializer.CreatedUserIds.Add(response1.UserId);
                _sqlInitializer.CreatedUserIds.Add(response2.UserId);
                _sqlInitializer.CreatedCustomerIds.Add(response1.ArbitraryCustomerId);
                _sqlInitializer.CreatedCustomerIds.Add(response2.ArbitraryCustomerId);
            }
        }

        [Test]
        public void
        CreateArbitraryAccount_GivenNoExistingCompanyRegistrationNumber_ShouldNotReuseCustomerEvenIfMatchesOtherCriteria()
        {
            var request1 = CreateNewArbitraryAccountRequest();
            request1.CompanyName = "mitt firma";
            request1.CompanyRegistrationNumber = string.Empty;
            request1.City = "Akershus";
            request1.ZipCode = "1234";
            request1.Country = "NO";
            request1.StreetAddress1 = "testveien 8";
            var webRequest = CreateWebRequest("accounts/arbitrary", "POST", request1);
            var webResponse = GetWebResponse(webRequest);
            var response1 = GetResponseContent<ArbitraryAccountResponse>(webResponse);


            var request2 = CreateNewArbitraryAccountRequest();
            request2.CompanyName = "mitt firma ";
            request2.CompanyRegistrationNumber = "777777";
            request2.City = "  Akershus ";
            request2.ZipCode = "  1234";
            request2.Country = "NO ";
            request2.StreetAddress1 = "  TestVeien 8 ";
            webRequest = CreateWebRequest("accounts/arbitrary", "POST", request2);
            webResponse = GetWebResponse(webRequest);
            var response2 = GetResponseContent<ArbitraryAccountResponse>(webResponse);
            try
            {
                Assert.AreNotEqual(response1.ArbitraryCustomerId, response2.ArbitraryCustomerId);
            }
            finally
            {
                _sqlInitializer.CreatedUserIds.Add(response1.UserId);
                _sqlInitializer.CreatedUserIds.Add(response2.UserId);
                _sqlInitializer.CreatedCustomerIds.Add(response1.ArbitraryCustomerId);
                _sqlInitializer.CreatedCustomerIds.Add(response2.ArbitraryCustomerId);
            }
        }

        #endregion

        #region Create Account

        [Test]
        public void CreateAccount_ShouldCreateAccount()
        {
            var account = CreateNewAccountInstance();
            
            var response = CreateUser(account);

            var userCreated = GetResponseContent<User>(response);
            _sqlInitializer.CreatedUsernames.Add(userCreated.MembershipUserId);

            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public void CreateAccount_ShouldCreateAccountWithRoles()
        {
            var account = CreateNewAccountInstance();
            account.Roles = new List<string> { PhoenixRoles.PhoenixUser };

            var response = CreateUser(account);

            var userCreated = GetResponseContent<User>(response);
            _sqlInitializer.CreatedUsernames.Add(userCreated.MembershipUserId);

            var userAccount = GetResponseContent<UserAccount>(GetWebResponse(CreateWebRequest("accounts?userId=" + userCreated.Id, "GET")));

            Assert.AreEqual(1, userAccount.Roles.Count);
            Assert.IsTrue(userAccount.Roles.Contains(PhoenixRoles.PhoenixUser));
        }

        [Test]
        public void CreateAccount_CreatesCorrespondingPhoenixUser()
        {
            var account = CreateNewAccountInstance();
            account.User = new User
            {
                CustomerId = account.CustomerId,
                FirstName = "MyName"
            };

            var response = CreateUser(account);

            var userCreated = GetResponseContent<User>(response);
            _sqlInitializer.CreatedUsernames.Add(userCreated.MembershipUserId);

            var firstNameDb = SqlReadHelper.GetFirstNameOfUser(userCreated.Id);

            Assert.AreEqual(firstNameDb, account.User.FirstName);
        }

        [Test]
        public void CreateAccount_NoCustomerSpecified_MustReturnBadRequest()
        {
            var account = CreateNewAccountInstance();
            account.CustomerId = 0;

            var response = CreateUser(account);

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public void CreateAccount_MissingRequiredAccountData_MustReturnBadRequest()
        {
            var account = CreateNewAccountInstance();
            account.Password = string.Empty;

            var response = CreateUser(account);
            
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public void CreateAccount_ShouldAutoGeneratePasswords()
        {
            var account = CreateNewAccountInstance();
            account.Password = string.Empty;

            var request = CreateWebRequest("accounts?autoGeneratePassword=true", "POST", account);
            var response = GetWebResponse(request);
            var userCreated = GetResponseContent<User>(response);
            _sqlInitializer.CreatedUsernames.Add(userCreated.MembershipUserId);
            
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public void CreateAccount_UserAlreadyExists_MustReturnBadRequest()
        {
            var existingUser = SqlReadHelper.GetImportedUserAccount();
            var account = CreateNewAccountInstance();
            account.Username = existingUser.Username;

            var response = CreateUser(account);

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public void CreateAccount_AssociatingToExistingPhoenixUser_MustReturnBadRequest()
        {
            var account = CreateNewAccountInstance();
            account.User = new User
            {
                Id = 1
            };

            var response = CreateUser(account);

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public void CreateAccount_MismatchingCustomerIdProperties_MustReturnBadRequest()
        {
            var account = CreateNewAccountInstance();
            account.User = new User
            {
                CustomerId = account.CustomerId + 1
            };

            var response = CreateUser(account);

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        #endregion

        #region Update Account

        [Test]
        public void UpdateAccount_ChangeUserData_MustReturnOkResponse()
        {
            var userName = SqlWriteHelper.InsertMembershipUser(Guid.NewGuid().ToString(), SqlWriteHelper.HashedImportedUserPassword, SqlWriteHelper.ImportedUserPasswordSalt);
            _sqlInitializer.CreatedUsernames.Add(userName);
            var userId = SqlReadHelper.GetUserIdForUsername(userName);

            var account = GetAccount(userId);
            account.User.LastName = "Last name - " + Guid.NewGuid();

            var request = CreateWebRequest("accounts/", "PUT", account);
            var response = GetWebResponse(request);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public void UpdateAccount_NonExistingUser_MustReturnNotFoundResponse()
        {
            var userId = SqlReadHelper.GetFirstUserId();
            var account = GetAccount(userId);

            account.Username = "SomethingNotExisting";

            var request = CreateWebRequest("accounts/", "PUT", account);
            var response = GetWebResponse(request);

            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public void UpdateAccount_ChangeUsernameForExistingUser_MustReturnOkResponse()
        {
            var userName = SqlWriteHelper.InsertMembershipUser(Guid.NewGuid().ToString(), SqlWriteHelper.HashedImportedUserPassword, SqlWriteHelper.ImportedUserPasswordSalt);
            var userId = SqlReadHelper.GetUserIdForUsername(userName);

            var account = GetAccount(userId);
            account.NewUsername = "NewUsername" + Guid.NewGuid();

            var request = CreateWebRequest("accounts/", "PUT", account);
            var response = GetWebResponse(request);
            _sqlInitializer.CreatedUsernames.Add(account.NewUsername);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public void UpdateAccount_ChangeUsernameForExistingUser_MustChangeUsername()
        {
            var userName = SqlWriteHelper.InsertMembershipUser(Guid.NewGuid().ToString(), SqlWriteHelper.HashedImportedUserPassword, SqlWriteHelper.ImportedUserPasswordSalt);
            var userId = SqlReadHelper.GetUserIdForUsername(userName);

            var account = GetAccount(userId);
            var newUsername = "NewUsername" + Guid.NewGuid();
            account.NewUsername = newUsername;

            var request = CreateWebRequest("accounts/", "PUT", account);
            var response = GetWebResponse(request);

            account = GetAccount(userId);
            _sqlInitializer.CreatedUsernames.Add(newUsername);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(newUsername, account.Username);
        }

        [Test]
        public void UpdateAccount_ChangeUsernameForExistingUser_UsernameUpdatedOnUser()
        {
            var userName = SqlWriteHelper.InsertMembershipUser(Guid.NewGuid().ToString(), SqlWriteHelper.HashedImportedUserPassword, SqlWriteHelper.ImportedUserPasswordSalt);
            var userId = SqlReadHelper.GetUserIdForUsername(userName);

            var account = GetAccount(userId);
            var newUsername = "NewUsername" + Guid.NewGuid();
            account.NewUsername = newUsername;

            var request = CreateWebRequest("accounts/", "PUT", account);
            var response = GetWebResponse(request);

            account = GetAccount(userId);
            _sqlInitializer.CreatedUsernames.Add(newUsername);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(newUsername, account.User.MembershipUserId);
        }

        [Test]
        public void UpdateAccount_ChangeEmailForExistingUser_MustReturnOkResponse()
        {
            var userName = SqlWriteHelper.InsertMembershipUser(Guid.NewGuid().ToString(), SqlWriteHelper.HashedImportedUserPassword, SqlWriteHelper.ImportedUserPasswordSalt);
            _sqlInitializer.CreatedUsernames.Add(userName);
            var userId = SqlReadHelper.GetUserIdForUsername(userName);

            var account = GetAccount(userId);
            account.Email = Guid.NewGuid() + "@test.com";

            var request = CreateWebRequest("accounts/", "PUT", account);
            var response = GetWebResponse(request);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public void UpdateAccount_ChangeEmailForExistingUser_EmailUpdated()
        {
            var userName = SqlWriteHelper.InsertMembershipUser(Guid.NewGuid().ToString(), SqlWriteHelper.HashedImportedUserPassword, SqlWriteHelper.ImportedUserPasswordSalt);
            _sqlInitializer.CreatedUsernames.Add(userName);
            var userId = SqlReadHelper.GetUserIdForUsername(userName);

            var account = GetAccount(userId);
            var newEmail = Guid.NewGuid() + "@test.com";
            account.Email = newEmail;

            var request = CreateWebRequest("accounts/", "PUT", account);
            var response = GetWebResponse(request);

            account = GetAccount(userId);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(newEmail, account.Email);
        }


        [Test]
        public void UpdateAccount_ShouldRemoveRoles()
        {
            var account = CreateNewAccountInstance();
            account.Roles = new List<string> { PhoenixRoles.PhoenixUser, PhoenixRoles.BehindAdmin };

            var user = GetResponseContent<User>(CreateUser(account));
            account = GetAccount(user.Id);
            _sqlInitializer.CreatedUsernames.Add(account.Username);

            account.Roles = new List<string> { PhoenixRoles.BehindAdmin };

            var request = CreateWebRequest("accounts/", "PUT", account);
            var response = GetWebResponse(request);

            account = GetAccount(user.Id);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(1, account.Roles.Count);
            Assert.IsTrue(account.Roles.Contains(PhoenixRoles.BehindAdmin));
        }

        [Test]
        public void UpdateAccount_RemovingPhoenixRoleOnSelf_ShouldReturnBadRequest()
        {
            var loggedOnUserId = SqlReadHelper.GetUserIdForUsername(_sqlInitializer.IntegrationTestPhoenixUserName);

            var loggedOnAccount = GetAccount(loggedOnUserId);
            loggedOnAccount.Roles.Remove(PhoenixRoles.PhoenixUser);

            var request = CreateWebRequest("accounts/", "PUT", loggedOnAccount);
            var response = GetWebResponse(request);
            var fault = GetResponseContent<Fault>(response);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual("Cannot remove phoenix role on logged in user", fault.ValidationErrors[0]);
        }
        
        [Test]
        public void UpdateAccount_ShouldAddRoles()
        {
            var account = CreateNewAccountInstance();
            account.Roles = new List<string> { PhoenixRoles.PhoenixUser };

            var user = GetResponseContent<User>(CreateUser(account));
            account = GetAccount(user.Id);
            _sqlInitializer.CreatedUsernames.Add(account.Username);

            account.Roles.Add(PhoenixRoles.BehindAdmin);

            var request = CreateWebRequest("accounts/", "PUT", account);
            var response = GetWebResponse(request);

            account = GetAccount(user.Id);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(2, account.Roles.Count);
            Assert.IsTrue(account.Roles.Contains(PhoenixRoles.BehindAdmin));
            Assert.IsTrue(account.Roles.Contains(PhoenixRoles.PhoenixUser));
        }

        #endregion

        #region Log On

        [Test]
        public void LogOn_CorrectCredentials_AuthenticationCookieAdded()
        {
            var importedOracleUser = SqlReadHelper.GetImportedUserAccount();
            var userName = importedOracleUser.Username;
            
            var response = LogOn(userName, importedOracleUser.Password);

            Assert.AreEqual("PhoenixAuth", response.Cookies[0].Name);
        }

        [Test]
        public void LogOn_CorrectCredentials_LastLoginDateIsUpdated()
        {
            var importedOracleUser = SqlReadHelper.GetImportedUserAccount();
            var userName = importedOracleUser.Username;

            LogOn(userName, importedOracleUser.Password);

            var lastLoginDateAsTicks = SqlReadHelper.GetLastLoginDateAsMinutesFromNow(userName);

            Assert.AreEqual(0, lastLoginDateAsTicks);
        }


        [Test]
        public void LogOn_CorrectCredentials_ReturnsStatusCode200()
        {
            var importedOracleUser = SqlReadHelper.GetImportedUserAccount();
            var userName = importedOracleUser.Username;

            var response = LogOn(userName, importedOracleUser.Password);
            
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public void LogOn_UppercaseUsername_ReturnsStatusCode200()
        {
            var importedOracleUser = SqlReadHelper.GetImportedUserAccount();
            var userName = importedOracleUser.Username;

            var response = LogOn(userName.ToUpper(), importedOracleUser.Password);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public void LogOn_CorrectCredentials_ReturnsDefaultDistributor()
        {
            var importedOracleUser = SqlReadHelper.GetImportedUserAccount();
            var userName = importedOracleUser.Username;

            var response = LogOn(userName, importedOracleUser.Password);
            var userContext = GetResponseContent<UserContext>(response);

            Assert.IsNotNull(userContext);
            Assert.IsNotNull(userContext.DefaultDistributor);
        }

        [Test]
        public void LogOn_WrongCredentials_ReturnsStatusCodeBadRequest()
        {
            var account = CreateNewAccountInstance();
            var userCreatedResponse = CreateUser(account);
            var userCreated = GetResponseContent<User>(userCreatedResponse);
            _sqlInitializer.CreatedUsernames.Add(userCreated.MembershipUserId);

            var logonResponse = LogOn(account.Username, "WrongPassword");

            Assert.AreEqual(HttpStatusCode.BadRequest, logonResponse.StatusCode);
        }

        #endregion

        #region Change Pasword

        [Test]
        public void ChangePassword_ImportedUser_ReturnsStatusCode200()
        {
            var userName = SqlWriteHelper.InsertMembershipUser(Guid.NewGuid().ToString(), SqlWriteHelper.HashedImportedUserPassword, SqlWriteHelper.ImportedUserPasswordSalt);
            _sqlInitializer.CreatedUsernames.Add(userName);
            const string newPassword = "p@ssword1";

            var response = ChangePassword(userName, newPassword);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public void ChangePassword_ImportedUser_CanLogOnWithNewPassword()
        {
            var userName = SqlWriteHelper.InsertMembershipUser(Guid.NewGuid().ToString(), SqlWriteHelper.HashedImportedUserPassword, SqlWriteHelper.ImportedUserPasswordSalt);
            _sqlInitializer.CreatedUsernames.Add(userName);
            const string newPassword = "myNewP@ssword1";

            var passwordResponse = ChangePassword(userName, newPassword);
            Assert.AreEqual(HttpStatusCode.OK, passwordResponse.StatusCode);

            var logonResponse = LogOn(userName, newPassword);

            Assert.AreEqual(HttpStatusCode.OK, logonResponse.StatusCode);
            Assert.AreEqual("PhoenixAuth", logonResponse.Cookies[0].Name);
        }

        [Test]
        public void ChangePassword_PhoenixCreatedUser_CanLogOnWithNewPassword()
        {
            var account = CreateNewAccountInstance();
            var userCreatedResponse = CreateUser(account);
            var userCreated = GetResponseContent<User>(userCreatedResponse);
            _sqlInitializer.CreatedUsernames.Add(userCreated.MembershipUserId);

            const string newPassword = "myNewP@ssword1";
            var passwordResponse = ChangePassword(account.Username, newPassword);
            Assert.AreEqual(HttpStatusCode.OK, passwordResponse.StatusCode);

            var logonResponse = LogOn(account.Username, newPassword);

            Assert.AreEqual(HttpStatusCode.OK, logonResponse.StatusCode);
            Assert.AreEqual("PhoenixAuth", logonResponse.Cookies[0].Name);
        }        
        
        [Test]
        public void ChangePassword_AsLearningPortalUser_GivenOthersUsername_ShouldReturnBadRequest()
        {
            var username = _sqlInitializer.IntegrationTestLearningPortalTokenUserName;
            var otherUsername = _sqlInitializer.IntegrationTestPhoenixUserName;
            LogOn(username);
            
            const string newPassword = "myNewP@ssword1";

            var passwordResponse = ChangePassword(otherUsername, newPassword);
            var fault = GetResponseContent<Fault>(passwordResponse);
            Assert.AreEqual("Cannot change other users's password", fault.Message);
            Assert.AreEqual(HttpStatusCode.Unauthorized, passwordResponse.StatusCode);
        }

        [Test]
        public void ChangePassword_AsLearningPortalUser_GivenSameUsername_ShouldChangePassword()
        {
            var username = _sqlInitializer.IntegrationTestLearningPortalTokenUserName;
            LogOn(username);

            const string newPassword = "myNewP@ssword1";

            var passwordResponse = ChangePassword(username, newPassword);
            Assert.AreEqual(HttpStatusCode.OK, passwordResponse.StatusCode);

            var logonResponse = LogOn(username, newPassword);

            Assert.AreEqual(HttpStatusCode.OK, logonResponse.StatusCode);
            Assert.AreEqual("PhoenixAuth", logonResponse.Cookies[0].Name);
        }

        #endregion

        #region Merge Accounts

        [Test]
        public void MergeAccounts_GivenUsersWithNoConflictingParticipations_ShouldMergeUsers()
        {
            var userName1 = SqlWriteHelper.InsertMembershipUser(Guid.NewGuid().ToString(), SqlWriteHelper.HashedImportedUserPassword, SqlWriteHelper.ImportedUserPasswordSalt);
            var userName2 = SqlWriteHelper.InsertMembershipUser(Guid.NewGuid().ToString(), SqlWriteHelper.HashedImportedUserPassword, SqlWriteHelper.ImportedUserPasswordSalt);

            _sqlInitializer.CreatedUsernames.Add(userName1);
            _sqlInitializer.CreatedUsernames.Add(userName2);

            var userId1 = SqlReadHelper.GetUserIdForUsername(userName1);
            var userId2 = SqlReadHelper.GetUserIdForUsername(userName2);

            var request = CreateWebRequest($"accounts/{userId1}/merge/{userId2}", "PUT");
            var response = GetWebResponse(request);

            var commentId = SqlClient.ExecuteScalar($"Select id from comments where Entity='user' and EntityId={userId1}");
            _sqlInitializer.CreatedCommentIds.Add(commentId);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(0, SqlClient.ExecuteScalar($"Select count(*) from users where id = {userId2}"), "Merged user was not successfully deleted.");
        }

        [Test]
        public void MergeAccounts_GivenUsersWithConflictingEnrollments_ShouldReturnBadRequest()
        {
            var userId1 = CreateDatabaseUserForMergeTests();
            var userId2 = CreateDatabaseUserForMergeTests();

            var customerId = SqlReadHelper.GetFirstCustomerId();
            var customerArticleId = SqlWriteHelper.InsertCustomerArticle(customerId, _sqlInitializer.CreatedArticleIds[0], _sqlInitializer.CreatedArticleIds[0], "test customer article");
            var activityId = SqlWriteHelper.InsertActivity(customerArticleId);
            var activitySetId = SqlWriteHelper.InsertActivitySet(customerId);
            SqlWriteHelper.AddActivityToActivitySet(activityId, activitySetId);

            var participantId1 = SqlWriteHelper.InsertParticipant(userId1, customerId, activityId, activitySetId);
            var participantId2 = SqlWriteHelper.InsertParticipant(userId2, customerId, activityId, activitySetId);

            _sqlInitializer.CreatedCustomerArticleIds.Add(customerArticleId);
            _sqlInitializer.CreatedActivityIds.Add(activityId);
            _sqlInitializer.CreatedActivitySetIds.Add(activitySetId);
            _sqlInitializer.CreatedParticipantIds.Add(participantId1);
            _sqlInitializer.CreatedParticipantIds.Add(participantId2);

            var request = CreateWebRequest($"accounts/{userId1}/merge/{userId2}", "PUT");
            var response = GetWebResponse(request);

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.AreEqual(1, SqlClient.ExecuteScalar($"Select count(*) from users where id = {userId2}"), "User should not have been deleted.");
        }

        [Test]
        public void MergeAccounts_GivenUsersWithEqualEnrollmentsWhereOneIsCancelled_ShouldDeleteCancelledEnrollmentAndMergeUsers()
        {
            var userId1 = CreateDatabaseUserForMergeTests();
            var userId2 = CreateDatabaseUserForMergeTests();

            var customerId = SqlReadHelper.GetFirstCustomerId();
            var customerArticleId = SqlWriteHelper.InsertCustomerArticle(customerId, _sqlInitializer.CreatedArticleIds[0], _sqlInitializer.CreatedArticleIds[0], "test customer article");
            var activityId = SqlWriteHelper.InsertActivity(customerArticleId);
            var activitySetId = SqlWriteHelper.InsertActivitySet(customerId);
            SqlWriteHelper.AddActivityToActivitySet(activityId, activitySetId);

            var participantId1 = SqlWriteHelper.InsertParticipant(userId1, customerId, activityId, activitySetId);
            var participantId2 = SqlWriteHelper.InsertParticipant(userId2, customerId, activityId, activitySetId);

            var enrollmentId2 = SqlClient.ExecuteScalar($"Select enrollmentId from Participants where Id = {participantId2}");
            SqlWriteHelper.CancelEnrollment(enrollmentId2);

            _sqlInitializer.CreatedCustomerArticleIds.Add(customerArticleId);
            _sqlInitializer.CreatedActivityIds.Add(activityId);
            _sqlInitializer.CreatedActivitySetIds.Add(activitySetId);
            _sqlInitializer.CreatedParticipantIds.Add(participantId1);
            _sqlInitializer.CreatedParticipantIds.Add(participantId2);

            var request = CreateWebRequest($"accounts/{userId1}/merge/{userId2}", "PUT");
            var response = GetWebResponse(request);

            var commentId = SqlClient.ExecuteScalar($"Select id from comments where Entity='user' and EntityId={userId1}");
            _sqlInitializer.CreatedCommentIds.Add(commentId);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            Assert.AreEqual(0, SqlClient.ExecuteScalar($"Select count(*) from enrollments where id = {enrollmentId2}"), "Enrollment was not successfully deleted.");
            Assert.AreEqual(0, SqlClient.ExecuteScalar($"Select count(*) from participants where enrollmentId = {enrollmentId2}"), "Participation was not successfully deleted.");
            Assert.AreEqual(0, SqlClient.ExecuteScalar($"Select count(*) from users where id = {userId2}"), "Merged user was not successfully deleted.");
        }

        #endregion

        #region Supporting Methods

        private UserAccount GetAccount(int userId)
        {
            var request = CreateWebRequest("accounts?userId=" + userId, "GET");

            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<UserAccount>(response);
            }
        }

        private HttpWebResponse CreateUser(UserAccount account)
        {
            var request = CreateWebRequest("accounts/", "POST", account);
            var response = GetWebResponse(request);
            return response;
        }

        private int CreateDatabaseUserForMergeTests()
        {
            var userName = SqlWriteHelper.InsertMembershipUser(Guid.NewGuid().ToString(), SqlWriteHelper.HashedImportedUserPassword, SqlWriteHelper.ImportedUserPasswordSalt);
            _sqlInitializer.CreatedUsernames.Add(userName);

            return SqlReadHelper.GetUserIdForUsername(userName);
        }

        private HttpWebResponse LogOn(string userName, string password)
        {
            var logOnModel = new LogOn
            {
                Password = password
            };

            var request = CreateWebRequest("accounts/" + userName + "/logon", "POST", logOnModel);
            request.CookieContainer = new CookieContainer();
            var response = GetWebResponse(request);
            return response;
        }

        private HttpWebResponse ChangePassword(string userName, string newPassword)
        {
            var changePassword = new ChangePassword
            {
                NewPassword = newPassword
            };

            var request = CreateWebRequest("accounts/" + userName + "/changepassword", "POST", changePassword);
            return GetWebResponse(request);
        }

        private ArbitraryAccountRequest CreateNewArbitraryAccountRequest()
        {
            return new ArbitraryAccountRequest
            {
                DistributorId = 1194,
                LanguageId = SqlReadHelper.GetFirstLanguageId(),
                CompanyName = "Test company " + Guid.NewGuid(),
                OurRef = "Sofia Lindqvist",
                FirstName = "First",
                LastName = "Last",
                City = "Oslo",
                Country = "NO",
                Email = Guid.NewGuid() + "@test.com",
                Phone = "123456789",
                StreetAddress1 = "Someplace",
                StreetAddress2 = "Somewhere",
                ZipCode = "1234"
            };
        }

        private UserAccount CreateNewAccountInstance()
        {
            return new UserAccount
            {
                Email = Guid.NewGuid() + "@test.com",
                Username = Guid.NewGuid().ToString(),
                Password = "p@ssword1",
                CustomerId = _sqlInitializer.CreatedCustomerIds.First()
            };
        }

        #endregion  
    }
}