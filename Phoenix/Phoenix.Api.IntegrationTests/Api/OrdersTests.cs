﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture] 
    public class OrdersTests : AuthorizedApiTestBase
    {
        [Test]
        public void GetOrder_ExistingOrder_ReturnsOrder()
        {
            var orderId = _sqlInitializer.InsertOrder(GetParticipantFromService(_sqlInitializer.CreatedParticipantIds.First()), GetActivityFromService(_sqlInitializer.CreatedActivityIds.First()));
            var order = GetOrderFromService(orderId);
            Assert.AreEqual(orderId, order.Id);
        }

        [Test]
        public void GetOrder_NonExistingOrder_ReturnsNotFound()
        {
            const int invalidOrderId = Int32.MaxValue;
            var request = CreateWebRequest("orders/" + invalidOrderId, "GET");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
            }
        }
 
        [Test]
        public void GetOrder_GivenInvoicedOrderWithMovedParticipant_ShouldReturnOriginalActivity()
        {
            var participant = GetParticipantFromService(_sqlInitializer.CreatedParticipantIds.First());
            var orderId = _sqlInitializer.InsertOrder(participant, null, SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Invoiced);

            var originalActivity = participant.ActivityId;
            participant.ActivityId = _sqlInitializer.InsertActivity();
            var movedActivity = UpdateParticipantViaService(participant).ActivityId;

            var order = GetOrderFromService(orderId);
            var returnedParticipant = order.Participants.First(p => p == participant.Id);
            var orderlines = order.OrderLines.Where(ol => ol.ParticipantId == returnedParticipant);
            var activityForReturnedParticipant = orderlines.Select(ol => ol.ParentActivityId).First();

            Assert.AreEqual(originalActivity, activityForReturnedParticipant);
            Assert.AreNotEqual(movedActivity, activityForReturnedParticipant);
        }

        [Test]
        public void GetOrders_UsingIdWithoutOrders_ReturnsStatusOk()
        {
            const int customerId = Int32.MaxValue;
            var request = CreateWebRequest("orders/?customerId=" + customerId, "GET");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }
        }

        [Test]
        public void GetOrderLines_QueryById_ReturnsStatusOk()
        {
            var orderId =_sqlInitializer.InsertOrder(GetParticipantFromService(_sqlInitializer.CreatedParticipantIds.First()), GetActivityFromService(_sqlInitializer.CreatedActivityIds.First()));
            var request = CreateWebRequest("orders/orderlines?id=" + orderId, "GET");
            using (var response = GetWebResponse(request))
            {
                var orderLines = GetResponseContent<List<OrderLine>>(response);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.IsNotNull(orderLines);
            }
        }

        //[Test]
        //public void GetOrderLines_QueryByGroupByKey_ReturnsOrderLines()
        //{
        //    var groupByKey = SqlReadHelper.GetFirstProposedOrderGroupByKey();
        //    var request = CreateWebRequest("orders/orderlines?groupByKey=" + groupByKey, "GET");
        //    using (var response = GetWebResponse(request))
        //    {
        //        var orderLines = GetResponseContent<List<OrderLine>>(response);
        //        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        //        Assert.IsNotNull(orderLines);                
        //    }
        //}

        [Test]
        public void GetOrders_QueryByGroupByKey_ReturnsOrderAssociatedWithKey()
        {
            var groupByKey = SqlReadHelper.GetFirstProposedOrderGroupByKey();
            var request = CreateWebRequest("orders/?groupbykey=" + groupByKey, "GET");
            using (var response = GetWebResponse(request))
            {
                var order = GetResponseContent<OrderInfo>(response);
                Assert.IsNotNull(order);
            }
        }

        [Test]
        public void CreateOrder_ValidOrder_ReturnsCreatedOrderAsDraft()
        {
            var order = GetOrderRequest();
            var createdOrder = CreateOrder(order);
            Assert.AreEqual(OrderStatus.Draft, createdOrder.Status);
        }

        [Test]
        public void CreateOrder_ParticipantAlreadyOnCurrentOrder_MustReturnBadRequest()
        {
            var participantId = _sqlInitializer.CreatedParticipantIds.First();
            var activityId = _sqlInitializer.CreatedActivityIds.First();
            _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), GetActivityFromService(activityId));

            var order = GetOrderRequest(participantId, activityId);

            var request = CreateWebRequest("orders/", "POST", order);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        [Test]
        public void CreateOrder_CurrentOrderIsCreditNote_ReturnsCreatedOrderAsDraft()
        {
            var participantId = _sqlInitializer.CreatedParticipantIds.First();
            var activityId = _sqlInitializer.CreatedActivityIds.First();
            _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), GetActivityFromService(activityId), _sqlInitializer.CreatedCustomerIds.First(), OrderStatus.Invoiced, true);

            var order = GetOrderRequest(participantId, activityId);

            var createdOrder = CreateOrder(order);
            Assert.AreEqual(OrderStatus.Draft, createdOrder.Status);
        }

        [Test]
        public void CreateOrder_CustomerIdNotSet_ReturnsBadRequest()
        {
            var order = GetOrderRequest();
            order.CustomerId = 0;

            var request = CreateWebRequest("orders/", "POST", order);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        [Test]
        public void CreateOrder_ParticipantConnectedToDifferentCustomerThanOrder_ReturnsBadRequest()
        {
            var participantCustomerId = SqlReadHelper.GetFirstCustomerIdForDistributor(1194);
            var orderCustomerId = _sqlInitializer.CreatedCustomerIds[0];
            Assert.AreNotEqual(participantCustomerId, orderCustomerId, "Customers must be different for test to make sense");

            var customerArticleId = SqlWriteHelper.InsertCustomerArticle(participantCustomerId, _sqlInitializer.CreatedArticleIds[0], _sqlInitializer.CreatedArticleIds[0], "test customer article");
            _sqlInitializer.CreatedCustomerArticleIds.Add(customerArticleId);
            
            var activityId = SqlWriteHelper.InsertActivity(customerArticleId);
            _sqlInitializer.CreatedActivityIds.Add(activityId);
            
            var activitySetId = SqlWriteHelper.InsertActivitySet(participantCustomerId);
            _sqlInitializer.CreatedActivitySetIds.Add(activitySetId);

            // Create a participant connected to "participantCustomerId"
            var participantId = SqlWriteHelper.InsertParticipant(_sqlInitializer.CreatedUserIds[0], participantCustomerId, activityId, activitySetId);
            _sqlInitializer.CreatedParticipantIds.Add(participantId);

            var order = GetOrderRequest(participantId);
            // Make sure the order is not connected to "participantCustomerId"
            order.CustomerId = orderCustomerId;
            
            var request = CreateWebRequest("orders/", "POST", order);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
                var errorMessage = GetResponseContent<Fault>(response).ValidationErrors[0];
                Assert.AreEqual("Participant is associated with a different customer than the one associated with the order.", errorMessage);
            }
        }

        [Test]
        public void CreateOrder_ActivityConnectedToDifferentCustomerThanOrder_ReturnsBadRequest()
        {
            var orderCustomerId = SqlReadHelper.GetFirstCustomerIdForDistributor(1194, true);
            var activityCustomerId = _sqlInitializer.CreatedCustomerIds[0];
            Assert.AreNotEqual(orderCustomerId, activityCustomerId);

            var customerArticleId = SqlWriteHelper.InsertCustomerArticle(activityCustomerId, _sqlInitializer.CreatedArticleIds[0], _sqlInitializer.CreatedArticleIds[0], "test customer article");
            _sqlInitializer.CreatedCustomerArticleIds.Add(customerArticleId);
            
            // Create an activity connected to "activityCustomerId"
            var activityId = SqlWriteHelper.InsertActivity(customerArticleId);
            _sqlInitializer.CreatedActivityIds.Add(activityId);

            var order = GetOrderRequest(null, activityId);
            order.Participants = new List<int>();
            // Make sure the order is not connected to "activityCustomerId"
            order.CustomerId = orderCustomerId;

            var request = CreateWebRequest("orders/", "POST", order);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
                var errorMessage = GetResponseContent<Fault>(response).ValidationErrors[0];
                Assert.AreEqual("Activity is associated with a different customer than the current order.", errorMessage);
            }
        }

        [Test]
        public void UpdateOrder_ValidOrder_ReturnsUpdatedOrderAsDraft()
        {
            var participantId = _sqlInitializer.CreatedParticipantIds.First();
            var activityId = _sqlInitializer.CreatedActivityIds.First();
            var orderId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), GetActivityFromService(activityId));

            var order = GetOrderFromService(orderId);

            var updatedOrder = UpdateOrder(order);
            Assert.AreEqual(OrderStatus.Draft, updatedOrder.Status);
        }

        [Test]
        public void DeleteOrder_CanDeleteDrafts()
        {
            var participantId = _sqlInitializer.CreatedParticipantIds.First();
            var activityId = _sqlInitializer.CreatedActivityIds.First();
            var orderId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), GetActivityFromService(activityId), _sqlInitializer.CreatedCustomerIds.First(), OrderStatus.Draft);
            var order = GetOrderFromService(orderId);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + order.Id, "DELETE")))
            {
                Assert.AreEqual(HttpStatusCode.Accepted, response.StatusCode);
            }

            // Verify that order was deleted
            using (var response = GetWebResponse(CreateWebRequest("orders/" + orderId, "GET")))
            {
                GetResponseContent<OrderInfo>(response);
                Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
            }
        }

        [Test]
        [TestCase(OrderStatus.Invoiced)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Rejected)]
        [TestCase(OrderStatus.Transferred)]
        [TestCase(OrderStatus.Proposed)]
        public void DeleteOrder_CannotDeleteNonDrafts(OrderStatus currentStatus)
        {
            var participantId = _sqlInitializer.CreatedParticipantIds.First();
            var activityId = _sqlInitializer.CreatedActivityIds.First();
            var orderId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), GetActivityFromService(activityId), _sqlInitializer.CreatedCustomerIds.First(), currentStatus);
            var order = GetOrderFromService(orderId);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + order.Id, "DELETE")))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }

            // Verify that order still exists
            order = GetOrderFromService(orderId);
            Assert.IsNotNull(order);
        }

        [Test]
        [TestCase(OrderStatus.Invoiced)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Rejected)]
        [TestCase(OrderStatus.Transferred)]
        public void UpdateOrder_OrderHasBeenInvoiced_ReturnsBadRequest(OrderStatus currentStatus)
        {
            var participantId = _sqlInitializer.CreatedParticipantIds.First();
            var activityId = _sqlInitializer.CreatedActivityIds.First();
            var orderId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), GetActivityFromService(activityId), _sqlInitializer.CreatedCustomerIds.First(), currentStatus);

            var order = GetOrderFromService(orderId);

            var request = CreateWebRequest("orders/" + order.Id, "PUT", order);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        [Test]
        [TestCase(OrderStatus.Invoiced)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Rejected)]
        [TestCase(OrderStatus.Transferred)]
        public void Invoice_GivenInvoicedOrder_ReturnsBadRequest(OrderStatus currentStatus)
        {
            var participantId = _sqlInitializer.CreatedParticipantIds.First();
            var activityId = _sqlInitializer.CreatedActivityIds.First();
            var orderId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), GetActivityFromService(activityId), _sqlInitializer.CreatedCustomerIds.First(), currentStatus);
            var order = GetOrderFromService(orderId);

            var request = CreateWebRequest("orders/invoice?id=" + order.Id, "PUT");
            using (var response = GetWebResponse(request))
            {
                var fault = GetResponseContent<Fault>(response);
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
                Assert.AreEqual("Order has already been invoiced", fault.ValidationErrors[0]);
            }
        }

        [Test]
        public void Invoice_GivenDraftOrder_ShouldSendToInvoicing()
        {

            const int metierNorwayDistributorId = 1194;

            var customerId = SqlReadHelper.GetFirstCustomerIdForDistributor(metierNorwayDistributorId, true);
            var customerArticleId = SqlWriteHelper.InsertCustomerArticle(customerId, _sqlInitializer.CreatedArticleIds[0], _sqlInitializer.CreatedArticleIds[0], "test customer article");
            _sqlInitializer.CreatedCustomerArticleIds.Add(customerArticleId);
            var activityId = SqlWriteHelper.InsertActivity(customerArticleId);
            _sqlInitializer.CreatedActivityIds.Add(activityId);
            var activitySetId = SqlWriteHelper.InsertActivitySet(customerId);
            _sqlInitializer.CreatedActivitySetIds.Add(activitySetId);
            var participantId = SqlWriteHelper.InsertParticipant(_sqlInitializer.CreatedUserIds[0], customerId, activityId, activitySetId);
            _sqlInitializer.CreatedParticipantIds.Add(participantId);

            var instantBeforeTest = DateTime.UtcNow;
            var orderId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), GetActivityFromService(activityId), customerId, OrderStatus.Draft);
            var order = GetOrderFromService(orderId);

            var request = CreateWebRequest("orders/invoice?id=" + order.Id, "PUT");
            using (var response = GetWebResponse(request))
            {
                var invoicedOrder = GetResponseContent<OrderInfo>(response);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(OrderStatus.Transferred, invoicedOrder.Status);
                Assert.Greater(invoicedOrder.OrderDate, instantBeforeTest);
                Assert.Greater(invoicedOrder.OrderNumber, 0);
            }
        }

        [Test]
        public void Invoice_GivenDraftOrderWithNonAgressoDistributor_ShouldReturnBadRequest()
        {
            int metierNordwayDistributorId = _sqlInitializer.DistributorIdWithoutExternalCustomers;
            var participantId = _sqlInitializer.CreatedParticipantIds.First();
            var activityId = _sqlInitializer.CreatedActivityIds.First();
            var orderId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), GetActivityFromService(activityId), SqlReadHelper.GetFirstCustomerIdForDistributor(metierNordwayDistributorId), OrderStatus.Draft);
            var order = GetOrderFromService(orderId);

            var request = CreateWebRequest("orders/invoice?id=" + order.Id, "PUT");
            using (var response = GetWebResponse(request))
            {
                var fault = GetResponseContent<Fault>(response);
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
                Assert.AreEqual("Participant is associated with a different customer than the one associated with the order.", fault.ValidationErrors[0]);
            }
        }

        [Test]
        public void Reset_GivenRejectedOrder_ShouldResetOrder()
        {
            var distributorIdWithoutExternalCustomers = _sqlInitializer.DistributorIdWithoutExternalCustomers;

            var customerId = SqlReadHelper.GetFirstCustomerIdForDistributor(distributorIdWithoutExternalCustomers);
            var customerArticleId = SqlWriteHelper.InsertCustomerArticle(customerId, _sqlInitializer.CreatedArticleIds[0], _sqlInitializer.CreatedArticleIds[0], "test customer article");
            _sqlInitializer.CreatedCustomerArticleIds.Add(customerArticleId);
            var activityId = SqlWriteHelper.InsertActivity(customerArticleId);
            _sqlInitializer.CreatedActivityIds.Add(activityId);
            var activitySetId = SqlWriteHelper.InsertActivitySet(customerId);
            _sqlInitializer.CreatedActivitySetIds.Add(activitySetId);
            var participantId = SqlWriteHelper.InsertParticipant(_sqlInitializer.CreatedUserIds[0], customerId, activityId, activitySetId);
            _sqlInitializer.CreatedParticipantIds.Add(participantId);

            var orderId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), 
                                                      GetActivityFromService(activityId),
                                                      customerId, 
                                                      OrderStatus.Rejected);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + orderId + "/reset", "PUT")))
            {
                var resetOrder = GetResponseContent<OrderInfo>(response);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(null, resetOrder.OrderNumber);
                Assert.AreEqual(OrderStatus.Draft, resetOrder.Status);
            }
        }

        [Test]
        [TestCase(OrderStatus.Draft)]
        [TestCase(OrderStatus.Invoiced)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Proposed)]
        [TestCase(OrderStatus.Transferred)]
        public void Reset_GivenNonRejectedOrder_ShouldReturnBadRequest(OrderStatus orderStatus)
        {
            var orderId = _sqlInitializer.InsertOrder(GetParticipantFromService(_sqlInitializer.CreatedParticipantIds.First()),
                                          GetActivityFromService(_sqlInitializer.CreatedActivityIds.First()),
                                          SqlReadHelper.GetFirstCustomerIdForDistributor(_sqlInitializer.DistributorIdWithoutExternalCustomers),
                                          orderStatus);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + orderId + "/reset", "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        [Test]
        public void Credit_GivenValidInvoicedOrder_ShouldReturnNewIsCreditNoteOrderWithStatusTransferred()
        {
            var customerId = SqlReadHelper.GetFirstCustomerIdForDistributor(1194, true);
            var customerArticleId = SqlWriteHelper.InsertCustomerArticle(customerId, _sqlInitializer.CreatedArticleIds[0], _sqlInitializer.CreatedArticleIds[0], "test customer article");
            _sqlInitializer.CreatedCustomerArticleIds.Add(customerArticleId);
            var activityId = SqlWriteHelper.InsertActivity(customerArticleId);
            _sqlInitializer.CreatedActivityIds.Add(activityId);
            var activitySetId = SqlWriteHelper.InsertActivitySet(customerId);
            _sqlInitializer.CreatedActivitySetIds.Add(activitySetId);
            var participantId = SqlWriteHelper.InsertParticipant(_sqlInitializer.CreatedUserIds[0], customerId, activityId, activitySetId);
            _sqlInitializer.CreatedParticipantIds.Add(participantId);

            var orderId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), GetActivityFromService(activityId), customerId, OrderStatus.Invoiced);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + orderId + "/credit", "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                
                var creditNoteOrder = GetResponseContent<OrderInfo>(response);
                _sqlInitializer.CreatedOrderIds.Add(creditNoteOrder.Id);

                Assert.AreNotEqual(orderId, creditNoteOrder.Id);
                Assert.AreEqual(OrderStatus.Transferred, creditNoteOrder.Status);
                Assert.IsTrue(creditNoteOrder.IsCreditNote);
                Assert.AreEqual(1, creditNoteOrder.Participants.Count(p => p == participantId));
                Assert.AreEqual(1, creditNoteOrder.FixedPriceActivities.Count(a => a == activityId));
            }
        }

        [Test]
        public void Credit_GivenInvoicedOrderWithCreditedParticipant_ShouldCreditOrderWithoutPreviouslyCreditedOrderLines()
        {
            var participantId1 = _sqlInitializer.CreatedParticipantIds.First();
            var participantId2 = _sqlInitializer.CreatedParticipantIds.Last();

            var participant1 = GetParticipantFromService(participantId1);
            var participant2 = GetParticipantFromService(participantId2);

            var customerId = participant1.CustomerId;

            if(participant1.CustomerId!=participant2.CustomerId)
                throw new Exception("Error in initialization of unit tests. CustomerIds must be equal!");

            var participants = new List<Participant> {participant1, participant2};

            var orderId = _sqlInitializer.InsertOrder(participants, null, customerId, OrderStatus.Invoiced);
            var creditOrderId = _sqlInitializer.InsertOrder(participant1, null, customerId, OrderStatus.Invoiced, true, orderId);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + orderId + "/creditnote", "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                var sql = string.Format(" from Orders_Participants op inner join orders o on o.Id = op.OrderId where o.CreditedOrderId = {0} and o.Id != {1}", orderId, creditOrderId);

                var creditNoteOrderId = SqlClient.ExecuteScalar(string.Concat("select o.id", sql));
                _sqlInitializer.CreatedOrderIds.Add(creditNoteOrderId);

                //Verify that only one participantOrderLine is included on the credit note
                var numberOfParticipantOrderLines = SqlClient.ExecuteScalar(string.Concat("select count(*) ", sql));
                Assert.AreEqual(1, numberOfParticipantOrderLines);

                //Verify that the credited participantid equals participantId2
                var creditedParticipantId = SqlClient.ExecuteScalar(string.Concat("select op.participantId ", sql));
                Assert.AreEqual(participantId2, creditedParticipantId);
            }
        }

        [Test]
        public void Credit_GivenInvoicedOrderWithCreditedActivities_ShouldCreditOrderWithoutPreviouslyCreditedOrderLines()
        {
            var activityId1 = _sqlInitializer.CreatedActivityIds.First();
            var activityId2 = _sqlInitializer.CreatedActivityIds.Last();

            var activity1 = GetActivityFromService(activityId1);
            var activity2 = GetActivityFromService(activityId2);

            var customerId = _sqlInitializer.CreatedCustomerIds.First();

            var activities = new List<Activity> { activity1, activity2 };

            var orderId = _sqlInitializer.InsertOrder(null, activities, customerId, OrderStatus.Invoiced);
            var creditOrderId = _sqlInitializer.InsertOrder(null, activity1, customerId, OrderStatus.Invoiced, true, orderId);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + orderId + "/creditnote", "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                var sql = string.Format(" from Orders_Activities oa inner join orders o on o.Id = oa.OrderId where o.CreditedOrderId = {0} and o.Id != {1}", orderId, creditOrderId);

                var creditNoteOrderId = SqlClient.ExecuteScalar(string.Concat("select o.id", sql));
                _sqlInitializer.CreatedOrderIds.Add(creditNoteOrderId);

                //Verify that only one activityOrderLine is included on the credit note
                var numberOfActivityOrderLines = SqlClient.ExecuteScalar(string.Concat("select count(*) ", sql));
                Assert.AreEqual(1, numberOfActivityOrderLines);

                //Verify that the credited activityId equals activityId2
                var creditedActivityId = SqlClient.ExecuteScalar(string.Concat("select oa.activityId ", sql));
                Assert.AreEqual(activityId2, creditedActivityId);
            }
        }

        [Test]
        public void Credit_GivenInvoicedOrderWithCreditedParticipant_ShouldReturnValidationError()
        {
            var participantId = _sqlInitializer.CreatedParticipantIds.First();

            var orderId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), null, SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Invoiced);
            _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), null, SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Invoiced, true);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + orderId + "/credit", "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);

                var fault = GetResponseContent<Fault>(response);
                Assert.AreEqual("Order is already partially credited", fault.ValidationErrors[0]);
            }
        }

        [Test]
        public void Credit_GivenInvoicedOrderWithCreditedActivity_ShouldReturnValidationError()
        {
            var activityId = _sqlInitializer.CreatedActivityIds.First();

            var orderId = _sqlInitializer.InsertOrder(null, GetActivityFromService(activityId), SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Invoiced);
            _sqlInitializer.InsertOrder(null, GetActivityFromService(activityId), SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Invoiced, true);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + orderId + "/credit", "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);

                var fault = GetResponseContent<Fault>(response);
                Assert.AreEqual("Order is already partially credited", fault.ValidationErrors[0]);
            }
        }

        
        [Test]
        [TestCase(OrderStatus.Draft)]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Rejected)]
        [TestCase(OrderStatus.Transferred)]
        public void Credit_GivenOrderNotYetInvoiced_ShouldReturnValidationError(OrderStatus orderStatus)
        {
            var participantId = _sqlInitializer.CreatedParticipantIds.First();
            var activityId = _sqlInitializer.CreatedActivityIds.First();

            var orderId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), GetActivityFromService(activityId), SqlReadHelper.GetFirstCustomerIdForDistributor(1194), orderStatus);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + orderId + "/credit", "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);

                var fault = GetResponseContent<Fault>(response);
                Assert.AreEqual("Order is not yet invoiced, so crediting is not allowed.", fault.ValidationErrors[0]);
            }
        }

        [Test]
        [TestCase(OrderStatus.NotInvoiced)]
        [TestCase(OrderStatus.Rejected)]
        [TestCase(OrderStatus.Transferred)]
        public void Credit_GivenNonDraftCreditNoteOrder_ShouldReturnValidationError(OrderStatus orderStatus)
        {
            var participantId = _sqlInitializer.CreatedParticipantIds.First();
            var activityId = _sqlInitializer.CreatedActivityIds.First();

            var orderId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), GetActivityFromService(activityId), SqlReadHelper.GetFirstCustomerIdForDistributor(1194), orderStatus, true);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + orderId + "/credit", "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);

                var fault = GetResponseContent<Fault>(response);
                Assert.AreEqual("Credit note has already been sent", fault.ValidationErrors[0]);
            }
        }

        [Test]
        public void Credit_GivenValidDraftCreditNote_ShouldReturnSameCreditNoteOrderWithStatusTransferred()
        {
            var customerId = SqlReadHelper.GetFirstCustomerIdForDistributor(1194, true);
            var customerArticleId = SqlWriteHelper.InsertCustomerArticle(customerId, _sqlInitializer.CreatedArticleIds[0], _sqlInitializer.CreatedArticleIds[0], "test customer article");
            _sqlInitializer.CreatedCustomerArticleIds.Add(customerArticleId);
            var activityId = SqlWriteHelper.InsertActivity(customerArticleId);
            _sqlInitializer.CreatedActivityIds.Add(activityId);
            var activitySetId = SqlWriteHelper.InsertActivitySet(customerId);
            _sqlInitializer.CreatedActivitySetIds.Add(activitySetId);
            var participantId = SqlWriteHelper.InsertParticipant(_sqlInitializer.CreatedUserIds[0], customerId, activityId, activitySetId);
            _sqlInitializer.CreatedParticipantIds.Add(participantId);

            var orderToCreditId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), GetActivityFromService(activityId), customerId, OrderStatus.Invoiced);
            // Create credit note containing participant, but not activity
            var creditNoteOrderId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), null, customerId, OrderStatus.Draft, true, orderToCreditId);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + creditNoteOrderId + "/credit", "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                var creditNoteOrder = GetResponseContent<OrderInfo>(response);

                Assert.AreEqual(creditNoteOrderId, creditNoteOrder.Id);
                Assert.AreEqual(OrderStatus.Transferred, creditNoteOrder.Status);
                Assert.IsTrue(creditNoteOrder.IsCreditNote);
                Assert.IsTrue(creditNoteOrder.Participants.Count(p => p == participantId) == 1);
                Assert.IsTrue(creditNoteOrder.FixedPriceActivities.Count(a => a == activityId) == 0);
            }
        }

        [Test]
        public void Credit_GivenDraftCreditNoteNotSpecifyingOrderBeingCredited_ShouldReturnValidationError()
        {
            var participantId = _sqlInitializer.CreatedParticipantIds.First();
            var orderId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), null, SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Draft, true);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + orderId + "/credit", "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);

                var fault = GetResponseContent<Fault>(response);
                Assert.AreEqual("Order being credited must be specified on credit note order", fault.ValidationErrors[0]);
            }
        }

        [Test]
        public void Credit_GivenDraftCreditNoteWithParticipantNotYetInvoiced_ShouldReturnValidationError()
        {
            var participantId = _sqlInitializer.CreatedParticipantIds.First();

            var orderToCreditId = _sqlInitializer.InsertOrder(SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Transferred);
            var orderId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), null, SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Draft, true, orderToCreditId);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + orderId + "/credit", "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);

                var fault = GetResponseContent<Fault>(response);
                Assert.AreEqual(string.Format("Credit note contains participant (ID: {0}) not yet invoiced", participantId), fault.ValidationErrors[0]);
            }
        }

        [Test]
        public void Credit_GivenDraftCreditNoteWithParticipantOnOrderNotYetInvoiced_ShouldReturnValidationError()
        {
            var participantId = _sqlInitializer.CreatedParticipantIds.First();

            var orderToCreditId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), null, SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Transferred);
            var orderId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), null, SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Draft, true, orderToCreditId);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + orderId + "/credit", "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);

                var fault = GetResponseContent<Fault>(response);
                Assert.AreEqual(string.Format("Credit note contains participant (ID: {0}) not yet invoiced", participantId), fault.ValidationErrors[0]);
            }
        }

        [Test]
        public void Credit_GivenDraftCreditNoteWithActivityNotYetInvoiced_ShouldReturnValidationError()
        {
            var activityId = _sqlInitializer.CreatedActivityIds.First();

            var orderToCreditId = _sqlInitializer.InsertOrder(SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Transferred);
            var orderId = _sqlInitializer.InsertOrder(null, GetActivityFromService(activityId), SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Draft, true, orderToCreditId);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + orderId + "/credit", "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);

                var fault = GetResponseContent<Fault>(response);
                Assert.AreEqual(string.Format("Credit note contains activity (ID: {0}) not yet invoiced", activityId), fault.ValidationErrors[0]);
            }
        }

        [Test]
        public void Credit_GivenDraftCreditNoteWithActivityOnOrderNotYetInvoiced_ShouldReturnValidationError()
        {
            var activityId = _sqlInitializer.CreatedActivityIds.First();

            var orderToCreditId = _sqlInitializer.InsertOrder(null, GetActivityFromService(activityId), SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Transferred);
            var orderId = _sqlInitializer.InsertOrder(null, GetActivityFromService(activityId), SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Draft, true, orderToCreditId);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + orderId + "/credit", "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);

                var fault = GetResponseContent<Fault>(response);
                Assert.AreEqual(string.Format("Credit note contains activity (ID: {0}) not yet invoiced", activityId), fault.ValidationErrors[0]);
            }
        }

        [Test]
        public void Credit_GivenDraftCreditNoteWithCreditedParticipant_ShouldReturnValidationError()
        {
            var participantId = _sqlInitializer.CreatedParticipantIds.First();

            var creditedOrder = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), null, SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Invoiced);
            // Insert credit order for participant
            _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), null, SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Transferred, true, creditedOrder);
            // Create new credit order for same participant
            var orderId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), null, SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Draft, true, creditedOrder);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + orderId + "/credit", "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);

                var fault = GetResponseContent<Fault>(response);
                Assert.AreEqual(string.Format("Credit note contains participant (ID: {0}) already credited", participantId), fault.ValidationErrors[0]);
            }
        }

        [Test]
        public void Credit_GivenDraftCreditNoteWithCreditedActivity_ShouldReturnValidationError()
        {
            var activityId = _sqlInitializer.CreatedActivityIds.First();

            var creditedOrder = _sqlInitializer.InsertOrder(null, GetActivityFromService(activityId), SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Invoiced);
            // Insert credit order for activity
            _sqlInitializer.InsertOrder(null, GetActivityFromService(activityId), SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Transferred, true, creditedOrder);
            // Create new credit order for same activity
            var orderId = _sqlInitializer.InsertOrder(null, GetActivityFromService(activityId), SqlReadHelper.GetFirstCustomerIdForDistributor(1194),OrderStatus.Draft, true, creditedOrder);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + orderId + "/credit", "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);

                var fault = GetResponseContent<Fault>(response);
                Assert.AreEqual(string.Format("Credit note contains activity (ID: {0}) already credited", activityId), fault.ValidationErrors[0]);
            }
        }

        [Test]
        public void Credit_GivenDraftCreditNoteWithParticipantNotOnOriginalOrder_ShouldReturnValidationError()
        {
            var participantId = _sqlInitializer.CreatedParticipantIds.First();

            _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), null, SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Invoiced);
            var orderToCreditId = _sqlInitializer.InsertOrder(SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Invoiced);
            var orderId = _sqlInitializer.InsertOrder(GetParticipantFromService(participantId), null, SqlReadHelper.GetFirstCustomerIdForDistributor(1194),
                                                      OrderStatus.Draft, true, orderToCreditId);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + orderId + "/credit", "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);

                var fault = GetResponseContent<Fault>(response);
                Assert.AreEqual(string.Format("Participant (ID: {0}) is currently invoiced in an order different from the order being credited", participantId), fault.ValidationErrors[0]);
            }
        }

        [Test]
        public void Credit_GivenDraftCreditNoteWithActivityNotOnOriginalOrder_ShouldReturnValidationError()
        {
            var activityId = _sqlInitializer.CreatedActivityIds.First();

            _sqlInitializer.InsertOrder(null, GetActivityFromService(activityId), SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Invoiced);
            var orderToCreditId = _sqlInitializer.InsertOrder(SqlReadHelper.GetFirstCustomerIdForDistributor(1194), OrderStatus.Invoiced);
            var orderId = _sqlInitializer.InsertOrder(null, GetActivityFromService(activityId), SqlReadHelper.GetFirstCustomerIdForDistributor(1194),
                                                      OrderStatus.Draft, true, orderToCreditId);

            using (var response = GetWebResponse(CreateWebRequest("orders/" + orderId + "/credit", "PUT")))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);

                var fault = GetResponseContent<Fault>(response);
                Assert.AreEqual(string.Format("Activity (ID: {0}) is currently invoiced in an order different from the order being credited", activityId), fault.ValidationErrors[0]);
            }
        }

        private OrderInfo GetOrderFromService(int orderId)
        {
            var request = CreateWebRequest("orders/" + orderId, "GET");
            using (var response = GetWebResponse(request))
            {
                var order = GetResponseContent<OrderInfo>(response);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                return order;
            }
        }

        private OrderInfo CreateOrder(OrderInfo order)
        {
            var request = CreateWebRequest("orders/", "POST", order);
            using (var response = GetWebResponse(request))
            {
                var createdOrder = GetResponseContent<OrderInfo>(response);
                _sqlInitializer.CreatedOrderIds.Add(createdOrder.Id);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

                return createdOrder;
            }
        }

        private OrderInfo GetOrderRequest(int? participantId = null, int? activityId = null)
        {
            if (!participantId.HasValue)
            {
                participantId = _sqlInitializer.CreatedParticipantIds.First();
            }
            if (!activityId.HasValue)
            {
                activityId = _sqlInitializer.CreatedActivityIds.First();
            }

            var customerId = _sqlInitializer.CreatedCustomerIds.First();
            var participant = GetParticipantFromService(participantId.Value);
            var activity = GetActivityFromService(activityId.Value);
            var userId = _sqlInitializer.CreatedUserIds.First();

            return new OrderInfo
            {
                Title = "TestOrder",
                CoordinatorId = userId,
                CustomerId = customerId,
                OrderNumber = 3000000,
                OurRef = "TestOrder OurRef",
                YourOrder = "TestOrder your order",
                YourRef = "Test Your Ref",
                InvoiceNumber = 12345,
                Participants = new List<int> { participant.Id },
                FixedPriceActivities = new List<int> { activity.Id }
            };
        }

        private OrderInfo UpdateOrder(OrderInfo order)
        {
            var request = CreateWebRequest("orders/" + order.Id, "PUT", order);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                return GetResponseContent<OrderInfo>(response);
            }
        }

        private Activity GetActivityFromService(int activityId)
        {
            var request = CreateWebRequest("activities/" + activityId, "GET");
            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<Activity>(response);
            }
        }

        private Participant GetParticipantFromService(int participantId)
        {
            var request = CreateWebRequest("participants/" + participantId, "GET");
            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<Participant>(response);
            }
        }

        private Participant UpdateParticipantViaService(Participant participant)
        {
            var request = CreateWebRequest("participants/" + participant.Id, "PUT", participant);
            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<Participant>(response);
            }
        }
    }
}