﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class CustomerArticlesTests : AuthorizedApiTestBase
    {
        #region Tests: customerarticles GET(*)

        [Test]
        public void GetCustomerArticles_GivenValidExamId_ShouldReturnRelatedCustomerArticles()
        {
            var examId = SqlReadHelper.GetLastCourseExamId();

            var initialCount = int.Parse(SqlClient.ExecuteScalarReturnString(string.Format("SELECT count(*) FROM CustomerArticles WHERE examid = {0}", examId)));

            var customerArticleId1 = SqlWriteHelper.InsertCustomerArticle(_sqlInitializer.CreatedCustomerIds[0], _sqlInitializer.CreatedArticleIds[0], _sqlInitializer.CreatedArticleIds[0], "test customer article", examId: examId);
            var customerArticleId2 = SqlWriteHelper.InsertCustomerArticle(_sqlInitializer.CreatedCustomerIds[0], _sqlInitializer.CreatedArticleIds[0], _sqlInitializer.CreatedArticleIds[0], "test customer article", examId: examId);

            _sqlInitializer.CreatedCustomerArticleIds.Add(customerArticleId1);
            _sqlInitializer.CreatedCustomerArticleIds.Add(customerArticleId2);

            var request = CreateWebRequest("customerarticles?examId=" + examId, "GET");
            using (var response = GetWebResponse(request))
            {
                var customerArticles = GetResponseContent<List<CustomerArticle>>(response);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(initialCount + 2, customerArticles.Count);
            }
        }

        [Test]
        public void GetCustomerArticles_GivenValidModuleId_ShouldReturnRelatedCustomerArticles()
        {
            var moduleId = SqlReadHelper.GetLastCourseModuleId();

            var initialCount = int.Parse(SqlClient.ExecuteScalarReturnString(string.Format("SELECT count(*) FROM CustomerArticles WHERE moduleId = {0}", moduleId)));
            
            var customerArticleId1 = SqlWriteHelper.InsertCustomerArticle(_sqlInitializer.CreatedCustomerIds[0], _sqlInitializer.CreatedArticleIds[0], _sqlInitializer.CreatedArticleIds[0], "test customer article", moduleId: moduleId);
            var customerArticleId2 = SqlWriteHelper.InsertCustomerArticle(_sqlInitializer.CreatedCustomerIds[0], _sqlInitializer.CreatedArticleIds[0], _sqlInitializer.CreatedArticleIds[0], "test customer article", moduleId: moduleId);

            _sqlInitializer.CreatedCustomerArticleIds.Add(customerArticleId1);
            _sqlInitializer.CreatedCustomerArticleIds.Add(customerArticleId2);

            var request = CreateWebRequest("customerarticles?moduleId=" + moduleId, "GET");
            using (var response = GetWebResponse(request))
            {
                var customerArticles = GetResponseContent<List<CustomerArticle>>(response);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(initialCount + 2, customerArticles.Count);
            }
        }

        [Test]
        public void GetCustomerArticles_GivenValidArticleId_ShouldReturnRelatedCustomerArticles()
        {
            var customerArticleId1 = SqlWriteHelper.InsertCustomerArticle(_sqlInitializer.CreatedCustomerIds[0], _sqlInitializer.CreatedArticleIds[0], _sqlInitializer.CreatedArticleIds[0], "test customer article");
            var customerArticleId2 = SqlWriteHelper.InsertCustomerArticle(_sqlInitializer.CreatedCustomerIds[0], _sqlInitializer.CreatedArticleIds[0], _sqlInitializer.CreatedArticleIds[0], "test customer article");

            _sqlInitializer.CreatedCustomerArticleIds.Add(customerArticleId1);
            _sqlInitializer.CreatedCustomerArticleIds.Add(customerArticleId2);

            var request = CreateWebRequest("customerarticles?articleId=" + _sqlInitializer.CreatedArticleIds[0], "GET");
            using (var response = GetWebResponse(request))
            {
                var customerArticles = GetResponseContent<List<SearchableCustomerArticle>>(response);

                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(2, customerArticles.Count);
            }
        }

        #endregion

        #region Tests: customerarticles GET(1)

        [Test]
        public void GetCustomerArticle_GivenValidId_ShouldReturnCustomerArticle()
        {
            var customerArticleId = SqlWriteHelper.InsertCustomerArticle(_sqlInitializer.CreatedCustomerIds[0],
                                                                         _sqlInitializer.CreatedArticleIds[0],
                                                                         _sqlInitializer.CreatedArticleIds[0],
                                                                         "test customer article");
            _sqlInitializer.CreatedCustomerArticleIds.Add(customerArticleId);

            var request = CreateWebRequest("customerarticles/" + customerArticleId, "GET");
            using (var response = GetWebResponse(request))
            {
                var article = GetResponseContent<CustomerArticle>(response);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(article.Id, customerArticleId);
            }
        }

        [Test]
        public void GetCustomerArticle_GivenIdToDeletedArticle_ShouldReturnNotFound()
        {
            var customerArticleId = SqlWriteHelper.InsertCustomerArticle(_sqlInitializer.CreatedCustomerIds[0],
                                                                         _sqlInitializer.CreatedArticleIds[0],
                                                                         _sqlInitializer.CreatedArticleIds[0],
                                                                         "test customer article",
                                                                         isDeleted: 1);
            _sqlInitializer.CreatedCustomerArticleIds.Add(customerArticleId);

            var request = CreateWebRequest("customerarticles/" + customerArticleId, "GET");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
            }
        }

        [Test]
        public void GetCustomerArticle_GivenIdToNonExistingArticle_ShouldReturnNotFound()
        {
            var request = CreateWebRequest("customerarticles/" + int.MaxValue, "GET");
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
            }
        }

        #endregion

        #region Tests: customerarticles POST

        [Test]
        public void CreateCustomerArticle_GivenValidObject_ShouldReturnCreated()
        {
            var customerArticle = GetValidCustomerArticle();

            var request = CreateWebRequest("customerarticles/", "POST", customerArticle);
            using (var response = GetWebResponse(request))
            {
                var created = GetResponseContent<CustomerArticle>(response);

                _sqlInitializer.CreatedCustomerArticleIds.Add(created.Id);
                _sqlInitializer.CreatedArticleIds.Add(created.ArticleCopyId);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            }
        }

        [Test]
        public void CreateCustomerArticle_GivenValidObject_ShouldCreateCopyOfOriginalArticle()
        {
            var customerArticle = GetValidCustomerArticle();

            var request = CreateWebRequest("customerarticles/", "POST", customerArticle);
            using (var response = GetWebResponse(request))
            {
                var created = GetResponseContent<CustomerArticle>(response);

                _sqlInitializer.CreatedCustomerArticleIds.Add(created.Id);
                _sqlInitializer.CreatedArticleIds.Add(created.ArticleCopyId);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);

                Assert.Greater(created.ArticleCopyId, 0);
                Assert.AreNotEqual(created.ArticleCopyId, created.ArticleOriginalId);
            }
        }

        [Test]
        public void CreateCustomerArticle_ShouldCopyAttachments()
        {
            var article = GetArticleWithAttachments();
            var customerArticle = GetValidCustomerArticle();
            customerArticle.ArticleOriginalId = article.Id;

            var request = CreateWebRequest("customerarticles/", "POST", customerArticle);
            using (var response = GetWebResponse(request))
            {
                var created = GetResponseContent<CustomerArticle>(response);

                _sqlInitializer.CreatedCustomerArticleIds.Add(created.Id);
                _sqlInitializer.CreatedArticleIds.Add(created.ArticleCopyId);
                
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
                Assert.Greater(created.ArticleCopyId, 0);
                Assert.AreNotEqual(created.ArticleCopyId, created.ArticleOriginalId);
                
                var articleCopy = GetArticle<ClassroomArticle>(created.ArticleCopyId);
                Assert.AreEqual(2, articleCopy.Attachments.Count);

                _sqlInitializer.CreatedAttachmentIds.AddRange(articleCopy.Attachments.Select(a => a.Id));
            }
        }
        
        [Test]
        public void CreateCustomerArticle_GivenInvalidObject_ShouldReturnBadRequest()
        {
            var customerArticle = new CustomerArticle();

            var request = CreateWebRequest("customerarticles/", "POST", customerArticle);
            using (var response = GetWebResponse(request))
            {
                GetResponseContent<CustomerArticle>(response);
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        #endregion

        #region Tests: customerarticles/:id PUT

        [Test]
        public void UpdateCustomerArticle_GivenValidObject_ShouldUpdateObject()
        {
            var random = new Random();
            var fixedPrice = random.Next(100000);

            var customerArticle = GetCustomerArticleFromApi();
            customerArticle.FixedPrice = fixedPrice;

            var request = CreateWebRequest("customerarticles/" + customerArticle.Id, "PUT", customerArticle);
            using (var response = GetWebResponse(request))
            {
                var putResponse = GetResponseContent<CustomerArticle>(response);
                
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(fixedPrice, putResponse.FixedPrice);
                
                // Fetch the object from the database to verify that it actually was updated
                var updated = GetCustomerArticleFromApi(customerArticle.Id);
                Assert.AreEqual(fixedPrice, updated.FixedPrice);
            }
        }
        
        [Test]
        public void UpdateCustomerArticle_GivenInvalidObject_ShouldReturnBadRequest()
        {
            var customerArticle = GetCustomerArticleFromApi();
            customerArticle.CustomerId = int.MaxValue; // Point to non existing customer to make invalid

            var request = CreateWebRequest("customerarticles/" + customerArticle.Id, "PUT", customerArticle);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        [Test]
        public void UpdateCustomerArticle_UpdatingSameObjectTwice_ShouldReturnConflict()
        {
            var customerArticle = GetCustomerArticleFromApi();
            
            var request1 = CreateWebRequest("customerarticles/" + customerArticle.Id, "PUT", customerArticle);
            using (var response = GetWebResponse(request1))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }
            
            var request2 = CreateWebRequest("customerarticles/" + customerArticle.Id, "PUT", customerArticle);
            using (var response = GetWebResponse(request2))
            {
                Assert.AreEqual(HttpStatusCode.Conflict, response.StatusCode);    
            }
        }

        #endregion

        private Article GetArticleWithAttachments()
        {
            var initialArticle = GetArticle<ClassroomArticle>(_sqlInitializer.CreatedArticleIds.First());
            initialArticle.Attachments = new List<Attachment>
            {
                new Attachment { Title = "file1", FileId = _sqlInitializer.CreatedFileIds.First() },
                new Attachment { Title = "file2", FileId = _sqlInitializer.CreatedFileIds.First() }
            };

            var request = CreateWebRequest("articles/" + initialArticle.Id, "PUT", initialArticle);
            var response = GetWebResponse(request);
            var article = GetResponseContent<ClassroomArticle>(response);
            _sqlInitializer.CreatedAttachmentIds.AddRange(article.Attachments.Select(a => a.Id));
            return article;
        }

        private T GetArticle<T>(int articleId) where T : Article
        {
            var request = CreateWebRequest("articles/" + articleId, "GET");

            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<T>(response);
            }
        }

        private CustomerArticle GetValidCustomerArticle()
        {
            var customerArticle = new CustomerArticle
            {
                UnitPrice = 0,
                CurrencyCodeId = 262,
                ArticleOriginalId = _sqlInitializer.CreatedArticleIds[0],
                CustomerId = _sqlInitializer.CreatedCustomerIds[0],
            };
            return customerArticle;
        }

        private CustomerArticle GetCustomerArticleFromApi()
        {
            var customerArticleId = SqlWriteHelper.InsertCustomerArticle(_sqlInitializer.CreatedCustomerIds[0],
                                                                         _sqlInitializer.CreatedArticleIds[0],
                                                                         _sqlInitializer.CreatedArticleIds[0],
                                                                         "test customer article");
            _sqlInitializer.CreatedCustomerArticleIds.Add(customerArticleId);
            return GetCustomerArticleFromApi(customerArticleId);
        }

        private CustomerArticle GetCustomerArticleFromApi(int customerArticleId)
        {
            var request = CreateWebRequest("customerarticles/" + customerArticleId, "GET");
            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<CustomerArticle>(response);
            }
        }
    }
}