﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using Metier.Phoenix.Core.Data.Entities;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class ProductsTests : AuthorizedApiTestBase
    {
        [Test]
        public void GetProducts_ReturnsProductWithSpecifiedId()
        {
            var productId = _sqlInitializer.CreatedProductIds.First();
            var request = CreateWebRequest("products/" + productId, "GET");

            using (var response = GetWebResponse(request))
            {
                var result = GetResponseContent<Product>(response);
                Assert.AreEqual(result.Id, productId);
            }
        }

        [Test]
        public void GetProducts_ReturnsListOfAllProducts()
        {
            var request = CreateWebRequest("products", "GET");

            using (var response = GetWebResponse(request))
            {
                var result = GetResponseContent<List<Product>>(response);

                Assert.GreaterOrEqual(result.Count, 1);
            }
        }

        [Test]
        public void CreateProduct_ShouldCreateProduct()
        {
            var product = CreateValidProduct();
            var request = CreateWebRequest("products/", "POST", product);
            var response = GetWebResponse(request);

            var productCreated = GetResponseContent<Product>(response);
            _sqlInitializer.CreatedProductIds.Add(productCreated.Id);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public void UpdateProduct_ShouldUpdateProduct()
        {
            var postedProduct = GetProduct(_sqlInitializer.CreatedProductIds.First());
            postedProduct.Title = "New Title";
            postedProduct.IsActive = false;

            var request = CreateWebRequest("products/" + postedProduct.Id, "PUT", postedProduct);
            var response = GetWebResponse(request);

            var updatedProduct = GetResponseContent<Product>(response);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual("New Title", updatedProduct.Title);
        }

        [Test]
        public void UpdateProduct_GivenOldProduct_ShouldReturnConflict()
        {
            var initialProduct = GetProduct(_sqlInitializer.CreatedProductIds.First());
            initialProduct.IsActive = false;
            initialProduct.Title = "First title";

            var request = CreateWebRequest("products/" + initialProduct.Id, "PUT", initialProduct);
            var response = GetWebResponse(request);

            GetResponseContent<Product>(response);

            initialProduct.Title = "Second title";
            initialProduct.IsActive = false;

            request = CreateWebRequest("products/" + initialProduct.Id, "PUT", initialProduct);
            response = GetWebResponse(request);

            Assert.AreEqual(HttpStatusCode.Conflict, response.StatusCode);
        }

        [Test]
        public void GetProductType_GivenProductId_ShouldReturnProductTypeWithTemplateParts()
        {
            var productId = _sqlInitializer.CreatedProductIds.First();
            var request = CreateWebRequest("products/" + productId + "/producttype", "GET");
            var response = GetWebResponse(request);

            var productType = GetResponseContent<ProductType>(response);

            Assert.GreaterOrEqual(productType.ProductTemplateParts.Count, 0);
        }

        private Product GetProduct(int productId)
        {
            var request = CreateWebRequest("products/" + productId, "GET");

            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<Product>(response);
            }
        }

        private static Product CreateValidProduct()
        {
            var product = new Product
            {
                Title = "My product",
                ProductNumber = "0.75",
                ProductTypeId = SqlReadHelper.GetFirstProductTypeId(),
                ProductCategoryId = SqlReadHelper.GetFirstProductCategoryId(),
                ImageRef = "some image"
            };
            return product;
        }
    }
}