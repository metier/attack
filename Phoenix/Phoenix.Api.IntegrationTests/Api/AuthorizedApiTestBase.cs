﻿using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using Metier.Phoenix.Api.Facade.Account;
using Newtonsoft.Json;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    public abstract class AuthorizedApiTestBase : ApiTestBase
    {
        [SetUp]
        public void AuthorizedApiTestBaseSetup()
        {
            LogOn();
        }
    }
    
    public abstract class ApiTestBase
    {
        [SetUp]
        public void ApiTestBaseSetup()
        {
            AuthCookies = new CookieContainer();
            _sqlInitializer = new SqlInitializer();
            _sqlInitializer.Init();
        }

        [TearDown]
        public void ApiTestBaseSetupTeardown()
        {
            _sqlInitializer.Teardown();
        }

        protected CookieContainer AuthCookies;
        protected SqlInitializer _sqlInitializer;

        protected void LogOn(string userName = null)
        {
            if (userName == null)
            {
                userName = _sqlInitializer.IntegrationTestPhoenixUserName;
            }
            var logOnModel = new LogOn
            {
                Password = SqlWriteHelper.ImportedUserPassword
            };

            var request = CreateWebRequest("accounts/" + userName + "/logon", "POST", logOnModel);
            request.CookieContainer = new CookieContainer();
            var response = GetWebResponse(request);
            AuthCookies = new CookieContainer();
            AuthCookies.Add(response.Cookies.Cast<Cookie>().FirstOrDefault(c => c.Name == "PhoenixAuth"));
        }

        protected string ApiRoot
        {
            get { return ConfigurationManager.AppSettings["ApiRoot"]; }
        }

        protected HttpWebResponse GetWebResponse(HttpWebRequest httpWebRequest)
        {
            try
            {
                return (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    var webResponse = (HttpWebResponse)e.Response;
                    return webResponse;
                }
                throw;
            }
        }

        protected T GetResponseContent<T>(HttpWebResponse response)
        {
            var responseValue = GetResponseString(response);
            return JsonConvert.DeserializeObject<T>(responseValue);
        }

        protected string GetResponseString(HttpWebResponse response)
        {
            var responseValue = string.Empty;
            using (var responseStream = response.GetResponseStream())
            {
                if (responseStream != null)
                {
                    using (var reader = new StreamReader(responseStream))
                    {
                        responseValue = reader.ReadToEnd();
                    }
                }
            }
            return responseValue;
        }

        protected HttpWebRequest CreateWebRequest(string resource, string method, object content = null, string contentType = "application/json")
        {
            var request = (HttpWebRequest)WebRequest.Create(ApiRoot + resource);
            request.Method = method;
            request.ContentType = contentType;
            request.CookieContainer = AuthCookies;

            if (content == null)
            {
                request.ContentLength = 0;
            }
            else
            {
                var json = JsonConvert.SerializeObject(content, new JsonSerializerSettings
                {
                    StringEscapeHandling = StringEscapeHandling.EscapeNonAscii
                });
                request.ContentLength = json.Length;
                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(json);
                }
            }
            return request;
        }
    }
}