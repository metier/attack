﻿using System;
using System.Net;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class ReportsTests : AuthorizedApiTestBase
    {
        [Test]
        [TestCase("reports/progressstatus/xlsx", "progress-status-e-learning.xlsx")]
        [TestCase("reports/activityparticipants/xlsx", "activity-participants.xlsx")]
        [TestCase("reports/productcoursestatus/xlsx", "participations-status.xlsx")]
        [TestCase("reports/potentialparticipations/xlsx", "potential-participations.xlsx")]
        [TestCase("reports/ordersforinvoicing/xlsx", "orders-for-invoicing.xlsx")]
        [TestCase("reports/allparticipations/xlsx", "all-participations.xlsx")]
        [TestCase("reports/questions/xlsx", "questions.xlsx")]
        [TestCase("reports/exams/xlsx", "exams.xlsx")]
        public void GetReport_GivenCustomerId_ShouldReturnOctetStreamWithFilename(string url, string filename)
        {
            var customerId = SqlReadHelper.GetFirstCustomerId();
            url += "?customerId=" + customerId;

            if (filename == "exams.xlsx")
            {
                var dateFilter = DateTime.Now.AddMonths(-1);
                url += "&fromDate=" + dateFilter.Year + "-" + dateFilter.Month + "-" + dateFilter.Day;
            }

            var request = CreateWebRequest(url, "GET", null, "application/octet-stream");
            var response = GetWebResponse(request);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            var header = response.GetResponseHeader("Content-Disposition");
            Assert.AreEqual(string.Format("attachment; filename={0}", filename), header);
        }

        [Test]
        public void GetPotentialParticipationsReport_GivenNoCustomerId_ShouldReturnBadRequest()
        {
            var request = CreateWebRequest("reports/potentialparticipations/xlsx", "GET", null, "application/octet-stream");
            var response = GetWebResponse(request);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}