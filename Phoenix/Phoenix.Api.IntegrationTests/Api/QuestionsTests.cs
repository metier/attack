﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;
using NUnit.Framework;
using Phoenix.Api.IntegrationTests.Sql;

namespace Phoenix.Api.IntegrationTests.Api
{
    public class QuestionsTests : AuthorizedApiTestBase
    {
        [Test]
        public void GetQuestion_ShouldReturnQuestion()
        {
            var question = GetQuestionFromService();

            Assert.IsNotNull(question);
            Assert.Greater(question.Id, 0);
        }

        [Test]
        public void SearchQuestions_ShouldReturnPartialListOfSearchableQuestions()
        {
            _sqlInitializer.CreatedQuestionIds.Add(SqlWriteHelper.InsertQuestion(QuestionTypes.TrueFalse));

            var request = CreateWebRequest("questions?query=", "GET");
            var response = GetWebResponse(request);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var result = GetResponseContent<PartialList<SearchableQuestion>>(response);
            Assert.IsNotNull(result);
            Assert.Greater(result.TotalCount, 0);
        }

        [Test]
        public void CreateQuestion_ShouldCreateQuestionWithOptionsAndProducts()
        {
            var question = GetValidQuestion();
            question.QuestionOptions.Add(new QuestionOption
            {
                IsCorrect = true,
                Text = "First option"
            });
            
            var request = CreateWebRequest("questions/", "POST", question);
            var response = GetWebResponse(request);
            
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);

            var created = GetResponseContent<Question>(response);
            
            Assert.IsNotNull(created);
            Assert.Greater(created.Id, 0);
            Assert.AreEqual(1, created.QuestionOptions.Count);
            Assert.AreEqual(1, created.Products.Count);
            Assert.AreEqual(question.Products[0].Id, created.Products[0].Id);

            _sqlInitializer.CreatedQuestionIds.Add(created.Id);
        }

        [Test]
        public void UpdateQuestion_ShouldUpdateQuestion()
        {
            var question = GetQuestionFromService();
            question.Products.Add(new Product { Id = SqlReadHelper.GetFirstProductId() });
            question.Rcos.Add(new Rco { Id = SqlReadHelper.GetFirstCourseRcoId() });
            question.Text = "updated text";

            var request = CreateWebRequest("questions/" + question.Id, "PUT", question);
            var response = GetWebResponse(request);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            var updated = GetResponseContent<Question>(response);

            Assert.AreEqual(question.Id, updated.Id);
            Assert.AreEqual(question.Text, updated.Text);
            Assert.AreEqual(1, updated.Products.Count);
            Assert.AreEqual(1, updated.Rcos.Count);
        }
        
        private Question GetQuestionFromService(int? id = null)
        {
            if (id == null)
            {
                id = SqlWriteHelper.InsertQuestion(QuestionTypes.TrueFalse);
                _sqlInitializer.CreatedQuestionIds.Add(id.Value);
            }
            
            var request = CreateWebRequest("questions/" + id, "GET");
            var response = GetWebResponse(request);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            return GetResponseContent<Question>(response);
        }

        private Question GetValidQuestion()
        {
            return new Question
            {
                LanguageId = SqlReadHelper.GetFirstLanguageId(),
                Type = QuestionTypes.MultipleChoiceSingle,
                Difficulty = QuestionDifficulties.Medium,
                Text = "A medium hard question",
                Rcos = new List<Rco> { new Rco { Id = SqlReadHelper.GetFirstCourseRcoId() } },
                Products = new List<Product>() { new Product { Id = SqlReadHelper.GetFirstProductId() } }
            };
        }
    }
}