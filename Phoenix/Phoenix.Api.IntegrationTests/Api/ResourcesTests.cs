﻿using System;
using System.Linq;
using System.Net;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Utilities.Collections;
using NUnit.Framework;

namespace Phoenix.Api.IntegrationTests.Api
{
    [TestFixture]
    public class ResourcesTests : AuthorizedApiTestBase
    {
        [Test]
        public void GetResource_GivenExistingResourceId_ShouldReturnResource()
        {
            var resourceId = _sqlInitializer.CreatedResourceIds.First();
            var request = CreateWebRequest("resources/" + resourceId, "GET");

            using (var response = GetWebResponse(request))
            {
                var resource = GetResponseContent<Resource>(response);
                Assert.AreEqual(resourceId, resource.Id);
            }
        }

        [Test]
        public void GetResource_GivenExistingResourceId_ShouldReturnOk()
        {
            var resourceId = _sqlInitializer.CreatedResourceIds.First();
            var request = CreateWebRequest("resources/" + resourceId, "GET");

            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }
        }

        [Test]
        public void GetResource_GivenInvalidResourceId_ShouldReturnNotFound()
        {
            const int resourceId = int.MaxValue;
            var request = CreateWebRequest("resources/" + resourceId, "GET");

            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
            }
        }

        [Test]
        public void SearchResource_EmptyQuery_ShouldReturnUnfiltered()
        {
            var request = CreateWebRequest("resources?", "GET");

            using (var response = GetWebResponse(request))
            {
                var resources = GetResponseContent<PartialList<Resource>>(response);
                Assert.Greater(resources.Items.Count, 0);
            }
        }

        [Test]
        public void CreateResource_GivenValidResource_ShouldCreateNewResource()
        {
            var validResource = CreateValidResource();
            var request = CreateWebRequest("resources/", "POST", validResource);
            using (var response = GetWebResponse(request))
            {
                var resource = GetResponseContent<Resource>(response);

                _sqlInitializer.CreatedResourceIds.Add(resource.Id);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            }
        }

        [Test]
        public void UpdateResource_GivenExistingResource_ShouldReturnStatusOk()
        {
            var resource = GetResource();
            resource.Contact = Guid.NewGuid().ToString();

            var request = CreateWebRequest("resources/", "PUT", resource);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }
        }

        [Test]
        public void UpdateResource_GivenExistingResource_ShouldReturnUpdatedResource()
        {
            var resource = GetResource();
            resource.Contact= Guid.NewGuid().ToString();

            var request = CreateWebRequest("resources/", "PUT", resource);
            using (var response = GetWebResponse(request))
            {
                var updatedResource = GetResponseContent<Resource>(response);
                Assert.AreEqual(resource.Contact, updatedResource.Contact);
            }
        }

        [Test]
        public void UpdateResource_InvalidFieldChangeToResource_ReturnsBadRequest()
        {
            var resource = GetResource();
            resource.Name = null;

            var request = CreateWebRequest("resources/", "PUT", resource);
            using (var response = GetWebResponse(request))
            {
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
        }

        private Resource GetResource()
        {
            var resourceId = _sqlInitializer.CreatedResourceIds.First();
            var request = CreateWebRequest("resources/" + resourceId, "GET");

            using (var response = GetWebResponse(request))
            {
                return GetResponseContent<Resource>(response);
            }
        }

        private Resource CreateValidResource()
        {
            return new Resource
            {
                Name = "MyTestResource" + Guid.NewGuid(),
                ResourceTypeId = 2
            };
        }
    }
}