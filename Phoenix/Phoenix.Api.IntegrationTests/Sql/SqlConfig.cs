﻿using System.Configuration;

namespace Phoenix.Api.IntegrationTests.Sql
{
    public static class SqlConfig
    {
        public static string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["MetierLmsContext"].ConnectionString; }
        }
    }
}