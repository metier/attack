﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;

namespace Phoenix.Api.IntegrationTests.Sql
{
    public class SqlInitializer
    {
        public List<int> CreatedProductIds { get; set; }
        public List<int> CreatedProductDescriptionIds { get; set; }
        public List<int> CreatedCustomerIds { get; set; }
        public List<int> CreatedArticleIds { get; set; }
        public List<int> CreatedCustomerArticleIds { get; set; }
        public List<int> CreatedAttachmentIds { get; set; }
        public List<string> CreatedUsernames { get; set; }
        public List<int> CreatedUserIds { get; set; }
        public List<int> CreatedHelpTextIds { get; set; }
        public List<int> CreatedCourseProgramIds { get; set; }
        public List<int> CreatedResourceIds { get; set; }
        public List<int> CreatedCommentIds { get; set; }
        public List<int> CreatedActivityIds { get; set; }
        public List<int> CreatedActivitySetIds { get; set; }
        public List<int> CreatedParticipantIds { get; set; }
        public List<int> CreatedCustomLeadingTextIds { get; set; }
        public List<int> CreatedOrderIds { get; set; }
        public List<string> CreatedScormSessionIds { get; set; }
        public List<int> CreatedScormPerformanceIds { get; set; }
        public List<int> CreatedFileIds { get; set; }
        public List<int> CreatedUserCompetenceIds { get; set; }
        public List<int> CreatedQuestionIds { get; set; }

        public int ProductIdWithDescriptionAndArticle { get; set; }
        public int ProductIdEmpty { get; set; }
        public string IntegrationTestLearningPortalTokenUserName { get; set; }
        public string IntegrationTestPhoenixUserName { get; set; }
        public int DistributorIdWithoutExternalCustomers = 1264610;
        public int CaseExamActivityId { get; set; }
        
        public SqlInitializer()
        {
            CreatedProductIds = new List<int>();
            CreatedProductDescriptionIds = new List<int>();
            CreatedCustomerIds = new List<int>();
            CreatedArticleIds = new List<int>();
            CreatedCustomerArticleIds = new List<int>();
            CreatedAttachmentIds = new List<int>();
            CreatedUsernames = new List<string>();
            CreatedUserIds = new List<int>();
            CreatedHelpTextIds = new List<int>();
            CreatedCourseProgramIds = new List<int>();
            CreatedResourceIds = new List<int>();
            CreatedCommentIds = new List<int>();
            CreatedActivityIds = new List<int>();
            CreatedActivitySetIds = new List<int>();
            CreatedParticipantIds = new List<int>();
            CreatedCustomLeadingTextIds = new List<int>();
            CreatedOrderIds = new List<int>();
            CreatedScormSessionIds = new List<string>();
            CreatedScormPerformanceIds = new List<int>();
            CreatedFileIds = new List<int>();
            CreatedUserCompetenceIds = new List<int>();
            CreatedQuestionIds = new List<int>();
        }

        public void Init()
        {
            var customerId = SqlWriteHelper.InsertCustomer(DistributorIdWithoutExternalCustomers, SqlReadHelper.GetFirstLanguageId());
            var metierNorwayCustomer = SqlWriteHelper.InsertCustomer(1194, SqlReadHelper.GetFirstLanguageId());
            ProductIdEmpty = SqlWriteHelper.InsertProduct(SqlReadHelper.GetFirstProductTypeId(), SqlReadHelper.GetFirstProductCategoryId());
            ProductIdWithDescriptionAndArticle = SqlWriteHelper.InsertProduct(SqlReadHelper.GetFirstProductTypeId(), SqlReadHelper.GetFirstProductCategoryId());
            var productDescriptionId = SqlWriteHelper.InsertProductDescription(ProductIdWithDescriptionAndArticle, SqlReadHelper.GetFirstLanguageId(), SqlReadHelper.GetFirstCustomerId());
            var elearningArticleId = SqlWriteHelper.InsertArticle("E-Learning", ProductIdWithDescriptionAndArticle, productDescriptionId, SqlReadHelper.GetFirstLanguageId());
            var classroomArticleId = SqlWriteHelper.InsertArticle("Classroom", ProductIdWithDescriptionAndArticle, productDescriptionId, SqlReadHelper.GetFirstLanguageId());
            var articleForCustomer = SqlWriteHelper.InsertArticle("E-Learning", ProductIdWithDescriptionAndArticle, productDescriptionId, SqlReadHelper.GetFirstLanguageId());
            var caseExamArticleId = SqlWriteHelper.InsertArticle("Case Exam", ProductIdWithDescriptionAndArticle, productDescriptionId, SqlReadHelper.GetFirstLanguageId());
            var resourceId = SqlWriteHelper.InsertResource(SqlReadHelper.GetFirstResourceTypeId());
            var commentId = SqlWriteHelper.InsertComment(CreateUser());
            IntegrationTestPhoenixUserName = SqlWriteHelper.InsertMembershipUser(Guid.NewGuid().ToString(), SqlWriteHelper.HashedImportedUserPassword, SqlWriteHelper.ImportedUserPasswordSalt, PhoenixRoles.PhoenixUser);
            IntegrationTestLearningPortalTokenUserName = SqlWriteHelper.InsertMembershipUser(Guid.NewGuid().ToString(), SqlWriteHelper.HashedImportedUserPassword, SqlWriteHelper.ImportedUserPasswordSalt, PhoenixRoles.LearningPortalTokenUser);
            var courseProgramId = InsertCourseProgram(customerId);
            var customerArticleId = SqlWriteHelper.InsertCustomerArticle(customerId, articleForCustomer, articleForCustomer, "Whatever", 0, 0 ,262, null, SqlReadHelper.GetLastCourseModuleId(), 0);
            var customerArticleCaseExamId = SqlWriteHelper.InsertCustomerArticle(customerId, caseExamArticleId, caseExamArticleId, "Whatever", 0, 0 ,262, SqlReadHelper.GetLastCourseExamId(),null, 0);
            var activityId = SqlWriteHelper.InsertActivity(customerArticleId);
            CaseExamActivityId = SqlWriteHelper.InsertActivity(customerArticleCaseExamId);
            var activitySetId = SqlWriteHelper.InsertActivitySet(customerId);
            var participantId = SqlWriteHelper.InsertParticipant(SqlReadHelper.GetFirstUserId(), customerId, activityId, activitySetId);
            var participantId2 = SqlWriteHelper.InsertParticipant(SqlReadHelper.GetSecondUserId(), customerId, activityId, activitySetId);
            var customLeadingTextId = SqlWriteHelper.InsertCustomLeadingText(customerId);
            CreatedFileIds.Add(SqlWriteHelper.InsertFile());
            CreatedCourseProgramIds.Add(courseProgramId);
            CreatedCustomerIds.Add(customerId);
            CreatedCustomerIds.Add(metierNorwayCustomer);
            CreatedProductIds.Add(ProductIdEmpty);
            CreatedProductIds.Add(ProductIdWithDescriptionAndArticle);
            CreatedProductDescriptionIds.Add(productDescriptionId);
            CreatedArticleIds.Add(elearningArticleId);
            CreatedArticleIds.Add(classroomArticleId);
            CreatedArticleIds.Add(articleForCustomer);
            CreatedArticleIds.Add(caseExamArticleId);
            CreatedResourceIds.Add(resourceId);
            CreatedCommentIds.Add(commentId);
            CreatedUsernames.Add(IntegrationTestPhoenixUserName);
            CreatedUsernames.Add(IntegrationTestLearningPortalTokenUserName);
            CreatedCustomerArticleIds.Add(customerArticleId);
            CreatedCustomerArticleIds.Add(customerArticleCaseExamId);
            CreatedActivityIds.Add(activityId);
            CreatedActivityIds.Add(CaseExamActivityId);
            CreatedActivitySetIds.Add(activitySetId);
            CreatedParticipantIds.Add(participantId);
            CreatedParticipantIds.Add(participantId2);
            CreatedCustomLeadingTextIds.Add(customLeadingTextId);
        }

        public int CreateUser()
        {
            int userId;
            var oracleImportUser = SqlWriteHelper.InsertMembershipUser(Guid.NewGuid().ToString(), SqlWriteHelper.HashedImportedUserPassword, SqlWriteHelper.ImportedUserPasswordSalt, out userId);
            CreatedUsernames.Add(oracleImportUser);
            CreatedUserIds.Add(userId);
            return userId;
        }

        public int InsertOrder(int? customerId = null, OrderStatus status = OrderStatus.Draft, bool? isCreditNote = null, int? creditedOrderId = null)
        {
            return InsertOrder(new List<Participant>(), new List<Activity>(), customerId, status, isCreditNote, creditedOrderId);
        }

        public int InsertOrder(List<Participant> participants, List<Activity> activities, int? customerId = null, OrderStatus status = OrderStatus.Draft, bool? isCreditNote = null, int? creditedOrderId = null)
        {
            if (!customerId.HasValue)
            {
                customerId = CreatedCustomerIds.First();
            }

            var orderId = SqlWriteHelper.InsertOrder(participants, activities, CreatedUserIds.First(), customerId.Value, status, isCreditNote, creditedOrderId);
            CreatedOrderIds.Add(orderId);
            return orderId;
        }

        public int InsertOrder(Participant participant, Activity activity, int? customerId = null, OrderStatus status = OrderStatus.Draft, bool? isCreditNote = null, int? creditedOrderId = null)
        {
            if (!customerId.HasValue)
            {
                customerId = CreatedCustomerIds.First();
            }

            var orderId = SqlWriteHelper.InsertOrder(participant, activity, CreatedUserIds.First(), customerId.Value, status, isCreditNote, creditedOrderId);
            CreatedOrderIds.Add(orderId);
            return orderId;
        }

        public int InsertActivity()
        {
            var activityId = SqlWriteHelper.InsertActivity(CreatedCustomerArticleIds.First());
            CreatedActivityIds.Add(activityId);
            return activityId;
        }

        public int InsertMultipleChoiceExamActivity(string name = "Multiple Choice")
        {
            var articleId = SqlWriteHelper.InsertArticle(name, 87, 354, 45);
            var customerArticleId = SqlWriteHelper.InsertCustomerArticle(CreatedCustomerIds.First(), articleId, articleId, name);
            var activityId = SqlWriteHelper.InsertActivity(customerArticleId);

            CreatedArticleIds.Add(articleId);
            CreatedCustomerArticleIds.Add(customerArticleId);
            CreatedActivityIds.Add(activityId);

            return activityId;
        }

        public int InsertExam(int participantId, DateTime created)
        {
            var examId = SqlWriteHelper.InsertExam(participantId, created);
            return examId;
        }

        private static int InsertCourseProgram(int customerId)
        {
            var exams = new List<CourseExam>
            {
                new CourseExam {Name = "Exam 1"},
            };
            var modules = new List<CourseModule>
            {
                new CourseModule {Name = "Module 1", Order = 1},
            };
            var steps = new List<CourseStep>
            {
                new CourseStep
                {
                    Name = "Step 1",
                    Order = 1,
                    Exams = exams,
                    Modules = modules
                }
            };

            return SqlWriteHelper.InsertCourseProgram("Test course program", customerId, steps: steps);
        }

        public void Teardown()
        {
            CreatedQuestionIds.ForEach(SqlWriteHelper.DeleteQuestion);
            CreatedScormPerformanceIds.ForEach(SqlWriteHelper.DeleteScormPerformance);
            CreatedScormSessionIds.ForEach(SqlWriteHelper.DeleteScormSession);
            CreatedScormSessionIds.ForEach(SqlWriteHelper.DeleteScormSession);

            CreatedOrderIds.OrderByDescending(id=> id).ToList().ForEach(SqlWriteHelper.DeleteOrder);
            CreatedParticipantIds.ForEach(SqlWriteHelper.DeleteParticipant);
            CreatedActivityIds.ForEach(SqlWriteHelper.DeleteActivity);
            CreatedActivitySetIds.ForEach(SqlWriteHelper.DeleteActivitySet);
            CreatedCustomerArticleIds.ForEach(SqlWriteHelper.DeleteCustomerArticle);
            CreatedArticleIds.ForEach(SqlWriteHelper.DeleteArticle);

            CreatedCourseProgramIds.ForEach(SqlWriteHelper.DeleteCourseProgram);
            CreatedCommentIds.ForEach(SqlWriteHelper.DeleteComment);
            CreatedCustomLeadingTextIds.ForEach(SqlWriteHelper.DeleteCustomLeadingText);

            CreatedProductIds.ForEach(SqlWriteHelper.DeleteProduct);
            CreatedProductDescriptionIds.ForEach(SqlWriteHelper.DeleteProductDescription);

            CreatedUserCompetenceIds.ForEach(SqlWriteHelper.DeleteUserCompetence);
            CreatedAttachmentIds.ForEach(SqlWriteHelper.DeleteAttachment);
            CreatedHelpTextIds.ForEach(SqlWriteHelper.DeleteHelpText);
            CreatedResourceIds.ForEach(SqlWriteHelper.DeleteResource);

            CreatedUsernames.ForEach(SqlWriteHelper.DeleteUser);
            CreatedUserIds.ForEach(SqlWriteHelper.DeleteUser);

            CreatedCustomerIds.ForEach(SqlWriteHelper.DeleteCustomer);
            CreatedFileIds.ForEach(SqlWriteHelper.DeleteFile);
           
        }

        public void SetParticipantDeleted(int participantId)
        {
            SqlWriteHelper.SetParticipantDeleted(participantId);
        }

        public void SetOrderStatus(int orderId, OrderStatus orderStatus)
        {
            SqlWriteHelper.SetOrderStatus(orderId, orderStatus);
        }
    }
}