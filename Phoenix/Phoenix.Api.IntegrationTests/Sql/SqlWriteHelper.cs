﻿using System;
using System.Collections.Generic;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Newtonsoft.Json;

namespace Phoenix.Api.IntegrationTests.Sql
{
    public class SqlWriteHelper
    {
        public static readonly string ImportedUserPassword = "evrytest";
        public static readonly string HashedImportedUserPassword = "ccjB52Oj03hLk++ox5YwSQ==";
        public static readonly string ImportedUserPasswordSalt = "rTWLKuLcBqsQYNS1NOWXRw";

        #region Delete 

        public static void DeleteProductDescriptionTexts(int productDescriptionId)
        {
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [ProductDescriptionTexts] WHERE ProductDescriptionId = {0}", productDescriptionId));
        }

        public static void DeleteProductDescription(int productDescriptionId)
        {
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [ProductDescriptions] WHERE Id = {0}", productDescriptionId));
        }

        public static void DeleteProduct(int productId)
        {
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Products] WHERE Id = {0}", productId));
        }

        public static void DeleteCustomer(int customerId)
        {
            SqlClient.ExecuteScalarNoReturn($"UPDATE Customers SET ParticipantInfoDefinitionId = null, AutomailDefinitionId = null, CustomerInvoicingId = null WHERE id = {customerId}");
            
            SqlClient.ExecuteScalarNoReturn($"DELETE p FROM [ParticipantInfoDefinitions] p INNER JOIN [Customers] c ON c.ParticipantInfoDefinitionId = p.Id WHERE c.Id = {customerId}");
            SqlClient.ExecuteScalarNoReturn($"DELETE p FROM [AutomailDefinitions] p INNER JOIN [Customers] c ON c.AutomailDefinitionId = p.Id WHERE c.Id = {customerId}");
            SqlClient.ExecuteScalarNoReturn($"DELETE p FROM [CustomerInvoicings] p INNER JOIN [Customers] c ON c.CustomerInvoicingId = p.Id WHERE c.Id = {customerId}");

            SqlClient.ExecuteScalarNoReturn($"DELETE FROM [CustomerAddresses] WHERE CustomerId = {customerId}");
            SqlClient.ExecuteScalarNoReturn($"DELETE FROM [CustomLeadingTexts] WHERE CustomerId = {customerId}");
            SqlClient.ExecuteScalarNoReturn($"DELETE FROM [CustomerContactPersons] WHERE CustomerId = {customerId}");
            SqlClient.ExecuteScalarNoReturn($"DELETE FROM [MetierContactPersons] WHERE CustomerId = {customerId}");
            
            SqlClient.ExecuteScalarNoReturn($"DELETE FROM [Customers] WHERE id = {customerId}");
        }

        public static void DeleteArticle(int articleId)
        {
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Article_CaseExams] WHERE id = {0}", articleId));
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Article_Classrooms] WHERE id = {0}", articleId));
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Article_Elearnings] WHERE id = {0}", articleId));
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Article_ExamCourses] WHERE id = {0}", articleId));
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Article_Exams] WHERE id = {0}", articleId));
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Article_MockExams] WHERE id = {0}", articleId));
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Article_Others] WHERE id = {0}", articleId));
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Article_Seminars] WHERE id = {0}", articleId));
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Article_Webinars] WHERE id = {0}", articleId));
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Articles] WHERE id = {0}", articleId));
        }

        public static void DeleteAttachment(int attachmentId)
        {
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Attachments] WHERE Id = {0}", attachmentId));
        }
        
        public static void DeleteFile(int fileId)
        {
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Files] WHERE Id = {0}", fileId));
        }

        public static void DeleteUser(int userId)
        {
            var username = SqlReadHelper.GetUsernameForUserId(userId);
            DeleteUser(username);
        }

        public static void DeleteUser(string username)
        {
            SqlClient.ExecuteScalarNoReturn(string.Format("UPDATE orders SET CoordinatorId = Null WHERE CoordinatorId = (SELECT TOP 1 Id FROM [Users] WHERE MembershipUserId = '{0}')", username));
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM UserInfoElements WHERE UserId = (SELECT TOP 1 Id FROM [Users] WHERE MembershipUserId = '{0}')", username));
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [ChangeLog]  WHERE UserId = (SELECT TOP 1 Id FROM [Users] WHERE MembershipUserId = '{0}')", username));
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [aspnet_Membership]  WHERE UserId = (SELECT TOP 1 UserId FROM [aspnet_Users] WHERE UserName = '{0}')", username));
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE uir FROM [aspnet_UsersInRoles] uir JOIN [aspnet_Users] u on u.UserId = uir.UserId WHERE u.UserName = '{0}'", username));
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [aspnet_Users]  WHERE UserName = '{0}'", username));
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Users]  WHERE MembershipUserId = '{0}'", username));
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [AuthenticationTokens]  WHERE UserName = '{0}'", username));
        }

        public static void DeleteUserCompetence(int userCompetenceId)
        {
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [UserCompetences] WHERE Id = {0}", userCompetenceId));
        }

        public static void DeleteHelpText(int helpTextId)
        {
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [HelpTexts] WHERE id = {0}", helpTextId));
        }

        public static void DeleteCourseProgram(int courseProgramId)
        {
            var stepIdSql = $"SELECT ID FROM [CourseSteps] WHERE CourseProgramId = {courseProgramId}";

            SqlClient.ExecuteScalarNoReturn($"DELETE FROM [CourseModules] WHERE CourseStepId in ({stepIdSql})");
            SqlClient.ExecuteScalarNoReturn($"DELETE FROM [CourseExams] WHERE CourseStepId in ({stepIdSql})");

            SqlClient.ExecuteScalarNoReturn($"DELETE FROM [CourseSteps] WHERE courseProgramid = {courseProgramId}");
            SqlClient.ExecuteScalarNoReturn($"DELETE FROM [CoursePrograms] WHERE id = {courseProgramId}");
        }

        public static void DeleteCustomerArticle(int customerArticleId)
        {
            DeleteCustomInformation("CustomerArticle", customerArticleId);
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [CustomerArticles] WHERE id = {0}", customerArticleId));
        }

        public static void DeleteResource(int resourceId)
        {
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Resources] WHERE Id = {0}", resourceId));
        }

        public static void DeleteComment(int commentId)
        {
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Comments] WHERE Id = {0}", commentId));
        }

        public static void DeleteActivity(int activityId)
        {
            DeleteCustomInformation("Activity", activityId);
            SqlClient.ExecuteScalarNoReturn($"DELETE FROM [ActivitySets_Activities] WHERE ActivityId = {activityId}");
            SqlClient.ExecuteScalarNoReturn($"DELETE FROM [ActivityPeriods] WHERE ActivityId = {activityId}");
            SqlClient.ExecuteScalarNoReturn($"DELETE FROM [Activities] WHERE Id = {activityId}");
        }

        public static void DeleteActivitySet(int activitySetId)
        {
            SqlClient.ExecuteScalarNoReturn($"DELETE FROM [ActivitySets_Activities] WHERE ActivitySetId = {activitySetId}");
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [ActivitySets] WHERE Id = {0}", activitySetId));
        }

        public static void DeleteParticipant(int participantId)
        {
            SqlClient.ExecuteScalarNoReturn($"DELETE FROM [ParticipantInfoElements] WHERE ParticipantId = {participantId}");
            SqlClient.ExecuteScalarNoReturn($"DELETE FROM [Participants_Attachments] WHERE ParticipantId = {participantId}");
            SqlClient.ExecuteScalarNoReturn($"DELETE FROM [Exams] WHERE ParticipantId = {participantId}");
            SqlClient.ExecuteScalarNoReturn($"DELETE FROM [Participants] WHERE Id = {participantId}");
        }
        
        public static void DeleteCustomLeadingText(int customLeadingTextId)
        {
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [CustomLeadingTexts] WHERE Id = {0}", customLeadingTextId));
        }

        public static void DeleteOrder(int createdOrderId)
        {
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Orders_Activities] WHERE OrderId = {0}", createdOrderId));
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Orders_Participants] WHERE OrderId = {0}", createdOrderId));
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Orders] WHERE Id = {0}", createdOrderId));
        }

        public static void DeleteScormSession(string sessionId)
        {
            // Will cascade delete attempt
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE p FROM [ScormPerformances] p " +
                                                              "JOIN [ScormAttempts] a ON a.PerformanceId = p.Id WHERE a.SessionId = '{0}'", sessionId));
        }

        public static void DeleteScormPerformance(int scormPerformanceId)
        {
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [ScormPerformances] WHERE Id = {0}", scormPerformanceId));
        }

        public static void DeleteQuestion(int questionId)
        {
            SqlClient.ExecuteScalarNoReturn(string.Format("DELETE FROM [Questions] WHERE Id = {0}", questionId));
        }

        #endregion


        #region Insert
        
        public static int InsertProduct(int productTypeId, int productCategoryId)
        {
            return SqlClient.ExecuteScalar("INSERT INTO [Products] ([ProductNumber],[Title],[ImageRef],[ProductTypeId], [ProductCategoryId]) " + 
                $"VALUES (1, 'The product', null, {productTypeId}, {productCategoryId}) " + "SELECT SCOPE_IDENTITY()");
        }


        public static int InsertProductDescription(int productId, int languageId, int customerId)
        {
            return SqlClient.ExecuteScalar(string.Format(
                            "INSERT INTO [ProductDescriptions] ([ProductId],[LanguageId],[Title],[CustomerId],[IsDeleted],[Created],[LastModified],[AttachmentId]) " +
                            "VALUES ({0}, {1}, 'The product description', {2}, 0, getutcdate(), getutcdate(), null) " +
                            "SELECT SCOPE_IDENTITY()", productId, languageId, customerId));
        }

        public static int InsertProductDescriptionText(int productDescriptionId, int templatePartId)
        {
            return SqlClient.ExecuteScalar(string.Format("INSERT INTO [ProductDescriptionTexts] ([ProductDescriptionId],[ProductTemplatePartId],[Text]) " +
                                                          "VALUES ({0}, {1}, 'Product description text') " +
                                                          "SELECT SCOPE_IDENTITY()", productDescriptionId, templatePartId));
        }

        public static int InsertCustomer(int distributorId, int languageId, bool isExternal = true)
        {
            var externalCustomerId = isExternal ? " newId()" : " null";
            var autoDefId = SqlClient.ExecuteScalar("INSERT INTO [dbo].[AutomailDefinitions] ([IsWelcomeNotification],[IsProgressNotificationElearning],[IsEnrollmentNotification],[IsReminder],[IsExcludeProgressSchedule]) VALUES (1,1,1,1,0) SELECT SCOPE_IDENTITY()");
            var custInvId = InsertCustomerInvoicings();
            var partInDefId = InsertParticipantInfoDefinitions();
            int customerId = SqlClient.ExecuteScalar("INSERT INTO [Customers] ([IsDeleted],[ParentId],[Name],[IsRoot],[Created],[LastModified],[DistributorId],[LanguageId],[ParticipantInfoDefinitionId],[ExternalCustomerId],[AutomailDefinitionId], [CustomerInvoicingId]) " + 
                $"VALUES (0, null, 'Test customer', 1, getutcdate(), getutcdate(), {distributorId}, {languageId}, {partInDefId}, {externalCustomerId}, {autoDefId}, {custInvId}) " + "SELECT SCOPE_IDENTITY()");

            InsertCustomLeadingTexts(customerId);

            return customerId;
        }

        public static int InsertCustomerArticle(int customerId, int articleCopyId, int articleOriginalId, string name, decimal fixedPrice = 0, decimal unitPrice = 0, int currencyCodeId = 262, int? examId = null, int? moduleId = null, int? isDeleted = 0, int fixedPriceNotBillable = 1, int unitPriceNotBillable = 0)
        {
            var sql = "INSERT INTO [CustomerArticles] ([ExamId],[ModuleId],[CustomerId],[ArticleCopyId],[ArticleOriginalId],[UnitPrice],[FixedPrice],[CurrencyCodeId],[IsDeleted],[Created], [LastModified], [FixedPriceNotBillable], [UnitPriceNotBillable]) " + 
                $"VALUES ({examId?.ToString() ?? "null"}, {moduleId?.ToString() ?? "null"}, {customerId}, {articleCopyId}, {articleOriginalId}, {unitPrice}, {fixedPrice}, {currencyCodeId}, {isDeleted}, getutcdate(), getutcdate(), {fixedPriceNotBillable}, {unitPriceNotBillable}) " + "SELECT SCOPE_IDENTITY()";

            return SqlClient.ExecuteScalar(sql);
        }

        public static int InsertArticle(string articleName, int productId, int productDescriptionId, int languageId)
        {
            var articleTypeId = SqlReadHelper.GetArticleTypeId(articleName);
            int codeId = SqlReadHelper.GetFirstArticleCodeId();
            var articleId = SqlClient.ExecuteScalar("INSERT INTO [Articles] ([ProductId],[ProductDescriptionId],[ArticleTypeId],[Title],[StatusCodeId],[Created],[LastModified],[LanguageId],[IsDeleted]) " + 
                $"VALUES ({productId}, {productDescriptionId}, {articleTypeId}, 'Test article', {codeId}, getutcdate(), getutcdate(), {languageId}, 0) " + 
                "SELECT SCOPE_IDENTITY()");

            if (articleName == "Classroom")
            {
                SqlClient.ExecuteScalar($"INSERT INTO [Article_Classrooms] ([Id]) VALUES ({articleId}) ");    
            }
            if (articleName == "E-Learning")
            {
                SqlClient.ExecuteScalar($"INSERT INTO [Article_Elearnings] ([Id]) VALUES ({articleId}) ");    
            }
            if (articleName == "Case Exam")
            {
                SqlClient.ExecuteScalar($"INSERT INTO [Article_CaseExams] ([Id], [Duration]) VALUES ({articleId}, 86400000) ");
            }
            if (articleName == "Internal Certification" || articleName == "Multiple Choice")
            {
                SqlClient.ExecuteScalar("INSERT INTO [Article_Exams] ([Id], [Duration], [ECTS], [ExamType], [NumberOfQuestions], [EasyQuestionCount], [MediumQuestionCount], [HardQuestionCount] ) " + 
                    $"VALUES ({articleId}, 86400000, 5, 0, 50, 15, 20, 15) ");
            }

            return articleId;
        }

        public static int InsertExam(int participantId, DateTime created)
        {
            var format = "yyyy-MM-dd HH:mm:ss";
            var formattedDate = created.ToString(format);

            var sql = "INSERT INTO Exams (ParticipantId, Created, IsSubmitted, IsTotalScoreOverridden) " +
                $"VALUES ({participantId}, '{formattedDate}', 0, 0) " + "SELECT SCOPE_IDENTITY()";

            return SqlClient.ExecuteScalar(sql);

        }


        public static string InsertMembershipUser(string userName, string hashedPassword, string passwordSalt, string role = PhoenixRoles.PhoenixUser, string comment = null)
        {
            int userId;
            return InsertMembershipUser(userName, hashedPassword, passwordSalt, out userId, role, comment);
        }

        public static string InsertMembershipTokenUser(string username, string role = PhoenixRoles.PhoenixUser)
        {
            var token = Guid.NewGuid().ToString();
            SqlClient.ExecuteScalarNoReturn(string.Format("EXEC dbo.CreateUserWithTokenAuthentication '{0}', '{1}', '{2}'", username, role, token));
            return token;
        }

        public static string InsertMembershipUser(string userName, string hashedPassword, string passwordSalt, out int userId, string role = PhoenixRoles.PhoenixUser, string comment = null)
        {
            var applicationId = SqlReadHelper.GetPhoenixMembershipApplicationId();
            var customerId = SqlReadHelper.GetFirstCustomerId();

            var membershipUserId = SqlClient.ExecuteScalarReturnString(string.Format("INSERT INTO [dbo].[aspnet_Users] ([ApplicationId] ,[UserName] ,[LoweredUserName] ,[IsAnonymous] ,[LastActivityDate])" +
                                                           "VALUES ('{0}' ,'{1}' ,'{2}' ,0 ,'2013-11-11 14:46:56.353') " +
                                                           "SELECT UserId FROM  [dbo].[aspnet_Users] WHERE UserName = '{1}'", new Guid(applicationId), userName, userName.ToLower()));
            SqlClient.ExecuteScalar("INSERT INTO [dbo].[aspnet_Membership] ([ApplicationId], [UserId] ,[Password] ,[PasswordFormat] ,[PasswordSalt] ,[Email] ,[LoweredEmail] ,[IsApproved] ,[IsLockedOut] ,[CreateDate] ,[LastLoginDate] ,[LastPasswordChangedDate] ,[LastLockoutDate] ,[FailedPasswordAttemptCount] ,[FailedPasswordAttemptWindowStart] ,[FailedPasswordAnswerAttemptCount] ,[FailedPasswordAnswerAttemptWindowStart] ,[Comment])" 
                                    + $"VALUES ('{new Guid(applicationId)}', '{new Guid(membershipUserId)}' ,'{hashedPassword}' ,1 , '{passwordSalt}','metierphoenix@gmail.com', 'metierphoenix@gmail.com' ,1 ,0 ,'2013-11-11 13:39:55.000' ,'2013-11-11 14:46:56.353' ,'2013-11-11 13:39:55.000' ,'2013-11-11 13:39:55.000' ,2 ,'2013-11-11 14:54:02.413' ,0 ,'1754-01-01 00:00:00.000' , '{comment ?? "OracleUserExport"}')");

            SqlClient.ExecuteScalar("INSERT INTO [dbo].[aspnet_UsersInRoles] ([UserId] ,[RoleId])" 
                                    + $"VALUES ('{membershipUserId}', (SELECT TOP 1 r.RoleId FROM [dbo].[aspnet_Roles] r WHERE r.LoweredRoleName = '{role}'))");

            userId = SqlClient.ExecuteScalar("INSERT INTO [dbo].[Users] ([CustomerId], [FirstName], [MembershipUserId]) " + 
                                                    $"VALUES ({customerId}, 'Testbruker', '{userName}') " + "SELECT SCOPE_IDENTITY()");

            SqlClient.ExecuteScalar("INSERT INTO UserInfoElements(UserId, LeadingTextId, InfoText) " 
                                    + $"select {userId}, Id, concat('infotext ', Id) from LeadingTexts");
            
            return userName;
        }

        public static int InsertHelpText(string section, string name, string text = "text")
        {
            return SqlClient.ExecuteScalar(string.Format("INSERT INTO [HelpTexts] ([Section],[Name],[Text]) " +
                                                          "VALUES ('{0}', '{1}', '{2}') " +
                                                          "SELECT SCOPE_IDENTITY()", section, name, text));
        }

        public static int InsertResource(int getFirstResourceTypeId)
        {
            return SqlClient.ExecuteScalar(string.Format("INSERT INTO [Resources] ([Name], [ResourceTypeId], [Amount]) " +
                                                         "VALUES ('IntegrationTestInitResource', {0}, 0) " +
                                                         "SELECT SCOPE_IDENTITY()", getFirstResourceTypeId));
        }

        public static int InsertComment(int userId)
        {
            return SqlClient.ExecuteScalar("INSERT INTO [Comments] ([Entity], [EntityId], [Comment], [UserId]) " +
                                           string.Format("VALUES ('integrationtest', 1, 'Integration test comment', {0}) ", userId) +
                                           "SELECT SCOPE_IDENTITY()");
        }

        public static int InsertCourseProgram(string name, int customerId, bool isDeleted = false, IEnumerable<CourseStep> steps = null)
        {
            var courseProgramId = SqlClient.ExecuteScalar(string.Format("INSERT INTO [CoursePrograms] ([Name], [CustomerId], [IsDeleted]) " +
                                                          "VALUES ('{0}', {1}, {2}) " +
                                                          "SELECT SCOPE_IDENTITY()", name, customerId, isDeleted ? 1 : 0));
            if (steps != null)
            {
                foreach (var step in steps)
                {
                    var stepId = SqlClient.ExecuteScalar(string.Format("INSERT INTO [CourseSteps] ([CourseProgramId], [Name], [Order]) " +
                                                                     "VALUES ({0}, '{1}', {2}) " +
                                                                     "SELECT SCOPE_IDENTITY()", courseProgramId, step.Name, step.Order));
                    foreach (var exam in step.Exams)
                    {
                        SqlClient.ExecuteScalar(string.Format("INSERT INTO [CourseExams] ([CourseStepId], [Name]) " +
                                                                      "VALUES ({0}, '{1}') ", stepId, exam.Name));               
                    }
                    
                    foreach (var module in step.Modules)
                    {
                        SqlClient.ExecuteScalar(string.Format("INSERT INTO [CourseModules] ([CourseStepId], [Name], [Order]) " +
                                                                      "VALUES ({0}, '{1}', {2}) ", stepId, module.Name, module.Order));               
                    }
                }
            }

            return courseProgramId;
        }

        public static int InsertActivity(int customerArticleId)
        {
            var activityId = SqlClient.ExecuteScalar("INSERT INTO [Activities] ([CustomerArticleId],[Name],[VisibleForParticipants],[MinParticipants],[MaxParticipants],[UnitPrice],[FixedPrice],[CurrencyCodeId],[EarliestInvoiceDate], [MomentTimezoneName], [Created],[LastModified],[IsDeleted], [Duration], [UnitPriceNotBillable],[FixedPriceNotBillable]) " + 
                                                         $"VALUES ({customerArticleId}, 'Test activity', getutcdate(), 0, 10, 1000, 8800, 262, getutcdate(), 'europe_oslo', getutcdate(), getutcdate(), 0, 1234, 0, 0) " +
                                                         "SELECT SCOPE_IDENTITY()");

            SqlClient.ExecuteScalarNoReturn($"INSERT INTO ActivityPeriods (ActivityId, [Start], [End]) values({activityId}, getUtcDate(), null)");

            return activityId;
        }

        public static int InsertActivitySet(int customerId)
        {
            return SqlClient.ExecuteScalar("INSERT INTO [dbo].[ActivitySets] ([CustomerId], [Name], [IsPublished], [PublishFrom], [PublishTo], [EnrollmentFrom], [EnrollmentTo], [IsUnenrollmentAllowed], [UnenrollmentDeadline], [IsDeleted], [Created], [LastModified]) " +
                                           $"VALUES ({customerId}, 'Test Activity Set', 1, getutcdate(), getutcdate(), getutcdate(), getutcdate(), 1, DATEADD(dd,1,getutcdate()), 0, getutcdate(), getutcdate()) " +
                                           "SELECT SCOPE_IDENTITY()");
        }

        public static void InsertCustomLeadingTexts(int customerId)
        {
            if (SqlClient.ExecuteScalar(string.Format("SELECT count(*) FROM CustomLeadingTexts WHERE CustomerId = {0}", customerId)) == 0)
            {
                //Using "Diverse kunde" (CustomerId = 2338) as basis for the new customers customleadingtexts...
                SqlClient.ExecuteScalar(string.Format("INSERT INTO CustomLeadingTexts(IsVisible, IsMandatory, [text], CustomerId, LeadingTextId, IsVisibleInvoiceLine, IsGroupByOnInvoice )" +
                                            "SELECT IsVisible, IsMandatory, [Text], {0}, LeadingTextId, IsVisibleInvoiceLine, IsGroupByOnInvoice FROM CustomLeadingTexts WHERE CustomerId = 2338", customerId));
            }

        }

        public static int InsertCustomLeadingText(int customerId)
        {
            const int leadingTextId = 11;

            if (SqlClient.ExecuteScalar(string.Format("SELECT count(*) FROM CustomLeadingTexts WHERE CustomerId = {0}", customerId)) == 0)
            {
                return SqlClient.ExecuteScalar(string.Format("INSERT INTO [dbo].[CustomLeadingTexts] ([IsVisible],[IsMandatory],[Text],[CustomerId],[LeadingTextId]) " +
                                               "VALUES (1, 1, 'Custom text', {0}, {1}) " +
                                               "SELECT SCOPE_IDENTITY()", customerId, leadingTextId));
            }

            return SqlClient.ExecuteScalar(string.Format("SELECT id FROM CustomLeadingTexts WHERE CustomerId = {0} and LeadingTextId = {1}", customerId, leadingTextId));
        }

        public static int InsertParticipant(int userId, int customerId, int activityId, int activitySetId)
        {
            const string statusCodeValue = ParticipantStatusCodes.Enrolled;
            int currencyCodeId = SqlReadHelper.GetFirstCurrencyCodeId();

            int enrollmentId = SqlClient.ExecuteScalar(string.Format("INSERT INTO Enrollments ([UserId],[ActivitySetId],[IsCancelled]) " +
                                           "VALUES({0},{1}, 0) " +
                                           "SELECT SCOPE_IDENTITY()", userId, activitySetId));

            int participantId = SqlClient.ExecuteScalar(string.Format("INSERT INTO [dbo].[Participants] ([UserId],[ActivitySetId],[CustomerId],[ActivityId],[StatusCodeValue],[CurrencyCodeId],[UnitPrice],[EarliestInvoiceDate],[AdditionalTime],[IsNotBillable],[EnrollmentId]) " +
                                           "VALUES({0},{1},{2},{3},'{4}',{5},1234,getutcdate(), 10000, 0, {6}) " +
                                           "SELECT SCOPE_IDENTITY()", userId, activitySetId, customerId, activityId, statusCodeValue, currencyCodeId, enrollmentId));


            if(int.Parse(SqlClient.ExecuteScalarReturnString(string.Format("SELECT count(*) FROM UserInfoElements WHERE UserId = {0}", userId))) == 0)
            {
                SqlClient.ExecuteScalar(string.Format("INSERT INTO UserInfoElements(UserId, LeadingTextId) " +
                                            "SELECT {0}, id  FROM LeadingTexts", userId));
            }


            SqlClient.ExecuteScalar(string.Format("INSERT INTO ParticipantInfoElements(ParticipantId, LeadingTextId, InfoText) " +
                                        "SELECT {0}, u.LeadingTextId, u.InfoText from UserInfoElements u " +
                                        "where u.userId = {1}", participantId, userId));

            return participantId;
        }

        public static int InsertCustomerInvoicings()
        {
            return SqlClient.ExecuteScalar("INSERT INTO [dbo].[CustomerInvoicings] ([BundledBilling], [OrderRefLeadingTextId], [YourRefLeadingTextId], [OurRef]) " +
                $"VALUES (1, 11, 12, 'our ref') " +
                "SELECT SCOPE_IDENTITY()");
        }


        public static int InsertParticipantInfoDefinitions()
        {
                return SqlClient.ExecuteScalar("INSERT INTO [dbo].[ParticipantInfoDefinitions] ([IsUserCanCreateAccount], [CustomerPortalUrl],[IsShowPricesToUserOnPortal],[CompetitionMode],[IsAllowUsersToSelfEnrollToCourses],[IsAllowUsersToSelfEnrollToExams],[AcceptedEmailAddressId],[IsCustomerMailAddressParticipantMailAddress],[EmailConfirmationAddress],[TermsAndConditionsText]) " + 
                    $"VALUES (1, null, 0, null, 1, 1, null, 0, null, null) " + 
                    "SELECT SCOPE_IDENTITY()");
        }

        public static int InsertCustomerAddress(int customerId, string street, string zipcode, string city, string countryCode, AddressType addressType = AddressType.Invoice)
        {
                return SqlClient.ExecuteScalar("INSERT INTO [dbo].[CustomerAddresses] ([AddressStreet1], [ZipCode], [City], [Country], [CustomerId], [AddressType]) " + 
                    $"VALUES ('{street}', '{zipcode}', '{city}', '{countryCode}', {customerId}, {(int)addressType}) " + 
                    "SELECT SCOPE_IDENTITY()");
        }

        public static int InsertMetierContactPerson(int customerId, int userId)
        {
                return SqlClient.ExecuteScalar("INSERT INTO [dbo].[MetierContactPersons] ([UserId], [CustomerId], [ContactType]) " + 
                    $"VALUES ({userId}, {customerId}, {(int)MetierContactType.ProgramCoordinator}) " + 
                    "SELECT SCOPE_IDENTITY()");
        }

        public static int InsertCustomerContactPerson(int customerId, string name)
        {
                return SqlClient.ExecuteScalar("INSERT INTO [dbo].[CustomerContactPersons] ([Name], [CustomerId]) " + 
                    $"VALUES ('{name}', {customerId}) " + 
                    "SELECT SCOPE_IDENTITY()");
        }


        public static int InsertFile()
        {
            return SqlClient.ExecuteScalar("INSERT INTO [Files] ([Filename], [Size], [ContentType], [RelativeFilePath]) " +
                                           "VALUES ('testfile.something', 1, 'example/dummy', 'something')" +
                                           "SELECT SCOPE_IDENTITY()");
        }

        public static int InsertQuestion(string type = "S")
        {
            return SqlClient.ExecuteScalar(string.Format("INSERT INTO [Questions] ([LanguageId],[CustomerId],[Type],[Text],[Difficulty], [Status], [CorrectBoolAnswer],[CorrectTextAnswer],[Created],[CreatedById],[Modified],[ModifiedById]) " +
                                           "VALUES ({0}, {1}, '{2}', 'Lorem ipsum', 1, 1, 1, NULL, getutcdate(), {3}, NULL, NULL)" +
                                           "SELECT SCOPE_IDENTITY()",
                                           SqlReadHelper.GetFirstLanguageId(),
                                           SqlReadHelper.GetFirstCustomerId(),
                                           type,
                                           SqlReadHelper.GetFirstUserId()));
        }

        public static int InsertOrder(Participant participant, Activity activity, int userId, int customerId, OrderStatus status, bool? creditNote, int? creditedOrderId = null)
        {
            var participants = new List<Participant> { participant };
            var activities = new List<Activity> { activity };

            return InsertOrder(participants, activities, userId, customerId, status, creditNote, creditedOrderId);
        }

        public static int InsertOrder(List<Participant> participants, List<Activity> activities, int userId, int customerId, OrderStatus status, bool? creditNote, int? creditedOrderId = null)
        {
            var isCreditNote = 0;
            if (creditNote.HasValue)
            {
                isCreditNote = creditNote.Value ? 1 : 0;
            }


            var orderId =
                SqlClient.ExecuteScalar(
                    string.Format(
                        "INSERT INTO [dbo].[Orders] ([CustomerId],[YourOrder],[YourRef],[OurRef],[OrderNumber],[InvoiceNumber],[Title],[LastModified],[CoordinatorId],[Status],[IsCreditNote],[CreditedOrderId]) " +
                        "VALUES ({0},'IntegrationTest','IntegrationTest','IntegrationTest',123456,987654,'Integration test order',getutcdate(),{1},{2},{3},{4})" +
                        "SELECT SCOPE_IDENTITY()", customerId, userId, (int)status, isCreditNote, creditedOrderId == null ? "null" : creditedOrderId.Value.ToString()));


            if (participants != null)
                foreach (var participant in participants)
                {
                    AddParticipantToOrder(participant, orderId, status, creditNote ?? false);
                }

            if (activities != null)
                foreach (var activity in activities)
                {
                    AddActivityToOrder(activity, orderId, status, creditNote ?? false);
                }

            return orderId;
        }

        #endregion

        #region Miscellaneous 

        public static void CancelEnrollment(int enrollmentId)
        {
            SqlClient.ExecuteScalarNoReturn($"UPDATE Participants Set [IsDeleted] = 1 WHERE EnrollmentId = {enrollmentId} ");
            SqlClient.ExecuteScalarNoReturn($"UPDATE Enrollments Set [IsCancelled] = 1 WHERE Id = {enrollmentId} ");
        }

        public static int AddActivityToActivitySet(int activiyId, int activitySetId)
        {
            return SqlClient.ExecuteScalar($"Insert into ActivitySets_Activities([ActivitySetId], [ActivityId]) Values ({activitySetId}, {activiyId})");
        }

        public static void AddParticipantToOrder(Participant participant, int orderId, OrderStatus status, bool isCreditNote)
        {
            if (participant == null) return;

            if (participant.ActivityId == 0)
            {
                participant.ActivityId = SqlClient.ExecuteScalar(string.Format("select ActivityId from Participants where id={0}", participant.Id));
            }

            if ((int)status > (int)OrderStatus.Draft || isCreditNote)
            {
                var user = participant.User ?? new User { FirstName = "Test", LastName = "User" };

                SqlClient.ExecuteScalar(string.Format("INSERT INTO [dbo].[Orders_Participants] ([OrderId] ,[ParticipantId], [Price], [CurrencyCodeId], [OnlinePaymentReference], [ActivityId], [ActivityName], [Infotext]) VALUES({0},{1},{2},{3},'{4}',{5},'{6}','{7}')",
                    orderId, participant.Id, (int)participant.UnitPrice, participant.CurrencyCodeId, participant.OnlinePaymentReference, participant.ActivityId, "Autogenerated activity by unit test", string.Concat(user.FirstName, " ", user.LastName)));
            }
            else
            {
                //var activityId = SqlClient.ExecuteScalar(string.Format("select ActivityId from Participants where id={0}", participant.Id));
                SqlClient.ExecuteScalar(string.Format("INSERT INTO [dbo].[Orders_Participants] ([OrderId], [ParticipantId], [ActivityId]) VALUES({0},{1},{2})", orderId, participant.Id, participant.ActivityId));
            }
        }

        public static void AddActivityToOrder(Activity activity, int orderId, OrderStatus status, bool isCreditNote)
        {
            if (activity == null) return;

            if ((int)status > (int)OrderStatus.Draft || isCreditNote)
            {
                SqlClient.ExecuteScalar(string.Format("INSERT INTO [dbo].[Orders_Activities] ([OrderId] ,[ActivityId], [Price], [CurrencyCodeId], [ActivityName]) VALUES({0},{1},{2},{3},'{4}')",
                    orderId, activity.Id, activity.FixedPrice.HasValue ? (int)activity.FixedPrice : 0, activity.CurrencyCodeId, activity.Name));
            }
            else
            {
                SqlClient.ExecuteScalar(string.Format("INSERT INTO [dbo].[Orders_Activities] ([OrderId] ,[ActivityId]) VALUES({0},{1})", orderId, activity.Id));
            }
        }

        public static void AddCustomInformationToEntity(string entityType, int entityId, string keyField, string field1, string field2, string field3, string field4, string field5)
        {
            SqlClient.ExecuteScalarNoReturn(string.Format("Insert into customInformation(entityId, entityType, keyfield, field1, field2, field3, field4, field5) " +
                                                          "values ({0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}')",
                                                          entityId, entityType, keyField, field1, field2, field3, field4, field5));
        }
       public static void DeleteCustomInformation(string entityType, int entityId)
        {
            SqlClient.ExecuteScalarNoReturn(string.Format("Delete from customInformation where entitytype='{0}' and entityId={1}", entityType, entityId));
        }

        public static void SetParticipantDeleted(int participantId)
        {
            SqlClient.ExecuteScalar(string.Format("UPDATE [dbo].[Participants] SET [IsDeleted] = 1 WHERE Id = {0}", participantId));
        }

        public static void SetOrderStatus(int orderId, OrderStatus orderStatus)
        {
            SqlClient.ExecuteScalar(string.Format("UPDATE [dbo].[Orders] SET [Status] = {0} WHERE Id = {1}", (int)orderStatus, orderId));
        }

        #endregion

    }
}