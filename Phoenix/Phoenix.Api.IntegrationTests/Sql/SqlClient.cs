﻿using System;
using System.Data.SqlClient;

namespace Phoenix.Api.IntegrationTests.Sql
{
    public class SqlClient
    {
        public static int ExecuteScalar(string query)
        {
            using (var conn = new SqlConnection(SqlConfig.ConnectionString))
            {
                var sqlCommand = new SqlCommand(query, conn);
                conn.Open();
                return Convert.ToInt32(sqlCommand.ExecuteScalar());
            }
        }

        public static string ExecuteScalarReturnString(string query)
        {
            using (var conn = new SqlConnection(SqlConfig.ConnectionString))
            {
                var sqlCommand = new SqlCommand(query, conn);
                conn.Open();
                return Convert.ToString(sqlCommand.ExecuteScalar());
            }
        }
        
        public static void ExecuteScalarNoReturn(string query)
        {
            using (var conn = new SqlConnection(SqlConfig.ConnectionString))
            {
                var sqlCommand = new SqlCommand(query, conn);
                conn.Open();
                sqlCommand.ExecuteScalar();
            }
        }
    }
}
