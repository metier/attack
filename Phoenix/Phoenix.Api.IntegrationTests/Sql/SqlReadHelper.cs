﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Facade.Account;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;

namespace Phoenix.Api.IntegrationTests.Sql
{
    public static class SqlReadHelper
    {
        public static int GetFirstCourseRcoId()
        {
            return SqlClient.ExecuteScalar("SELECT TOP 1 Id FROM [RCOs] WHERE Context = 'course' and Id in(select ParentId from [RCOs] where Context = 'lesson')");
        }

        public static int GetFirstLessonRcoId(int? courseRcoId = null)
        {
            return courseRcoId.HasValue
                ? SqlClient.ExecuteScalar(string.Format("SELECT TOP 1 Id FROM [RCOs] WHERE Context = 'lesson' and ParentId = {0}", courseRcoId))
                : SqlClient.ExecuteScalar("SELECT TOP 1 Id FROM [RCOs] WHERE Context = 'lesson'");
        }
        
        public static int GetFirstArticleTypeId()
        {
            return SqlClient.ExecuteScalar("SELECT TOP 1 Id FROM [ArticleTypes]");
        }

        public static int GetArticleTypeId(string name)
        {
            return SqlClient.ExecuteScalar(string.Format("SELECT TOP 1 Id FROM [ArticleTypes] where name = '{0}'", name));
        }
        
        public static int GetFirstLanguageId()
        {
            return SqlClient.ExecuteScalar("SELECT TOP 1 Id FROM [Languages]");
        }

        public static int GetFirstProductTypeId()
        {
            return SqlClient.ExecuteScalar("SELECT TOP 1 Id FROM [ProductTypes]");
        }

        public static int GetFirstProductCategoryId()
        {
            return SqlClient.ExecuteScalar("SELECT TOP 1 Id FROM [ProductCategories]");
        }

        public static int GetFirstCurrencyCodeId()
        {
            return 262;     //Well, it's not the first, but it's the best (NOK)  ;-)
            //return GetFirstCodeId(5);
        }

        public static int GetFirstArticleCodeId()
        {
            return GetFirstCodeId(2);
        }

        public static int GetFirstCustomerArticleId(ArticleTypes articleType)
        {
            return SqlClient.ExecuteScalar($"select top 1 ca.id from CustomerArticles ca inner join Articles a on a.id = ca.ArticleCopyId where a.ArticleTypeId = {(int)articleType} Order by ca.Id desc");
        }

        public static int GetFirstActivityOnCustomerArticle(int customerArticleId)
        {
            return SqlClient.ExecuteScalar($"select top 1 id from Activities where CustomerArticleId = {customerArticleId}");
        }

        private static int GetFirstCodeId(int codeTypeId)
        {
            return SqlClient.ExecuteScalar(string.Format("SELECT TOP 1 Id FROM [Codes] WHERE CodeTypeId = {0}", codeTypeId));
        }

        public static int GetFirstTemplatePartId(int productId)
        {
            return SqlClient.ExecuteScalar(string.Format("SELECT TOP 1 t.Id FROM [ProductTemplateParts] t, [ProductTypes] pt, [Products] p WHERE t.ProductTypeId = pt.Id and pt.Id = p.ProductTypeId and p.Id = {0}", productId));
        }

        public static int GetFirstProductDescriptionId()
        {
            return SqlClient.ExecuteScalar("SELECT TOP 1 Id FROM [ProductDescriptions] WHERE IsDeleted != 1");
        }

        public static int GetCustomerForUser(int userId)
        {
            return SqlClient.ExecuteScalar(string.Format("select top 1 c.Id from [dbo].[Customers] as c inner join [dbo].[Users] as u on c.Id = u.CustomerId where u.Id = {0}", userId));
        }

        public static int GetFirstCustomerIdForDistributor(int distributorId, bool externalCustomerIdRequired = false)
        {
            if(externalCustomerIdRequired)
                return SqlClient.ExecuteScalar($"SELECT TOP 1 Id FROM [Customers] where DistributorID = {distributorId} and ExternalCustomerId is not null");

            return SqlClient.ExecuteScalar($"SELECT TOP 1 Id FROM [Customers] where DistributorID = {distributorId}");
        }

        public static int GetFirstCustomerId()
        {
            return SqlClient.ExecuteScalar("SELECT TOP 1 Id FROM [Customers] WHERE AutomailDefinitionId IS NOT NULL");
        }

        public static int GetFirstProductId()
        {
            return SqlClient.ExecuteScalar("SELECT TOP 1 Id FROM [Products]");
        }

        public static int GetFirstDistributorId()
        {
            return SqlClient.ExecuteScalar(string.Format("SELECT TOP 1 Id FROM [Resources] WHERE ResourceTypeId = 1193"));
        }

        public static int GetFirstResourceTypeId()
        {
            return SqlClient.ExecuteScalar("SELECT TOP 1 Id FROM [ResourceTypes] ORDER BY Id DESC");
        }

        public static string GetPhoenixMembershipApplicationId()
        {
            return SqlClient.ExecuteScalarReturnString(string.Format("SELECT TOP 1 ApplicationId FROM [aspnet_Applications] where ApplicationName = 'Phoenix'"));
        }

        public static string GetFirstNameOfUser(int userId)
        {
            return SqlClient.ExecuteScalarReturnString(string.Format("SELECT TOP 1 FirstName FROM [Users] where Id = {0}", userId));
        }

        public static string GetFirstProposedOrderGroupByKey()
        {
            return SqlClient.ExecuteScalarReturnString(string.Format("SELECT TOP 1 GroupByKey FROM [v_Phoenix_ProposedOrders]"));
        }

        public static UserAccount GetImportedUserAccount()
        {
            var username = SqlClient.ExecuteScalarReturnString(string.Format("SELECT TOP 1 UserName FROM [aspnet_Users] where UserId = (SELECT TOP 1 UserId FROM [aspnet_Membership] WHERE Password = '{0}')", SqlWriteHelper.HashedImportedUserPassword));
            return new UserAccount
            {
                Username = username,
                Password = "evrytest"
            };
        }

        public static Int64 GetLastLoginDateAsMinutesFromNow(string userName)
        {
            return SqlClient.ExecuteScalar(string.Format("SELECT top 1 DATEDIFF(MINUTE, SYSUTCDATETIME(), m.LastLoginDate) FROM aspnet_Users u INNER JOIN aspnet_Membership m on u.UserId = m.UserId and u.ApplicationId = m.ApplicationId WHERE u.UserName= '{0}'", userName));
        }


        public static int GetFirstUserId()
        {
            return GetFirstUserId(new List<int>());
        }

        public static int GetSecondUserId()
        {
            var firstId = GetFirstUserId(new List<int>());
            return GetFirstUserId(new List<int>() {firstId});
        }

        public static int GetFirstUserId(List<int> userIdsToExclude )
        {
            string userIdListAsString = "0";
            foreach (var userId in userIdsToExclude)
            {
                userIdListAsString += (", ") + userId ;
            }

            return SqlClient.ExecuteScalar(string.Format("SELECT TOP 1 Id FROM [Users] WHERE id not in({0})", userIdListAsString));
        }

        public static int GetUserIdForUsername(string username)
        {
            return SqlClient.ExecuteScalar(string.Format("SELECT TOP 1 Id FROM [Users] WHERE [MembershipUserId] = '{0}'", username));
        }

        public static string GetUsernameForUserId(int userId)
        {
            return SqlClient.ExecuteScalarReturnString(string.Format("SELECT TOP 1 MembershipUserId FROM [Users] WHERE [Id] = '{0}'", userId));
        }

        public static int GetLastCourseExamId()
        {
            return SqlClient.ExecuteScalar("SELECT TOP 1 Id FROM [CourseExams] ORDER BY Id desc");
        }

        public static int GetLastCourseModuleId()
        {
            return SqlClient.ExecuteScalar("SELECT TOP 1 Id FROM [CourseModules] ORDER BY Id desc");
        }

        public static int GetFirstEnrollmentIdForUser(int userId, int activitySetId)
        {
            return SqlClient.ExecuteScalar(string.Format("SELECT TOP 1 Id FROM [Enrollments] WHERE [UserId] = {0} and [ActivitySetId] = {1}", userId, activitySetId));
        }

    }
}