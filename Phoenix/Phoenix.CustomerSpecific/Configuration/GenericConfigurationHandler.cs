namespace Metier.Phoenix.CustomerSpecific.Configuration
{
    using System;
    using System.Configuration;
    using System.Xml;

    public class GenericConfigurationHandler : IConfigurationSectionHandler
    {
        private void PopulateChildNodes(ConfigurationNode parent, XmlNode xmlNode)
        {
            foreach (XmlNode node in xmlNode.ChildNodes)
            {
                if (XmlNodeType.Element == node.NodeType)
                {
                    ConfigurationNode item = new ConfigurationNode();
                    parent.Children.Add(item);
                    item.Tag = node.Name;
                    foreach (XmlAttribute attribute in node.Attributes)
                    {
                        if (XmlNodeType.Attribute == attribute.NodeType)
                        {
                            item.Attributes.Add(attribute.Name, attribute.Value);
                        }
                    }
                    this.PopulateChildNodes(item, node);
                }
            }
        }

        object IConfigurationSectionHandler.Create(object parent, object configContext, XmlNode section)
        {
            ConfigurationNode node = new ConfigurationNode {
                Tag = section.Name
            };
            foreach (XmlAttribute attribute in section.Attributes)
            {
                if (XmlNodeType.Attribute == attribute.NodeType)
                {
                    node.Attributes.Add(attribute.Name, attribute.Value);
                }
            }
            this.PopulateChildNodes(node, section);
            return node;
        }
    }
}

