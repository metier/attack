namespace Metier.Phoenix.CustomerSpecific.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class ConfigurationNode
    {        
        public ConfigurationNode()
        {
            this.Attributes = new Dictionary<string, string>();
            this.Children = new List<ConfigurationNode>();
        }

        public Dictionary<string, string> Attributes { get; set; }

        public List<ConfigurationNode> Children { get; set; }

        public string Tag { get; set; }
    }
}

