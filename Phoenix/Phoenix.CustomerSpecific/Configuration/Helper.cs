using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Linq;
using jlib.functions;

namespace Metier.Phoenix.CustomerSpecific.Configuration
{    
    public class Helper
    {
        public System.Web.HttpRequest Request { get; set; }
        public Helper(System.Web.HttpRequest request)
        {
            Request = request;
            SiteUrl = "/" + request.Url.LocalPath.Substring(request.ApplicationPath.Length).Split('/')[1].ToLower();
        }
        public string SiteUrl { get; set; }
        public ConfigurationNode SiteNode
        {
            get
            {
                return Helper.GetConfigurationNodes(CustomerRules, new List<string> { "sites" }).First().Children.FirstOrDefault(site => site.Attributes.SafeGetValue("url") == SiteUrl);
            }
        }

        public ConfigurationNode CustomerRules
        {
            get
            {
                return System.Configuration.ConfigurationManager.GetSection("customerSpecificConfiguration") as ConfigurationNode;                
            }
        }

        public static List<ConfigurationNode> GetConfigurationNodes(ConfigurationNode parent, List<string> names)
        {
            List<ConfigurationNode> matches = new List<ConfigurationNode>();
            if (names.Count == 0)
            {
                matches.Add(parent);
            }
            else
            {
                var nameToMatch = names[0];
                names.RemoveAt(0);
                parent.Children.ForEach(child => {
                    if (child.Tag == nameToMatch)
                    {
                        matches.AddRange(GetConfigurationNodes(child, names));
                    }
                });
            }
            return matches;
        }

        public List<IdValueClass> GetCustomersForDropdown()
        {
            List<IdValueClass> values = new List<IdValueClass>();            
            Helper.GetConfigurationNodes(SiteNode, new List<string> { "authorization", "customers-for-dropdown", "customer" }).ForEach(node => {
                values.Add(new IdValueClass(node.Attributes.SafeGetValue("id").Int(), node.Attributes.SafeGetValue("value").Str()));
            });
            return values;
        }
        public List<IdValueClass> GetDistributors()
        {
            List<IdValueClass> values = new List<IdValueClass>();
            Helper.GetConfigurationNodes(SiteNode, new List<string> { "authorization", "distributors", "distributor" }).ForEach(node => {
                values.Add(new IdValueClass(node.Attributes.SafeGetValue("id").Int(), node.Attributes.SafeGetValue("value").Str()));
            });
            return values;
        } 
        public class IdValueClass
        {
            public IdValueClass(int Id, string Value)
            {
                this.Id = Id;
                this.Value = Value;
            }
            public int Id { get; set; }
            public string Value { get; set; }
        }
    }
}

