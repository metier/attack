﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.functions;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.IO;

namespace Phoenix.CustomerSpecific.FolloBanen.Shared {
	public partial class API : System.Web.UI.Page {

		
		protected void Page_Init(object sender, EventArgs e) {
            //If request starts with "/_/" then don't send to Phoenix
            if (Request["endpoint"].Str().StartsWith("/_/"))
            {
                new Phoenix.CustomerSpecific.API.InternalAPI().ProcessRequest(Request, Response);
            }
            else
            {
                new Phoenix.CustomerSpecific.API.APIFilterer().ProcessRequest(Request, Response);
            }
		}
	}
}