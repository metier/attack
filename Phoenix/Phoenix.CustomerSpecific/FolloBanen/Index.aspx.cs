﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Metier.Phoenix.CustomerSpecific.Configuration;
using jlib.functions.json;

namespace Phoenix.CustomerSpecific.FolloBanen
{
    public partial class Index : System.Web.UI.Page
    {        
        public string CustomerIds
        {
            get
            {
                Helper configurationHelper = new Helper(Request);
                return DynamicJson.Serialize(configurationHelper.GetCustomersForDropdown());
            }
        }
        public string DistributorIds
        {
            get
            {
                Helper configurationHelper = new Helper(Request);
                return DynamicJson.Serialize(configurationHelper.GetDistributors());
            }
        }
    }
}