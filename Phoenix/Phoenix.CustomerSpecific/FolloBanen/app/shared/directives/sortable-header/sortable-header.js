﻿/*global angular */
/*jslint maxlen: 150 */
angular.module('app').directive('sortableHeader', [function () {
    'use strict';
    return {
        require: ['^userList'],
        scope: {
            title: '@',
            orderBy: '@'
        },
        link: function (scope, element, attrs, grid) {
            scope.grid = grid[0];
        },
        restrict: 'EA',
        replace: true,
        templateUrl: 'app/shared/directives/sortable-header/sortable-header.html',
        controller: [
            '$scope',
            function ($scope) {
                var self = this;
                $scope.orderDescending = function () {
                    self.order('desc');
                };

                $scope.orderAscending = function () {
                    self.order('asc');
                };

                $scope.isOrderedDescending = function () {
                    return self.isOrderedBy($scope.orderBy, 'desc');
                };

                $scope.isOrderedAscending = function () {
                    return self.isOrderedBy($scope.orderBy, 'asc');
                };

                $scope.isNotOrdered = function () {
                    return !$scope.isOrderedAscending() && !$scope.isOrderedDescending();
                };

                self.isOrderedBy = function (orderBy, orderDirection) {
                    return $scope.grid.getSortCol() === orderBy && $scope.grid.getSortOrder() === orderDirection;
                };

                self.order = function (orderDirection) {
                    $scope.grid.setSort($scope.orderBy, orderDirection);
                };
            }]
    };
}]);