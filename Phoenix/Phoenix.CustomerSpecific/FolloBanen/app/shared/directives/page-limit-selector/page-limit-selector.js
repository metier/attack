﻿angular.module('app').directive('pageLimitSelector', [    
    function () {
        'use strict';
        return {
            restrict: 'E',
            scope: {},
            require: ['^userList'],
            link: function (scope, element, attrs, grid) {
                scope.grid = grid[0];
            },            
            templateUrl:  'app/shared/directives/page-limit-selector/page-limit-selector.html',
            controller: ['$scope', '$location', function ($scope, $location) {
                $scope.limits = [10, 20, 50, 100];                
                $scope.init = function () {
                    $scope.selectedLimit = parseInt($scope.grid.getPageLimit(), 10) || $scope.limits[0];
                    return $scope.selectedLimit;
                }

                $scope.limitSelected = function () {
                    $scope.grid.setPageLimit($scope.selectedLimit);                    
                };
            }]
        };
    }
]);