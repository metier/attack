﻿(function () {
    'use strict';
    angular.module('app.register').controller('ParticipantEditModalController', [
            '$rootScope',
            '$scope',
            '$filter',
            '$modal',
            '$modalInstance',
            '$q',
            'isReCertification',
            'bulkParticipants',
            'participants',
            'participantComments',
            'user',
            'customer',
            'languageId',
            'customLeadingTexts',
            'participantStatusCodes',
            'countryCodes',
            'activitySet',
            'CustomerService',
            'UserService',
            'FlashService',

            function ($rootScope, $scope, $filter, $modal, $modalInstance, $q, isReCertification, bulkParticipants, participants, participantComments, user, customer, languageId, customLeadingTexts, participantStatusCodes, countryCodes, activitySet, CustomerService, UserService, FlashService) {
                var vm = this;
                vm.init = init;

                vm.isReCertification = isReCertification;
                vm.bulkParticipants = bulkParticipants;

                vm.activitySet = activitySet;
                vm.user = user;

				vm.openDate = function (id, $event)
				{
					if (!vm.dateStatus[id]) vm.dateStatus[id] = {};
                    vm.dateStatus[id].opened = true;
                };

                vm.dateStatus = {
                    //opened: false
                };

                vm.init();

                vm.allParticipants = participants;
                if (participantComments && participantComments.length > 0) vm.comments = participantComments[0].CommentText;

                vm.customer = customer;
                vm.languageId = languageId;

                vm.customLeadingTexts = customLeadingTexts;
                vm.purchaserEmailAddress = $.grep(vm.customLeadingTexts, function (item) { return item.CustomLeadingText.LeadingTextId == 17; })[0].InfoText;
                vm.canUnenroll = vm.activitySet.IsUnenrollmentAllowed && (!vm.activitySet.UnenrollmentDeadline || new Date(vm.activitySet.UnenrollmentDeadline) > new Date());
                vm.participantStatusCodes = participantStatusCodes;
                vm.countryCodes = countryCodes;

                vm.openUserSearchDialog = openUserSearchDialog;

                vm.participantStatusFilter = function (status) {
                    return status.Value == "COMPLETED" || status.Value == "ENROLLED" || status.Value == "NOTATTENDED" || status.Value == "CANCELLED" || status.Value == vm.participant.ParticipantStatusCodeValue;
                };
				vm.countryIsRequired = function ()
				{
					return $.grep(vm.participations, function (o) { return o.ParticipantStatusCodeValue == "CANCELLED" || o.ParticipantStatusCodeValue == "NOTATTENDED"; }).length == 0
				};
				vm.toggleParticipantStatus = function (participant)
				{
					if (participant.ParticipantStatusCodeValue != 'CANCELLED')
					{
						participant.OldParticipantStatusCodeValue = participant.ParticipantStatusCodeValue;
						participant.ParticipantStatusCodeValue = 'CANCELLED';
					} else
					{
						participant.ParticipantStatusCodeValue = participant.OldParticipantStatusCodeValue || 'ENROLLED';
					}
				};
				vm.dateChanged = function (parentObject, propertyName, forceDateOnly)
				{
					if (forceDateOnly && !!parentObject && !!parentObject[propertyName])
					{
						parentObject[propertyName] = new Date(parentObject[propertyName].getFullYear(), parentObject[propertyName].getMonth(), parentObject[propertyName].getDate(), 0, parentObject[propertyName].getTimezoneOffset() * -1 );
					}
					//alert(fieldId);
					//$scope.participantForm["TEXT_28266"].$valid
				};

                $scope.$watch("globals.isInstructor", function (newValue, oldValue) {
                    vm.isInstructor = newValue;
                });

				vm.searchPhoenixIdCmd = function ()
				{
                    if ((vm.user == null || vm.user.User.Id != vm.searchPhoenixId)) {
                        vm.user = null;
                        vm.customLeadingTexts = [];
                        vm.searchPhoenixIdStatus = "";
                        if (vm.searchPhoenixId) {
                            vm.searchPhoenixIdStatus = "Searching...";
                            return UserService.GetById(vm.searchPhoenixId).then(function (result) {
                                if ($.grep(vm.allParticipants, function (item) { return item.UserId == result.data.User.Id })[0]) {
                                    vm.searchPhoenixIdStatus = "User has already been added to course";
                                    return;
                                }
                                vm.user = result.data;
                                vm.searchPhoenixIdStatus = "User " + vm.user.User.FirstName + " " + vm.user.User.LastName + " was selected";
                                return CustomerService.GetById(vm.user.CustomerId).then(function () {
                                    vm.customLeadingTexts = UserService.BuildCustomLeadingTextsArray(vm.customer, vm.purchaserEmailAddress, null, vm.user);
                                });
                            }).catch(function () {
                                vm.searchPhoenixIdStatus = "User was not found";
                            });
                        }
                    }
                };

                function openUserSearchDialog() {
                    var modalInstance = $modal.open({
                        backdrop: 'static',
                        templateUrl: 'app/users/popups/user-search-modal.html',
                        controller: 'UserSearchModalController',
                        controllerAs: 'vm',
                        resolve: {},
                        windowClass: 'modal-very-wide'
                    });
                    modalInstance.result.then(function (userId) {
                        if (userId) {
                            vm.searchPhoenixId = userId;
                            vm.searchPhoenixIdCmd();
                        }
                    });
                }


                vm.saveComments = function () {
                    if (!$scope.participantForm.comments.$dirty) return;
                    var deferred = $q.resolve();

                    deferred = deferred.then(function () {
                        return UserService.GetEnrollmentCommentsById(vm.participations[0].EnrollmentId);
                    }).then(function (result) {
                        if (result.data.length == 0)
                            return UserService.AddComment({ "Entity": "enrollment", "EntityId": vm.participations[0].EnrollmentId, "UserId": $rootScope.globals.currentUser.User.Id, "CommentText": vm.comments });
                        else
                            return UserService.UpdateComment($.extend({}, result.data[0], { "CommentText": vm.comments }));
                    });
                    return deferred;
                };
                vm.saveUser = function () {
                    if (!vm.user.User.Id) {
                        vm.user.User.UserNotificationType = "1";
                        vm.user.User.CustomerId = vm.customer.Id;
                        vm.user.CustomerId = vm.customer.Id;
                        vm.user.Roles = ["LearningPortalUser"];
                        vm.user.PreferredLanguageId = vm.languageId;
                        vm.user.Email = "noemail-" + new Date().getTime() + "@jbv.noemail";

                        vm.user.Username = vm.user.Email;
                        vm.user.User.UserInfoElements = [];
						$.each(vm.customLeadingTexts, function (index, item)
						{
                            if (item.CustomLeadingText.LeadingTextId != 17)
                                vm.user.User.UserInfoElements.push({ "LeadingTextId": item.CustomLeadingText.LeadingTextId, "InfoText": UserService.PrepareCustomLeadingTextsForSubmission(item.InfoText, item.InputType) });
                        });
                        return UserService.Create(vm.user).then(function (data) {
							return UserService.GetById(data.data.Id).then(function (data)
							{
								vm.user = data.data;
								if (vm.user.Email.indexOf("noemail-") > -1)
								{
									vm.user.NewUsername = "" + vm.user.User.Id;
									vm.user.User.MembershipUserId = vm.user.NewUsername;
									vm.user.Email = vm.user.NewUsername + "@jbv.noemail";
									return UserService.Update(vm.user);
								}
							});
                        });
                    } else {
                        UserService.UpdateUserWithCustomLeadingTexts(vm.user, vm.customLeadingTexts);
						return UserService.Update(vm.user).then(function (data)
						{
                            vm.user = data.data;
                        });
                    }
                };

                vm.updateNewRegistrationParticipants = function (data) {
                    if (vm.activitySet.AvailableSpots) vm.activitySet.AvailableSpots--;
                    var participations = [];
                    var deferred = $q.resolve();
					$.each(data.data.Activities, function (index, activity)
					{
						$.each(activity.Participants, function (index1, participation)
						{
							if (participation.UserId == vm.user.User.Id && !participation.IsDeleted)
							{
								participations.push(participation);

								//Locate participation that was edited
								var currentParticipation = $.grep(vm.participations, function (item) { return item.ActivityId == activity.Id; })[0];
								var status = currentParticipation.ParticipantStatusCodeValue;
								var completedDate = currentParticipation.ParticipantCompletedDate;
								if (status != participation.StatusCodeValue || completedDate != participation.ParticipantCompletedDate)
									deferred = deferred.then(function ()
									{
										return UserService.UpdateParticipantStatusAndCompletedDate(participation.Id, currentParticipation.ParticipantStatusCodeValue, currentParticipation.ParticipantCompletedDate);
									});
                            }
                        });
                    });

                    vm.participations = participations;
                    return deferred;
                };
                vm.saveParticipations = function () {
                    //Enroll if needed
                    var returnFunction = $q.resolve();
					if (!vm.participations[0].Id)
					{
                        returnFunction = UserService.EnrollUserInActivitySet(vm.user.User.Id, vm.activitySet.Id, {"InfoElements" : [{ "LeadingTextId": 17, "InfoText": vm.purchaserEmailAddress }] }).then(vm.updateNewRegistrationParticipants);
                    } else {
                        returnFunction = returnFunction.then(function () {
                            return UserService.UpdateParticipationsWithCustomLeadingTexts(vm.participations, vm.customLeadingTexts);
                        });
                    }
                    return returnFunction;
                };


                vm.saveCleanup = function () {
                    vm.isSaving = false;
                    vm.searchPhoenixId = null;
                    vm.searchPhoenixIdStatus = null;
                    vm.user = null;
                    vm.init();
                };

                function init() {
                    vm.participant = $.grep(participants, function (participant) { return vm.user && vm.user.User.Id == participant.UserId; })[0] || {};
					if (!vm.participant._Participations)
					{
                        vm.participations = [];
                        $.each(vm.activitySet.Activities, function (index, item) {
							vm.participations.push({ "ActivityName": item.Name, "ActivityId": item.Id, "ParticipantStatusCodeValue": (vm.bulkParticipants.length == 0 ? "ENROLLED" : ""), ArticleType: (item.CustomerArticle.ArticleTypeId == 1 ? "E-Learning" : ""), Order: ((item.CustomerArticle || {}).Module || {}).Order });
                        });
					} else
					{
                        vm.participations = angular.copy(vm.participant._Participations);
						$.each(vm.participations, function(index, item){
							var a=$.grep(vm.activitySet.Activities, function(act){ return act.Id == item.ActivityId; })[0];
							item.Order = ((((a || {}).CustomerArticle || {}).Module || {}).Order || 0);
						});
					}
					vm.participations.sort(function(a,b){return a.Order-b.Order;});

					$.each(vm.participations, function (i, p)
					{
						if (p.ParticipantCompletedDate) p.ParticipantCompletedDate = new Date(p.ParticipantCompletedDate);
					});
                    setTimeout(function () { $('#searchPhoenixId').focus(); }, 0);
                }

                vm.alertSaveFailure = function (status) {
                    var msg;
                    try {
                        msg = "\n* " + $.parseJSON(status.statusText).ValidationErrors.join("\n* ");
                    } catch (e) { };

                    alert('Save failed!' + msg);
                    vm.isSaving = false;
                };

                //This function could be tied to onchange of the email field
                vm.getScopeUserByEmailIfNew = function () {
                    if (vm.user.User.Id || !vm.user.Email) return $q.resolve();
                    return UserService.GetByUsername(vm.user.Email).then(function (result) {
                        //Merge changed values from form with database record
                        vm.user = $.extend(true, {}, result.data, vm.user);
                        $.each(vm.allParticipants, function () {
                            if (this.UserId == vm.user.User.Id && !this.IsDeleted)
                                vm.participations = this._Participations;
                        });
                    }).catch(function () {
                        //User was not found -- continue with add
                    });
                };

                vm.bulkSave = function () {
                    vm.isSaving = true;
                    var deferred = $q.resolve();
                    var error = "", counter = 0, lastUserIndex = -1;
                    $.each(vm.bulkParticipants, function (index, user) {
                        $.each(vm.participations, function (index1, status) {
							if (status.ParticipantStatusCodeValue || status.ParticipantCompletedDate)
							{
                                var currentParticipation = $.grep(user._Participations, function (item) { return item.ActivityId == status.ActivityId; })[0];
								if (currentParticipation.ParticipantStatusCodeValue != status.ParticipantStatusCodeValue || currentParticipation.ParticipantCompletedDate != status.ParticipantCompletedDate)
								{
									if (currentParticipation.ParticipantStatusCodeValue == "CANCELLED")
									{
                                        //error += "<li>#" + currentParticipation.Id + " - " + currentParticipation.FirstName + " " + currentParticipation.LastName + " is cancelled on " + currentParticipation.ActivityName + ", and therefore can't be bulk updated.</li>";
                                    } else {
                                        deferred = deferred.then(function () {
                                            if (lastUserIndex != index) {
                                                lastUserIndex = index;
                                                counter++;
                                            }

											return UserService.UpdateParticipantStatusAndCompletedDate(currentParticipation.Id, status.ParticipantStatusCodeValue, status.ParticipantCompletedDate);
										}).then(function ()
										{
											currentParticipation.ParticipantStatusCodeValue = status.ParticipantStatusCodeValue;
											currentParticipation.ParticipantCompletedDate = status.ParticipantCompletedDate;
                                        });
                                    }
                                }
                            }
                        });
                    });
                    deferred = deferred.finally(function () {
                        vm.error = counter + " user(s) were updated.<br />" + (error ? "<ul>" + error + "</ul>" : "");
                        vm.isSaving = false;
                        vm.dialogCloseMode = true;
                    });
                    return deferred;
                }

                vm.cancel = function () {
                    $modalInstance.close();
                };

                vm.save = function () {
                    vm.isSaving = true;
                    vm.getScopeUserByEmailIfNew().finally(vm.saveUser).then(vm.saveParticipations).then(vm.saveComments).then(function () { $modalInstance.close(); vm.saveCleanup(); }).catch(vm.alertSaveFailure);
                };
                vm.saveAndAddNew = function () {
                    vm.isSaving = true;
                    vm.getScopeUserByEmailIfNew().finally(vm.saveUser).then(vm.saveParticipations).then(vm.saveComments).then(function () {
                        vm.participant = {};
                        vm.participations = [{}];
                        vm.comments = null;
                        vm.user = null;
                        angular.forEach(vm.customLeadingTexts, function (item) {
                            if (item.CustomLeadingText.LeadingTextId != 17) item.InfoText = null;
                        });

                        vm.saveCleanup();
                    }).catch(vm.alertSaveFailure);
                };

                $(document).on("blur","input.date", function () {
                    var vals = $(this).val().split(".");
                    if (vals.length == 3) {
                        for (var x = 0; x < vals.length; x++)
                            if (vals[x].length == 1) vals[x] = "0" + vals[x];
                        if (vals[2].length == 2) vals[2] = "19" + vals[2];

                        $(this).val(vals.join(".")).change();
                    }
                });
            }
    ]);
})();
