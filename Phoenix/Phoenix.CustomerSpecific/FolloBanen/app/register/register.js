﻿angular.module('app.register', ['ui.router', 'ngCookies', 'app.config', 'ui.bootstrap'])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('register', {
        //url:'/register/:customerId?/:languageId?/:date?',
        url: '/register?query&page&limit&orderBy&orderDirection',
        controller: 'RegisterController',
        templateUrl: 'app/register/register.view.html',
        controllerAs: 'vm',
        scope: true,
        bindToController: { date: '=' },
        //reloadOnSearch: false
    });
}])
.controller('RegisterController', [
    '$rootScope',
    '$scope',
    '$modal',
    'UserService',
    'CustomerService',
    'FlashService',
    'i18ng',
    '$q',
function ($rootScope, $scope, $modal, UserService, CustomerService, FlashService, i18ng, $q) {
    'use strict';

    var vm = this;
    vm.modeRegister = true;
    vm.participants = { list: [], loading: false };
    vm.calendar = { dates: [], status: "" };
    vm.selectedParticipants = [];
    vm.activitySets = [];
    vm.editParticipant = editParticipant;

    vm.data = {
        languages:[]
    };

    $scope.$watch("vm.activitySetId", function (newValue, oldValue) {
        if (newValue) {
            var returnFunction = $q.resolve();
            if (!vm.selectedActivitySets || vm.selectedActivitySets.length < 1 || vm.selectedActivitySets[0].Id != newValue)
                returnFunction = returnFunction.then(function () {
                    return UserService.GetActivitysetById(newValue).then(function (result) {
                        vm.selectedActivitySets = [result.data];
                        if(vm.selectedActivitySets.length>0){
                            vm.customerId = ""+vm.selectedActivitySets[0].CustomerId;
                            if (vm.selectedActivitySets[0].Activities.length > 0) vm.languageId = "" + vm.selectedActivitySets[0].Activities[0].CustomerArticle.LanguageId;
                        }
                    });
                });
            returnFunction = returnFunction.then(function(){
                if(!vm.customer || vm.customer.Id != vm.selectedActivitySets[0].CustomerId)
                    return CustomerService.GetById(vm.selectedActivitySets[0].CustomerId).then(function (result) {
                        vm.customer = result.data;
                    });
            }).then(function () {
                vm.refreshUserList(vm.selectedActivitySets[0]);
            });
        }
    });

    $scope.$watch("globals.currentUser", function (newValue, oldValue) {
        if (newValue) vm.purchaserEmail = newValue.Email;
    });
    $scope.$watch("globals.isInstructor", function (newValue, oldValue) {
        vm.isInstructor = newValue;
    });

    $scope.$watch("vm.customerId", function (newValue, oldValue) {
        if (newValue && vm.customer && newValue == vm.customer.Id) return;
        vm.activitySets = [];
        vm.activitySet = null;
        if (newValue) {
            vm.calendar.status = 'working';
            return UserService.GetActivitySetsByCustomer(newValue, true).then(function (sets) {
                vm.activitySets = sets.data;
                vm.generateCalendarDateArray();

                //populate language dropdown
                var languages = [];
                angular.forEach(vm.activitySets, function (set) {
                    angular.forEach(set.Activities, function (activity) {
                        var language = $.grep($rootScope.settings.allLanguages, function (item) { return item.Id == activity.CustomerArticle.LanguageId; })[0];
                        if (languages.indexOf(language)==-1)
                            languages.push(language);
                    });
                });
                //If current languageId is not available for this customer, clear it
                if (vm.languageId && $.grep(languages, function (item) { return item.Id == vm.languageId; }).length == 0)
                    vm.languageId = null;

                vm.data.languages = languages;

                vm.calendar.status = '';
                return CustomerService.GetById(newValue).then(function (customer) {
                    vm.customer = customer.data;
                });
            }).catch(function () {
                vm.calendar.status = '';
            });
        }
    });

    $scope.$watch("vm.languageId", function (newValue, oldValue) {
        if (newValue && newValue == oldValue) return;
        vm.activitySet = null;
        if (newValue) {
            vm.generateCalendarDateArray();
        }
    });

    UserService.GetParticipantStatusCodes().then(function (codes) {
        vm.participantStatusCodes = codes.data;
    });
    UserService.GetCountryCodes().then(function (codes) {
        vm.countryCodes = codes.data;
    });

    vm.selectionChanged = function (ids) {
        vm.selectedParticipants = ids;
    }

    function getElementsAtDate(date) {
        if (!date) return;
        var dateVal = Number(new Date(date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2)));
        return $.grep(vm.calendar.dates, function (element) {
            return element.DateVal == dateVal;
        })[0];
    }

    function refreshUserListByDate(date) {
        var highlight = getElementsAtDate(date);
        if (!highlight) return false;
        vm.selectedActivitySets = highlight.Sets;
        vm.refreshUserList(vm.selectedActivitySets[0]);
    }

    function refreshCalendar() {
        $('#datepicker').datepicker("destroy");
        if (vm.calendar.dates.length == 0 || !vm.languageId) {
            if (vm.calendar.status !== 'working') vm.calendar.status = 'nocourses';
            return;
        }
        vm.calendar.status = '';
        $('#datepicker').datepicker({
            beforeShowDay: function (date) {
                var dateVal = Number(new Date(date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2)));
                var highlight = $.grep(vm.calendar.dates, function (element) {
                    return element.DateVal == dateVal;
                })[0];

                if (highlight) {
                    return [true, "cal-event", highlight.Sets[0].Name];
                } else {
                    return [false, '', ''];
                }
            },
            onSelect: function (date, inst) {
                vm.date = inst.input.datepicker('getDate');
                refreshUserListByDate(vm.date);
                $scope.$apply();
            }
        });
        if (vm.activitySetId) {
            var selectedDate = $.grep(vm.calendar.dates, function (set) { return $.grep(set.Sets, function (activitySet) { return activitySet.Id == vm.activitySetId; })[0] != null })[0];
            if (selectedDate) vm.date = new Date(selectedDate.DateVal);
        }
        if (vm.date) {
            var highlight = getElementsAtDate(vm.date);
            if (highlight) {
                vm.selectedActivitySets = highlight.Sets;
                $('#datepicker').datepicker("setDate", vm.date);
                refreshUserListByDate(vm.date);
            }
        }
    }

    vm.generateCalendarDateArray = function () {
        vm.calendar.dates = [];
        angular.forEach(vm.activitySets, function (set) {
            angular.forEach(set.Activities, function (activity) {
                //activity.LanguageId == newValue &&
                if (activity.ActivityPeriods.length > 0 && activity.ActivityPeriods[0].End != null && activity.CustomerArticle.LanguageId == vm.languageId) {
                    var date = new Date(activity.ActivityPeriods[0].Start.split('T')[0]);
                    var existingCalendarDate = getElementsAtDate(date);
                    if (existingCalendarDate) {
                        if (existingCalendarDate.Sets.indexOf(set) == -1)
                            existingCalendarDate.Sets.push(set);
                    } else {
                        vm.calendar.dates.push({ DateVal: Number(new Date(activity.ActivityPeriods[0].Start.split('T')[0])), Sets: [set] });
                    }
                }
            });
        });
        refreshCalendar();
    }

    function unenrollParticipant(participant) {
        var messageVariables = { name: participant.FirstName + ' ' + participant.LastName };
        if (window.confirm(i18ng.t('register.confirm_unenroll', messageVariables))) {
            participant.isBusyEditing = true;
            return UserService.UnEnrollUserFromActivitySet(participant.UserId, vm.activitySet.Id).then(function () {
                FlashService.Success(i18ng.t('register.unenroll_success', messageVariables), false);
                if (vm.activitySet.AvailableSpots) vm.activitySet.AvailableSpots++;
            }).catch(function () {
                FlashService.Error(i18ng.t('register.unenroll_failure', messageVariables), false);
            }).finally(function () {
                participant.isBusyEditing = false;
                return vm.refreshUserList();
            });
        }
    }

    function openParticipantDialog(isReCertification, isBulkEntry, participant, user, participantComments) {
        var modalInstance = $modal.open({
            backdrop: 'static',
            templateUrl: 'app/register/popups/participant-edit-modal.html',
            controller: 'ParticipantEditModalController as vm',
            resolve: {
                isReCertification: function () { return isReCertification; },
                bulkParticipants: function () { return (isBulkEntry ? vm.selectedParticipants : []); },
                participants: function () { return vm.participants.list; },
                user: function () { return user; },
                languageId: function () { return vm.languageId; },
                customer: function () { return vm.customer; },
                participantComments: function () { return participantComments; },
                customLeadingTexts: function () {
                    return UserService.BuildCustomLeadingTextsArray(vm.customer, vm.purchaserEmail, participant);
                },
                activitySet: function () { return vm.activitySet; },
                participantStatusCodes: function () { return vm.participantStatusCodes; },
                countryCodes: function () { return vm.countryCodes; }
            },
            windowClass: 'modal-wider'
        });
        modalInstance.result.then(function () {
            vm.refreshUserList();
        });
        modalInstance.opened.then(function () {
            if (vm.participant) vm.participant.isBusyEditing = false;
        });
    }

    function editParticipant(participant, isReCertification, isBulkEntry) {
        vm.participant = participant;
        if (participant) {
            participant.isBusyEditing = true;
            var userObject, participantComments;
            UserService.GetById(participant.UserId).then(function (user) {
                userObject = user.data;
                return UserService.GetEnrollmentCommentsById(participant.EnrollmentId);
            }).then(function (comments) {
                participantComments = comments.data;
                openParticipantDialog(isReCertification, isBulkEntry, participant, userObject, participantComments);
                participant.isBusyEditing = false;
            });
        } else {
            openParticipantDialog(isReCertification, isBulkEntry);
        }
    }
    vm.showParticipantList = function (activitySetId) {
        vm.modeRegister = true;
        if (vm.activitySetId != activitySetId) {
            vm.activitySets = [];
            vm.activitySet = null;
            vm.activitySetId = activitySetId;
        }
    }
    vm.refreshUserList = function (activitySet) {
        if (activitySet)
            vm.activitySet = activitySet;
        else if (!vm.activitySet)
            vm.activitySet = vm.selectedActivitySets[0];

        vm.canUnenroll = vm.activitySet.IsUnenrollmentAllowed && (!vm.activitySet.UnenrollmentDeadline || new Date(vm.activitySet.UnenrollmentDeadline) > new Date());
        vm.courseIsWithinEnrollmentPeriod = (!vm.activitySet.EnrollmentFrom || new Date(vm.activitySet.EnrollmentFrom) < new Date())
                        && (!vm.activitySet.EnrollmentTo || new Date(vm.activitySet.EnrollmentTo) > new Date());
        vm.courseHasAvailableSpots = (vm.activitySet.AvailableSpots == null || vm.activitySet.AvailableSpots > 0);
        vm.canEdit = vm.isInstructor || vm.courseIsWithinEnrollmentPeriod;
        vm.participantUnenrollmentFunction = vm.canUnenroll ? unenrollParticipant : null;
        vm.participantEditFunction = vm.canEdit ? editParticipant : null;

        vm.participants.loading = true;
        return UserService.GetParticipantsByActivitySetByUser(vm.activitySet).then(function (result) {
            vm.participants.list = result.data;
        }).finally(function () {
            vm.participants.loading = false;
        });
    }


}
]);