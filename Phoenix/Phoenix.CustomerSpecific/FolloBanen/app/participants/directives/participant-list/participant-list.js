﻿angular.module('app').directive('participantList', [    
    function () {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                select: '&onSelect',
                edit: '=',
                unenroll: '=',
                selectionChanged: '&onSelectionChanged',
                participants: '=',
                loading: '=',
                customer: '=',
                activityset: '=',
                showCourseDate:'@'
                /*selectedParticipants:'='*/
            },
            templateUrl: 'app/participants/directives/participant-list/participant-list.html',
            controllerAs: 'vm',
            bindToController: true,
            controller: [                
                '$scope',
                'UserService',
                function ($scope, UserService) {
                    var vm = this;                    
                    vm.selectedParticipants = [];
					
                    $scope.$watch("vm.participants", function (newValue, oldValue) {
                        vm.selectedParticipants = [];
						if (vm.participants)
						{
							vm.participants.sort(function (a, b)
							{
								return ("" + a.LastName + a.FirstName).localeCompare("" + b.LastName + b.FirstName);
							});
							angular.forEach(vm.participants, function (p)
							{
								if (p.Comment === undefined)
								{
									p.Comment = "";
									UserService.GetEnrollmentCommentsById(p.EnrollmentId).then(function (response)
									{
										if (response.data.length>0)
											p.Comment = response.data[0].CommentText;
									});
								}
							});
						}

                        setTimeout(function () {
                            $(".editable-click").editable({
                                type: "text",
                                validate: function (value) {
                                    var textId = Number($(this).attr("data-pk").split("-")[1]);
                                    if (value && UserService.GetCustomLeadingTextType(textId)=="number" && $.isNumeric(value) == "") {
                                        return 'Only numbers are allowed';
                                    }
                                },
                                success: function (response, newValue) {
                                    var field = this;
                                    var uId = Number($(this).attr("data-pk").split("-")[0]);
                                    var textId = Number($(this).attr("data-pk").split("-")[1]);
                                    var user = $.grep(vm.participants, function (item) { return item.UserId == uId; })[0];
                                    var leadingText = { CustomLeadingText: { LeadingTextId: textId }, InfoText: newValue, InputType:UserService.GetCustomLeadingTextType(textId) };
                                    UserService.UpdateParticipationsWithCustomLeadingTexts(user._Participations, [leadingText]).then(function () {
                                        return UserService.GetById(uId).then(function (user) {
                                            UserService.UpdateUserWithCustomLeadingTexts(user.data, [leadingText]);
                                            return UserService.Update(user.data);
                                        });
                                    }).then(function () {
                                        $(field).removeClass("editable-unsaved");
                                    });                                    
                                }
                            });
                        }, 0);
                    });

					$scope.$watch("vm.selectedParticipants", function (newValue, oldValue)
					{
						if (vm.selectionChanged) vm.selectionChanged({ ids: newValue });
					});

					vm.getParticipantColor = function (participant)
					{
						var date = participant.ActivityStart ? new Date(participant.ActivityStart.split('T')[0]) : null;
						if (participant.ParticipantStatusCodeValue == "NOSHOW" || participant.ParticipantStatusCodeValue == "NOTATTENDED") return 'red';
						else if (participant.ParticipantStatusCodeValue == "ENROLLED" && date < new Date()) return 'yellow';
						else if (participant.ParticipantStatusCodeValue == "CANCELLED") return 'gray';
						return 'green';
					};

					vm.toggleSelect = function (participant)
					{
						if (participant)
						{
							var selectedParticipantIndex = vm.getSelectedParticipantIndex(participant);

							if (selectedParticipantIndex == -1)
							{
								vm.selectedParticipants.push(participant);
							} else
							{
								vm.selectedParticipants.splice(selectedParticipantIndex, 1);
							}
						} else
						{
							var allSelected = vm.isSelected();
							vm.selectedParticipants = [];
							if (!allSelected)
								$.each(vm.participants, function (index, item) { vm.toggleSelect(item); });

						}
					};

					vm.isSelected = function (participant)
					{
						if (participant) return vm.getSelectedParticipantIndex(participant) > -1;
						else return vm.selectedParticipants.length == vm.participants.length;
					};

					vm.getSelectedParticipantIndex = function (participant)
					{
						return vm.selectedParticipants.indexOf(participant);
					};

					vm.getParticipantCustomText = function (participant, textId)
					{
						var t = $.grep(participant.ParticipantInfoElements, function (o)
						{
							return (o.LeadingTextId == textId);
						})[0];
						if (t) return t.InfoText;
					};
                    
            }]
        };
    }
]);