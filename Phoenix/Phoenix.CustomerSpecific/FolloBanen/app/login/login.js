﻿angular.module('app.login', ['ui.router', 'ngCookies', 'app.config'])
    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider.state('login', {
            url: '/login',
            controller: 'LoginController',
            templateUrl: 'app/login/login.view.html',
            controllerAs: 'vm'
        });
    }])

.controller('LoginController', [
    '$location',
    'AuthenticationService',
    'FlashService',
    'app.config',
function ($location, AuthenticationService, FlashService, Config) {
    'use strict';
    var vm = this;

    vm.login = login;
    /*
    (function initController() {
        // reset login status
        AuthenticationService.ClearCredentials();
    })();*/

    function login() {
        vm.dataLoading = true;
        AuthenticationService.Login(vm.username, vm.password, function (response, status, headers, config) {
            if (status == 200) {
                $location.path(Config.defaultUrl);
            } else {
                FlashService.Error("User not found.");
                vm.dataLoading = false;
            }
        });
    };
}
]);