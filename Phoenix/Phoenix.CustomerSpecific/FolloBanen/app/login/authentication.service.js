﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('AuthenticationService', AuthenticationService);

    AuthenticationService.$inject = ['$http', '$cookies', '$rootScope', '$timeout', 'UserService'];
    function AuthenticationService($http, $cookies, $rootScope, $timeout, UserService) {
        var service = {};

        service.Login = Login;        
        service.ClearCredentials = ClearCredentials;

        return service;

        function Login(username, password, callback) {
            //ClearCredentials();
            /* Use this for real authentication
             ----------------------------------------------*/
            $http.post('/api/accounts/' + escape(username) + "/logon", { password: password })
                .success(function (response, status, headers, config) {
                    callback(response, status, headers, config);
                }).error(function (response, status, headers, config) {
                    callback(response, status, headers, config);
                })

        }
        

        function ClearCredentials() {
            $rootScope.globals = {};                        
            document.cookie = 'PhoenixAuth=; path=/; expires=' + new Date(0).toUTCString();            
        }
    }


})();