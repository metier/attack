﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('CustomerService', CustomerService);

    CustomerService.$inject = ['$http'];
    function CustomerService($http) {
        var service = {};

        service.GetById = GetById;
        service.GetByDistributor = GetByDistributor;
        
        return service;        

        function GetById(id) {
            return $http.get('/api/customers/' + id);
        }
        function GetByDistributor(DistributorId) {
            return $http.get('/api/customers?distributorId=' + DistributorId);
        }
        

        // private functions

        function handleSuccess(data) {
            return data.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
