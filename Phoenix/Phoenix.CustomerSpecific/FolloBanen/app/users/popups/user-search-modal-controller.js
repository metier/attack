﻿angular.module('app.users')
    .controller('UserSearchModalController', [                    
        '$modalInstance',                
        function ($modalInstance) {                
            'use strict';
            var vm = this;
            
            vm.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            vm.saveAndClose = function (user) {
                $modalInstance.close(user.Id);
            }
            vm.queryComplete = function (data) {
                vm.users = data;
            }
    }
]);
