﻿angular.module('app').directive('userList', [    
    function () {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                'select': '=',
                'showParticipantList': '=',
                'queryComplete': '&onQueryComplete'
            },
            templateUrl: 'app/users/directives/user-list/user-list.html',
            controllerAs: 'vm',
            bindToController: true,
            controller: [
                '$rootScope',                
                '$modal',                                
                'CustomerService',
                'UserService',                                
                function ($rootScope, $modal, CustomerService, UserService) {
                    var vm = this;

                    vm.users = { TotalCount: 0, Items: [] };
                    
                    vm.getSortCol = function () {
                        return vm.searchOptions.orderBy;
                    };
                    vm.getSortOrder = function () {
                        return vm.searchOptions.orderDirection;
                    };
                    vm.getPageLimit = function () {
                        return vm.searchOptions.limit;
                    };
                    vm.setPageLimit = function (limit) {
                        vm.searchOptions.limit = limit;
                        vm.searchOptions.page = 1;
                        vm.search();
                    };
                    vm.setSort = function (col, order) {
                        vm.searchOptions.orderBy = col;
                        if (order) vm.searchOptions.orderDirection = order;
                        vm.search();
					};
					vm.hideUser = function (user)
					{
						user.expanded = !user.expanded;
						return UserService.GetById(user.Id).then(function (result)
						{
							result.data.User.ExpiryDate = new Date();
							return UserService.Update(result.data).then(function (result)
							{								
								vm.users.Items.splice(vm.users.Items.indexOf(user), 1);
							});
						});
					};
                    vm.searchOptions = {
                        distributorId: $rootScope.settings.distributors[0].Id,
                        query: '',
                        page: 1,
                        limit: 10,
                        orderBy: 'id',
                        alsoSearchCustomFields: true,
						includeParticipations: true,
						isActive: true,
                        orderDirection: 'asc'
                    };

					vm.search = function ()
					{
						vm.isSearching = true;
						vm.users.Items = [];
						if (vm.runningSearch)
							vm.runningSearch.cancel();

						vm.runningSearch = UserService.Search(vm.searchOptions);
						
						return vm.runningSearch.then(function (result)
						{
							vm.users = result.data;
							if (vm.queryComplete) vm.queryComplete({ data: result.data });
							if (!vm.customer && vm.users.Items.length > 0)
								return CustomerService.GetById(vm.users.Items[0].CustomerId).then(function (result)
								{
									vm.customer = result.data;
								});
						}).finally(function (result)
						{
							vm.isSearching = false;
							vm.runningSearch = null;
						});
					};                   

                    vm.search();
                    setTimeout(function () { $('#search').focus(); }, 0);
                    
            }]
        };
    }
]);