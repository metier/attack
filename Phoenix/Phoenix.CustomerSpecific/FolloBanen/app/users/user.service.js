﻿angular
    .module('app')
    .factory('UserService', [
        '$http',
        '$q',
        '$filter',
function UserService($http, $q, $filter) {
    'use strict';
    var service = {};

    service.GetActivitySetsByCustomer = GetActivitySetsByCustomer;
    service.GetActivitySetsByInstructor = GetActivitySetsByInstructor;

    service.GetActivitysetById = GetActivitysetById;
    service.GetInstructorsByDistributor = GetInstructorsByDistributor;
    service.IsUserInstructor = IsUserInstructor;

    service.GetByCustomer = GetByCustomer;
    service.GetByDistributor = GetByDistributor;

    service.GetByActivitySet = GetByActivitySet;
    service.GetParticipantsByActivitySet = GetParticipantsByActivitySet;
    service.GetParticipantsByActivitySetByUser = GetParticipantsByActivitySetByUser;

    service.GetParticipantsByUser = GetParticipantsByUser;
    service.GetParticipantsByUserByUser = GetParticipantsByUserByUser;

    service.GetParticipantStatusCodes = GetParticipantStatusCodes;
    service.GetCountryCodes = GetCountryCodes;

    service.GetCurrent = GetCurrent;
    service.GetByUsername = GetByUsername;
    service.GetByEmail = GetByEmail;
    service.GetById = GetById;
    service.Search = Search;
    service.Create = Create;
    service.Update = Update;
    service.GetParticipantById = GetParticipantById;
    service.UpdateParticipant = UpdateParticipant;
	service.UpdateParticipantStatus = UpdateParticipantStatus;
	service.UpdateParticipantStatusAndCompletedDate = UpdateParticipantStatusAndCompletedDate;
    service.EnrollUserInActivitySet = EnrollUserInActivitySet;
    service.UnEnrollUserFromActivitySet = UnEnrollUserFromActivitySet;
    service.SendEnrollmentConfirmation = SendEnrollmentConfirmation;

    service.GetParticipantCommentsById = GetParticipantCommentsById;
    service.UpdateComment = UpdateComment;
    service.AddComment = AddComment;
    service.GetEnrollmentCommentsById = GetEnrollmentCommentsById;

    service.BuildCustomLeadingTextsArray = BuildCustomLeadingTextsArray;
    service.PrepareCustomLeadingTextsForSubmission = PrepareCustomLeadingTextsForSubmission;
    service.UpdateUserWithCustomLeadingTexts = UpdateUserWithCustomLeadingTexts;
    service.UpdateParticipationsWithCustomLeadingTexts = UpdateParticipationsWithCustomLeadingTexts;
    service.GetCustomLeadingTextType = GetCustomLeadingTextType;
    service.GetAllLanguages = GetAllLanguages;
    return service;

    function PrepareCustomLeadingTextsForSubmission(data, type) {
        if (type == "date" && data) return $filter('date')(data, 'yyyy-MM-dd');
        return data;
    };

    function GetActivitySetsByCustomer(customerId, courseCalendarMode) {
        return $http.get('/api/activitysets?customerId=' + customerId + '&calculateAvailableSpots=true&courseCalendar=' + (courseCalendarMode ? 'true' : 'false'));
    }
    function GetActivitySetsByInstructor(instructorId) {
        return $http.get('/api/activitysets?instructorUserId=' + instructorId);
    }

    //Todo: move to other service
    function GetActivitysetById(setId) {
        return $http.get('/api/activitysets/' + setId);
    }
    //Todo: move to other service

    function GetParticipantStatusCodes() {
        return GetCodes("participantstatuses");
    }
    function GetCountryCodes() {
        return GetCodes("countrycodes");
    }
    function GetCodes(code) {
        return $http.get('/api/codes?type=' + code);
    }
    function GetAllLanguages() {
        return $http.get('/api/languages');
    }


    function GetInstructorsByDistributor(distributorId, limit) {
        return $http.get('/api/users?instructorUsersOnly=true&limit=' + (limit ? limit : 999999) + '&distributorId=' + distributorId);
    }
    function IsUserInstructor(distributorId, userId) {
        var deferred = $q.defer();
        return GetInstructorsByDistributor(distributorId).then(function (result) {
            var match = $.grep(result.data.Items, function (item) { return item.Id == userId; })[0];
            deferred.resolve(match != null);
            return deferred.promise;
        });
    }

    function GetByCustomer(customerId, limit) {
        return $http.get('/api/users?limit=' + (limit ? limit : 999999) + '&customerId=' + customerId);
    }
    function GetByDistributor(distributorId, limit) {
        return $http.get('/api/users?limit=' + (limit ? limit : 999999) + '&distributorId=' + distributorId);
    }
    function GetByActivitySet(setId, limit) {
        return $http.get('/api/users?limit=' + (limit ? limit : 999999) + '&activitySetId=' + setId);
    }
    function GetParticipantsByActivitySet(setId) {
        return $http.get('/api/participants?includeParticipantInfoElements=true&includeParticipantsEnrolledInActivitySetButThroughOtherActivitySet=true&activitySetId=' + setId);
    }
    function GetParticipantsByActivitySetByUser(activitySet)
    {
        return $http.get('/api/participants?includeParticipantInfoElements=true&includeParticipantsEnrolledInActivitySetButThroughOtherActivitySet=true&activitySetId=' + activitySet.Id).then(function (result) {
            //var processedUsers = {};
            var userGroups = {};
            angular.forEach(result.data.Items, function (participant)
            {
                var a = $.grep(activitySet.Activities, function (act) { return act.Id == participant.ActivityId; })[0];
                participant.Order = ((((a || {}).CustomerArticle || {}).Module || {}).Order || 0);
                if (!userGroups[participant.UserId]) userGroups[participant.UserId] = [];
                userGroups[participant.UserId].push(participant);
                //participant.SortKey = (!!participant.ActivityStart ? participant.ActivityStart.split('T')[0] : "xx") + participant.ActivityName;
                //if (!processedUsers[participant.UserId])
                //{
                //    processedUsers[participant.UserId] = participant;
                //    users.push(participant);
                //    participant._Participations = [participant];
                //}
                //else
                //{
                //    processedUsers[participant.UserId]._Participations.push(participant);
                //}
            });
            var users = [];
            angular.forEach(userGroups, function (group)
            {
                var sortedUserParticipations = group.sort(function (a, b) { return a.Order - b.Order; });
                sortedUserParticipations[0]._Participations = sortedUserParticipations;
                users.push(sortedUserParticipations[0]);
            });

            //angular.forEach(users, function (u, index)
            //{
            //    if (!!u._Participations)
            //    {
            //        u._Participations.sort(function (a, b) { return a.Order - b.Order; });
            //        users[]
            //    }
            //        //u._Participations = u._Participations.sort(function (a, b) { return (a.SortKey < b.SortKey ? -1 : (a.SortKey > b.SortKey ? 1 : 0)); });
            //});
            return { data: users };
        });
    }
    function GetParticipantsByUser(userId) {
        return $http.get('/api/participants?includeParticipantInfoElements=true&userId=' + userId);
    }
    function GetParticipantsByUserByUser(userId) {
        return $http.get('/api/participants?includeParticipantInfoElements=true&userId=' + userId).then(function (result) {
            var sets = [];
            angular.forEach(result.data.Items, function (participant) {
                var set = $.grep(sets, function (participant1) {
                    return participant1.ActivitySetId == participant.ActivitySetId;
                })[0];
                if (!set) {
                    set = participant;
                    sets.push(set);
                    set._Participations = [participant];
                } else {
                    set._Participations.push(participant);
                }
                var activityDate = participant.ActivityStart ? new Date(participant.ActivityStart.split('T')[0]) : null;
                if (activityDate && (!set.EarliestDate || set.EarliestDate > activityDate)) set.EarliestDate = activityDate;
            });
            sets = sets.sort(function (a, b) { return a.EarliestDate - b.EarliestDate; });
            return { data: sets };
        });
    }

    function GetCurrent() {
        return $http.get('/api/accounts/current');
    }

    function GetByUsername(username) {
        return $http.get('/api/accounts?username=' + escape(username));
    }
    function GetByEmail(email) {
        return $http.get('/api/accounts?email=' + escape(email));
    }
    function GetById(id) {
        return $http.get('/api/accounts?userId=' + id);
    }
	function Search(options)
	{
        var parameters = {
            distributorId: options.distributorId,
            customerId: options.customerId,
            activityId: options.activityId,
            activitySetId: options.activitySetId,
            phoenixUsersOnly: options.phoenixUsersOnly,
            query: options.query,
            alsoSearchCustomFields: options.alsoSearchCustomFields,
			includeParticipations: options.includeParticipations,
			isActive: options.isActive,
            limit: options.limit,
            orderBy: options.orderBy,
			orderDirection: options.orderDirection,
			advancedQuery: (options.hse ? "LeadingText14:" + options.hse + "|" : "") + (options.employer ? "LeadingText15:" + options.employer + "|" : "")
		};

        if (typeof options.page === 'number' && options.page > 0) {
            parameters.skip = (options.page - 1) * parameters.limit;
        }

		var canceller = $q.defer();
		var p = $http.get('/api/users', { params: parameters, timeout: canceller.promise });
		p.cancel = function () { canceller.resolve("user cancelled"); };
		return p;
    }

    function Create(user) {
        return $http.post('/api/accounts?autoGeneratePassword=true&source=LearningPortal', user);
    }

    function Update(user) {
        return $http.put('/api/accounts', user);
    }
    function GetParticipantById(id) {
        return $http.get('/api/participants/' + id);
    }
    function UpdateParticipant(participant) {
        return $http.put('/api/participants/' + participant.Id, participant);
    }

    function UpdateParticipantStatus(participantId, status) {
        return $http.put('/api/participants/' + participantId + '/status/' + status);
	}
	function UpdateParticipantStatusAndCompletedDate(participantId, status, completedDate)
	{
		return $http.put('/api/participants/' + participantId + '/status', { SetStatus: true, Status: status, SetCompletedDate: true, CompletedDate: completedDate});
	}
    function EnrollUserInActivitySet(userId, activitySetId, data) {
        return $http.put('/api/activitysets/' + activitySetId + '/enroll?userId=' + userId, data);
    }
    function UnEnrollUserFromActivitySet(userId, activitySetId) {
        return $http.put('/api/activitysets/' + activitySetId + '/unenroll?userId=' + userId);
    }
    function SendEnrollmentConfirmation(toEmail, subject, html) {
        return $http.post('/api/_/sendenrollmentemail', { ToEmail: toEmail, Subject: subject, Body: html });
    }
    function GetParticipantCommentsById(id) {
        return $http.get('/api/comments?entity=participant&entityId=' + id);
    }
    function UpdateComment(comment) {
        return $http.put('/api/comments', comment);
    }
    function AddComment(comment) {
        return $http.post('/api/comments', comment);
    }
    function UpdateUserWithCustomLeadingTexts(user, customLeadingTexts) {
        $.each(customLeadingTexts, function (index, item) {
            var match = $.grep(user.User.UserInfoElements, function (itemToMatch) {
                return item.CustomLeadingText.LeadingTextId == itemToMatch.LeadingTextId && itemToMatch.LeadingTextId != 17;
            })[0];
            if (match)
                match.InfoText = service.PrepareCustomLeadingTextsForSubmission(item.InfoText, item.InputType);
        });
    }
    function UpdateParticipationsWithCustomLeadingTexts(participations, customLeadingTexts) {
        var returnFunction = $q.resolve();
        $.each(participations, function (index, participation) {
            returnFunction = returnFunction.then(function () {
                return service.GetParticipantById(participation.Id);
            }).then(function (result) {
                var resultCopy = angular.copy(result.data);
				result.data.StatusCodeValue = participation.ParticipantStatusCodeValue;
				result.data.CompletedDate = participation.ParticipantCompletedDate;
                if (result.data.StatusCodeValue == "COMPLETED" && !result.data.Result) result.data.Result = 100;
                $.each(customLeadingTexts, function (index, leadingText) {
                    var currentElement = $.grep(result.data.ParticipantInfoElements, function (elementToUpdate) { return leadingText.CustomLeadingText.LeadingTextId == elementToUpdate.LeadingTextId; })[0];
                    if (currentElement) {
                        currentElement.InfoText = service.PrepareCustomLeadingTextsForSubmission(leadingText.InfoText, leadingText.InputType);
                        var participationElement = $.grep(participation.ParticipantInfoElements, function (elementToUpdate) { return leadingText.CustomLeadingText.LeadingTextId == elementToUpdate.LeadingTextId; })[0];
                        if (participationElement) participationElement.InfoText = currentElement.InfoText;
                    }
                });
                if (!angular.equals(result.data, resultCopy))
                    return service.UpdateParticipant(result.data).then(function ()
                    {
                        // Manually set CompletedDate if it changed to COMPLETED, since Phoenix will use today's date when status changes to COMPLETED
                        if (!!result.data.CompletedDate && result.data.StatusCodeValue == "COMPLETED" && resultCopy.StatusCodeValue != "COMPLETED")
                            return service.UpdateParticipantStatusAndCompletedDate(participation.Id, participation.ParticipantStatusCodeValue, participation.ParticipantCompletedDate);
                    });

                    // deferred = deferred.then(function ()
                            //{
                            //    return UserService.UpdateParticipantStatusAndCompletedDate(participation.Id, currentParticipation.ParticipantStatusCodeValue, currentParticipation.ParticipantCompletedDate);
                            //});

            });
        });
        return returnFunction;
    }
    function GetCustomLeadingTextType(id) {
        if (id == 14) return "number";
        if (id == 11) return "date";
        return "text";
    }
    function GetEnrollmentCommentsById(id) {
        return $http.get('/api/comments?entity=enrollment&entityId=' + id);
    }
    function BuildCustomLeadingTextsArray(customer, purchaserEmail, participant, user) {
        var values = [];
        angular.forEach(customer.CustomLeadingTexts, function (item) {
            var value = "";
            if (participant) {
                value = $.grep(participant.ParticipantInfoElements, function (text) { return text.LeadingTextId == item.LeadingTextId; })[0];
            } else if (user) {
                value = $.grep(user.User.UserInfoElements, function (text) { return text.LeadingTextId == item.LeadingTextId; })[0];
            }
            if (value) value = value.InfoText;
            if (item.LeadingTextId == 17 && !value) value = purchaserEmail;
            if (item.LeadingTextId == 14) {
                if (value === null || value===undefined || ("" + value) === "") value = null;
                else value = Number(value);
            }
            if (item.LeadingTextId == 11) {
                if (value) value = new Date(value);
            }
            values.push({ CustomLeadingText: { Id: item.Id, Text: item.Text, IsMandatory: item.IsMandatory, IsVisible: (participant || user || value == "" || value == null ? item.IsVisible : false), LeadingTextId: item.LeadingTextId }, InfoText: value, InputType: GetCustomLeadingTextType(item.LeadingTextId) });
        });
        return values;
    }
}
    ]);