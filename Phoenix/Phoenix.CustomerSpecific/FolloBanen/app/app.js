﻿'use strict';
angular.module('app', [
    'i18ng',
    'ui.router',
    'app.config',
    'app.register',
    'app.login',
    'app.users'
]).
filter('to_trusted', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]).
directive('compile', ['$compile', function ($compile) {
    return function (scope, element, attrs) {
        scope.$watch(
        function (scope) {
            return scope.$eval(attrs.compile);
        },
        function (value) {
            element.html(value);
            $compile(element.contents())(scope);
        }
    )
    };
}]).
config(['i18ngProvider', '$urlRouterProvider', '$httpProvider', 'app.config', function (i18ngProvider, $urlRouterProvider, $httpProvider, appConfig) {
    i18ngProvider.init({
        /*lng: 'en',*/
        resGetPath: 'locales/__lng__/__ns__.json',
        fallbackLng: 'en'
    });
    $.fn.editable.defaults.mode = 'inline';

    $urlRouterProvider.otherwise('/login');

    $httpProvider.interceptors.push(['$q', 'FlashService', '$location', 'i18ng', '$injector', function ($q, FlashService, $location, i18ng, $injector) {
        return {
            'request': function (config) {
                if (config.url.toLowerCase().indexOf("/api") == 0) {
                    var query = config.url.substring(4);
                    var baseUrl = appConfig.apiPath;
                    if (!config.data) config.data = "";
            
                    for (var param in config.params) {
                        if (config.params[param])
                            query += (query.indexOf("?") == -1 ? "?" : "&") + param + "=" + config.params[param];
                    }
                    config.url = baseUrl + encodeURIComponent(query);
                    config.params = null;
                }
                if (!$httpProvider.defaults.headers.get) {
                    $httpProvider.defaults.headers.get = {};
                }
                $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
                $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
                return config;
            }
            , 'response': function (config) {                                                                
                return config;
            },
            'responseError': function (rejection) {
                if (['/login'].indexOf($location.path()) === -1 && (rejection.status == 403 || rejection.status == 401)) {                    
                    if (rejection.config.url.indexOf("endpoint=%2Faccounts%2Fcurrent") == -1) {
                        //Check if session is valid
                        var UserService = $injector.get('UserService');
                        UserService.GetCurrent().catch(function () {                                                        
                            $location.path('/login');
                            FlashService.Error(i18ng.t('login.timeout'), true);
                            setTimeout(function () { FlashService.Clear(); }, 100);                            
                        });
                    }                    
                }
                return $q.reject(rejection);
            }
        };
    }]);
}])
.run(['$rootScope', '$location', 'UserService','CustomerService', 'app.config', function ($rootScope, $location, UserService, CustomerService, appConfig) {
    
    var distributors = [], customers = [];
    angular.forEach(appConfig.distributors, function (item) {
        distributors.push(item);
    });
    angular.forEach(appConfig.customers, function (item) {
        customers.push(item);
    });
    
    $rootScope.globals = {};
    $rootScope.settings = { distributors: distributors, customers: customers, allLanguages:[] };

    $rootScope.dateOptions = {
        'year-format': "'yyyy'",
        'starting-day': 1
    };

    $rootScope.logout = function () {
        $rootScope.globals = {};
        document.cookie = 'PhoenixAuth=; path=/; expires=' + new Date(0).toUTCString();
        $location.path('/login');
    }

    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        // redirect to login page if not logged in and trying to access a restricted page
        var restrictedPage = ['/login'].indexOf($location.path()) === -1;
        var loggedIn = $rootScope.globals.currentUser;
        if (restrictedPage && !loggedIn) {
            UserService.GetCurrent().then(function (user) {
                $rootScope.globals.currentUser = user.data;
                return UserService.IsUserInstructor($rootScope.settings.distributors[0].Id, $rootScope.globals.currentUser.User.Id);
            }).then(function (isInstructor) {
                $rootScope.globals.isInstructor = isInstructor;
                UserService.GetAllLanguages().then(function (languages) {
                    $rootScope.settings.allLanguages = languages.data;
                });
                if ($rootScope.settings.customers.length > 0 && !$rootScope.settings.customers[0].Value) {
                    return CustomerService.GetByDistributor($rootScope.settings.distributors[0].Id).then(function (customers) {
                        $.each($rootScope.settings.customers, function (index, customer) {
                            var match = $.grep(customers.data.Items, function (item) { return item.Id == customer.Id; })[0];
                            if (match) customer.Value = match.Name;
                        });
                    });
                }                
            }).catch(function () {
                $location.path('/login');
            });
        }
    });

}
]);