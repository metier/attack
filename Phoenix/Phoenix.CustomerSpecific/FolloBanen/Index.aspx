﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Phoenix.CustomerSpecific.FolloBanen.Index" %>
<!DOCTYPE html>
<html ng-app="app">
<head>
    <meta charset="utf-8" />
    <title>Follobanen Safety Course Registration</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link href="app/css/app.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    
    <link href="css/jquery-ui.css" rel="stylesheet" />
    <link href="js/jqueryui-editable/css/jqueryui-editable.css" rel="stylesheet" />
    
    
</head>
<body>

    <div class="jumbotron">
        <div class="container">
            <div>
                <div ng-class="{ 'alert': flash, 'alert-success': flash.type === 'success', 'alert-danger': flash.type === 'error' }" ng-if="flash" compile="flash.message"></div>
                <button type="button" class="logout" ng-click="logout()" ng-show="globals.currentUser" i18ng="'global.logout'"></button>
                <div ui-view></div>
            </div>
        </div>
    </div>       
    <!---->
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    
    <script src="js/angular.min.js"></script>
    <script src="js/angular-ui-router.min.js"></script>
       

    <script src="js/angular-cookies.js"></script>
    <script src="js/ui-bootstrap-tpls-0.13.3.min.js"></script>
    <script src="js/i18next-1.10.1.min.js"></script>
    <script src="js/i18ng.min.js"></script>
    <script src="js/jqueryui-editable/js/jqueryui-editable.js"></script>
    
    
    <script type="text/javascript">
        angular.module('app.config', []).constant('app.config', {
        basePath: '/CustomerSpecific/follobanen/',        
        customers: <%=CustomerIds%>,        
        distributors: <%=DistributorIds%>,
        apiPath: 'Shared/API.aspx?endpoint=',        
        defaultUrl: '/register',
        defaultState: 'register'
    });
    </script>

    <script src="app/app.js?1"></script>
    
    <script src="app/login/authentication.service.js"></script>
    <script src="app/users/user.service.js?2"></script>
    <script src="app/shared/flash.service.js"></script>
        
    <script src="app/customers/customer.service.js"></script>

        
    <script src="app/login/login.js"></script>
    <script src="app/register/register.js"></script>
    <script src="app/users/users.js"></script>

    <script src="app/shared/directives/print/print.js"></script>
    <script src="app/shared/directives/sortable-header/sortable-header.js"></script>
    <script src="app/shared/directives/page-limit-selector/page-limit-selector.js"></script>    
    <script src="app/users/directives/user-list/user-list.js?1"></script>    
    <script src="app/participants/directives/participant-list/participant-list.js?2"></script>
    

    <script src="app/register/popups/participant-edit-modal-controller.js?4"></script>
    <script src="app/users/popups/user-search-modal-controller.js"></script>    
</body>
</html>