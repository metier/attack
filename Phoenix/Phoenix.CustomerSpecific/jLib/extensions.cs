﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using jlib.functions;
using System.Security.Cryptography;
using System.Xml;
using System.Xml.Linq;
using System.Dynamic;
using System.Text.RegularExpressions;
using Microsoft.CSharp.RuntimeBinder;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using System.Net;
using System.Collections;
using System.Reflection;
using System.Runtime.Serialization;
using System.Threading;
using System.IO;

namespace jlib.functions {
	public static class extensions {
		//public static bool HasProperty(this dynamic obj, string property){
		//    foreach (var s in obj.GetDynamicMemberNames()) 
		//        if(property==s) return true;                    
		//    return false;
		//}

		public static T AsEnum<T>(this string Value)
			where T : struct {
			T o;
			Enum.TryParse(Value, true, out o);

			//Return
			return o;
		}


		public static void Use<T>(this T item, Action<T> work) {
			work(item);
		}

		public static DataRow SafeGetRow(this DataTable oTable, int iRow) {
			if (iRow > -1 && oTable.Rows.Count > iRow) return oTable.Rows[iRow];
			return null;
		}
		public static DataRow SafeGetRow(this DataRow[] oRows, int iRow) {
			if (iRow > -1 && oRows.Length > iRow) return oRows[iRow];
			return null;
		}
		public static List<DataRow> ToList(this DataRow[] oRows) {
			List<DataRow> oLRows = new List<DataRow>();
			for (int x = 0; x < oRows.Length; x++) oLRows.Add(oRows[x]);
			return oLRows;
		}
		

		public static Dictionary<string, object> ToDictionary(this DataRow oRow) {
			Dictionary<string, object> o = new Dictionary<string, object>();
			for (int x = 0; x < oRow.Table.Columns.Count; x++) o.Add(oRow.Table.Columns[x].ColumnName, oRow[x]);
			return o;
		}
		public static string SafeGetValue(this DataTable oTable, int iRow, string sCol) {
			if (iRow > -1 && oTable.Rows.Count > iRow && oTable.Columns.Contains(sCol)) return convert.cStr(oTable.Rows[iRow][sCol]);
			return null;
		}
		public static string SafeGetValue(this DataTable oTable, int iRow, int iCol) {
			if (iRow > -1 && oTable.Rows.Count > iRow && oTable.Rows[iRow].ItemArray.Length > iCol) return convert.cStr(oTable.Rows[iRow][iCol]);
			return null;
		}
		public static string SafeGetValue(this DataRow[] oRows, int iRow, string sCol) {
			if (iRow > -1 && oRows.Length > iRow && oRows[iRow].Table.Columns.Contains(sCol)) return convert.cStr(oRows[iRow][sCol]);
			return null;
		}
		public static string SafeGetValue(this DataRow[] oRows, int iRow, int iCol) {
			if (iRow > -1 && oRows.Length > iRow && oRows[iRow].ItemArray.Length > iCol) return convert.cStr(oRows[iRow][iCol]);
			return null;
		}
		public static string SafeGetValue(this DataRow oRow, string sCol) {
			if (oRow == null || !oRow.Table.Columns.Contains(sCol)) return null;
			return oRow[sCol].Str();
		}
		public static string SafeFormat(this DataRow oRow, string sFormat, params string[] sFields) {
			if (oRow == null) return null;
			object[] oArr = new object[sFields.Length];
			for (int x = 0; x < oArr.Length; x++) oArr[x] = oRow[sFields[x]];
			return String.Format(sFormat, oArr);
		}
		//Replaces all entries of n0 in a list with n1
		public static int Replace<T>(this IList<T> List, params T[] oValues) {
			if (List == null) return 0;
			int iNumReplacements = 0;
			for (int x = 0; x < oValues.Length; x = x + 2) {
				List<int> Replacements = List.IndexOfMulti(oValues[x]);
				Replacements.ForEach(Index => List[Index] = oValues[x + 1]);
				iNumReplacements += Replacements.Count;
			}
			return iNumReplacements;
		}

		public static bool Contains<T>(this IList<T> List, T value, StringComparison comparison) {
			if (List == null) return false;
			if (typeof(T) == typeof(string)) {
				string find = value as string;
				foreach (var s in List) {
					string item = s as string;
					if (find == null && item == null) return true;
					if (find.Equals(item, comparison)) return true;
				}
				return false;
			}
			return List.Contains(value);
		}
		public static List<T> Trim<T>(this List<T> List) {
			for (int x = 0; x < List.Count; x++)
				if (List[x].IsNullOrEmpty()) List.RemoveAt(x--);
			return List;
		}
		

		//Swaps all entries of n0 with n1, and swap all entries of n1 with n0
		public static int Swap<T>(this IList<T> List, params T[] oValues) {
			if (List == null) return 0;
			int iNumSwaps = 0;
			for (int x = 0; x < oValues.Length; x = x + 2) {
				List<int> Swap1 = List.IndexOfMulti(oValues[x]);
				List<int> Swap2 = List.IndexOfMulti(oValues[x + 1]);
				Swap1.ForEach(Index => List[Index] = oValues[x + 1]);
				Swap2.ForEach(Index => List[Index] = oValues[x]);
				iNumSwaps += Swap1.Count;
			}
			return iNumSwaps;
		}
		public static TKey SafeGetValue<TKey>(this IList<TKey> oList, int iIndex) {
			if (iIndex > -1 && iIndex < oList.Count) return oList[iIndex];
			return default(TKey);
		}
		public static IList<T> Shuffle<T>(this IList<T> list) {
			Random rnd = new Random(unchecked(Environment.TickCount * 31 + System.Threading.Thread.CurrentThread.ManagedThreadId));
			int n = list.Count;
			while (n > 1) {
				n--;
				int k = rnd.Next(n + 1);
				T value = list[k];
				list[k] = list[n];
				list[n] = value;
			}
			return list;
		}
		public static TKey Max<TKey>(this IList<TKey> oList) {
			TKey o = (oList.Count == 0 ? default(TKey) : oList[0]);
			for (int x = 1; x < oList.Count; x++)
				if (((IComparable)o).CompareTo(oList[x]) < 0) o = oList[x];
			return o;
		}
		public static TKey Min<TKey>(this IList<TKey> oList) {
			TKey o = (oList.Count == 0 ? default(TKey) : oList[0]);
			for (int x = 1; x < oList.Count; x++)
				if (((IComparable)o).CompareTo(oList[x]) > 0) o = oList[x];
			return o;
		}
		public static bool AddIfNotExist<TValue>(this IList<TValue> oList, TValue oValue) {
			if (oList.Contains(oValue)) return false;
			oList.Add(oValue);
			return true;
		}
		public static bool ContainsOtherEntries<TValue>(this IList<TValue> oList, TValue oValue) {
			for (int x = 0; x < oList.Count; x++) if (!oList[x].Equals(oValue)) return true;
			return false;
		}
		public static string Join<TValue>(this IList<TValue> oList, string sSeparator) {
			StringBuilder oSB = new StringBuilder();
			for (int x = 0; x < oList.Count; x++) oSB.Append(oList[x] + (x < oList.Count - 1 ? sSeparator : ""));
			return oSB.ToString();
		}
		public static List<int> IndexOfMulti<TValue>(this IList<TValue> oList, TValue oValue) {
			List<int> i = new List<int>();
			for (int x = 0; x < oList.Count; x++) if (oList[x] != null && oList[x].Equals(oValue)) i.Add(x);
			return i;
		}
		//}
		//public static class dictionary {
		public static TKey FindKeyByValue<TKey, TValue>(this IDictionary<TKey, TValue> oDictionary, TValue oValue) {
			foreach (KeyValuePair<TKey, TValue> oPair in oDictionary) if (oValue.Equals(oPair.Value)) return oPair.Key;
			return default(TKey);
		}
		public static bool AddIfNotExist<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue value) {
			if (dict.ContainsKey(key)) return false;
			dict.Add(key, value);
			return true;
		}

		public static IEnumerable<TKey> FindKeysByValue<TKey, TValue>(this IDictionary<TKey, TValue> oDictionary, TValue oValue) {
			return from KeyValuePair<TKey, TValue> oPair in oDictionary where (oValue.Equals(oPair.Value)) select oPair.Key;
		}

		public static TValue SafeGetValue<TKey, TValue>(this IDictionary<TKey, TValue> oDictionary, params TKey[] oKey) {
			TValue oValue;
			for (int x = 0; x < oKey.Length; x++)
				if (oKey[x] != null && oDictionary.TryGetValue(oKey[x], out oValue)) return oValue;

			return default(TValue);
		}
		//}
		//public static class math {

		public static object Median(this IEnumerable<object> oList) {
			object[] oArr = oList.ToArray();
			Array.Sort(oArr);
			if (oArr.Length == 0) {
				return "N/A";// throw new InvalidOperationException("Empty collection");
			} else if (oArr.Length % 2 == 0 && convert.isNumeric(oArr[oArr.Length / 2 - 1]) && convert.isNumeric(oArr[oArr.Length / 2])) {
				return (convert.cDbl(oArr[oArr.Length / 2 - 1]) + convert.cDbl(oArr[oArr.Length / 2])) / 2d;
			} else {
				return oArr[oArr.Length / 2];
			}
		}

		public static bool ContainsI(this string sStr, string sComparison) {
			return sStr.Str().ToLower().Contains(sComparison.Str().ToLower());
		}

		//public static int Sum(this IEnumerable<int> list) {
		//    // While you could implement this, you could also use Enumerable.Sum()
		//}
		//}
		//public static class generic {
		public static string ToCamelCase(this string sStr) {
			if (sStr.IsNullOrEmpty()) return sStr;
			string[] sArr = sStr.Str().Split(' ', '-', '.');
			StringBuilder oSB = new StringBuilder();
			for (int x = 0; x < sArr.Length; x++)
				oSB.Append((oSB.Length == 0 ? "" : sStr.Substring(oSB.Length, 1)) + (sArr[x].IsNullOrEmpty() ? "" : sArr[x].Substring(0, 1).ToUpper() + sArr[x].Substring(1).ToLower()));
			return oSB.ToString();
		}
		public static string SplitOnUpperCase(this string sStr) {
			if (sStr.IsNullOrEmpty()) return sStr;
			StringBuilder sb = new StringBuilder();
			for (int x = 0; x < sStr.Length; x++) {
				if (char.IsUpper(sStr[x]) && x > 0) sb.Append(" ");
				sb.Append(sStr[x]);
			}
			return sb.ToString();
		}
		public static string SafeSplit(this string sStr, string sSeparator, int iIndex) {
			return parse.splitValue(sStr, sSeparator, iIndex);
		}

		//Index-safe Substring function with extended functionality
		public static string Substr(this string sStr, int iStartIndex) {
			return sStr.Substr(iStartIndex, convert.cStr(sStr).Length);
		}
		public static string Substr(this string sStr, int iStartIndex, int iLength) {
			if (sStr.IsNullOrEmpty()) return "";
			//if negative, count iStartIndex from end of string
			if (iStartIndex < 0) return sStr.Substring(sStr.Length + iStartIndex, Math.Min(iLength, -iStartIndex));

			//if length is positive, do a normal substring
			else if (iLength > -1) return sStr.Substring(iStartIndex, Math.Min(iLength, sStr.Length - iStartIndex));

			//if length is negative, get string starting from iStartIndex and end it iLength characters counting from end of original string
			else if (sStr.Length + iLength - iStartIndex > 0) return sStr.Substring(iStartIndex, Math.Min(sStr.Length + iLength - iStartIndex, sStr.Length - iStartIndex));
			return "";
		}
		public static string Right(this string sStr, int iNumChars) {
			if (sStr.Length <= iNumChars) return sStr;
			return sStr.Substring(sStr.Length - iNumChars);
		}
		public static string[] Str(this object[] oObj) {
			if (oObj == null || oObj.Length == 0) return new string[0];
			string[] Arr = new string[oObj.Length];
			for (int x = 0; x < oObj.Length; x++) Arr[x] = oObj[x].Str();
			return Arr;
		}
		public static bool[] Bln(this object[] oObj) {
			if (oObj == null || oObj.Length == 0) return new bool[0];
			bool[] Arr = new bool[oObj.Length];
			for (int x = 0; x < oObj.Length; x++) Arr[x] = oObj[x].Bln();
			return Arr;
		}
		public static int[] Int(this object[] oObj) {
			if (oObj == null || oObj.Length == 0) return new int[0];
			int[] Arr = new int[oObj.Length];
			for (int x = 0; x < oObj.Length; x++) Arr[x] = oObj[x].Int();
			return Arr;
		}
		public static long[] Lng(this object[] oObj) {
			if (oObj == null || oObj.Length == 0) return new long[0];
			long[] Arr = new long[oObj.Length];
			for (int x = 0; x < oObj.Length; x++) Arr[x] = oObj[x].Lng();
			return Arr;
		}
		public static double[] Dbl(this object[] oObj) {
			if (oObj == null || oObj.Length == 0) return new double[0];
			double[] Arr = new double[oObj.Length];
			for (int x = 0; x < oObj.Length; x++) Arr[x] = oObj[x].Dbl();
			return Arr;
		}
		public static DateTime[] Dte(this object[] oObj) {
			if (oObj == null || oObj.Length == 0) return new DateTime[0];
			DateTime[] Arr = new DateTime[oObj.Length];
			for (int x = 0; x < oObj.Length; x++) Arr[x] = oObj[x].Dte();
			return Arr;
		}
		public static DateTime[] Date(this object[] oObj) {
			if (oObj == null || oObj.Length == 0) return new DateTime[0];
			DateTime[] Arr = new DateTime[oObj.Length];
			for (int x = 0; x < oObj.Length; x++) Arr[x] = oObj[x].Date();
			return Arr;
		}
		public static Decimal[] Dec(this object[] oObj) {
			if (oObj == null || oObj.Length == 0) return new Decimal[0];
			Decimal[] Arr = new Decimal[oObj.Length];
			for (int x = 0; x < oObj.Length; x++) Arr[x] = oObj[x].Dec();
			return Arr;
		}
		public static bool IsNullOrEmpty(this object oObj) {
			return oObj == null || oObj.ToString() == "" || oObj == DBNull.Value;
		}
		public static bool IsNotNullOrEmpty(this object oObj) {
			return !oObj.IsNullOrEmpty();
		}

		public static string Str(this object oObj) {
			return convert.cStr(oObj);
		}
		public static bool Bln(this object oObj) {
			return convert.cBool(oObj);
		}
		public static bool Bln(this bool oObj) {
			return convert.cBool(oObj);
		}
		public static int Int(this object oObj) {
			return convert.cInt(oObj);
		}
		public static int Int(this double oObj) {
			return convert.cInt(oObj);
		}
		public static long Lng(this object oObj) {
			return convert.cLong(oObj);
		}
		public static double Dbl(this object oObj) {
			return convert.cDbl(oObj);
		}
		public static DateTime Dte(this object oObj) {
			return convert.cDate(oObj);
		}
		public static DateTimeOffset DteOffset(this object oObj) {
			return convert.cDateOffset(oObj);
		}
		public static short Short(this object oObj) {
			return convert.cShort(oObj);
		}
		public static Guid Guid(this object oObj) {
			return convert.cGuid(oObj);
		}


		/// <summary>
		/// Defaults to DateTime.MinValue if date is invalid
		/// </summary>
		/// <param name="oObj"></param>
		/// <returns></returns>
		public static DateTime Date(this object oObj) {
			return convert.cDate(oObj, DateTime.MinValue);
		}
		public static Decimal Dec(this object oObj) {
			return convert.cDec(oObj);
		}
		public static string DateStr(this DateTime date) {

			//If Date and is DateTime.Min, return ""
			if (date == DateTime.MinValue)
				return "";
			else
				return date.ToShortDateString();
		}

		public static string DateTimeStr(this DateTime date) {

			//If Date and is DateTime.Min, return ""
			if (date == DateTime.MinValue)
				return "";
			else
				return date.ToString();
		}
		public static string DateStr(this DateTime? date) {

			//If Date and is DateTime.Min, return ""
			if (date == null || date == DateTime.MinValue)
				return "";
			else
				return date.Value.ToShortDateString();
		}

		public static string DateTimeStr(this DateTime? date) {

			//If Date and is DateTime.Min, return ""
			if (date == null || date == DateTime.MinValue)
				return "";
			else
				return date.ToString();
		}
		public static List<XmlNode> ParentsAndSelf(this XmlNode node) {
			List<XmlNode> nodes = new List<XmlNode>() { node };
			while (node.ParentNode != null) {
				node = node.ParentNode;
				nodes.Add(node);
			}
			return nodes;
		}
		public static List<XmlNode> ToList(this XmlNodeList list) {
			List<XmlNode> nodes = new List<XmlNode>();
			if (list != null) {
				foreach (XmlNode node in list)
					nodes.Add(node);
			}
			return nodes;
		}
		public static XmlNode SafeAppendChild(this XmlNode self, XmlNode node) {
			if (node != null) self.AppendChild(node);
			return self;
		}
		//Get all branches in an XML-tree below another branch, represented by a node on this branch
		public static XmlNode BranchesBelow(this XmlNode self, XmlNode belowNode) {
			List<XmlNode> fullBranch = belowNode.ParentsAndSelf();
			if (!fullBranch.Contains(self) || self == belowNode) return null;
			XmlNode node = self.OwnerDocument.ImportNode(self, false);
			bool found = false;
			for (int x = 0; x < self.ChildNodes.Count; x++) {
				if (found) node.AppendChild(self.OwnerDocument.ImportNode(self.ChildNodes[x], true));
				else if (fullBranch.Contains(self.ChildNodes[x])) found = true;
			}
			if (!found) node.SafeAppendChild(self.LastChild.BranchesBelow(belowNode));
			else if (node.ChildNodes.Count == 0) return null;
			return node;
		}
		//Split an XmlDocument based on a list of nodes. 
		public static List<XmlDocument> SplitOnNodes(this XmlDocument doc, XmlNodeList splits) {
			List<XmlDocument> docs = new List<XmlDocument>();
			if (splits == null || splits.Count == 0) return new List<XmlDocument>() { doc };
			for (int x = 0; x <= splits.Count; x++) {

				XmlNode currentSplit = (x == splits.Count ? null : splits[x]);
				XmlNode prevSplit = (x == 0 ? null : splits[x - 1]);

				XmlDocument xDocCurrent = new XmlDocument();
				List<XmlNode> siblings = new List<XmlNode>();

				//if this is the last split, then we need to get document from split to end
				bool stepPrevious = (currentSplit != null);
				if (!stepPrevious) currentSplit = prevSplit;
				bool stopAddingNodes = false;

				while (currentSplit.ParentNode != null && currentSplit.ParentNode as XmlDocument == null) {
					if (stepPrevious) {
						if (!stopAddingNodes) {
							while (currentSplit.PreviousSibling != null && (prevSplit == null || !prevSplit.ParentsAndSelf().Contains(currentSplit.PreviousSibling))) {
								currentSplit = currentSplit.PreviousSibling;
								siblings.Insert(0, xDocCurrent.ImportNode(currentSplit, true));
							}

							//if current node has PreviousSibling, and PreviousSibling contains prevSplit, we need to get all child-nodes below prevSplit-branch
							if (currentSplit.PreviousSibling != null && prevSplit != null && prevSplit.ParentsAndSelf().Contains(currentSplit.PreviousSibling) && prevSplit != currentSplit.PreviousSibling) {
								//XmlNode lowerBranch = xDocCurrent.ImportNode(currentSplit.PreviousSibling, false);
								XmlNode node1 = currentSplit.PreviousSibling.BranchesBelow(prevSplit);
								if (node1 != null) siblings.Insert(0, xDocCurrent.ImportNode(node1, true));
							}
						}

					} else {
						while (currentSplit.NextSibling != null) {
							currentSplit = currentSplit.NextSibling;
							siblings.Add(xDocCurrent.ImportNode(currentSplit, true));
						}
					}
					currentSplit = currentSplit.ParentNode;
					if (prevSplit != null && prevSplit.ParentsAndSelf().Contains(currentSplit)) stopAddingNodes = true;
					XmlNode parent = xDocCurrent.ImportNode(currentSplit, false);
					siblings.ForEach(node => { parent.AppendChild(node); });
					siblings = new List<XmlNode>() { parent };
					if (currentSplit.ParentNode == null || currentSplit.ParentNode as XmlDocument != null) xDocCurrent.AppendChild(parent);
				}
				docs.Add(xDocCurrent);
			}
			return docs;
		}

		public static XmlDocument SetNodeValue(this XmlDocument XmlDoc, string XmlPath, object Value) {
			//Load Node
			XmlNode xmlNode = XmlDoc.SelectSingleNode(XmlPath);

			//Set Value
			xmlNode.InnerText = Value.Str();

			//Return Document
			return XmlDoc;
		}

		public static List<XmlNode> SelectNodes(this List<XmlNode> Nodes, string XmlPath) {
			List<XmlNode> returnList = new List<XmlNode>();
			foreach (XmlNode node in Nodes) {
				returnList.AddRange(node.SelectNodes(XmlPath).ToList());
			}
			return returnList;
		}


		public static XmlDocument ToXmlDocument(this XDocument xDocument) {
			var xmlDocument = new XmlDocument();
			using (var xmlReader = xDocument.CreateReader()) {
				xmlDocument.Load(xmlReader);
			}
			return xmlDocument;
		}

		public static XDocument ToXDocument(this XmlDocument xmlDocument) {
			using (var nodeReader = new XmlNodeReader(xmlDocument)) {
				nodeReader.MoveToContent();
				return XDocument.Load(nodeReader);
			}
		}

		public static bool IsLike(this string Value, string RegEx) {
			return Regex.IsMatch(Value, RegEx, RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.Multiline | RegexOptions.Singleline);
		}

		public static object GetMember(this System.Dynamic.DynamicObject Object, string Member) {
			var ObjectBinder = Microsoft.CSharp.RuntimeBinder.Binder.GetMember(CSharpBinderFlags.None, Member, Object.GetType(), new[] { CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, null) });
			var Callsite = CallSite<Func<CallSite, object, object>>.Create(ObjectBinder);
			return Callsite.Target(Callsite, Object);
		}
		public static Dictionary<string, object> GetMembers(this System.Dynamic.DynamicObject Object) {
			Dictionary<string, object> Members = new Dictionary<string, object>();
			List<string> MemberNames = Object.GetDynamicMemberNames().ToList();
			MemberNames.ForEach(x => {
				Members.Add(x, Object.GetMember(x));
			});
			return Members;
		}

		public static bool IsAllUppercase(this string Value) {
			return convert.isAllUppercase(Value);
		}
		public static bool IsAllLowercase(this string Value) {
			return convert.isAllLowercase(Value);
		}
		public static string ToProperCase(this string Value) {
			return convert.toProperCase(Value);
		}

		public static ObjectType SafeGet<ObjectType>(this ObjectType o) where ObjectType : new() {
			if (o == null) return new ObjectType();
			return o;
		}

		public static List<System.Net.Cookie> GetCookies(this System.Net.CookieContainer cookies) {
			List<System.Net.Cookie> cookieList = new List<System.Net.Cookie>();
			Hashtable table = (Hashtable)cookies.GetType().InvokeMember("m_domainTable", BindingFlags.NonPublic | BindingFlags.GetField | BindingFlags.Instance, null, cookies, new object[] { });

			foreach (var key in table.Keys) {
				foreach (Cookie cookie in cookies.GetCookies(new Uri(string.Format("http://{0}/", parse.stripLeadingCharacter(key,"."))))) {
					cookieList.Add(cookie);
				}
			}
			return cookieList;
		}

		public static string JoinStrings(this string First, string Separator, string Last) {
			if (!String.IsNullOrWhiteSpace(Last) && !First.Equals(Last, StringComparison.CurrentCultureIgnoreCase)) return String.Format("{0}{1}{2}", First, Separator, Last);

			else return First;

		}

		public static bool LockIf(this object SyncHandle, Func<bool> fnIf, Action fnOperation) {
			//Declare Return Variable
			var operationExecuted = false;

			//See if true
			if (fnIf()) {
				//Lock Sync Handle
				lock (SyncHandle) {
					//Check Condition Again
					if (fnIf()) {
						//Set Flag
						operationExecuted = true;

						//Execute Operation
						fnOperation();
					}

				}

			}

			//Return
			return operationExecuted;

		}

		public static DateTime AddWeekDays(this DateTime Date, int Days) {

			int daysLeft = Days;
			while (daysLeft > 0) {
				Date = Date.AddDays(1);
				daysLeft--;

				if ((int)Date.DayOfWeek == 0 || (int)Date.DayOfWeek == 6) Date = Date.AddDays(1);
				if ((int)Date.DayOfWeek == 0 || (int)Date.DayOfWeek == 6) Date = Date.AddDays(1);


			}

			return Date;
		}


        
		public static T SerializedClone<T>(T source) {
			DataContractSerializer serializer = new DataContractSerializer(typeof(T));
			using (MemoryStream ms = new MemoryStream()) {
				serializer.WriteObject(ms, source);
				ms.Seek(0, SeekOrigin.Begin);
				return (T)serializer.ReadObject(ms);
			}
		}
		private readonly static object _lock = new object();
		public static T ReflectionClone<T>(T original, List<string> propertyExcludeList) {
			try {
				Monitor.Enter(_lock);
				T copy = Activator.CreateInstance<T>();
				PropertyInfo[] piList = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
				foreach (PropertyInfo pi in piList) {
					if (!propertyExcludeList.Contains(pi.Name)) {
						if (pi.GetValue(copy, null) != pi.GetValue(original, null)) {
							pi.SetValue(copy, pi.GetValue(original, null), null);
						}
					}
				}
				return copy;
			} finally {
				Monitor.Exit(_lock);
			}
		}
	}
}
