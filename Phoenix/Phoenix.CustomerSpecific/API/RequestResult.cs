﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using jlib.functions;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.IO;

namespace Phoenix.CustomerSpecific.API {
	public class RequestResult {
		public RequestResult() {
			this.HttpResponseCode = 200;
		}
		public void SetError(Exception e) {
			RequestError = e;
			if (e as WebException != null) {
				System.Net.WebException ex = (System.Net.WebException)e;
				if (ex.Response != null) {
					HttpErrorMessage = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
					if (HttpErrorMessage.Str().StartsWith("{")) {
						try {
							HttpErrorJSON = jlib.functions.json.DynamicJson.Parse(HttpErrorMessage);
						} catch (Exception) { }
					}
				}
				HttpResponseCode = parse.inner_substring(ex.Message, null, "(", ")", null).Int();
			}
			if (e != null && HttpResponseCode == 200) HttpResponseCode = 0;
		}
		public bool Error {
			get {
				return HttpResponseCode != 200;
			}
		}
		public Exception RequestError { get; private set; }
		public string Data { get; set; }
		public int HttpResponseCode { get; set; }
		public string HttpErrorMessage { get; private set; }
		public string PhoenixCookieValue { get; set; }
		public jlib.functions.json.DynamicJson HttpErrorJSON { get; private set; }
		private dynamic m_oDataObject;
		public dynamic DataObject {
			get {
				if (Data.IsNullOrEmpty()) return null;
				if (m_oDataObject == null) m_oDataObject = jlib.functions.json.DynamicJson.Parse(Data);
				return m_oDataObject;
			}
		}
		public WebResponse WebResponse { get; set; }
	}
}