﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jlib.functions;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.IO;
using System.Xml.Linq;
using Metier.Phoenix.CustomerSpecific.Configuration;
using System.Configuration;
namespace Phoenix.CustomerSpecific.API
{
    public class APIFilterer {
		#region ***** ENUMS *****
			
		#endregion
	
		#region ***** CONSTRUCTORS *****

		#endregion

		#region ***** PROPERTIES *****				

        public string APIFilterEndpoint { get; set; }
		public string PhoenixCookieValue {get;set;}

		
		private ConfigurationNode _CustomerRules;
		public ConfigurationNode CustomerRules {
			get {
                if (_CustomerRules == null) _CustomerRules = ConfigurationHelper.CustomerRules;
                return _CustomerRules;
			}
		}
        private ConfigurationNode _GlobalRules;
        public ConfigurationNode GlobalRules
        {
            get
            {
                if (_GlobalRules == null) _GlobalRules = System.Configuration.ConfigurationManager.GetSection("globalConfiguration") as ConfigurationNode;
                return _GlobalRules;
            }
        }

        public string EndPointTarget {get;set;}
		public string EndPointMethod {get;set;}
		private ConfigurationNode _EndPointRule;
		public ConfigurationNode EndPointRule {
			get {
                if (_EndPointRule == null) _EndPointRule = GetEndPointRule(EndPointMethod, EndPointTarget);
                return _EndPointRule;
			}
		}
        //This method will look up the specific EndPoint for the given site (including site-specific overrides)        
        public ConfigurationNode GetEndPointRule(int endpointId)
        {
            var node = Helper.GetConfigurationNodes(GlobalRules, new List<string> { "endpoints" }).First().Children.FirstOrDefault(endpoint => endpoint.Attributes.SafeGetValue("id").Int() == endpointId );
            if (node == null)
                throw new HttpException(404, "EndpointId " + endpointId + " not found in configuration!");
            return ValidateAndLookupSiteSpecificOverride(node);
        }
        public ConfigurationNode GetEndPointRule(string method, string target)
        {
            var node = Helper.GetConfigurationNodes(GlobalRules, new List<string> { "endpoints" }).First().Children.FirstOrDefault(endpoint => MatchCommaSeparatedValues(endpoint.Attributes.SafeGetValue("method"), method) && MatchRegExValue(endpoint.Attributes.SafeGetValue("url"), target));
            if (node == null) throw new HttpException(404, "Endpoint " + method + " " + target + " not found in configuration!");
            return ValidateAndLookupSiteSpecificOverride(node);
        }
        private ConfigurationNode ValidateAndLookupSiteSpecificOverride(ConfigurationNode node) {
            if (node == null) throw new HttpException(404, "Endpoint not found!");
            var currentFilter = Helper.GetConfigurationNodes(ConfigurationHelper.SiteNode, new List<string> { "filters", "allow" }).Concat(Helper.GetConfigurationNodes(GlobalRules, new List<string> { "global", "filters", "allow" })).FirstOrDefault(allow => MatchCommaSeparatedValues(allow.Attributes.SafeGetValue("endpoint"), node.Attributes["id"]));

            if (currentFilter == null) throw new HttpException(403, "Endpoint " + EndPointMethod + " " + EndPointTarget + " not allowed for " + ConfigurationHelper.SiteUrl + ".");
            else if (currentFilter.Children.FirstOrDefault(child => child.Tag == "override") != null) node = currentFilter.Children.FirstOrDefault(child => child.Tag == "override");

            return node;
        }


        #endregion
        private static Dictionary<string, dynamic> _SessionUsers = new Dictionary<string, dynamic>();
        private const int CurrentUserEndpointId = 2;
        Helper ConfigurationHelper;
        public dynamic GetSessionUser()
        {
            if (PhoenixCookieValue.IsNullOrEmpty())
                return null;

            dynamic currentUser=null;
            lock (_SessionUsers) { 
                currentUser = _SessionUsers.SafeGetValue(PhoenixCookieValue);
                if (currentUser == null)
                {
                    //get endpoint 2 node and execute this. store result
                    var currentUserEndpoint = GetEndPointRule(CurrentUserEndpointId);
                    var result = RequestUrl(APIFilterEndpoint + "?endpoint=" + HttpUtility.UrlEncode(currentUserEndpoint.Attributes.SafeGetValue("url")), "", "GET", currentUserEndpoint.Attributes.SafeGetValue("token"), PhoenixCookieValue);
                    if (result.HttpResponseCode == 200)
                    {
                        currentUser = result.DataObject;
                        _SessionUsers.Add(PhoenixCookieValue, currentUser);
                    }
                }
            }
            return currentUser;
        }
        public bool IsSessionUserValid()
        {
            dynamic currentUser = GetSessionUser();
            if (currentUser == null) return false;

            //Validate currentUser to make sure currentUser is a valid response from endpoint==2
            var currentUserEndpoint = GetEndPointRule(CurrentUserEndpointId);
            var responseRulesToValidate = currentUserEndpoint.Children.Where(node => node.Tag == "response").SelectMany(child => child.Children);
            return (ValidateRules(false, ConfigurationHelper.SiteNode, responseRulesToValidate, EndPointTarget, ref currentUser));
        }
        #region ***** METHODS *****
        public void ProcessRequest(System.Web.HttpRequest request,System.Web.HttpResponse response) {
            ConfigurationHelper = new Helper(request);

            PhoenixCookieValue = jlib.net.HTTP.getCookieValue(request.Cookies, "PhoenixAuth", "");			
			APIFilterEndpoint = request.Url.AbsoluteUri.SafeSplit("?", 0);

			response.Clear();
			response.ContentType = "application/json";
			EndPointTarget= request["endpoint"];
			EndPointMethod = request.HttpMethod;
			
			if (EndPointTarget.IsNullOrEmpty()) throw new HttpException(404, "Endpoint must be specified");
						
			string payload = (request.ContentEncoding == null ? convert.cStreamToString(request.InputStream) : convert.cStreamToString(request.InputStream, request.ContentEncoding));
			dynamic payloadObject = jlib.functions.json.DynamicJson.Parse(convert.cStr(payload,"{}"));
            
            if (ConfigurationHelper.SiteNode == null) throw new HttpException(404, "Site " + ConfigurationHelper.SiteUrl + " not found in configuration!");

			var currentEndpoint= EndPointRule;									

			//Validate request to make sure parameters/payload is allowed
			var requestRulesToValidate = currentEndpoint.Children.Where(node=>node.Tag == "request").SelectMany(child => child.Children);
			if(!ValidateRules(true, ConfigurationHelper.SiteNode, requestRulesToValidate, EndPointTarget, ref payloadObject)) throw new HttpException(403, "Request Rules could not be validated");
			
            //Require a valid user to perform this operation
            if(currentEndpoint.Attributes.SafeGetValue("requireValidUser").IsNullOrEmpty() || currentEndpoint.Attributes.SafeGetValue("requireValidUser").Bln())
            {
                if (!IsSessionUserValid()) throw new HttpException(403,"Session user is empty or not valid.");
            }

            //Perform request
            Phoenix.CustomerSpecific.API.RequestResult result = RequestUrl(System.Configuration.ConfigurationManager.AppSettings["PhoenixApiUrl"] + EndPointTarget, payload, EndPointMethod, currentEndpoint.Attributes.SafeGetValue("token"), PhoenixCookieValue);

			if (result.HttpResponseCode < 200 || result.HttpResponseCode >= 300) {
				//throw new Exception("Call failed. Endpoint: " + EndPointTarget + " / Method: " + EndPointMethod + " / Code: " + result.HttpResponseCode + " / Message: " + result.HttpErrorMessage + " / Payload: " + payload);
				response.StatusCode = result.HttpResponseCode;
				response.StatusDescription = result.HttpErrorMessage;
				response.End();
			}

			//Update cookie if it has changed
			if (result.PhoenixCookieValue.IsNotNullOrEmpty() && result.PhoenixCookieValue != PhoenixCookieValue) {
				jlib.net.HTTP.DeleteCookie(request, response, "PhoenixAuth");
				HttpCookie cookie = new HttpCookie("PhoenixAuth", result.PhoenixCookieValue);
                cookie.Expires = DateTime.Now.AddHours(5);
                response.Cookies.Add(cookie);
				PhoenixCookieValue = result.PhoenixCookieValue;
			}

			//Validate response to make sure response is allowed
			var responseRulesToValidate = currentEndpoint.Children.Where(node => node.Tag == "response").SelectMany(child => child.Children);
			var data = result.DataObject;
			if(!ValidateRules(false, ConfigurationHelper.SiteNode, responseRulesToValidate, EndPointTarget, ref data)) throw new HttpException(403, "Response Rules could not be validated");
			
			//Validate request			
			response.Write(data);
			response.End();
			
		}

		private bool MatchCommaSeparatedValues(string list, string value) {
			return ("," + list + ",").Contains("," + value + ",");
		}
		private bool MatchRegExValue(string pattern, string value) {
			Regex regex = new Regex(pattern);
			return regex.Match(value).Success;
		}
		//Since the ConstantIds could be used internally in the application, we need to split out the Id-part of  the constants
		private string ParseConstantIds(List<ConfigurationNode> values) {
            string returnValue = "";
            values.ForEach(value => { returnValue += (returnValue.IsNullOrEmpty() ? "" : ",") + value.Attributes.SafeGetValue("id"); });
			return returnValue;
		}
		private List<XElement> GetJSONValues(dynamic jsonObject, string path) {
			XElement xElement = jsonObject._Xml ;
			IEnumerable<XElement> xElements=new List<XElement>{xElement};
			string []pathElements=path.Split('.');
			foreach(var pathElement in pathElements)
				xElements=xElements.Elements(pathElement);

            //In case this is wrapped as an array of item
            var items = xElements.Elements("item");
            if (items.Count() > 0) xElements = items;
            
            return xElements.ToList();
			/*
			//xElement.
			List<string> values = new List<string>();
			
			//xElement.Elements("Items").Elements("item").Elements("DistributorId")
			if (jsonObject == null) return values;
			string currentPath = path.SafeSplit(".", 0);
			string remainingPath = parse.stripLeadingCharacter(path.Substring(currentPath.Length), ".");

			List<dynamic> objectsToProcess = new List<dynamic>();
			if (jsonObject.GetType() == typeof(jlib.functions.json.DynamicJson) && jsonObject.IsArray) {
				foreach (var obj in jsonObject) objectsToProcess.Add(obj);
			} else {
				objectsToProcess.Add(jsonObject);
			}
			objectsToProcess.ForEach(objectToProcess => {
				if (currentPath.IsNullOrEmpty())
					values.Add(convert.cStr(objectToProcess));
				else
					values.AddRange(GetJSONValues(convert.SafeGetProperty(objectToProcess, currentPath), remainingPath));
			});

			return values;
			*/
		}

		
		private RequestResult RequestUrl(string jsonUrl, string jsonContent, string verb, string tokenKey, string phoenixCookieValue) {
            var logFile = ConfigurationManager.AppSettings["webservice.comm.log"];            
            Stopwatch timer = new Stopwatch();
			timer.Start();
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(jsonUrl);
			request.Method = verb;
			request.CookieContainer = new CookieContainer();
            bool tokenRequest = false;
			if (tokenKey.IsNotNullOrEmpty()) {
                if (System.Configuration.ConfigurationManager.AppSettings[tokenKey].IsNotNullOrEmpty())
                {
                    request.Headers.Add("PhoenixAuthToken", System.Configuration.ConfigurationManager.AppSettings[tokenKey]);
                    tokenRequest = true;
                }
			}
            if (phoenixCookieValue.IsNotNullOrEmpty() && (!tokenRequest || jsonUrl.StartsWith(APIFilterEndpoint))) {
				request.CookieContainer.Add(new System.Net.Cookie("PhoenixAuth", phoenixCookieValue, "/", new System.Uri(jsonUrl).Host));
			}
			System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
			RequestResult result = new RequestResult();
			if (verb == "POST" || verb == "PUT") request.ContentLength = 0;
			if (!jsonContent.IsNullOrEmpty()) {
				byte[] byteArray = encoding.GetBytes(jsonContent);
				request.ContentType = @"application/json";

				request.ContentLength = byteArray.Length;
				request.GetRequestStream().Write(byteArray, 0, byteArray.Length);
			}
            var logMessage = "\n\n" + verb + " " + jsonUrl + "\n" + (jsonContent.IsNullOrEmpty() ? "" : jsonContent + "\n\n");
            try {
				result.WebResponse = request.GetResponse();
				result.Data = new StreamReader(result.WebResponse.GetResponseStream(), Encoding.UTF8).ReadToEnd();
				if (request.CookieContainer != null) {
					request.CookieContainer.GetCookies().ForEach(cookie => {
						if (cookie.Name == "PhoenixAuth" && (result.PhoenixCookieValue.IsNullOrEmpty() || phoenixCookieValue!=cookie.Value)) result.PhoenixCookieValue = cookie.Value;
					});
                    
                }
			} catch (WebException ex) {
				result.SetError(ex);
                logMessage += ex.Message + "\n" + ex.StackTrace + "\n";
            }            
            if (logFile.IsNotNullOrEmpty())
            {
                System.IO.File.AppendAllText(logFile.Contains(":\\") ? logFile : System.Web.HttpContext.Current.Server.MapPath(logFile), logMessage + result.HttpResponseCode + ": " + convert.cStr(result.Data, result.HttpErrorMessage));
            }
			return result;
		}
		private bool ValidateRules(bool isRequest, ConfigurationNode currentSite, IEnumerable<ConfigurationNode> requestRulesToValidate, string endpointTarget,ref dynamic payloadObject) {
			foreach(var rule in requestRulesToValidate){
				//If this is a group, validate the required number of rules
				if (rule.Tag == "group") {
					int counter = rule.Attributes.SafeGetValue("require").Int();
					foreach (var node in rule.Children) {
                        try
                        {
                            if (node.Tag == "validate")
                            {
                                if (ValidateRule(isRequest, currentSite, node, endpointTarget, ref payloadObject)) counter--;
                            }
                            else if (node.Tag == "group")
                            {
                                if (ValidateRules(isRequest, currentSite, node.Children, endpointTarget, ref payloadObject)) counter--;
                            }
                        } catch (Exception) { }
                        if (counter == 0) break;
					}
					if (counter > 0) throw new HttpException(403, "Rule could not be validated");
				} else {
					if (!ValidateRule(isRequest, currentSite, rule, endpointTarget, ref payloadObject)) throw new HttpException(403, "Rule could not be validated");
				}
			}
			return true;
		}

		private bool ValidateRule(bool isRequest, ConfigurationNode site, ConfigurationNode rule, string endpointTarget, ref dynamic payloadObject) {

			//Is this request validated by the success of a different request?
			string endpointId = rule.Attributes.SafeGetValue("endpoint");
            if (endpointId.IsNotNullOrEmpty()) {
                var endpointRuleToCheck = GlobalRules.Children.Find(node => node.Tag == "endpoints").Children.FirstOrDefault(endpoint => endpoint.Attributes.SafeGetValue("id") == endpointId);
                //if Url is specified on the validate-node, us this. If not, grab Url from Endpoint definition
                string url = convert.cStr(rule.Attributes.SafeGetValue("url"), endpointRuleToCheck.Attributes.SafeGetValue("url"));
                //if (url.Contains("{")) {
                //	List<string> urlReplacementSegments = url.Split('{').ToList();
                //	for (int replacement = 0; replacement < urlReplacementSegments.Count; replacement++) {
                //		if (urlReplacementSegments[replacement].Contains("}")) {
                //			var key = urlReplacementSegments[replacement].SafeSplit("}", 0);
                //			var replacementValue = jlib.net.HTTP.decodeRawQueryString(endpointTarget, false).SafeGetValue(key); //extract replacement value from query string
                //			if (replacementValue.IsNullOrEmpty()) {
                //				XElement value = GetJSONValues(payloadObject, key).FirstOrDefault();
                //				if (value != null) replacementValue = value.Value; //if not found in query string, extract from payload
                //			}
                //			urlReplacementSegments[replacement] = replacementValue + urlReplacementSegments[replacement].Substring(key.Length + 1);
                //		}
                //	}					
                //	url = urlReplacementSegments.Join("");
                //} else if (url.Contains("(")) {//RegEx replacement
                //

                //Replace based on payload of posted data                
                var match = new Regex(@"\{[A-Za-z\.]*}").Match(url);
                while (match.Success)
                {
                    List<XElement> values = GetJSONValues(payloadObject, match.Value.Substring(1, match.Value.Length-2));
                    url = url.Replace(match.Value, values.Count == 0 ? "" : values[0].Value);
                    match = match.NextMatch();
                }                
                
                //Replace based on query string parameters
                string[]parts = new Regex(EndPointRule.Attributes.First(a => a.Key == "url").Value).Split(endpointTarget);
				url = string.Format(url, parts);                    
                
                var result = RequestUrl(APIFilterEndpoint + "?endpoint=" + HttpUtility.UrlEncode(url), "", "GET", endpointRuleToCheck.Attributes.SafeGetValue("token"), PhoenixCookieValue);
				if (result.HttpResponseCode != 200) throw new HttpException(result.HttpResponseCode, "Executing endpoint #" + endpointId + " returned an exception. Code: " + result.HttpResponseCode + " / Message: " + result.HttpErrorMessage);
				foreach (var responseRule in endpointRuleToCheck.Children.Where(c => c.Tag == "response").SelectMany(r => r.Children)) {
					var data = result.DataObject;
					if (!ValidateRule(false, site, responseRule, endpointTarget, ref data)) return false;
				}
				return true;
			}

			string allowedValue = rule.Attributes.SafeGetValue("value");
			bool matchAnyValue = rule.Attributes.SafeGetValue("match") == "any"; //Can be any or all (default is all)
			string path = rule.Attributes.SafeGetValue("path");				//Validate a JSON object value
			string parameter = rule.Attributes.SafeGetValue("parameter");	//Validate a URL parameter
			bool parameterValueRequiresValue = rule.Attributes.SafeGetValue("required").Bln();
			if (allowedValue == "#distributorIds") allowedValue = ParseConstantIds(Helper.GetConfigurationNodes(site, new List<string> {"authorization","distributors","distributor" }));
            if (allowedValue == "#customerIds") allowedValue = ParseConstantIds(Helper.GetConfigurationNodes(site, new List<string> { "authorization", "customers", "customer" }));
            if (allowedValue == "#userRoles") allowedValue = site.Attributes.SafeGetValue("userRoles");
			List<XElement> valuesToCheck = new List<XElement>();
			if (allowedValue.IsNotNullOrEmpty()) {
				//Validate URL parameter
				if (parameter.IsNotNullOrEmpty()) {
					string parameterValue = jlib.net.HTTP.decodeRawQueryString(endpointTarget.ToLower(), false).SafeGetValue(parameter.ToLower());

					//If parameter not found and parameter is a regex-format -- ie (\d*)
					if (parameterValue.IsNullOrEmpty() && parameter.ToLower().Contains("(")) {
						Regex regex = new Regex(parameter);
						Match match = regex.Match(endpointTarget);
						if (match.Success) parameterValue = match.Value;						
					}

					if (parameterValue.IsNotNullOrEmpty()) valuesToCheck.Add(new XElement("temp", parameterValue));
				}
				//Validate JSON object value(s)
				if (path.IsNotNullOrEmpty()) valuesToCheck = GetJSONValues(payloadObject, path);
			}
			if (parameterValueRequiresValue && valuesToCheck.Count == 0) throw new Exception("Parameter " + convert.cStr(path, parameter) + " requires a value.");			
			bool foundMatch = false;
			int removedCounter = 0;
			valuesToCheck.ForEach(valueToCheck => {
				if (valueToCheck.IsNullOrEmpty() && parameterValueRequiresValue) throw new Exception("Parameter " + convert.cStr(path, parameter) + " requires a value.");
				if (!MatchCommaSeparatedValues(allowedValue, valueToCheck.Value)) {
					if (!matchAnyValue) {
						if(isRequest) throw new Exception("Parameter " + convert.cStr(path, parameter) + " is not a valid value (" + valueToCheck.Value + ").");
                        //Only remove if it hasn't already been removed by previous validation rule
                        if (valueToCheck.Parent.Parent != null)
                        {
                            valueToCheck.Parent.Remove();
                            removedCounter++;
                        }
					}
				} else {
					foundMatch = true;
				}
			});
			if (removedCounter > 0 && payloadObject.IsDefined("TotalCount")) payloadObject.TotalCount = payloadObject.TotalCount - removedCounter;
			if ((matchAnyValue || valuesToCheck.Count()>0) && !foundMatch) throw new Exception("Parameter " + convert.cStr(path, parameter) + " is not a valid value (" + allowedValue + ").");
			return true;
		}		
		#endregion
	}
}