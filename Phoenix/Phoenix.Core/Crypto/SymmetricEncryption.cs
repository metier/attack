﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace Metier.Phoenix.Core.Crypto
{
    public static class SymmetricEncryption
    {
        private const string KeyBase64 = @"3HIjZ8kAtr0Xw0OQi/tbdwjgnIBkjK2Cin+bGDRuJJw=";
        private const string IvBase64 = @"uG0oV0UOMbPcj3gx1Qq3rA==";
        
        private static readonly byte[] Key;
        private static readonly byte[] Iv;

        static SymmetricEncryption()
        {
            Key = Convert.FromBase64String(KeyBase64);
            Iv = Convert.FromBase64String(IvBase64);
        }

        private static AesManaged GetEncryption(byte[] key, byte[] iv)
        {
            var aes = new AesManaged
            {
                KeySize = 256,
                Padding = PaddingMode.PKCS7,
                Mode = CipherMode.CBC,
                Key = key,
                IV = iv
            };
            return aes;
        }

        public static byte[] Encrypt(string plainText)
        {
            return Encrypt(plainText, Key, Iv);
        }

        public static string Decrypt(byte[] cipherText)
        {
            return Decrypt(cipherText, Key, Iv);
        }

        public static byte[] Encrypt(string plainText, byte[] key, byte[] iv)
        {
            if (plainText == null)
                throw new ArgumentNullException("plainText");
            if (key == null || key.Length <= 0)
                throw new ArgumentNullException("key");
            if (iv == null || iv.Length <= 0)
                throw new ArgumentNullException("iv");
            
            byte[] encrypted;
            using (var aes = GetEncryption(key, iv))
            {
                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            return encrypted;
        }

        public static string Decrypt(byte[] cipherText, byte[] key, byte[] iv)
        {
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (key == null || key.Length <= 0)
                throw new ArgumentNullException("key");
            if (iv == null || iv.Length <= 0)
                throw new ArgumentNullException("iv");

            string plaintext;
            using (var aes = GetEncryption(key, iv))
            {
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
                using (var msDecrypt = new MemoryStream(cipherText))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }
    }
}