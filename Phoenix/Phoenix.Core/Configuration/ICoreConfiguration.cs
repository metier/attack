﻿namespace Metier.Phoenix.Core.Configuration
{
    public interface ICoreConfiguration
    {
        string GetPasswordResetFormUrl(string source);
        string PhoenixApiUrl { get; }
        string ConnectionString { get; }
    }
}