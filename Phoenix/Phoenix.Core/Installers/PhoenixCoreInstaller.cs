﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Metier.Phoenix.Core.Installers
{
    public class PhoenixCoreInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Classes.FromThisAssembly().Pick().WithServiceDefaultInterfaces().Configure(s => s.IsFallback()).LifestyleTransient()
            );
        }
    }
}