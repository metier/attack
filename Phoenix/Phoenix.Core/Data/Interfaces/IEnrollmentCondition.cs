﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.Interfaces
{

    public interface IEnrollmentCondition
    {
         string Title { get; set; }
        int ProductId { get; set; }
        int ArticleTypeId { get; set; }
        int? LanguageId { get; set; }
        int? CustomerId { get; set; }
        bool IsStrictRequirement { get; set; }

        Product Product { get; set; }
        ArticleType ArticleType { get; set; }

        Language Language { get; set; }
        Customer Customer { get; set; }

    }
}
