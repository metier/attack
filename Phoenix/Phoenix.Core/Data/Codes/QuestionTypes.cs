﻿namespace Metier.Phoenix.Core.Data.Codes
{
    public static class QuestionTypes
    {
        public static string MultipleChoiceSingle = "S";
        public static string MultipleChoiceMultiple = "M";
        public static string TrueFalse = "TF";

        public static bool IsValidType(string type)
        {
            return type == MultipleChoiceMultiple ||
                   type == MultipleChoiceSingle ||
                   type == TrueFalse;
        }
    }
}