﻿namespace Metier.Phoenix.Core.Data.Codes
{
    public class LeadingTextIds
    {
        public const int CustomField1 = 11;
        public const int CustomField2 = 12;
        public const int EmployeeId = 13;
        public const int Department = 14;
        public const int Position = 15;
        public const int Supervisor = 16;
        public const int ConfirmationEmail = 17;
    }
}