﻿namespace Metier.Phoenix.Core.Data.Codes
{
    public static class ScormAttemptStatus
    {
        public const string Active = "A";
        public const string Failed = "F";
        public const string Complete = "C";
    }
}