﻿namespace Metier.Phoenix.Core.Data.Codes
{
    public class ParticipantResultCodes
    {
        public const string Passed = "PASSED";
        public const string Failed = "FAILED";

        public static string GetHumanFriendly(string status)
        {
            switch (status)
            {
                case Passed:
                    return "Passed";
                case Failed:
                    return "Failed";
                default:
                    return status;
            }
        }

        public static bool IsValidResult(string result)
        {
            return result == Passed ||
                   result == Failed;
        }
    }
}