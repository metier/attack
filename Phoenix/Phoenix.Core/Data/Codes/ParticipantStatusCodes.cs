﻿using System.Collections.Generic;

namespace Metier.Phoenix.Core.Data.Codes
{
    public static class ParticipantStatusCodes
    {
        public const string  Cancelled = "CANCELLED";
        public const string  Enrolled = "ENROLLED";
        public const string Completed = "COMPLETED";
        public const string NoShow = "NOSHOW";
        public const string Dispensation = "DISPENSATION";
        public const string OnHold = "ONHOLD";
        public const string InProgress = "INPROGRESS";
        public const string NotAttended = "NOTATTENDED";

        public static List<string> GetCodesAsSortedList()
        {
            return new List<string>{
                NoShow,
                NotAttended,
                Cancelled,
                OnHold,
                Enrolled,
                InProgress,
                Dispensation,
                Completed
            };
        }

        public static string GetHumanFriendly(string status)
        {
            switch (status)
            {
                case Cancelled:
                    return "Cancelled";
                case Enrolled:
                    return "Enrolled";
                case Completed:
                    return "Completed";
                case NoShow:
                    return "No-show";
                case Dispensation:
                    return "Dispensation";
                case OnHold:
                    return "On Hold";
                case InProgress:
                    return "In progress";
                case NotAttended:
                    return "Not attended";
                default:
                    return status;
            }
        }

        public static bool IsValidStatus(string status)
        {
            return status == Cancelled ||
                   status == Enrolled ||
                   status == Completed ||
                   status == NoShow ||
                   status == Dispensation ||
                   status == OnHold ||
                   status == InProgress ||
                   status == NotAttended;
        }
    }
}