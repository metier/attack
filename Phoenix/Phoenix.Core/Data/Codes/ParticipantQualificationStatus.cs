﻿namespace Metier.Phoenix.Core.Data.Codes
{
    public static class ParticipantQualificationStatus
    {
        public const string NotQualified = "NOTQUALIFIED";
        public const string PartiallyQualified = "PARTIALLYQUALIFIED";
        public const string Qualified = "QUALIFIED";
    }
}