﻿namespace Metier.Phoenix.Core.Data.Codes
{
    public static class ScormLessonStatus
    {
        public const string NotAttempted = "N";
        public const string Incomplete = "I";
        public const string Complete = "C";
        public const string Passed = "P";
        public const string Failed = "F";

        public static string GetDescription(string status)
        {
            switch (status)
            {
                case NotAttempted:
                    return "Not attempted";
                case Incomplete:
                    return "Incompleted";
                case Complete:
                    return "Completed";
                case Passed:
                    return "Passed";
                case Failed:
                    return "Failed";
                default:
                    return string.Empty;
            }
        }

        public static bool IsValidStatus(string status)
        {
            return status == NotAttempted ||
                   status == Incomplete ||
                   status == Complete ||
                   status == Passed ||
                   status == Failed;
        }
    }
}