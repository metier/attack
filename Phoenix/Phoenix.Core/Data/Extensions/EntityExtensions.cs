﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.Extensions
{
    public static class EntityExtensions
    {
        /// <summary>
        /// Configures the DbContext with correct state for a given entity.
        /// Updates all entities, unchanged or not (could be improved).
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="context"></param>
        public static void ConfigureDbContextForEntry(this IdentifiableEntity entity, DbContext context)
        {
            var entry = context.Entry(entity);
            entry.State = entity.Id == 0 ? EntityState.Added : EntityState.Modified;
        }

        /// <summary>
        /// Configures the DbContext with correct state for a given entity
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="context"></param>
        public static void ConfigureDbContextForEntryInt64(this IdentifiableEntityInt64 entity, DbContext context)
        {
            var entry = context.Entry(entity);
            entry.State = entity.Id == 0 ? EntityState.Added : EntityState.Modified;
        }

        /// <summary>
        /// Updates related entities and creates/deletes the related entities in the database
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TRelated"></typeparam>
        /// <param name="entity"></param>
        /// <param name="dbEntity"></param>
        /// <param name="relatedEntitiesProvider"></param>
        /// <param name="context"></param>
        public static void UpdateRelatedEntities<T, TRelated>(this T entity, 
                                                              T dbEntity, 
                                                              Func<T, List<TRelated>> relatedEntitiesProvider, 
                                                              DbContext context)
            where T : EntityBase
            where TRelated : EntityBase
        {
            var postedRelatedEntities = relatedEntitiesProvider(entity).ToList();
            var dbRelatedEntities = relatedEntitiesProvider(dbEntity).ToList();

            var addedRelatedEntities = postedRelatedEntities.Where(a => !dbRelatedEntities.Select(d => d.Id).Contains(a.Id));
            var deletedRelatedEntities = dbRelatedEntities.Where(a => !postedRelatedEntities.Select(n => n.Id).Contains(a.Id));

            foreach (var relatedEntity in addedRelatedEntities)
            {
                relatedEntitiesProvider(dbEntity).Add(relatedEntity);
            }
            foreach (var relatedEntity in deletedRelatedEntities)
            {
                context.Entry(relatedEntity).State = EntityState.Deleted;
                relatedEntitiesProvider(dbEntity).Remove(relatedEntity);
            }

            foreach (var dbRelatedEntity in relatedEntitiesProvider(dbEntity))
            {
                var postedRelatedEntity = relatedEntitiesProvider(entity).SingleOrDefault(s => s.Id != 0 && s.Id == dbRelatedEntity.Id);
                if (postedRelatedEntity != null)
                {
                    if (!postedRelatedEntity.RowVersion.SequenceEqual(dbRelatedEntity.RowVersion))
                    {
                        throw new DbUpdateConcurrencyException("There is a newer record of the entity in the database.");
                    }
                    context.Entry(dbRelatedEntity).CurrentValues.SetValues(postedRelatedEntity);
                }
            }
        }

        /// <summary>
        /// Updates related entities and creates/deletes the related entities in the database
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TRelated"></typeparam>
        /// <param name="entity"></param>
        /// <param name="dbEntity"></param>
        /// <param name="relatedEntitiesProvider"></param>
        /// <param name="context"></param>
        public static void UpdateRelatedEntitiesInt64<T, TRelated>(this T entity,
                                                              T dbEntity,
                                                              Func<T, List<TRelated>> relatedEntitiesProvider,
                                                              DbContext context)
            where T : EntityBaseInt64
            where TRelated : EntityBaseInt64
        {
            var postedRelatedEntities = relatedEntitiesProvider(entity).ToList();
            var dbRelatedEntities = relatedEntitiesProvider(dbEntity).ToList();

            var addedRelatedEntities = postedRelatedEntities.Where(a => !dbRelatedEntities.Select(d => d.Id).Contains(a.Id));
            var deletedRelatedEntities = dbRelatedEntities.Where(a => !postedRelatedEntities.Select(n => n.Id).Contains(a.Id));

            foreach (var relatedEntity in addedRelatedEntities)
            {
                relatedEntitiesProvider(dbEntity).Add(relatedEntity);
            }
            foreach (var relatedEntity in deletedRelatedEntities)
            {
                context.Entry(relatedEntity).State = EntityState.Deleted;
                relatedEntitiesProvider(dbEntity).Remove(relatedEntity);
            }

            foreach (var dbRelatedEntity in relatedEntitiesProvider(dbEntity))
            {
                var postedRelatedEntity = relatedEntitiesProvider(entity).SingleOrDefault(s => s.Id != 0 && s.Id == dbRelatedEntity.Id);
                if (postedRelatedEntity != null)
                {
                    if (!postedRelatedEntity.RowVersion.SequenceEqual(dbRelatedEntity.RowVersion))
                    {
                        throw new DbUpdateConcurrencyException("There is a newer record of the entity in the database.");
                    }
                    context.Entry(dbRelatedEntity).CurrentValues.SetValues(postedRelatedEntity);
                }
            }
        }

        /// <summary>
        /// Updates related entities and adds/removes the association to the related entities
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TRelated"></typeparam>
        /// <param name="entity"></param>
        /// <param name="dbEntity"></param>
        /// <param name="relatedEntitiesProvider"></param>
        /// <param name="contextDbSet"></param>
        public static void UpdateExistingRelatedEntities<T, TRelated>(this T entity,
                                                                  T dbEntity,
                                                                  Func<T, List<TRelated>> relatedEntitiesProvider,
                                                                  IQueryable<TRelated> contextDbSet)
            where T : IdentifiableEntity
            where TRelated : IdentifiableEntity
        {
            ReplaceEntitiesFromContext(entity, relatedEntitiesProvider, contextDbSet);

            var postedRelatedEntities = relatedEntitiesProvider(entity).ToList();
            var dbRelatedEntities = relatedEntitiesProvider(dbEntity).ToList();

            var addedRelatedEntities = postedRelatedEntities.Where(a => !dbRelatedEntities.Select(d => d.Id).Contains(a.Id));
            foreach (var relatedEntity in addedRelatedEntities)
            {
                relatedEntitiesProvider(dbEntity).Add(contextDbSet.SingleOrDefault(a => a.Id == relatedEntity.Id));
            }

            var deletedRelatedEntities = dbRelatedEntities.Where(a => !postedRelatedEntities.Select(p => p.Id).Contains(a.Id));
            foreach (var relatedEntity in deletedRelatedEntities)
            {
                relatedEntitiesProvider(dbEntity).Remove(relatedEntity);
            }
        }


        /// <summary>
        /// Updates related entities and adds/removes the association to the related entities
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TRelated"></typeparam>
        /// <param name="entity"></param>
        /// <param name="dbEntity"></param>
        /// <param name="relatedEntitiesProvider"></param>
        /// <param name="contextDbSet"></param>
        public static void UpdateExistingRelatedEntitiesInt64<T, TRelated>(this T entity,
                                                                  T dbEntity,
                                                                  Func<T, List<TRelated>> relatedEntitiesProvider,
                                                                  IQueryable<TRelated> contextDbSet)
            where T : IdentifiableEntityInt64
            where TRelated : IdentifiableEntityInt64
        {
            ReplaceEntitiesFromContextInt64(entity, relatedEntitiesProvider, contextDbSet);

            var postedRelatedEntities = relatedEntitiesProvider(entity).ToList();
            var dbRelatedEntities = relatedEntitiesProvider(dbEntity).ToList();

            var addedRelatedEntities = postedRelatedEntities.Where(a => !dbRelatedEntities.Select(d => d.Id).Contains(a.Id));
            foreach (var relatedEntity in addedRelatedEntities)
            {
                relatedEntitiesProvider(dbEntity).Add(contextDbSet.SingleOrDefault(a => a.Id == relatedEntity.Id));
            }

            var deletedRelatedEntities = dbRelatedEntities.Where(a => !postedRelatedEntities.Select(p => p.Id).Contains(a.Id));
            foreach (var relatedEntity in deletedRelatedEntities)
            {
                relatedEntitiesProvider(dbEntity).Remove(relatedEntity);
            }
        }

        /// <summary>
        /// Replaces all related entities with fresh entities from DbContext
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TRelated"></typeparam>
        /// <param name="entity"></param>
        /// <param name="relatedEntitiesProvider"></param>
        /// <param name="contextDbSet"></param>
        public static void ReplaceEntitiesFromContext<T, TRelated>(this T entity,
                                                                    Func<T, List<TRelated>> relatedEntitiesProvider,
                                                                    IQueryable<TRelated> contextDbSet)
            where T : IdentifiableEntity
            where TRelated : IdentifiableEntity
        {
            var detachedEntities = relatedEntitiesProvider(entity).ToList();
            relatedEntitiesProvider(entity).Clear();
            foreach (var detachedEntity in detachedEntities)
            {
                relatedEntitiesProvider(entity).Add(contextDbSet.SingleOrDefault(a => a.Id == detachedEntity.Id));
            }
        }

        /// <summary>
        /// Replaces all related entities with fresh entities from DbContext
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TRelated"></typeparam>
        /// <param name="entity"></param>
        /// <param name="relatedEntitiesProvider"></param>
        /// <param name="contextDbSet"></param>
        public static void ReplaceEntitiesFromContextInt64<T, TRelated>(this T entity,
                                                                    Func<T, List<TRelated>> relatedEntitiesProvider,
                                                                    IQueryable<TRelated> contextDbSet)
            where T : IdentifiableEntityInt64
            where TRelated : IdentifiableEntityInt64
        {
            var detachedEntities = relatedEntitiesProvider(entity).ToList();
            relatedEntitiesProvider(entity).Clear();
            foreach (var detachedEntity in detachedEntities)
            {
                relatedEntitiesProvider(entity).Add(contextDbSet.SingleOrDefault(a => a.Id == detachedEntity.Id));
            }
        }
    }
}