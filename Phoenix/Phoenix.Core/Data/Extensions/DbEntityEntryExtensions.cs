﻿using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;

namespace Metier.Phoenix.Core.Data.Extensions
{
    public static class DbEntityEntryExtensions
    {
        public static bool HasChanges<T>(this DbEntityEntry<T> item) where T : class
        {
            var dbPropertyValues = item.GetDatabaseValues();
            var properties = item.Entity.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);

            var valuePropertiesHasChanges = properties.Where(propertyInfo => propertyInfo.PropertyType.IsValueType || propertyInfo.PropertyType == typeof(String))
                                                 .Select(propertyInfo => item.Property(propertyInfo.Name))
                                                 .Any(dbPropertyEntry => PropertyHasChanged(dbPropertyEntry, dbPropertyValues));

            return valuePropertiesHasChanges;
        }

        private static bool PropertyHasChanged(DbPropertyEntry propertyEntry, DbPropertyValues dbPropertyValues)
        {
            var propertyValue = dbPropertyValues[propertyEntry.Name];
            if (propertyValue == null && propertyEntry.CurrentValue == null)
                return false;

            if (propertyValue != null && propertyEntry.CurrentValue == null)
                return true;

            if (propertyValue == null && propertyEntry.CurrentValue != null)
                return true;

            return !propertyValue.Equals(propertyEntry.CurrentValue);
        }
    }
}