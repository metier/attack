﻿using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Extensions;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Data.ViewEntities.Reports;
using Metier.Phoenix.Core.Utilities.Attributes;
using Metier.Phoenix.Core.Data.ViewEntities.Reports.CustomerSpecific.Jernbaneverket;

namespace Metier.Phoenix.Core.Data
{
    public class MetierLmsContext : DbContext
    {
        static MetierLmsContext()
        {
            Database.SetInitializer<MetierLmsContext>(null);
        }

        // For unit testing
        public MetierLmsContext()
        {
        }

        public MetierLmsContext(string connectionString) : base(connectionString)
        {
            // Enabled DateTimeKind-attribute which can be used to force date times to be serialized as UTC
            Database.CommandTimeout = 60;
            ((IObjectContextAdapter)this).ObjectContext.ObjectMaterialized += OnObjectMaterialized;
            ((IObjectContextAdapter)this).ObjectContext.SavingChanges += OnSavingChanges;
        }

        private static void OnObjectMaterialized(object sender, ObjectMaterializedEventArgs e)
        {
            DateTimeKindAttribute.Apply(e.Entity);
            EncryptedColumnAttribute.Decrypt(e.Entity);
        }

        private void OnSavingChanges(object sender, EventArgs e)
        {
            var objectContext = sender as ObjectContext;
            if (objectContext != null)
            {
                var entriesWithChanges = objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Modified);
                foreach (var entry in entriesWithChanges)
                {
                    EncryptedColumnAttribute.Encrypt(entry.Entity);
                    objectContext.DetectChanges();
                }
            }

            SetCreatedAndModifiedValuesOnEntities();
        }

        private void SetCreatedAndModifiedValuesOnEntities()
        {
            var timeTrackingEntities = ChangeTracker.Entries<TimeTrackingEntityBase>().ToList();

            foreach (var item in timeTrackingEntities.Where(t => t.State == EntityState.Added))
            {
                item.Entity.Created = DateTime.UtcNow;
                item.Entity.LastModified = DateTime.UtcNow;
            }
            foreach (var item in timeTrackingEntities.Where(t => t.State == EntityState.Modified))
            {
                if (item.HasChanges())
                {
                    item.Entity.LastModified = DateTime.UtcNow;
                }
                else
                {
                    item.State = EntityState.Unchanged;
                }
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            BuildCustomers(modelBuilder);
            BuildArticles(modelBuilder);
            BuildResources(modelBuilder);
            BuildActivities(modelBuilder);
            BuildEnrollmentConditions(modelBuilder);
            BuildActivitySets(modelBuilder);
            BuildOrders(modelBuilder);
            BuildReports(modelBuilder);
            BuildMailEntries(modelBuilder);
            BuildParticipants(modelBuilder);
            BuildUserCompetences(modelBuilder);
            BuildQuestions(modelBuilder);
            BuildExams(modelBuilder);
            BuildAnswers(modelBuilder);

            //modelBuilder.ComplexType<ActivitySetWithExtraData>();
            //modelBuilder.Conventions.Add(new FunctionsConvention<MetierLmsContext>("dbo"));
            
        }


        //[DbFunctionDetails(ResultColumnName = "AvailableSpots")]
        //[DbFunction("MetierLmsContext", "GetActivitySetExtraData")]
        //public IQueryable<ActivitySetExtraData> GetActivitySetExtraData(int activitySetId)
        //{
        //    var activitySetIdParameter = new ObjectParameter("activitySetId", typeof(int));

        //    //return ((IObjectContextAdapter)this).ObjectContext
        //    //    .CreateQuery<Customer>(
        //    //        string.Format("[{0}].{1}", GetType().Name,
        //    //            "[CustomersByZipCode](@ZipCode)"), zipCodeParameter);

        //    return ((IObjectContextAdapter)this).ObjectContext
        //    .CreateQuery<ActivitySetExtraData>(string.Format("[{0}].{1}", GetType().Name, "[GetActivitySetExtraData](@activitySetId)", activitySetIdParameter));
        //}

        //[DbFunctionDetails(ResultColumnName = "AvailableSpots")]
        //[DbFunction("MetierLmsContext", "GetActivitySetWithExtraData")]
        //public IQueryable<ActivitySetWithExtraData> GetActivitySetWithExtraData()
        //{
            
        //    return ((IObjectContextAdapter)this).ObjectContext
        //    .CreateQuery<ActivitySetWithExtraData>(string.Format("[{0}].{1}", GetType().Name, "[GetActivitySetWithExtraData]()"));
        //}

        private void BuildExams(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Exam>()
                .HasMany(e => e.Questions)
                .WithMany()
                        .Map(mc =>
                        {
                            mc.ToTable("Exams_Questions");
                            mc.MapLeftKey("ExamId");
                            mc.MapRightKey("QuestionId");
                        });
            modelBuilder.Entity<Exam>()
                .HasMany(e => e.Answers)
                .WithRequired()
                .HasForeignKey(a => a.ExamId);
        }

        private void BuildAnswers(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Answer>()
                .HasMany(e => e.OptionsAnswers)
                .WithMany()
                        .Map(mc =>
                        {
                            mc.ToTable("Answers_QuestionOptions");
                            mc.MapLeftKey("AnswerId");
                            mc.MapRightKey("QuestionOptionId");
                        });
        }

        private void BuildMailEntries(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Mail>()
                .HasMany(ci => ci.MailStatuses)
                .WithRequired()
                .HasForeignKey(m => m.MailId);

            modelBuilder.Entity<SerializedMailMessage>()
                .HasRequired(ci => ci.Mail)
                .WithOptional(c => c.SerializedMailMessage)
                .Map(m => m.MapKey("MailId"));
        }

        private void BuildReports(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ActivityParticipant>().HasKey(ap => new { ap.ActivityId, ap.ParticipantId, ap.UserId});
            modelBuilder.Entity<ProgressStatus>().HasKey(e => new { e.CustomerId, e.ProductId, e.ArticleId, e.UserId});
            modelBuilder.Entity<ProductCourseStatus>().HasKey(e => new { e.ProductId, e.UserId });
            modelBuilder.Entity<PotentialParticipation>().HasKey(e => new { e.CustomerId, e.ProductId, e.ProductPath, e.UserId });
            modelBuilder.Entity<AllParticipation>().HasKey(e => new { e.ProductId, e.UserName });
            modelBuilder.Entity<PrepaidClassroom>().HasKey(e => new { e.DistributorId, e.AccountNumber, e.InvoiceNumber});
        }

        private void BuildOrders(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>()
                .HasMany(o => o.ParticipantOrderLines)
                .WithRequired(p => p.Order)
                .Map(m => m.MapKey("OrderId"));

            modelBuilder.Entity<Order>()
                .HasMany(o => o.ActivityOrderLines)
                .WithRequired(a => a.Order)
                .Map(m => m.MapKey("OrderId"));

        }

        private void BuildArticles(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Article>()
                        .HasMany(a => a.Attachments)
                        .WithMany()
                        .Map(mc =>
                            {
                                mc.ToTable("Articles_Attachments");
                                mc.MapLeftKey("ArticleId");
                                mc.MapRightKey("AttachmentId");
                            }
                        );

            modelBuilder.Entity<Article>()
                        .HasMany(a => a.ArticleExamCourses)
                        .WithRequired()
                        .HasForeignKey(c => c.ArticleId);
        }

        private void BuildActivitySets(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ActivitySet>()
                        .HasMany(a => a.Activities)
                        .WithMany()
                        .Map(mc =>
                            {
                                mc.ToTable("ActivitySets_Activities");
                                mc.MapLeftKey("ActivitySetId");
                                mc.MapRightKey("ActivityId");
                            }
                        );

            //modelBuilder.Entity<ActivitySet>()
            //    .HasOptional(c => c.ActivitySetExtraData)
            //    .WithMany()
            //    .HasForeignKey(c => c.Id);

            //modelBuilder.Entity<ActivitySet>()
            //    .HasRequired(ci => ci.ActivitySetExtraData)
            //    .WithMany()
            //    .Map(mc => { 
            //    mc.map
            //    });

            modelBuilder.Entity<ActivitySetShare>().HasKey(ap => new { ap.ActivitySetId, ap.CustomerId });

            //modelBuilder.Entity<ActivitySet>()
            //.HasOptional(b => b.ActivitySetExtraData)
            //.WithMany();
            

            //modelBuilder.Entity<ActivitySetExtraData>().HasKey(ap => new { ap.Id });

            //modelBuilder.Entity<ActivitySet>()

            //            .HasRequired(a => a.ActivitySetExtraData)
            //            .WithOptional()
            //            .Map(mc => { 
            //            mc.
            //            });
            //.WithRequiredPrincipal(c => c.Id);
            //.WithOptional(c => c.Id);
            //.WithRequiredDependent( c => c.Id );


            //.WithRequired()
            //.HasForeignKey(c => c.);
        }
        
        public System.Collections.Generic.IEnumerable<ActivitySet> LoadActivitySetAvailableSpots(IQueryable<ActivitySet> activitySets)
        {
            var activitySetList = activitySets.ToList();
            if (activitySetList.Count == 0) return activitySetList;

            var result = this.Database.SqlQuery(typeof(ActivitySetAvailableSpots), $@"

WITH AvailableSpotsCTE AS
 (
     SELECT MaxParticipants, (SELECT COUNT(*) FROM Participants p WHERE p.IsDeleted = 0 AND p.ActivityId = a.Id) AS CurrentParticipants, aa.ActivitySetId from Activities a, ActivitySets_Activities aa
         where aa.ActivityId = a.Id AND ISNULL(MaxParticipants,0) > 0
)
SELECT ActivitySetId, MIN(MaxParticipants - CurrentParticipants) AS AvailableSpots FROM AvailableSpotsCTE where ActivitySetId in ({string.Join(",", activitySetList.ToList().Select(a1 => a1.Id))})

group by ActivitySetId

");

            result.ToListAsync().Result.ForEach(o =>
            {
                var count = o as ActivitySetAvailableSpots;                
                activitySetList.First(a1 => a1.Id == count.ActivitySetId).AvailableSpots = count.AvailableSpots;
            });

            return activitySetList;

        }

        private void BuildActivities(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Activity>()
                .HasMany(a => a.ActivityOrderLines)
                .WithRequired(l => l.Activity)
                .Map(m => m.MapKey("ActivityId"));

            modelBuilder.Entity<Activity>()
                        .HasMany(a => a.Attachments)
                        .WithMany()
                        .Map(mc =>
                            {
                                mc.ToTable("Activities_Attachments");
                                mc.MapLeftKey("ActivityId");
                                mc.MapRightKey("AttachmentId");
                            }
                        );

            modelBuilder.Entity<Activity>()
                        .HasMany(a => a.Resources)
                        .WithMany()
                        .Map(mc =>
                        {
                            mc.ToTable("Activities_Resources");
                            mc.MapLeftKey("ActivityId");
                            mc.MapRightKey("ResourceId");
                        }
                        );

            modelBuilder.Entity<Activity>()
                        .HasMany(a => a.ActivityExamCourses)
                        .WithRequired()
                        .HasForeignKey(c => c.ActivityId);

            modelBuilder.Entity<Activity>()
                        .HasOptional(a => a.ActivityAutomailText);

            modelBuilder.Entity<Activity>()
                        .HasMany(a => a.EnrollmentConditions)
                        .WithRequired()
                        .HasForeignKey(ec => ec.ActivityId);

        }

        private void BuildEnrollmentConditions(DbModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<EnrollmentCondition>()
                .HasRequired(ec => ec.Product)
                .WithMany()
                .HasForeignKey(ec => ec.ProductId);

            modelBuilder.Entity<EnrollmentCondition>()
                .HasRequired(e => e.ArticleType)
                .WithMany()
                .HasForeignKey(e => e.ArticleTypeId);

            modelBuilder.Entity<EnrollmentCondition>()
                .HasOptional(e => e.Language)
                .WithMany()
                .HasForeignKey(e => e.LanguageId);

            modelBuilder.Entity<EnrollmentCondition>()
                .HasOptional(e => e.Customer)
                .WithMany()
                .HasForeignKey(e => e.CustomerId);

            modelBuilder
                .Entity<CustomerArticleEnrollmentCondition>()
                .HasRequired(ec => ec.Product)
                .WithMany()
                .HasForeignKey(ec => ec.ProductId);

            modelBuilder.Entity<CustomerArticleEnrollmentCondition>()
                .HasRequired(e => e.ArticleType)
                .WithMany()
                .HasForeignKey(e => e.ArticleTypeId);

            modelBuilder.Entity<CustomerArticleEnrollmentCondition>()
                .HasOptional(e => e.Language)
                .WithMany()
                .HasForeignKey(e => e.LanguageId);

            modelBuilder.Entity<CustomerArticleEnrollmentCondition>()
                .HasOptional(e => e.Customer)
                .WithMany()
                .HasForeignKey(e => e.CustomerId);
        }

        private void BuildQuestions(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Question>()
                        .HasMany(a => a.Products)
                        .WithMany()
                        .Map(mc =>
                        {
                            mc.ToTable("Questions_Products");
                            mc.MapLeftKey("QuestionId");
                            mc.MapRightKey("ProductId");
                        }
                        );

            modelBuilder.Entity<Question>()
                        .HasMany(a => a.Tags)
                        .WithMany()
                        .Map(mc =>
                        {
                            mc.ToTable("Questions_Tags");
                            mc.MapLeftKey("QuestionId");
                            mc.MapRightKey("TagId");
                        }
                        );

            modelBuilder.Entity<Question>()
                        .HasMany(a => a.Rcos)
                        .WithMany()
                        .Map(mc =>
                        {
                            mc.ToTable("Questions_Rcos");
                            mc.MapLeftKey("QuestionId");
                            mc.MapRightKey("RcoId");
                        }
                        );
        }

        private void BuildResources(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Resource>()
                        .HasMany(a => a.Attachments)
                        .WithMany()
                        .Map(mc =>
                            {
                                mc.ToTable("Resources_Attachments");
                                mc.MapLeftKey("ResourceId");
                                mc.MapRightKey("AttachmentId");
                            }
                        );
        }

        private void BuildParticipants(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Participant>()
                .HasMany(p => p.ParticipantOrderLines)
                .WithRequired(l => l.Participant)
                .Map(m => m.MapKey("ParticipantId"));

            modelBuilder.Entity<Participant>()
                        .HasMany(a => a.Attachments)
                        .WithMany()
                        .Map(mc =>
                            {
                                mc.ToTable("Participants_Attachments");
                                mc.MapLeftKey("ParticipantId");
                                mc.MapRightKey("AttachmentId");
                            }
                        );
        }

        private void BuildUserCompetences(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserCompetence>()
            .HasMany(a => a.Attachments)
            .WithMany()
            .Map(mc =>
            {
                mc.ToTable("UserCompetences_Attachments");
                mc.MapLeftKey("UserCompetenceId");
                mc.MapRightKey("AttachmentId");
            }
            );
        }

        private static void BuildCustomers(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerAddress>()
                .HasRequired(ca => ca.Customer)
                .WithMany(c => c.Addresses)
                .Map(m => m.MapKey("CustomerId"));

            modelBuilder.Entity<MetierContactPerson>()
                .HasRequired(ma => ma.Customer)
                .WithMany(c => c.MetierContactPersons)
                .Map(m => m.MapKey("CustomerId"));

            modelBuilder.Entity<CustomerContactPerson>()
                .HasRequired(ci => ci.Customer)
                .WithMany(c => c.CustomerContactPersons)
                .Map(m => m.MapKey("CustomerId"));

            modelBuilder.Entity<Customer>()
                .HasOptional(c => c.ParticipantInfoDefinition);

            modelBuilder.Entity<Customer>()
                .HasOptional(c => c.CustomerInvoicing);

            modelBuilder.Entity<Customer>()
                .HasOptional(c => c.AutomailDefinition);

            modelBuilder.Entity<CustomLeadingText>()
                .HasRequired(clt => clt.Customer)
                .WithMany(c => c.CustomLeadingTexts)
                .HasForeignKey(c => c.CustomerId);

            modelBuilder.Entity<CustomLeadingText>()
                .HasRequired(clt => clt.LeadingText)
                .WithMany(l => l.CustomLeadingTexts)
                .HasForeignKey(clt => clt.LeadingTextId);


            modelBuilder.Entity<ParticipantInfoDefinition>()
                .HasMany(p => p.AcceptedEmailAddresses)
                .WithRequired()
                .Map(m => m.MapKey("ParticipantInfoDefinitionId"));

			modelBuilder.Entity<Customer>()
				.HasOptional(c => c.DiplomaLogoAttachment);

			modelBuilder.Entity<Customer>()
				.HasOptional(c => c.DiplomaSignatureAttachment);
		}

        // Tables
        public virtual IDbSet<Customer> Customers { get; set; }
        public virtual IDbSet<ParticipantInfoDefinition> ParticipantInfoDefinitions { get; set; }
        public virtual IDbSet<Language> Languages { get; set; }
        public virtual IDbSet<LeadingText> LeadingTextes { get; set; }
        public virtual IDbSet<CustomLeadingText> CustomLeadingTexts { get; set; }
        public virtual IDbSet<ProductDescription> ProductDescriptions { get; set; }
        public virtual IDbSet<Product> Products { get; set; }
        public virtual IDbSet<ProductCategory> ProductCategories { get; set; }
        public virtual IDbSet<ProductType> ProductTypes { get; set; }
        public virtual IDbSet<CodeType> CodeTypes { get; set; }
        public virtual IDbSet<Code> Codes { get; set; }
        public virtual IDbSet<Article> Articles { get; set; }
        public virtual IDbSet<ArticleType> ArticleTypes { get; set; }
        public virtual IDbSet<ElearningArticle> ElearningArticles { get; set; }
        public virtual IDbSet<Attachment> Attachments { get; set; }
        public virtual IDbSet<User> Users { get; set; }
        public virtual IDbSet<HelpText> HelpTexts { get; set; }
        public virtual IDbSet<CourseProgram> CoursePrograms { get; set; }
        public virtual IDbSet<CustomerArticle> CustomerArticles { get; set; }
        public virtual IDbSet<Distributor> Distributors { get; set; }
        public virtual IDbSet<Resource> Resources { get; set; }
        public virtual IDbSet<Comment> Comments { get; set; } 
        public virtual IDbSet<Activity> Activities { get; set; }
        public virtual IDbSet<EnrollmentCondition> EnrollmentConditions { get; set; }
        public virtual IDbSet<CustomerArticleEnrollmentCondition> CustomerArticleEnrollmentConditions { get; set; }
        public virtual IDbSet<ActivityPeriod> ActivityPeriods { get; set; }
        public virtual IDbSet<ActivitySet> ActivitySets { get; set; }
        //public virtual IDbSet<ActivitySetWithExtraData> ActivitySetsWithExtraData { get; set; }
        //public virtual IDbSet<ActivitySetExtraData> ActivitySetsExtraData { get; set; }
        public virtual IDbSet<ActivitySetShare> ActivitySetShares { get; set; }
        public virtual IDbSet<Enrollment> Enrollments { get; set; }
        public virtual IDbSet<Participant> Participants { get; set; }
        public virtual IDbSet<Order> Orders { get; set; }
        public virtual IDbSet<ParticipantOrderLine> ParticipantOrderLines { get; set; }
        public virtual IDbSet<ActivityOrderLine> ActivityOrderLines { get; set; }
        public virtual IDbSet<AuthenticationToken> AuthenticationTokens { get; set; }
        public virtual IDbSet<ScormAttempt> ScormAttempts { get; set; }
        public virtual IDbSet<ScormPerformance> ScormPerformances { get; set; }
        public virtual IDbSet<PasswordResetRequest> PasswordResetRequests { get; set; }
        public virtual IDbSet<Mail> Mails { get; set; }
        public virtual IDbSet<SerializedMailMessage> SerializedMailMessages { get; set; }
        public virtual IDbSet<Rco> Rcos { get; set; }
        public virtual IDbSet<File> Files { get; set; }
        public virtual IDbSet<UserCompetence> UserCompetences { get; set; }
        public virtual IDbSet<UserDiploma> UserDiplomas { get; set; }
        public virtual IDbSet<Tag> Tags { get; set; } 
        public virtual IDbSet<Question> Questions { get; set; }
        public virtual IDbSet<QuestionOption> QuestionOptions { get; set; } 
        public virtual IDbSet<Exam> Exams { get; set; } 
        public virtual IDbSet<Answer> Answers { get; set; }
        public virtual IDbSet<ActivityExamCourse> ActivityExamCourses { get; set; }
        public virtual IDbSet<ArticleExamCourse> ArticleExamCourses { get; set; }
        public virtual IDbSet<GradeScale> GradeScales { get; set; } 
        public virtual IDbSet<SharedFile> SharedFiles { get; set; }
        public virtual IDbSet<ChangeLogEntry> ChangeLog { get; set; } 
        public virtual IDbSet<CustomInformation> CustomInformationSet { get; set; } 
        public virtual IDbSet<VersionLogItem> VersionLogItems { get; set; } 
        public virtual IDbSet<MetierContactPerson> MetierContactPersons { get; set; } 
        
        // Views
        public virtual IDbSet<ImportedMembershipUser> ImportedUsers { get; set; }
        public virtual IDbSet<SearchableArticle> SearchableArticles { get; set; }
        public virtual IDbSet<SearchableCustomer> SearchableCustomers { get; set; } 
        public virtual IDbSet<SearchableActivity> SearchableActivities { get; set; }
        public virtual IDbSet<SearchableActivityWithActivitySet> SearchableActivitiesWithActivitySets { get; set; }
        public virtual IDbSet<SearchableUser> SearchableUsers { get; set; }
        public virtual IDbSet<SearchableOrder> SearchableOrders { get; set; }
        public virtual IDbSet<SearchableParticipant> SearchableParticipants { get; set; }
        public virtual IDbSet<SearchableCustomerArticle> SearchableCustomerArticles { get; set; }
        public virtual IDbSet<SearchableRco> SearchableCourseRcos { get; set; }
        public virtual IDbSet<SearchableMail> SearchableMails { get; set; }
        public virtual IDbSet<ProposedOrderLine> ProposedOrderLines { get; set; }
        public virtual IDbSet<ProgressStatus> ProgressStatuses { get; set; }
        public virtual IDbSet<ActivityParticipant> ActivityParticipants { get; set; }
        public virtual IDbSet<ProductCourseStatus> ProductCourseStatuses { get; set; }
        public virtual IDbSet<ParticipationStatusPrActivity> ParticipationsStatusPrActivity { get; set; }
        public virtual IDbSet<PotentialParticipation> PotentialParticipations { get; set; } 
        public virtual IDbSet<AllParticipation> AllParticipations { get; set; }
        public virtual IDbSet<HubspotParticipant> HubspotParticipants { get; set; }
        public virtual IDbSet<ParticipationPrActivity> ParticipationsPrActivity { get; set; }
        public virtual IDbSet<OrdersForInvoicing> OrdersForInvoicings { get; set; }
        public virtual IDbSet<SearchableQuestion> SearchableQuestions { get; set; } 
        public virtual IDbSet<PrepaidClassroom> PrepaidClassrooms { get; set; } 
        public virtual IDbSet<SafetyCourseParticipant> SafetyCourseParticipants { get; set; }

        private class ActivitySetAvailableSpots
        {
            public int ActivitySetId { get; set; }
            public int? AvailableSpots { get; set; }
        }
    }    
}