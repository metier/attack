﻿namespace Metier.Phoenix.Core.Data.Enums
{
    public enum EducationPartnerSchemaTypes
    {
        HiMolde = 0,
        Skema = 1
    }
}