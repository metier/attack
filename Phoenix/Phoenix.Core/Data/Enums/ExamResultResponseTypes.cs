﻿namespace Metier.Phoenix.Core.Data.Enums
{
    public enum ExamResultResponseTypes
    {
        NoScore = 0,
        MasteryScore = 1
    }
}