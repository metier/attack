﻿namespace Metier.Phoenix.Core.Data.Enums
{
    public enum MetierContactType
    {
        AccountManager = 0,
        ProgramCoordinator = 1,
        ExamCoordinator = 2,
        SubjectMatterExpert = 3
    }
}