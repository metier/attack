﻿namespace Metier.Phoenix.Core.Data.Enums
{
    public enum MailStatusType
    {
        Created = 0,
        Sent = 1,
        Failed = 2,
        Cancelled = 9,
        ResendingAttempt2 = 10,
        ResendingAttempt3 = 11
    }
}