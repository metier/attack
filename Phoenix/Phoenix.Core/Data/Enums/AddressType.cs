﻿namespace Metier.Phoenix.Core.Data.Enums
{
    public enum AddressType
    {
        Invoice = 0,
        Post = 1
    }   
}