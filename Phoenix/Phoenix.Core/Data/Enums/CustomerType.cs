﻿namespace Metier.Phoenix.Core.Data.Enums
{
    public enum CustomerType
    {
        Arbitrary = 0,
        Corporate = 1
    }
}