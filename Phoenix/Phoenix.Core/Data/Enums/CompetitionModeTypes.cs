﻿namespace Metier.Phoenix.Core.Data.Enums
{
    public enum CompetitionModeTypes
    {
        SeeClassmatesWithScores = 0,
        SeeClassmatesWithoutScores = 1,
        None = 2
    }
}