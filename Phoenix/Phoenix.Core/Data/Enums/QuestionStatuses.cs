﻿namespace Metier.Phoenix.Core.Data.Enums
{
    public enum QuestionStatuses
    {
        Draft = 1,
        Published = 2,
        Obsolete = 9
    }

    public static class QuestionStatusExtensions
    {
        public static string ToExcelString(this QuestionStatuses status)
        {
            switch (status)
            {
                case QuestionStatuses.Draft:
                    return "D";
                case QuestionStatuses.Published:
                    return "P";
                case QuestionStatuses.Obsolete:
                    return "O";
                default:
                    return null;
            }
        }
    }
}