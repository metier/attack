﻿namespace Metier.Phoenix.Core.Data.Enums
{
    public enum ValidationErrorCodes
    {
        // Invoice codes: 1xxx-range
        InvoiceMissingWorkOrder = 1001

        // Exam codes: 2xxx-range
        , ParticipantNotFound = 2001
        , ParticipantNotQualified = 2002

        // Authentication codes: 3xxx-range
        , UserExpired = 3001
    }
}