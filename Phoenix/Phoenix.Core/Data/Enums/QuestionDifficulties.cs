﻿namespace Metier.Phoenix.Core.Data.Enums
{
    public enum QuestionDifficulties
    {
        Easy = 1,
        Medium = 2,
        Hard = 3
    }
}