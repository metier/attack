﻿namespace Metier.Phoenix.Core.Data.Enums
{
    public enum EducationTypes
    {
        None = 0,
        University = 1,
        UniversityCollege = 2,
        HighSchoolGeneralStudyCompetence = 3
    }
}