﻿namespace Metier.Phoenix.Core.Data.Enums
{
    public enum ArticleTypes
    {
        ELearning = 1,
        Classroom = 2,
        MultipleChoice = 3,
        CaseExam = 4,
        ProjectAssignment = 5,
        InternalCertification = 6,
        Other = 7,
        Mockexam = 8,
        ExternalCertification = 9,
        Seminar = 10,
        Webinar = 11,
        Subscription = 12
    }
}