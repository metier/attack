﻿namespace Metier.Phoenix.Core.Data.Enums
{
    public enum OrderStatus
    {
        Proposed = 0,
        Draft = 1,
        Transferred = 2,
        NotInvoiced = 3,
        Invoiced = 4,
        Rejected = 5
    }
}