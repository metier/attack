﻿namespace Metier.Phoenix.Core.Data.Enums
{
    public enum MessageType
    {
        PasswordResetRequest = 0,
        UserRegistrationNotification = 1,
        EnrollmentConfirmation = 2,
        ReminderWorkshopExam = 3,
        ReminderElearning = 4,
        ElearningCompleted = 5,
        EnrollmentConfirmationCustomer = 6,
        EnrollmentNotQualifiedForExam = 7,
        ReminderExaminer = 8,
        CandidateSurvey = 9,
        ExamSubmitted = 10,
        MissingExamCompetency = 11
    }
}