﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.ViewEntities
{
    [Table("v_Phoenix_Searchable_Customers")]
    public class SearchableCustomer
    {
        public int Id { get; set; }
        public string ExternalCustomerId { get; set; }
        public string Name { get; set; }
        public string DistributorName { get; set; }
        public int? ParentId { get; set; }
        public string ParentExternalCustomerId { get; set; }
        public string ParentName { get; set; }
        public string Language { get; set; }
        public int DistributorId { get; set; }
        public bool IsArbitrary { get; set; }
    }
}