﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Core.Data.ViewEntities
{
    [Table("v_Phoenix_Searchable_Mails")]
    public class SearchableMail
    {   
        public int Id { get; set; }
        public int? UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public MessageType MessageType { get; set; }
        public string Subject { get; set; }
        public string RecipientEmail { get; set; }
        public DateTime Created { get; set; }
        public MailStatusType? MailStatus { get; set; }
        public DateTime? StatusTime { get; set; }
    }
}