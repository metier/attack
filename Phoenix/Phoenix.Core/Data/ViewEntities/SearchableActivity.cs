﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.ViewEntities
{
    [Table("v_Phoenix_Searchable_Activities")]
    public abstract class SearchableActivity
    {
        public Guid Id { get; set; } // Dummy field to satisfy entity framework
        public int ActivityId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int CustomerArticleId { get; set; }
        public int DistributorId { get; set; }
        public int ArticleOriginalId { get; set; }
        public bool IsDeleted { get; set; }
        public string ArticlePath { get; set; }
        public string ArticleType { get; set; }
        public string Name { get; set; }
        public string MomentTimezoneName { get; set; }
        
        public string RcoCourseVersion { get; set; }
        public int? RcoCourseId { get; set; }
        
        public int? RcoExamContainerId { get; set; }
        public string RcoExamContainerName { get; set; }
        
        public DateTimeOffset? ActivityStart { get; set; }        
        public DateTimeOffset? ActivityEnd { get; set; }

        public bool IsProposed { get; set; }
        public decimal? FixedPrice { get; set; }


        
        public int LanguageId { get; set; }
        public int ArticleTypeId { get; set; }
        public int ProductId { get; set; }
        public string ArticleCode { get; set; }
    }

    [Table("v_Phoenix_Searchable_Activities")]
    public class SearchableActivityWithActivitySet : SearchableActivity
    {
        public int? ActivitySetId { get; set; }
        public string ActivitySetName { get; set; }
    }        
}