﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.ViewEntities
{
    [Table("v_Phoenix_Searchable_CustomerArticles")]
    public class SearchableCustomerArticle
    {
        public int Id { get; set; }
        public decimal? FixedPrice { get; set; }
        public decimal? UnitPrice { get; set; }
        public int? CurrencyCodeId { get; set; }
        public string Title { get; set; }

        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int? DistributorId { get; set; }

        public int ArticleCopyId { get; set; }
        public int ArticleOriginalId { get; set; }
        
        public int? RcoCourseId { get; set; }
        public string CourseName { get; set; }
        public string CourseVersion { get; set; }

        public int? RcoExamContainerId { get; set; }
        public string RcoExamContainerName { get; set; }

        public int? StatusCodeId { get; set; }
        public string Status { get; set; }

        public DateTime Created { get; set; }
    }
}