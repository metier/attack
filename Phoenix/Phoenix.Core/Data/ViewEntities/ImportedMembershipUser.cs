﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.ViewEntities
{
    [Table("Phoenix_ImportedMembershipUser")]
    public class ImportedMembershipUser
    {
        [Key]
        public Int64 RowId { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
    }
}