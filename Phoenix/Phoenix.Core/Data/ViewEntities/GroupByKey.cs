﻿namespace Metier.Phoenix.Core.Data.ViewEntities
{
    public class GroupByKey
    {
        public int DistributorId { get; set; }
        public int CustomerId { get; set; }
        public int ActivitySetId { get; set; }
        public int CurrencyCodeId { get; set; }
        public string Signature { get; set; }
    }
}