﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Core.Data.ViewEntities
{
    [Table("v_Phoenix_SearchableOrders")]
    public class SearchableOrder
    {
        [Key, Column(Order = 0)]
        public string GroupByKey { get; set; }
        public int DistributorId { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public int CustomerId { get; set; }
        public int? CoordinatorId { get; set; }
        public string CoordinatorFullName { get; set; }
        public string YourOrder { get; set; }
        public string YourRef { get; set; }
        public string OurRef { get; set; }
        public decimal? TotalAmount { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime LastModified { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public bool BundledBilling { get; set; }
        public CustomerType CustomerType { get; set; }

        [Key, Column(Order = 1)]
        public int? OrderId { get; set; }
        public int? OrderNumber { set; get; }
        public int? InvoiceNumber { get; set; }
        public bool IsCreditNote { get; set; }
    }
}