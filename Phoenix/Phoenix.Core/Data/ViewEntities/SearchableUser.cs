﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.ViewEntities
{
    [Table("v_Phoenix_Searchable_Users")]
    public class SearchableUser
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string MembershipUserId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
		public bool CustomerIsArbitrary { get; set; }
		public int DistributorId { get; set; }
        public bool IsPhoenixUser { get; set; }		

		public string LeadingText11 { get; set; } //CustomField1
        public string LeadingText12 { get; set; } //CustomField2
        public string LeadingText13 { get; set; } //EmployeeId
        public string LeadingText14 { get; set; } //Department
        public string LeadingText15 { get; set; } //Position
        public string LeadingText16 { get; set; } //Supervisor
        public string LeadingText17 { get; set; } //ConfirmationEmail
        public int NumberOfParticipations { get; set; }
        public int NumberOfEnrollments { get; set; }
        public DateTimeOffset? LatestCourseDate { get; set; }
        public DateTime? LatestEnrollmentDate { get; set; }
		public DateTime? ExpiryDate { get; set; }
		[NotMapped]
        public List<SearchableParticipant> Participations { get; set; }
    }
}