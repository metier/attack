﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.ViewEntities
{
    [Table("v_Phoenix_Searchable_Articles")]
    public class SearchableArticle
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string Path { get; set; }
        public string Title { get; set; }
        public string Language { get; set; }
        public string StatusCode { get; set; }
        public string ArticleType { get; set; }
        public string CourseVersion { get; set; }
    }
}