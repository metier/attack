﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.Enums;
using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.ViewEntities
{
    [Table("v_Phoenix_SearchableParticipants")]
    public class SearchableParticipant
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int UserId { get; set; }
        public int ActivitySetId { get; set; }
        public int ActivityId { get; set; }
        public int ActivityOwnerCustomerId { get; set; }
        public string ActivityOwnerCustomerName { get; set; }
        public int CustomerArticleId { get; set; }
		public string CustomerArticleTitle { get; set; }
		public int ArticleId { get; set; }
        public int? OrderId { get; set; }
        
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CustomerName { get; set; }
        public string ArticlePath { get; set; }
        public string ArticleType { get; set; }
        public int ArticleTypeId { get; set; }
        public decimal? UnitPrice { get; set; }
        public string CurrencyCode { get; set; }
        public string ActivitySetName { get; set; }
        public string ActivityName { get; set; }
        public bool IsInvoiced { get; set; }
        public int? OrderNumber { get; set; }
        public int? InvoiceNumber { get; set; }
        public string ParticipantStatusCodeValue { get; set; }
		public DateTime? ParticipantCompletedDate { get; set; }		
		public OrderStatus? OrderStatus { get; set; }
        
        public string MomentTimezoneName { get; set; }
		public bool HideActivityDates { get; set; }

		public DateTimeOffset? ActivityStart { get; set; }
        public DateTimeOffset? ActivityEnd { get; set; }
        public int? RcoCourseId { get; set; }
        public int? RcoExamContainerId { get; set; }
        public int? LanguageId { get; set; }
        public int? ProductDescriptionId { get; set; }
        public bool IsUserCompetenceInfoRequired { get; set; }
        public int? EnrollmentId { get; set; }
        public int EnrolledActivitySetId { get; set; }
        public string EnrolledActivitySetName { get; set; }
        public int RcoLessonCount { get; set; }
        public int RcoLessonCompletedCount { get; set; }
        public int RcoExamCount { get; set; }
        public int RcoExamCompletedCount { get; set; }
 
        [NotMapped]
		public List<ParticipantInfoElement> ParticipantInfoElements { get; set; }
    }
}