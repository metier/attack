﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.ViewEntities.Reports
{
    [Table("v_Phoenix_Report_ToBeInvoiced")]
    public class OrdersForInvoicing
    {
        [Key]
        public Guid RowId { get; set; }
        public string Id { get; set; }
        public string IdType { get; set; }
        public int DistributorId { get; set; }
        public string DistributorName { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int ProductId { get; set; }
        public string Path { get; set; }
        public string ArticleCode { get; set; }
        public string ActivityTitle { get; set; }
        public DateTime EarliestInvoiceDate { get; set; }
        public string CurrencyCode { get; set; }
        public decimal Amount { get; set; }
        public int OrderStatus { get; set; }
        public bool IsArbitrary { get; set; }
    }
}