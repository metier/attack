﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.ViewEntities.Reports
{
    [Table("v_Phoenix_Report_PotentialParticipations")]
    public class PotentialParticipation
    {
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public int UserId { get; set; }

        public string Customer { get; set; }
        public string ProductPath { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string CustomField1 { get; set; }
        public string CustomField2 { get; set; }
        public string EmployeeId { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string Supervisor { get; set; }
        public string ConfirmationEmail { get; set; }
        public bool IsArbitrary { get; set; }
		public string ProductIdsEnrolled { get; set; }
	}
}