﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.ViewEntities.Reports
{
    [Table("v_Phoenix_Report_ProgressStatus")]
    public class ProgressStatus
    {
        public int CustomerId { get; set; }
        public int DistributorId { get; set; }
        public string Customer { get; set; }
        public string Distributor { get; set; }
        public int ProductId { get; set; }
        public int ArticleId { get; set; }
        public string ArticleNumber { get; set; }
        public int ActivityId { get; set; }
        public string ActivityTitle { get; set; }
        public DateTimeOffset? StartDate { get; set; }

        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string CustomField1 { get; set; }
        public string CustomField2 { get; set; }
        public string EmployeeId { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string Supervisor { get; set; }
        public string ConfirmationEmail { get; set; }
        public DateTime EnrollmentDate { get; set; }
        public DateTime? CompletedDate { get; set; }
        
        public int NumberOfCompletedLessons { get; set; }
        public int NumberOfLessons { get; set; }
        public string Status { get; set; }
        public bool IsArbitrary { get; set; }
    }
}