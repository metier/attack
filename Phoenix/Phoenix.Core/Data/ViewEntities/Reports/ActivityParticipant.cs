﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.ViewEntities.Reports
{
    [Table("v_Phoenix_Report_ActivityParticipants")]
    public class ActivityParticipant
    {
        public int CustomerId { get; set; }
        public int DistributorId { get; set; }
        public int UserId { get; set; }
        public int ActivityId { get; set; }
        public int ActivitySetId { get; set; }
        public int ProductId { get; set; }
        public int ArticleId { get; set; }
        public int ArticleTypeId { get; set; }
        public int CustomerArticleId { get; set; }
        public int ParticipantId { get; set; }

        public string Customer { get; set; }
        public string Distributor { get; set; }
        public string ProductNumber { get; set; }
        public string ArticleNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime? EnrollmentDate { get; set; }
        public DateTimeOffset? StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public string Status { get; set; }
        public string Result { get; set; }
        public bool IsArbitrary { get; set; }
    }
}