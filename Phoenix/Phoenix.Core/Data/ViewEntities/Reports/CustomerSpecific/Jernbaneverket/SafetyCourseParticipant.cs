﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.ViewEntities.Reports.CustomerSpecific.Jernbaneverket
{
    [Table("v_Phoenix_report_SafetyCourseParticipants")]
    public class SafetyCourseParticipant
    {

        public int Id { get; set; }

        public int CustomerId { get; set; }
        public int UserId { get; set; }
		
		public string CustomerName { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string UserName { get; set; }
        public string CountryOfCitizenship { get; set; }
        public string DateOfBirth { get; set; }
		public string Position { get; set; }
		//public string HelmetNumber { get; set; }
		public string IdNumber { get; set; }
		public string Employer { get; set; }
		public string SubContractorFor { get; set; }
		public string ConfirmationEmail { get; set; }
		public string ActivitySet { get; set; }
		public string StatusPart1 { get; set; }
		public string StatusPart2 { get; set; }
		public string StatusPart3 { get; set; }
		public string Comment { get; set; }

		public DateTime? UserExpiryDate { get; set; }      
		public DateTime? CourseDate { get; set; }      
        public DateTime? LatestCompletedStatusPart1 { get; set; }
		public DateTime? LatestCompletedStatusPart2 { get; set; }
        public DateTime? LatestCompletedStatusPart3 { get; set; }
    }
}