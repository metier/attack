﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.ViewEntities.Reports
{
    [Table("v_Phoenix_Report_ParticipationStatusPrActivity")]
    public class ParticipationStatusPrActivity
    {
        [Key]
        public int ParticipantId { get; set; }
        public int UserId { get; set; }
        public int CustomerId { get; set; }
        public int DistributorId { get; set; }
        public int ProductId { get; set; }

        public string Customer { get; set; }
        public string Distributor { get; set; }
        public string ProductNumber { get; set; }
        public string ArticleNumber { get; set; }
        public string ContentType { get; set; }
        public string ProductTitle { get; set; }
        public string ActivitySetTitle { get; set; }
        
        // User
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? ExpiryDate { get; set; }
        
        // Participant info elements
        public string CustomField1 { get; set; }
        public string CustomField2 { get; set; }
        public string EmployeeId { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string Supervisor { get; set; }
        public string ConfirmationEmail { get; set; }
        
        public string ActivityTitle { get; set; }
        public DateTime EnrollmentDate { get; set; }
        public DateTimeOffset? StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public int? NumberOfCompletedLessons { get; set; }
        public int? NumberOfLessons { get; set; }
        public string CourseStatus { get; set; }
        public string Status { get; set; }

        public bool IsArbitrary { get; set; }
    }
}