﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.ViewEntities.Reports
{
    [Table("v_Phoenix_report_prepaidClassroms")]
    public class PrepaidClassroom
    {
        public string CustomerName { get; set; }
        public string AccountNumber { get; set; }
        public string Article { get; set; }
        public string WorkOrder { get; set; }
        public string Course { get; set; }
        public string OrderTitle { get; set; }
        public string Distributor { get; set; }

        public int DistributorId { get; set; }

        public DateTime? ClassroomDate { get; set; }
        public DateTime? OrderDate { get; set; }

        public Decimal? Amount { get; set; }

        public int? InvoiceNumber { get; set; }
        public string Currency { get; set; }
    }
}