﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.ViewEntities.Reports
{
    [Table("v_Phoenix_Report_ParticipantsPrActivity")]
    public class ParticipationPrActivity
    {
        [Key]
        public int ParticipantId { get; set; }
        public int DistributorId { get; set; }
        public string Distributor { get; set; }
        public int CustomerId { get; set; }
        public string Customer { get; set; }
        public string ParentOrg { get; set; }
        public int ProductId { get; set; }
        public string ProductPath { get; set; }
        public string ArticleNumber { get; set; }
        public string ContentType { get; set; }
        public string CourseLink { get; set; }
        public string ArticleName { get; set; }
        public string CustomerArticleName { get; set; }
        public string CustomerArticleLanguage { get; set; }
        public string ActivityName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public string CustomField1 { get; set; }
        public string CustomField2 { get; set; }
        public string EmployeeId { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string Supervisor { get; set; }
        public string ConfirmationEmail { get; set; }
        public DateTime? EnrollmentDate { get; set; }
        public DateTimeOffset? StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public string Status { get; set; }
        public int NumberOfLessons { get; set; }
        public int Points { get; set; }
        public int NumberOfCompletedLessons { get; set; }
        public string AccountManager { get; set; }
        public string Coordinator { get; set; }
        public bool? SelfEnrollmentExams { get; set; }
        public bool? SelfEnrollmentCourses { get; set; }
        public string UserRole { get; set; }
        public bool IsArbitrary { get; set; }

    }
}