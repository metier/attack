﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Metier.Phoenix.Core.Data.ViewEntities.Reports
{
    [Table("v_Phoenix_Report_HubspotParticipants")]
    public class HubspotParticipant
    {
        [Key]
        public int UserId { get; set; }
        public int DistributorId { get; set; }
        //public int ProductId { get; set; }
        public int CustomerId { get; set; }

        public string Distributor { get; set; }
        public string CoursesEnrolled { get; set; }
        public string CoursesCompleted { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Customer { get; set; }
        public bool IsArbitrary { get; set; }
        public DateTime? FirstEnrollmentDate { get; set; }
        public DateTime? LastEnrollmentDate { get; set; }
    }
}