﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Core.Data.ViewEntities
{
    [Table("v_Phoenix_Searchable_Questions")]
    public class SearchableQuestion
    {
        public int Id { get; set; }
        
        public string CustomerName { get; set; }
        public int? CustomerId { get; set; }

        public string Author { get; set; }

        public string Text { get; set; }
        
        public int LanguageId { get; set; }
        public string Language { get; set; }

        public string Type { get; set; }
        public QuestionStatuses Status { get; set; }
        public QuestionDifficulties Difficulty { get; set; }

        [NotMapped]
        public List<Tag> Tags { get; set; }
        [NotMapped]
        public List<Product> Products { get; set; }
        [NotMapped]
        public List<Rco> Rcos { get; set; }

        public DateTime Created { get; set; }
        public int CreatedById { get; set; }
        public string CreatedByName { get; set; }

        public DateTime? Modified { get; set; }
        public int? ModifiedById { get; set; }
        public string ModifiedByName { get; set; }

        public DateTime? LastUsed { get; set; }
    }
}