﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.ViewEntities
{
    [Table("v_Phoenix_ProposedOrderLines")]
    public class ProposedOrderLine
    {
        [Key, Column(Order = 0)]
        public string GroupByKey { get; set; }
        [Key, Column(Order = 1)]
        public int ParticipantId { get; set; }
        [Key, Column(Order = 2)]
        public int ActivityId { get; set; }
        public string Title { get; set; }
        public string DetailedText { get; set; }
        public int? ActivitySetId { get; set; }
        public int DistributorId { get; set; }
        public int CustomerId { get; set; }
        public int? CoordinatorId { get; set; }
        public string CoordinatorFullName { get; set; }
        public string Name { get; set; }
        public string YourOrder { get; set; }
        public string YourRef { get; set; }
        public string OurRef { get; set; }
        public bool BundledBilling { get; set; }
        public bool? CustomField1IsGroupByOnInvoice { get; set; }
        public bool? CustomField2IsGroupByOnInvoice { get; set; }
        public bool? EmployeeIdIsGroupByOnInvoice { get; set; }
        public bool? DepartmentIsGroupByOnInvoice { get; set; }
        public bool? PositionIsGroupByOnInvoice { get; set; }
        public bool? SupervisorIsGroupByOnInvoice { get; set; }
        public bool? ConfirmationEmailIsGroupByOnInvoice { get; set; }
        public string CustomField1InfoText { get; set; }
        public string CustomField2InfoText { get; set; }
        public string EmployeeIdInfoText { get; set; }
        public string DepartmentInfoText { get; set; }
        public string PositionInfoText { get; set; }
        public string SupervisorInfoText { get; set; }
        public string ConfirmationEmailInfoText { get; set; }
        public int ParticipantUserId { get; set; }
        public int? CurrencyCodeId { get; set; }
        public decimal? UnitPrice { get; set; }
    }
}