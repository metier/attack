﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.ViewEntities
{
    [Table("v_Phoenix_SearchableRcos")]
    public class SearchableRco
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Context { get; set; }
        public string Version { get; set; }
        public string Language { get; set; }
        public DateTime Created { get; set; }
        public int? ChildCount { get; set; }
    }
}