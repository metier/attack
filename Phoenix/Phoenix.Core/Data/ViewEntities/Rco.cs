﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.ViewEntities
{
    [Table("Rcos")]
    public class Rco : IdentifiableEntity
    {
        public int? ParentCourseId { get; set; }
        public string Name { get; set; }
        public string Context { get; set; }
        public string Version { get; set; }
        public string Language { get; set; }
        public bool? IsFinalTest { get; set; }
        public string ParentName { get; set; }
        public int? ParentId { get; set; }
        public string ParentContext { get; set; }
        public DateTime Created { get; set; }

        public int? GoldPoints { get; set; }
        public int? SilverPoints { get; set; }
        public int? UnlockTestPoints { get; set; }
        public int? Weight { get; set; }
        public int ChildCount { get; set; }
    }
}