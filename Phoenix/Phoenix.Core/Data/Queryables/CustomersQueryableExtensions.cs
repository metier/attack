﻿using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.Queryables
{
    public static class CustomersQueryableExtensions
    {
         public static IQueryable<Customer> WithAll(this IQueryable<Customer> queryable)
         {
             return queryable.Include(c => c.Addresses)
                                 .Include(c => c.MetierContactPersons)
                                 .Include(c => c.CustomerContactPersons)
                                 .Include(c => c.CustomerInvoicing)
                                 .Include(c => c.ParticipantInfoDefinition)
                                 .Include("ParticipantInfoDefinition.AcceptedEmailAddresses")
                                 .Include(c => c.AutomailDefinition)
								 .Include(c => c.DiplomaLogoAttachment)
								 .Include(c => c.DiplomaSignatureAttachment)
								 .Include(c => c.CustomLeadingTexts);
         }
    }
}