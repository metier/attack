﻿using System.Linq;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.Queryables
{
    public static class CustomerArticleQueryableExtensions
    {
        public static CustomerArticle FindById(this IQueryable<CustomerArticle> customerArticles, int id, IQueryable<Article> articles)
        {
            return customerArticles.FirstOrDefault(c => c.Id == id && !c.IsDeleted).WithArticle(articles);
        }

        public static CustomerArticle WithArticle(this CustomerArticle customerArticle, IQueryable<Article> articles)
        {
            if (customerArticle != null)
            {
                var articleCopy = articles.FindByIdWithArticlePath(customerArticle.ArticleCopyId);
                if (articleCopy != null)
                {
                    customerArticle.ArticleCopy = articleCopy;
                }
            }
            return customerArticle;
        }

    }
}