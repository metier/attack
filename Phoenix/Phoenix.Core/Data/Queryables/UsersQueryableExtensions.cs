﻿using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.Queryables
{
    public static class UsersQueryableExtensions
    {
        public static User FindById(this IQueryable<User> users, int id)
        {
            return users.FirstOrDefault(u => u.Id == id);
        }

        public static IQueryable<User> WithAll(this IQueryable<User> queryable)
        {
            return queryable.WithAttachment().WithCustomer().WithUserInfoElements();
        }

        public static IQueryable<User> WithAttachment(this IQueryable<User> queryable)
        {
            return queryable.Include(u => u.Attachment);
        }

        public static IQueryable<User> WithCustomer(this IQueryable<User> queryable)
        {
            return queryable.Include(u => u.Customer).Include(u => u.Customer.ParticipantInfoDefinition);
        }

        public static IQueryable<User> WithUserInfoElements(this IQueryable<User> queryable)
        {
            return queryable.Include(u => u.UserInfoElements);
        }

        public static IQueryable<User> WithUserInfoElementsWithLeadingTexts(this IQueryable<User> queryable)
        {
            return queryable.Include("UserInfoElements.LeadingText");
        }
    }
}