﻿using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.Queryables
{
    public static class CourseProgramQueryableExtensions
    {
        public static IQueryable<CourseProgram> WithAll(this IQueryable<CourseProgram> coursePrograms)
        {
            return coursePrograms
                .Include("Steps.Exams.CustomerArticles.ArticleCopy.Product")
                .Include("Steps.Exams.CustomerArticles.ArticleCopy.ArticleType")
                .Include("Steps.Exams.CustomerArticles.ArticleCopy.Language")

                .Include("Steps.Modules.CustomerArticles.ArticleCopy.Product")
                .Include("Steps.Modules.CustomerArticles.ArticleCopy.ArticleType")
                .Include("Steps.Modules.CustomerArticles.ArticleCopy.Language");

        }
    }
}