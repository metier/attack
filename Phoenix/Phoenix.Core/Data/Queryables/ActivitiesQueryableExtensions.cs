﻿using System;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.Queryables
{
    public static class ActivitiesQueryableExtensions
    {
        public static Activity FindById(this IQueryable<Activity> activities, int id)
        {
            return activities.FirstOrDefault(a => a.Id == id && !a.IsDeleted);
        }

        public static Activity IncludeParticipants(this Activity activity, MetierLmsContext context)
        {
            if (activity != null)
            {
                context.Entry(activity)
                    .Collection(a => a.Participants)
                    .Query()
                    .Where(p => !p.IsDeleted)
                    .WithQualificationStatus(activity, context)
                    .Load();
            }

            return activity;
        }
        
        public static Activity IncludeParticipantsWithOrderInformation(this Activity activity, MetierLmsContext context)
        {
            if (activity != null)
            {
                if (activity.Participants == null || activity.Participants.Count == 0)
                    activity.IncludeParticipants(context);

                activity.Participants.ForEach(participant =>
                {
                    context.Entry(participant).Collection(p => p.ParticipantOrderLines).Load();
                    participant.ParticipantOrderLines.ForEach(orderLine => context.Entry(orderLine).Reference(pol => pol.Order).Load());
                });
            }
            return activity;
        }

        public static IQueryable<Activity> WithAllExceptParticipants(this IQueryable<Activity> activities)
        {
            return activities.Include(a => a.ActivityPeriods)
                             .Include(a => a.Resources)
                             .Include(a => a.Resources.Select(r=>r.Attachments))
                             .Include(a => a.Resources.Select(r => r.Addresses))
                             .Include(a => a.Attachments)
                             .Include(a => a.CaseAttachment)
                             .Include(a => a.ActivityExamCourses)
                             .Include(a => a.ActivityAutomailText)
                             .Include(a => a.EnrollmentConditions);
        }
    }
}