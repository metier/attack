﻿using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.Queryables
{
    public static class ResourceQueryableExtensions
    {
        public static IQueryable<Resource> WithAll(this IQueryable<Resource> resources)
        {
            return resources.WithResourceType().WithCurrency().WithAddresses().WithAttachments();
        }

        public static IQueryable<Resource> WithResourceType(this IQueryable<Resource> resources)
        {
            return resources.Include(r => r.ResourceType);
        }

        public static IQueryable<Resource> WithCurrency(this IQueryable<Resource> resources)
        {
            return resources.Include(r => r.Currency);
        }

        public static IQueryable<Resource> WithAddresses(this IQueryable<Resource> resources)
        {
            return resources.Include(r => r.Addresses);
        }

        public static IQueryable<Resource> WithAttachments(this IQueryable<Resource> resources)
        {
            return resources.Include(r => r.Attachments);
        }
    }
}