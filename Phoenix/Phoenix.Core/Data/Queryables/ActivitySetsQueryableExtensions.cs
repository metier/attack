﻿using System.Data.Entity;
using System.Linq;
using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Interfaces;
using Metier.Phoenix.Core.Utilities;

namespace Metier.Phoenix.Core.Data.Queryables
{
    public static class ActivitySetsQueryableExtensions
    {
        public static ActivitySet IncludeActivitySetShares(this ActivitySet activitySet, MetierLmsContext context)
        {
            if (activitySet != null)
            {
                context.Entry(activitySet).Collection(a => a.ActivitySetShares).Query().Load();
                foreach (var activitySetShare in activitySet.ActivitySetShares)
                {
                    activitySetShare.Customer = context.Customers.FirstOrDefault(c => c.Id == activitySetShare.CustomerId);
                }
            }
            return activitySet;
        }

        public static IQueryable<ActivitySet> WithAllExceptShares(this IQueryable<ActivitySet> activitySets)
        {
            return activitySets.WithActivitiesAndResources().WithActivitiesAndActivityPeriods().WithEnrollments().WithCustomer();
        }

        public static IQueryable<ActivitySet> WithActivitiesAndActivityPeriods(this IQueryable<ActivitySet> activitySets)
        {
            return activitySets.Include(set => set.Activities.Select(activity => activity.ActivityPeriods))
                .Include(set => set.Activities.Select(activity => activity.CustomerArticle.ArticleCopy.Product))
                .Include(set => set.Activities.Select(activity => activity.CustomerArticle.ArticleCopy.ArticleType))
                .Include(set => set.Activities.Select(activity => activity.CustomerArticle.ArticleCopy.Language))
                .Include(set => set.Activities.Select(activity => activity.CustomerArticle.Module))
                ;       //This will NOT populate the "objectchain" deep enough to enable the article to i.e. produce an articlepath.
        }

        public static IQueryable<ActivitySet> WithActivities(this IQueryable<ActivitySet> activitySets)
        {
            return activitySets.Include(a => a.Activities);
        }

        public static IQueryable<ActivitySet> WithActivitiesAndResources(this IQueryable<ActivitySet> activitySets)
        {
            return activitySets.Include("Activities.Resources");
        }

        public static IQueryable<ActivitySet> WithEnrollments(this IQueryable<ActivitySet> activitySets)
        {
            return activitySets.Include(a => a.Enrollments);
        }

        public static IQueryable<ActivitySet> WithCustomer(this IQueryable<ActivitySet> activitySets)
        {
            return activitySets.Include(a => a.Customer);
        }
    }
}