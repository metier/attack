﻿using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.Queryables
{
    public static class ExamsQueryableExtensions
    {
        public static IQueryable<Exam> WithQuestions(this IQueryable<Exam> exams)
        {
            return exams.Include("Questions.QuestionOptions");
        }
        
        public static IQueryable<Exam> WithAnswers(this IQueryable<Exam> exams)
        {
            return exams.Include("Answers.OptionsAnswers");
        }
    }
}