﻿using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.Queryables
{
    public static class OrdersQueryableExtensions
    {
        public static IQueryable<Order> WithAll(this IQueryable<Order> orders)
        {
            var result = orders.WithParticipants().WithUsers().WithActivities().WithCustomer();
            return result;
        }

        public static IQueryable<Order> WithParticipants(this IQueryable<Order> orders)
        {
            return orders.Include("ParticipantOrderLines.Participant.ParticipantInfoElements");
        }

        public static IQueryable<Order> WithUsers(this IQueryable<Order> orders)
        {
            return orders.Include("ParticipantOrderLines.Participant.User");
        }
        
        public static IQueryable<Order> WithActivities(this IQueryable<Order> orders)
        {
            return orders.Include("ActivityOrderLines.Activity");
        }
        
        public static IQueryable<Order> WithCustomer(this IQueryable<Order> orders)
        {
            return orders.Include(o => o.Customer);
        }

    }
}