﻿using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.Queryables
{
    public static class ProductTypeQueryableExtensions
    {
        public static IQueryable<ProductType> WithProductTemplateParts(this IQueryable<ProductType> productTypes)
        {
            return productTypes.Include(pt => pt.ProductTemplateParts);
        }
    }
}