﻿using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.Queryables
{
    public static class ArticleQueryableExtensions
    {
        public static Article FindByIdWithArticlePath(this IQueryable<Article> articles, int id)
        {
            return articles.WithArticlePath().FirstOrDefault(a => a.Id == id && !a.IsDeleted);
        }

        public static IQueryable<Article> WithArticlePath(this IQueryable<Article> articles)
        {
            return articles.WithProduct().WithLanguage().WithArticleType();
        }

        public static IQueryable<Article> WithAll(this IQueryable<Article> articles)
        {
            return articles.WithAttachments().WithStatusCode().WithModifiedBy().WithCreatedBy().WithArticleExamCourses().WithArticlePath();
        }

        public static IQueryable<Article> WithProduct(this IQueryable<Article> articles)
        {
            return articles.Include("Product.ProductCategory");
        }
        
        public static IQueryable<Article> WithLanguage(this IQueryable<Article> articles)
        {
            return articles.Include(a => a.Language);
        }
        
        public static IQueryable<Article> WithAttachments(this IQueryable<Article> articles)
        {
            return articles.Include(a => a.Attachments);
        }
        
        public static IQueryable<Article> WithStatusCode(this IQueryable<Article> articles)
        {
            return articles.Include(a => a.StatusCode);
        }

        public static IQueryable<Article> WithArticleType(this IQueryable<Article> articles)
        {
            return articles.Include(a => a.ArticleType);
        }

        public static IQueryable<Article> WithModifiedBy(this IQueryable<Article> articles)
        {
            return articles.Include(a => a.ModifiedBy);
        }

        public static IQueryable<Article> WithCreatedBy(this IQueryable<Article> articles)
        {
            return articles.Include(a => a.CreatedBy);
        }

        public static IQueryable<Article> WithArticleExamCourses(this IQueryable<Article> articles)
        {
            return articles.Include(a => a.ArticleExamCourses);
        }
    }
}