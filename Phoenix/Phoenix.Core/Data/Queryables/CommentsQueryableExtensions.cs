﻿using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.Queryables
{
    public static class CommentsQueryableExtensions
    {
        public static IQueryable<Comment> WithAll(this IQueryable<Comment> comments)
        {
            return comments.WithUser();
        }

        public static IQueryable<Comment> WithUser(this IQueryable<Comment> comments)
        {
            return comments.Include(c => c.User);
        }
    }
}