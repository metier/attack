﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Linq;
using Castle.Core.Internal;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Exceptions;

namespace Metier.Phoenix.Core.Data.Queryables
{
    public static class ParticipantsQueryableExtensions
    {
        private class QualifyingArticle
        {
            public int EnrollmentConditionId { get; set; }
            public int ProductId { get; set; }
            public int ArticleTypeId { get; set; }
            public int LanguageId { get; set; }
            public int CustomerId { get; set; }
        }

        private class QualifyingParticipation
        {
            public int UserId { get; set; }
            public string StatusCodeValue { get; set; }
            public int ProductId { get; set; }
            public int ArticleTypeId { get; set; }
            public int LanguageId { get; set; }
            public int CustomerId { get; set; }
        }

        public static IQueryable<Participant> WithParticipantInfoElements(this IQueryable<Participant> participants)
        {
            return participants.Include("ParticipantInfoElements.LeadingText");
        }
        
        public static IQueryable<Participant> WithAttachments(this IQueryable<Participant> participants)
        {
            return participants.Include(p => p.Attachments);
        }

        public static IQueryable<Participant> WithOrderInformation(this IQueryable<Participant> participant)
        {
            return participant.Include("ParticipantOrderLines.Order");
        }

        public static Participant IncludeQualificationStatus(this Participant participant, MetierLmsContext context)
        {
            if(participant == null)
                return null;

            int activityId = participant.ActivityId;

            var activity =
                context.Activities.Where(a => a.Id == activityId).Include(a => a.EnrollmentConditions).FirstOrDefault();

            var participants = new List<Participant> { participant};
            participants.AsQueryable().WithQualificationStatus(activity, context);

            return participants.First();
        }

        public static IQueryable<Participant> WithQualificationStatus(this IQueryable<Participant> participants, Activity activity, MetierLmsContext context)
        {
            var participantList = participants.ToList();

            if (participantList.Any(p => p.ActivityId != activity.Id))
                throw new ValidationException("All participants must be enrolled to the provided activity.");

            var enrollmentConditions = context.EnrollmentConditions.Where(ec => ec.ActivityId == activity.Id);

            //If no enrollmentconditions exist, it's all good
            if (enrollmentConditions.ToList().Count == 0)
            {
                participantList.ForEach(p => p.QualificationStatus = ParticipantQualificationStatus.Qualified);
                return participants;
            }

            //Retrieves all participations for the users on the activity
            var qualifyingParticipations = GetQualifyingParticipations(activity, context);
            var qualificationStatusCodesAsPrioritizedList = new List<string>
            {
                ParticipantQualificationStatus.NotQualified,
                ParticipantQualificationStatus.PartiallyQualified,
                ParticipantQualificationStatus.Qualified
            };

            foreach (var participant in participantList)
            {
                foreach (var condition in enrollmentConditions)
                {
                    var status = GetBestParticipationQualificationStatusForCondition(participant.UserId, qualifyingParticipations.AsQueryable(), condition);

                    if (string.IsNullOrEmpty(participant.QualificationStatus) ||
                        qualificationStatusCodesAsPrioritizedList.IndexOf(status) < qualificationStatusCodesAsPrioritizedList.IndexOf(participant.QualificationStatus))
                    {
                        participant.QualificationStatus = status;
                    }
                }
            }

            return participants;
        }

        private static string GetBestParticipationQualificationStatusForCondition(int userId, IQueryable<QualifyingParticipation> qualifyingParticipations, EnrollmentCondition condition)
        {
            var participants = GetParticipationsMatchingCondition(userId, qualifyingParticipations.AsQueryable(), condition);

            if (!participants.Any())
            {
                return ParticipantQualificationStatus.NotQualified;
            }

            var status = participants.OrderByDescending(p => ParticipantStatusCodes.GetCodesAsSortedList().IndexOf(p.StatusCodeValue)).First().StatusCodeValue;

            if (condition.IsStrictRequirement)
            {
                switch (status)
                {
                    case ParticipantStatusCodes.Completed:
                    case ParticipantStatusCodes.Dispensation:
                        return ParticipantQualificationStatus.Qualified;
                    default:
                        return ParticipantQualificationStatus.NotQualified;
                }
            }

            switch (status)
            {
                case ParticipantStatusCodes.Completed:
                case ParticipantStatusCodes.Dispensation:
                    return ParticipantQualificationStatus.Qualified;
                case ParticipantStatusCodes.Enrolled:
                case ParticipantStatusCodes.InProgress:
                    return ParticipantQualificationStatus.PartiallyQualified;
                default:
                    return ParticipantQualificationStatus.NotQualified;
            }
        }

        private static IQueryable<QualifyingParticipation> GetParticipationsMatchingCondition(int userId, IQueryable<QualifyingParticipation> qualifyingParticipations, EnrollmentCondition condition)
        {
            return 
                from p in qualifyingParticipations
                where p.UserId == userId
                where p.ProductId == condition.ProductId && p.ArticleTypeId == condition.ArticleTypeId
                where (!condition.LanguageId.HasValue || condition.LanguageId.Value == p.LanguageId)
                where (!condition.CustomerId.HasValue || condition.CustomerId.Value == p.CustomerId)
                select p;
        }

        private static List<QualifyingParticipation> GetQualifyingParticipations(Activity activity, MetierLmsContext context)
        {
            //Retrieves all id's of the users on the activity
            var activityUserIds = context.Users
                .Join(context.Participants.Where(p => !p.IsDeleted && p.ActivityId == activity.Id), u => u.Id,
                    p => p.UserId, (u, p) => u.Id);

            return GetQualifyingParticipations(activityUserIds, context);
        }

        private static List<QualifyingParticipation> GetQualifyingParticipations(IQueryable<int> userIds, MetierLmsContext context)
        {
            var qualifyingParticipations = (from p in context.Participants
                join a in context.Activities on p.ActivityId equals a.Id
                join ca in context.CustomerArticles on a.CustomerArticleId equals ca.Id
                join art in context.Articles on ca.ArticleCopyId equals art.Id
                where !a.IsDeleted
                where !p.IsDeleted
                where !art.IsDeleted
                //where p.ActivityId != activity.Id
                where userIds.Contains(p.UserId)
                select
                    new QualifyingParticipation
                    {
                        UserId = p.UserId,
                        StatusCodeValue = p.StatusCodeValue,
                        ProductId = art.ProductId,
                        ArticleTypeId = art.ArticleTypeId,
                        LanguageId = art.LanguageId,
                        CustomerId = ca.CustomerId
                    }).Distinct().ToList();

            return qualifyingParticipations;
        }

    }
}