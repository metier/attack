﻿using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.Queryables
{
    public static class ProductDescriptionQueryableExtensions
    {
        public static IQueryable<ProductDescription> WithAll(this IQueryable<ProductDescription> productDescriptions)
        {
            return productDescriptions.WithLanguage()
                                      .WithProductDescriptionTexts()
                                      .WithTemplateParts()
                                      .WithAttachment()
                                      .WithCustomer()
                                      .WithModifiedBy()
                                      .WithCreatedBy();
        }

        public static IQueryable<ProductDescription> WithProductDescriptionTexts(this IQueryable<ProductDescription> productDescriptions)
        {
            return productDescriptions.Include(pd => pd.ProductDescriptionTexts);
        }

        public static IQueryable<ProductDescription>  WithTemplateParts(this IQueryable<ProductDescription> productDescriptions)
        {
            return productDescriptions.Include("ProductDescriptionTexts.ProductTemplatePart");
        }
        
        public static IQueryable<ProductDescription> WithLanguage(this IQueryable<ProductDescription> productDescriptions)
        {
            return productDescriptions.Include(pd => pd.Language);
        }

        public static IQueryable<ProductDescription> WithCustomer(this IQueryable<ProductDescription> productDescriptions)
        {
            return productDescriptions.Include(pd => pd.Customer);
        }

        public static IQueryable<ProductDescription> WithAttachment(this IQueryable<ProductDescription> productDescriptions)
        {
            return productDescriptions.Include(pd => pd.Attachment);
        }

        public static IQueryable<ProductDescription> WithModifiedBy(this IQueryable<ProductDescription> productDescriptions)
        {
            return productDescriptions.Include(pd => pd.ModifiedBy);
        }

        public static IQueryable<ProductDescription> WithCreatedBy(this IQueryable<ProductDescription> productDescriptions)
        {
            return productDescriptions.Include(pd => pd.CreatedBy);
        }
    }
}