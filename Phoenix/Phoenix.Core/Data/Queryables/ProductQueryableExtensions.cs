﻿using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.Queryables
{
    public static class ProductQueryableExtensions
    {
        public static IQueryable<Product> WithAll(this IQueryable<Product> products)
        {
            return products.WithProductType()
                                      .WithProductCategory();
        }

        public static IQueryable<Product> WithProductType(this IQueryable<Product> products)
        {
            return products.Include(pd => pd.ProductType);
        }

        public static IQueryable<Product> WithProductCategory(this IQueryable<Product> products)
        {
            return products.Include(p => p.ProductCategory);
        }
    }
}