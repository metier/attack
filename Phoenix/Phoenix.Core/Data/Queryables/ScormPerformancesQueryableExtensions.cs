﻿using System.Linq;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.Queryables
{
    public static class ScormPerformancesQueryableExtensions
    {
        public static ScormPerformance FindByUserIdAndRocId(this IQueryable<ScormPerformance> performances, int userId, int rcoId)
        {
            return performances.FirstOrDefault(p => p.UserId == userId && p.RcoId == rcoId);
        }
    }
}