﻿using System.Linq;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.Queryables
{
    public static class ScormAttemptsQueryableExtensions
    {
        public static ScormAttempt FindMostRecent(this IQueryable<ScormAttempt> attempts, int performanceId)
        {
            return attempts.Where(a => a.PerformanceId == performanceId).OrderByDescending(o => o.StartTime).FirstOrDefault();
        }
        
        public static IQueryable<ScormAttempt> IsActive(this IQueryable<ScormAttempt> attempts)
        {
            return attempts.Where(a => a.InternalState == ScormAttemptStatus.Active);
        }
    }
}