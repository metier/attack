﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.Enums;
using Newtonsoft.Json;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Mails")]
    public class Mail
    {
        public int Id { get; set; }
        public MessageType? MessageType { get; set; }
        public DateTime Created { get; set; }
        public string RecipientEmail { get; set; }
        public string Subject { get; set; }
        public int? UserId { get; set; }
        public int? ParticipantId { get; set; }
        public int? ActivityId { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }

        [JsonIgnore]
        public SerializedMailMessage SerializedMailMessage { get; set; }
        [JsonIgnore]
        public List<MailStatus> MailStatuses { get; set; }
    }
}