﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("ParticipantInfoDefinitions")]
    public class ParticipantInfoDefinition : EntityBase
    {
        public ParticipantInfoDefinition()
        {
            AcceptedEmailAddresses = new List<AcceptedEmailAddress>();
        }

        public bool? IsUserCanCreateAccount { get; set; }
        public string CustomerPortalUrl { get; set; }
        public string CustomerPortalFriendlyName { get; set; }        
        public bool? IsShowPricesToUserOnPortal { get; set; }
        public CompetitionModeTypes? CompetitionMode { get; set; }
        public bool? IsAllowUsersToSelfEnrollToCourses { get; set; }
        public bool? IsAllowUsersToSelfEnrollToExams { get; set; }
        public bool? IsCustomerMailAddressParticipantMailAddress { get; set; }
        public string EmailConfirmationAddress { get; set; }
        public string TermsAndConditionsText { get; set; }
        
        public List<AcceptedEmailAddress> AcceptedEmailAddresses { get; set; }
    }
}