﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("AcceptedEmailAddresses")]
    public class AcceptedEmailAddress : EntityBase
    {
        public string Domain { get; set; }
        public bool IsDeleted { get; set; }

        public bool IsEmptyNew()
        {
            return Id == 0 &&
                   string.IsNullOrEmpty(Domain);
        }
    }
}