﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.ViewEntities;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Questions")]
    public class Question : EntityBase
    {
        public Question()
        {
            QuestionOptions = new List<QuestionOption>();
            Rcos = new List<Rco>();
            Tags = new List<Tag>();
            Products = new List<Product>();
        }

        public int? BasedOnQuestionId { get; set; }

        public int LanguageId { get; set; }
        public Language Language { get; set; }
        public int? CustomerId { get; set; }
        public string Type { get; set; }
        public string Text { get; set; }
        public QuestionDifficulties Difficulty { get; set; }
        public QuestionStatuses Status { get; set; }
        public bool IsOptionsRandomized { get; set; }
        public bool? CorrectBoolAnswer { get; set; }
        public string CorrectTextAnswer { get; set; }
        public int Points { get; set; }
        public string Author { get; set; }
        
        public DateTime Created { get; set; }
        public int CreatedById { get; set; }

        public DateTime? Modified { get; set; }
        public int? ModifiedById { get; set; }

        public DateTime? LastUsed { get; set; }

        public List<QuestionOption> QuestionOptions { get; set; }
        public List<Rco> Rcos { get; set; }
        public List<Tag> Tags { get; set; }
        public List<Product> Products { get; set; }
    }
}