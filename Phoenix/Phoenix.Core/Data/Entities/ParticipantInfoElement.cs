﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("ParticipantInfoElements")]
    public class ParticipantInfoElement : EntityBase
    {
        public int ParticipantId { get; set; }
        public string InfoText { get; set; }
        public int LeadingTextId { get; set; }
        public LeadingText LeadingText { get; set; }
    }
}