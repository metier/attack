﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Utilities.Attributes;
using Newtonsoft.Json;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Exams")]
    public class Exam : EntityBaseInt64
    {
        public Exam()
        {
            Questions = new List<Question>();
            Answers = new List<Answer>();
        }

        public bool IsSubmitted { get; set; }
        public int ParticipantId { get; set; }
        [DateTimeKind(DateTimeKind.Utc)]
        public DateTime Created { get; set; }
        public DateTime? SubmittedAt { get; set; }
        
        public int? TotalScore { get; set; }
        public bool IsTotalScoreOverridden { get; set; }
        
        public User ModifiedBy { get; set; }
        public int? ModifiedById { get; set; }
        public DateTime? Modified { get; set; }
        
        [NotMapped, JsonIgnore]
        public bool IsTotalScoreCalculated { get { return !IsTotalScoreOverridden; } }
        
        public List<Question> Questions { get; set; }
        public List<Answer> Answers { get; set; }
    }
}