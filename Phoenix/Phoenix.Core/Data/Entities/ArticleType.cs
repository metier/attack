﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("ArticleTypes")]
    public class ArticleType : EntityBase
    {
        public string Name { get; set; }
        public int? GroupId { get; set; }
        public string CharCode { get; set; }
    }
}