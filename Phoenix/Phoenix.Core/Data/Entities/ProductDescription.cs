﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("ProductDescriptions")]
    public class ProductDescription : EntityBase
    {
        public ProductDescription()
        {
        }

        public string Title { get; set; }

        public List<ProductDescriptionText> ProductDescriptionTexts { get; set; }
        
        public Customer Customer { get; set; }
        public int? CustomerId { get; set; }

        public Language Language { get; set; }
        public int LanguageId { get; set; }

        public int ProductId { get; set; }

        public int? AttachmentId { get; set; }
        public Attachment Attachment { get; set; }

        public bool IsDeleted { get; set; }
        
        public DateTime Created { get; set; }
        public int? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public DateTime LastModified { get; set; }
        public int? ModifiedById { get; set; }
        public User ModifiedBy { get; set; }
    }
}

