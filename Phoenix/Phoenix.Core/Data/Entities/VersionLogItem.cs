﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("VersionLogItems")]
    public class VersionLogItem : IdentifiableEntity
    {
        public VersionLogItem()
        {
        }

        public string InternalReference { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DeploymentDate { get; set; }

        [NotMapped]
        public bool IsNew => DeploymentDate > DateTime.Now.AddDays(-14);
    }
}