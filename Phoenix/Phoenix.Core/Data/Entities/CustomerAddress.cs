﻿using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("CustomerAddresses")]
    public class CustomerAddress : TimeTrackingEntityBase
    {
        public string AddressStreet1 { get; set; } 
        public string AddressStreet2 { get; set; } 
        public string ZipCode { get; set; } 
        public string City { get; set; } 
        public string Country { get; set; }
        public AddressType AddressType { get; set; }
        public Customer Customer { get; set; }    
    }
}