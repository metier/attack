﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("ProductCategories")]
    public class ProductCategory : EntityBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ProductCodePrefix { get; set; }
    }
}