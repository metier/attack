﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Activities")]
    public class Activity : EntityBase
    {
        public Activity()
        {
            Resources = new List<Resource>();
            Attachments = new List<Attachment>();
            ActivityPeriods = new List<ActivityPeriod>();
            Participants = new List<Participant>();
            ActivityExamCourses = new List<ActivityExamCourse>();
        }

        public string Name { get; set; }
        public int CustomerArticleId { get; set; }
        public CustomerArticle CustomerArticle { get; set; }
        public int? RcoCourseId { get; set; }
        public int? RcoExamContainerId { get; set; }
        
        public DateTime? VisibleForParticipants { get; set; }
        public DateTime? EarliestInvoiceDate { get; set; }
        
        public int? MinParticipants { get; set; }
        public int? MaxParticipants { get; set; }
        
        public decimal? UnitPrice { get; set; }
        public decimal? FixedPrice { get; set; }
        public int? CurrencyCodeId { get; set; }

        public bool FixedPriceNotBillable { get; set; }
        public bool UnitPriceNotBillable { get; set; }

        public int? CaseAttachmentId { get; set; }
        public Attachment CaseAttachment { get; set; }

        public long? Duration { get; set; }
        public DateTimeOffset? EndDate { get; set; }

        public long? CompletionTargetDuration { get; set; }
        public DateTime? CompletionTargetDate { get; set; }

        public List<ActivityOrderLine> ActivityOrderLines { get; set; }
        public List<Resource> Resources { get; set; }
        public List<Attachment> Attachments { get; set; }
        public List<ActivityPeriod> ActivityPeriods { get; set; }
        public List<Participant> Participants { get; set; }
        public List<ActivityExamCourse> ActivityExamCourses { get; set; }
        public List<EnrollmentCondition> EnrollmentConditions { get; set; }

        // Timezone name as represented by moment.js (example: "europe_oslo")
        public string MomentTimezoneName { get; set; }

        public DateTime? EnrollmentConfirmationDate { get; set; }
        public bool? SendProgressAutomail { get; set; }
        public bool? SendReminderAutomail { get; set; }
        public bool? SendExamSubmissionAutomail { get; set; }
        public bool? SendEnrollmentConfirmationAutomail { get; set; }

        public int? ActivityAutomailTextId { get; set; }
        public ActivityAutomailText ActivityAutomailText { get; set; }

        public DateTime Created { get; set; }
        public DateTime LastModified { get; set; }
        public bool IsDeleted { get; set; }

        public int? EducationalPartnerResourceId { get; set; }

        public int? NumberOfQuestions{ get; set; }
        public int? NumberOfQuestionsPerPage { get; set; }
        public int? EasyQuestionCount{ get; set; }
        public int? MediumQuestionCount{ get; set; }
        public int? HardQuestionCount{ get; set; }
        public int? EasyCustomerSpecificCount{ get; set; }
        public int? MediumCustomerSpecificCount{ get; set; }
        public int? HardCustomerSpecificCount{ get; set; }
        public string ECTS { get; set; }
		public string EnrolledUserActivityInformation { get; set; }
		public bool HideActivityDates { get; set; }
        public string EnrolledUserActivityExpiredInformation { get; set; }
    }
}