﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("CoursePrograms")]
    public class CourseProgram : EntityBase
    {
        public CourseProgram()
        {
            Steps = new List<CourseStep>();
        }

        public int CustomerId { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public int? EducationalPartnerResourceId { get; set; }

        public List<CourseStep> Steps { get; set; }
    }
}