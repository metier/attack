﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("ProductTypes")]
    public class ProductType : EntityBase
    {
        public string Name { get; set; }
        
        public List<ProductTemplatePart> ProductTemplateParts { get; set; }
    }
}