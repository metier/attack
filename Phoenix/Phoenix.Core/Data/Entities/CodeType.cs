﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("CodeTypes")]
    public class CodeType : EntityBase
    {
        public string Description { get; set; }
        public string Name { get; set; }

        public List<Code> Codes { get; set; }
    }
}