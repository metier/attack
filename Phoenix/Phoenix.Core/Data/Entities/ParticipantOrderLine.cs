﻿using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Orders_Participants")]
    public class ParticipantOrderLine : EntityBase
    {
        public Order Order { get; set; }

        //NB: Adding JsonIgnore to this to avoid a (sort of) cartesian product in the resulting Json-string that NewtonSoft JsonSerializer generates.
        [JsonIgnore]                            
        public Participant Participant { get; set; }

        public decimal? Price { get; set; }
        public int? CurrencyCodeId { get; set; }
        public string OnlinePaymentReference { get; set; }
        public int ActivityId { get; set; }
        public string ActivityName { get; set; }
        public string InfoText { get; set; }

    }
}