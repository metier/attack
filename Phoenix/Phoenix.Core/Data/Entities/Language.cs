﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Languages")]
    public class Language
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public string Identifier { get; set; }
    }
}