﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Distributors")]
    public class Distributor : EntityBase
    {
        public string Name { get; set; }
        public string ContactEmail { get; set; }
        public string LoginFormUrl { get; set; }
        public string PasswordResetFormUrl { get; set; }
        public string AutomailSender { get; set; }
        public string LogoUrl { get; set; }

        [NotMapped]
        public bool HasDefaultInvoiceProvider
        {
            //No good! Magic numbers in two separate places - check out class Metier.Phoenix.Api.Services.ExternalCustomers.ExternalCustomerProviderResolver
            get { return !(Id == 1195 || Id == 1194); }
        }

		[NotMapped]
		public bool SendSeparatePasswordResetEmail
		{			
			get { return Id == 10000; }
		}
	}
}