﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.Enums;
using Newtonsoft.Json;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Orders")]
    public class Order : EntityBase
    {
        public int? CoordinatorId { get; set; }
        public string YourOrder { get; set; }
        public string YourRef { get; set; }
        public string OurRef { get; set; }
        public int? OrderNumber { get; set; }
        public int? InvoiceNumber { get; set; }
        public OrderStatus Status { get; set; }
        public string Title { get; set; }
        public DateTime LastModified { get; set; }
        public DateTime? OrderDate { get; set; }
        public bool IsCreditNote { get; set; }
        public int? CreditedOrderId { get; set; }
        public string WorkOrder { get; set; }

        public List<ParticipantOrderLine> ParticipantOrderLines { get; set; }
        public List<ActivityOrderLine> ActivityOrderLines { get; set; }

        public Customer Customer { get; set; }
        public int CustomerId { get; set; }

        [JsonIgnore]
        [NotMapped]
        public string GroupByKey { get; set; }

        [JsonIgnore]
        [NotMapped]
        public bool IsFullCreditNote { get; set; }

    }
}