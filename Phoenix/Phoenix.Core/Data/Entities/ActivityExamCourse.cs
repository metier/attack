﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Activity_ExamCourses")]
    public class ActivityExamCourse : EntityBase
    {
        public int ActivityId { get; set; }
        public int CourseRcoId { get; set; }
    }
}