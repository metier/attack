﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("ActivityAutomailTexts")]
    public class ActivityAutomailText : EntityBase
    {
        public string Text { get; set; }
    }
}