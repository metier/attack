﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("UserInfoElements")]
    public class UserInfoElement : EntityBase
    {
        public int UserId { get; set; }
        public User User { get; set; }

        public string InfoText { get; set; }

        public int LeadingTextId { get; set; }
        public LeadingText LeadingText { get; set; }
    }
}