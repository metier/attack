﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Answers")]
    public class Answer : EntityBaseInt64
    {
        public Answer()
        {
            OptionsAnswers = new List<QuestionOption>();
        }

        public long ExamId { get; set; }
        public int QuestionId { get; set; }
        public bool IsMarkedForReview { get; set; }
        public bool? BoolAnswer { get; set; }
        public int Points { get; set; }

        public List<QuestionOption> OptionsAnswers { get; set; }
    }
}