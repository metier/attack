﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Articles")]
    public abstract class Article : EntityBase
    {
        protected Article()
        {
            Attachments = new List<Attachment>();
            ArticleExamCourses = new List<ArticleExamCourse>();
        }

        public virtual string ArticlePath
        {
            get
            {
                if (Product == null || Product.ProductCategory == null || Language == null || ArticleType == null)
                {
                    return "";
                }
                return string.Format("{0}{1}-{2}", Product.ProductPath, ArticleType.CharCode.TrimEnd().ToUpper(), Language.Identifier.ToUpper());
            }
        }

        public virtual string ArticleCode
        {
            get
            {
                if (Product == null || Product.ProductCategory == null || ArticleType == null)
                {
                    return "";
                }
                return string.Format("{0}{1}", Product.ProductPath, ArticleType.CharCode.ToUpper());
            }
        }

        public string Title { get; set; }
        
        public DateTime Created { get; set; }
        public int? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public DateTime LastModified { get; set; }
        public int? ModifiedById { get; set; }
        public User ModifiedBy { get; set; }

        public bool IsUserCompetenceInfoRequired { get; set; }
        public bool IsDeleted { get; set; }

        public int ProductId { get; set; }
        public Product Product { get; set; }

        public int LanguageId { get; set; }
        public Language Language { get; set; }
        public int? ProductDescriptionId { get; set; }
        
        public int? StatusCodeId { get; set; }
        public Code StatusCode { get; set; }
        
        public int ArticleTypeId { get; set; }
        public ArticleType ArticleType { get; set; }
        
        public List<Attachment> Attachments { get; set; }
        public List<ArticleExamCourse> ArticleExamCourses { get; set; }
		public string EnrolledUserActivityInformation { get; set; }
        public string EnrolledUserActivityExpiredInformation { get; set; }
    }

    [Table("Article_Elearnings")]
    public class ElearningArticle : Article
    {
        public string PDU { get; set; }
        public string ActivityNumber { get; set; }
        public string ActivityName { get; set; }
        public string ProviderNumber { get; set; }
        public string ProviderName { get; set; }
        public int? RcoCourseId { get; set; }
    }

    [Table("Article_Classrooms")]
    public class ClassroomArticle : Article
    {
        public string PDU { get; set; }
    }

    [Table("Article_Seminars")]
    public class SeminarArticle : Article
    {
        public string PDU { get; set; }
    }

    [Table("Article_Webinars")]
    public class WebinarArticle : Article
    {
        public string PDU { get; set; }
    }

    [Table("Article_Exams")]
    public class ExamArticle : Article
    {
        public long Duration { get; set; }
        public string ECTS { get; set; }
        public int NumberOfQuestions { get; set; }
        public int? NumberOfQuestionsPerPage { get; set; }
        public ExamTypes? ExamType { get; set; }
        public int? EasyQuestionCount { get; set; }
        public int? MediumQuestionCount { get; set; }
        public int? HardQuestionCount { get; set; }
        public int? EasyCustomerSpecificCount { get; set; }
        public int? MediumCustomerSpecificCount { get; set; }
        public int? HardCustomerSpecificCount { get; set; }
        public ExamResultResponseTypes? ExamResultResponseType { get; set; }
        public decimal? MasteryScore { get; set; }
        public string NoResultResponse { get; set; }
        public string BelowRequiredScoreResponse { get; set; }
        public string AboveRequiredScoreResponse { get; set; }
    }

    [Table("Article_CaseExams")]
    public class CaseExamArticle : Article
    {
        public long? Duration { get; set; }
        public string ECTS { get; set; }

        public int? CaseAttachmentId { get; set; }
        public Attachment CaseAttachment { get; set; }
    }

    [Table("Article_Others")]
    public class OtherArticle : Article
    {
    }

    [Table("Article_Mockexams")]
    public class MockexamArticle : Article
    {
        public int? RcoExamContainerId { get; set; }
    }

    [Table("Article_Subscriptions")]
    public class SubscriptionArticle : Article
    {
    }
}