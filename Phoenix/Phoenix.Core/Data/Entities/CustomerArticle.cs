﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using Newtonsoft.Json;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("CustomerArticles")]
    public class CustomerArticle : EntityBase
    {
        public int? ExamId { get; set; }
        public CourseExam Exam { get; set; }
        public int? ModuleId { get; set; }
        public CourseModule Module { get; set; }
        public int CustomerId { get; set; }
        public int? EducationalPartnerId { get; set; }
        public int ArticleCopyId { get; set; }
        public int ArticleOriginalId { get; set; }

        [JsonIgnore]
        public Article ArticleCopy { get; set; }
		
		[NotMapped]
        public int? LanguageId
        {
            get { return ArticleCopy == null ? null : (int?)ArticleCopy.LanguageId; }
        }

        [NotMapped]
        public int? ArticleTypeId
        {
            get { return ArticleCopy == null ? null : (int?)ArticleCopy.ArticleTypeId; }
        }

        [NotMapped]
        public int? ProductDescriptionId
        {
            get { return ArticleCopy == null ? null : ArticleCopy.ProductDescriptionId; }
        }

        [NotMapped]
        public int? ProductId
        {
            get { return ArticleCopy == null ? null : (int?)ArticleCopy.ProductId; }
        }

       
        [NotMapped]
        public string ArticlePath
        {
            get { return ArticleCopy == null ? string.Empty : ArticleCopy.ArticlePath; }
        }

		
		// This property is used when creating CustomerArticle. Value is written to ArticleCopy entitys
		// And in some instances, we return the title to the client (ie. for rendering course calendar and user's list of enrolled courses)
		[NotMapped]		
        public string Title
		{
			get {
				if (_Title != null) return _Title;
				return ArticleCopy == null ? null : ArticleCopy.Title;
			}
			set
			{
				_Title = value;
			}
		}
		private string _Title;


		public decimal? FixedPrice { get; set; }
        public decimal UnitPrice { get; set; }
        public int CurrencyCodeId { get; set; }

        public bool FixedPriceNotBillable { get; set; }
        public bool UnitPriceNotBillable { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastModified { get; set; }

        public List<CustomerArticleEnrollmentCondition> EnrollmentConditions { get; set; }

    }
}