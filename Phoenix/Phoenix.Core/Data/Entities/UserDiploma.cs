using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Utilities.Attributes;
using Newtonsoft.Json;

namespace Metier.Phoenix.Core.Data.Entities
{

    [Table("UserDiplomas")]
    public class UserDiploma : EntityBase
    {
        public UserDiploma()
        {
            Created = DateTime.UtcNow;
        }
        public int UserId { get; set; }
        public int FileId { get; set; }
        public string Title { get; set; }
        public DateTime Created { get; set; }
        public Guid? ShareGuid { get; set; }

    }
}