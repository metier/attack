﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("CustomerInvoicings")]
    public class CustomerInvoicing : EntityBase
    {
        public bool BundledBilling { get; set; }
        public int? OrderRefLeadingTextId { get; set; }
        public string OrderRefCustomText { get; set; }
        public int? YourRefLeadingTextId { get; set; }
        public string YourRefCustomText { get; set; }
        public string OurRef { get; set; }
    }
}