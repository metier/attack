﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("ActivitySetShares")]
    public class ActivitySetShare
    {
        public int ActivitySetId { get; set; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}