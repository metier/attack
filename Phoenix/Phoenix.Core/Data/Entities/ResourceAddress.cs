﻿using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("ResourceAddresses")]
    public class ResourceAddress : EntityBase
    {
        public string StreetAddress { get; set; }
        public string StreetAddress2 { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public AddressType AddressType { get; set; }

        public int ResourceId { get; set; }
        public Resource Resource { get; set; }
    }
}