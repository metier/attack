﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Article_ExamCourses")]
    public class ArticleExamCourse : EntityBase
    {
        public int ArticleId { get; set; }
        public int CourseRcoId { get; set; }
    }
}