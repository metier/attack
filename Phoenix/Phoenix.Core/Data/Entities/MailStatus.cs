﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("MailStatuses")]
    public class MailStatus
    {
        public int Id { get; set; }
        [Column("MailStatus")]
        public MailStatusType Status { get; set; }
        public DateTime StatusTime { get; set; }
        public byte[] RowVersion { get; set; }
        public int MailId { get; set; }
    }
}