﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("ProductDescriptionTexts")]
    public class ProductDescriptionText : EntityBase
    {
        public string Text { get; set; }

        public int ProductDescriptionId { get; set; }
        public ProductTemplatePart ProductTemplatePart { get; set; }
        public int ProductTemplatePartId { get; set; }
    }
}