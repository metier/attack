﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Metier.Phoenix.Core.Data.Enums;
using Newtonsoft.Json;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("CustomInformation")]
    public class CustomInformation : EntityBase
    {
        public int EntityId { get; set; }
        public string EntityType { get; set; }
        public string KeyField { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }
        public string Field3 { get; set; }
        public string Field4 { get; set; }
        public string Field5 { get; set; }
    }
}