﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("AuthenticationTokens")]
    public class AuthenticationToken
    {
        [Key]
        public string UserName { get; set; }
        public Guid Token { get; set; }
    }
}