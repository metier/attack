﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("ResourceTypes")]
    public class ResourceTypes : EntityBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}