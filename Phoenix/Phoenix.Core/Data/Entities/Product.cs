﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Products")]
    public class Product : EntityBase
    {
        public virtual string ProductPath
        {
            get
            {
                if (ProductCategory == null)
                {
                    return "";
                }
                return string.Format("{0}{1}", ProductCategory.ProductCodePrefix, ProductNumber);
            }
        }

        public string ProductNumber { get; set; }
        public string Title { get; set; }
        public bool IsActive { get; set; }
        public string ImageRef { get; set; }

        public List<ProductDescription> ProductDescriptions { get; set; }
        
        public ProductType ProductType { get; set; }
        public int ProductTypeId { get; set; }

        public virtual ProductCategory ProductCategory { get; set; }
        public int ProductCategoryId { get; set; }

        [NotMapped]
        public int NumberOfProductDescriptions { get; set; }

        [NotMapped]
        public int NumberOfArticles { get; set; }

        public string GetProductCode()
        {
            if (ProductCategory == null)
            {
                return null;
            }

            return ProductCategory.ProductCodePrefix + ProductNumber;
        }
    }
}