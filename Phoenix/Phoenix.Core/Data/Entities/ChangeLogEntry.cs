﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Business.ChangeLog;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("ChangeLog")]
    public class ChangeLogEntry : EntityBaseInt64
    {
        public ChangeLogEntityTypes EntityType { get; set; }
        public int EntityId { get; set; }
        public ChangeLogTypes Type { get; set; }
        public string Text { get; set; }
        public DateTime Created { get; set; }
        public User User { get; set; }
        public int? UserId { get; set; }
    }
}