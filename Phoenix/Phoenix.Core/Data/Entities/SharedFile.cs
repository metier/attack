﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("SharedFiles")]
    public class SharedFile : EntityBase
    {
        public Guid ShareGuid { get; set; }
        public int FileId { get; set; }
        public File File { get; set; }
    }
}