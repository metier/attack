﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.Interfaces;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("EnrollmentConditions")]
    public class EnrollmentCondition : TimeTrackingEntityBase, IEnrollmentCondition
    {

        public string Title { get; set; }
        public int ActivityId { get; set; }
        public int ProductId { get; set; }
        public int ArticleTypeId { get; set; }
        public int? LanguageId { get; set; }
        public int? CustomerId { get; set; }
        public bool IsStrictRequirement { get; set; }

        public Product Product { get; set; }
        public ArticleType ArticleType { get; set; }

        public virtual Language Language { get; set; }
        public virtual Customer Customer { get; set; }

    }
}