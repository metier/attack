﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Metier.Phoenix.Core.Data.Enums;
using Newtonsoft.Json;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Customers")]
    public class Customer : TimeTrackingEntityBase
    {
        public Customer()
        {
            Addresses = new List<CustomerAddress>();
            MetierContactPersons = new List<MetierContactPerson>();
            CustomerContactPersons = new List<CustomerContactPerson>();
            CustomLeadingTexts = new List<CustomLeadingText>();
        }

        public bool IsArbitrary { get; set; }
        
        public int? ParentId { get; set; }

        public Customer Parent { get; set; }

        // User for arbitrarty customers
        public int? UserId { get; set; }
        
        public string Name { get; set; }
        
        public bool IsRoot { get; set; }
        
        public int DistributorId { get; set; }

        public int? LanguageId { get; set; }

        /// <summary>
        /// Also known as Organization Number (organisasjonsnummer) for Norwegian companies.
        /// </summary>
        public string CompanyRegistrationNumber { get; set; }      

        public string ExternalCustomerId { get; set; }      
        
        public List<CustomerAddress> Addresses { get; set; }

        public List<MetierContactPerson> MetierContactPersons { get; set; }

        public List<CustomerContactPerson> CustomerContactPersons { get; set; }

        public int? CustomerInvoicingId { get; set; }
        public CustomerInvoicing CustomerInvoicing { get; set; }

        public int? ParticipantInfoDefinitionId { get; set; }
        public ParticipantInfoDefinition ParticipantInfoDefinition { get; set; }

        public int? AutomailDefinitionId { get; set; }
        public AutomailDefinition AutomailDefinition { get; set; }

        public List<CustomLeadingText> CustomLeadingTexts { get; set; }

        [JsonIgnore]
        public CustomerAddress InvoicingAddress
        {
            get { return Addresses.FirstOrDefault(a => a.AddressType == AddressType.Invoice); }
        }

        [JsonIgnore]
        public CustomerAddress PostAddress
        {
            get { return Addresses.FirstOrDefault(a => a.AddressType == AddressType.Post); }
        }

		public bool DiplomaShowCustomerLogo { get; set; }
		public bool DiplomaShowCustomSignature { get; set; }
		public bool DiplomaShowElearningLessons { get; set; }
		public string DiplomaCustomSignatureText { get; set; }
		public string DiplomaCustomSignatureTitle { get; set; }
		
		public int? DiplomaLogoAttachmentId { get; set; }
		public Attachment DiplomaLogoAttachment { get; set; }
		public int? DiplomaSignatureAttachmentId { get; set; }
		public Attachment DiplomaSignatureAttachment { get; set; }
        public bool IsMetierPlusCustomer { get; set; }
    }
}