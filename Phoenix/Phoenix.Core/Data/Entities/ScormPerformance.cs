﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("ScormPerformances")]
    public class ScormPerformance : EntityBase
    {
        public int RcoId { get; set; }
        public int UserId { get; set; }
        public int Score { get; set; }
        
        public long? TimeUsedInSeconds { get; set; }
        public DateTime? CompletedDate { get; set; }

        public string Status { get; set; }
        public string OriginalStatus { get; set; }
    }
}