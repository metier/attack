﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("AutomailDefinitions")]
    public class AutomailDefinition : EntityBase
    {
        public bool? IsWelcomeNotification { get; set; }
        public bool? IsProgressNotificationElearning { get; set; }
        public bool? IsExamSubmissionNotification { get; set; }
        public bool? IsEnrollmentNotification { get; set; }
        public bool IsExcludeProgressSchedule { get; set; }
        public bool? IsReminder { get; set; }
        public string AutomailText { get; set; }
    }
}