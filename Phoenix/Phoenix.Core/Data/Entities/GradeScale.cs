﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("GradeScales")]
    public class GradeScale : EntityBase
    {
        public string Ects { get; set; }
        public string Grade { get; set; }
        public decimal MinPercentage { get; set; }
    }
}