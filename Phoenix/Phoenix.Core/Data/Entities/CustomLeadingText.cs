﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("CustomLeadingTexts")]
    public class CustomLeadingText : EntityBase
    {
        public bool IsVisible { get; set; }
        public bool IsVisibleInvoiceLine { get; set; }
        public bool IsGroupByOnInvoice { get; set; }
        public bool IsMandatory { get; set; }
        public string Text { get; set; }

        public Customer Customer { get; set; }
        public int CustomerId { get; set; }

        public LeadingText LeadingText { get; set; }
        public int LeadingTextId { get; set; }
    }
}