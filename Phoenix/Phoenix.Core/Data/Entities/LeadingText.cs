﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("LeadingTexts")]
    public class LeadingText : EntityBase
    {
        public bool IsVisible { get; set; }
        public bool IsVisibleFixed { get; set; }
        public bool IsMandatory { get; set; }
        public bool IsMandatoryFixed { get; set; }
        public string Text { get; set; }

        public List<CustomLeadingText> CustomLeadingTexts { get; set; }
    }
}