﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("HelpTexts")]
    public class HelpText : EntityBase
    {
        public string Section { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
    }
}