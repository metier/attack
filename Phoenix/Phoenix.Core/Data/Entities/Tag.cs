﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Tags")]
    public class Tag : EntityBase
    {
        public string Text { get; set; }
    }
}