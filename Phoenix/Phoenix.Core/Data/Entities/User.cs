﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Users")]
    public class User : EntityBase
    {
        public User()
        {
            UserInfoElements = new List<UserInfoElement>();
        }

        public Customer Customer { get; set; }
        public int CustomerId { get; set; }
        public string MembershipUserId { get; set; }
        public string ExternalUserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string StreetAddress { get; set; }
        public string StreetAddress2 { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public int? PreferredLanguageId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public bool ParticipantNotBillable { get; set; }
        public UserNotificationTypes UserNotificationType { get; set; }
        public DateTime? UserRegistrationNotificationDate { get; set; }
        public bool? IsAllowUserEcts { get; set; }
        public string NoEctsReason { get; set; }

        public bool? IsPasswordGeneratedOnCreate { get; set; }

        public int? AttachmentId { get; set; }
        public Attachment Attachment { get; set; }

        public List<UserInfoElement> UserInfoElements { get; set; }
    }
}