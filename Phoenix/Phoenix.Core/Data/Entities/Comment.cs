﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Comments")]
    public class Comment : EntityBase
    {
        public string Entity { get; set; }
        public int EntityId { get; set; }
        [Column("Comment")]
        public string CommentText { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? LastModified { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
        public bool IsDeleted { get; set; }
    }
}