﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("CourseExams")]
    public class CourseExam : EntityBase
    {
        public string Name { get; set; }
        public int Order { get; set; }
        public int CourseStepId { get; set; }
        public CourseStep CourseStep { get; set; }
        public List<CustomerArticle> CustomerArticles { get; set; }
    }
}