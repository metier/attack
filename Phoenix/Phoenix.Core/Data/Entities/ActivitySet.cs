﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.ViewEntities;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("ActivitySets")]
    public class ActivitySet : EntityBase//, Interfaces.ActivitySet
    {
        public ActivitySet()
        {
            Activities = new List<Activity>();
            Enrollments = new List<Enrollment>();
            ActivitySetShares = new List<ActivitySetShare>();
        }

        public Customer Customer { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public bool IsPublished { get; set; }
        public DateTime? PublishFrom { get; set; }
        public DateTime? PublishTo { get; set; }
        public DateTime? EnrollmentFrom { get; set; }
        public DateTime? EnrollmentTo { get; set; }
        public bool IsUnenrollmentAllowed { get; set; }
        public DateTime? UnenrollmentDeadline { get; set; }
        public int? CourseProgramPriority { get; set; }

        public List<Activity> Activities { get; set; }
        public List<Enrollment> Enrollments { get; set; }
        public List<ActivitySetShare> ActivitySetShares { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastModified { get; set; }
        [NotMapped]
        public int? AvailableSpots { get; set; }

    }
}