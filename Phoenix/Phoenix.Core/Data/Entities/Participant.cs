﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Participants")]
    public class Participant : EntityBase
    {
        public Participant()
        {
            ParticipantInfoElements = new List<ParticipantInfoElement>();
            ParticipantOrderLines = new List<ParticipantOrderLine>();
            Attachments = new List<Attachment>();
        }

        public int CustomerId { get; set; }
        public int ActivityId { get; set; }
        public int ActivitySetId { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
        public string StatusCodeValue { get; set; }
        public string Result { get; set; }
        public int? CurrencyCodeId { get; set; }
        public decimal? UnitPrice { get; set; }
        public DateTime? EarliestInvoiceDate { get; set; }
        public long? AdditionalTime { get; set; }
        public bool IsNotBillable { get; set; }
        public string OnlinePaymentReference { get; set; }
        public string ExamGrade { get; set; }
        public DateTime? ExamGradeDate { get; set; }
        public int? ExamGraderUserId { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? GradingListDate { get; set; }
        public DateTime Created { get; set; }
        
        public List<ParticipantInfoElement> ParticipantInfoElements { get; set; }
        public List<Attachment> Attachments { get; set; }
        public DateTime? CompletedDate { get; set; }
        public int? ExaminerResourceId { get; set; }
        public UserResource ExaminerResource { get; set; }

        public int EnrollmentId { get; set; }

        [NotMapped]
        public string QualificationStatus { get; set; }

        public List<ParticipantOrderLine> ParticipantOrderLines { get; set; }

        /// <summary>
        /// Returns infotext to be displayed on the orderlines for participants.
        /// The following objects must be loaded on the participant-object, before invoking this method:
        ///     - User
        ///     - ParticipantInfoElements
        ///     - ParticipantInfoElements.CustomLeadingText
        /// </summary>
        /// <returns></returns>
        public string GetInfoTexts()
        {
            if (User == null)
                throw new ValidationException("User-object is null. The user must be loaded before invoking this method.");

            if (ParticipantInfoElements == null)
                throw new ValidationException("ParticipantInfoElements-object is null. The ParticipantInfoElements must be loaded before invoking this method.");

            if (ParticipantInfoElements.Any(element => element.LeadingText == null))
            {
                throw new ValidationException("The LeadingText-object on one or more ParticipantInfoElements is null. All CustomLeadingTexts must be loaded before invoking this method.");
            }

            var visibleCustomLeadingTexts = ParticipantInfoElements.Select(element => element.LeadingText.CustomLeadingTexts.FirstOrDefault(clt => clt.CustomerId == CustomerId && clt.IsVisibleInvoiceLine)).Where(c=> c != null).ToList();

            var infoElements = ParticipantInfoElements.Where(p => visibleCustomLeadingTexts.Any(clt => clt.LeadingTextId == p.LeadingTextId) && !string.IsNullOrEmpty(p.InfoText)).ToList();

            string fullName = string.Concat(User.FirstName, " ", User.LastName);
            string infoText = infoElements.Any() ? string.Join(", ", infoElements.Select(p => visibleCustomLeadingTexts.FirstOrDefault(clt=> clt.LeadingTextId == p.LeadingTextId).Text + ": " + p.InfoText)) : string.Empty;

            if (String.IsNullOrWhiteSpace(infoText))
                return fullName;

            return string.Concat(fullName, ", ", infoText);

        }
    }
}