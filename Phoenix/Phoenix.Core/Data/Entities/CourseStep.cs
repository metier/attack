﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("CourseSteps")]
    public class CourseStep : EntityBase
    {
        public CourseStep()
        {
            Exams = new List<CourseExam>();
            Modules = new List<CourseModule>();
        }

        public string Name { get; set; }
        public int Order { get; set; }
        public int CourseProgramId { get; set; }

        public List<CourseExam> Exams { get; set; }
        public List<CourseModule> Modules { get; set; }        
    }
}