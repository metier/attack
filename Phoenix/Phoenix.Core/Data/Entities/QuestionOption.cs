﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("QuestionOptions")]
    public class QuestionOption : EntityBase
    {
        public int QuestionId { get; set; }
        public string Text { get; set; }
        public bool IsCorrect { get; set; }
    }
}