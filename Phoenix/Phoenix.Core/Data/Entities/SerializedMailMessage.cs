﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("SerializedMailMessages")]
    public class SerializedMailMessage
    {
        public int Id { get; set; }
        public string SerializedMessage { get; set; }

        [JsonIgnore]
        public Mail Mail { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; } 
    }
}