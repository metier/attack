﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Attachments")]
    public class Attachment : EntityBase
    {
        public Attachment()
        {
            Created = DateTime.UtcNow;
        }

        public int FileId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
		public Guid? ShareGuid { get; set; }

	}
}