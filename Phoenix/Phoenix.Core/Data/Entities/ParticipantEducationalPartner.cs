﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("ParticipantEducationalPartners")]
    public class ParticipantEducationalPartner : EntityBase
    {
        public int EducationalPartnerId { get; set; }
        public int ParticipantInfoDefinitionId { get; set; }
    }
}