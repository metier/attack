﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Codes")]
    public class Code : EntityBase
    {
        public string Value { get; set; }
        public string Description { get; set; }
        public int CodeTypeId { get; set; }
    }
}