﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("PasswordResetRequests")]
    public class PasswordResetRequest : EntityBase
    {
        public Guid ResetToken { get; set; }
        public int UserId { get; set; }
        public DateTime Created { get; set; }
    }
}