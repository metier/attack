﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("ScormAttempts")]
    public class ScormAttempt : EntityBase
    {
        public Guid SessionId { get; set; }
        public int UserId { get; set; }
        public string SuspendData { get; set; }
        public string Status { get; set; }
        public int? Score { get; set; }
        public DateTime StartTime { get; set; }

        [JsonIgnore]
        public int PerformanceId { get; set; }
        
        [JsonIgnore]
        public string InternalState { get; set; }

        [JsonIgnore]
        public long TimeUsedInSeconds { get; set; }        
    }
}