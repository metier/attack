﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Enrollments")]
    public class Enrollment : EntityBase
    {
        public int UserId { get; set; }

        public int ActivitySetId { get; set; }
        public bool IsCancelled { get; set; }
    }
}