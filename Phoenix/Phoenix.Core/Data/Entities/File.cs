﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Files")]
    public class File
    {
        public int Id { get; set; }
        public string Filename { get; set; }
        public long Size { get; set; }
        public string ContentType { get; set; }
        public string RelativeFilePath { get; set; }
    }
}