﻿using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("MetierContactPersons")]
    public class MetierContactPerson : EntityBase
    {
        public Customer Customer { get; set; }
        public int UserId { get; set; }
        public MetierContactType? ContactType { get; set; }
        public bool IsDeleted { get; set; }
    }
}