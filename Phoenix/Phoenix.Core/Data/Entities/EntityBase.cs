﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Metier.Phoenix.Core.Data.Entities
{
    public abstract class IdentifiableEntity
    {
        public int Id { get; set; }
    }

    public abstract class EntityBase : IdentifiableEntity
    {
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }

    public abstract class TimeTrackingEntityBase : EntityBase
    {
        public DateTime Created { get; set; }
        public DateTime LastModified { get; set; }
    }

    public abstract class IdentifiableEntityInt64
    {
        public long Id { get; set; }
    }

    public abstract class EntityBaseInt64 : IdentifiableEntityInt64
    {
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}