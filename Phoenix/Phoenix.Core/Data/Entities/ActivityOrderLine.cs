﻿using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Orders_Activities")]
    public class ActivityOrderLine : EntityBase
    {

        public Order Order { get; set; }
        public Activity Activity { get; set; }
        public decimal? Price { get; set; }
        public int? CurrencyCodeId { get; set; }
        public string ActivityName { get; set; }

        [JsonIgnore]
        [NotMapped]
        public string ArticlePath { get; set; }
    }
}