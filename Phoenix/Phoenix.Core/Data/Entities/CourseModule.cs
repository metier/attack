﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("CourseModules")]
    public class CourseModule : EntityBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
        public int CourseStepId { get; set; }
        public CourseStep CourseStep { get; set; }
        public List<CustomerArticle> CustomerArticles { get; set; }
    }
}