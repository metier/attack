﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("CustomerContactPersons")]
    public class CustomerContactPerson : EntityBase
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string Email { get; set; }
        public string TelephoneNumber { get; set; }
        public string ResponsibleFor { get; set; }
        public bool IsDeleted { get; set; }

        public Customer Customer { get; set; }

        public bool IsEmptyNew()
        {
            return Id == 0 &&
                   string.IsNullOrEmpty(Name) &&
                   string.IsNullOrEmpty(Title) &&
                   string.IsNullOrEmpty(Email) &&
                   string.IsNullOrEmpty(TelephoneNumber) &&
                   string.IsNullOrEmpty(ResponsibleFor);
        }
    }
}