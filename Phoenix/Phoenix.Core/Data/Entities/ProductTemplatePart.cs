﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("ProductTemplateParts")]
    public class ProductTemplatePart : EntityBase
    {
        public int Order { get; set; }
        public string Title { get; set; }

        public int ProductTypeId { get; set; }        
        public int LanguageId { get; set; }
        public int? CharLimit { get; set; }
    }
}