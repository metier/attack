﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("Resources")]
    public class Resource : EntityBase
    {
        public Resource()
        {
            Addresses = new List<ResourceAddress>();
            Attachments = new List<Attachment>();
        }

        public string Name { get; set; }
        public string Contact { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public decimal? Amount { get; set; }
        public string Description { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? LastModified { get; set; }

        public List<ResourceAddress> Addresses { get; set; }
        public List<Attachment> Attachments { get; set; }

        public int ResourceTypeId { get; set; }
        public ResourceTypes ResourceType { get; set; }

        public int? CurrencyId { get; set; }
        public Code Currency { get; set; }

        public EducationPartnerSchemaTypes? EducationPartnerSchemaType { get; set; }
    }

    [Table("Resource_Users")]
    public class UserResource : Resource
    {
        public User User { get; set; }
        public int UserId { get; set; }
    }
}