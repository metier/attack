using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Utilities.Attributes;
using Newtonsoft.Json;

namespace Metier.Phoenix.Core.Data.Entities
{
    [Table("UserCompetences")]
    public class UserCompetence : EntityBase
    {
        public UserCompetence()
        {
            Attachments = new List<Attachment>();
        }

        public int UserId { get; set; } 
        
        public string FirstName { get; set; } 
        public string LastName { get; set; } 

        /// <summary>
        /// Formal title: Mr/Mrs/Ms
        /// </summary>
        public string Title { get; set; }
        public string Email { get; set; }
        
        /// <summary>
        /// 11 digit number
        /// </summary>
        [NotMapped]
        public string SocialSecurityNumber { get; set; }

        [JsonIgnore]
        [EncryptedColumn("SocialSecurityNumber", TypeCode.String)]
        public byte[] SocialSecurityNumberEncrypted { get; set; }

        /// <summary>
        /// Should be provided if user doesn't have a SocialSecurityNumber
        /// </summary>
        public DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Two letter country code (ISO3166)
        /// </summary>
        public string CountryOfBirth { get; set; }
        /// <summary>
        /// Two letter country code (ISO3166)
        /// </summary>
        public string Country { get; set; }
        public string Nationality { get; set; }
        public string ZipCode { get; set; }
        public string StreetAddress { get; set; }

        public EducationTypes? EducationType { get; set; }
        public string EducationDescription { get; set; }
        public int? GraduationYear { get; set; }
        
        /// <summary>
        /// Level of education (Bachelor, Master, PhD, etc)
        /// </summary>
        public string EducationLevel { get; set; }

        /// <summary>
        /// Name of the school/college/university
        /// </summary>
        public string InstitutionName { get; set; }

        /// <summary>
        /// Required if EducationType is 'None'
        /// </summary>
        public bool? IsAccreditedPracticalCompetenceSatisfied { get; set; }
        public string PracticalCompetenceDescription { get; set; }
        
        /// <summary>
        /// Free text for additional info
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// List of uploaded files
        /// </summary>
        public List<Attachment> Attachments { get; set; }

        /// <summary>
        /// If user has accepted terms and conditions
        /// </summary>
        public bool IsTermsAccepted { get; set; }

        public DateTime Created { get; set; }
        public DateTime? TermsAcceptedDate { get; set; }

        public string GetSexFromSocialSecurityNumber()
        {
            if (!string.IsNullOrEmpty(SocialSecurityNumber) &&  SocialSecurityNumber.Length == 11)
            {
                return Convert.ToInt32(SocialSecurityNumber[8]) % 2 == 0 ? "F" : "M";
            }
            return string.Empty;
        }

        public DateTime? GetDateOfBirthFromSocialSecurityNumber()
        {
            if (!string.IsNullOrEmpty(SocialSecurityNumber) && SocialSecurityNumber.Length == 11)
            {
                try
                {
                    //It is not possible to 100% determine the century from a SSN, ref.: https://no.wikipedia.org/wiki/F%C3%B8dselsnummer
                    var century =  Convert.ToInt32(SocialSecurityNumber.Substring(6, 1)) >= 5 ? 2000 : 1900;
                    var year = century + Convert.ToInt32(SocialSecurityNumber.Substring(4, 2));

                    if (DateTime.Now.Year - year > 80)  //Assuming that graduates are at most 80 years of age
                        year = year + 100;

                    if (DateTime.Now.Year - year < 16)  //Assuming that graduates are at least 16 years of age
                        year = year - 100;

                    var month = Convert.ToInt32(SocialSecurityNumber.Substring(2, 2));
                    var day = Convert.ToInt32(SocialSecurityNumber.Substring(0, 2));
                    var dateOfBirth = new DateTime(year, month, day);

                    return dateOfBirth;
                }
                catch
                {
                    return null;
                }

            }
            return null;
        }
    }
}