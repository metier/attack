﻿using System;

namespace Metier.Phoenix.Core.Data.Facade
{
    public class ActivityAvailabilityPeriods
    {
        public DateTimeOffset? Start { get; set; }
        public DateTimeOffset? End { get; set; }
    }
}