using System;
using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Data.Facade
{
    public class ParticipationAvailability
    {
        public bool IsAvailable { get; set; }
        public DateTime? AvailabilityStart { get; set; }
        public DateTime? AvailabilityEnd { get; set; }
        public List<ActivityAvailabilityPeriods> ActivityPeriods { get; set; }
		public long? AdditionalTime { get; set; }
	}
}