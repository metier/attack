﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Remoting.Messaging;
using Metier.Phoenix.Core.Business.FileStorage;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using File = Metier.Phoenix.Core.Data.Entities.File;

namespace Metier.Phoenix.Core.Factories.GradingLists
{
    public class CandidateSurveyFactory : ICandidateSurveyFactory
    {
        private readonly IHiMoldeGradingListDocumentFactory _hiMoldeGradingListDocumentFactory;
        private readonly ISkemaGradingListDocumentFactory _skemaGradingListDocumentFactory;
        private readonly IFileStorage _fileStorage;
        private readonly MetierLmsContext _context;
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        public CandidateSurveyFactory(IHiMoldeGradingListDocumentFactory hiMoldeGradingListDocumentFactory, 
            ISkemaGradingListDocumentFactory skemaGradingListDocumentFactory,
            IFileStorage fileStorage,
            MetierLmsContext context)
        {
            _hiMoldeGradingListDocumentFactory = hiMoldeGradingListDocumentFactory;
            _skemaGradingListDocumentFactory = skemaGradingListDocumentFactory;
            _fileStorage = fileStorage;
            _context = context;
        }

        public SharedFile Create(Resource educationalPartner, IList<Participant> participants)
        {
            switch (educationalPartner.EducationPartnerSchemaType)
            {
                case EducationPartnerSchemaTypes.HiMolde:
                {
                    var gradingList = _hiMoldeGradingListDocumentFactory.Create(participants);
                    return CreateSharedXlsxFile(gradingList, string.Format("Sensurliste HIMolde - {0}.xlsx", DateTime.UtcNow));
                }
                    
                case EducationPartnerSchemaTypes.Skema:
                {
                    var gradingList = _skemaGradingListDocumentFactory.Create(participants);
                    return CreateSharedXlsxFile(gradingList, string.Format("Sensurliste Skema - {0}.xlsx", DateTime.UtcNow));
                }
                default:
                    return null;
            }
        }

        private SharedFile CreateSharedXlsxFile(byte[] gradingList, string fileName)
        {
            var file = _fileStorage.CreateFile(fileName, XlsxContentType, new MemoryStream(gradingList));
            var sharedFile = new SharedFile
            {
                ShareGuid = Guid.NewGuid(),
                FileId = file.Id
            };
            _context.SharedFiles.Add(sharedFile);
            _context.SaveChanges();
            return sharedFile;
        }
    }
}