﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Factories.GradingLists
{
    public interface IGradingListDocumentFactory
    {
        byte[] Create(IEnumerable<Participant> participants);
    }
}