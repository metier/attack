﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats.Excel;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Factories.GradingLists.Data;

namespace Metier.Phoenix.Core.Factories.GradingLists
{
    public class HiMoldeGradingListDocumentFactory : IHiMoldeGradingListDocumentFactory
    {
        private readonly IExcelFactory _excelFactory;
        private readonly MetierLmsContext _context;

        public HiMoldeGradingListDocumentFactory(IExcelFactory excelFactory, MetierLmsContext context)
        {
            _excelFactory = excelFactory;
            _context = context;
        }

        public byte[] Create(IEnumerable<Participant> participants)
        {
            var gradingListItems = participants.Select(GetGradingListItemForParticipant).ToList();
            return _excelFactory.CreateDocument(gradingListItems, "Sensurliste");
        }

        private HiMoldeData GetGradingListItemForParticipant(Participant participant)
        {
            var customer = _context.Customers.First(c => c.Id == participant.CustomerId);
            var activity = _context.Activities.First(a => a.Id == participant.ActivityId);

            var userData = _context.Users.Where(u => u.Id == participant.UserId)
	                               .GroupJoin(_context.UserCompetences, u => u.Id, uc => uc.UserId, (u, uc) => new { User = u, UserCompetence = uc })
                                   .First();
            var user = userData.User;
            var userCompetence = userData.UserCompetence.FirstOrDefault() ??  new UserCompetence();

            var sex = userCompetence.GetSexFromSocialSecurityNumber();
            var title = GetTitle(sex);

            var gradingListItem = new HiMoldeData
            {
                Date = participant.GradingListDate,
                Id = user.ExternalUserId    ,
                CustomerName = customer.Name,
                Sex = title,
                FirstName = user.FirstName,
                LastName = user.LastName,
                SocialSecurityNumber = GetSocialSecurityNumber(userCompetence),
                EducationType = userCompetence.EducationType.ToString(),
                InstitutionName = userCompetence.InstitutionName,
                EducationLevel = userCompetence.EducationLevel,
                EducationDescription = userCompetence.EducationDescription,
                GraduationYear = userCompetence.GraduationYear,
                PracticalCompetence = userCompetence.IsAccreditedPracticalCompetenceSatisfied.HasValue ? 
                                      (userCompetence.IsAccreditedPracticalCompetenceSatisfied.Value ? "Ja" : "Nei") :
                                      null,
                PracticalCompetenceDescription = userCompetence.PracticalCompetenceDescription,
                Address = string.Format("{0} {1}", user.StreetAddress, user.StreetAddress2),
                ZipCode = user.ZipCode,
                City = user.City,
                Country = user.Country,
                Grade = participant.ExamGrade,
                Ects = activity.ECTS
            };

            return gradingListItem;
        }

        //Return SocialSecurityNumber if exists, or Date of Birth if not (otherwise an empty string is returned)
        private string GetSocialSecurityNumber(UserCompetence userCompetence)
        {
            if(string.IsNullOrWhiteSpace(userCompetence.SocialSecurityNumber))
                return userCompetence.DateOfBirth?.ToString("dd/MM/yyyy") ?? "";

            return userCompetence.SocialSecurityNumber;
        }

        private static string GetTitle(string sex)
        {
            return sex != null ? (sex == "M" ? "Mr" : "Ms") : null;
        }
    }
}