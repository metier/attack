﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Factories.GradingLists
{
    public interface ICandidateSurveyFactory
    {
        SharedFile Create(Resource educationalPartner, IList<Participant> participants);
    }
}