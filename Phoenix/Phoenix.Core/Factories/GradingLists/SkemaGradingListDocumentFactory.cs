using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats.Excel;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Factories.GradingLists.Data;

namespace Metier.Phoenix.Core.Factories.GradingLists
{
    public class SkemaGradingListDocumentFactory : ISkemaGradingListDocumentFactory
    {
        private readonly IExcelFactory _excelFactory;
        private readonly MetierLmsContext _context;

        public SkemaGradingListDocumentFactory(IExcelFactory excelFactory, MetierLmsContext context)
        {
            _excelFactory = excelFactory;
            _context = context;
        }

        public byte[] Create(IEnumerable<Participant> participants)
        {
            var gradingListItems = participants.Select(GetGradingListItemForParticipant);
            return _excelFactory.CreateDocument(gradingListItems, "Sensurliste");
        }

        private SkemaData GetGradingListItemForParticipant(Participant participant)
        {
            var customer = _context.Customers.First(c => c.Id == participant.CustomerId);
            var userData = _context.Users.Where(u => u.Id == participant.UserId)
                                   .GroupJoin(_context.UserCompetences, u => u.Id, uc => uc.UserId, (u, uc) => new { User = u, UserCompetence = uc })
                                   .First();
            var user = userData.User;
            var userCompetence = userData.UserCompetence.FirstOrDefault() ?? new UserCompetence();
            var searchableUser = _context.SearchableUsers.First(su => su.Id == user.Id);
            var examActivity = _context.Activities.First(a => a.Id == participant.ActivityId);

            var startDate = GetStartDate(participant);
            var endDate = GetEndDate(participant);

            var moduleResults = GetModuleResults(participant, participant.ExamGrade, startDate, endDate);

            var ssnDateOfBirth = userCompetence.GetDateOfBirthFromSocialSecurityNumber();
            var birthDate = userCompetence.DateOfBirth.HasValue
                ? userCompetence.DateOfBirth.Value.ToString("dd.MM.yyyy")
                : (ssnDateOfBirth != null ? ssnDateOfBirth.Value.ToString("dd.MM.yyyy") : string.Empty);

            var gradingListItem = new SkemaData
            {
                Date = participant.GradingListDate,
                Id = user.Id,
                CustomerName = customer.Name,
                Title = userCompetence.Title,
                FirstName = user.FirstName,
                LastName = user.LastName,
                DateOfBirth = birthDate,
                Address = string.Format("{0} {1}", user.StreetAddress, user.StreetAddress2),
                ZipCode = user.ZipCode,
                City = user.City,
                Country = user.Country,
                Email = searchableUser.Email,
                EducationType = userCompetence.EducationType.ToString(),
                InstitutionName = userCompetence.InstitutionName,
                EducationLevel = userCompetence.EducationLevel,
                EducationDescription = userCompetence.EducationDescription,
                GraduationYear = userCompetence.GraduationYear,
                PracticalCompetence = userCompetence.IsAccreditedPracticalCompetenceSatisfied.HasValue ?
                                      (userCompetence.IsAccreditedPracticalCompetenceSatisfied.Value ? "Yes" : "No") :
                                      null,
                PracticalCompetenceDescription = userCompetence.PracticalCompetenceDescription,
                TotalEcts = examActivity.ECTS,
                ModuleResults = moduleResults
            };

            return gradingListItem;
            }

        private List<CustomInformation> GetCustomInformation(int activityId)
        {
            return _context.CustomInformationSet
                .Where(c => c.EntityId == activityId && c.EntityType.Equals("Activity"))
                .OrderBy(c => c.Id)
                .ToList();
        }

        private DateTime? GetStartDate(Participant participant)
        {
            var examActivity = _context.Activities.Include(a => a.ActivityExamCourses)
                                       .First(a => a.Id == participant.ActivityId && !a.IsDeleted);

            var customerArticleArticle = _context.CustomerArticles.Where(ca => ca.Id == examActivity.CustomerArticleId && !ca.IsDeleted)
                                  .Join(_context.Articles, ca => ca.ArticleCopyId, a => a.Id, (ca, a) => a)
                                  .First(a => !a.IsDeleted);

            var examArticle = customerArticleArticle as ExamArticle;
            if (examArticle != null)
            {
                return GetStartDateForExamArticle(participant, examActivity);
            }

            var caseExamArticle = customerArticleArticle as CaseExamArticle;
            if (caseExamArticle != null)
            {
                return GetStartDateForCaseExamArticle(participant);
            }
            return null;
        }

        private DateTime? GetStartDateForCaseExamArticle(Participant participant)
        {
            // If CaseExamArticle
            // - Return earliest started course from user courses on module activities in same step as exam activity
            var courseStep = GetCourseStepWithModules(participant);

            if (courseStep != null)
            {
                var moduleIds = courseStep.Modules.Select(m => m.Id).ToList();
                var earliestParticipation =
                    _context.CustomerArticles
                            .Where(ca => ca.ModuleId.HasValue && moduleIds.Contains(ca.ModuleId.Value) && !ca.IsDeleted)
                            .Join(_context.Activities, ca => ca.Id, a => a.CustomerArticleId, (ca, a) => a)
                            .Where(a => !a.IsDeleted)
                            .Join(_context.Participants, a => a.Id, p => p.ActivityId, (a, p) => p)
                            .OrderBy(p => p.Created)
                            .FirstOrDefault(p => !p.IsDeleted);

                return earliestParticipation != null ? (DateTime?) earliestParticipation.Created : null;
            }
            return null;
        }

        private DateTime? GetStartDateForExamArticle(Participant participant, Activity examActivity)
        {
            if (participant.ActivityId != examActivity.Id)
                throw new ValidationException("Participant is not enrolled to the given activity.");

            //Make sure EnrollmentConditions are loaded
            var enrollmentConditions = examActivity.EnrollmentConditions;
            if (enrollmentConditions == null || enrollmentConditions.Count == 0)
                enrollmentConditions = _context.EnrollmentConditions.Where(ec => ec.ActivityId == examActivity.Id).ToList();

            DateTime? startDate = null;
            foreach (var condition in enrollmentConditions)
            {

                var matchingParticipations = from p in _context.Participants
                    join a in _context.Activities on p.ActivityId equals a.Id
                    join ca in _context.CustomerArticles on a.CustomerArticleId equals ca.Id
                    join art in _context.Articles on ca.ArticleCopyId equals art.Id
                    where !p.IsDeleted && p.UserId == participant.UserId && p.ActivityId != examActivity.Id
                    where art.ProductId == condition.ProductId && art.ArticleTypeId == condition.ArticleTypeId
                    where (condition.CustomerId == null || condition.CustomerId == ca.CustomerId)
                          && (condition.LanguageId == null || condition.LanguageId == art.LanguageId)
                    where !ca.IsDeleted && !a.IsDeleted && !art.IsDeleted
                    select p;

                if (matchingParticipations.Any())
                {
                    var courseStartDate = matchingParticipations.Select(p => p.Created).Max();
                    if (startDate == null || startDate > courseStartDate)
                        startDate = courseStartDate;
                }
            }

            //Note: Will not return start date if no enrollment conditions exist
            return startDate;
        }

        private DateTime? GetEndDate(Participant participant)
        {
            var exam = _context.Exams.FirstOrDefault(e => e.ParticipantId == participant.Id);
            return exam != null ? exam.SubmittedAt : participant.ExamGradeDate;
        }

        private List<ModuleResults> GetModuleResults(Participant participant, string examGrade, DateTime? startDate, DateTime? endDate)
        {
            var items = GetCustomInformation(participant.ActivityId);
            var moduleResultsList = new List<ModuleResults>();

            foreach (var item in items)
            {
                moduleResultsList.Add(new ModuleResults
            {
                    PartnerId = item.KeyField,
                    LocalGrade = examGrade,
                    Description = item.Field1,
                    Ects = item.Field2,
                    StartDate = startDate,
                    EndDate = endDate
                });
        }

            return moduleResultsList;
        }

        public CourseStep GetCourseStepWithModules(Participant participant)
        {
            var customerArticle = _context.Participants.Where(p => p.Id == participant.Id && !p.IsDeleted)
                                          .Join(_context.Activities, p => p.ActivityId, a => a.Id, (p, a) => a).Where(a => !a.IsDeleted)
                                          .Join(_context.CustomerArticles, a => a.CustomerArticleId, ca => ca.Id, (a, ca) => ca).Where(ca => !ca.IsDeleted)
                                          .Include(ca => ca.Exam).First();
            if (customerArticle.Exam == null)
            {
                return null;
            }
            _context.Entry(customerArticle.Exam).Reference(e => e.CourseStep).Load();
            var courseStep = customerArticle.Exam.CourseStep;
            _context.Entry(courseStep).Collection(s => s.Modules).Load();

            return courseStep;
        }
    }
}