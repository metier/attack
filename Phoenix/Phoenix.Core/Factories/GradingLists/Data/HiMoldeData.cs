﻿using System;
using Metier.Phoenix.Core.Business.DocumentProduction.Attributes;

namespace Metier.Phoenix.Core.Factories.GradingLists.Data
{
    public class HiMoldeData
    {
        [DisplayName("Dato")]
        public DateTime? Date { get; set; }

        public string Id { get; set; }
        
        [DisplayName("Kundenavn")]
        public string CustomerName { get; set; }

        [DisplayName("Kjønn")]
        public string Sex { get; set; }

        [DisplayName("Fornavn")]
        public string FirstName { get; set; }

        [DisplayName("Etternavn")]
        public string LastName { get; set; }

        [DisplayName("Fødselsdato/personnr")]
        public string SocialSecurityNumber { get; set; }

        [DisplayName("Utdanningstype")]
        public string EducationType { get; set; }

        [DisplayName("Institusjonsnavn")]
        public string InstitutionName { get; set; }

        [DisplayName("Utdanningsnivå")]
        public string EducationLevel { get; set; }

        [DisplayName("Utdanningsbeskrivelse")]
        public string EducationDescription { get; set; }

        [DisplayName("Utdanningsår")]
        public int? GraduationYear { get; set; }

        // Ja eller nei
        [DisplayName("Realkompetanse")]
        public string PracticalCompetence { get; set; }

        [DisplayName("Realkompetansebeskrivelse")]
        public string PracticalCompetenceDescription { get; set; }

        [DisplayName("Adresse")]
        public string Address { get; set; }

        [DisplayName("Postnummer")]
        public string ZipCode { get; set; }

        [DisplayName("Poststed")]
        public string City { get; set; }

        [DisplayName("Land")]
        public string Country { get; set; }

        [DisplayName("Kode")]
        public string ExamCode { get; set; }

        [DisplayName("Karakter")]
        public string Grade { get; set; }

        [DisplayName("Studiepoeng")]
        public string Ects { get; set; }
    }
}