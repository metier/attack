﻿using System;
using System.Collections.Generic;
using Metier.Phoenix.Core.Business.DocumentProduction.Attributes;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats.Excel;

namespace Metier.Phoenix.Core.Factories.GradingLists.Data
{
    public class SkemaData
    {
        public ExcelRowStyleOptions RowStyleOptions { get; set; }

        public DateTime? Date { get; set; }

        public int Id { get; set; }

        [DisplayName("Mr, Ms")]
        public string Title { get; set; }

        [DisplayName("First name")]
        public string FirstName { get; set; }

        [DisplayName("Last name")]
        public string LastName { get; set; }

        [DisplayName("Birth date")]
        public string DateOfBirth { get; set; }

        public string Address { get; set; }

        [DisplayName("ZIP code")]
        public string ZipCode { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string Email { get; set; }

        [DisplayName("Total credits")]
        public string TotalEcts { get; set; }

        public IList<ModuleResults> ModuleResults{ get; set; }

        [DisplayName("Education type")]
        public string EducationType { get; set; }

        [DisplayName("Institution name")]
        public string InstitutionName { get; set; }

        [DisplayName("Education level")]
        public string EducationLevel { get; set; }

        [DisplayName("Education description")]
        public string EducationDescription { get; set; }

        [DisplayName("Graduation year")]
        public int? GraduationYear { get; set; }

        [DisplayName("Practical competence")]
        public string PracticalCompetence { get; set; }

        [DisplayName("Practical competence description")]
        public string PracticalCompetenceDescription { get; set; }

        [DisplayName("Customer name")]
        public string CustomerName { get; set; }
    }

    public class ModuleResults
    {
        [DisplayName("Module code#")]
        [CountListColumns]
        public string PartnerId { get; set; }

        [DisplayName("Local grade #")]
        [CountListColumns]
        public string LocalGrade { get; set; }

        [DisplayName("Description #")]
        [CountListColumns]
        public string Description { get; set; }

        [DisplayName("Credits #")]
        [CountListColumns]
        public string Ects { get; set; }

        [DisplayName("Start date ")]
        [CountListColumns]
        public DateTime? StartDate { get; set; }

        [DisplayName("End date ")]
        [CountListColumns]
        public DateTime? EndDate { get; set; }

        [DisplayName("Start date #")]
        [CountListColumns]
        public DateTime? StartDateNo { get; set; }

        [DisplayName("End date #")]
        [CountListColumns]
        public DateTime? EndDateNo { get; set; }

        public double EctsAsInteger()
        {
            double result;
            var sucess = double.TryParse(Ects, out result);

            return sucess ? result : 0;
        }
    }
}