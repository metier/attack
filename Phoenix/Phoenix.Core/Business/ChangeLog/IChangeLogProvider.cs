﻿using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Core.Business.ChangeLog
{
    public interface IChangeLogProvider
    {
        ChangeLogEntry Write(int entityId, ChangeLogEntityTypes entityType, ChangeLogTypes type, string text, params object[] formatArgs);
        PartialList<ChangeLogEntryDto> GetEntries(int entityId, ChangeLogEntityTypes entityType, int skip = 0, int limit = 10, ChangeLogTypes? type = null);
    }
}