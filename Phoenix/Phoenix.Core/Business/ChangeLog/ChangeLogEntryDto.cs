﻿using System;

namespace Metier.Phoenix.Core.Business.ChangeLog
{
    public class ChangeLogEntryDto
    {
        public ChangeLogEntityTypes EntityType { get; set; }
        public int EntityId { get; set; }
        public ChangeLogTypes Type { get; set; }
        public string Text { get; set; }
        public DateTime Created { get; set; }
        public int? UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}