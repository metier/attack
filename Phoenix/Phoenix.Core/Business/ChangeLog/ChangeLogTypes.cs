﻿namespace Metier.Phoenix.Core.Business.ChangeLog
{
    public enum ChangeLogTypes
    {
        Created = 1
        , Modified = 2
        , StatusChanged = 3
        , ExamSubmitted = 4
        , GradeSet = 5
        , CandidateSurveySent = 6
    }
}