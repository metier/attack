﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Core.Business.ChangeLog
{
    public class ChangeLogProvider : IChangeLogProvider
    {
        private readonly MetierLmsContext _context;

        public ChangeLogProvider(MetierLmsContext context)
        {
            _context = context;
        }

        public ChangeLogEntry Write(int entityId, ChangeLogEntityTypes entityType, ChangeLogTypes type, string text, params object[] formatArgs)
        {
            var entry = new ChangeLogEntry
            {
                EntityId = entityId,
                EntityType = entityType,
                Type = type,
                Created = DateTime.UtcNow,
                Text = string.Format(text, formatArgs),
                UserId = GetUserId()
            };

            _context.ChangeLog.Add(entry);
            _context.SaveChanges();
            return entry;
        }

        public PartialList<ChangeLogEntryDto> GetEntries(int entityId, ChangeLogEntityTypes entityType, int skip = 0, int limit = 20, ChangeLogTypes? type = null)
        {
            var query = _context.ChangeLog.Include(e => e.User).Where(e => e.EntityId == entityId && e.EntityType == entityType);

            if (type != null)
            {
                query = query.Where(e => e.Type == type);
            }
            var totalCount = query.Count();
            query = query.OrderByDescending(e => e.Created);
            query = query.Skip(skip);
            if (limit > 0)
            {
                query = query.Take(limit);    
            }
            return query.ToList().Select(Map).ToPartialList(totalCount);
        }

        private ChangeLogEntryDto Map(ChangeLogEntry entity)
        {
            return new ChangeLogEntryDto
            {
                Created = entity.Created,
                EntityId = entity.EntityId,
                EntityType = entity.EntityType,
                FirstName = entity.User == null ? "System" : entity.User.FirstName,
                LastName = entity.User == null ? "User" : entity.User.LastName,
                Text = entity.Text,
                Type = entity.Type,
                UserId = entity.UserId
            };
        }

        private int? GetUserId()
        {
            if (HttpContext.Current != null && HttpContext.Current.User != null)
            {
                var currentUsername = HttpContext.Current.User.Identity.Name;
                var user = _context.Users.FirstOrDefault(u => u.MembershipUserId == currentUsername);
                if (user != null)
                {
                    return user.Id;
                }
            }
            return null;
        }
    }
}