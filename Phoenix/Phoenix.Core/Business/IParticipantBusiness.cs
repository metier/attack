﻿using System;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Facade;

namespace Metier.Phoenix.Core.Business
{
    public interface IParticipantBusiness
    {
        bool IsPastDueDate(int participantId);
        bool IsPastDueDate(Participant participant, Activity activity, Article article);
        DateTime GetParticipantStart(int participantId);
        DateTime GetParticipantStart(Participant participant);
        DateTime GetParticipantStart(Participant participant, Activity activity);
        DateTime? GetDueDate(int participantId);
        DateTime? GetDueDate(Participant participant, Activity activity, Article article, Exam exam);
        ParticipationAvailability GetAvailability(int participantId);
        ParticipationAvailability GetAvailability(Participant participant, Activity activity, Article article);
    }
}