using System;
using System.Configuration;
using System.IO;
using System.Linq;
using Metier.Phoenix.Core.Data;
using File = Metier.Phoenix.Core.Data.Entities.File;

namespace Metier.Phoenix.Core.Business.FileStorage
{
    public class FileStorage : IFileStorage
    {
        private readonly MetierLmsContext _context;
        private readonly string _filesFolder;

        public FileStorage(MetierLmsContext context)
        {
            _context = context;
            _filesFolder = ConfigurationManager.AppSettings["FileUploadDir"];
        }

        public File CreateFile(string filename, string contentType, Stream fileStream)
        {
            var fileEntity = CreateFileEntity(filename, contentType, fileStream);
            using (fileStream)
            {
                if (!Directory.Exists(_filesFolder))
                {
                    Directory.CreateDirectory(_filesFolder);
                }
                using (Stream file = System.IO.File.OpenWrite(GetFilePath(fileEntity.RelativeFilePath)))
                {
                    CopyStream(fileStream, file);
                }
            }

            return fileEntity;
        }

        public Stream GetFileStream(File file)
        {
            return System.IO.File.OpenRead(GetFilePath(file.RelativeFilePath));
        }

        private static void CopyStream(Stream input, Stream output)
        {
            var buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }

        private File CreateFileEntity(string filename, string contentType, Stream fileStream)
        {
            var file = new File
            {
                Filename = filename,
                RelativeFilePath = GetUniqueFileName(filename),
                ContentType = contentType,
                Size = fileStream.Length
            };

            _context.Files.Add(file);
            _context.SaveChanges();
            return file;
        }

        private static string GetUniqueFileName(string filename)
        {
            var extension = filename.Split('.').LastOrDefault();
            if (extension != null)
            {
                return Guid.NewGuid() + "." + extension;
            }
            return Guid.NewGuid().ToString();
        }

        private string GetFilePath(string filename)
        {
            return Path.Combine(_filesFolder, filename);
        }
    }
}