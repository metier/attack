﻿using System.IO;
using File = Metier.Phoenix.Core.Data.Entities.File;

namespace Metier.Phoenix.Core.Business.FileStorage
{
    public interface IFileStorage
    {
        File CreateFile(string filename, string contentType, Stream fileStream);
        Stream GetFileStream(File file);
    }
}