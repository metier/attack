﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.Facade;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Core.Utilities.Extensions;

namespace Metier.Phoenix.Core.Business
{
    public class ParticipantBusiness : IParticipantBusiness
    {
        private readonly MetierLmsContext _context;

        public ParticipantBusiness(MetierLmsContext context)
        {
            _context = context;
        }

        public bool IsPastDueDate(int participantId)
        {
            var dueDate = GetDueDate(participantId);
            if (dueDate == null)
            {
                return true;
            }
            return DateTime.UtcNow > dueDate.Value;
        }

        public bool IsPastDueDate(Participant participant, Activity activity, Article article)
        {
            var dueDate = GetDueDate(participant, activity, article);
            if (dueDate == null)
            {
                return true;
            }
            return DateTime.UtcNow > dueDate.Value;
        }

        public DateTime? GetDueDate(int participantId)
        {
            var participant = _context.Participants.FirstOrDefault(p => p.Id == participantId);
            if (participant == null)
            {
                throw new ApplicationException("Participant with id {0} could not be found");
            }
            var articleActivities = _context.Participants.Where(p => p.Id == participant.Id)
                .Join(_context.Activities, par => par.ActivityId, act => act.Id, (par, act) => act)
                .Join(_context.CustomerArticles, act => act.CustomerArticleId, cusart => cusart.Id, (act, cusart) => new {Activity = act, CustomerArticle = cusart})
                .Join(_context.Articles, x => x.CustomerArticle.ArticleCopyId, art => art.Id, (x, art) => new {Article = art, x.Activity})
                .First();

            if (articleActivities.Activity.ActivityPeriods == null || articleActivities.Activity.ActivityPeriods.Count == 0)
                _context.Entry(articleActivities.Activity).Collection(a => a.ActivityPeriods).Load();

            return GetDueDate(participant, articleActivities.Activity, articleActivities.Article);
        }

        public DateTime? GetDueDate(Participant participant, Activity activity, Article article, Exam exam = null)
        {
            if (article is ExamArticle)
            {
                return GetDueDateForExam(participant, activity, exam);
            }
            if (article is ElearningArticle)
            {
                return GetDueDateForDurationBasedActivity(participant, activity);
            }
            if (article is CaseExamArticle)
            {
                return GetDueDateForCaseExam(participant, activity);
            }

            return GetDueDateForArticle(participant, activity);
        }

        private DateTime? GetDueDateForExam(Participant participant, Activity activity, Exam exam = null)
        {
            if (activity.EndDate != null)
                throw new ValidationException("End Date should not be set on activities of type Multiple Choice or Internal Certification");

            if (activity.ActivityPeriods.Count != 1)
                throw new ValidationException("Exam activities must have one, and only one activity period");

            if (!activity.Duration.HasValue)
                throw new ValidationException("Exam activities must have duration specified");

            if(exam == null)
                exam = _context.Exams.FirstOrDefault(e => e.ParticipantId == participant.Id);

            if (exam == null)
            {
                if (activity.ActivityPeriods.Any(p => p.End.HasValue))
                {
                    var end = activity.ActivityPeriods.Where(p => p.End.HasValue).Max(p => p.End.Value);
                    return end.UtcDateTime.AddMilliseconds(activity.Duration.GetValueOrDefault())
                        .AddMilliseconds(participant.AdditionalTime.GetValueOrDefault());
                }
                return null;
            }
            return exam.Created.AddMilliseconds(activity.Duration.GetValueOrDefault()) //Not converted to utc as the created-date is updated on the server.
                .AddMilliseconds(participant.AdditionalTime.GetValueOrDefault());
        }

        private DateTime? GetDueDateForCaseExam(Participant participant, Activity activity)
        {
            if (activity.EndDate.HasValue)
            {
                return activity.EndDate.Value.UtcDateTime.AddMilliseconds(participant.AdditionalTime.GetValueOrDefault());
            }

            if (activity.ActivityPeriods.Count != 1)
                throw new ValidationException("Exam activities must have one, and only one activity period");

            if (!activity.Duration.HasValue)
                throw new ValidationException("Exam activities must have either end date or duration specified");

            var start = activity.ActivityPeriods.Where(p => p.Start.HasValue).Max(p => p.Start.Value);
            return start.UtcDateTime.AddMilliseconds(activity.Duration.Value).AddMilliseconds(participant.AdditionalTime.GetValueOrDefault());
        }

        private DateTime? GetAvailabilityEndDateForDurationBasedActivity(Participant participant, Activity activity)
        {
            ValidateDurationBasedActivity(activity);

            var startDate = GetParticipantStart(participant, activity);
            var endDate = activity.ActivityPeriods[0].End;

            if (activity.Duration.HasValue)
                return startDate.AddMilliseconds(activity.Duration.Value).AddMilliseconds(participant.AdditionalTime.GetValueOrDefault());

            return endDate?.UtcDateTime.AddMilliseconds(participant.AdditionalTime.GetValueOrDefault());
        }

        private DateTime? GetDueDateForDurationBasedActivity(Participant participant, Activity activity)
        {
            ValidateDurationBasedActivity(activity);

            var startDate = GetParticipantStart(participant, activity);

            if (activity.CompletionTargetDuration.HasValue)
                return startDate.AddMilliseconds(activity.CompletionTargetDuration.Value).AddMilliseconds(participant.AdditionalTime.GetValueOrDefault());

            return activity.CompletionTargetDate?.AddMilliseconds(participant.AdditionalTime.GetValueOrDefault());
        }

        private void ValidateDurationBasedActivity(Activity activity)
        {
            if (activity.EndDate != null)
                throw new ValidationException("End Date should not be set on e-learning activities");

            if (activity.ActivityPeriods.Count != 1)
                throw new ValidationException("E-learning activities must have one, and only one activity period");
        }

        private DateTime? GetDueDateForArticle(Participant participant, Activity activity)
        {
            if (activity.EndDate != null)
                throw new ValidationException("End Date should not be set on activities of this type");

            if (activity.ActivityPeriods == null || activity.ActivityPeriods.Count == 0)
                activity.ActivityPeriods = _context.ActivityPeriods.Where(ap => ap.ActivityId == activity.Id).ToList();

            if (activity.ActivityPeriods.Count == 0)
                throw new ValidationException("Activities of this type must have at least one activity period defined");

            if (activity.ActivityPeriods.Any(p => p.End.HasValue))
            {
                return activity.ActivityPeriods.Where(p => p.End.HasValue).Max(p => p.End.Value)
                    .UtcDateTime.AddMilliseconds(participant.AdditionalTime.GetValueOrDefault());
            }

            return null;
        }

        public DateTime GetParticipantStart(int participantId)
        {
            var participant = _context.Participants.FirstOrDefault(p => p.Id == participantId);
            return GetParticipantStart(participant);
        }

        public DateTime GetParticipantStart(Participant participant)
        {
            var activity = _context.Activities.Include(a=>a.ActivityPeriods).FirstOrDefault(a => a.Id == participant.Id);
            return GetParticipantStart(participant, activity);
        }

        public DateTime GetParticipantStart(Participant participant, Activity activity)
        {
            if (activity == null || participant.ActivityId != activity.Id)
                throw new ValidationException("Invalid activity provided");

            if (activity.ActivityPeriods == null || activity.ActivityPeriods.Count == 0)
                activity.ActivityPeriods = _context.ActivityPeriods.Where(ap => ap.ActivityId == activity.Id).ToList();

            var dates = new List<DateTime?>();
            var confirmationMailDate = _context.Mails.FirstOrDefault(m => m.ParticipantId == participant.Id && m.MessageType == MessageType.EnrollmentConfirmation)?.Created;
            var activityStartDate = activity.ActivityPeriods?.Min(ap => ap.Start.AsUtcDateTime());

            dates.Add(participant.Created);
            dates.Add(activityStartDate);
            dates.Add(confirmationMailDate ?? activity.EnrollmentConfirmationDate);

            return dates.Max().GetValueOrDefault();
        }


        private ParticipationAvailability GetAvailabilityForDurationBasedArticle(Participant participant, Activity activity, Article article)
        {
            var participantAvailability = InitParticipationAvailability(participant, activity, article);

            if (activity.Duration.HasValue)
            {
                var start = GetParticipantStart(participant, activity);
                var end = GetAvailabilityEndDateForDurationBasedActivity(participant, activity);
                //var dueDate = GetDueDate(participant, activity, article);

                participantAvailability.IsAvailable = start < DateTime.UtcNow && DateTime.UtcNow < end?.Date;
                participantAvailability.AvailabilityStart = start;
                participantAvailability.AvailabilityEnd = end;
            }

            return participantAvailability;
        }
        private ParticipationAvailability GetAvailabilityForExam(Participant participant, Activity activity, ExamArticle article)
        {
            var participantAvailability = InitParticipationAvailability(participant, activity, article);

            var exam = _context.Exams.FirstOrDefault(e => e.ParticipantId == participant.Id);

            if (exam != null)
            {
                var examEnd = GetExamEnd(exam, activity, participant);
                var examExpired = DateTime.UtcNow > examEnd;

                if (examExpired)
                {
                    participantAvailability.IsAvailable = false;
                    participantAvailability.AvailabilityStart = participantAvailability.AvailabilityEnd = null;
                }
                else
                {
                    participantAvailability.IsAvailable = true;

                    var examCreatedAvailabilityPeriods = activity.ActivityPeriods.Where(a => (!a.Start.HasValue || a.Start < exam.Created) && (!a.End.HasValue || a.End > exam.Created)).ToList();
                    var earliestDate = examCreatedAvailabilityPeriods.Any(p => !p.Start.HasValue) ? null : examCreatedAvailabilityPeriods.OrderBy(p => p.Start).Select(p => p.Start).FirstOrDefault();

                    participantAvailability.AvailabilityStart = earliestDate.AsUtcDateTime();
                    participantAvailability.AvailabilityEnd = examEnd;
                }
            }

            return participantAvailability;
        }

        private ParticipationAvailability InitParticipationAvailability(Participant participant, Activity activity, Article article)
        {
            var currentlyValidAvailabilityPeriods = GetCurrentlyAvailablePeriods(article, activity, participant);

            var earliestDate = currentlyValidAvailabilityPeriods.Any(p => !p.Start.HasValue) ? null : currentlyValidAvailabilityPeriods.OrderBy(p => p.Start).Select(p => p.Start).FirstOrDefault();
            var latestDate = currentlyValidAvailabilityPeriods.Any(p => !p.End.HasValue) ? null : currentlyValidAvailabilityPeriods.OrderByDescending(p => p.End).Select(p => p.End).FirstOrDefault();

            return new ParticipationAvailability
            {
                IsAvailable = currentlyValidAvailabilityPeriods.Any() && earliestDate.EarlierOrNull(DateTime.UtcNow) && latestDate.LaterOrNull(DateTime.UtcNow),
                AvailabilityStart = earliestDate.AsUtcDateTime(),
                AvailabilityEnd = latestDate.AsUtcDateTime(),
                ActivityPeriods = activity.ActivityPeriods.Select(ap => new ActivityAvailabilityPeriods { Start = ap.Start, End = ap.End }).ToList(),
				AdditionalTime = participant.AdditionalTime
            };

        }

        public ParticipationAvailability GetAvailability(Participant participant, Activity activity, Article article)
        {
            if (article is ExamArticle)
            {
                return GetAvailabilityForExam(participant, activity, (ExamArticle)article);
            }
            if (article is ElearningArticle || article is SubscriptionArticle)
            {
                return GetAvailabilityForDurationBasedArticle(participant, activity, article);
            }

            return InitParticipationAvailability(participant, activity, article);
        }

        public ParticipationAvailability GetAvailability(int participantId)
        {
            var participant = _context.Participants.FirstOrDefault(p => p.Id == participantId);
            if (participant == null)
            {
                throw new ValidationException("Participant with id {0} could not be found");
            }
            var activity = _context.Activities.AsNoTracking().Include(a => a.ActivityPeriods).First(a => a.Id == participant.ActivityId);
            var article = _context.CustomerArticles.Where(ca => ca.Id == activity.CustomerArticleId)
                .Join(_context.Articles, ca => ca.ArticleCopyId, a => a.Id, (ca, a) => a)
                .First();

            return GetAvailability(participant, activity, article);
        }

        private List<ActivityPeriod> GetCurrentlyAvailablePeriods(Article article, Activity activity, Participant participant)
        {
            var activityPeriods = activity.ActivityPeriods;

            if (article.ArticleTypeId == (int)ArticleTypes.ELearning)
            {
                var additionalTime = participant.AdditionalTime ?? 0;

                activityPeriods =
                    activity.ActivityPeriods.Select(
                        ap =>
                            new ActivityPeriod
                            {
                                ActivityId = ap.ActivityId,
                                Start = ap.Start,
                                End = ap.End.HasValue ? ap.End += TimeSpan.FromMilliseconds(additionalTime) : null
                            })
                        .ToList();
            }

            return activityPeriods.Where(a => (!a.Start.HasValue || a.Start < DateTime.UtcNow)).ToList();
        }

        private DateTime GetExamEnd(Exam exam, Activity activity, Participant participant)
        {
            var duration = activity.Duration ?? 0;
            var additionalTime = participant.AdditionalTime.HasValue ? participant.AdditionalTime.Value : 0;
            return exam.Created + TimeSpan.FromMilliseconds(duration) + TimeSpan.FromMilliseconds(additionalTime);
        }
    }
}