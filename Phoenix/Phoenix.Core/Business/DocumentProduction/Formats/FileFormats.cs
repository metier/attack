﻿namespace Metier.Phoenix.Core.Business.DocumentProduction.Formats
{
    public enum FileFormats
    {
        Excel,
        Csv
    }
}