﻿using System.Collections.Generic;
using OfficeOpenXml;

namespace Metier.Phoenix.Core.Business.DocumentProduction.Formats.Excel
{
    public interface IExcelFactory
    {
        byte[] CreateDocument<T>(IEnumerable<T> items, string documentName = "Report");
        ExcelPackage CreateExcelPackage<T>(IEnumerable<T> items, string documentName = "Report");
    }
}