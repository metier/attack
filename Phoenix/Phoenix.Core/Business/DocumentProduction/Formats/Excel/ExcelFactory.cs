using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Metier.Phoenix.Core.Business.DocumentProduction.Attributes;
using Metier.Phoenix.Core.Utilities.Logging;
using NLog;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace Metier.Phoenix.Core.Business.DocumentProduction.Formats.Excel
{
    public class ExcelFactory : IExcelFactory
    {
        private readonly Logger _logger;

        public ExcelFactory()
        {
            _logger = LogManager.GetCurrentClassLogger();
        }

        public byte[] CreateDocument<T>(IEnumerable<T> items, string documentName = "Report")
        {
            var package = CreateExcelPackage(items, documentName);
            return package.GetAsByteArray();
            
        }
        public ExcelPackage CreateExcelPackage<T>(IEnumerable<T> items, string documentName = "Report")
        {
            using (new TimingLogScope(_logger, "Created Excel document"))
            {
                var package = new ExcelPackage();

                var listOfItems = items.ToList();
                var columnNames = GetPropertyNames(listOfItems).ToList();

                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(documentName);

                WriteHeadings<T>(worksheet, columnNames);
                WriteItems(worksheet, listOfItems, columnNames);

                return package;
            }
            
        }
        private static IEnumerable<string> GetPropertyNames<T>(IList<T> items)
        {
            var propertyInfos = (typeof(T)).GetProperties();
            foreach (var propertyInfo in propertyInfos)
            {
                if (propertyInfo.PropertyType == typeof (ExcelRowStyleOptions))
                {
                    continue;
                }
                if (propertyInfo.PropertyType.IsGenericType &&
                    propertyInfo.PropertyType.GetGenericTypeDefinition() == typeof (IList<>))
                {
                    if (items.Any())
                    {
                        var maxNumberOfItems = items.Max(i => ((IList)propertyInfo.GetValue(i)).Count);
                        var innerProperties = propertyInfo.PropertyType.GetGenericArguments()[0].GetProperties();
                        for (int i = 0; i < maxNumberOfItems; i++)
                        {
                            foreach (var innerProperty in innerProperties)
                            {
                                yield return propertyInfo.Name + "." + i + "." + innerProperty.Name;
                            }
                        }
                    }
                }
                else
                {
                    yield return propertyInfo.Name; 
                }
            }
        } 

        private object GetValue<T>(T item, string columnName)
        {
            // T eller T.i.Tchild (i = index)
            var tPropertyNames = typeof (T).GetProperties().Select(p => p.Name);
            if (tPropertyNames.Contains(columnName))
            {
                return typeof (T).GetProperty(columnName).GetValue(item);
            }

            var propertyValues = columnName.Split('.');
            if (propertyValues.Length != 3)
            {
                return null;
            }

            var tProperty = typeof(T).GetProperty(propertyValues[0]);
            var tChildProperty = tProperty.PropertyType.GetGenericArguments()[0].GetProperty(propertyValues[2]);
            
            var index = Convert.ToInt32(propertyValues[1]);
            var tList = tProperty.GetValue(item) as IList;

            if (tList == null)
            {
                return null;
            }
            if (tList.Count < (index + 1)) // Would have resulted in ArgumentOutOfRangeException otherwise
            {
                return null;
            }
            return tChildProperty.GetValue(tList[index], null);
        }

        private void WriteItems<T>(ExcelWorksheet worksheet, IList<T> items, List<string> columnNames)
        {
            for (int i = 0; i < items.Count; i++)
            {
                SetRowStyle(worksheet, items, i);

                for (int j = 0; j < columnNames.Count; j++)
                {
                    var value = GetValue(items[i], columnNames[j]);
                    var cell = worksheet.Cells[i + 2, j + 1];
                    if (value is DateTime || value is DateTimeOffset)
                    {
                        cell.Style.Numberformat.Format = "mm-dd-yy";
                    }

                    if (value is DateTimeOffset)
                    {
                        cell.Value = ((DateTimeOffset)value).DateTime;
                    }
                    else
                    {
                        cell.Value = value;
                    }
                    
                }
            }
        }

        private static void SetRowStyle<T>(ExcelWorksheet worksheet, IList<T> items, int i)
        {
            var excelRowStylePropertyInfo =
                typeof (T).GetProperties().FirstOrDefault(p => typeof (ExcelRowStyleOptions) == p.PropertyType);
            if (excelRowStylePropertyInfo != null)
            {
                var rowOptions = excelRowStylePropertyInfo.GetValue(items[i]) as ExcelRowStyleOptions;
                if (rowOptions != null)
                {
                    if (rowOptions.BackgroundColor.HasValue)
                    {
                        var row = worksheet.Row(i + 2);
                        row.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        row.Style.Fill.BackgroundColor.SetColor(rowOptions.BackgroundColor.Value);
                    }
                }
            }
        }

        private static void WriteHeadings<T>(ExcelWorksheet worksheet, List<string> columns)
        {
            var tPropertyNames = typeof (T).GetProperties().Select(p => p.Name).ToList();
            for (int i = 0; i < columns.Count(); i++)
            {
                if (tPropertyNames.Contains(columns[i]))
                {
                    // Type T properties
                    var property = typeof(T).GetProperty(columns[i]);
                    var displayNameAttribute = property.GetAttribute<DisplayNameAttribute>();
                    worksheet.Cells[1, i + 1].Value = displayNameAttribute == null ? columns[i] : displayNameAttribute.DisplayName;
                }
                else
                {
                    // Type IList<> properties
                    var propertyValues = columns[i].Split('.');
                    if (propertyValues.Length != 3)
                    {
                        worksheet.Cells[1, i + 1].Value = "Unknown heading";
                    }

                    var tParentProperty = typeof(T).GetProperty(propertyValues[0]);
                    var tListChildProperty = tParentProperty.PropertyType.GetGenericArguments()[0].GetProperty(propertyValues[2]);
                    var displayNameAttribute = tListChildProperty.GetAttribute<DisplayNameAttribute>();

                    var displayName = displayNameAttribute == null ? propertyValues[2] : displayNameAttribute.DisplayName;
                    
                    var countColumns = tListChildProperty.GetAttribute<CountListColumnsAttribute>();
                    if (countColumns != null)
                    {
                        displayName += " " + (Convert.ToInt32(propertyValues[1]) + 1);
                    }

                    worksheet.Cells[1, i + 1].Value = displayName;
                }
            }
            using (var range = worksheet.Cells["A1:BZ1"])
            {
                range.Style.Font.Bold = true;
            }
        }
    }
}