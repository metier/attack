﻿using System.Drawing;

namespace Metier.Phoenix.Core.Business.DocumentProduction.Formats.Excel
{
    public class ExcelRowStyleOptions
    {
        public Color? BackgroundColor { get; set; } 
    }
}