﻿using System.Collections.Generic;

namespace Metier.Phoenix.Core.Business.DocumentProduction.Formats.Csv
{
    public interface ICsvFactory
    {        
        byte[] CreateDocument<T>(IEnumerable<T> items, bool EncloseCsvValuesInApostrophes);
    }
}