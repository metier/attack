using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Metier.Phoenix.Core.Business.DocumentProduction.Attributes;
using Metier.Phoenix.Core.Utilities.Logging;
using NLog;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using System.Text;

namespace Metier.Phoenix.Core.Business.DocumentProduction.Formats.Csv
{
    public class CsvFactory : ICsvFactory
    {
        private readonly Logger _logger;
		private bool EncloseCsvValuesInApostrophes { get; set; }


		public CsvFactory()
        {
            _logger = LogManager.GetCurrentClassLogger();
        }

        public byte[] CreateDocument<T>(IEnumerable<T> items, bool EncloseCsvValuesInApostrophes)
        {
			this.EncloseCsvValuesInApostrophes = EncloseCsvValuesInApostrophes;
			Excel.ExcelFactory excelFactory = new Excel.ExcelFactory();
            var package = excelFactory.CreateExcelPackage<T>(items);

			var columnCount = package.Workbook.Worksheets[1].Dimension.End.Column;
			if (items.Any())
				columnCount = items.First().GetType().GetProperties().Count();
					
			var data = ConvertToCsv(package, columnCount);
            return data;
        }

        public byte[] ConvertToCsv(ExcelPackage package, int maxColumnNumber)
        {
            var worksheet = package.Workbook.Worksheets[1];
            var currentRow = new List<string>(maxColumnNumber);
            var totalRowCount = worksheet.Dimension.End.Row;
            var currentRowNum = 1;

            var memory = new MemoryStream();

            using (var writer = new StreamWriter(memory, Encoding.UTF8))
            {
                while (currentRowNum <= totalRowCount)
                {
                    BuildRow(worksheet, currentRow, currentRowNum, maxColumnNumber);
                    WriteRecordToFile(currentRow, writer, currentRowNum, totalRowCount);
                    currentRow.Clear();
                    currentRowNum++;
                }
            }

            return memory.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="record">List of cell values</param>
        /// <param name="sw">Open Writer to file</param>
        /// <param name="rowNumber">Current row num</param>
        /// <param name="totalRowCount"></param>
        /// <remarks>Avoiding writing final empty line so bulk import processes can work.</remarks>
        private void WriteRecordToFile(List<string> record, StreamWriter sw, int rowNumber, int totalRowCount)
        {
            var commaDelimitedRecord = ToDelimitedString(record);

            if (rowNumber == totalRowCount)
            {
                sw.Write(commaDelimitedRecord);
            }
            else
            {
                sw.WriteLine(commaDelimitedRecord);
            }
        }

        
        /// <summary>
        /// Takes a List collection of string and returns a delimited string.  Note that it's easy to create a huge list that won't turn into a huge string because
        /// the string needs contiguous memory.
        /// </summary>
        /// <param name="list">The input List collection of string objects</param>
        /// <param name="qualifier">
        /// The default delimiter. Using a colon in case the List of string are file names,
        /// since it is an illegal file name character on Windows machines and therefore should not be in the file name anywhere.
        /// </param>
        /// <param name="insertSpaces">Whether to insert a space after each separator</param>
        /// <returns>A delimited string</returns>
        /// <remarks>This was implemented pre-linq</remarks>
        public string ToDelimitedString(List<string> list)
        {
            string delimiter = ",";
            string qualifier = "";
            var result = new StringBuilder();
            for (int i = 0; i < list.Count; i++)
            {
                string initialStr =  list[i];
                result.Append((qualifier == string.Empty) ? initialStr : string.Format("{1}{0}{1}", initialStr, qualifier));
                if (i < list.Count - 1)
                {
                    result.Append(delimiter);
  
                }
            }
            return result.ToString();
        }

        private void BuildRow(ExcelWorksheet worksheet, List<string> currentRow, int currentRowNum, int maxColumnNumber)
        {
            for (int i = 1; i <= maxColumnNumber; i++)
            {
                var cell = worksheet.Cells[currentRowNum, i];
                if (cell == null)
                {
                    // add a cell value for empty cells to keep data aligned.
                    AddCellValue(string.Empty, currentRow);
                }
                else
                {
                    AddCellValue(GetCellText(cell), currentRow);
                }
            }
        }

        /// <summary>        
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        private string GetCellText(ExcelRangeBase cell)
        {
            return cell.Value == null ? string.Empty : cell.Value.ToString();
        }

        private void AddCellValue(string s, List<string> record)
        {
			if(this.EncloseCsvValuesInApostrophes)
				record.Add(string.Format("{0}{1}{0}", '"', s));
			else
				record.Add(s);
		}

    }
}