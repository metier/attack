﻿using System;

namespace Metier.Phoenix.Core.Business.DocumentProduction.Attributes
{
    /// <summary>
    /// Display name to be used as heading in document instead of property name
    /// </summary>
    public class DisplayNameAttribute : Attribute
    {
        private readonly string _displayName;

        public DisplayNameAttribute(string displayName)
        {
            _displayName = displayName;
        }

        public string DisplayName
        {
            get { return _displayName; }
        }
    }
}