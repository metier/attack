﻿using System;

namespace Metier.Phoenix.Core.Business.DocumentProduction.Attributes
{
    /// <summary>
    /// For list elements with same display name, given this attribute will add a number after the display name
    /// </summary>
    public class CountListColumnsAttribute : Attribute
    {
    }
}