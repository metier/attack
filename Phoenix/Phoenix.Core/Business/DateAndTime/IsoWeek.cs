﻿using System;
using System.Globalization;

namespace Metier.Phoenix.Core.Business.DateAndTime
{
    public class IsoWeek : IComparable<IsoWeek>
    {
        public int WeekNumber { get; private set; }
        public int Year { get; private set; }

        private IsoWeek()
        {
        }

        public IsoWeek(int weekNo, int year)
        {
            if(!IsValidWeekNo(weekNo, year))
                throw new Exceptions.ValidationException("Ivalid week number");

            WeekNumber = weekNo;
            Year = year;
        }

        private bool IsValidWeekNo(int weekNo, int year)
        {
            var date = new DateTime(year, 12, 31);
            IsoWeek maxWeekNo = GetIsoWeekFromDate(date);
            var week = new IsoWeek {WeekNumber = weekNo, Year = year};

            if (weekNo == 0 || week > maxWeekNo)
                return false;

            return true;
        }

        private string YearAndWeekNoAsString()
        {
            return string.Concat(Year, (WeekNumber < 10 ? ("0") : ""), WeekNumber);
        }

        #region Comparison Operators and functions

        public int CompareTo(IsoWeek other)
        {
            // If other is not a valid object reference, this instance is greater.
            if (other == null) return 1;

            return int.Parse(YearAndWeekNoAsString()).CompareTo(int.Parse(other.YearAndWeekNoAsString()));
        }

        public bool Equals(IsoWeek isoWeek)
        {
            return CompareTo(isoWeek) == 0;
        }

        // Define the is greater than operator.
        public static bool operator >(IsoWeek operand1, IsoWeek operand2)
        {
            return operand1.CompareTo(operand2) == 1;
        }

        // Define the is less than operator.
        public static bool operator <(IsoWeek operand1, IsoWeek operand2)
        {
            return operand1.CompareTo(operand2) == -1;
        }

        // Define the is greater than or equal to operator.
        public static bool operator >=(IsoWeek operand1, IsoWeek operand2)
        {
            return operand1.CompareTo(operand2) >= 0;
        }

        // Define the is less than or equal to operator.
        public static bool operator <=(IsoWeek operand1, IsoWeek operand2)
        {
            return operand1.CompareTo(operand2) <= 0;
        }

        #endregion

        public static IsoWeek GetIsoWeekFromDate(DateTime date)
        {
            int weekNo = WeekOfYearISO8601(date);
            int year = date.Year;

            if (date.Month == 1 && weekNo > 50)
                year--;

            if (date.Month == 12 && weekNo == 1)
                year++;

            return new IsoWeek { WeekNumber = weekNo, Year = year };
        }

        public static int WeekOfYearISO8601(DateTime date)
        {
            var day = (int) CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(date);
            return CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(date.AddDays(4 - (day == 0 ? 7 : day)), CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }
    }
}
