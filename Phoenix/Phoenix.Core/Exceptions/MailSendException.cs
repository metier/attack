﻿using System;

namespace Metier.Phoenix.Core.Exceptions
{
    public class MailSendException : ApplicationException
    {
        public MailSendException(string message) : base(message)
        {
        }

        public MailSendException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}