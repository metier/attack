﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Core.Exceptions
{
    public class ValidationException : Exception
    {
        private readonly IEnumerable<ValidationResult> _validationResults;
        private readonly ValidationErrorCodes? _errorCode;

        public ValidationException(string validationError, ValidationErrorCodes? errorCode = null)
            : base("Validation Error")
        {
            _validationResults = new List<ValidationResult> { new ValidationResult(validationError) };
            _errorCode = errorCode;
        }

        public ValidationException(string message, IEnumerable<ValidationResult> validationResults, ValidationErrorCodes? errorCode = null)
            : base(message)
        {
            _validationResults = validationResults;
            _errorCode = errorCode;
        }

        public IEnumerable<ValidationResult> ValidationResults
        {
            get { return _validationResults; }
        }

        public ValidationErrorCodes? ErrorCode
        {
            get { return _errorCode; }
        }
    }
}