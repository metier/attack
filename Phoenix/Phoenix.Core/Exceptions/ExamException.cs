﻿using System;

namespace Metier.Phoenix.Core.Exceptions
{
    public class ExamException : ApplicationException
    {
        public ExamException(string message) : base(message)
        {
        }
    }
}