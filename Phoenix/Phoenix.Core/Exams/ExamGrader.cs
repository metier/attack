using System;
using System.Linq;
using Metier.Phoenix.Core.Business.ChangeLog;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Exceptions;

namespace Metier.Phoenix.Core.Exams
{
    public class ExamGrader : IExamGrader
    {
        private readonly MetierLmsContext _context;
        private readonly IChangeLogProvider _changeLogProvider;

        public ExamGrader(MetierLmsContext context, IChangeLogProvider changeLogProvider)
        {
            _context = context;
            _changeLogProvider = changeLogProvider;
        }

        public void SetGrade(Exam submittedExam, ExamArticle examArticle, Participant participant)
        {
            var totalScoreExam = submittedExam.Questions.Sum(q => q.Points);
            if (submittedExam.IsTotalScoreCalculated)
            {
                submittedExam.TotalScore = CalculateScoreFromAnswers(submittedExam);    
            }
            participant.ExamGrade = DetermineGrade(examArticle.ECTS, submittedExam.TotalScore.GetValueOrDefault(), totalScoreExam);
            participant.Result = participant.ExamGrade == "F" ? ParticipantResultCodes.Failed : ParticipantResultCodes.Passed;
            _changeLogProvider.Write(
                participant.Id, 
                ChangeLogEntityTypes.Participant, 
                ChangeLogTypes.GradeSet, 
                "Grade set to \"{0}\". Result \"{1}\"", participant.ExamGrade, ParticipantResultCodes.GetHumanFriendly(participant.Result));
        }

        private int CalculateScoreFromAnswers(Exam submittedExam)
        {
            var score = 0;
            foreach (var answer in submittedExam.Answers)
            {
                var correspondingQuestion = submittedExam.Questions.First(q => q.Id == answer.QuestionId);
                if (IsCorrectAnswer(correspondingQuestion, answer))
                {
                    answer.Points = correspondingQuestion.Points;
                    score += correspondingQuestion.Points;
                }
            }
            return score;
        }

        internal string DetermineGrade(string ects, int achievedScore, int totalScore)
        {
            if (totalScore == 0)
            {
                return string.Empty;
            }
            var percentageScore = Decimal.Divide(achievedScore, totalScore); 

            var gradeScale = _context.GradeScales.Any(g => g.Ects == ects) ?
                             _context.GradeScales.Where(g => g.Ects == ects).OrderByDescending(g => g.MinPercentage).ToList() :
                             _context.GradeScales.Where(g => g.Ects == "DEFAULT").OrderByDescending(g => g.MinPercentage).ToList();
            

            var gradeScaleItem = gradeScale.FirstOrDefault(g => percentageScore >= g.MinPercentage);

            return gradeScaleItem != null ? gradeScaleItem.Grade : null;
        }

        internal bool IsCorrectAnswer(Question question, Answer answer)
        {
            if (question.Type == QuestionTypes.TrueFalse)
            {
                if (!answer.BoolAnswer.HasValue)
                {
                    return false;
                }
                if (!question.CorrectBoolAnswer.HasValue)
                {
                    throw new ExamException(string.Format("Question (ID: {0}) of type {1} is missing boolean answer.", question.Id, question.Type));
                }
                return answer.BoolAnswer == question.CorrectBoolAnswer;
            }
            if (question.Type == QuestionTypes.MultipleChoiceSingle || question.Type == QuestionTypes.MultipleChoiceMultiple)
            {
                var correctOptions = question.QuestionOptions.Where(q => q.IsCorrect).ToList();
                return answer.OptionsAnswers.Count == correctOptions.Count && answer.OptionsAnswers.All(oa => correctOptions.Any(o => o.Id == oa.Id));
            }
            throw new ExamException(string.Format("Unable to check answer for question with ID {0}, question type {1}.", question.Id, question.Type));
        }
    }
}