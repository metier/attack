﻿using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Exams
{
    public interface IExamGrader
    {
        void SetGrade(Exam submittedExam, ExamArticle examArticle, Participant participant);
    }
}