﻿using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Security
{
    public interface IPasswordResetRequestFactory
    {
        PasswordResetRequest CreatePasswordResetRequest(User user);
    }
}