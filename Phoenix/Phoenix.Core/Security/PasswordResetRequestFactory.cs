using System;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Core.Security
{
    public class PasswordResetRequestFactory : IPasswordResetRequestFactory
    {
        private readonly MetierLmsContext _context;

        public PasswordResetRequestFactory(MetierLmsContext context)
        {
            _context = context;
        }

        public PasswordResetRequest CreatePasswordResetRequest(User user)
        {
            DeleteExistingPasswordResetRequests(user);

            var passwordResetRequest = new PasswordResetRequest
            {
                Created = DateTime.UtcNow,
                ResetToken = Guid.NewGuid(),
                UserId = user.Id
            };

            _context.PasswordResetRequests.Add(passwordResetRequest);
            _context.SaveChanges();

            return passwordResetRequest;
        }

        private void DeleteExistingPasswordResetRequests(User user)
        {
            var existingResetRequest = _context.PasswordResetRequests.Where(p => p.UserId == user.Id).ToList();
            foreach (var passwordResetRequest in existingResetRequest)
            {
                _context.PasswordResetRequests.Remove(passwordResetRequest);
            }
            _context.SaveChanges();
        }
    }
}