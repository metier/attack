﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Core.Utilities
{
    public static class EnumerableExtensions
    {
        public static PartialList<T> ToPartialList<T>(this IEnumerable<T> items, int totalCount, string orderedBy = null)
        {
            return new PartialList<T>(items, totalCount, orderedBy);
        } 

        public static void ForEach<T>(this IEnumerable<T> list, Action<T> action)
        {
            if (list == null || action == null)
                return;
            foreach (T obj in list)
                action(obj);
        }

        /// <summary>
        /// Will compress source list into a smaller list of list of items
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static IEnumerable<List<T>> Compress<T>(this List<T> source, int size)
        {   
            if (size == 0)
            {
                return new List<List<T>> { source };
            }
            if (source.Count < size)
            {
                throw new InvalidOperationException("Size must be equal or smaller than source count");
            }

            var partitioned = new List<List<T>>();
            var distribution = GetDistribution(source.Count, size);
            var taken = 0;
            foreach (var item in distribution)
            {
                partitioned.Add(source.Skip(taken).Take(item).ToList());
                taken += item;
            }
            return partitioned;
        }

        /// <summary>
        /// Will stretch source list into a larger list of list of items distributed evenly around
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static IEnumerable<List<T>> Stretch<T>(this List<T> source, int size)
        {
            if (size <= source.Count)
            {
                throw new InvalidOperationException("Size must be greater than source count");
            }
    
            var numberOfEmptyItemsToAdd = size - source.Count;
            var spaceBetweenLessons = numberOfEmptyItemsToAdd/source.Count;
            var remainder = numberOfEmptyItemsToAdd%source.Count;

            if (spaceBetweenLessons == 0 && numberOfEmptyItemsToAdd > 0)
                spaceBetweenLessons = 1;

            if (numberOfEmptyItemsToAdd < source.Count)
                remainder = 0;

            var result = new List<List<T>>();
            source.Reverse();           //Reversing the list so in case of uneven number of empty spaces, there will be more "slack" at the end of the list.

            foreach (var item in source)
            {
                result.Add(new List<T> { item });

                //Add the remainder after the first item in the reversed list.
                numberOfEmptyItemsToAdd = numberOfEmptyItemsToAdd - AddEmptyItems(result, remainder);
                remainder = 0;

                numberOfEmptyItemsToAdd = numberOfEmptyItemsToAdd - AddEmptyItems(result, new List<int> { numberOfEmptyItemsToAdd, spaceBetweenLessons}.Min());
            }

            source.Reverse();
            result.Reverse();

            return result;
        }

        private static int AddEmptyItems<T>(List<List<T>> result, int numberOfEmptyItemsToAdd)
        {
            int numberOfItemsAdded = 0;

            for (int j = 0; j < numberOfEmptyItemsToAdd; j++)
            {
                if (numberOfEmptyItemsToAdd > 0)
                {
                    result.Add(new List<T>());
                    numberOfItemsAdded++;
                }
            }
            return numberOfItemsAdded;
        }

        private static IEnumerable<int> GetDistribution(int totalSize, int bucketSize)
        {
            var itemsPrBucket = ((double)totalSize / bucketSize);
            var equalItemsPrBucket = (int)Math.Floor(itemsPrBucket);
            var distribution = new List<int>(bucketSize);

            for (int i = 0; i < bucketSize; i++)
            {
                distribution.Add(equalItemsPrBucket);
            }
            var remindingItems = totalSize - (equalItemsPrBucket * bucketSize);
            for (int i = 0; i < remindingItems; i++)
            {
                distribution[i] += 1;
            }
            return distribution;
        }

        public static IEnumerable<T> AsRandom<T>(this IList<T> list)
        {
            int[] indexes = Enumerable.Range(0, list.Count).ToArray();
            Random generator = new Random();

            for (int i = 0; i < list.Count; ++i)
            {
                int position = generator.Next(i, list.Count);

                yield return list[indexes[position]];

                indexes[position] = indexes[i];
            }
        }
    }
}