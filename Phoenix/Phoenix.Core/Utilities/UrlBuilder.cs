using System;
using System.Collections.Generic;

namespace Metier.Phoenix.Core.Utilities
{
    public class UrlBuilder
    {
        private readonly string _baseUrl;
        private readonly List<string> _parameters;

        public UrlBuilder(string baseUrl)
        {
            var urlParts = baseUrl.Split('?');
            
            _parameters = new List<string>();
            _baseUrl = urlParts[0];
            
            if (urlParts.Length > 1)
            {
                var queryString = urlParts[1];
                _parameters.AddRange(queryString.Split('&'));
            }
        }

        public UrlBuilder AddParameter(string name, string value)
        {
            _parameters.Add(name + "=" + value);
            return this;
        }

        public override string ToString()
        {
            if (_parameters.Count == 0)
            {
                return _baseUrl;
            }
            return _baseUrl + "?" + String.Join("&", _parameters.ToArray());
        }
    }
}