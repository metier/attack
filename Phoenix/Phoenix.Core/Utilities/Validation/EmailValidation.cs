﻿using System;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace Metier.Phoenix.Core.Utilities.Validation
{
    public static class EmailValidation
    {
        public static bool IsValidEmail(string email)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(email))
                    return false;

                if (email.EndsWith("."))
                    return false;

                //Ref.: http://stackoverflow.com/questions/1365407/c-sharp-code-to-validate-email-address
                var m = new MailAddress(email);
                return m.Address == email;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}