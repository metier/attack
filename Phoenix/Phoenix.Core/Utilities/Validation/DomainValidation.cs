﻿using System.Text.RegularExpressions;

namespace Metier.Phoenix.Core.Utilities.Validation
{
    public static class DomainValidation
    {
        public static bool IsValidDomain(string domain)
        {
            return domain != null && Regex.IsMatch(domain, @"^[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,6}$");
        }
    }
}