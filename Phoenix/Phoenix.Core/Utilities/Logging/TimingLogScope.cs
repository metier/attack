﻿using System;
using System.Diagnostics;
using NLog;

namespace Metier.Phoenix.Core.Utilities.Logging
{
    public class TimingLogScope : IDisposable
    {
        private readonly Logger _logger;
        private readonly string _message;
        private readonly LogLevel _logLevel;
        private readonly Stopwatch _stopwatch;

        public TimingLogScope(Logger logger, string message, LogLevel logLevel = null)
        {
            _logger = logger;
            _message = message;
            _logLevel = logLevel ?? LogLevel.Debug;
            _stopwatch = new Stopwatch();
            _stopwatch.Start();
        }

        public void Dispose()
        {
            _stopwatch.Stop();
            _logger.Log(_logLevel, "{0} - Time used: {1} ms", _message, _stopwatch.ElapsedMilliseconds);
        }
    }
}