﻿using System;

namespace Metier.Phoenix.Core.Utilities.Extensions
{
    public static class DateTimeExtensions
    {
        public static DateTime? AsUtcDateTime(this DateTimeOffset? value)
        {
            return value.HasValue ? value.Value.UtcDateTime : (DateTime?) null;
        }

        public static DateTime? AsDateTime(this DateTimeOffset? value)
        {
            return value.HasValue ? value.Value.DateTime : (DateTime?)null;
        }

        public static bool EarlierOrNull(this DateTimeOffset? value, DateTime utcTime)
        {
            return !value.HasValue || utcTime > value.Value.UtcDateTime;
        }

        public static bool LaterOrNull(this DateTimeOffset? value, DateTime utcTime)
        {
            return !value.HasValue || utcTime < value.Value.UtcDateTime;
        }

        public static bool EarlierOrNull(this DateTime? value, DateTime utcTime)
        {
            return !value.HasValue || utcTime > value.Value;
        }

        public static bool LaterOrNull(this DateTime? value, DateTime utcTime)
        {
            return !value.HasValue || utcTime < value.Value;
        }
    }
}