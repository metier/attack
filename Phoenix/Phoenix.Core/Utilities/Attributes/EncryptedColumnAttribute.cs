﻿using System;
using System.Linq;
using System.Reflection;
using Metier.Phoenix.Core.Crypto;

namespace Metier.Phoenix.Core.Utilities.Attributes
{
    /// <summary>
    /// Attribute to be used with encrypted columns.
    /// Constructor takes name and type of a plain text property.
    /// Entity Framework will automatically decrypt the encrypted property into the plain text property and vica versa
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class EncryptedColumnAttribute : Attribute
    {
        private readonly string _plainTextPropertyName;
        private readonly TypeCode _plainTextTypeCode;

        public EncryptedColumnAttribute(string plainTextPropertyName, TypeCode plainTextTypeCode)
        {
            _plainTextPropertyName = plainTextPropertyName;
            _plainTextTypeCode = plainTextTypeCode;
        }

        public string PlainTextPropertyName
        {
            get { return _plainTextPropertyName; }
        }

        public TypeCode PlainTextTypeCode
        {
            get { return _plainTextTypeCode; }
        }

        public static void Encrypt(object entity)
        {
            if (entity == null)
                return;

            var properties = entity.GetType().GetProperties();
            foreach (var property in properties)
            {
                var encryptedAttribute = property.GetCustomAttribute<EncryptedColumnAttribute>();
                if (encryptedAttribute != null)
                {
                    var plaintextProperty = properties.FirstOrDefault(p => p.Name == encryptedAttribute.PlainTextPropertyName);
                    if (plaintextProperty != null)
                    {
                        var plainTextValue = plaintextProperty.GetValue(entity);
                        if (plainTextValue == null)
                        {
                            property.SetValue(entity, null);    
                        }
                        else
                        {
                            var plainText = Convert.ToString(plainTextValue);
                            var cipherText = SymmetricEncryption.Encrypt(plainText);
                            property.SetValue(entity, cipherText);    
                        }
                    }
                }
            }
        }

        public static void Decrypt(object entity)
        {
            if (entity == null)
                return;

            var properties = entity.GetType().GetProperties();
            foreach (var property in properties)
            {
                var encryptedAttribute = property.GetCustomAttribute<EncryptedColumnAttribute>();
                if (encryptedAttribute != null)
                {
                    var plaintextProperty = properties.FirstOrDefault(p => p.Name == encryptedAttribute.PlainTextPropertyName);
                    if (plaintextProperty != null)
                    {
                        var cipherText = (byte[])property.GetValue(entity);
                        if (cipherText != null && cipherText.Length > 0)
                        {
                            var plaintext = SymmetricEncryption.Decrypt(cipherText);
                            var plaintextAsTargetType = Convert.ChangeType(plaintext, encryptedAttribute.PlainTextTypeCode);
                            plaintextProperty.SetValue(entity, plaintextAsTargetType);
                        }
                    }
                }
            }
        }
    }
}