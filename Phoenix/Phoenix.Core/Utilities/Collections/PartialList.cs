﻿using System.Collections.Generic;
using System.Linq;

namespace Metier.Phoenix.Core.Utilities.Collections
{
    public class PartialList<T>
    {
        public PartialList()
        {
        }

        public PartialList(IEnumerable<T> items, int totalCount, string orderedBy = null)
        {
            TotalCount = totalCount;
            Items = items.ToList();
            OrderedBy = orderedBy;
        }

        public int TotalCount { get; set; }
        public string OrderedBy { get; set; }
        public List<T> Items { get; set; }
    }
}