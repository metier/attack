﻿using System;

namespace Metier.Phoenix.Core.Utilities.Sorting
{
    public class SortNumberedStrings : ISortNumberedStrings
    {
        public string PadInitialNumbersForSorting(string stringForSorting, bool isSortedAtTop)
        {
            if (isSortedAtTop)
            {
                return "9999" + stringForSorting;
            }

            if (string.IsNullOrEmpty(stringForSorting))
            {
                return stringForSorting;
            }

            stringForSorting = stringForSorting.TrimStart('0');

            const int numberOfLeadingZeros = 4;
            var leadingNumber = GetLeadingNumber(stringForSorting);
            var result = "";

            if (leadingNumber >= 0)
            {
                // Finding number of digits: https://stackoverflow.com/questions/4483886/how-can-i-get-a-count-of-the-total-number-of-digits-in-a-number
                var zeroszToPrepend = numberOfLeadingZeros - (int)Math.Floor(Math.Log10(leadingNumber) + 1);
                for (int i = 0; i < zeroszToPrepend; i++)
                {
                    result = result + "0";
                }
            }
            return result + stringForSorting;
        }

        private int GetLeadingNumber(string input)
        {
            char[] chars = input.ToCharArray();
            int lastValid = -1;

            for (int i = 0; i < chars.Length; i++)
            {
                if (Char.IsDigit(chars[i]))
                {
                    lastValid = i;
                }
                else
                {
                    break;
                }
            }

            if (lastValid >= 0)
            {
                return int.Parse(new string(chars, 0, lastValid + 1));
            }
            return -1;
        }
    }
}