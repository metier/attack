﻿namespace Metier.Phoenix.Core.Utilities.Sorting
{
    public interface ISortNumberedStrings
    {
        string PadInitialNumbersForSorting(string stringForSorting, bool isSortedAtTop);
    }
}