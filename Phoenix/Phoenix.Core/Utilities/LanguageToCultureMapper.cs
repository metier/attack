﻿using System.Globalization;

namespace Metier.Phoenix.Core.Utilities
{
    public static class LanguageToCultureMapper
    {
        public static CultureInfo GetCulture(string languageIdentifier)
        {
            switch (languageIdentifier)
            {
                case "DA":
                    return new CultureInfo("da-DK");
                case "NL":
                    return new CultureInfo("nl-NL");
                case "EN":
                    return new CultureInfo("en-US");
                case "DE":
                    return new CultureInfo("de-DE");
                case "NO":
                    return new CultureInfo("nb-NO");
                case "SV":
                    return new CultureInfo("sv-SE");
                case "FR":
                    return new CultureInfo("fr-FR");
                case "ES":
                    return new CultureInfo("es-ES");
                case "PL":
                    return new CultureInfo("pl-PL");
                case "IT":
                    return new CultureInfo("it-IT");
                case "PT":
                    return new CultureInfo("pt-PT");
                default:
                    return new CultureInfo("en-US");
            }
        }
    }
}