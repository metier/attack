﻿namespace Metier.Phoenix.Api.Messaging
{
    public interface IHandle<in T> where T : MessageBase
    {
        void Handle(T message);
    }
}