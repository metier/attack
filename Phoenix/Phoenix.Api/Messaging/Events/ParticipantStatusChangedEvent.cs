﻿namespace Metier.Phoenix.Api.Messaging.Events
{
    public class ParticipantStatusChangedEvent : MessageBase
    {
        private readonly int _participantId;
        private readonly string _newStatus;
        private readonly string _oldStatus;

        public int ParticipantId
        {
            get { return _participantId; }
        }

        public string NewStatus
        {
            get { return _newStatus; }
        }

        public string OldStatus
        {
            get { return _oldStatus; }
        }

        public ParticipantStatusChangedEvent(int participantId, string oldStatus, string newStatus)
        {
            _participantId = participantId;
            _oldStatus = oldStatus;
            _newStatus = newStatus;
        }
    }
}