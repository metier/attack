﻿namespace Metier.Phoenix.Api.Messaging.Events
{
    public class ActivityResourcesChangedEvent : MessageBase
    {
        public int ActivityId { get; set; }
    }
}