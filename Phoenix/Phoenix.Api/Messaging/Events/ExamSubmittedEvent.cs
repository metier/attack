﻿namespace Metier.Phoenix.Api.Messaging.Events
{
    public class ExamSubmittedEvent : MessageBase
    {
        public int ParticipantId { get; set; } 
        public int AttachmentId { get; set; } 
    }
}