﻿namespace Metier.Phoenix.Api.Messaging.Events
{
    public class ParticipantCreatedEvent : MessageBase
    {
        public int ParticipantId { get; set; }
    }
}