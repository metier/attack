﻿using System.Collections.Generic;

namespace Metier.Phoenix.Api.Messaging.Events
{
    public class UserEnrolledEvent : MessageBase
    {
        public List<int> ParticipantIds { get; set; }
    }
}