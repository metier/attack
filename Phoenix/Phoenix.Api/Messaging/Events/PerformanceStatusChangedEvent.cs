﻿namespace Metier.Phoenix.Api.Messaging.Events
{
    public class PerformanceStatusChangedEvent : MessageBase
    {
        public int PerformanceId { get; set; }
    }
}