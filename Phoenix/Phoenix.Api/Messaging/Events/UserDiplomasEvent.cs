﻿namespace Metier.Phoenix.Api.Messaging.Events
{
    public class UserDiplomasEvent : MessageBase
    {
        public int UserId { get; set; } 
        //public int FileId { get; set; }
        public int DiplomaId { get; set; } 
    }
}