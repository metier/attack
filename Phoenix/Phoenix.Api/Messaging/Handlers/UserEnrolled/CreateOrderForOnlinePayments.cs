﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data;
using NLog;

namespace Metier.Phoenix.Api.Messaging.Handlers.UserEnrolled
{
    public class CreateOrderForOnlinePayments : IHandle<UserEnrolledEvent>
    {
        private readonly MetierLmsContext _context;
        private readonly IOrdersProvider _ordersProvider;
        private readonly Logger _logger;

        public CreateOrderForOnlinePayments(MetierLmsContext context, IOrdersProvider ordersProvider)
        {
            _context = context;
            _ordersProvider = ordersProvider;
            _logger = LogManager.GetCurrentClassLogger();
        }

        public void Handle(UserEnrolledEvent message)
        {

            _logger.Info("CreateOrderForOnlinePayments JLS Event");
            //JVT: Don't auto-invoice credit card orders

            //var participantIdsWithPaymentReference = new List<int>();
            //try
            //{
            //    participantIdsWithPaymentReference = _context.Participants.Where(p => message.ParticipantIds.Contains(p.Id))
            //        .Where(p => !string.IsNullOrEmpty(p.OnlinePaymentReference))
            //        .Select(p => p.Id).ToList();

            //    var groupByKeys = _context.ProposedOrderLines.Where(ol => participantIdsWithPaymentReference.Contains(ol.ParticipantId))
            //        .Select(ol => ol.GroupByKey).Distinct()
            //        .ToList();

            //    if (groupByKeys.Count > 1)
            //    {
            //        _logger.Warn("User enrolled event was handled with a list of participants mapping to more than one groupbykey. " +
            //                     "Normally such an event should map to a single groupbykey (participant IDs: {0})",
            //            string.Join(",", participantIdsWithPaymentReference));
            //    }

            //    foreach (var groupByKey in groupByKeys)
            //    {
            //        _ordersProvider.InvoiceByGroupByKey(groupByKey);
            //    }
            //}
            //catch (Exception e)
            //{
            //    _logger.Error(string.Format("Failed to invoice one or more participants with online payment reference (participant ids: {0})", string.Join(",", participantIdsWithPaymentReference)), e);
            //}
        }
    }
}