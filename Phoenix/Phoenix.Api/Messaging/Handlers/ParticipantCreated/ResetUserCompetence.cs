﻿using System.Linq;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Core.Data;

namespace Metier.Phoenix.Api.Messaging.Handlers.ParticipantCreated
{
    public class ResetUserCompetence : IHandle<ParticipantCreatedEvent>
    {
        private readonly MetierLmsContext _context;

        public ResetUserCompetence(MetierLmsContext context)
        {
            _context = context;
        }

        public void Handle(ParticipantCreatedEvent message)
        {
            var participant = _context.Participants.FirstOrDefault(p => p.Id == message.ParticipantId);
            if (participant != null)
            {
                var article = _context.Activities.Where(a => a.Id == participant.ActivityId)
                                .Join(_context.CustomerArticles, a => a.CustomerArticleId, ca => ca.Id, (a, ca) => new { CustomerArticle = ca })
                                .Join(_context.Articles, x => x.CustomerArticle.ArticleCopyId, a => a.Id, (ca, a) => a)
                                .FirstOrDefault();

                if (article != null && article.IsUserCompetenceInfoRequired)
                {
                    var userCompetence = _context.UserCompetences.FirstOrDefault(uc => uc.UserId == participant.UserId);
                    if (userCompetence != null && userCompetence.IsTermsAccepted)
                    {
                        userCompetence.IsTermsAccepted = false;
                        userCompetence.TermsAcceptedDate = null;
                        _context.SaveChanges();
                    }
                }
            }
        }
    }
}