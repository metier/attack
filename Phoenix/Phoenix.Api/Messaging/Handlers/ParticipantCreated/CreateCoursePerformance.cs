﻿using System.Linq;
using Castle.Core.Logging;
using Metier.Phoenix.Api.Data;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using NLog;

namespace Metier.Phoenix.Api.Messaging.Handlers.ParticipantCreated
{
    public class CreateCoursePerformance : IHandle<ParticipantCreatedEvent>
    {
        private readonly MetierLmsContext _context;
        private readonly IEventAggregator _eventAggregator;
        private readonly Logger _logger;

        public CreateCoursePerformance(MetierLmsContext context, IEventAggregator eventAggregator)
        {
            _context = context;
            _eventAggregator = eventAggregator;
            _logger = LogManager.GetCurrentClassLogger();

        }

        public void Handle(ParticipantCreatedEvent message)
        {
            _logger.Info("CreateCoursePerformance JLS");
            var participant = _context.Participants.First(p => p.Id == message.ParticipantId && !p.IsDeleted);
            var activity = _context.Activities.First(a => a.Id == participant.ActivityId);
            if (activity.RcoCourseId.HasValue)
            {
                var performance = _context.ScormPerformances.FirstOrDefault(p => p.UserId == participant.UserId && p.RcoId == activity.RcoCourseId.Value);
                if (performance == null)
                {
                    performance = new ScormPerformance
                    {
                        UserId = participant.UserId, 
                        RcoId = activity.RcoCourseId.Value, 
                        Status = ScormLessonStatus.NotAttempted,
                        OriginalStatus = ScormLessonStatus.NotAttempted
                    };

                    _context.ScormPerformances.Add(performance);
                    _context.SaveChanges();
                    
                    _eventAggregator.Publish(new PerformanceStatusChangedEvent { PerformanceId = performance.Id});
                }
            }
        }
    }
}