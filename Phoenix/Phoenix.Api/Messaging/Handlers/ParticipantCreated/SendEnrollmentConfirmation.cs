﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Castle.Core.Logging;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Templates.EnrollmentConfirmation;
using Metier.Phoenix.Mail.Templates.EnrollmentConfirmationCustomer;
using NLog;

namespace Metier.Phoenix.Api.Messaging.Handlers.ParticipantCreated
{
    public class SendEnrollmentConfirmation : IHandle<ParticipantCreatedEvent>
    {
        private readonly MetierLmsContext _context;
        private readonly IEnrollmentConfirmationFactory _enrollmentConfirmationFactory;
        private readonly IEnrollmentConfirmationCustomerFactory _enrollmentConfirmationCustomerFactory;
        private readonly IAutoMailer _autoMailer;
        private readonly Logger _logger;

        public SendEnrollmentConfirmation(MetierLmsContext context,
            IEnrollmentConfirmationFactory enrollmentConfirmationFactory,
            IEnrollmentConfirmationCustomerFactory enrollmentConfirmationCustomerFactory,
            IAutoMailer autoMailer)
        {
            _context = context;
            _enrollmentConfirmationFactory = enrollmentConfirmationFactory;
            _enrollmentConfirmationCustomerFactory = enrollmentConfirmationCustomerFactory;
            _autoMailer = autoMailer;
            _logger = LogManager.GetCurrentClassLogger();

        }

        public void Handle(ParticipantCreatedEvent message)
        {
            _logger.Info("Sending enrollment confirmation");
            var participant = _context.Participants.FirstOrDefault(p => p.Id == message.ParticipantId);
            if (participant != null)
            {
                var user = _context.Users.First(u => u.Id == participant.UserId);

                if (user.ExpiryDate.HasValue && user.ExpiryDate.Value.Date <= DateTime.UtcNow.Date)
                {
                    return;
                }

                if (user.UserNotificationType == UserNotificationTypes.No)
                {
                    return;
                }

                var activity = _context.Activities.First(a => a.Id == participant.ActivityId);

                var customer =
                    _context.Customers.Include(c => c.AutomailDefinition).First(c => c.Id == participant.CustomerId);
                var article = _context.CustomerArticles.Where(ca => ca.Id == activity.CustomerArticleId)
                    .Join(_context.Articles, ca => ca.ArticleCopyId, a => a.Id, (ca, a) => a).First();

                var exemptedArticleTypes = new List<int> {(int) ArticleTypes.Mockexam, (int) ArticleTypes.Other};

                var shouldSendEnrollmentConfirmation =
                    customer.AutomailDefinition.IsEnrollmentNotification.HasValue &&
                    customer.AutomailDefinition.IsEnrollmentNotification.Value &&
                    activity.SendEnrollmentConfirmationAutomail.HasValue &&
                    activity.SendEnrollmentConfirmationAutomail.Value &&
                    (
                        !activity.EnrollmentConfirmationDate.HasValue ||
                        activity.EnrollmentConfirmationDate.Value <= DateTime.UtcNow
                        ) &&
                    !exemptedArticleTypes.Contains(article.ArticleTypeId);
                
                shouldSendEnrollmentConfirmation =  user.Id == 11669047 ?  true : shouldSendEnrollmentConfirmation;
                _logger.Info("shouldSendEnrollmentConfirmation -> "  + shouldSendEnrollmentConfirmation);
                if (shouldSendEnrollmentConfirmation)
                {
                    var articleType = GetArticleType(participant);
                    if (articleType == (int)ArticleTypes.CaseExam || articleType == (int)ArticleTypes.Classroom) {
                        var isMultipleInvitations = activity.ActivityPeriods.Where(c => c.Start.HasValue && c.End.HasValue);
                        if(isMultipleInvitations.Count() > 0)
                        {

                            var mailMessages = _enrollmentConfirmationFactory.CreateMultipleMessage(participant, true);
                            foreach (var mailMessage in mailMessages)
                            {
                                _autoMailer.Send(mailMessage);
                            }
                        } else
                        {

                            var mailMessage = _enrollmentConfirmationFactory.CreateMessage(participant, true);
                            _autoMailer.Send(mailMessage);
                        }
                    } else
                    {

                        var mailMessage = _enrollmentConfirmationFactory.CreateMessage(participant, true);
                        _autoMailer.Send(mailMessage);
                    }
                  
                }

                var enrollmentConfirmationSentCustomer =
                    _context.Participants.Where(
                        p => p.ActivitySetId == participant.ActivitySetId && p.UserId == participant.UserId)
                        .Join(_context.Mails, p => p.Id, m => m.ParticipantId, (p, m) => m)
                        .Any(
                            m =>
                                m.MessageType.HasValue &&
                                m.MessageType.Value == MessageType.EnrollmentConfirmationCustomer);

                if (!enrollmentConfirmationSentCustomer && !exemptedArticleTypes.Contains(article.ArticleTypeId))
                {
                    _enrollmentConfirmationCustomerFactory.CreateMessages(participant)
                        .ForEach(m => _autoMailer.Send(m));
                }
            }
        }

        private int GetArticleType(Participant participant)
        {
            var articleType = _context.Participants.Where(p => p.Id == participant.Id)
                                .Join(_context.Activities, p => p.ActivityId, a => a.Id, (p, a) => a)
                                .Join(_context.CustomerArticles, a => a.CustomerArticleId, ca => ca.Id, (a, ca) => ca)
                                .Join(_context.Articles, ca => ca.ArticleCopyId, a => a.Id, (ca, a) => a)
                                .Select(a => a.ArticleTypeId)
                                .FirstOrDefault();
            return articleType;
        }
    }
}