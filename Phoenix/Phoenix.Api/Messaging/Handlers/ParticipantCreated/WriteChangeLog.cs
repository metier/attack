﻿using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Core.Business.ChangeLog;
using NLog;

namespace Metier.Phoenix.Api.Messaging.Handlers.ParticipantCreated
{
    public class WriteChangeLog : IHandle<ParticipantCreatedEvent>
    {
        private readonly IChangeLogProvider _changeLogProvider;
        private readonly Logger _logger;

        public WriteChangeLog(IChangeLogProvider changeLogProvider)
        {
            _changeLogProvider = changeLogProvider;
        }

        public void Handle(ParticipantCreatedEvent message)
        {
            _changeLogProvider.Write(message.ParticipantId, 
                ChangeLogEntityTypes.Participant, 
                ChangeLogTypes.Created,
                "Participant enrolled");
        }
    }
}