﻿using System;
using System.Linq;
using System.Web.Helpers;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using NLog;

namespace Metier.Phoenix.Api.Messaging.Handlers.PerformanceStatusChanged
{
    public class UpdateRelatedPerformances : IHandle<PerformanceStatusChangedEvent>
    {
        private readonly MetierLmsContext _context;
        private readonly IEventAggregator _eventAggregator;
        private readonly IPerformanceProvider _performanceProvider;
        private readonly IParticipantsProvider _participantsProvider;
        private readonly IRcoProvider _rcoProvider;
        private readonly IScormProvider _scormProvider;        
        private readonly Logger _logger;

        public UpdateRelatedPerformances(MetierLmsContext context, 
                                         IEventAggregator eventAggregator, 
                                         IPerformanceProvider performanceProvider, 
                                         IParticipantsProvider participantsProvider, 
                                         IRcoProvider rcoProvider,
                                         IScormProvider scormProvider)
        {
            _context = context;
            _eventAggregator = eventAggregator;
            _performanceProvider = performanceProvider;
            _participantsProvider = participantsProvider;
            _rcoProvider = rcoProvider;
            _scormProvider = scormProvider;
            _logger = LogManager.GetCurrentClassLogger();
        }

        public void Handle(PerformanceStatusChangedEvent message)
        {
            var performance = _context.ScormPerformances.FirstOrDefault(p => p.Id == message.PerformanceId);
            if (performance == null) return;

            _logger.Debug("Performance (ID: {0}) changed status to '{1}'", message.PerformanceId, performance.Status);

            var rco = _context.Rcos.FirstOrDefault(r => r.Id == performance.RcoId);
            if (rco == null) return;

            if (rco.Context == "course" || rco.Context == "examcontainer")
            {
                _participantsProvider.UpdateStatus(performance.UserId, performance.RcoId);
            }
            else if (rco.IsFinalTest.GetValueOrDefault())
            {
                if (performance.Status == ScormLessonStatus.Complete)
                {
                    UpdateLessonsStatus(performance.UserId, performance.RcoId, ScormLessonStatus.Complete);
                    UpdateParentStatus(performance.UserId, performance.RcoId, ScormLessonStatus.Complete);
                }
                else if (performance.Status == ScormLessonStatus.Incomplete)
                {
                    UpdateParentStatus(performance.UserId, performance.RcoId, ScormLessonStatus.Incomplete);
                }
                else if (performance.Status == ScormLessonStatus.NotAttempted)
                {
                    UpdateParentStatus(performance.UserId, performance.RcoId, ScormLessonStatus.NotAttempted);
                    
                    //If this is a final test and status is Not Attempted, insert empty attempt
                    _scormProvider.ResetAttemptStatusIfNecessary(performance.Id);
                }
            }
            else
            {
                if (
                    (rco.Context == "lesson" && AreAllLessonsCompleted(performance.UserId, performance.RcoId) ||
                    (rco.Context == "exam" && AreAllExamsCompleted(performance.UserId, performance.RcoId)))
                    )
                {
                    UpdateParentStatus(performance.UserId, performance.RcoId, ScormLessonStatus.Complete);
                }
                else if (performance.Status == ScormLessonStatus.Incomplete)
                {
                    UpdateParentStatus(performance.UserId, performance.RcoId, ScormLessonStatus.Incomplete);
                }
                else if (performance.Status == ScormLessonStatus.Complete)
                {
                    UpdateParentStatus(performance.UserId, performance.RcoId, ScormLessonStatus.Incomplete);
                }
            }
        }

        private bool AreAllLessonsCompleted(int userId, int rcoId)
        {
            var performances = _performanceProvider.GetPerformancesForAllLessons(userId, rcoId);
            return performances.All(p => p.Status == ScormLessonStatus.Complete);
        }

        private bool AreAllExamsCompleted(int userId, int rcoId)
        {
            var performances = _performanceProvider.GetPerformancesForAllExams(userId, rcoId);
            return performances.All(p => p.Status == ScormLessonStatus.Complete);
        }
        
        private void UpdateLessonsStatus(int userId, int rcoId, string status)
        {
            var lessons = _rcoProvider.GetCourseRcos(rcoId).ToList();
            foreach (var lesson in lessons)
            {
                var performance = _context.ScormPerformances.FirstOrDefault(p => p.RcoId == lesson.Id && p.UserId == userId);
                if (performance == null)
                {
                    performance = CreatePerformance(userId, lesson.Id);
                }
                var isPerformanceStatusChanged = performance.Status != status;
                if (isPerformanceStatusChanged)
                {
                    performance.Status = status;

                    if (performance.Status == ScormLessonStatus.Complete)
                    {
                        performance.CompletedDate = DateTime.UtcNow;
                        performance.TimeUsedInSeconds = 0;
                    }

                    _context.SaveChanges();
                    _eventAggregator.Publish(new PerformanceStatusChangedEvent { PerformanceId = performance.Id });    
                }
            }
        }
        
        private void UpdateParentStatus(int userId, int rcoId, string status)
        {
            var parentRco = _rcoProvider.GetTopParent(rcoId);
            if (parentRco.Id == rcoId)
            {
                return;
            }

            var performance = _context.ScormPerformances.FirstOrDefault(p => p.RcoId == parentRco.Id && p.UserId == userId);
            if (performance == null)
            {
                performance = CreatePerformance(userId, parentRco.Id);
            }

            if (performance.Status == ScormLessonStatus.Complete && status == ScormLessonStatus.Complete)
            {
                return;
            }

            var isParentPerformanceStatusChanged = performance.Status != status;
            if (isParentPerformanceStatusChanged)
            {
                performance.Status = status;

                if (performance.Status == ScormLessonStatus.Complete)
                {
                    performance.CompletedDate = DateTime.UtcNow;
                    performance.TimeUsedInSeconds = 0;
                }
                _context.SaveChanges();
                _eventAggregator.Publish(new PerformanceStatusChangedEvent { PerformanceId = performance.Id });
            }
        }

        private ScormPerformance CreatePerformance(int userId, int rcoId)
        {
            return _performanceProvider.Create(new ScormPerformance
            {
                RcoId = rcoId,
                UserId = userId,
                Status = ScormLessonStatus.NotAttempted,
                OriginalStatus = ScormLessonStatus.NotAttempted
            });
        }
    }
}