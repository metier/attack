﻿using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Api.Messaging.Handlers
{
    public class AllocateExaminersToExamParticipants : IHandle<ActivityResourcesChangedEvent>, IHandle<ParticipantCreatedEvent>
    {
        private readonly MetierLmsContext _context;
        private readonly IArticleRepository _articleRepository;
        
        private const int ExaminerResourceTypeId = 5;

        public AllocateExaminersToExamParticipants(MetierLmsContext context, IArticleRepository articleRepository)
        {
            _context = context;
            _articleRepository = articleRepository;
        }

        public void Handle(ParticipantCreatedEvent message)
        {
            var participant = _context.Participants.Single(p => p.Id == message.ParticipantId);
            AllocateExaminersForActivity(participant.ActivityId);
        }

        public void Handle(ActivityResourcesChangedEvent message)
        {
            AllocateExaminersForActivity(message.ActivityId);
        }

        internal void AllocateExaminersForActivity(int activityId)
        {
            var article = _articleRepository.FindByActivityId(activityId);
            if (ShouldAllocateExaminer(article))
            {
                var activity = _context.Activities.Include(a => a.Resources)
                    .Include(a => a.Participants)
                    .First(a => a.Id == activityId);

                var examinerResources = activity.Resources.Where(r => r.ResourceTypeId == ExaminerResourceTypeId).ToList();
                var participants = activity.Participants.Where(p => !p.IsDeleted && string.IsNullOrEmpty(p.ExamGrade)).ToList();
                for (var i = 0; i < participants.Count; i++)
                {
                    if (examinerResources.Count == 0)
                    {
                        participants[i].ExaminerResourceId = null;
                    }
                    else
                    {
                        participants[i].ExaminerResourceId = examinerResources[i%examinerResources.Count].Id;
                    }
                }
                _context.SaveChanges();
            }
        }

        private bool ShouldAllocateExaminer(Article article)
        {
            return article.ArticleTypeId == (int)ArticleTypes.CaseExam || 
                   article.ArticleTypeId == (int)ArticleTypes.ProjectAssignment;
        }
    }
}