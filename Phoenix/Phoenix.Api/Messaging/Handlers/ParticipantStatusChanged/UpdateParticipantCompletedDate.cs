﻿using System;
using System.Linq;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;

namespace Metier.Phoenix.Api.Messaging.Handlers.ParticipantStatusChanged
{
    public class UpdateParticipantCompletedDate : IHandle<ParticipantStatusChangedEvent>
    {
        private readonly MetierLmsContext _context;

        public UpdateParticipantCompletedDate(MetierLmsContext context)
        {
            _context = context;
        }

        public void Handle(ParticipantStatusChangedEvent message)
        {
            if (message.NewStatus == message.OldStatus)
            {
                return;
            }

            var participant = _context.Participants.FirstOrDefault(p => p.Id == message.ParticipantId && !p.IsDeleted);
            if (participant == null)
            {
                return;
            }
            
            if (message.NewStatus == ParticipantStatusCodes.Completed)
            {
                participant.CompletedDate = DateTime.UtcNow;
            }
            else
            {
                participant.CompletedDate = null;
            }
            _context.SaveChanges();
        }
    }
}