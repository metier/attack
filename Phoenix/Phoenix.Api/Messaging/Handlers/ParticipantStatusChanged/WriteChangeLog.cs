﻿using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Core.Business.ChangeLog;
using Metier.Phoenix.Core.Data.Codes;

namespace Metier.Phoenix.Api.Messaging.Handlers.ParticipantStatusChanged
{
    public class WriteChangeLog : IHandle<ParticipantStatusChangedEvent>
    {
        private readonly IChangeLogProvider _changeLogProvider;

        public WriteChangeLog(IChangeLogProvider changeLogProvider)
        {
            _changeLogProvider = changeLogProvider;
        }

        public void Handle(ParticipantStatusChangedEvent message)
        {
            _changeLogProvider.Write(message.ParticipantId, 
                ChangeLogEntityTypes.Participant, 
                ChangeLogTypes.StatusChanged,
                "Status changed to \"{0}\"", ParticipantStatusCodes.GetHumanFriendly(message.NewStatus));
        }
    }
}