﻿using System;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Templates.ExamSubmitted;

namespace Metier.Phoenix.Api.Messaging.Handlers.ExamSubmitted
{
    public class SendExamSubmittedAutomail : IHandle<ExamSubmittedEvent>
    {
        private readonly MetierLmsContext _context;
        private readonly IAutoMailer _autoMailer;
        private readonly IExamSubmittedMessageFactory _examSubmittedMessageFactory;

        public SendExamSubmittedAutomail(MetierLmsContext context, IAutoMailer autoMailer, IExamSubmittedMessageFactory examSubmittedMessageFactory)
        {
            _context = context;
            _autoMailer = autoMailer;
            _examSubmittedMessageFactory = examSubmittedMessageFactory;
        }

        public void Handle(ExamSubmittedEvent message)
        {
            var participant = _context.Participants.First(p => p.Id == message.ParticipantId);
            if (!ShouldSendAutomail(participant)) return;
            
            var mailMessage = _examSubmittedMessageFactory.CreateMessage(participant);
            _autoMailer.Send(mailMessage);
        }

        private bool ShouldSendAutomail(Participant participant)
        {
            var customer = _context.Customers.Include(c => c.AutomailDefinition).First(c => c.Id == participant.CustomerId);
            if (!customer.AutomailDefinition.IsExamSubmissionNotification.HasValue || !customer.AutomailDefinition.IsExamSubmissionNotification.Value)
            {
                return false;
            }
            var user = _context.Users.First(u => u.Id == participant.UserId);
            if (user.UserNotificationType == UserNotificationTypes.No)
            {
                return false;
            }
            if (user.ExpiryDate.HasValue && user.ExpiryDate.Value.Date <= DateTime.UtcNow.Date)
            {
                return false;
            }
            var activity = _context.Activities.First(a => a.Id == participant.ActivityId);
            if (!activity.SendExamSubmissionAutomail.HasValue || !activity.SendExamSubmissionAutomail.Value)
            {
                return false;
            }
            return true;
        }
    }
}