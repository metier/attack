﻿using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Core.Business.ChangeLog;

namespace Metier.Phoenix.Api.Messaging.Handlers.ExamSubmitted
{
    public class WriteChangeLog : IHandle<ExamSubmittedEvent>
    {
        private readonly IChangeLogProvider _changeLogProvider;

        public WriteChangeLog(IChangeLogProvider changeLogProvider)
        {
            _changeLogProvider = changeLogProvider;
        }

        public void Handle(ExamSubmittedEvent message)
        {
            _changeLogProvider.Write(message.ParticipantId, 
                ChangeLogEntityTypes.Participant, 
                ChangeLogTypes.ExamSubmitted,
                "Exam submitted");
        }
    }
}