﻿using System;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Templates.UserDiplomas;

namespace Metier.Phoenix.Api.Messaging.Handlers.SendUserDiplomas
{
    public class SendUserDiplomasAutomail : IHandle<UserDiplomasEvent>
    {
        private readonly MetierLmsContext _context;
        private readonly IAutoMailer _autoMailer;
        private readonly IUserDiplomasMessageFactory _userDiplomaMessageFactory;

        public SendUserDiplomasAutomail(MetierLmsContext context, IAutoMailer autoMailer, IUserDiplomasMessageFactory userDiplomaMessageFactory)
        {
            _context = context;
            _autoMailer = autoMailer;
            _userDiplomaMessageFactory = userDiplomaMessageFactory;
        }

        public void Handle(UserDiplomasEvent message)
        {
            var participant = _context.Users.First(p => p.Id == message.UserId);
            if (!ShouldSendAutomail(participant)) return;
            
            var userDiplomaInformation = _context.UserDiplomas.First(p => p.Id == message.DiplomaId);

            var mailMessage = _userDiplomaMessageFactory.CreateMessage(participant, userDiplomaInformation);
            _autoMailer.Send(mailMessage);
        }

        private bool ShouldSendAutomail(User user)
        {
            var customer = _context.Customers.Include(c => c.AutomailDefinition).First(c => c.Id == user.CustomerId);
            if (!customer.AutomailDefinition.IsExamSubmissionNotification.HasValue || !customer.AutomailDefinition.IsExamSubmissionNotification.Value)
            {
                return false;
            }
            if (user.UserNotificationType == UserNotificationTypes.No)
            {
                return false;
            }
            if (user.ExpiryDate.HasValue && user.ExpiryDate.Value.Date <= DateTime.UtcNow.Date)
            {
                return false;
            }
            return true;
        }
    }
}