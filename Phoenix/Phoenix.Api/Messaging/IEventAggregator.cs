﻿namespace Metier.Phoenix.Api.Messaging
{
    public interface IEventAggregator
    {
        void Publish<T>(T message) where T : MessageBase;
    }
}