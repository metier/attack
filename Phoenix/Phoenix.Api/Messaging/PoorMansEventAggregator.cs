﻿using Castle.MicroKernel;

namespace Metier.Phoenix.Api.Messaging
{
    /// <summary>
    /// Poor mans EventAggregator - A simple implementation of EventAggregator.
    /// Will forward messages to types implementing the IHandle interface
    /// </summary>
    public class PoorMansEventAggregator : IEventAggregator
    {
        private readonly IKernel _kernel;

        public PoorMansEventAggregator(IKernel kernel)
        {
            _kernel = kernel;
        }

        public void Publish<T>(T message) where T : MessageBase
        {
            var handlers = _kernel.ResolveAll<IHandle<T>>();
            foreach (var handler in handlers)
            {
                handler.Handle(message);
                _kernel.ReleaseComponent(handler);
            }
        }
    }
}