﻿using System.Configuration;

namespace Metier.Phoenix.Api.Configuration.ConfigurationSections
{
    public class PhoenixConfigurationSection :  ConfigurationSection
    {
        [ConfigurationProperty("sourceSystems")]
        public SourceSystemConfigurationElementCollection SourceSystems
        {
            get
            {
                return this["sourceSystems"] as SourceSystemConfigurationElementCollection;
            }
        }

        [ConfigurationProperty("passwordResetTokenExpirationInMinutes")]
        public int PasswordResetTokenExpirationInMinutes
        {
            get
            {
                var expirationInMinutes = this["passwordResetTokenExpirationInMinutes"] as int?;
                return expirationInMinutes.HasValue ? expirationInMinutes.Value : 0;
            }
        }
    }
}