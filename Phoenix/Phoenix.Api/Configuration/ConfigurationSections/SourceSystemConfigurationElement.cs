﻿using System.Configuration;

namespace Metier.Phoenix.Api.Configuration.ConfigurationSections
{
    public class SourceSystemConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get
            {
                return this["name"] as string;
            }
        }

        [ConfigurationProperty("passwordResetFormUrl", IsRequired = false)]
        public string PasswordResetFormUrl
        {
            get
            {
                return this["passwordResetFormUrl"] as string;
            }
        }
    }
}