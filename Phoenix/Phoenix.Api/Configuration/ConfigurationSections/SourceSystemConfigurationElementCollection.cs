﻿using System.Configuration;

namespace Metier.Phoenix.Api.Configuration.ConfigurationSections
{
    public class SourceSystemConfigurationElementCollection : ConfigurationElementCollection
    {
        public SourceSystemConfigurationElement this[int index]
        {
            get
            {
                return BaseGet(index) as SourceSystemConfigurationElement;
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new SourceSystemConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SourceSystemConfigurationElement)element).Name;
        } 
    }
}