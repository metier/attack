﻿using System.Web.Http.Controllers;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Metier.Phoenix.Api.Agresso.CustomerService;
using Metier.Phoenix.Api.Agresso.ImportService;
using Metier.Phoenix.Api.Agresso.QueryEngine;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Api.Factories;
using Metier.Phoenix.Api.Factories.Exam;
using Metier.Phoenix.Api.Factories.ExamPlayer;
using Metier.Phoenix.Api.Messaging;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Reporting;
using Metier.Phoenix.Api.Reporting.Reports;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Api.Services.Agresso.Configuration;
using Metier.Phoenix.Api.Services.Agresso.Configuration.Norway;
using Metier.Phoenix.Api.Services.Agresso.Configuration.Sweden;
using Metier.Phoenix.Api.Services.Agresso.Customers;
using Metier.Phoenix.Api.Services.Agresso.Invoicing;
using Metier.Phoenix.Api.Services.Agresso.Invoicing.Factories;
using Metier.Phoenix.Api.Services.Agresso.Invoicing.Xml;
using Metier.Phoenix.Api.Services.ExternalCustomers;
using Metier.Phoenix.Api.Services.ExternalCustomers.Providers.Agresso.Norway;
using Metier.Phoenix.Api.Services.ExternalCustomers.Providers.Agresso.Sweden;
using Metier.Phoenix.Api.Services.ExternalCustomers.Providers.Default;
using Metier.Phoenix.Api.Services.Invoices;
using Metier.Phoenix.Api.Services.Invoices.DefaultOrderService;
using Metier.Phoenix.Core.Business;
using Metier.Phoenix.Core.Business.ChangeLog;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats.Excel;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats.Csv;
using Metier.Phoenix.Core.Business.FileStorage;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Exams;
using Metier.Phoenix.Core.Security;
using Metier.Phoenix.Core.Utilities.Sorting;

namespace Metier.Phoenix.Api.Configuration
{
    public class ComponentsInstaller : IWindsorInstaller
    {
        public virtual void Install(IWindsorContainer container, IConfigurationStore store)
        {
            Register(container);
            RegisterAllControllers(container);
            RegisterValidators(container);

            RegisterExternalCustomersServices(container);
            RegisterInvoicesServices(container);

            RegisterMessageHandlers(container);
            RegisterReporting(container);
        }

        private void RegisterReporting(IWindsorContainer container)
        {
            container.Register(
                Component.For<IExcelFactory>().ImplementedBy<ExcelFactory>().LifestyleTransient(),
                Component.For<ICsvFactory>().ImplementedBy<CsvFactory>().LifestyleTransient(),
                Component.For<IReportFileProvider>().ImplementedBy<ReportFileProvider>().LifestyleTransient()
                );

            container.Register(Classes.FromThisAssembly()
                .BasedOn(typeof(IReportProvider<>))
                .WithService.AllInterfaces().LifestyleTransient());
        }

        private void RegisterMessageHandlers(IWindsorContainer container)
        {
            container.Register(Component.For<IEventAggregator>().ImplementedBy<PoorMansEventAggregator>());

            container.Register(Classes.FromThisAssembly()
                .BasedOn(typeof(IHandle<>))
                .WithService.AllInterfaces().LifestyleTransient());
        }

        private void RegisterValidators(IWindsorContainer container)
        {
            container.Register(
                Component.For<IValidate<AcceptedEmailAddress>>().ImplementedBy<AcceptedEmailAddressValidator>().LifestyleTransient()
                , Component.For<IValidate<CustomerContactPerson>>().ImplementedBy<CustomerContactPersonValidator>().LifestyleTransient()
                , Component.For<IValidate<Customer>>().ImplementedBy<CustomerValidator>().LifestyleTransient()
                , Component.For<IValidate<ParticipantInfoDefinition>>().ImplementedBy<ParticipantInfoDefinitionValidator>().LifestyleTransient()
                , Component.For<IValidateActivity>().ImplementedBy<ActivityValidator>().LifestyleTransient()
                , Component.For<IValidate<ActivitySet>>().ImplementedBy<ActivitySetValidator>().LifestyleTransient()
                , Component.For<IValidateOrder>().ImplementedBy<OrderValidator>().LifestyleTransient()
                , Component.For<IValidateParticipant>().ImplementedBy<ParticipantValidator>().LifestyleTransient()
                , Component.For<IValidate<Question>>().ImplementedBy<QuestionValidator>().LifestyleTransient()
                , Component.For<IValidate<Tag>>().ImplementedBy<TagsValidator>().LifestyleTransient()
                );
        }

        private static void RegisterAllControllers(IWindsorContainer container)
        {
            container.Register(Classes.FromThisAssembly().BasedOn<IHttpController>().LifestyleTransient());
        }

        private static void Register(IWindsorContainer container)
        {
            container.Register(
                Component.For<MetierLmsContext>().DependsOn(Dependency.OnValue("connectionString", "Name=MetierLmsContext")).LifestyleScoped()
                , Component.For<ISortNumberedStrings>().ImplementedBy<SortNumberedStrings>()
                , Component.For<IHttpContextProvider>().ImplementedBy<HttpContextProvider>()
                , Component.For<ICustomersProvider>().ImplementedBy<CustomersProvider>().LifestyleTransient()
                , Component.For<IIdentityProvider>().ImplementedBy<IdentityProvider>().LifestyleTransient()
                , Component.For<ICustomerRepository>().ImplementedBy<CustomerRepository>().LifestyleTransient()
                , Component.For<ILeadingTextRepository>().ImplementedBy<LeadingTextRepository>().LifestyleTransient()
                , Component.For<ICodeRepository>().ImplementedBy<CodeRepository>().LifestyleTransient()
                , Component.For<IDistributorRepository>().ImplementedBy<DistributorRepository>().LifestyleTransient()
                , Component.For<IProductRepository>().ImplementedBy<ProductRepository>().LifestyleTransient()
                , Component.For<IProductDescriptionRepository>().ImplementedBy<ProductDescriptionRepository>().LifestyleTransient()
                , Component.For<IProductCategoryRepository>().ImplementedBy<ProductCategoryRepository>().LifestyleTransient()
                , Component.For<IProductTypeRepository>().ImplementedBy<ProductTypeRepository>().LifestyleTransient()
                , Component.For<IAttachmentRepository>().ImplementedBy<AttachmentRepository>().LifestyleTransient()
                , Component.For<IArticleRepository>().ImplementedBy<ArticleRepository>().LifestyleTransient()
                , Component.For<ILanguageRepository>().ImplementedBy<LanguageRepository>().LifestyleTransient()
                , Component.For<IAccountsProvider>().ImplementedBy<AccountsProvider>().LifestyleTransient()
                , Component.For<IUsersRepository>().ImplementedBy<UsersRepository>().LifestyleTransient()
                , Component.For<IHelpTextRepository>().ImplementedBy<HelpTextRepository>().LifestyleTransient()
                , Component.For<ICourseProgramRepository>().ImplementedBy<CourseProgramRepository>().LifestyleTransient()
                , Component.For<ICustomerArticleRepository>().ImplementedBy<CustomerArticleRepository>().LifestyleTransient()
                , Component.For<ICustomerArticlesProvider>().ImplementedBy<CustomerArticlesProvider>().LifestyleTransient()
                , Component.For<IResourceRepository>().ImplementedBy<ResourceRepository>().LifestyleTransient()
                , Component.For<IPhoenixMembership>().ImplementedBy<PhoenixMembership>().LifestyleTransient()
                , Component.For<IPhoenixMembershipRoles>().ImplementedBy<PhoenixMembershipRoles>().LifestyleTransient()
                , Component.For<IConfiguration>().ImplementedBy<PhoenixConfiguration>().LifestyleTransient()
                , Component.For<ICommentsRepository>().ImplementedBy<CommentsRepository>().LifestyleTransient()
                , Component.For<IActivityRepository>().ImplementedBy<ActivityRepository>().LifestyleTransient()
                , Component.For<IActivitySetRepository>().ImplementedBy<ActivitySetRepository>().LifestyleTransient()
                , Component.For<IActivitySetProvider>().ImplementedBy<ActivitySetProvider>().LifestyleTransient()
                , Component.For<IParticipantRepository>().ImplementedBy<ParticipantRepository>().LifestyleTransient()
                , Component.For<IParticipantFactory>().ImplementedBy<ParticipantFactory>().LifestyleTransient()
                , Component.For<IParticipantsProvider>().ImplementedBy<ParticipantsProvider>().LifestyleTransient()
                , Component.For<IOrderRepository>().ImplementedBy<OrderRepository>().LifestyleTransient()
                , Component.For<IOrderFactory>().ImplementedBy<OrderFactory>().LifestyleTransient()
                , Component.For<IOrderInfoFactory>().ImplementedBy<OrderInfoFactory>().LifestyleTransient()
                , Component.For<IActivityProvider>().ImplementedBy<ActivityProvider>().LifestyleTransient()
                , Component.For<IAuthenticationTokenRepository>().ImplementedBy<AuthenticationTokenRepository>().LifestyleTransient()
                , Component.For<IScormAttemptRepository>().ImplementedBy<ScormAttemptRepository>().LifestyleTransient()
                , Component.For<IScormProvider>().ImplementedBy<ScormProvider>().LifestyleTransient()
                , Component.For<IRcoRepository>().ImplementedBy<RcoRepository>().LifestyleTransient()
                , Component.For<IRcoProvider>().ImplementedBy<RcoProvider>().LifestyleTransient()
                , Component.For<IPerformanceProvider>().ImplementedBy<PerformanceProvider>().LifestyleTransient()
                , Component.For<IPerformanceSummaryFactory>().ImplementedBy<PerformanceSummaryFactory>().LifestyleTransient()
                , Component.For<IAuthorizationProvider>().ImplementedBy<AuthorizationProvider>().LifestyleTransient()
                , Component.For<IMailsProvider>().ImplementedBy<MailsProvider>().LifestyleTransient()
                , Component.For<IUserCompetenceProvider>().ImplementedBy<UserCompetenceProvider>().LifestyleTransient()
                , Component.For<IUserDiplomaProvider>().ImplementedBy<UserDiplomaProvider>().LifestyleTransient()
                , Component.For<IParticipantBusiness>().ImplementedBy<ParticipantBusiness>().LifestyleTransient()
                , Component.For<IQuestionsProvider>().ImplementedBy<QuestionsProvider>().LifestyleTransient()
                , Component.For<ITagsProvider>().ImplementedBy<TagsProvider>().LifestyleTransient()
                , Component.For<IExamsProvider>().ImplementedBy<ExamsProvider>().LifestyleTransient()
                , Component.For<IExamFactory>().ImplementedBy<ExamFactory>().LifestyleTransient()
                , Component.For<IExamPlayerFactory>().ImplementedBy<ExamPlayerFactory>().LifestyleTransient()
                , Component.For<IExamGrader>().ImplementedBy<ExamGrader>().LifestyleTransient()
                , Component.For<IPasswordResetRequestFactory>().ImplementedBy<PasswordResetRequestFactory>().LifestyleTransient()
                , Component.For<ICommentsProvider>().ImplementedBy<CommentsProvider>().LifestyleTransient()
                , Component.For<IFileStorage>().ImplementedBy<FileStorage>().LifestyleTransient()
                , Component.For<IChangeLogProvider>().ImplementedBy<ChangeLogProvider>().LifestyleTransient()
                , Component.For<IVersionLogProvider>().ImplementedBy<VersionLogProvider>().LifestyleTransient()
                , Component.For<ICustomInformationRepository>().ImplementedBy<CustomInformationRepository>().LifestyleTransient()
            );
        }

        private void RegisterExternalCustomersServices(IWindsorContainer container)
        {
            container.Register(
                  Component.For<IExternalCustomerProvider>().ImplementedBy<DefaultCustomersProvider>().LifestyleTransient()
                , Component.For<IExternalCustomersProviderFactory>().ImplementedBy<ExternalCustomersProviderFactory>().LifestyleTransient()
                , Component.For<IOrdersProviderResolver>().ImplementedBy<OrdersProviderResolver>().LifestyleTransient()

                // Agresso services
                , Component.For<CustomerSoap>().UsingFactoryMethod(() => new CustomerSoapClient("CustomerSoap")).LifestyleTransient()
                , Component.For<QueryEngineV201101Soap>().UsingFactoryMethod(() => new QueryEngineV201101SoapClient("QueryEngineV201101Soap")).LifestyleTransient()
                , Component.For<ImportV200606Soap>().UsingFactoryMethod(() => new ImportV200606SoapClient("ImportV200606Soap")).LifestyleTransient()

                // Agresso Common
                , Component.For<IExternalCustomerFactory>().ImplementedBy<ExternalCustomerFactory>().LifestyleTransient()
                , Component.For<IAgressoConfigurationProvider>().ImplementedBy<AgressoConfigurationProvider>().LifestyleTransient()

                // Metier Academy Norway
                , Component.For<IAgressoNorwayConfiguration>().ImplementedBy<AgressoNorwayConfiguration>().LifestyleTransient()
                , Component.For<IAgressoNorwayCustomerProvider>().ImplementedBy<AgressoNorwayCustomerProvider>().LifestyleTransient().IsFallback()

                // Metier Academy Sweden
                , Component.For<IAgressoSwedenConfiguration>().ImplementedBy<AgressoSwedenConfiguration>().LifestyleTransient()
                , Component.For<IAgressoSwedenCustomerProvider>().ImplementedBy<AgressoSwedenCustomerProvider>().LifestyleTransient().IsFallback()
                
            );
        }

        private void RegisterInvoicesServices(IWindsorContainer container)
        {
            container.Register(
                Component.For<IOrdersService>().ImplementedBy<OrdersService>().LifestyleTransient()
                , Component.For<IOrdersProvider>().ImplementedBy<OrdersProvider>().LifestyleTransient() 
                
                
                , Component.For<IDefaultOrderService>().ImplementedBy<DefaultOrderService>().LifestyleTransient().IsFallback()
                , Component.For<IAgressoOrderService>().ImplementedBy<AgressoOrderService>().LifestyleTransient().IsFallback()
                , Component.For<IAgressoOrderSerializer>().ImplementedBy<AgressoOrderSerializer>().LifestyleTransient()
                , Component.For<IAgressoOrderFactory>().ImplementedBy<AgressoOrderFactory>().LifestyleTransient()
                , Component.For<IAgressoOrderDetailFactory>().ImplementedBy<AgressoOrderDetailFactory>().LifestyleTransient()
                , Component.For<IAgressoQueryXmlParser>().ImplementedBy<AgressoQueryXmlParser>().LifestyleTransient()
                );
        }
    }
}