﻿using Metier.Phoenix.Core.Configuration;

namespace Metier.Phoenix.Api.Configuration
{
    public interface IConfiguration : ICoreConfiguration
    {
        int IntervalBetweenBackgroundTasks { get; }
        int PasswordResetTokenExpirationInMinutes { get; }
        string UploadsFolderUrl { get; }
        
        string GetAttachmentFileUrl(int fileId);
    }
}