﻿using System;
using System.Configuration;
using System.Web;
using Metier.Phoenix.Api.Configuration.ConfigurationSections;

namespace Metier.Phoenix.Api.Configuration
{
    public class PhoenixConfiguration : IConfiguration
    {
        private readonly PhoenixConfigurationSection _phoenixConfigurationSection;

        public PhoenixConfiguration()
        {
            _phoenixConfigurationSection = (PhoenixConfigurationSection)ConfigurationManager.GetSection("phoenixConfiguration");
        }

        public string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["MetierLmsContext"].ConnectionString; }
        }

        public int IntervalBetweenBackgroundTasks
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["IntervalBetweenBackgroundTasks"]); }
        }

        public int PasswordResetTokenExpirationInMinutes
        {
            get { return _phoenixConfigurationSection.PasswordResetTokenExpirationInMinutes; }
        }

        public string UploadsFolderUrl
        {
            get
            {
                var uploadFolderName = ConfigurationManager.AppSettings["FileUploadDir"];
                return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) +
                       HttpContext.Current.Request.ApplicationPath + "/" + uploadFolderName;
            }
        }

        public string GetAttachmentFileUrl(int fileId)
        {
            return UploadsFolderUrl + "/" + fileId;
        }

        public string GetPasswordResetFormUrl(string source)
        {
            if (_phoenixConfigurationSection != null)
            {
                foreach (SourceSystemConfigurationElement sourceSystem in _phoenixConfigurationSection.SourceSystems)
                {
                    if (sourceSystem.Name.ToLower() == source.ToLower())
                    {
                        return sourceSystem.PasswordResetFormUrl ?? string.Empty;
                    }
                }
            }
            return string.Empty;
        }

        public string PhoenixApiUrl
        {
            get { return ConfigurationManager.AppSettings["PhoenixApiUrl"]; }
        }
    }
}