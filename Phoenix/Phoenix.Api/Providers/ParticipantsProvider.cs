﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Transactions;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Facade.Participant;
using Metier.Phoenix.Api.Factories;
using Metier.Phoenix.Api.Messaging;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Business;
using Metier.Phoenix.Core.Business.ChangeLog;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Core.Utilities.Collections;
using ValidationException = Metier.Phoenix.Core.Exceptions.ValidationException;

namespace Metier.Phoenix.Api.Providers
{
    public class ParticipantsProvider : IParticipantsProvider
    {
        private readonly IActivitySetRepository _activitySetRepository;
        private readonly IParticipantRepository _participantRepository;
        private readonly IOrderFactory _orderFactory;
        private readonly IRcoProvider _rcoProvider;
        private readonly IAuthorizationProvider _authorizationProvider;
        private readonly IEventAggregator _eventAggregator;
        private readonly IParticipantBusiness _participantBusiness;
        private readonly IChangeLogProvider _changeLogProvider;
        private readonly MetierLmsContext _context;

        public ParticipantsProvider(IActivitySetRepository activitySetRepository, 
                                    IParticipantRepository participantRepository,
                                    IOrderFactory orderFactory,
                                    IRcoProvider rcoProvider,
                                    IAuthorizationProvider authorizationProvider,
                                    IEventAggregator eventAggregator,
                                    IParticipantBusiness participantBusiness,
                                    IChangeLogProvider changeLogProvider,
                                    MetierLmsContext context)
        {
            _activitySetRepository = activitySetRepository;
            _participantRepository = participantRepository;
            _orderFactory = orderFactory;
            _rcoProvider = rcoProvider;
            _authorizationProvider = authorizationProvider;
            _eventAggregator = eventAggregator;
            _participantBusiness = participantBusiness;
            _changeLogProvider = changeLogProvider;
            _context = context;
        }

        public Participant Move(int id, ParticipantMoveRequest moveRequest)
        {
            var participant = _participantRepository.FindById(id);
            if (participant == null)
            {
                throw new ValidationException("Participant does not exist");
            }

            var activity = _context.Activities.FindById(moveRequest.ActivityId).IncludeParticipants(_context);
            if (activity == null)
            {
                throw new ValidationException("Activity does not exist");
            }
            if (activity.Participants.Any(p => p.UserId == participant.UserId))
            {
                throw new ValidationException("User is already registered on this activity");
            }
            if (activity.MaxParticipants.HasValue && activity.MaxParticipants <= activity.Participants.Count)
            {
                throw new ValidationException("Activity is full");
            }
            
            var activitySet = _activitySetRepository.FindById(moveRequest.ActivitySetId);
            if (activitySet == null)
            {
                throw new ValidationException("Activity set does not exist");
            }
            
            participant.ActivityId = moveRequest.ActivityId;
            participant.ActivitySetId = moveRequest.ActivitySetId;
            return _participantRepository.Update(id, participant);
        }

        public Participant GetParticipant(int participantId)
        {
            return _context.Participants.Where(p =>  p.Id == participantId).ToList().FirstOrDefault();
        }

        public IEnumerable<Participant> GetParticipants(int[] participantIds, bool includeDeletedParticipants = false)
        {
            return _context.Participants.Where(p => participantIds.Contains(p.Id) && (includeDeletedParticipants || (p.IsDeleted == false))).ToList();
        }

        public IEnumerable<Participant> GetParticipants(int activityId, bool includeDeletedParticipants = false)
        {
            return _context.Participants.Where(p => p.ActivityId == activityId && (includeDeletedParticipants || (p.IsDeleted == false))).ToList();
        }

        public IEnumerable<Participant> GetParticipantsOnActivitySet(int activitySetId, bool includeDeletedParticipants = false)
        {
            return _context.Participants.Where(p => p.ActivitySetId == activitySetId && (includeDeletedParticipants || (p.IsDeleted == false))).ToList();
        }

        public IEnumerable<Participant> GetParticipations(int userId)
        {
            return _context.Participants.Where(p => p.UserId == userId && p.IsDeleted == false).ToList();
        }

        public Enrollment GetEnrollment(int enrollmentId)
        {
            return _context.Enrollments.FirstOrDefault(e => e.Id == enrollmentId);
        }

        public List<Enrollment> GetEnrollments(int userId)
        {
            return _context.Enrollments.Where(e => e.UserId == userId).ToList();
        }

        public List<Enrollment> GetEnrollmentsOnActivity(int activityId)
        {
            var enrollmentIds = _context.Participants.Where(p => p.ActivityId == activityId && p.IsDeleted == false).Select(e=> e.EnrollmentId);
            return _context.Enrollments.Where(e => enrollmentIds.Contains(e.Id)).ToList();

        }

        public List<Enrollment> GetEnrollmentsOnActivitySet(int activitySetId)
        {
            return _context.Enrollments.Where(e => e.ActivitySetId == activitySetId).ToList();
        }

        public IEnumerable<SearchableParticipant> GetParticipationsForEnrollment(int enrollmentId)
        {
            var enrollment = _context.Enrollments.FirstOrDefault(e => e.Id == enrollmentId);

            if(enrollment == null)
                throw new BadRequestException(string.Format("Coult not find any enrollments for id: {0}", enrollmentId));

            return _context.SearchableParticipants.Where(p => p.UserId == enrollment.UserId && p.ActivitySetId == enrollment.ActivitySetId).ToList();
        }
        

        public IEnumerable<Participant> GetParticipants(int userId, int rcoId)
        {
            var parent = _rcoProvider.GetTopParent(rcoId);
            if (parent != null)
            {
                if (parent.Context == "course")
                {
                    var activityIds = _context.Activities.Where(a => a.RcoCourseId == parent.Id).Select(a => a.Id);
                    return _context.Participants.Where(p => activityIds.Contains(p.ActivityId) && p.UserId == userId).ToList();
                }
                if (parent.Context == "examcontainer")
                {
                    var activityIds = _context.Activities.Where(a => a.RcoExamContainerId == parent.Id).Select(a => a.Id);
                    return _context.Participants.Where(p => activityIds.Contains(p.ActivityId) && p.UserId == userId).ToList();
                }
            }
            return null;
        }
        
        public RcoCourse GetRcoCourse(int participantId)
        {
            var participant = _context.Participants.FirstOrDefault(p => p.Id == participantId && !p.IsDeleted);
            if (participant != null)
            {
                var activity = _context.Activities.FirstOrDefault(a => a.Id == participant.ActivityId && !a.IsDeleted);
                if (activity != null && activity.RcoCourseId.HasValue)
                {
                    return _rcoProvider.GetCourse(activity.RcoCourseId.Value);
                }
            }
            return null;
        }

        public RcoExamContainer GetRcoExamContainer(int participantId)
        {
            var participant = _context.Participants.FirstOrDefault(p => p.Id == participantId && !p.IsDeleted);
            if (participant != null)
            {
                var activity = _context.Activities.FirstOrDefault(a => a.Id == participant.ActivityId && !a.IsDeleted);
                if (activity != null && activity.RcoExamContainerId.HasValue)
                {
                    return _rcoProvider.GetExamContainer(activity.RcoExamContainerId.Value);
                }
            }
            return null;
        }
        
        public void UpdateStatus(int userId, int rcoId)
        {
            var performance = _context.ScormPerformances.FirstOrDefault(p => p.UserId == userId && p.RcoId == rcoId);
            if (performance != null)
            {
                var participants = GetParticipants(performance.UserId, performance.RcoId);
                if (participants != null)
                {
                    foreach (var participant in participants)
                    {
                        var oldStatus = participant.StatusCodeValue;

                        switch (performance.Status)
                        {
                            case ScormLessonStatus.Complete:
                                participant.StatusCodeValue = ParticipantStatusCodes.Completed;
                                break;
                            case ScormLessonStatus.Incomplete:
                                participant.StatusCodeValue = ParticipantStatusCodes.InProgress;
                                break;
                            case ScormLessonStatus.NotAttempted:
                                participant.StatusCodeValue = ParticipantStatusCodes.Enrolled;
                                break;
                        }

                        _eventAggregator.Publish(new ParticipantStatusChangedEvent(participant.Id, oldStatus, participant.StatusCodeValue));
                    }
                    _context.SaveChanges();
                }
            }
        }

        public void SubmitExam(int id, Attachment attachment)
        {
            using (var transaction = new TransactionScope())
            {
                if (attachment == null)
                {
                    throw new ArgumentNullException("attachment");
                }

                var participant = _context.Participants.FirstOrDefault(p => p.Id == id && !p.IsDeleted);
                if (participant == null)
                {
                    throw new EntityNotFoundException("Participant with id {0} could not be found");
                }

                var validArticleTypes = new[] { (int)ArticleTypes.CaseExam, (int)ArticleTypes.ProjectAssignment, (int)ArticleTypes.ExternalCertification };

                var articleActivities = _context.Participants.Where(p => p.Id == participant.Id)
                    .Join(_context.Activities, par => par.ActivityId, act => act.Id, (par, act) => act)
                    .Join(_context.CustomerArticles, act => act.CustomerArticleId, cusart => cusart.Id, (act, cusart) => new { Activity = act, CustomerArticle = cusart })
                    .Join(_context.Articles, x => x.CustomerArticle.ArticleCopyId, art => art.Id, (x, art) => new { Article = art, x.Activity })
                    .First();

                _context.Entry(articleActivities.Activity).Collection(act => act.ActivityPeriods).Load();

                if (!validArticleTypes.Contains(articleActivities.Article.ArticleTypeId))
                {
                    throw new ValidationException("Exam can only be submitted for case exams, project assignments and external certifications");
                }

                if (!_authorizationProvider.IsAdmin() && _participantBusiness.IsPastDueDate(participant, articleActivities.Activity, articleActivities.Article))
                {
                    throw new ValidationException("Exam can not be submitted after due date");
                }

                if (!_authorizationProvider.IsAdminOr(participant.UserId))
                {
                    throw new ValidationException("Only admins can submit exams on behalf of other users");
                }
                var oldStatus = participant.StatusCodeValue;

                participant.StatusCodeValue = ParticipantStatusCodes.Completed;
                participant.Attachments.Add(attachment);
                _context.SaveChanges();

                _eventAggregator.Publish(new ExamSubmittedEvent { ParticipantId = participant.Id, AttachmentId = attachment.Id });
                if (oldStatus != participant.StatusCodeValue)
                {
                    _eventAggregator.Publish(new ParticipantStatusChangedEvent(participant.Id, oldStatus, participant.StatusCodeValue));    
                }

                transaction.Complete();
            }
        }

        public Participant ChangeStatus(int id, string status)
        {
            if (!ParticipantStatusCodes.IsValidStatus(status))
            {
                throw new ValidationException("Invalid status");
            }
            var participant = _context.Participants.FirstOrDefault(p => p.Id == id && !p.IsDeleted);
            if (participant == null)
            {
                throw new EntityNotFoundException("Participant with id {0} could not be found");
            }

            if (participant.StatusCodeValue != status && status == ParticipantStatusCodes.Completed)
            {
                participant.CompletedDate = DateTime.UtcNow;
            }

            participant.StatusCodeValue = status;
            _context.SaveChanges();
            return participant;
        }
		public Participant UpdateParticipant(int id, ParticipantUpdateRequest updateRequest)
		{
			if (updateRequest.SetStatus && !ParticipantStatusCodes.IsValidStatus(updateRequest.Status))
			{
				throw new ValidationException("Invalid status");
			}
			var participant = _context.Participants.FirstOrDefault(p => p.Id == id && !p.IsDeleted);
			if (participant == null)
			{
				throw new EntityNotFoundException("Participant with id {0} could not be found");
			}

			if (updateRequest.SetStatus)
				participant.StatusCodeValue = updateRequest.Status;

			if (updateRequest.SetCompletedDate)
				participant.CompletedDate = updateRequest.CompletedDate;

			_context.SaveChanges();
			return participant;
		}

		public ParticipantGradeResponse SetExamGrade(int id, string grade)
        {
            if (!CanGradeParticipant(id))
            {
                throw new ValidationException("User is not authorized to grade participant " + id);
            }   
            if (!string.IsNullOrEmpty(grade))
            {
                var validGrades = new[] { "A", "B", "C", "D", "E", "F" };
                if (!validGrades.Contains(grade))
                {
                    throw new ValidationException("Invalid grade");
                }
            }
            
            var participant = _context.Participants.FirstOrDefault(p => p.Id == id && !p.IsDeleted);
            if (participant == null)
            {
                throw new EntityNotFoundException("Participant with id {0} could not be found");
            }

            if (!IsExamType(participant))
            {
                throw new ValidationException("Exam grade can only be set for case exams, project assignments, external certifications, multiple choice and internal certifications");
            }
            string gradeSetMessage;
            if (string.IsNullOrEmpty(grade))
            {
                participant.ExamGrade = null;
                participant.Result = null;
                participant.ExamGradeDate = null;
                participant.ExamGraderUserId = null;
                gradeSetMessage = "Grade reset";
            }
            else
            {
                participant.ExamGrade = grade;
                participant.Result = grade == "F" ? ParticipantResultCodes.Failed : ParticipantResultCodes.Passed;
                participant.ExamGradeDate = DateTime.UtcNow;
                participant.ExamGraderUserId = _authorizationProvider.GetCurrentUser().Id;
                gradeSetMessage = string.Format("Grade set to \"{0}\". Result \"{1}\"", participant.ExamGrade, ParticipantResultCodes.GetHumanFriendly(participant.Result));
            }
            
            _context.SaveChanges();
            _changeLogProvider.Write(participant.Id, ChangeLogEntityTypes.Participant, ChangeLogTypes.GradeSet, gradeSetMessage);
            return new ParticipantGradeResponse
            {
                ParticipantRowVersion = participant.RowVersion,
                ExamGrade = participant.ExamGrade,
                ExamGradeDate = participant.ExamGradeDate,
                StatusCodeValue = participant.StatusCodeValue,
                Result = participant.Result
            };
        }

        public bool CanGradeParticipant(int participantId)
        {
            if (_authorizationProvider.IsAdmin())
            {
                return true;
            }

            var currentUser = _authorizationProvider.GetCurrentUser();
            var participant = _context.Participants.Include(p => p.ExaminerResource).FirstOrDefault(p => p.Id == participantId);
            if (participant != null && participant.ExaminerResource != null)
            {
                return currentUser.Id == participant.ExaminerResource.UserId;
            }
            return false;
        }

        private bool IsExamType(Participant participant)
        {
            var validArticleTypes = new[] { (int)ArticleTypes.CaseExam, (int)ArticleTypes.ProjectAssignment, (int)ArticleTypes.ExternalCertification, (int)ArticleTypes.MultipleChoice, (int)ArticleTypes.InternalCertification };

            var article = _context.Participants.Where(p => p.Id == participant.Id)
                .Join(_context.Activities, par => par.ActivityId, act => act.Id, (par, act) => act)
                .Join(_context.CustomerArticles, act => act.CustomerArticleId, cusart => cusart.Id, (act, cusart) => new { Activity = act, CustomerArticle = cusart })
                .Join(_context.Articles, x => x.CustomerArticle.ArticleCopyId, art => art.Id, (x, art) => art)
                .First();

            return validArticleTypes.Contains(article.ArticleTypeId);
        }

        public PartialList<ExamSubmission> GetExamSubmissionsForCurrentUser(bool includeGraded, int skip, int limit)
        {
            var currentUser = _authorizationProvider.GetCurrentUser();
            var participantsDataQuery = _context.Participants
                                // Select participants for  ProjectAssignments and CaseExams
                                .Join(_context.Activities, p => p.ActivityId, a => a.Id, (p, a) => new { Participant = p, Activity = a})
                                .Join(_context.CustomerArticles, x => x.Activity.CustomerArticleId, ca => ca.Id, (x, ca) => new { CustomerArticle = ca, x.Participant, x.Activity })
                                .Join(_context.Articles, x => x.CustomerArticle.ArticleCopyId, a => a.Id, (x, a) => new { Article = a, x.Participant, x.Activity })
                                .Where(x => x.Article.ArticleTypeId == (int)ArticleTypes.ProjectAssignment || x.Article.ArticleTypeId == (int)ArticleTypes.CaseExam)
                                
                                // Select those participants that have the current user as the ExaminerResoruceId
                                .Where(p => p.Participant.ExaminerResourceId.HasValue)
                                .Join(_context.Resources.OfType<UserResource>(), x => x.Participant.ExaminerResourceId.Value, r => r.Id, (x, r) => new { x.Participant, x.Activity, x.Article, Resource = r })
                                .Join(_context.Users, x => x.Resource.UserId, u => u.Id, (x, u) => new { x.Participant, x.Activity, x.Article, User = u })
                                .Where(x => x.User.Id == currentUser.Id);

            if (!includeGraded)
            {
                // Select only those particiapnts who are not graded
                participantsDataQuery = participantsDataQuery.Where(x => string.IsNullOrEmpty(x.Participant.ExamGrade));
            }
            // Execute query
            var participantsData = participantsDataQuery.ToList();
            // Lazy load additional data
            foreach (var data in participantsData)
            {
                _context.Entry(data.Participant).Collection(a => a.Attachments).Load();
                _context.Entry(data.Participant).Reference(p => p.User).Load();
                _context.Entry(data.Activity).Collection(a => a.ActivityPeriods).Load();
            }
            participantsData = participantsData.Where(x => x.Participant.Attachments.Any())
                                               .Where(x => _participantBusiness.IsPastDueDate(x.Participant, x.Activity, x.Article))
                                               .OrderByDescending(x => x.Participant.Attachments.Max(a => a.Created))
                                               .ToList();
            
            var totalCount = participantsData.Count();
            var participantsWindow = participantsData.Skip(skip).Take(limit).ToList();
            
            return participantsWindow.Select(x => new ExamSubmission
            {
                ActivityTitle = x.Activity.Name,
                ParticipantId = x.Participant.Id,
                ExamGrade = x.Participant.ExamGrade,
                Attachments = x.Participant.Attachments,
                FirstName = x.Participant.User.FirstName,
                LastName = x.Participant.User.LastName,
                ExamGradeDate = x.Participant.ExamGradeDate,
            }).ToPartialList(totalCount);
        }
    }
}