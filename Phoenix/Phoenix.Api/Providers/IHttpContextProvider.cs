﻿using System.Web;

namespace Metier.Phoenix.Api.Providers
{
    public interface IHttpContextProvider
    {
        HttpContextBase Context { get; } 
    }
}