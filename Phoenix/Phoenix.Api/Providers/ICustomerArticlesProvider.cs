﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;

namespace Metier.Phoenix.Api.Providers
{
    public interface ICustomerArticlesProvider
    {
        CustomerArticle Get(int id);
        CustomerArticle Create(CustomerArticle customerArticle);
        CustomerArticle Update(CustomerArticle customerArticle);
        IEnumerable<CustomerArticle> FindByExamId(int examId);
        IEnumerable<CustomerArticle> FindByModuleId(int moduleId);
        IEnumerable<SearchableCustomerArticle> FindBasedOnArticle(int articleId, int? distributorId = null);
        IEnumerable<CustomerArticleEnrollmentCondition> GetEnrollmentConditions(int id);
        IEnumerable<CustomerArticleEnrollmentCondition> SaveEnrollmentConditions(IEnumerable<CustomerArticleEnrollmentCondition> enrollmentConditions, int customerArticleId);
    }
}