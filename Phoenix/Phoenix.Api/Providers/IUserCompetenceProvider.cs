﻿using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Providers
{
    public interface IUserCompetenceProvider
    {
        UserCompetence GetByUserId(int userId);
        UserCompetence Update(int userId, UserCompetence userCompetence);
        UserCompetence Create(int userId, UserCompetence userCompetence);
    }
}