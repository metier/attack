using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Transactions;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Facade.Enrollment;
using Metier.Phoenix.Api.Facade.Participant;
using Metier.Phoenix.Api.Factories;
using Metier.Phoenix.Api.Messaging;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Interfaces;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Extensions;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Exceptions;

namespace Metier.Phoenix.Api.Providers
{
    public class ActivitySetProvider : IActivitySetProvider
    {
        private static readonly object SyncRoot = new object();

        private readonly IActivitySetRepository _activitySetRepository;
        private readonly IActivityRepository _activityRepository;
        private readonly IParticipantRepository _participantRepository;
        private readonly MetierLmsContext _context;
        private readonly IParticipantFactory _participantFactory;
        private readonly IAuthorizationProvider _authorizationProvider;
        private readonly IEventAggregator _eventAggregator;

        public ActivitySetProvider(IActivitySetRepository activitySetRepository,
                                    IActivityRepository activityRepository,
                                    IParticipantRepository participantRepository,
                                    MetierLmsContext context,
                                    IParticipantFactory participantFactory,
                                    IAuthorizationProvider authorizationProvider,
                                    IEventAggregator eventAggregator)
        {
            _activitySetRepository = activitySetRepository;
            _activityRepository = activityRepository;
            _participantRepository = participantRepository;
            _context = context;
            _participantFactory = participantFactory;
            _authorizationProvider = authorizationProvider;
            _eventAggregator = eventAggregator;
        }

        public ActivitySet AddActivity(int id, int activityId)
        {
            lock (SyncRoot)
            {
                using (var transaction = new TransactionScope())
                {
                    var dbActivitySet = _activitySetRepository.FindById(id);
                    var dbActivity = _context.Activities.FindById(activityId).IncludeParticipants(_context);

                    var enrollments = dbActivitySet.Enrollments.Where(e => !e.IsCancelled).ToList();
                    if (!HasRoomForParticipants(dbActivity, enrollments.Count))
                    {
                        throw new ValidationException("Activity set has more enrollments than max number of participants on this activity.");
                    }
                    dbActivity = AddParticipantsToActivity(dbActivity, enrollments, dbActivitySet.Id);
                    dbActivitySet.Activities.Add(dbActivity);
                    _context.SaveChanges();

                    transaction.Complete();
                    return dbActivitySet;
                } 
            }
        }

        private Activity AddParticipantsToActivity(Activity activity, IEnumerable<Enrollment> enrollments, int activitySetId)
        {
            var createdParticipants = new List<Participant>();
            foreach (var enrollment in enrollments)
            {
                var user = _context.Users.Include("UserInfoElements.LeadingText").FirstOrDefault(u => u.Id == enrollment.UserId);
                var userParticipant = _participantFactory.Create(user, enrollment, activity, activitySetId, null);
                if (!activity.Participants.Any(p => p.UserId == enrollment.UserId))
                {
                    createdParticipants.Add(userParticipant);
                    activity.Participants.Add(userParticipant);
                }
            }
            var updatedActivity = _activityRepository.Update(activity.Id, activity);
            foreach (var participant in createdParticipants)
            {
                _eventAggregator.Publish(new ParticipantCreatedEvent {ParticipantId = participant.Id});
            }
            return updatedActivity;
        }
        
        public ActivitySet RemoveActivity(int id, int activityId)
        {
            lock (SyncRoot)
            {
                var dbActivitySet = _activitySetRepository.FindById(id);
                var dbActivity = _context.Activities.FindById(activityId).IncludeParticipants(_context);

                if (_activityRepository.ActivityLockedInOrder(dbActivity))
                {
                    throw new ValidationException("Activity cannot be removed because it has been added to an order");
                }

                using (var transaction = new TransactionScope())
                {
                    dbActivity = RemoveParticipantsFromActivity(dbActivity, dbActivitySet.Enrollments.Where(e => !e.IsCancelled));
                    dbActivitySet.Activities.Remove(dbActivity);

                    _context.SaveChanges();
                    transaction.Complete();
                    return dbActivitySet;
                }
            }
        }

        private Activity RemoveParticipantsFromActivity(Activity activity, IEnumerable<Enrollment> cancelledEnrollments)
        {
            var userIds = cancelledEnrollments.Select(u => u.UserId).ToList();
            var participantsToRemove = activity.Participants.Where(p => userIds.Contains(p.UserId)).ToList();
            foreach (var participant in participantsToRemove)
            {
                if (GetActivitySetsWithEnrolledUserAndActivity(participant.UserId, activity.Id).Count() <= 1)
                {
                    _participantRepository.Delete(participant);
                }
            }
            return activity;
        }

        public Tuple<ActivitySet, List<int>> Enroll(int id, int userId, EnrollmentDetails enrollmentDetails, string paymentReference = null, int? customerId = null)
        {
            if (!_authorizationProvider.IsAdminOr(userId) && 
                !_authorizationProvider.HasRole(PhoenixRoles.LearningPortalTokenUser) &&
                !_authorizationProvider.HasRole(PhoenixRoles.EzTokenUser))
            {
                throw new UnauthorizedException("Cannot enroll other users");
            }
            
            lock (SyncRoot)
            {
                var dbActivitySet = _activitySetRepository.FindById(id);

                if (dbActivitySet == null)
                {
                    throw new ValidationException(string.Format("Activity set with id {0} does not exist", id));
                }

                dbActivitySet.ReplaceEntitiesFromContext(a => a.Activities, _context.Activities.Include(a => a.Participants));

                var participantIds = new List<int>();
                using (var transaction = new TransactionScope())
                {
                    var enrollment = CreateEnrollment(dbActivitySet, userId);
                    _context.SaveChanges();
                    //if (!dbActivitySet.Activities.All(HasAvailableSeats))
                    //{
                    //    throw new ValidationException("One or more of the activities in the activity set is already full");
                    //}

                    participantIds = AddParticipants(dbActivitySet, enrollment, enrollmentDetails, paymentReference, customerId);

                    _context.SaveChanges();
                    transaction.Complete();
                }

                return new Tuple<ActivitySet, List<int>>(dbActivitySet, participantIds);
            }
        }

        public IEnumerable<ActivitySet> Enroll(int[] activitySetIds, int userId, EnrollmentDetails enrollmentDetails, string paymentReference = null, int? customerId = null)
        {
            var participantIds = new List<int>();
            var enrolledActivitySets = new List<ActivitySet>();
            using (var transaction = new TransactionScope())
            {
                foreach (var activitySetId in activitySetIds)
                {
                    var enrollData = Enroll(activitySetId, userId, enrollmentDetails, paymentReference, customerId);
                    enrolledActivitySets.Add(enrollData.Item1);
                    participantIds.AddRange(enrollData.Item2);
                }
                transaction.Complete();
            }
            _eventAggregator.Publish(new UserEnrolledEvent { ParticipantIds = participantIds });
            return enrolledActivitySets;
        }

        //private bool HasAvailableSeats(Activity activity)
        //{
        //    if (!activity.MaxParticipants.HasValue)
        //    {
        //        return true;
        //    }
        //    return activity.Participants.Count(p => !p.IsDeleted) < activity.MaxParticipants;
        //}

        private bool HasRoomForParticipants(Activity activity, int numberOfParticipants)
        {
            if (!activity.MaxParticipants.HasValue)
            {
                return true;
            }
            return activity.Participants.Count(p => !p.IsDeleted) + numberOfParticipants <= activity.MaxParticipants;
        }

        private static Enrollment CreateEnrollment(ActivitySet dbActivitySet, int userId)
        {
            var existing = dbActivitySet.Enrollments.FirstOrDefault(e => e.UserId == userId);
            if (existing != null)
            {
                existing.IsCancelled = false;
                return existing;
            }

            var enrollment = new Enrollment {UserId = userId};
            dbActivitySet.Enrollments.Add(enrollment);
            
            return enrollment;
        }

        private List<int> AddParticipants(ActivitySet dbActivitySet, Enrollment enrollment, EnrollmentDetails enrollmentDetails, string paymentReference, int? customerId = null)
        {
            var participantIds = new List<int>();
            foreach (var activity in dbActivitySet.Activities)
            {
                var user = _context.Users.Include("UserInfoElements.LeadingText").FirstOrDefault(u => u.Id == enrollment.UserId);

                if (user == null)
                    throw new ValidationException("Invalid userId");

                var customer = _context.Customers.WithAll().FirstOrDefault(c => c.Id == (customerId ?? user.CustomerId));
                var overridePrice = enrollmentDetails?.ParticipantPayments?.FirstOrDefault(p => p.ActivityId == activity.Id)?.Price;
                var overrideCurrencyCodeId = enrollmentDetails?.ParticipantPayments?.FirstOrDefault(p => p.ActivityId == activity.Id)?.CurrencyCodeId;

                var userParticipant = _participantFactory.Create(user, enrollment, activity, dbActivitySet.Id, enrollmentDetails?.InfoElements, paymentReference, customer, overridePrice, overrideCurrencyCodeId);
                _activityRepository.AddParticipant(activity, userParticipant);

                participantIds.Add(userParticipant.Id);
            }
            return participantIds;
        }

        public ActivitySet Unenroll(int id, int userId)
        {
            if (!_authorizationProvider.IsAdminOr(userId) && !_authorizationProvider.HasRole(PhoenixRoles.LearningPortalTokenUser))
            {
                throw new UnauthorizedException("Cannot unenroll other users");
            }
            
            lock (SyncRoot)
            {
                var dbActivitySet = _activitySetRepository.FindById(id);
                if (!_authorizationProvider.IsAdmin())
                {
                    if (!dbActivitySet.IsUnenrollmentAllowed)
                    {
                        throw new UnauthorizedException("Unenrollment is not allowed");
                    }
                }

                using (var transaction = new TransactionScope())
                {
                    CancelEnrollment(userId, dbActivitySet);
                    CancelParticipants(dbActivitySet, userId);

                    _context.SaveChanges();
                    transaction.Complete();
                }
                return dbActivitySet;
            }
        }
        
        private void CancelEnrollment(int userId, ActivitySet dbActivitySet)
        {
            var enrollment = dbActivitySet.Enrollments.FirstOrDefault(e => e.UserId == userId);
            if (enrollment != null)
            {
                enrollment.IsCancelled = true;
            }
        }

        private void CancelParticipants(ActivitySet activitySet, int userId)
        {
            activitySet.ReplaceEntitiesFromContext(a => a.Activities, _context.Activities.Include(a => a.Participants));
            foreach (var activity in activitySet.Activities)
            {
                var participantsToRemove = activity.Participants.Where(p => p.UserId == userId).ToList();
                foreach (var participant in participantsToRemove)
                {
                    if (GetActivitySetsWithEnrolledUserAndActivity(participant.UserId, activity.Id).Count() <= 1)
                    {
                        _participantRepository.Delete(participant);
                    }
                }
            }
        }
        
        private IEnumerable<ActivitySet> GetActivitySetsWithEnrolledUserAndActivity(int userId, int activityId)
        {
            return _context.ActivitySets.WithActivitiesAndActivityPeriods()
                                        .Where(aset => aset.Enrollments.Any(e => !e.IsCancelled && e.UserId == userId) &&
                                                       aset.Activities.Any(a => a.Id == activityId)).ToList();
        }

        public ActivitySet Share(int id, int customerId)
        {
            var activitySet = _context.ActivitySets.Include(a => a.ActivitySetShares).FirstOrDefault(a => a.Id == id && !a.IsDeleted);
            if (activitySet == null)
            {
                throw new EntityNotFoundException(string.Format("Could not find activity set with ID {0}", id));
            }
            if (activitySet.ActivitySetShares.Any(c => c.CustomerId == customerId))
            {
                return activitySet;
            }
            
            var customer = _context.Customers.FirstOrDefault(c => c.Id == customerId);
            if (customer == null)
            {
                throw new EntityNotFoundException(string.Format("Could not find customer with ID {0}", customerId));
            }
            activitySet.ActivitySetShares.Add(new ActivitySetShare { ActivitySetId = id, CustomerId = customerId });
            _context.SaveChanges();
            return _activitySetRepository.FindById(id);
        }

        public ActivitySet Unshare(int id, int customerId)
        {
            var activitySet = _context.ActivitySets.Include(a => a.ActivitySetShares).FirstOrDefault(a => a.Id == id && !a.IsDeleted);
            if (activitySet == null)
            {
                throw new EntityNotFoundException(string.Format("Could not find activity set with ID {0}", id));
            }
            if (activitySet.ActivitySetShares.All(c => c.CustomerId != customerId))
            {
                return activitySet;
            }
            activitySet.ActivitySetShares.RemoveAll(c => c.CustomerId == customerId);
            _context.SaveChanges();
            return _activitySetRepository.FindById(id);
        }
    }
}