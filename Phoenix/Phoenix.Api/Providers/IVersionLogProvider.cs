﻿using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Providers
{
    public interface IVersionLogProvider
    {
        PartialList<VersionLogItem> GetVersionLogItems(int? skip, int? limit);
    }
}