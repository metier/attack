﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Metier.Phoenix.Api.Data;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Exceptions;
using System.Data.Entity;
using WebGrease.Css.Extensions;
using Metier.Phoenix.Core.Data.Extensions;

namespace Metier.Phoenix.Api.Providers
{
    public class CustomerArticlesProvider : ICustomerArticlesProvider
    {
        private readonly ICustomerArticleRepository _customerArticleRepository;
        private readonly MetierLmsContext _context;

        public CustomerArticlesProvider(ICustomerArticleRepository customerArticleRepository, MetierLmsContext context)
        {
            _customerArticleRepository = customerArticleRepository;
            _context = context;

            Mapper.CreateMap<CustomerArticle, SearchableCustomerArticle>();
        }

        public CustomerArticle Get(int id)
        {
            return _context.CustomerArticles.FindById(id, _context.Articles);
        }

        public CustomerArticle Create(CustomerArticle customerArticle)
        {
            return _customerArticleRepository.Create(customerArticle);
        }

        public CustomerArticle Update(CustomerArticle customerArticle)
        {
            return _customerArticleRepository.Update(customerArticle);
        }

        public IEnumerable<CustomerArticle> FindByExamId(int examId)
        {
            var customerArticles = _context.CustomerArticles.Where(c => c.ExamId == examId && !c.IsDeleted).ToList();
            foreach (var customerArticle in customerArticles)
            {
                customerArticle.WithArticle(_context.Articles);
            }
            return customerArticles;
        }

        public IEnumerable<CustomerArticle> FindByModuleId(int moduleId)
        {
            var customerArticles = _context.CustomerArticles.Where(c => c.ModuleId == moduleId && !c.IsDeleted).ToList();
            foreach (var customerArticle in customerArticles)
            {
                customerArticle.WithArticle(_context.Articles);
            }
            return customerArticles;
        }

        public IEnumerable<SearchableCustomerArticle> FindBasedOnArticle(int articleId, int? distributorId = null)
        {
            var infos = _context.SearchableCustomerArticles.Where(c => c.ArticleOriginalId == articleId);
            if (distributorId.HasValue)
            {
                infos = infos.Where(i => i.DistributorId == distributorId);
            }
            return infos;
        }

        public IEnumerable<CustomerArticleEnrollmentCondition> GetEnrollmentConditions(int id)
        {
            var enrollmentConditions =
                _context.CustomerArticleEnrollmentConditions.Where(e => e.CustomerArticleId == id)
                    .Include(e => e.Product)
                    .Include(e => e.ArticleType)
                    .Include(e => e.Customer)
                    .Include(e => e.Language)
                    .ToList();

            return enrollmentConditions;

        }

        public IEnumerable<CustomerArticleEnrollmentCondition> SaveEnrollmentConditions(IEnumerable<CustomerArticleEnrollmentCondition> enrollmentConditions, int customerArticleId)
        {
            if (enrollmentConditions == null)
                return null;

            //To avoid possible multiple iterations of IEnumerable
            var enrollmentConditionsList = enrollmentConditions as IList<CustomerArticleEnrollmentCondition> ?? enrollmentConditions.ToList();

            ValidateEnrollmentConditions(enrollmentConditionsList, customerArticleId);

            enrollmentConditionsList.ForEach(ec => ec.ConfigureDbContextForEntry(_context));

            //Remove deleted rows
            var dbEnrollmentConditions = _context.CustomerArticleEnrollmentConditions.Where(e => e.CustomerArticleId == customerArticleId).ToList();

            foreach (var item in dbEnrollmentConditions)
            {
                if (enrollmentConditionsList.All(e => e.Id != item.Id))
                    _context.CustomerArticleEnrollmentConditions.Remove(item);
            }

            _context.SaveChanges();

            return GetEnrollmentConditions(customerArticleId);
        }

        private void ValidateEnrollmentConditions(IList<CustomerArticleEnrollmentCondition> enrollmentConditions, int customerArticleId)
        {
            if (customerArticleId <= 0)
                throw new ValidationException("Invalid customer article id");

            if (enrollmentConditions.Any(ec => ec.CustomerArticleId != customerArticleId))
                throw new ValidationException("Invalid customer article id provided for given enrollment condition.");

            if (enrollmentConditions.Select(ec => new { ec.ProductId, ec.ArticleTypeId }).Distinct().Count() < enrollmentConditions.Count())
                throw new ValidationException("Only one enrollment condition may be registered for a given combination of product and articletype.");

        }


    }
}