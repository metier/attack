﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Facade.ExamPlayer;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Interfaces;

namespace Metier.Phoenix.Api.Providers
{
    public interface IActivityProvider
    {
        Activity Get(int id, bool includeParticipantsWithOrderInformation = true);
        OrderLine GetOrderLine(int id);
        IEnumerable<Activity> GetActivities(int[] activityIds, bool includeDeletedActivities = false);
        IEnumerable<ActivitySet> GetActivitySets(int id);
        ProductDescription GetProductDescription(int id);
        IEnumerable<ExamStatus> GetExamStatuses(int id);
        IEnumerable<EnrollmentCondition> GetEnrollmentConditions(int id);
        IEnumerable<EnrollmentCondition> SaveEnrollmentConditions(IEnumerable<EnrollmentCondition> enrollmentConditions, int activityId);
    }
}