﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Providers
{
    public interface IPerformanceProvider
    {
        PerformanceInfo GetPerformancesByArticle(int userId, int articleId);
        ScormPerformance RegisterPerformance(int userId, int rcoId, ScormPerformanceRegisterRequest request);
        PerformanceInfo GetPerformancesByActivity(int userId, int activityId);
        IEnumerable<PerformanceInfo> GetPerformancesByUserId(int userId);
        void CreateParentPerformanceIfNotExists(int userId, int relatedRcoId);
        ScormPerformance GetParentPerformance(int userId, int relatedRcoId);
        ScormPerformance Create(ScormPerformance performance);
        List<PerformanceSummary> GetPerformanceSummaryByActivity(int id);
        List<PerformanceSummary> GetPerformanceSummaryByCustomer(int id);
        ScormPerformance GetPerformance(int userId, int rcoId);
        IEnumerable<ScormPerformance> GetPerformancesForAllLessons(int userId, int lessonRcoId);
        IEnumerable<ScormPerformance> GetPerformancesForAllExams(int userId, int rcoId);
    }
}