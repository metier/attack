using System;
using Metier.Phoenix.Api.Facade.Mail;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Providers
{
    public interface IMailsProvider
    {
        PartialList<SearchableMail> Search(string query,
            int[] messageTypes,
            int[] mailStatuses,
            DateTime? fromDate,
            DateTime? toDate,
            string orderBy,
            string orderDirection,
            int skip,
            int limit);

        MailSentResponse Send(int mailId);
        MailPreview GetPreview(int mailId);
    }
}