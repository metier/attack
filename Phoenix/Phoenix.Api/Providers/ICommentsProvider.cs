﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Providers
{
    public interface ICommentsProvider
    {
        List<Comment> FindByEntityId(string entity, int entityId);
        List<Comment> GetUserComments(int userId, bool includeParticipantComments, bool includeEnrollmentComments);
        List<Comment> GetParticipationComments(int participantId, bool includeUserComments, bool includeEnrollmentComments);
        List<Comment> GetEnrollmentComments(int enrollmentId, bool includeUserComments, bool includeParticipantComments);

        List<Comment> GetParticipantComments(int activityId, bool includeUserComments = false, bool includeEnrollmentComments = false);
        List<Comment> Delete(int id);
        List<Comment> Create(Comment comment);
        List<Comment> Update(Comment comment);

        List<Comment> GetAllEnrollmentComments(int activitySetId, bool includeUserComments, bool includeParticipantComments);
    }
}