﻿using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Providers
{
    public interface ITagsProvider
    {
        PartialList<Tag> SearchTags(string query, int[] tagIds, string orderBy, string orderDirection, int? skip, int? limit);
        Tag Create(Tag tag);
    }
}