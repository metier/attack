﻿using System.Web;

namespace Metier.Phoenix.Api.Providers
{
    public class HttpContextProvider : IHttpContextProvider
    {
        public HttpContextBase Context
        {
            get
            {
                return new HttpContextWrapper(HttpContext.Current);
            }
        }
    }
}