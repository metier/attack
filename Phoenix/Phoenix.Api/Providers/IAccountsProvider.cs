﻿using System;
using Metier.Phoenix.Api.Facade.Account;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Providers
{
    public interface IAccountsProvider
    {
        User CreateUser(UserAccount account, bool autoGeneratePassword, string source);
        ArbitraryAccountResponse CreateArbitraryAccount(ArbitraryAccountRequest request, string source);
        bool Validate(string username, string password);
        bool ChangePassword(string username, string newPassword);
        bool ResetPassword(Guid token, string email, string newPassword);
        void RequestPasswordReset(string email, string source);
        UserAccount GetByUserId(int userId);
        UserAccount GetByUsername(string username);
        UserAccount GetByEmail(string email);
        UserAccount GetCurrentUser();
        UserAccount Update(UserAccount account);
        void MergeAccounts(int userId, int userIdToMerge);
        UserContext GetUserContext(string username);
    }
}