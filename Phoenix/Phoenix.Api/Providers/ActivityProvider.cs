﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Facade.ExamPlayer;
using Metier.Phoenix.Api.Factories.ExamPlayer;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Interfaces;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Core.Data.Extensions;
using WebGrease.Css.Extensions;

namespace Metier.Phoenix.Api.Providers
{
    public class ActivityProvider : IActivityProvider
    {
        private readonly MetierLmsContext _context;
        private readonly IExamPlayerFactory _examPlayerFactory;

        public ActivityProvider(MetierLmsContext context, IExamPlayerFactory examPlayerFactory)
        {
            _context = context;
            _examPlayerFactory = examPlayerFactory;
        }

        public Activity Get(int id, bool includeParticipantsWithOrderInformation = true)
        {
            if(includeParticipantsWithOrderInformation)
                return _context.Activities.WithAllExceptParticipants().FindById(id).IncludeParticipantsWithOrderInformation(_context);

            return _context.Activities.WithAllExceptParticipants().FindById(id);
        }

        public OrderLine GetOrderLine(int activityId)
        {
            var activity = _context.Activities.FindById(activityId);

            if (activity != null)
            {
                var customerArticle = _context.CustomerArticles.FindById(activity.CustomerArticleId, _context.Articles);
                return new OrderLine
                {
                    Id = activity.Id,
                    ParentActivityId = activity.Id,
                    CurrencyCodeId = activity.CurrencyCodeId.GetValueOrDefault(),
                    Article = customerArticle.ArticlePath,
                    Text = activity.Name,
                    Price = activity.FixedPrice.GetValueOrDefault(),
                    ValueAddedTax = 0,
                    Type = OrderLineType.FixedPriceActivity
                };    
            }
            return null;
        }

        public IEnumerable<Activity> GetActivities(int[] activityIds, bool includeDeletedActivities = false)
        {
            return _context.Activities.Where(p => activityIds.Contains(p.Id) && (includeDeletedActivities || (p.IsDeleted == false))).ToList();
        }

        public IEnumerable<ActivitySet> GetActivitySets(int id)
        {
            return _context.ActivitySets.WithActivities()
                                        .Where(a => !a.IsDeleted && a.Activities.Any(act => act.Id == id)) as IEnumerable<ActivitySet>;
        }

        public ProductDescription GetProductDescription(int id)
        {
            return _context.Activities.Where(a => a.Id == id && !a.IsDeleted)
                           .Join(_context.CustomerArticles, a => a.CustomerArticleId, c => c.Id, (a, c) => c)
                           .Join(_context.Articles, c => c.ArticleCopyId, a => a.Id, (c, a) => a)
                           .Join(_context.ProductDescriptions, a => a.ProductDescriptionId, p => p.Id, (a, p) => p)
                           .WithAll()
                           .FirstOrDefault();
        }

        public IEnumerable<ExamStatus> GetExamStatuses(int id)
        {
            var activity = _context.Activities.Include(a => a.Participants)
                                              .Include(a => a.ActivityPeriods)
                                              .Include(a => a.ActivityExamCourses)
                                              .FirstOrDefault(a => a.Id == id && !a.IsDeleted);
            if (activity == null)
            {
                throw new EntityNotFoundException(string.Format("Activity with ID {0} not found", id));
            }

            var customerArticle = _context.CustomerArticles.FirstOrDefault(a => a.Id == activity.CustomerArticleId && !a.IsDeleted);
            if (customerArticle == null)
            {
                throw new EntityNotFoundException(string.Format("Customer Article with ID {0} not found", activity.CustomerArticleId));
            }
            
            var examStatuses = new List<ExamStatus>();
            var article = _context.Articles.WithLanguage().FirstOrDefault(a => a.Id == customerArticle.ArticleCopyId && !a.IsDeleted) as ExamArticle;
            if (article != null)
            {
                foreach (var participant in activity.Participants.Where(p => !p.IsDeleted))
                {
                    var exam = _context.Exams.FirstOrDefault(e => e.ParticipantId == participant.Id);
                    examStatuses.Add(_examPlayerFactory.CreateExamStatus(exam, participant, activity, article));
                }    
            }
            return examStatuses;
        }

        public IEnumerable<EnrollmentCondition> GetEnrollmentConditions(int id)
        {
            var enrollmentConditions =
                _context.EnrollmentConditions.Where(e => e.ActivityId == id)
                    .Include(e => e.Product)
                    .Include(e => e.ArticleType)
                    .Include(e=> e.Customer)
                    .Include(e=>e.Language)
                    .ToList();

            return enrollmentConditions;
        }

        public IEnumerable<EnrollmentCondition> SaveEnrollmentConditions(IEnumerable<EnrollmentCondition> enrollmentConditions, int activityId)
        {
            if (enrollmentConditions == null)
                return null;

            //To avoid possible multiple iterations of IEnumerable
            var enrollmentConditionsList = enrollmentConditions as IList<EnrollmentCondition> ?? enrollmentConditions.ToList();

            ValidateEnrollmentConditions(enrollmentConditionsList, activityId);

            enrollmentConditionsList.ForEach(ec=> ec.ConfigureDbContextForEntry(_context));

            //Remove deleted rows
            var dbEnrollmentConditions = _context.EnrollmentConditions.Where(e => e.ActivityId == activityId).ToList();

            foreach (var item in dbEnrollmentConditions)
            {
                if (enrollmentConditionsList.All(e => e.Id != item.Id))
                    _context.EnrollmentConditions.Remove(item);
            }

            _context.SaveChanges();

            return GetEnrollmentConditions(activityId);
        }

        private void ValidateEnrollmentConditions(IList<EnrollmentCondition> enrollmentConditions, int activityId)
        {
            if (activityId <= 0)
                throw new ValidationException("Invalid activityId");

            if(enrollmentConditions.Any(ec => ec.ActivityId != activityId))
                throw new ValidationException("Invalid activityId provided for given enrollment condition.");

            if (enrollmentConditions.Select(ec => new {ec.ProductId, ec.ArticleTypeId}).Distinct().Count() < enrollmentConditions.Count())
                throw new ValidationException("Only one enrollment condition may be registered for a given combination of product and articletype.");

        }

    }
}