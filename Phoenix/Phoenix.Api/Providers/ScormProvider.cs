﻿using System;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Transactions;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Messaging;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Exceptions;
using NLog.LayoutRenderers;

namespace Metier.Phoenix.Api.Providers
{
    public class ScormProvider : IScormProvider
    {
        private readonly MetierLmsContext _context;
        private readonly IPerformanceProvider _performanceProvider;
        private readonly IScormAttemptRepository _attemptRepository;
        private readonly IEventAggregator _eventAggregator;
        private readonly IAuthorizationProvider _authorizationProvider;

        public ScormProvider(MetierLmsContext context,
                             IPerformanceProvider performanceProvider,
                             IScormAttemptRepository attemptRepository,
                             IEventAggregator eventAggregator,
                             IAuthorizationProvider authorizationProvider)
        {
            _context = context;
            _performanceProvider = performanceProvider;
            _attemptRepository = attemptRepository;
            _eventAggregator = eventAggregator;
            _authorizationProvider = authorizationProvider;
        }

        public ScormAttempt GetAttempt(Guid sessionId, bool activeOnly = true)
        {
            var attempt = _context.ScormAttempts.FirstOrDefault(a => a.SessionId == sessionId);

            if (attempt == null)            
                throw new EntityNotFoundException("Session does not exist");
                        
            if (attempt.InternalState != ScormAttemptStatus.Active)
            {
                if(activeOnly)
                    throw new EntityNotFoundException("Session is terminated");
                else
                {
                    //If we allow non-active attempts, make sure it's the latest for this performanceId
                    var mostRecentAttempt = _context.ScormAttempts.FindMostRecent(attempt.PerformanceId);
                    //attempt = mostRecentAttempt;
                    if (mostRecentAttempt.Id != attempt.Id)
                        throw new EntityNotFoundException("A more recent attempt exists");
                }
            }
            if (!_authorizationProvider.IsAdminOr(attempt.UserId))
            {
                throw new UnauthorizedException("Access denied");
            }
            return attempt;
        }

        public ScormAttempt CreateSession(int userId, int rcoId)
        {
            if (!_authorizationProvider.IsAdminOr(userId))
            {
                throw new UnauthorizedException("Access denied");
            }

            if (!_context.Users.Any(u => u.Id == userId))
            {
                throw new EntityNotFoundException("User does not exist");
            }

            if (!_context.Rcos.Any(r => r.Id == rcoId))
            {
                throw new EntityNotFoundException("Rco does not exist");
            }

            _performanceProvider.CreateParentPerformanceIfNotExists(userId, rcoId);

            var performance = _context.ScormPerformances.FindByUserIdAndRocId(userId, rcoId) ??
                              CreatePerformance(userId, rcoId);

            var newAttempt = new ScormAttempt
            {
                SessionId = Guid.NewGuid(),
                UserId = userId,
                InternalState = ScormAttemptStatus.Active,
                PerformanceId = performance.Id,
                Status = performance.Status             //Setting status from the performance, as it may have been changed manually by a coordinator
            };

            var mostRecentAttempt = _context.ScormAttempts.FindMostRecent(performance.Id);
            if (mostRecentAttempt != null)
            {
                newAttempt.SuspendData = mostRecentAttempt.SuspendData;
                if (performance.Status == null)                     //I suspect this will only occur in test-scenarios, but still - it makes sense...
                    newAttempt.Status = mostRecentAttempt.Status;
            }

            AutoTerminateActiveAttempts(performance.Id);

            var attempt = _attemptRepository.Create(newAttempt);
            return attempt;
        }
        private void PerformScormActionWithRetry(Action action, int retry = 6)
        {
            while (true)
            {
                try
                {
                    action();
                    return;
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    if (--retry < 0) throw;
                    _RecoverFromDbUpdateConcurrencyException(ex);
                }
            }
        }
        public void UpdateStatus(Guid sessionId, string status)
        {
            PerformScormActionWithRetry(new Action(() => 
            {
                if (!ScormLessonStatus.IsValidStatus(status))
                {
                    throw new ValidationException("Invalid status");
                }

                var attempt = GetAttempt(sessionId, activeOnly: false);
                if (!_authorizationProvider.IsAdminOr(attempt.UserId))
                {
                    throw new UnauthorizedException("Access denied");
                }
                attempt.Status = status;

                var performance = _context.ScormPerformances.First(p => p.Id == attempt.PerformanceId);
                performance.Status = status;

                _context.SaveChanges();
            }));            
        }
        

        public void UpdateScore(Guid sessionId, int score)
        {
            PerformScormActionWithRetry(new Action(() =>
            {
                var attempt = GetAttempt(sessionId, activeOnly: false);
                attempt.Score = score;

                var performance = _context.ScormPerformances.First(p => p.Id == attempt.PerformanceId);
                performance.Score = score;

                _context.SaveChanges();
            }));                
        }
        
        public void UpdateSuspendData(Guid sessionId, string suspendData)
        {
            PerformScormActionWithRetry(new Action(() =>
            {
                var attempt = GetAttempt(sessionId, activeOnly: false);
                if (!_authorizationProvider.IsAdminOr(attempt.UserId))
                {
                    throw new UnauthorizedException("Access denied");
                }
                attempt.SuspendData = suspendData;

                _context.SaveChanges();
            }));
        } 
        public void TerminateSession(Guid sessionId)
        {
            PerformScormActionWithRetry(new Action(() =>
            {
                var attempt = GetAttempt(sessionId, activeOnly: false);
                if (!_authorizationProvider.IsAdminOr(attempt.UserId))
                {
                    throw new UnauthorizedException("Access denied");
                }
                using (var transaction = new TransactionScope())
                {
                    attempt.InternalState = ScormAttemptStatus.Complete;
                    attempt.TimeUsedInSeconds = (long)(DateTime.UtcNow - attempt.StartTime).TotalSeconds;

                    var performance = _context.ScormPerformances.First(p => p.Id == attempt.PerformanceId);

                    var isStatusChanged = performance.OriginalStatus != attempt.Status;

                    performance.Score = attempt.Score ?? performance.Score;

                    performance.Status = attempt.Status;
                    performance.OriginalStatus = attempt.Status;
                    performance.TimeUsedInSeconds = attempt.TimeUsedInSeconds + (performance.TimeUsedInSeconds ?? 0);
                    if (attempt.Status == ScormLessonStatus.Complete)
                    {
                        performance.CompletedDate = DateTime.UtcNow;
                    }

                    _context.SaveChanges();

                    if (isStatusChanged)
                    {
                        _eventAggregator.Publish(new PerformanceStatusChangedEvent { PerformanceId = performance.Id });
                    }

                    transaction.Complete();
                }
            }));                  
        }
        private void _RecoverFromDbUpdateConcurrencyException(DbUpdateConcurrencyException ex)
        {
			var objContext = ((IObjectContextAdapter)_context).ObjectContext;
			objContext.Refresh(System.Data.Entity.Core.Objects.RefreshMode.StoreWins, ex.Entries.Select(e => e.Entity));

			// Update original values from the database
			//var entry = ex.Entries.Single();
			//entry.OriginalValues.SetValues(entry.GetDatabaseValues());
		}
        

        private void AutoTerminateActiveAttempts(int performanceId)
        {
            var activeAttempts = _context.ScormAttempts.Where(a => a.InternalState == ScormAttemptStatus.Active && a.PerformanceId == performanceId);
            foreach (var activeAttempt in activeAttempts)
            {
                activeAttempt.InternalState = ScormAttemptStatus.Complete;
                activeAttempt.TimeUsedInSeconds = (long)(DateTime.UtcNow - activeAttempt.StartTime).TotalSeconds;
            }
            _context.SaveChanges();
        }

        private ScormPerformance CreatePerformance(int userId, int rcoId)
        {
            return _performanceProvider.Create(new ScormPerformance
            {
                RcoId = rcoId,
                UserId = userId,
                Status = ScormLessonStatus.NotAttempted,
                OriginalStatus = ScormLessonStatus.NotAttempted,
            });
        }       
        public void ResetRcoStatus(int rcoId, int userId, bool includeChildren)
        {
            if (!_context.Users.Any(u => u.Id == userId))
            {
                throw new EntityNotFoundException("User does not exist");
            }

            if (!_context.Rcos.Any(r => r.Id == rcoId))
            {
                throw new EntityNotFoundException("Rco does not exist");
            }

            var rcosToReset = _context.Rcos.Where(r => r.Id == rcoId || (includeChildren && r.ParentId == rcoId)).ToList();
            rcosToReset.ForEach(r => {

                var performance = _context.ScormPerformances.FindByUserIdAndRocId(userId, r.Id);
                if (performance != null)
                {
                    if (performance.Status != ScormLessonStatus.NotAttempted || performance.Score > 0 || performance.TimeUsedInSeconds != null || performance.CompletedDate != null)
                    {
                        //Reset performance
                        performance.Status = ScormLessonStatus.NotAttempted;
                        performance.Score = 0;
                        performance.TimeUsedInSeconds = null;
                        performance.CompletedDate = null;

                    }
                    ResetAttemptStatusIfNecessary(performance.Id);
                }
            });
            _context.SaveChanges();
        }

        public void ResetAttemptStatusIfNecessary(int performanceId)
        {
            var mostRecentAttempt = _context.ScormAttempts.FindMostRecent(performanceId);
            if (mostRecentAttempt != null)
            {
                AutoTerminateActiveAttempts(performanceId);

                if (mostRecentAttempt.Score != null || !String.IsNullOrEmpty(mostRecentAttempt.SuspendData) || mostRecentAttempt.Status != ScormLessonStatus.NotAttempted || mostRecentAttempt.TimeUsedInSeconds > 0)
                {
                    //Insert blank attempt
                    var newAttempt = new ScormAttempt
                    {
                        SessionId = Guid.NewGuid(),
                        UserId = mostRecentAttempt.UserId,
                        InternalState = ScormAttemptStatus.Complete,
                        PerformanceId = performanceId,
                        Status = ScormLessonStatus.NotAttempted
                    };
                    _attemptRepository.Create(newAttempt);
                }
            }
        }
    }
}