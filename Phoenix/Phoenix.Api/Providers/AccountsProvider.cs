﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Transactions;
using System.Web.Security;
using Metier.Phoenix.Api.Configuration;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade.Account;
using Metier.Phoenix.Api.Facade.Codes;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Core.Security;
using Metier.Phoenix.Core.Utilities.Validation;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Templates.PasswordResetRequest;
using Metier.Phoenix.Mail.Templates.UserRegistrationNotification;
using NLog;
using ValidationException = Metier.Phoenix.Core.Exceptions.ValidationException;

namespace Metier.Phoenix.Api.Providers
{
    public class AccountsProvider : IAccountsProvider
    {
        private static readonly object SyncRoot = new object();

        private readonly IUsersRepository _usersRepository;
        private readonly IPhoenixMembership _phoenixMembership;
        private readonly IPhoenixMembershipRoles _phoenixRoles;
        private readonly ICustomersProvider _customersProvider;
        private readonly IIdentityProvider _identityProvider;
        private readonly IDistributorRepository _distributorRepository;
        private readonly MetierLmsContext _context;
        private readonly IConfiguration _configuration;
        private readonly IAutoMailer _autoMailer;
        private readonly IAuthorizationProvider _authorizationProvider;
        private readonly IUserRegistrationNotificationFactory _userRegistrationNotificationFactory;
        private readonly ICodeRepository _codeRepository;
        private readonly IPasswordResetRequestFactory _passwordResetRequestFactory;
        private readonly IPasswordResetRequestMessageFactory _passwordResetRequestMessageFactory;
        private readonly Logger _logger;

        public AccountsProvider(IUsersRepository usersRepository,
            IPhoenixMembership phoenixMembership,
            IPhoenixMembershipRoles phoenixRoles,
            ICustomersProvider customersProvider,
            IIdentityProvider identityProvider,
            IDistributorRepository distributorRepository,
            MetierLmsContext context,
            IConfiguration configuration,
            IAutoMailer autoMailer,
            IAuthorizationProvider authorizationProvider,
            IUserRegistrationNotificationFactory userRegistrationNotificationFactory,
            ICodeRepository codeRepository,
            IPasswordResetRequestFactory passwordResetRequestFactory,
            IPasswordResetRequestMessageFactory passwordResetRequestMessageFactory)
        {
            _usersRepository = usersRepository;
            _phoenixMembership = phoenixMembership;
            _phoenixRoles = phoenixRoles;
            _customersProvider = customersProvider;
            _identityProvider = identityProvider;
            _distributorRepository = distributorRepository;
            _context = context;
            _configuration = configuration;
            _autoMailer = autoMailer;
            _authorizationProvider = authorizationProvider;
            _userRegistrationNotificationFactory = userRegistrationNotificationFactory;
            _codeRepository = codeRepository;
            _passwordResetRequestFactory = passwordResetRequestFactory;
            _passwordResetRequestMessageFactory = passwordResetRequestMessageFactory;
        }

        public User CreateUser(UserAccount account, bool autoGeneratePassword, string source)
        {
            lock (SyncRoot)
            {
                var createValidationErrors = ValidateForCreate(account).ToList();
                if (createValidationErrors.Any())
                {
                    throw new ValidationException("Could not create user", createValidationErrors);
                }

                User user;
                using (var transactionScope = new TransactionScope())
                {
                    user = account.User ?? new User();
                    user.CustomerId = account.CustomerId;
                    if (!string.IsNullOrEmpty(account.Username)) account.Username = account.Username.Trim();
                    if (!string.IsNullOrEmpty(account.Email)) account.Email = account.Email.Trim();

                    user.MembershipUserId = account.Username;
                    user.IsPasswordGeneratedOnCreate = autoGeneratePassword;
                    user.IsAllowUserEcts = user.IsAllowUserEcts.HasValue ? user.IsAllowUserEcts : true;

                    if (account.CustomerId == 0)
                    {
                        var newCustomer = _customersProvider.CreateArbitraryCustomer(account);
                        user.CustomerId = newCustomer.Id;

                        user = _usersRepository.Create(user);
                        newCustomer.UserId = user.Id;
                        _customersProvider.Update(newCustomer.Id, newCustomer);
                    }
                    else
                    {
                        user = _usersRepository.Create(user);
                    }

                    if (autoGeneratePassword)
                    {
                        _phoenixMembership.CreateUser(account.Username, _phoenixMembership.GeneratePassword(16, 3), account.Email);
                    }
                    else
                    {
                        _phoenixMembership.CreateUser(account.Username, account.Password, account.Email);
                    }
                    if (account.Roles == null)
                    {
                        account.Roles = new List<string>();
                    }
                    if (account.Roles != null)
                    {
                        var roles = account.Roles.Distinct().ToArray();
                        if (roles.Any())
                        {
                            Roles.AddUserToRoles(account.Username, roles);
                        }
                    }

                    account = GetByUserId(user.Id);

                    SendUserRegistrationAutomailIfQualifying(account, source);

                    transactionScope.Complete();
                }
                return user;
            }
        }

        public ArbitraryAccountResponse CreateArbitraryAccount(ArbitraryAccountRequest request, string source)
        {
            lock (SyncRoot)
            {
                var validationResults = Validate(request).ToList();
                if (validationResults.Any())
                {
                    throw new ValidationException("Could not create arbitrary user", validationResults);
                }
                bool isNewUser = false;

                UserAccount userAccount = null;
                var customer = _customersProvider.FindSimilarCustomer(request);
                if (customer != null)
                    customer = _customersProvider.GetCustomer(customer.Id);

                var username = _phoenixMembership.GetUserNameByEmail(request.Email.ToLower());

				if (!string.IsNullOrEmpty(username))
				{
					username = username.Trim();
					userAccount = GetByUsername(username);
				}

				//If no user exists with Email == request.Email, try looking up user by Username == request.Email
				if (string.IsNullOrEmpty(username))
				{
					userAccount = GetByUsername(request.Email.ToLower());
					if (userAccount != null) username = userAccount.Username;
				}

                using (var transaction = new TransactionScope())
                {
                    if (userAccount == null)
                    {
                        isNewUser = true;
                        userAccount = new UserAccount
                        {
                            DistributorId = request.DistributorId,
                            User = new User
                            {
                                FirstName = request.FirstName,
                                LastName = request.LastName,
                                PreferredLanguageId = request.PreferredLanguageId,
                                City = request.City,
                                Country = request.Country.ToUpper(),
                                StreetAddress = request.StreetAddress1,
                                StreetAddress2 = request.StreetAddress2,
                                ZipCode = request.ZipCode,
                                PhoneNumber = request.Phone,
                                MembershipUserId = request.Email.Trim(),
                                UserRegistrationNotificationDate = DateTime.UtcNow,
                                IsAllowUserEcts = true
                            },
                            CompanyName = request.CompanyName,
                            CompanyRegistrationNumber = request.CompanyRegistrationNumber,
                            OurRef = request.OurRef,
                            Email = request.Email.Trim(),
                            Username = request.Email.Trim()
                        };

                        // Create new customer if it wasn't found
                        if (customer == null)
                        {
                            customer = _customersProvider.CreateArbitraryCustomer(request);
                            //customer = _customersProvider.CreateArbitraryCustomer(userAccount);
                        }
                        else
                        {
                            if (customer.CustomerInvoicing == null) customer.CustomerInvoicing = new CustomerInvoicing();
                            customer.CustomerInvoicing.OurRef = request.OurRef;
                        }

                        userAccount.User.CustomerId = customer.Id;
                        userAccount.User = _usersRepository.Create(userAccount.User);
                        userAccount.User.IsPasswordGeneratedOnCreate = true;
                        _phoenixMembership.CreateUser(userAccount.Username, _phoenixMembership.GeneratePassword(16, 3), userAccount.Email);
                        Roles.AddUserToRoles(userAccount.Username, new[] { PhoenixRoles.LearningPortalUser });
                    }

                    // Create new customer if it wasn't found
                    if (customer == null)
                    {
                        userAccount.CustomerId = 0;
                        userAccount.DistributorId = request.DistributorId;
                        userAccount.CompanyName = request.CompanyName;
                        userAccount.OurRef = request.OurRef;
                        request.PreferredLanguageId = userAccount.User.PreferredLanguageId ?? request.LanguageId;
                        customer = _customersProvider.CreateArbitraryCustomer(request);
                    }
					else
					{
                        if (customer.CustomerInvoicing == null) customer.CustomerInvoicing = new CustomerInvoicing();
                        customer.CustomerInvoicing.OurRef = request.OurRef;
					}

                    // Relate the user to the customer
                    if (userAccount.User.Customer.IsArbitrary && userAccount.User.CustomerId != customer.Id)
                    {
                        // Update the user with the new arbitrary customer if it was previously related to a different arbitrary customer
                        userAccount.User.CustomerId = customer.Id;
                        //userAccount.CustomerId = customer.Id;
                        userAccount.User = _usersRepository.Update(userAccount.User);
                    }

                    // Relate the customer to the user
                    customer.UserId = userAccount.User.Id;
                    customer = _customersProvider.Update(customer.Id, customer);

                    transaction.Complete();
                }

                if (isNewUser)
                {
                    SendUserRegistrationAutomailIfQualifying(userAccount, source);
                }

                return new ArbitraryAccountResponse
                {
                    UserId = userAccount.User.Id,
                    ArbitraryCustomerId = customer.Id
                };
            }
        }

        private IEnumerable<ValidationResult> Validate(ArbitraryAccountRequest request)
        {
            if (request.DistributorId <= 0)
                yield return new ValidationResult("DistributorId is required");
            if (request.LanguageId <= 0)
                yield return new ValidationResult("LanguageId is required");
            if (string.IsNullOrEmpty(request.FirstName))
                yield return new ValidationResult("FirstName is required");
            if (string.IsNullOrEmpty(request.LastName))
                yield return new ValidationResult("LastName is required");
            if (string.IsNullOrEmpty(request.Email))
                yield return new ValidationResult("Email is required");
            if (!string.IsNullOrEmpty(request.Email) && !EmailValidation.IsValidEmail(request.Email))
                yield return new ValidationResult("Invalid Email");
            if (string.IsNullOrEmpty(request.Phone))
                yield return new ValidationResult("Phone is required");
            if (string.IsNullOrEmpty(request.CompanyName))
                yield return new ValidationResult("CompanyName is required");
            if (string.IsNullOrEmpty(request.StreetAddress1))
                yield return new ValidationResult("StreetAddress1 is required");
            if (string.IsNullOrEmpty(request.ZipCode))
                yield return new ValidationResult("ZipCode is required");
            if (string.IsNullOrEmpty(request.City))
                yield return new ValidationResult("City is required");
            if (string.IsNullOrEmpty(request.Country))
                yield return new ValidationResult("Country is required");
            if (!_codeRepository.IsValidCodeValue(request.Country.ToUpper(), CodeTypes.CountryCodes))
                yield return new ValidationResult("Invalid ISO3166 Alpha-2 Country Code (Two letter: NO, SV, etc)");
        }

        public bool ChangePassword(string username, string newPassword)
        {
            if (!_authorizationProvider.IsAdminOr(username))
            {
                throw new UnauthorizedException("Cannot change other users's password");
            }

            return ChangePasswordCore(username, newPassword);
        }

        public bool ResetPassword(Guid token, string email, string newPassword)
        {
            var user = GetByEmail(email);
            var passwordResetRequest = _context.PasswordResetRequests.SingleOrDefault(p => p.ResetToken == token && p.UserId == user.User.Id);
            if (passwordResetRequest == null)
            {
                throw new ValidationException(string.Format("The password reset token {0} provided for user with email {1} is not valid", token, email));
            }
            var ageForTokenInMinutes = (DateTime.UtcNow - passwordResetRequest.Created).TotalMinutes;
            var maximumAgeForTokenInMinutes = _configuration.PasswordResetTokenExpirationInMinutes;

            if (!IsResetRequestForVirginUser(user.User, passwordResetRequest) && (ageForTokenInMinutes > maximumAgeForTokenInMinutes))
            {
                throw new ValidationException("Token has expired");
            }
            _context.PasswordResetRequests.Remove(passwordResetRequest);
            _context.SaveChanges();

            return ChangePasswordCore(user.Username, newPassword);
        }

        //Struggeled to find a good function name, but this applies to newly created users that has never logged in before.
        private static bool IsResetRequestForVirginUser(User user, PasswordResetRequest passwordResetRequest)
        {
            if (user.UserRegistrationNotificationDate.HasValue && user.UserRegistrationNotificationDate.Value.Subtract(passwordResetRequest.Created).Days == TimeSpan.FromDays(0).Days)
                return true;

            return false;
        }

        public UserAccount GetByUserId(int userId)
        {
            var user = _context.Users.WithAll().FindById(userId);

            if (user == null)
            {
                return null;
            }

            var membershipUser = _phoenixMembership.GetUser(user.MembershipUserId, false);

            if (membershipUser == null)
            {
                return null;
            }

            return CreateUserAccountDto(user, membershipUser, Roles.GetRolesForUser(membershipUser.UserName), GetDefaultDistributor(user));
        }

        public UserAccount GetByUsername(string username)
        {
            if (!string.IsNullOrEmpty(username)) username = username.Trim();

            var membershipUser = _phoenixMembership.GetUser(username, false);

            if (membershipUser == null)
            {
                return null;
            }

            var user = _usersRepository.FindByUsername(membershipUser.UserName);

            if (user == null)
            {
                return null;
            }

            return CreateUserAccountDto(user, membershipUser, _phoenixRoles.GetRolesForUser(username), GetDefaultDistributor(user));
        }

        public UserAccount GetByEmail(string email)
        {
            var username = _phoenixMembership.GetUserNameByEmail(email.ToLower());
            if (string.IsNullOrEmpty(username))
            {
                throw new EntityNotFoundException(string.Format("No user exists with email address {0}", email));
            }
            return GetByUsername(username);
        }

        public UserAccount GetCurrentUser()
        {
            var userName = _identityProvider.GetCurrentUser().Identity.Name;
            return GetByUsername(userName);
        }

        private UserAccount CreateUserAccountDto(User user, MembershipUser membershipUser, IEnumerable<string> roles, Distributor defaultDistributor)
        {
            return new UserAccount
            {
                CustomerId = user.CustomerId,
                Email = membershipUser.Email,
                Username = membershipUser.UserName,
                User = user,
                Roles = roles.ToList(),
                UserContext = new UserContext { DefaultDistributor = defaultDistributor }
            };
        }

        public UserAccount Update(UserAccount account)
        {
            lock (SyncRoot)
            {
                if (!string.IsNullOrWhiteSpace(account.Username)) account.Username = account.Username.Trim();             //Make sure no leading or trailing spaces are added.
                if (!string.IsNullOrWhiteSpace(account.NewUsername)) account.NewUsername = account.NewUsername.Trim();
                if (!string.IsNullOrWhiteSpace(account.Email)) account.Email = account.Email.Trim();

                var username = account.Username;

                var membershipUser = _phoenixMembership.GetUser(account.Username, false);

                if (membershipUser == null)
                {
                    throw new EntityNotFoundException(string.Format("No user found with username {0}.",
                        account.Username));
                }


                var currentUserName = _identityProvider.GetCurrentUser().Identity.Name;
                var currentUserRoles = Roles.GetRolesForUser(currentUserName);

                var isElevatedUser = currentUserRoles != null && (currentUserRoles.Contains(PhoenixRoles.LearningPortalTokenUser) || currentUserRoles.Contains(PhoenixRoles.PhoenixUser));
                if (!isElevatedUser)
                {
                    //If LearningPortalUser, verify that account.Username is username of current user
                    if (currentUserName != account.Username) throw new UsernameChangeException(string.Format("User is not authorized to update user {0}", account.Username));

                    //Copy Roles from the previous value
                    account.Roles = currentUserRoles.ToList();
                }


                using (var transactionScope = new TransactionScope())
                {
                    if (!string.IsNullOrWhiteSpace(account.NewUsername))
                    {
                        var success = _phoenixMembership.ChangeUsername(membershipUser.UserName, account.NewUsername);
                        if (!success)
                        {
                            throw new UsernameChangeException(string.Format("Could not change username for user {0}",
                                account.Username));
                        }
                        username = account.NewUsername;
                        membershipUser = _phoenixMembership.GetUser(username, false);
                        if (membershipUser == null)
                        {
                            throw new EntityNotFoundException(string.Format("Could not retrieve user with username {0}.",
                                username));
                        }
                        account.User.MembershipUserId = account.NewUsername;
                    }

                    if (membershipUser.Email != account.Email)
                    {
                        membershipUser.Email = account.Email;
                        _phoenixMembership.UpdateUser(membershipUser);
                    }

                    if (account.Roles != null)
                    {
                        var currentRoles = Roles.GetRolesForUser(username);
                        var removedRoles = currentRoles.Except(account.Roles).Distinct().ToArray();
                        var addedRoles = account.Roles.Except(currentRoles).Distinct().ToArray();

                        if (removedRoles.Any(r => r.ToLower() == PhoenixRoles.PhoenixUser.ToLower()) &&
                            username == _identityProvider.GetCurrentUser().Identity.Name)
                        {
                            throw new RoleUpdateException("Cannot remove phoenix role on logged in user");
                        }

                        if (removedRoles.Length > 0)
                        {
                            Roles.RemoveUserFromRoles(username, removedRoles);
                        }
                        if (addedRoles.Length > 0)
                        {
                            Roles.AddUserToRoles(username, addedRoles);
                        }
                    }

                    var user = _usersRepository.Update(account.User);
                    var defaultDistributor = GetDefaultDistributor(user);
                    membershipUser = _phoenixMembership.GetUser(username, false);
                    var roles = Roles.GetRolesForUser(username);
                    transactionScope.Complete();
                    account = CreateUserAccountDto(user, membershipUser, roles, defaultDistributor);
                }
                return account;
            }
        }

        public void MergeAccounts(int userId, int userIdToMerge)
        {
            using (var transaction = new TransactionScope())
            {
                var currentUserAccount = GetCurrentUser();
                var primaryAccount = GetByUserId(userId);
                var secondaryAccount = GetByUserId(userIdToMerge);

                if (primaryAccount == null || secondaryAccount == null)
                    throw new ValidationException("Could not find one or both accounts of the users to merge. Merge aborted.");

                if (currentUserAccount.User.Id == userId || currentUserAccount.User.Id == userIdToMerge)
                    throw new ValidationException("Currently logged in user can not be part of the merge. Could not merge users.");

                lock (SyncRoot)
                {
                    _usersRepository.MergeUsers(primaryAccount.User, secondaryAccount.User);
                }

                Membership.DeleteUser(secondaryAccount.Username, true);
                AddComment(primaryAccount.User, secondaryAccount.User, currentUserAccount);

                _context.SaveChanges();
                transaction.Complete();
            }
        }

        private void AddComment(User primaryUser, User secondaryUser, UserAccount currentUserAccount)
        {
            string comment = $"Successfully merged user {secondaryUser.MembershipUserId} into {primaryUser.MembershipUserId}.";
            _context.Comments.Add(new Comment
            {
                Entity = "user",
                EntityId = primaryUser.Id,
                CommentText = comment,
                User = currentUserAccount.User,
                UserId = currentUserAccount.User.Id,
                IsDeleted = false,
                Created = DateTime.UtcNow,
                LastModified = DateTime.UtcNow
            });
        }


        public UserContext GetUserContext(string username)
        {
            var userAccount = GetByUsername(username);
            return userAccount.UserContext;
        }

        private Distributor GetDefaultDistributor(User user)
        {
            var defaultDistributor = _distributorRepository.GetDistributors().First(d => d.Id == user.Customer.DistributorId);
            return defaultDistributor;
        }

        public void RequestPasswordReset(string email, string source)
        {
            var userAccount = GetByEmail(email);

            if (userAccount.User.UserRegistrationNotificationDate.HasValue && userAccount.User.UserRegistrationNotificationDate.Value.Date > DateTime.Today)
            {
                throw new ValidationException("Cannot reset password when user's registration notification date is in the future.");
            }

            using (var scope = new TransactionScope())
            {
                var distributor = _context.Distributors.FirstOrDefault(d => d.Id == userAccount.User.Customer.DistributorId);
                var resetPasswordFormUrl = (distributor == null || string.IsNullOrWhiteSpace(distributor.PasswordResetFormUrl))
                    ? _configuration.GetPasswordResetFormUrl(source)
                    : distributor.PasswordResetFormUrl;

                if (string.IsNullOrEmpty(resetPasswordFormUrl))
                {
                    throw new ValidationException("Invalid source.");
                }

                var passwordResetRequest = _passwordResetRequestFactory.CreatePasswordResetRequest(userAccount.User);

                var autoMailMessage = _passwordResetRequestMessageFactory.CreatePasswordResetMessage(passwordResetRequest.ResetToken, userAccount.Email, userAccount.User.Id, resetPasswordFormUrl);
                var sendSuccess = _autoMailer.Send(autoMailMessage);
                if (!sendSuccess)
                {
                    throw new MailSendException(string.Format("Failed to send mail to recipient {0}", userAccount.Email));
                }

                scope.Complete();
            }
        }

        private bool ChangePasswordCore(string username, string newPassword)
        {
            if (string.IsNullOrEmpty(newPassword))
            {
                throw new ValidationException("New password can not be empty");
            }

            var randomPassword = _phoenixMembership.GetProvider(_context).ResetPassword(username, null);
            return _phoenixMembership.GetProvider(_context).ChangePassword(username, randomPassword, newPassword);
        }

        public bool Validate(string username, string password)
        {
            var user = _context.Users.FirstOrDefault(u => u.MembershipUserId.ToLower() == username.ToLower());

            if (user == null)
            {
                return false;
            }
            if (user.ExpiryDate.HasValue && user.ExpiryDate.Value < DateTime.UtcNow)
            {
                throw new ValidationException("Your user profile has expired", ValidationErrorCodes.UserExpired);
            }

            return _phoenixMembership.GetProvider(_context).ValidateUser(username, password);
        }

        private IEnumerable<ValidationResult> ValidateForCreate(UserAccount account)
        {
            if (account.User != null)
            {
                if (account.User.Id != 0)
                {
                    yield return new ValidationResult("Id property of user is not 0. Cannot create user to existing user.");
                }
                if (account.CustomerId != account.User.CustomerId)
                {
                    yield return new ValidationResult("CustomerId property must match CustomerId property in User object.");
                }
                if(!EmailValidation.IsValidEmail(account.Email))
                {
                    yield return new ValidationResult("Invalid email address on user.");
                }
            }

            var membershipUser = _phoenixMembership.GetUser(account.Username, false);
            if (membershipUser != null)
            {
                yield return new ValidationResult("Username already used");
            }
        }

        private void SendUserRegistrationAutomailIfQualifying(UserAccount userAccount, string source)
        {
            var shouldSendAutomail = userAccount.User.UserRegistrationNotificationDate.HasValue &&
                                     userAccount.User.UserRegistrationNotificationDate.Value.Date <= DateTime.Today;

            if (shouldSendAutomail)
            {
				var customer = _context.Customers.Include(c => c.AutomailDefinition).First(c => c.Id == userAccount.User.CustomerId);
				var customerAutomailDefinition = customer.AutomailDefinition;
                var welcomeNotificationEnabled = customerAutomailDefinition != null && (customerAutomailDefinition.IsWelcomeNotification.HasValue && customerAutomailDefinition.IsWelcomeNotification.Value);
                var userNotificationEnabled = userAccount.User.UserNotificationType == UserNotificationTypes.Yes;
                var userExpired = userAccount.User.ExpiryDate.HasValue && userAccount.User.ExpiryDate.Value.Date <= DateTime.UtcNow.Date;

                if (welcomeNotificationEnabled && userNotificationEnabled && !userExpired)
                {
					var sendSeparatePasswordResetEmail = false;
					var sendPasswordResetInstructions = userAccount.User.IsPasswordGeneratedOnCreate.HasValue && userAccount.User.IsPasswordGeneratedOnCreate.Value;

					sendSeparatePasswordResetEmail = _context.Distributors.FirstOrDefault(d => d.Id == customer.DistributorId).SendSeparatePasswordResetEmail;

					Guid? resetToken = null;
					string passwordResetFormUrl = null;
					if (!sendSeparatePasswordResetEmail && sendPasswordResetInstructions)
					{

						var distributor = _context.Distributors.FirstOrDefault(d => d.Id == customer.DistributorId);
						passwordResetFormUrl = (distributor == null || string.IsNullOrWhiteSpace(distributor.PasswordResetFormUrl))
							? _configuration.GetPasswordResetFormUrl(source)
							: distributor.PasswordResetFormUrl;

						if (string.IsNullOrEmpty(passwordResetFormUrl))
						{
							throw new ValidationException("Invalid source.");
						}

						var passwordResetRequest = _passwordResetRequestFactory.CreatePasswordResetRequest(userAccount.User);
						resetToken = passwordResetRequest.ResetToken;
					}

					var mailMessage = _userRegistrationNotificationFactory.CreateUserRegistrationNotificationMessage(userAccount.User, passwordResetFormUrl, resetToken);
                    _autoMailer.Send(mailMessage);

                    if (!resetToken.HasValue)
                    {
                        RequestPasswordReset(userAccount.Email, source);
                    }
                }
            }
        }
    }
}