﻿using System;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Api.Facade.Mail;
using Metier.Phoenix.Api.Services.Agresso.Invoicing.References;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Core.Utilities.Collections;
using Metier.Phoenix.Mail;

namespace Metier.Phoenix.Api.Providers
{
    public class MailsProvider : IMailsProvider
    {
        private readonly MetierLmsContext _context;
        private readonly IAutoMailer _autoMailer;

        public MailsProvider(MetierLmsContext context, IAutoMailer autoMailer)
        {
            _context = context;
            _autoMailer = autoMailer;
        }

        public PartialList<SearchableMail> Search(string query, int[] messageTypes, int[] mailStatuses, DateTime? fromDate, DateTime? toDate, string orderBy,
            string orderDirection, int skip, int limit)
        {
            IQueryable<SearchableMail> mails = _context.SearchableMails;

            if (!string.IsNullOrEmpty(query))
            {
                foreach (var subQuery in query.Split(' '))
                {
                    var q = subQuery;
                    mails = mails.Where(m => m.RecipientEmail.Contains(q) || m.FirstName.Contains(q) || m.LastName.Contains(q) || m.Subject.Contains(q));
                }
            }

            if (mailStatuses != null && mailStatuses.Any())
            {
                mails = mails.Where(m => mailStatuses.Contains((int)m.MailStatus));
            }

            if (messageTypes != null && messageTypes.Any())
            {
                mails = mails.Where(m => messageTypes.Contains((int)m.MessageType));
            }

            if (fromDate.HasValue)
            {
                mails = mails.Where(m => DbFunctions.CreateDateTime(m.StatusTime.Value.Year, m.StatusTime.Value.Month, m.StatusTime.Value.Day, 0, 0, 0) >= fromDate.Value);
            }
            if (toDate.HasValue)
            {
                mails = mails.Where(m => DbFunctions.CreateDateTime(m.StatusTime.Value.Year, m.StatusTime.Value.Month, m.StatusTime.Value.Day, 0, 0, 0) <= toDate.Value);
            }

            var totalCount = mails.Count();
            mails = Order(mails, orderBy, orderDirection);
            mails = mails.Skip(skip).Take(limit);

            return mails.ToPartialList(totalCount, orderBy);
        }

        public MailSentResponse Send(int mailId)
        {
            _autoMailer.Send(mailId);
            
            var sentMail = _context.Mails.Include(m => m.MailStatuses).First(m => m.Id == mailId);
            var status = sentMail.MailStatuses.OrderByDescending(s => s.StatusTime).First();
            return new MailSentResponse
            {
                Id = mailId,
                Status = status.Status,
                StatusTime = status.StatusTime
            };
        }

        public MailPreview GetPreview(int mailId)
        {
            var autoMailMessage = _autoMailer.Get(mailId);
            return new MailPreview
            {
                Body = autoMailMessage.Body,
                RecipientEmail = autoMailMessage.RecipientEmail,
                Subject = autoMailMessage.Subject
            };
        }
        
        private IQueryable<SearchableMail> Order(IQueryable<SearchableMail> mails, string orderBy, string orderDirection)
        {
            orderBy = orderBy == null ? "" : orderBy.ToLower();
            orderDirection = orderDirection == null ? "" : orderDirection.ToLower();

            switch (orderBy)
            {
                case "recipientemail":
                    return orderDirection == "desc" ? mails.OrderByDescending(o => o.RecipientEmail) : mails.OrderBy(o => o.RecipientEmail);
                case "created":
                    return orderDirection == "desc" ? mails.OrderByDescending(o => o.Created) : mails.OrderBy(o => o.Created);
                case "statustime":
                    return orderDirection == "desc" ? mails.OrderByDescending(o => o.StatusTime) : mails.OrderBy(o => o.StatusTime);
                case "mailstatus":
                    return orderDirection == "desc" ? mails.OrderByDescending(o => o.MailStatus) : mails.OrderBy(o => o.MailStatus);
                case "messagetype":
                    return orderDirection == "desc" ? mails.OrderByDescending(o => o.MessageType) : mails.OrderBy(o => o.MessageType);
                case "firstname":
                    return orderDirection == "desc" ? mails.OrderByDescending(o => o.FirstName) : mails.OrderBy(o => o.FirstName);
                case "lastname":
                    return orderDirection == "desc" ? mails.OrderByDescending(o => o.LastName) : mails.OrderBy(o => o.LastName);
                case "subject":
                    return orderDirection == "desc" ? mails.OrderByDescending(o => o.Subject) : mails.OrderBy(o => o.Subject);
                default:
                    return mails.OrderByDescending(o => o.StatusTime);
            }
        }
    }
}