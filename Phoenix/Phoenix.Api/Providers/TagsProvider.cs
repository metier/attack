﻿using System.Linq;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Providers
{
    public class TagsProvider : ITagsProvider
    {
        private readonly MetierLmsContext _context;
        private readonly IValidate<Tag> _tagValidator;

        public TagsProvider(MetierLmsContext context, IValidate<Tag> tagValidator)
        {
            _context = context;
            _tagValidator = tagValidator;
        }

        public PartialList<Tag> SearchTags(string query, int[] tagIds, string orderBy, string orderDirection, int? skip, int? limit)
        {
            IQueryable<Tag> tags = _context.Tags;

            if (!string.IsNullOrEmpty(query))
            {
                foreach (string subQuery in query.Split(' '))
                {
                    var q = subQuery;
                    tags = tags.Where(c => c.Text.Contains(q));
                }
            }

            if (tagIds != null && tagIds.Length > 0)
            {
                tags = tags.Where(t => tagIds.ToList().Contains(t.Id));
            }

            var totalCount = tags.Count();
            tags = Order(tags, orderBy, orderDirection);

            if (skip.HasValue)
            {
                tags = tags.Skip(skip.Value);
            }
            if (limit.HasValue && limit.Value > 0)
            {
                tags = tags.Take(limit.Value);
            }

            return tags.ToPartialList(totalCount, orderBy);
        }

        public Tag Create(Tag tag)
        {
            var validationResults = _tagValidator.Validate(tag).ToList();
            if (validationResults.Any())
            {
                throw new ValidationException("Invalid tag", validationResults);
            }

            _context.Tags.Add(tag);
            _context.SaveChanges();

            return tag;
        }

        private IQueryable<Tag> Order(IQueryable<Tag> customers, string orderBy, string orderDirection)
        {
            orderBy = orderBy == null ? "" : orderBy.ToLower();
            orderDirection = orderDirection == null ? "" : orderDirection.ToLower();

            switch (orderBy)
            {
                case "text":
                    return orderDirection == "desc" ? customers.OrderByDescending(c => c.Text) : customers.OrderBy(c => c.Text);
                default:
                    return orderDirection == "desc" ? customers.OrderByDescending(c => c.Text) : customers.OrderBy(c => c.Text);
            }
        }
    }
}