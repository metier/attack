﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Transactions;
using AutoMapper;
using Castle.Core.Internal;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Factories;
using Metier.Phoenix.Api.Services.ExternalCustomers;
using Metier.Phoenix.Api.Services.Invoices;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;
using Metier.Phoenix.Core.Utilities.Logging;
using NLog;
using ValidationException = Metier.Phoenix.Core.Exceptions.ValidationException;

namespace Metier.Phoenix.Api.Providers
{
    public class OrdersProvider : IOrdersProvider
    {
        private static readonly object InvoiceSyncRoot = new object();
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly IOrderRepository _orderRepository;
        private readonly ICustomersProvider _customersProvider;
        private readonly IOrdersService _ordersService;
        private readonly IOrdersProviderResolver _providerResolver;
        private readonly IParticipantsProvider _participantsProvider;
        private readonly IActivityProvider _activityProvider;
        private readonly IOrderFactory _orderFactory;
        private readonly IOrderInfoFactory _orderInfoFactory;
        private readonly IValidateOrder _orderValidator;
        private readonly MetierLmsContext _context;
        private readonly IExternalCustomersProviderFactory _externalCustomersProviderFactory;

        public OrdersProvider(IOrderRepository orderRepository, 
                              ICustomersProvider customersProvider, 
                              IOrdersService ordersService,
                              IOrdersProviderResolver providerResolver,
                              IParticipantsProvider participantsProvider,
                              IActivityProvider activityProvider,
                              IOrderFactory orderFactory,
                              IValidateOrder orderValidator,
                              MetierLmsContext context,
                              IExternalCustomersProviderFactory externalCustomersProviderFactory,
                              IOrderInfoFactory orderInfoFactory)
        {
            Mapper.CreateMap<Order, OrderInfo>()
                  .ForMember(o => o.Participants, opt => opt.Ignore())
                  .ForMember(o => o.FixedPriceActivities, opt => opt.Ignore());

            Mapper.CreateMap<OrderInfo, Order>()
                  .ForMember(o => o.ParticipantOrderLines, opt => opt.Ignore())
                  .ForMember(o => o.ActivityOrderLines, opt => opt.Ignore());

            _orderRepository = orderRepository;
            _customersProvider = customersProvider;
            _ordersService = ordersService;
            _providerResolver = providerResolver;
            _participantsProvider = participantsProvider;
            _activityProvider = activityProvider;
            _orderFactory = orderFactory;
            _orderInfoFactory = orderInfoFactory;
            _orderValidator = orderValidator;
            _context = context;
            _externalCustomersProviderFactory = externalCustomersProviderFactory;
        }

        public OrderInfo GetOrderInfo(int orderId)
        {
            var order = _orderRepository.FindOrder(orderId);

            if(order != null)
            {
                return _orderInfoFactory.ConvertOrder(order);
            }
                
            return null;
         }

        public OrderInfo GetProposedOrderInfo(string groupByKey)
        {
            var order = _orderRepository.FindProposedOrder(groupByKey);
            if (order != null)
            {
                return _orderInfoFactory.ConvertOrder(order);
            }

            return null;
        }

        public IEnumerable<OrderLine> GetOrderInfoLinesByOrderId(int id)
        {
            var order = _orderRepository.FindOrder(id);
            if (order != null)
            {
                return GetOrderInfoLines(order);
            }
            return null;
        }
       
        public OrderInfo ResetOrder(int id)
        {
            var order = _orderRepository.FindOrder(id);

            if (order == null)
                return null;

            if (order.Status != OrderStatus.Rejected)
            {
                throw new ValidationException("Can only reset rejected orders");
            }

            if(order.ParticipantOrderLines?.Count > 0 &&
               order.ParticipantOrderLines.Join(_context.ParticipantOrderLines.Include(pol=> pol.Participant).Include(pol=> pol.Order), line=> line.Participant.Id, pol=> pol.Participant.Id, (line, pol)=> pol).Any(pol => pol.Order.Id > order.Id))
                throw new ValidationException("One or more participants are added to a succeding order. Please contact the system administrator in order to solve this situation.");

            if (order.ActivityOrderLines?.Count > 0 &&
                order.ActivityOrderLines.Join(_context.ActivityOrderLines.Include(aol => aol.Activity), line => line.Activity.Id, aol => aol.Activity.Id, (line, aol) => aol).Any(aol => aol.Order.Id > order.Id))
                throw new ValidationException("One or more participants are added to a succeding order. Please contact the system administrator in order to solve this situation.");

            order.OrderNumber = null;
            ResetOrderLines(order);
            order.Status = OrderStatus.Draft;

            return _orderInfoFactory.ConvertOrder(_orderRepository.Update(id, order, true));
        }

        private void ResetOrderLines(Order order)
        {
            //Do not reset orderlines on CreditNotes
            if (order.IsCreditNote || order.IsFullCreditNote)
                return;

            foreach (var line in order.ParticipantOrderLines)
            {
                line.ActivityName = string.Empty;
                line.Price = null;
                line.CurrencyCodeId = null;
                line.InfoText = string.Empty;
                line.OnlinePaymentReference = string.Empty;
            }

            foreach (var line in order.ActivityOrderLines)
            {
                line.ActivityName = string.Empty;
                line.Price = null;
                line.CurrencyCodeId = null;
            }

        }

        public OrderInfo CreditOrder(int id)
        {
            OrderInfo result;

            var order = _orderRepository.FindOrder(id);
            if (order == null)
            {
                throw new EntityNotFoundException(string.Format("Could not find order with ID {0}", id));
            }

            if (!order.IsCreditNote)
            {
                _orderValidator.ValidateForFullCreditNote(
                    order,
                    _orderRepository.GetDictionaryOfOrdersForParticipantsInOrder(order),
                    _orderRepository.GetDictionaryOfOrdersForActivtiesInOrder(order));

                result = _orderInfoFactory.ConvertOrder(CreateCreditNoteFromOrder(order));
            }
            else
            {
                _orderValidator.ValidateForPartialCreditNote(
                    order,
                    _orderRepository.GetDictionaryOfOrdersForParticipantsInOrder(order),
                    _orderRepository.GetDictionaryOfOrdersForActivtiesInOrder(order));

                result = _orderInfoFactory.ConvertOrder(order);
            }

            result = InvoiceByOrderId(result.Id, result.WorkOrder);
            return result;
        }

        public OrderInfo CreateCreditOrderDraft(int id)
        {
            var order = _orderRepository.FindOrder(id);
            if (order == null)
            {
                throw new EntityNotFoundException(string.Format("Could not find order with ID {0}", id));
            }

            if (order.Status != OrderStatus.Invoiced)
            {
                throw new ValidationException("Can only create credit order template from invoiced orders");
            }

            return _orderInfoFactory.ConvertOrder(CreateCreditNoteDraftFromOrder(order));
        }

        public IEnumerable<string> GetWorkOrders(int customerId)
        {
            using (new TimingLogScope(_logger, string.Format("Query for work orders of customer with id {0}", customerId)))
            {
                var customer = _context.Customers.FirstOrDefault(c => c.Id == customerId);
                if (customer == null)
                {
                    throw new EntityNotFoundException(string.Format("Could not find customer with ID {0}", customerId));
                }

                return _ordersService.GetWorkOrders(new WorkOrderRequest
                {
                    ExternalCustomerId = customer.ExternalCustomerId,
                    Provider = _providerResolver.GetInvoicesProviders(customer.DistributorId)
                });
            }
        }

        private Order CreateCreditNoteDraftFromOrder(Order order)
        {
            var creditNoteDraft = _orderFactory.CreateCreditNoteDraftFromOrder(order);

            var creditedOrders = _orderRepository.FindCreditedOrders(order);

            var creditedParticipants = new List<Participant>();
            var creditedActivities = new List<Activity>();

            foreach (var creditedOrder in creditedOrders)
            {
                creditedParticipants.AddRange(creditedOrder.ParticipantOrderLines.Select(pol => pol.Participant));
                creditedActivities.AddRange(creditedOrder.ActivityOrderLines.Select(aol => aol.Activity));
            }

            if (creditNoteDraft.ParticipantOrderLines != null)
                creditNoteDraft.ParticipantOrderLines.RemoveAll(pol => creditedParticipants.Contains(pol.Participant));
            
            if (creditNoteDraft.ActivityOrderLines != null)
                creditNoteDraft.ActivityOrderLines.RemoveAll(aol => creditedActivities.Contains(aol.Activity));

            return _orderRepository.Create(creditNoteDraft);
        }

        private Order CreateCreditNoteFromOrder(Order order)
        {
            var creditNoteOrder = _orderFactory.CreateCreditNoteFromOrder(order);
            return _orderRepository.Create(creditNoteOrder);
        }

        private IEnumerable<OrderLine> GetOrderInfoLines(Order order)
        {
            return _orderInfoFactory.GetOrderInfoLines(order);
        }

        public OrderInfo AddParticipantsToOrder(int orderId, int[] participantIds)
        {
            var order = _orderRepository.FindOrder(orderId);

            var participants = _participantsProvider.GetParticipants(participantIds).ToList();
            ValidateParticipantOrderLinesToAdd(order, participants);
            
            _orderRepository.AddParticipantsToOrder(order, participants);

            return GetOrderInfo(orderId);
        }

        private void ValidateParticipantOrderLinesToAdd(Order order, List<Participant> participants)
        {
            if (order.Status != OrderStatus.Draft)
                throw new ValidationException("Can't create orderlines for orders that have a status different from 'Draft'.");

            if(participants.Any(p=> p.CustomerId !=order.CustomerId))
                throw new ValidationException($"Can only add participants from customer '{order.Customer.Name}' to this order.");

            participants.ForEach(p => ValidateParticipantToAdd(order, p));
        }

        private void ValidateParticipantToAdd(Order order, Participant participant)
        {
            var orderLines = _context.ParticipantOrderLines.Where(pol => pol.Participant.Id == participant.Id && pol.Order.Id != order.Id);

            if (!orderLines.Any())
                return;

            //This statement will not retrieve 'Proposed' orders, so no need to check for that status in the succeeding validations
            var currentOrder = _context.Orders.First(o => o.Id == orderLines.Max(ol => ol.Order.Id));

            if (currentOrder.IsCreditNote || currentOrder.IsFullCreditNote)
            {
                if (new List<OrderStatus> {OrderStatus.Draft, OrderStatus.Rejected}.Contains(currentOrder.Status))
                    throw new ValidationException("This participant is already added to a credit note that is not yet invoiced.");

                return;
            }

            throw new ValidationException("This participant is already added to another order.");
        }
        
        public OrderInfo AddActivitiesToOrder(int orderId, int[] activityIds)
        {
            var order = _orderRepository.FindOrder(orderId);
            var activities = _activityProvider.GetActivities(activityIds);

            _orderRepository.AddActivitiesToOrder(order, activities.ToList());

            return GetOrderInfo(orderId);
        }

        public OrderInfo RemoveOrderlinesFromOrder(int orderId, int[] orderLineIds)
        {
            var order = _orderRepository.FindOrder(orderId);

            if (orderLineIds.Any(id => (order.ParticipantOrderLines.FindAll(l => orderLineIds.Contains(id)).Count + order.ActivityOrderLines.FindAll(l => orderLineIds.Contains(id)).Count) == 0))
            {
                throw new ValidationException("One or more orderlines is not related to the given order.");
            }

            _orderRepository.RemoveOrderLines(orderLineIds);

            return GetOrderInfo(orderId);

        }

        public PartialList<SearchableOrder> Search(string query, int? distributorId, int? coordinatorId, int[] statusFilter, int[] customerTypeFilter, DateTime? fromDate, DateTime? toDate, int skip, int limit, string orderBy, string orderDirection)
        {
            return _orderRepository.Find(query, distributorId, coordinatorId, statusFilter, customerTypeFilter, fromDate, toDate, skip, limit, orderBy, orderDirection);
        }

        public OrderInfo Create(OrderInfo orderInfo)
        {
            var order = MapToOrder(orderInfo);
            CreateExternalCustomerForArbitraryCustomerIfNotCreated(order);

            return _orderInfoFactory.ConvertOrder(_orderRepository.Create(order));
        }

        public OrderInfo Update(int id, OrderInfo orderInfo)
        {
            var order = MapToOrder(orderInfo);
            order = _orderRepository.Update(id, order);

            return _orderInfoFactory.ConvertOrder(order);
        }

        public void Delete(int id)
        {
            _orderRepository.Delete(id);
        }

        public OrderInfo InvoiceByOrderId(int id, string workOrder)
        {
            lock (InvoiceSyncRoot)
            {
                var order = _orderRepository.FindOrder(id);



                CreateExternalCustomerForArbitraryCustomerIfNotCreated(order);

                if (!string.IsNullOrWhiteSpace(workOrder))
                {
                    order.WorkOrder = workOrder;
                }

                var invoicedOrder = InvoiceOrder(order);
                var orderInfo = _orderInfoFactory.ConvertOrder(invoicedOrder);
                
                return orderInfo;
            }
        }
        
        private void CreateExternalCustomerForArbitraryCustomerIfNotCreated(Order order)
        {
			
			var customer = _context.Customers.Include(c => c.Addresses).FirstOrDefault(c => c.Id == order.CustomerId);

            if (customer == null)
                throw new ValidationException("Invalid customerId when trying to create external customer.");

            var externalCustomerProvider = _externalCustomersProviderFactory.GetExternalCustomerProvider(customer.DistributorId);

            if (externalCustomerProvider.Validate(customer))
            {
                var createRequest = new ExternalCustomerCreateRequest
                {
                    Name = customer.Name,
                    CompanyRegistrationNumber = customer.CompanyRegistrationNumber,
                    AddressStreet1 = customer.InvoicingAddress.AddressStreet1,
                    City = customer.InvoicingAddress.City,
                    Country = customer.InvoicingAddress.Country,
                    ZipCode = customer.InvoicingAddress.ZipCode,					
				};
                var externalCustomerId = externalCustomerProvider.Create(createRequest);

                customer.ExternalCustomerId = externalCustomerId;
                _context.SaveChanges();
            }
        }

        public OrderInfo InvoiceByGroupByKey(string groupByKey, string workOrder = null)
        {
            Order created = null;
            OrderInfo invoiced = null;

            lock (InvoiceSyncRoot)
            {
                    var order = _orderRepository.FindProposedOrder(groupByKey);
                    if (!string.IsNullOrEmpty(workOrder))
                    {
                        order.WorkOrder = workOrder;
                    }

                try
                {
                    created = _orderRepository.Create(order);
                    invoiced = InvoiceByOrderId(created.Id, order.WorkOrder);
                }
                catch
                {
                    if(created != null)
                        _orderRepository.Delete(created.Id);

                    throw;
                }

                return invoiced;
                }    
            }

        private Order InvoiceOrder(Order order)
        {
            Order creditedOrder = null;
            string orderNumber = string.Empty;

            var customer = _customersProvider.GetCustomer(order.CustomerId);

            if (order.Status != OrderStatus.Draft)
            {
                throw new ValidationException("Order has already been invoiced");
            }

            if (!_providerResolver.HasInvoiceProviders(customer.DistributorId))
            {
                throw new ValidationException("Distributor for customer has no invoice provider");
            }

            VerifySingleCurrency(order);

            if (string.IsNullOrEmpty(order.WorkOrder))
            {
                var workOrders = _ordersService.GetWorkOrders(new WorkOrderRequest
                {
                    ExternalCustomerId = customer.ExternalCustomerId,
                    Provider = _providerResolver.GetInvoicesProviders(customer.DistributorId)
                }).ToList();

                if (workOrders.Count > 1)
                {
                    throw new ValidationException(
                        "More than one work order exists for customer. Work order must be specified.");
                }
                order.WorkOrder = workOrders.FirstOrDefault();
            }

            if (order.CreditedOrderId.HasValue)
            {
                creditedOrder = _orderRepository.FindOrder(order.CreditedOrderId.Value);
            }

            var orderRequest = new OrderRequest
            {
                ExternalCustomerId = customer.ExternalCustomerId,
                Order = order,
                CreditedOrder = creditedOrder,
                Provider = _providerResolver.GetInvoicesProviders(customer.DistributorId)
            };


            using (var transaction = new TransactionScope())
            {
                if(!(order.IsCreditNote || order.IsFullCreditNote))
                {
                    PersistOrderLines(orderRequest.Order);
                }

                orderRequest = _ordersService.PrepareOrderRequest(orderRequest);

                _orderRepository.Update(orderRequest.Order.Id, orderRequest.Order);
                order = _ordersService.Create(orderRequest);

                transaction.Complete();
            }

            try
            {
                return _orderRepository.Update(order.Id, order, true);    //Deliberately placing this outside of the transaction scope in order to mark the order transferred even though no ordernumber is attached (should hardly happen, but you never know).
            }
            catch (Exception ex)
            {
                var message = string.Format("Order successfully transferred with Order Number '{0}', but an error occured when updating the order in Phoenix. Please contact technical support.", orderNumber);
                throw new ValidationException(message);
            }
        }

        /// <summary>
        /// Persists orderlines so we have a snapshot of the orderdetails from when the order is sent to invoicing.
        /// </summary>
        /// <param name="order">The order to be invoiced.</param>
        private void PersistOrderLines(Order order)
        {
            if (IsLocked(order))
            {
                throw new ValidationException("Could not update order lines. The order is in a state where altering order lines is not permitted!");
            }

            lock (order)
            {
                PersistParticipantOrderLines(order);
                PersistActivityOrderLines(order);                
            }

        }

        private void PersistParticipantOrderLines(Order order)
        {
            var lineIds = order.ParticipantOrderLines.Select(p => p.Id).ToList();
            var lines = _context.ParticipantOrderLines.Include(p => p.Participant).Where(pol => lineIds.Contains(pol.Id));
            var activityIds = order.ParticipantOrderLines.Select(p => p.ActivityId).Distinct().ToList();

            var activitiesHashTable = new Hashtable();
            foreach (var activityId in activityIds)
            {
                activitiesHashTable.Add(activityId, _activityProvider.Get(activityId, includeParticipantsWithOrderInformation: false).Name);
            }

            foreach (var line in lines)
            {
                var participant = line.Participant;

                line.ActivityId = participant.ActivityId;
                line.ActivityName = (string)activitiesHashTable[participant.ActivityId];
                line.CurrencyCodeId = participant.CurrencyCodeId;
                line.InfoText = participant.GetInfoTexts();
                line.OnlinePaymentReference = participant.OnlinePaymentReference;
                line.Price = participant.UnitPrice;
            }
        }

        private void PersistActivityOrderLines(Order order)
        {
            var lineIds = order.ActivityOrderLines.Select(a => a.Id).ToList();
            var lines = _context.ActivityOrderLines.Include(a => a.Activity).Where(aol => lineIds.Contains(aol.Id)).ToList();

            foreach (var line in lines)
            {
                var activity = line.Activity;

                line.ActivityName = _activityProvider.Get(activity.Id).Name;
                line.CurrencyCodeId = activity.CurrencyCodeId;
                line.Price = activity.FixedPrice;
            }
        }

        private void VerifySingleCurrency(Order order)
        {
            var currencyCodes = order.ParticipantOrderLines.Where(p => p.Price.HasValue && p.Price > 0).Select(p => p.CurrencyCodeId).ToList();
            currencyCodes.AddRange(order.ActivityOrderLines.Where(a => a.Price.HasValue && a.Price.Value > 0).Select(a => a.CurrencyCodeId));

            if (currencyCodes.Where(c => c.HasValue).Select(c => c.Value).Distinct().Count() > 1)
            {
                throw new ValidationException("Order can only contain a single currency");
            }
        }

        private Order MapToOrder(OrderInfo orderInfo)
        {
            var order = Mapper.Map<OrderInfo, Order>(orderInfo);
            order.ParticipantOrderLines = new List<ParticipantOrderLine>();
            order.ActivityOrderLines = new List<ActivityOrderLine>();

            //var participants = _context.Participants.Include(p => p.User).Include(p => p.ParticipantInfoElements).Where(p => orderInfo.Participants.Contains(p.Id));
            var participants = _context.Participants.Where(p => orderInfo.Participants.Contains(p.Id)).ToList();
            var activities = _context.Activities.Where(a => orderInfo.FixedPriceActivities.Contains(a.Id)).ToList();

            foreach (var participant in participants)
            {
                order.ParticipantOrderLines.Add(_orderRepository.CreateParticipantOrderLine(order, participant));
            }

            foreach (var activity in activities)
            {
                order.ActivityOrderLines.Add(_orderRepository.CreateActivityOrderLine(order, activity));
            }

            return order;
        }


        public bool IsLocked(Order order)
        {
            return (order.Status == OrderStatus.Transferred || 
                    order.Status == OrderStatus.NotInvoiced ||
                    order.Status == OrderStatus.Invoiced);
        }

    }
}