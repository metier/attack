using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Transactions;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Api.Facade.ExamContent;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.Extensions;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Providers
{
    public class QuestionsProvider : IQuestionsProvider
    {
        private readonly MetierLmsContext _context;
        private readonly IAccountsProvider _accountsProvider;
        private readonly IValidate<Question> _questionValidator;
        
        public QuestionsProvider(MetierLmsContext context, 
                                 IAccountsProvider accountsProvider,
                                 IValidate<Question> questionValidator)
        {
            _context = context;
            _accountsProvider = accountsProvider;
            _questionValidator = questionValidator;
        }

        public Question Get(int id)
        {
            return _context.Questions
                           .Include(q => q.QuestionOptions)
                           .Include(q => q.Tags)
                           .Include(q => q.Products)
                           .Include(q => q.Rcos)
                           .FirstOrDefault(q => q.Id == id); 
        }

        public Question Create(Question question)
        {
            var validationResults = _questionValidator.Validate(question).ToList();
            if (validationResults.Any())
            {
                throw new ValidationException("Invalid question", validationResults);
            }

            CleanupOptions(question);

            question.ReplaceEntitiesFromContext(a => a.Tags, _context.Tags);
            question.ReplaceEntitiesFromContext(a => a.Products, _context.Products);
            question.ReplaceEntitiesFromContext(a => a.Rcos, _context.Rcos);

            var currentUser = _accountsProvider.GetCurrentUser();
            question.Created = DateTime.UtcNow;
            question.CreatedById = currentUser.User.Id;

            _context.Questions.Add(question);
            _context.SaveChanges();

            return question;
        }

        public List<Question> BatchCreate(List<Question> questions)
        {
            using (var transaction = new TransactionScope())
            {
                var createdQuestions = questions.Select(Create).ToList();
                transaction.Complete();
                return createdQuestions;
            }
        }

        public List<Question> BatchUpdate(QuestionsChangeSet changeSet)
        {
            var questions = new List<Question>();
            var productsToAdd = _context.Products.AsQueryable().Where(p => changeSet.ProductIdsToAdd.Contains(p.Id)).ToList();
            var productsToRemove = _context.Products.AsQueryable().Where(p => changeSet.ProductIdsToRemove.Contains(p.Id)).ToList();
            var rcosToAdd = _context.Rcos.AsQueryable().Where(r => changeSet.VersionIdsToAdd.Contains(r.Id)).ToList();
            var rcosToRemove = _context.Rcos.AsQueryable().Where(r => changeSet.VersionIdsToRemove.Contains(r.Id)).ToList();
            var tagsToAdd = _context.Tags.AsQueryable().Where(t => changeSet.TagIdsToAdd.Contains(t.Id)).ToList();
            var tagsToRemove = _context.Tags.AsQueryable().Where(t => changeSet.TagIdsToRemove.Contains(t.Id)).ToList();

            foreach (var questionId in changeSet.QuestionIds)
            {
                var question =  _context.Questions
                           .Include(q => q.QuestionOptions)
                           .Include(q => q.Tags)
                           .Include(q => q.Products)
                           .Include(q => q.Rcos).AsNoTracking()         //Can't use the Get() -method because 'AsNoTracking()' must be specified to make things work.
                           .FirstOrDefault(q => q.Id == questionId); 

                question = ApplyChangesToQuestion(question, changeSet, productsToAdd, productsToRemove, rcosToAdd, rcosToRemove, tagsToAdd, tagsToRemove);
                questions.Add(question);
            }

            return BatchUpdate(questions);
        }

        private Question ApplyChangesToQuestion(Question question, QuestionsChangeSet changeSet, List<Product> productsToAdd, List<Product> productsToRemove, List<Rco> rcosToAdd, List<Rco> rcosToRemove, List<Tag> tagsToAdd, List<Tag> tagsToRemove)
        {

            if (changeSet.DifficultyId > 0) {
                question.Difficulty = (QuestionDifficulties)changeSet.DifficultyId;
            }

            if (changeSet.StatusId > 0) {
                question.Status = (QuestionStatuses)changeSet.StatusId;
            }

            if (changeSet.LanguageId > 0) {
                question.LanguageId = changeSet.LanguageId;
            }

            if (changeSet.IsChangeCustomerTailoredSetting) {
                question.CustomerId = changeSet.CustomerId;
            }

            if (changeSet.Points.HasValue) {
                question.Points = changeSet.Points.Value;
            }

            if (!string.IsNullOrEmpty(changeSet.Author)) {
                question.Author = changeSet.Author;
            }

            foreach (var productToAdd in productsToAdd.Where(productToAdd => !question.Products.Exists(p => p.Id == productToAdd.Id)))
            {
                question.Products.Add(productToAdd);
            }
            //question.Products.AddRange(productsToAdd.Select(p => !question.Products.Exists(p2 => p2.Id == p.Id)).ToList());       //This might work instead of the above...
            question.Products.RemoveAll(p => productsToRemove.Exists(p2 => p2.Id == p.Id));

            foreach (var rcoToAdd in rcosToAdd.Where(rcoToAdd => !question.Rcos.Exists(r=> r.Id == rcoToAdd.Id)))
            {
                question.Rcos.Add(rcoToAdd);
            }

            question.Rcos.RemoveAll(r => rcosToRemove.Exists(r2 => r2.Id == r.Id));

            foreach (var tagToAdd in tagsToAdd.Where(tagToAdd => !question.Tags.Exists(t => t.Id == tagToAdd.Id)))
            {
                question.Tags.Add(tagToAdd);
            }

            question.Tags.RemoveAll(t => tagsToRemove.Exists(t2 => t2.Id == t.Id));

            return question;
        }

        public List<Question> BatchUpdate(List<Question> questions)
        {
            using (var transaction = new TransactionScope())
            {
                var updatedQuestions = questions.Select(q => Update(q.Id, q)).ToList();
                transaction.Complete();
                return updatedQuestions;
            }
        }

        public Question Update(int id, Question question)
        {
            var validationResults = _questionValidator.Validate(question).ToList();
            if (validationResults.Any())
            {
                throw new ValidationException("Invalid question", validationResults);
            }
            var dbQuestion = Get(id);
            if (!question.RowVersion.SequenceEqual(dbQuestion.RowVersion))
            {
                throw new DbUpdateConcurrencyException("There is a newer record of the question in the database.");
            }

            if (dbQuestion.Status == QuestionStatuses.Obsolete)
            {
                throw new ValidationException("Cannot update obsolete questions", validationResults);
            }
            if (dbQuestion.Status == QuestionStatuses.Published)
            {
                return CreateNewVersion(question, dbQuestion);
            }
            
            CleanupOptions(question);
            
            var currentUser = _accountsProvider.GetCurrentUser();
            question.Modified = DateTime.UtcNow;
            question.ModifiedById = currentUser.User.Id;

            _context.Entry(dbQuestion).CurrentValues.SetValues(question);

            question.UpdateRelatedEntities(dbQuestion, q => q.QuestionOptions, _context);

            question.UpdateExistingRelatedEntities(dbQuestion, q => q.Tags, _context.Tags);
            question.UpdateExistingRelatedEntities(dbQuestion, q => q.Products, _context.Products);
            question.UpdateExistingRelatedEntities(dbQuestion, q => q.Rcos, _context.Rcos);

            _context.SaveChanges();

            return Get(id);
        }

        private Question CreateNewVersion(Question postedQuestion, Question dbQuestion)
        {
            using (var transaction = new TransactionScope())
            {
                dbQuestion.Status = QuestionStatuses.Obsolete;
                postedQuestion.BasedOnQuestionId = dbQuestion.Id;
                postedQuestion.Modified = null;
                postedQuestion.ModifiedById = null;
                postedQuestion.LastUsed = null;
                postedQuestion.Id = 0;
                var newVersion = Create(postedQuestion);
                transaction.Complete();
                return newVersion;
            }
        }

        private static void CleanupOptions(Question question)
        {
            if (question.Type != QuestionTypes.TrueFalse)
            {
                question.CorrectBoolAnswer = null;
            }
            if (question.Type != QuestionTypes.MultipleChoiceMultiple && question.Type != QuestionTypes.MultipleChoiceSingle)
            {
                question.QuestionOptions.Clear();
            }
        }

        public IEnumerable<Question> GetPreviousVersions(int questionId)
        {
            var list = new List<Question>();
            var previous = GetPreviousVersion(questionId);
            if (previous != null)
            {
                list.Add(previous);
                list.AddRange(GetPreviousVersions(previous.Id));
            }
            return list;
        }
        
        public Question GetPreviousVersion(int questionId)
        {
            return _context.Questions.Join(_context.Questions, current => current.BasedOnQuestionId, previous => previous.Id, (current, previous) => new { current, previous })
                                               .Where(x => x.current.Id == questionId)
                                               .Select(x => x.previous)
                                               .FirstOrDefault();
        }

        public Question GetNewestVersion(int questionId)
        {
            return GetNewerVersions(questionId).LastOrDefault();
        }

        public IEnumerable<Question> GetNewerVersions(int questionId)
        {
            var list = new List<Question>();
            var newerVersion = GetNewerVersion(questionId);
            if (newerVersion != null)
            {
                list.Add(newerVersion);
                list.AddRange(GetNewerVersions(newerVersion.Id));
            }
            return list;
        }

        private Question GetNewerVersion(int questionId)
        {
            return _context.Questions.Join(_context.Questions, current => current.Id, newer => newer.BasedOnQuestionId, (current, newer) => new { current, newer })
                                               .Where(x => x.current.Id == questionId)
                                               .Select(x => x.newer)
                                               .FirstOrDefault();
        }

        public PartialList<SearchableQuestion> Search(string query, int[] tagIds, int[] productIds, int[] rcoIds, QuestionDifficulties[] difficulties, QuestionStatuses[] statuses, int[] languageIds, int customerSpecificFilterValue, int[] customerIds, DateTime? lastUsedFromDate, DateTime? lastUsedToDate, int skip, int limit, string orderBy, string orderDirection)
        {
            
            var queryable = _context.Questions
                .Join(_context.SearchableQuestions, q => q.Id, sq => sq.Id, (question, searchableQuestion) => new
                {
                    question.Tags,
                    question.Products,
                    question.Rcos,
                    SearchableQuestion = searchableQuestion
                });
            if (tagIds.Any())
            {
                queryable = queryable.Where(x => x.Tags.Any(t => tagIds.Contains(t.Id)));
            }
            if (productIds.Any())
            {
                queryable = queryable.Where(x => x.Products.Any(p => productIds.Contains(p.Id)));
            }
            if (rcoIds.Any())
            {
                queryable = queryable.Where(x => x.Rcos.Any(r => rcoIds.Contains(r.Id)));
            }
            if (difficulties.Any())
            {
                queryable = queryable.Where(x => difficulties.Contains(x.SearchableQuestion.Difficulty));
            }
            if (statuses.Any())
            {
                queryable = queryable.Where(x => statuses.Contains(x.SearchableQuestion.Status));
            }
            if (languageIds.Any())
            {
                queryable = queryable.Where(x => languageIds.Contains(x.SearchableQuestion.LanguageId));
            }
            if (customerSpecificFilterValue.Equals(1))          //Show only generic questions
            {
                queryable = queryable.Where(x => !x.SearchableQuestion.CustomerId.HasValue);
            }
            if (customerSpecificFilterValue.Equals(2))          //Show only custom questions
            {
                queryable = queryable.Where(x => x.SearchableQuestion.CustomerId.HasValue);

                if (customerIds.Any())
                    queryable = queryable.Where(x => customerIds.Contains(x.SearchableQuestion.CustomerId.Value));
            }
            if (lastUsedFromDate.HasValue)
            {
                var dateLowerBound = lastUsedFromDate.Value.Date;
                queryable = queryable.Where(x => x.SearchableQuestion.LastUsed >= dateLowerBound);
            }
            if (lastUsedToDate.HasValue)
            {
                var dateUpperBound = lastUsedToDate.Value.Date + TimeSpan.FromDays(1);
                queryable = queryable.Where(x => x.SearchableQuestion.LastUsed <= dateUpperBound);
            }
            if (!string.IsNullOrEmpty(query))
            {
                queryable = queryable.Where(x => x.SearchableQuestion.Text.ToLower().Contains(query.ToLower()));
            }
            
            var totalCount = queryable.Count();

            orderBy = orderBy == null ? "" : orderBy.ToLower();
            orderDirection = orderDirection == null ? "" : orderDirection.ToLower();

            if (orderBy == "id")
            {
                queryable = orderDirection == "desc" ? queryable.OrderByDescending(x => x.SearchableQuestion.Id) : queryable.OrderBy(x => x.SearchableQuestion.Id);
            }
            if (orderBy == "difficulty")
            {
                queryable = orderDirection == "desc" ? queryable.OrderByDescending(x => x.SearchableQuestion.Difficulty) : queryable.OrderBy(x => x.SearchableQuestion.Difficulty);
            }
            if (orderBy == "status")
            {
                queryable = orderDirection == "desc" ? queryable.OrderByDescending(x => x.SearchableQuestion.Status) : queryable.OrderBy(x => x.SearchableQuestion.Status);
            }
            else
            {
                queryable = orderDirection == "desc" ? queryable.OrderByDescending(x => x.SearchableQuestion.Id) : queryable.OrderBy(x => x.SearchableQuestion.Id);
            }

            queryable = queryable.Skip(skip);
            if (limit > 0)
            {
                queryable = queryable.Take(limit);
            }
            var result = queryable.ToList().Select(x =>
            {
                x.SearchableQuestion.Products = x.Products;
                x.SearchableQuestion.Rcos = x.Rcos;
                x.SearchableQuestion.Tags = x.Tags;
                return x.SearchableQuestion;
            });
            return result.ToPartialList(totalCount, orderBy);
        }
    }
}