﻿using System;
using System.Collections.Generic;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Providers
{
    public interface IOrdersProvider
    {
        OrderInfo GetOrderInfo(int id);
        OrderInfo GetProposedOrderInfo(string groupByKey);
        OrderInfo Create(OrderInfo order);
        PartialList<SearchableOrder> Search(string query, int? distributorId, int? coordinatorId, int[] statusFilter, int[] customerTypeFilter, DateTime? fromDate, DateTime? toDate, int skip, int limit, string orderBy, string orderDirection);
        OrderInfo Update(int id, OrderInfo order);

        OrderInfo InvoiceByOrderId(int id, string workOrder = null);
        OrderInfo InvoiceByGroupByKey(string groupByKey, string workOrder = null);
        
        void Delete(int id);
        IEnumerable<OrderLine> GetOrderInfoLinesByOrderId(int id);

        OrderInfo ResetOrder(int id);
        OrderInfo CreditOrder(int id);
        OrderInfo CreateCreditOrderDraft(int id);
        IEnumerable<string> GetWorkOrders(int customerId);

        OrderInfo AddParticipantsToOrder(int orderId, int[] participantIds);
        OrderInfo AddActivitiesToOrder(int id, int[] activityIds);
        OrderInfo RemoveOrderlinesFromOrder(int id, int[] orderLineIds);

    }
}