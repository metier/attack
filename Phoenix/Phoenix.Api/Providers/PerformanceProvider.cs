using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Factories;
using Metier.Phoenix.Api.Messaging;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Queryables;

namespace Metier.Phoenix.Api.Providers
{
    public class PerformanceProvider : IPerformanceProvider
    {
        private readonly MetierLmsContext _context;
        private readonly IRcoProvider _rcoProvider;
        private readonly IEventAggregator _eventAggregator;
        private readonly IPerformanceSummaryFactory _performanceSummaryFactory;

        public PerformanceProvider(MetierLmsContext context, IRcoProvider rcoProvider, IEventAggregator eventAggregator, IPerformanceSummaryFactory performanceSummaryFactory)
        {
            _context = context;
            _rcoProvider = rcoProvider;
            _eventAggregator = eventAggregator;
            _performanceSummaryFactory = performanceSummaryFactory;
        }

        public IEnumerable<PerformanceInfo> GetPerformancesByUserId(int userId)
        {
            if (userId == 0)
            {
                throw new ArgumentException("User id must be set");
            }

            var rcoIds = _context.ScormPerformances.Where(p => p.UserId == userId).Select(p => p.RcoId).ToList();

            var courseRcoIds = _context.Rcos.Where(r => rcoIds.Contains(r.Id) && r.Context == "course").Select(r => r.Id).ToList();
            var coursePerformanceInfos = courseRcoIds.Select(id => new PerformanceInfo
            {
                UserId = userId, 
                Course = _rcoProvider.GetCourse(id), 
                Performances = GetPerformancesForAllLessonsIncludingCourse(userId, id).ToList()
            });

            var examContainerRcoIds = _context.Rcos.Where(r => rcoIds.Contains(r.Id) && r.Context == "examcontainer").Select(r => r.Id).ToList();
            var examContainerPerformanceInfos = examContainerRcoIds.Select(id => new PerformanceInfo
            {
                UserId = userId,
                ExamContainer = _rcoProvider.GetExamContainer(id),
                Performances = GetPerformancesForAllLessonsIncludingCourse(userId, id).ToList()
            });
            return coursePerformanceInfos.Concat(examContainerPerformanceInfos).ToList();
        }

        public PerformanceInfo GetPerformancesByActivity(int userId, int activityId)
        {
            if (userId == 0)
            {
                throw new ArgumentException("User id must be set");
            }
            if (activityId == 0)
            {
                throw new ArgumentException("Activity id must be set");
            }
            var coursePerformance = new PerformanceInfo { UserId = userId };
            var activity = _context.Activities.FirstOrDefault(a => a.Id == activityId && !a.IsDeleted);
            if (activity != null && activity.RcoCourseId.HasValue)
            {
                coursePerformance.Course = _rcoProvider.GetCourse(activity.RcoCourseId.Value);
                coursePerformance.Performances = GetPerformancesForAllLessonsIncludingCourse(userId, activity.RcoCourseId.Value).ToList();
                return coursePerformance;
            }
            if (activity != null && activity.RcoExamContainerId.HasValue)
            {
                coursePerformance.ExamContainer = _rcoProvider.GetExamContainer(activity.RcoExamContainerId.Value);
                coursePerformance.Performances = GetPerformancesForAllExamsIncludingContainer(userId, activity.RcoExamContainerId.Value).ToList();
                return coursePerformance;
            }
            return coursePerformance;
        }

        public PerformanceInfo GetPerformancesByArticle(int userId, int articleId)
        {
            if (userId == 0)
            {
                throw new ArgumentException("User id must be set");
            }
            if (articleId == 0)
            {
                throw new ArgumentException("Article id must be set");
            }

            var performanceInfo = new PerformanceInfo { UserId = userId };

            var elearningArticle = _context.Articles.FirstOrDefault(a => a.Id == articleId) as ElearningArticle;
            if (elearningArticle != null && elearningArticle.RcoCourseId.HasValue)
            {
                performanceInfo.Course = _rcoProvider.GetCourse(elearningArticle.RcoCourseId.Value);
                performanceInfo.Performances = GetPerformancesForAllLessonsIncludingCourse(userId, elearningArticle.RcoCourseId.Value).ToList();
                return performanceInfo;
            }

            var mockexamArticle = _context.Articles.FirstOrDefault(a => a.Id == articleId) as MockexamArticle;
            if (mockexamArticle != null && mockexamArticle.RcoExamContainerId.HasValue)
            {
                performanceInfo.ExamContainer = _rcoProvider.GetExamContainer(mockexamArticle.RcoExamContainerId.Value);
                performanceInfo.Performances = GetPerformancesForAllExamsIncludingContainer(userId, mockexamArticle.RcoExamContainerId.Value).ToList();
                return performanceInfo;
            }
            
            return performanceInfo;
        }

        public ScormPerformance RegisterPerformance(int userId, int rcoId, ScormPerformanceRegisterRequest request)
        {
            if (rcoId == 0)
            {
                throw new ArgumentException("RCO id must be set");
            }

            var user = _context.Users.FirstOrDefault(u => u.Id == userId);
            if (user == null)
            {
                throw new EntityNotFoundException("User does not exist");
            }

            CreateParentPerformanceIfNotExists(userId, rcoId);

            var performance = _context.ScormPerformances.FirstOrDefault(p => p.UserId == userId && p.RcoId == rcoId);
            if (performance == null)
            {
                performance = new ScormPerformance
                {
                    UserId = userId,
                    RcoId = rcoId,
                    Status = ScormLessonStatus.NotAttempted,
                    OriginalStatus = ScormLessonStatus.NotAttempted
                };
                _context.ScormPerformances.Add(performance);
            }
            var rco = _context.Rcos.First(r => r.Id == rcoId);
            //JVT 2018-03-02: This prevents status changes from Complete-> other status. Commented out
            //var isCompletedFinalTest = rco.IsFinalTest.GetValueOrDefault() &&
            //                           performance.Status == ScormLessonStatus.Complete;
            var isStatusChanged = performance.Status != request.Status;

            performance.CompletedDate = request.CompletedDate.HasValue ? request.CompletedDate.Value : (DateTime?) null;
            performance.TimeUsedInSeconds = request.TimeUsedInSeconds.GetValueOrDefault();
            performance.Score = request.Score.GetValueOrDefault();
            performance.Status = request.Status;
			performance.OriginalStatus = request.Status;

			using (var transaction = new TransactionScope())
            {
                _context.SaveChanges();

                if (isStatusChanged)// && !isCompletedFinalTest)
                {
                    _eventAggregator.Publish(new PerformanceStatusChangedEvent {PerformanceId = performance.Id});
                }

                transaction.Complete();
                return performance;
            }
        }

        public void CreateParentPerformanceIfNotExists(int userId, int relatedRcoId)
        {
            var parent = _rcoProvider.GetTopParent(relatedRcoId);
            if (parent.Id == relatedRcoId)
            {
                // No parent found
                return;
            }

            var parentPerformance = _context.ScormPerformances.FirstOrDefault(p => p.UserId == userId && p.RcoId == parent.Id);
            if (parentPerformance == null)
            {
                parentPerformance = new ScormPerformance
                {
                    UserId = userId,
                    RcoId = parent.Id,
                    Status = ScormLessonStatus.NotAttempted,
                    OriginalStatus = ScormLessonStatus.NotAttempted
                };
                _context.ScormPerformances.Add(parentPerformance);
                using (var transaction = new TransactionScope())
                {
                    _context.SaveChanges();
                    _eventAggregator.Publish(new PerformanceStatusChangedEvent {PerformanceId = parentPerformance.Id});
                    transaction.Complete();
                }
            }
        }

        public ScormPerformance GetParentPerformance(int userId, int relatedRcoId)
        {
            var course = _rcoProvider.GetTopParent(relatedRcoId);
            return _context.ScormPerformances.FirstOrDefault(p => p.UserId == userId && p.RcoId == course.Id);
        }

        public ScormPerformance GetPerformance(int userId, int rcoId)
        {
            return _context.ScormPerformances.FirstOrDefault(p => p.UserId == userId && p.RcoId == rcoId);
        }

        public ScormPerformance Create(ScormPerformance performance)
        {
            _context.ScormPerformances.Add(performance);
            _context.SaveChanges();
            return performance;
        }

        public List<PerformanceSummary> GetPerformanceSummaryByCustomer(int id)
        {
            var performanceSummaries = new List<PerformanceSummary>();
            var users = _context.Users.WithAttachment().Where(u => u.CustomerId == id);
            foreach (var user in users)
            {
                var coursePerformances = GetPerformancesByUserId(user.Id);
                performanceSummaries.Add(_performanceSummaryFactory.Create(user, coursePerformances));
            }
            return performanceSummaries.OrderByDescending(p => p.TotalScore).ToList();
        }

        public List<PerformanceSummary> GetPerformanceSummaryByActivity(int id)
        {
            var performanceSummaries = new List<PerformanceSummary>();

            var activity = _context.Activities.FirstOrDefault(a => a.Id == id && !a.IsDeleted);
            if (activity != null)
            {
                activity.IncludeParticipants(_context);
                var userIds = activity.Participants.Select(p => p.UserId).ToList();
                var users = _context.Users.WithAttachment().Where(u => userIds.Contains(u.Id));
                foreach (var user in users)
                {
                    var coursePerformance = GetPerformancesByActivity(user.Id, id);
                    var performanceSummary = _performanceSummaryFactory.Create(user, new List<PerformanceInfo>{coursePerformance});
                    performanceSummaries.Add(performanceSummary);
                }
            }
            return performanceSummaries.OrderByDescending(p => p.TotalScore).ToList();
        }

        public IEnumerable<ScormPerformance> GetPerformancesForAllLessons(int userId, int lessonRcoId)
        {
            var rcos = _rcoProvider.GetCourseRcos(lessonRcoId, false);
            return rcos.Select(rco => GetPerformance(userId, rco.Id) ?? GetNewPerformance(userId, rco.Id)).ToList();
        }

        private IEnumerable<ScormPerformance> GetPerformancesForAllLessonsIncludingCourse(int userId, int rcoId)
        {
            var rcos = _rcoProvider.GetCourseRcos(rcoId, true);
            return rcos.Select(rco => GetPerformance(userId, rco.Id) ?? GetNewPerformance(userId, rco.Id)).ToList();
        }

        public IEnumerable<ScormPerformance> GetPerformancesForAllExams(int userId, int rcoId)
        {
            var rcos = _rcoProvider.GetExamContainerRcos(rcoId, false);
            return rcos.Select(rco => GetPerformance(userId, rco.Id) ?? GetNewPerformance(userId, rco.Id)).ToList();
        }

        private IEnumerable<ScormPerformance> GetPerformancesForAllExamsIncludingContainer(int userId, int rcoId)
        {
            var rcos = _rcoProvider.GetExamContainerRcos(rcoId, true);
            return rcos.Select(rco => GetPerformance(userId, rco.Id) ?? GetNewPerformance(userId, rco.Id)).ToList();
        }

        private ScormPerformance GetNewPerformance(int userId, int rcoId)
        {
            return new ScormPerformance
            {
                RcoId = rcoId,
                UserId = userId,
                Status = ScormLessonStatus.NotAttempted,
                OriginalStatus = ScormLessonStatus.NotAttempted,
            };
        }
    }
}