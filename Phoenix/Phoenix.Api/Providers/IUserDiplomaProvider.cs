﻿using Metier.Phoenix.Core.Data.Entities;
using System.Collections.Generic;

namespace Metier.Phoenix.Api.Providers
{
    public interface IUserDiplomaProvider
    {
        UserDiploma Get(int userId);
        IEnumerable<UserDiploma> GetByUserId(int userId);
        UserDiploma Update(int userId, UserDiploma userDiploma);
        UserDiploma Create(int userId, UserDiploma userDiploma);
        bool Remove(int diplomaId);
    }
}