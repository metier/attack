﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Api.Messaging;
using System.Net.Mail;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Extensions;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Templates.MissingExamCompetency;

namespace Metier.Phoenix.Api.Providers
{
    public class UserDiplomaProvider : IUserDiplomaProvider
    {
        private readonly MetierLmsContext _context;
        private readonly IAutoMailer _autoMailer;        
        private readonly IMissingExamCompetencyMessageFactory _missingExamCompetencyMessageFactory;
        private readonly IEventAggregator _eventAggregator;

        public UserDiplomaProvider(MetierLmsContext context, IAutoMailer autoMailer, IMissingExamCompetencyMessageFactory missingExamCompetencyMessageFactory, IEventAggregator eventAggregator)
        {
            _context = context;
            _autoMailer = autoMailer;            
            _missingExamCompetencyMessageFactory = missingExamCompetencyMessageFactory;
            _eventAggregator = eventAggregator;
        }

        public UserDiploma Get(int userDiplomaId)
        {
            return _context.UserDiplomas.FirstOrDefault(u => u.Id == userDiplomaId);
        }

        public IEnumerable<UserDiploma> GetByUserId(int userId)
        {
            return _context.UserDiplomas.Where(u => u.UserId == userId);
        }

        public UserDiploma Update(int userId, UserDiploma userDiploma)
        {
            if (userId <= 0)
            {
                throw new ValidationException("UserId must be provided");
            }
            if (userDiploma.Id <= 0)
            {
                throw new ValidationException("Id must be provided");
            }
            if (string.IsNullOrEmpty(userDiploma.Title))
            {
                throw new ValidationException("Title must be provided");
            }
            var dbUserDiploma = Get(userDiploma.Id);
            if (!userDiploma.RowVersion.SequenceEqual(dbUserDiploma.RowVersion))
            {
                throw new DbUpdateConcurrencyException("There is a newer record of the UserDiploma in the database.");
            }
            dbUserDiploma.Title = userDiploma.Title;
            _context.SaveChanges();
            return Get(userDiploma.Id);
        }

        public UserDiploma Create(int userId, UserDiploma userDiploma)
        {
            if (userId <= 0)
            {
                throw new ValidationException("UserId must be provided");
            }

            if (string.IsNullOrEmpty(userDiploma.Title))
            {
                throw new ValidationException("Diploma title must be provided");
            }
            userDiploma.UserId = userId;
            userDiploma.Created = DateTime.UtcNow;
            if(userDiploma.ShareGuid == Guid.Empty)
                userDiploma.ShareGuid = Guid.NewGuid();
            var created = _context.UserDiplomas.Add(userDiploma);
            _context.SaveChanges();

            _eventAggregator.Publish(new UserDiplomasEvent { UserId = userDiploma.UserId, DiplomaId = userDiploma.Id   });
            return created;// Get(created.Id);
        }

        public bool Remove(int diplomaId)
        {
            var existingDiploma = Get(diplomaId);
            if(existingDiploma == null)
            {
                throw new ValidationException("Diploma does not exist.");
            }
            _context.UserDiplomas.Remove(existingDiploma);
            var changes = _context.SaveChanges();
            return changes > 0 ? true : false;
        }
    }
}