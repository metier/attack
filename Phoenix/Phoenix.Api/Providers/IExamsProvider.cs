﻿using Metier.Phoenix.Api.Facade.ExamPlayer;

namespace Metier.Phoenix.Api.Providers
{
    public interface IExamsProvider
    {
        ExamInfo GetExamInfo(int activityId);
        Exam StartExam(int activityId);
        Core.Data.Entities.Exam GetExamResult(int participantId);
        Exam GetExam(int examId);
        ExamStatus GetExamStatus(int examId);
        void SubmitAnswer(int examId, Answer answer);
        ExamResult SubmitExam(int examId);
        Core.Data.Entities.Exam UnsubmitExam(int examId);
        void OverrideTotalScore(int examId, bool isOverridden = true, int? totalScore = null);
    }
}