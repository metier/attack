﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Transactions;
using System.Web.Security;
using Metier.Phoenix.Api.Configuration;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade.Account;
using Metier.Phoenix.Api.Facade.Codes;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Providers
{
    public class VersionLogProvider : IVersionLogProvider
    {
        private readonly MetierLmsContext _context;

        public VersionLogProvider(MetierLmsContext context)
        {
            _context = context;
        }

        public PartialList<VersionLogItem> GetVersionLogItems(int? skip, int? limit)
        {
            IQueryable<VersionLogItem> items = _context.VersionLogItems.OrderByDescending(v => v.Id);

            var totalCount = items.Count();

            if (skip.HasValue)
            {
                items = items.Skip(skip.Value);
            }
            if (limit.HasValue && limit.Value > 0)
            {
                items = items.Take(limit.Value);
            }

            return items.ToPartialList(totalCount);

        }

    }
}