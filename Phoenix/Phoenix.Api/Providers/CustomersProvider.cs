﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Transactions;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Facade.Account;
using Metier.Phoenix.Api.Services.ExternalCustomers;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;
using ValidationException = Metier.Phoenix.Core.Exceptions.ValidationException;

namespace Metier.Phoenix.Api.Providers
{
    public class CustomersProvider : ICustomersProvider
    {
        private readonly MetierLmsContext _context;
        private readonly ICustomerRepository _customerRepository;

        public CustomersProvider(MetierLmsContext context,
            ICustomerRepository customerRepository)
        {
            _context = context;
            _customerRepository = customerRepository;
        }

        public PartialList<SearchableCustomer> GetCustomers(int? distributorId = null, string query = null, int[] customerIds = null, int? childrenOfCustomerId = null, string orderBy = null, string orderDirection = null, int? skip = null, int? limit = null)
        {
            if (childrenOfCustomerId.HasValue)
            {
                if (customerIds != null)
                {
                    var appendList = customerIds.ToList();
                    appendList.AddRange(GetChildrenIds(childrenOfCustomerId.Value));
                    customerIds = appendList.ToArray();
                }
                else
                {
                    customerIds = GetChildrenIds(childrenOfCustomerId.Value).ToArray();
                }
            }

            return _customerRepository.GetCustomers(distributorId, query, customerIds, orderBy, orderDirection, skip, limit);
        }

        public Customer GetCustomer(int id)
        {
            return _customerRepository.FindById(id);
        }

        public Customer GetByExternalCustomer(int distributorId, string externalCustomerId)
        {
            return _customerRepository.FindbyExternalCustomerId(distributorId, externalCustomerId);
        }
        public Customer GetByCompanyRegistrationNumber(int distributorId, bool isArbitrary, string companyRegistrationNumber)
        {
            if(companyRegistrationNumber == null || companyRegistrationNumber.Replace(" ", "").Length == 0)
                return null;

            companyRegistrationNumber = companyRegistrationNumber.Replace(" ", "");
            return _customerRepository.FindbyCompanyRegistrationNumber(distributorId, isArbitrary, companyRegistrationNumber);
        }

        public Customer Create(Customer customer)
        {
            Customer existingCustomer = null;

            customer.IsRoot = customer.ParentId == null;
            if (!string.IsNullOrWhiteSpace(customer.CompanyRegistrationNumber))
                customer.CompanyRegistrationNumber = customer.CompanyRegistrationNumber.Replace(" ", "");

            TrimAddressInfo(customer.Addresses);

            if (customer.IsArbitrary)
            {
                existingCustomer = FindSimilarCustomer(customer);
                customer = existingCustomer ?? SetDefaultValuesOnArbitraryCustomer(customer);
            }
            else if (!string.IsNullOrWhiteSpace(customer.CompanyRegistrationNumber))
                existingCustomer = GetByCompanyRegistrationNumber(customer.DistributorId, customer.IsArbitrary, customer.CompanyRegistrationNumber);

            return existingCustomer ?? _customerRepository.Create(customer);
        }

        private void TrimAddressInfo(List<CustomerAddress> addresses)
        {
            addresses?.ForEach(a => a.AddressStreet1 = a.AddressStreet1?.Trim(' '));
            addresses?.ForEach(a => a.AddressStreet2 = a.AddressStreet2?.Trim(' '));
            addresses?.ForEach(a => a.ZipCode = a.ZipCode?.Trim(' '));
            addresses?.ForEach(a => a.City = a.City?.Trim(' '));
            addresses?.ForEach(a => a.Country = a.Country?.Trim(' '));
        }

        private Customer FindSimilarCustomer(Customer request)
        {

            if (!string.IsNullOrWhiteSpace(request.CompanyRegistrationNumber))
            {
                //Should never be more than onearbitrary customer with the same company registration number, 
                //but prefer selecting the latest created customer IF it ever should happen.
                return _context.Customers.OrderByDescending(cust => cust.Id).FirstOrDefault(c => c.IsArbitrary &&
                                                            c.DistributorId == request.DistributorId &&
                                                            c.CompanyRegistrationNumber.Equals(request.CompanyRegistrationNumber)
                                                            );
            }

            return _context.Customers
                .Where(
                    c => c.Name.Trim().ToLower() == request.Name.Trim().ToLower() &&
                         c.IsArbitrary &&
                         c.DistributorId == request.DistributorId)
                         .Include(c => c.Addresses)
                .FirstOrDefault(c => c.Addresses.Any(a => a.City.Trim().ToLower() == request.InvoicingAddress.City.Trim().ToLower() &&
                                                          a.ZipCode.Trim().ToLower() == request.InvoicingAddress.ZipCode.Trim().ToLower() &&
                                                          a.Country.Trim().ToLower() == request.InvoicingAddress.Country.Trim().ToLower() &&
                                                          a.AddressStreet1.Trim().ToLower() == request.InvoicingAddress.AddressStreet1.Trim().ToLower()));

        }

        public Customer FindSimilarCustomer(ArbitraryAccountRequest request)
        {

            if (!string.IsNullOrWhiteSpace(request.CompanyRegistrationNumber))
            {
                //Should never be more than one arbitrary customer with the same company registration number, 
                //but prefer selecting the latest created customer IF it ever should happen.
                return _context.Customers.OrderByDescending(cust => cust.Id).FirstOrDefault(c => c.IsArbitrary &&
                                                            c.DistributorId == request.DistributorId &&
                                                            c.CompanyRegistrationNumber.Equals(request.CompanyRegistrationNumber)
                                                            );
            }

            return _context.Customers
                .Where(c => c.Name.Trim().ToLower() == request.CompanyName.Trim().ToLower() && c.DistributorId == request.DistributorId)
                .Include(c => c.Addresses)
                .FirstOrDefault(c => c.Addresses.Any(a => a.City.Trim().ToLower() == request.City.Trim().ToLower() &&
                                                          a.ZipCode.Trim().ToLower() == request.ZipCode.Trim().ToLower() &&
                                                          a.Country.Trim().ToLower() == request.Country.Trim().ToLower() &&
                                                          a.AddressStreet1.Trim().ToLower() == request.StreetAddress1.Trim().ToLower()));
        }

        public IEnumerable<int> GetChildrenIds(int parentCustomerId, bool includeParent = false)
        {
            var allChildrenIds = new List<int>();
            if (includeParent)
            {
                allChildrenIds.Add(parentCustomerId);
            }
            var childIds = _context.Customers.Where(c => c.ParentId == parentCustomerId).Select(c => c.Id).ToArray();
            allChildrenIds.AddRange(childIds);
            foreach (var childId in childIds)
            {
                allChildrenIds.AddRange(GetChildrenIds(childId, includeParent));
            }
            return allChildrenIds.Distinct().ToList();
        }

        public bool Delete(int id)
        {
            var customer = GetCustomer(id);
            var validationErrors = ValidateForDelete(customer).ToList();
            if (validationErrors.Any())
            {
                throw new ValidationException("Could not delete customer", validationErrors);
            }

            return _customerRepository.Delete(customer);
        }

        public List<CustomLeadingText> Create(List<CustomLeadingText> entities)
        {
            return _customerRepository.Create(entities);
        }

        public Customer Update(int id, Customer customer)
        {
            customer.IsRoot = customer.ParentId == null;
            return _customerRepository.Update(id, customer);
        }

        public List<CustomLeadingText> GetLeadingTexts(int customerId)
        {
            return _customerRepository.GetLeadingTexts(customerId);
        }

        public Customer CreateArbitraryCustomer(ArbitraryAccountRequest arbitraryAccount)
        {
            var validationErrors = ValidateForCreateArbitraryCustomer(arbitraryAccount).ToList();
            if (validationErrors.Any())
            {
                throw new ValidationException("Could not create arbitrary customer.", validationErrors);
            }

            Customer createdCustomer;
            using (var transactionScope = new TransactionScope())
            {
                var customer = new Customer
                {
                    Name = arbitraryAccount.CompanyName,
                    CompanyRegistrationNumber = arbitraryAccount.CompanyRegistrationNumber,
                    IsArbitrary = true,
                    DistributorId = arbitraryAccount.DistributorId,
                    LanguageId = arbitraryAccount.LanguageId,
                    Addresses = new List<CustomerAddress>
                    {
                        new CustomerAddress
                        {
                            AddressType = AddressType.Invoice,
                            AddressStreet1 = arbitraryAccount.StreetAddress1,
                            AddressStreet2 = arbitraryAccount.StreetAddress2,
                            ZipCode = arbitraryAccount.ZipCode,
                            City = arbitraryAccount.City,
                            Country = arbitraryAccount.Country
                        }
                    }
                };


                customer = SetDefaultValuesOnArbitraryCustomer(customer);
                customer.CustomerInvoicing.OurRef = arbitraryAccount.OurRef;

                createdCustomer = _customerRepository.Create(customer);
                transactionScope.Complete();
            }

            return createdCustomer;
        }

        private Customer SetDefaultValuesOnArbitraryCustomer(Customer customer)
        {
            customer.ParticipantInfoDefinition = new ParticipantInfoDefinition
            {
                CompetitionMode = CompetitionModeTypes.None
            };
            customer.CustomLeadingTexts = _context.LeadingTextes.ToList().Select(lt => new CustomLeadingText
            {
                LeadingTextId = lt.Id,
                IsVisible = true,
                Text = lt.Text,
                IsMandatory = lt.IsMandatory,
                IsVisibleInvoiceLine = false,
                IsGroupByOnInvoice = false
            }).ToList();
            customer.CustomerInvoicing = new CustomerInvoicing
            {
                BundledBilling = false,
                OrderRefLeadingTextId = LeadingTextIds.CustomField1,
                YourRefLeadingTextId = LeadingTextIds.CustomField2
            };
            customer.AutomailDefinition = new AutomailDefinition
            {
                IsWelcomeNotification = true,
                IsEnrollmentNotification = true,
                IsExcludeProgressSchedule = true,
                IsProgressNotificationElearning = false,
                IsReminder = true,
                IsExamSubmissionNotification = true
            };

            return customer;
        }

        public Customer CreateArbitraryCustomer(UserAccount userAccount)
        {
            var validationErrors = ValidateForCreateArbitraryCustomer(userAccount).ToList();
            if (validationErrors.Any())
            {
                throw new ValidationException("Could not create arbitrary customer.", validationErrors);
            }

            return CreateArbitraryCustomer(new ArbitraryAccountRequest
            {
                DistributorId = userAccount.DistributorId.Value,
                CompanyName = userAccount.CompanyName,
                CompanyRegistrationNumber = userAccount.CompanyRegistrationNumber,
                OurRef = userAccount.OurRef,
                StreetAddress1 = userAccount.User.StreetAddress,
                StreetAddress2 = userAccount.User.StreetAddress2,
                ZipCode = userAccount.User.ZipCode,
                City = userAccount.User.City,
                Country = userAccount.User.Country,
                PreferredLanguageId = userAccount.User.PreferredLanguageId,
                LanguageId = userAccount.User.PreferredLanguageId ?? 43         //Defaults to english if preferred language is not selected (sorry for the "magic" number)
            });
        }

        private IEnumerable<ValidationResult> ValidateForCreateArbitraryCustomer(ArbitraryAccountRequest accountRequest)
        {
            if (string.IsNullOrEmpty(accountRequest.Country) || string.IsNullOrEmpty(accountRequest.City))
            {
                yield return new ValidationResult("Country and city are required when creating a customer in Agresso");
            }

            if (accountRequest.DistributorId == 0)
            {
                yield return new ValidationResult("DistributorId is not set");
            }
            if (string.IsNullOrEmpty(accountRequest.CompanyName))
            {
                yield return new ValidationResult("CompanyName is required");
            }
        }
        
        private IEnumerable<ValidationResult> ValidateForCreateArbitraryCustomer(UserAccount account)
        {
            if (account.User == null)
            {
                yield return new ValidationResult("A User object must be specified to create an arbitrary customer");
            }
            else
            {
                if (string.IsNullOrEmpty(account.User.Country) || string.IsNullOrEmpty(account.User.City))
                {
                    yield return new ValidationResult("Country and city are required when creating a customer in Agresso");
                }
            }

            if (!account.DistributorId.HasValue || account.DistributorId == 0)
            {
                yield return new ValidationResult("DistributorId is not set");
            }
            if (account.CustomerId != 0)
            {
                yield return new ValidationResult("CustomerId is set. Arbitrary customers cannot be associated with another customer");
            }
            if (string.IsNullOrEmpty(account.CompanyName))
            {
                yield return new ValidationResult("CompanyName is required");
            }
        }

        private IEnumerable<ValidationResult> ValidateForDelete(Customer customer)
        {
            if (!string.IsNullOrWhiteSpace(customer.ExternalCustomerId))
                yield return new ValidationResult("Customer has reference to account in external accounting system.");

            if (_context.Users.Any(u => u.CustomerId == customer.Id))
                yield return new ValidationResult("Customer must not contain any users.");

            if (_context.Participants.Any(p => p.CustomerId == customer.Id))
                yield return new ValidationResult("Customer must not have any related participations.");

            if (_context.Orders.Any(o => o.CustomerId == customer.Id))
                yield return new ValidationResult("Customer must not have any related orders.");

            if (_context.CoursePrograms.Any(cp => cp.CustomerId == customer.Id))
                yield return new ValidationResult("Customer must not have any course programs.");

            if (_context.ActivitySets.Any(a => a.CustomerId == customer.Id))
                yield return new ValidationResult("Customer must not have any activitysets.");

            if (_context.ActivitySetShares.Any(a => a.CustomerId == customer.Id))
                yield return new ValidationResult("Customer must not have any shared activitysets.");

            if (_context.CustomerArticles.Any(a => a.CustomerId == customer.Id))
                yield return new ValidationResult("Customer must not have any customer articles.");

            if (_context.ProductDescriptions.Any(pd => pd.CustomerId == customer.Id))
                yield return new ValidationResult("Customer must not have any related product descriptions.");

            if (_context.EnrollmentConditions.Any(e => e.CustomerId == customer.Id))
                yield return new ValidationResult("Customer must not have any related enrollment conditions.");

            //TODO: Uncomment when FLMS-462 is merged
            //if (_context.CustomerArticleEnrollmentConditions.Any(e => e.CustomerId == customer.Id))
            //    yield return new ValidationResult("Customer must not have any related enrollment conditions on customer articles.");

            if (_context.Questions.Any(q => q.CustomerId == customer.Id))
                yield return new ValidationResult("Customer must not have any custom exam questions.");
        }
    }
}