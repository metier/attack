﻿using System;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Providers
{
    public interface IScormProvider
    {
        ScormAttempt CreateSession(int userId, int rcoId);

        void UpdateStatus(Guid sessionId, string status);
        void UpdateScore(Guid sessionId, int score);
        void UpdateSuspendData(Guid sessionId, string suspendData);
        void TerminateSession(Guid sessionId);
        ScormAttempt GetAttempt(Guid sessionId, bool activeOnly = true);
        void ResetRcoStatus(int rcoId, int userId, bool includeChildren);
        void ResetAttemptStatusIfNecessary(int performanceId);
    }
}