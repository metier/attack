﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Facade.Account;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Providers
{
    public interface ICustomersProvider
    {
        Customer GetCustomer(int id);
        Customer Create(Customer customer);
        Customer Update(int id, Customer update);
        bool Delete(int id);
        
        List<CustomLeadingText> Create(List<CustomLeadingText> entities);
        List<CustomLeadingText> GetLeadingTexts(int customerId);

        Customer CreateArbitraryCustomer(ArbitraryAccountRequest arbitraryAccount);
        Customer CreateArbitraryCustomer(UserAccount userAccount);
        Customer GetByExternalCustomer(int distributorId, string externalCustomerId);
        Customer GetByCompanyRegistrationNumber(int distributorId, bool isArbitrary, string companyRegistrationNumber);
        PartialList<SearchableCustomer> GetCustomers(int? distributorId = null, string query = null, int[] customerIds = null, int? childrenOfCustomerId = null, string orderBy = null, string orderDirection = null, int? skip = null, int? limit = null);

        /// <summary>
        /// Returns all the cildren of the given <see cref="parentCustomerId"/>
        /// </summary>
        /// <param name="parentCustomerId"></param>
        /// <param name="includeParent"></param>
        /// <returns></returns>
        IEnumerable<int> GetChildrenIds(int parentCustomerId, bool includeParent = false);

        Customer FindSimilarCustomer(ArbitraryAccountRequest request);
    }
}