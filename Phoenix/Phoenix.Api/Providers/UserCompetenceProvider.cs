﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Extensions;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Mail;
using Metier.Phoenix.Mail.Templates.MissingExamCompetency;

namespace Metier.Phoenix.Api.Providers
{
    public class UserCompetenceProvider : IUserCompetenceProvider
    {
        private readonly MetierLmsContext _context;
        private readonly IAutoMailer _autoMailer;        
        private readonly IMissingExamCompetencyMessageFactory _missingExamCompetencyMessageFactory;

        public UserCompetenceProvider(MetierLmsContext context, IAutoMailer autoMailer, IMissingExamCompetencyMessageFactory missingExamCompetencyMessageFactory)
        {
            _context = context;
            _autoMailer = autoMailer;            
            _missingExamCompetencyMessageFactory = missingExamCompetencyMessageFactory;
        }

        public UserCompetence GetByUserId(int userId)
        {
            return _context.UserCompetences.Include(uc => uc.Attachments).FirstOrDefault(u => u.UserId == userId);
        }

        public UserCompetence Update(int userId, UserCompetence userCompetence)
        {
            if (userId <= 0)
            {
                throw new ValidationException("UserId must be provided");
            }
            if (userCompetence.Id <= 0)
            {
                throw new ValidationException("Id must be provided");
            }

            userCompetence.UserId = userId;
            var dbUserCompetence = GetByUserId(userCompetence.UserId);
            if (!userCompetence.RowVersion.SequenceEqual(dbUserCompetence.RowVersion))
            {
                throw new DbUpdateConcurrencyException("There is a newer record of the UserCompetence in the database.");
            }
            // Users are not allowed to change Created time
            userCompetence.Created = dbUserCompetence.Created;
            if (userCompetence.IsTermsAccepted && !dbUserCompetence.IsTermsAccepted)
            {
                userCompetence.TermsAcceptedDate = DateTime.UtcNow;

                //Send email to all ecoaches of enrolled exams if user is missing exam competence
                if (userCompetence.EducationType.HasValue && userCompetence.EducationType.Value == Core.Data.Enums.EducationTypes.None
                    && userCompetence.IsAccreditedPracticalCompetenceSatisfied.HasValue && !userCompetence.IsAccreditedPracticalCompetenceSatisfied.Value)
                {
                    
                    var autoMailMessage = _missingExamCompetencyMessageFactory.CreateMissingExamCompetencyMessage(userCompetence.UserId);
                    var sendSuccess = _autoMailer.Send(autoMailMessage);
                    if (!sendSuccess)
                    {
                        throw new MailSendException(string.Format("Failed to send missing exam competency email for {0}", userCompetence.Email));
                    }

                }
            }
            else
            {
                userCompetence.TermsAcceptedDate = dbUserCompetence.TermsAcceptedDate;
            }
            _context.Entry(dbUserCompetence).CurrentValues.SetValues(userCompetence);
            // Must set non-mapped encrypted field manually
            dbUserCompetence.SocialSecurityNumber = userCompetence.SocialSecurityNumber;
            
            userCompetence.UpdateRelatedEntities(dbUserCompetence, a => a.Attachments, _context);
            _context.SaveChanges();
            return GetByUserId(userCompetence.UserId);
        }

        public UserCompetence Create(int userId, UserCompetence userCompetence)
        {
            if (userId <= 0)
            {
                throw new ValidationException("UserId must be provided");
            }

            userCompetence.UserId = userId;
            userCompetence.Created = DateTime.UtcNow;
            if (userCompetence.IsTermsAccepted)
            {
                userCompetence.TermsAcceptedDate = DateTime.UtcNow;
            }
            _context.UserCompetences.Add(userCompetence);
            _context.SaveChanges();
            return GetByUserId(userId);
        }
    }
}