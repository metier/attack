﻿using System;
using System.Collections.Generic;
using Metier.Phoenix.Api.Facade.ExamContent;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Providers
{
    public interface IQuestionsProvider
    {
        Question Get(int id);
        Question Create(Question question);
        List<Question> BatchCreate(List<Question> questions);
        List<Question> BatchUpdate(QuestionsChangeSet changeSet);
        List<Question> BatchUpdate(List<Question> questions);
 
        Question Update(int id, Question question);
        PartialList<SearchableQuestion> Search(string query, int[] tagIds, int[] productIds, int[] rcoIds, QuestionDifficulties[] difficulties, QuestionStatuses[] statuses, int[] languageIds, int customerSpecificFilterValue, int[] customerIds, DateTime? lastUsedFromDate, DateTime? lastUsedToDate, int skip, int limit, string orderBy, string orderDirection);

        IEnumerable<Question> GetPreviousVersions(int questionId);
        IEnumerable<Question> GetNewerVersions(int questionId);
        Question GetPreviousVersion(int questionId);
        Question GetNewestVersion(int questionId);
    }
}