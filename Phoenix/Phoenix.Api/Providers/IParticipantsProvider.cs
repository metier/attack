﻿using System;
using System.Collections;
using System.Collections.Generic;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Facade.Participant;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Providers
{
    public interface IParticipantsProvider
    {
        Participant Move(int id, ParticipantMoveRequest moveRequest);
//        OrderLine GetOrderLine(Participant participant);
        Participant GetParticipant(int participantId);
        IEnumerable<Participant> GetParticipants(int userId, int rcoId);
        IEnumerable<Participant> GetParticipants(int[] participantIds, bool includeDeletedParticipants = false);
        IEnumerable<Participant> GetParticipants(int activityId, bool includeDeletedParticipants = false);
        IEnumerable<Participant> GetParticipations(int userId);
        IEnumerable<SearchableParticipant> GetParticipationsForEnrollment(int enrollmentId);
        Enrollment GetEnrollment(int enrollmentId);
        List<Enrollment> GetEnrollments(int userId);
        List<Enrollment> GetEnrollmentsOnActivity(int activityId);
        List<Enrollment> GetEnrollmentsOnActivitySet(int activitySetId);
        IEnumerable<Participant> GetParticipantsOnActivitySet(int activitySetId, bool includeDeletedParticipants = false);
        RcoCourse GetRcoCourse(int participantId);
        RcoExamContainer GetRcoExamContainer(int participantId);
        void UpdateStatus(int userId, int rcoId);
        void SubmitExam(int id, Attachment attachment);
        Participant ChangeStatus(int id, string status);
		Participant UpdateParticipant(int id, ParticipantUpdateRequest updateRequest);
		ParticipantGradeResponse SetExamGrade(int id, string grade);
        PartialList<ExamSubmission> GetExamSubmissionsForCurrentUser(bool includeGraded, int skip, int limit);

        /// <summary>
        /// Checks if the current user can grade the given participant
        /// </summary>
        /// <param name="participantId"></param>
        /// <returns></returns>
        bool CanGradeParticipant(int participantId);
    }
}