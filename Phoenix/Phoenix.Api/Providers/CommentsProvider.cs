﻿using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Exceptions;

namespace Metier.Phoenix.Api.Providers
{
    public class CommentsProvider : ICommentsProvider
    {
        private readonly ICommentsRepository _commentsRepository;
        private readonly IParticipantsProvider _participantsProvider;
        
        public CommentsProvider(ICommentsRepository commentsRepository, IParticipantsProvider participantsProvider)
        {
            _commentsRepository = commentsRepository;
            _participantsProvider = participantsProvider;
        }

        public List<Comment> FindByEntityId(string entity, int entityId)
        {
            return _commentsRepository.FindByEntityId(entity, entityId);
        }
        
        public List<Comment> GetUserComments(int userId, bool includeEnrollmentComments, bool includeParticipantComments)
        {
            var comments = GetUserComments(userId);

            if (includeEnrollmentComments)
                comments.AddRange(GetAllEnrollmentComments(userId));
            
            if (includeParticipantComments)
                comments.AddRange(GetAllParticipationComments(userId));

            comments = comments.OrderBy(c => c.Id).ToList();
            return comments;
        }

        public List<Comment> GetParticipationComments(int participantId, bool includeUserComments, bool includeEnrollmentComments)
        {
            var comments = GetParticipationComments(participantId);
            var participant = _participantsProvider.GetParticipant(participantId);

            if (includeUserComments)
                comments.AddRange(GetUserComments(participant.UserId));

            if (includeEnrollmentComments)
                comments.AddRange(GetEnrollmentComments(participant.EnrollmentId));

            comments = comments.OrderBy(c => c.Id).ToList();

            return comments;
        }

        public List<Comment> GetEnrollmentComments(int enrollmentId, bool includeUserComments, bool includeParticipantComments)
        {
            var comments = GetEnrollmentComments(enrollmentId);
            var enrollment = _participantsProvider.GetEnrollment(enrollmentId);

            if (includeUserComments)
                comments.AddRange(GetUserComments(enrollment.UserId));

            if (includeParticipantComments)
            {
                var participations = _participantsProvider.GetParticipationsForEnrollment(enrollmentId);
                comments.AddRange(GetParticipationComments(participations.Select(p=> p.Id).ToList()));
            }

            comments = comments.OrderBy(c => c.Id).ToList();

            return comments;
        }

        #region private functions

        private List<Comment> GetUserComments(int userId)
        {
            return _commentsRepository.FindByEntityId("user", userId);
        }

        private List<Comment> GetParticipationComments(int participantId)
        {
            return _commentsRepository.FindByEntityId("participant", participantId);
        }

        private List<Comment> GetAllParticipationComments(int userId)
        {
            var participations = _participantsProvider.GetParticipations(userId);
            return _commentsRepository.FindByEntityIds("participant", participations.Select(p=> p.Id).ToArray());
        }

        private List<Comment> GetParticipationComments(List<int> participantIds)
        {
            return _commentsRepository.FindByEntityIds("participant", participantIds.ToArray());
        }

        private List<Comment> GetEnrollmentComments(int enrollmentId)
        {
            return _commentsRepository.FindByEntityId("enrollment", enrollmentId);
        }

        private List<Comment> GetAllEnrollmentComments(int userId)
        {
            var enrollments = _participantsProvider.GetEnrollments(userId);
            return _commentsRepository.FindByEntityIds("enrollment", enrollments.Select(e=> e.Id).ToArray());
        }

        #endregion

        public List<Comment> GetAllEnrollmentComments(int activitySetId, bool includeUserComments, bool includeParticipantComments)
        {
            var participants = _participantsProvider.GetParticipantsOnActivitySet(activitySetId);

            var enrollmentIds = participants.Select(p => p.EnrollmentId).Distinct().ToArray();
            var comments = _commentsRepository.FindByEntityIds("enrollment", enrollmentIds);

            if (includeUserComments)
            {
                var userIds = participants.Select(p => p.UserId).ToArray();
                var userComments = _commentsRepository.FindByEntityIds("user", userIds);
                comments.AddRange(userComments);
            }

            if (includeParticipantComments)
            {
                var participantIds = participants.Select(p => p.Id).ToArray();
                var participantComments = _commentsRepository.FindByEntityIds("participant", participantIds);
                comments.AddRange(participantComments);
            }

            return comments.OrderBy(c => c.Id).ToList();

        }

        public List<Comment> GetParticipantComments(int activityId, bool includeUserComments = false, bool includeEnrollmentComments = false)
        {
            var participants = _participantsProvider.GetParticipants(activityId);
            var participantIds = participants.Select(p => p.Id).ToArray();
            var userIds = participants.Select(p => p.UserId).ToArray();

            var participantComments = _commentsRepository.FindByEntityIds("participant", participantIds);

            if (includeUserComments)
            {
                var userComments = _commentsRepository.FindByEntityIds("user", userIds);
                participantComments.AddRange(userComments);
            }

            if (includeEnrollmentComments)
            {
                var enrollments = _participantsProvider.GetEnrollmentsOnActivity(activityId);
                var enrollmentComments = _commentsRepository.FindByEntityIds("enrollment", enrollments.Select(e=> e.Id).ToArray());
                participantComments.AddRange(enrollmentComments);
            }

            return participantComments.OrderBy(c => c.Id).ToList();
        }

        public List<Comment> Delete(int id)
        {
            ValidateWrite(id);
            return _commentsRepository.Delete(id);
        }

        public List<Comment> Create(Comment comment)
        {
            ValidateWrite(comment);
            return _commentsRepository.Create(comment);
        }

        public List<Comment> Update(Comment comment)
        {
            ValidateWrite(comment);
            return _commentsRepository.Update(comment);
        }

        private void ValidateWrite(int id)
        {
            var comment = _commentsRepository.FindById(id);
            ValidateWrite(comment);
        }

        private void ValidateWrite(Comment comment)
        {
            if (comment.Entity == "participantGrading")
            {
                var canGrade = _participantsProvider.CanGradeParticipant(comment.EntityId);
                if (!canGrade)
                {
                    throw new ValidationException("User is not authorized to create, delete or change comment " + comment.Id);
                }
            }
        }
    }
}