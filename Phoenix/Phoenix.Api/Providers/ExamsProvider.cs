﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Transactions;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade.ExamPlayer;
using Metier.Phoenix.Api.Factories.Exam;
using Metier.Phoenix.Api.Factories.ExamPlayer;
using Metier.Phoenix.Api.Messaging;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Business;
using Metier.Phoenix.Core.Business.ChangeLog;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Exams;
using Metier.Phoenix.Core.Exceptions;
using Answer = Metier.Phoenix.Api.Facade.ExamPlayer.Answer;
using Exam = Metier.Phoenix.Api.Facade.ExamPlayer.Exam;

namespace Metier.Phoenix.Api.Providers
{
    public class ExamsProvider : IExamsProvider
    {
        private readonly MetierLmsContext _context;
        private readonly IAuthorizationProvider _authorizationProvider;
        private readonly IExamFactory _examFactory;
        private readonly IExamPlayerFactory _examPlayerFactory;
        private readonly IExamGrader _examGrader;
        private readonly IEventAggregator _eventAggregator;
        private readonly IParticipantBusiness _participantBusiness;
        private readonly IChangeLogProvider _changeLogProvider;

        public ExamsProvider(MetierLmsContext context, 
            IAuthorizationProvider authorizationProvider, 
            IExamFactory examFactory, 
            IExamPlayerFactory examPlayerFactory,
            IExamGrader examGrader,
            IEventAggregator eventAggregator,
            IParticipantBusiness participantBusiness,
            IChangeLogProvider changeLogProvider)
        {
            _context = context;
            _authorizationProvider = authorizationProvider;
            _examFactory = examFactory;
            _examPlayerFactory = examPlayerFactory;
            _examGrader = examGrader;
            _eventAggregator = eventAggregator;
            _participantBusiness = participantBusiness;
            _changeLogProvider = changeLogProvider;
        }

        public ExamInfo GetExamInfo(int activityId)
        {
            var examInfoData = GetExamInfoDataFromActivityId(activityId);
            examInfoData.Exam = _context.Exams
                                     .WithQuestions()
                                     .WithAnswers()
                                     .FirstOrDefault(e => e.ParticipantId == examInfoData.Participant.Id);
            var exam = examInfoData.Exam != null ? _examPlayerFactory.CreateExam(examInfoData.Exam, examInfoData.Participant, examInfoData.Activity, examInfoData.Article) : null;
            var availability = _participantBusiness.GetAvailability(examInfoData.Participant.Id);
            var examInfo = new ExamInfo
            {
                Name = examInfoData.Activity.Name,
                StartStartTime = availability.AvailabilityStart,
                StartEndTime = availability.AvailabilityEnd,
                DurationInSeconds = (examInfoData.Activity.Duration.GetValueOrDefault() + examInfoData.Participant.AdditionalTime.GetValueOrDefault()) / 1000,
                NumberOfQuestions = examInfoData.Activity.NumberOfQuestions.HasValue ? examInfoData.Activity.NumberOfQuestions.Value : 0,
                NumberOfQuestionsPerPage = examInfoData.Activity.NumberOfQuestionsPerPage ?? (examInfoData.Activity.NumberOfQuestions.HasValue ? examInfoData.Activity.NumberOfQuestions.Value : 0),
                Language = examInfoData.Article.Language.Identifier.ToLower(),
                Exam = exam
            };
            examInfo.TimeBeforeAvailableInSeconds = GetTimeBeforeAvailableInSeconds(examInfo.StartStartTime, examInfo.StartEndTime);
            if (exam != null)
            {
                if (exam.Status.IsSubmitted)
                {
                    examInfo.Result = _examPlayerFactory.CreateResult(examInfoData.Exam, examInfoData.Article);
                } 
                else if (exam.Status.TimeRemainingInSeconds <= 0)
                {
                    examInfo.Result = SubmitExam(examInfoData);    
                }
                
            }
            
            return examInfo;
        }

        private long? GetTimeBeforeAvailableInSeconds(DateTimeOffset? startStartTime, DateTimeOffset? startEndTime)
        {
            if (startStartTime == null) return null;
            if (startEndTime != null && DateTime.UtcNow > startEndTime.Value.UtcDateTime) return null;
            return (long?)(startStartTime.Value.UtcDateTime - DateTime.UtcNow).TotalSeconds;
        }

        public Exam StartExam(int activityId)
        {
            var examInfoData = GetExamInfoDataFromActivityId(activityId);

            ValidateExamAvailability(examInfoData.Activity);

            var exam = _context.Exams.FirstOrDefault(e => e.ParticipantId == examInfoData.Participant.Id);
            if (exam != null)
            {
                throw new ValidationException(string.Format("Participant {0} already has an active exam with ID {1}", examInfoData.Participant.Id, exam.Id));
            }
            var oldStatus = examInfoData.Participant.StatusCodeValue;
            examInfoData.Participant.StatusCodeValue = ParticipantStatusCodes.InProgress;
            _eventAggregator.Publish(new ParticipantStatusChangedEvent(examInfoData.Participant.Id, oldStatus, examInfoData.Participant.StatusCodeValue));
            
            var examEntity = _examFactory.Create(examInfoData.Article, examInfoData.Activity,  examInfoData.Participant, examInfoData.CustomerArticle);
            _context.Exams.Add(examEntity);
            _context.SaveChanges();

            return _examPlayerFactory.CreateExam(examEntity, examInfoData.Participant, examInfoData.Activity, examInfoData.Article);
        }

        public Core.Data.Entities.Exam GetExamResult(int participantId)
        {
            var exam = _context.Exams
                               .WithQuestions()
                               .WithAnswers()
                               .Include(e => e.ModifiedBy)
                               .FirstOrDefault(e => e.ParticipantId == participantId);

            return exam;
        }

        public void OverrideTotalScore(int examId, bool isOverridden = true, int? totalScore = null)
        {
            var data = GetExamInfoDataFromExamId(examId, false);
            if (!data.Exam.IsSubmitted)
            {
                throw new ValidationException("Cannot override score on exams that are in progress");
            }
            if (isOverridden && !totalScore.HasValue)
            {
                throw new ValidationException("Total score is required when overridden");
            }
            if (isOverridden && totalScore < 0)
            {
                throw new ValidationException("Score cannot be negative");
            }

            data.Exam.IsTotalScoreOverridden = isOverridden;
            data.Exam.TotalScore = totalScore;
            data.Exam.ModifiedById = data.CurrentUser.Id;
            data.Exam.Modified = DateTime.UtcNow;

            _examGrader.SetGrade(data.Exam, data.Article, data.Participant);
            _context.SaveChanges();
        }

        public Exam GetExam(int examId)
        {
            var examInfoData = GetExamInfoDataFromExamId(examId);
            return _examPlayerFactory.CreateExam(examInfoData.Exam, examInfoData.Participant, examInfoData.Activity, examInfoData.Article);
        }

        public ExamStatus GetExamStatus(int examId)
        {
            var examInfoData = GetExamInfoDataFromExamId(examId);
            return _examPlayerFactory.CreateExamStatus(examInfoData.Exam, examInfoData.Participant, examInfoData.Activity, examInfoData.Article);
        }

        public void SubmitAnswer(int examId, Answer answer)
        {
            var examInfoData = GetExamInfoDataFromExamId(examId);
            
            var examStatus = _examPlayerFactory.CreateExamStatus(examInfoData.Exam, examInfoData.Participant, examInfoData.Activity, examInfoData.Article);
            ValidateActiveExam(examStatus);

            var answerEntity = examInfoData.Exam.Answers.FirstOrDefault(a => a.Id == answer.Id);
            if (answerEntity == null)
            {
                throw new EntityNotFoundException(string.Format("Answer with ID {0} is not part of exam with ID {1}", answer.Id, examId));
            }

            var question = examInfoData.Exam.Questions.First(q => q.Id == answerEntity.QuestionId);
            if (question.Type == QuestionTypes.TrueFalse)
            {
                answerEntity.BoolAnswer = answer.BoolAnswer;
            }
            else if (question.Type == QuestionTypes.MultipleChoiceSingle)
            {
                answerEntity.OptionsAnswers = _context.QuestionOptions.Where(o => o.Id == answer.SingleChoiceAnswer).ToList();
            }
            else if (question.Type == QuestionTypes.MultipleChoiceMultiple)
            {
                answerEntity.OptionsAnswers = _context.QuestionOptions.Where(o => answer.MultipleChoiceAnswers.Contains(o.Id)).ToList();
            }
            answerEntity.IsMarkedForReview = answer.IsMarkedForReview;
            _context.SaveChanges();
        }

        public Core.Data.Entities.Exam UnsubmitExam(int examId)
        {
            using (var transaction = new TransactionScope())
            {
                var examInfoData = GetExamInfoDataFromExamId(examId, false);
                var participant = examInfoData.Participant;

                if (!examInfoData.Exam.IsSubmitted)
                {
                    _changeLogProvider.Write(participant.Id, ChangeLogEntityTypes.Participant, ChangeLogTypes.ExamSubmitted, "Could not unsubmit exam. It is not yet submitted.");
                    return examInfoData.Exam;
                }

                examInfoData.Exam.IsSubmitted = false;
                examInfoData.Exam.SubmittedAt = null;
                examInfoData.Exam.TotalScore = null;

                participant.ExamGrade = null;
                participant.Result = null;
                SetParticipantStatusCodeValue(examInfoData.Participant, ParticipantStatusCodes.InProgress);

                _changeLogProvider.Write(participant.Id, ChangeLogEntityTypes.Participant, ChangeLogTypes.ExamSubmitted, "Exam unsubmitted");

                _context.SaveChanges();
                transaction.Complete();
                
                return examInfoData.Exam;
            }
        }

        public ExamResult SubmitExam(int examId)
        {
            var examInfoData = GetExamInfoDataFromExamId(examId);
            return SubmitExam(examInfoData);
        }

        private ExamResult SubmitExam(ExamInfoData examInfoData)
        {
            if (examInfoData.Exam.IsSubmitted)
            {
                return _examPlayerFactory.CreateResult(examInfoData.Exam, examInfoData.Article);
            }

            SetParticipantStatusCodeValue(examInfoData.Participant, ParticipantStatusCodes.Completed);
            
            examInfoData.Exam.IsSubmitted = true;
            examInfoData.Exam.SubmittedAt = DateTime.UtcNow;
            return GradeExam(examInfoData);
        }

        private ExamResult GradeExam(ExamInfoData examInfoData)
        {
            _examGrader.SetGrade(examInfoData.Exam, examInfoData.Article, examInfoData.Participant);
            _context.SaveChanges();
            return _examPlayerFactory.CreateResult(examInfoData.Exam, examInfoData.Article);
        }

        private void SetParticipantStatusCodeValue(Participant participant, string statusCode)
        {
            if (ParticipantStatusCodes.IsValidStatus(statusCode))
            {
                var oldStatus = participant.StatusCodeValue;
                participant.StatusCodeValue = statusCode;

                if (oldStatus != participant.StatusCodeValue)
                {
                    _eventAggregator.Publish(new ParticipantStatusChangedEvent(participant.Id, oldStatus, participant.StatusCodeValue));
                }
            }
            else
            {
                throw new ValidationException(string.Format("Invalid statuscode: {0}", statusCode));
            }
        }

        private static void ValidateActiveExam(ExamStatus examStatus)
        {
            if (examStatus.TimeRemainingInSeconds <= 0)
            {
                throw new ValidationException("No time remaining");
            }
            if (examStatus.IsSubmitted)
            {
                throw new ValidationException("Exam is already submitted");
            }
        }

        private static void ValidateExamAvailability(Activity activity)
        {
            var activityPeriod = activity.ActivityPeriods.FirstOrDefault();
            if (activityPeriod == null || !activityPeriod.Start.HasValue)
            {
                throw new ValidationException("Unable not determine availability for activity");
            }
            if (activityPeriod.Start.Value.UtcDateTime > DateTime.UtcNow)
            {
                throw new ValidationException("Exam is not yet available");
            }
            if (activityPeriod.End.HasValue && activityPeriod.End.Value.UtcDateTime < DateTime.UtcNow)
            {
                throw new ValidationException("Exam is no longer available");
            }
        }

        private ExamInfoData GetExamInfoDataFromActivityId(int activityId)
        {
            var activity = _context.Activities.Include(a => a.Participants)
                                              .Include(a => a.ActivityPeriods)
                                              .Include(a => a.ActivityExamCourses)
                                              .FirstOrDefault(a => a.Id == activityId && !a.IsDeleted);
            if (activity == null)
            {
                throw new EntityNotFoundException(string.Format("Activity with ID {0} not found", activityId));
            }

            var currentUser = _authorizationProvider.GetCurrentUser();
            var participant = activity.Participants.FirstOrDefault(p => p.UserId == currentUser.Id && !p.IsDeleted);
            if (participant == null)
            {
                throw new ValidationException("Current user is not a participant of activity " + activityId, ValidationErrorCodes.ParticipantNotFound);
            }

            var customerArticle = _context.CustomerArticles.FirstOrDefault(a => a.Id == activity.CustomerArticleId && !a.IsDeleted);
            if (customerArticle == null)
            {
                throw new EntityNotFoundException(string.Format("Customer Article with ID {0} not found", activity.CustomerArticleId));
            }
            var article = _context.Articles.WithLanguage().FirstOrDefault(a => a.Id == customerArticle.ArticleCopyId && !a.IsDeleted) as ExamArticle;
            if (article == null)
            {
                throw new EntityNotFoundException(string.Format("Exam Article with ID {0} not found", customerArticle.ArticleCopyId));
            }

            return new ExamInfoData
            {
                Activity = activity,
                Participant = participant,
                Article = article,
                CustomerArticle = customerArticle
            };
        }

        private ExamInfoData GetExamInfoDataFromExamId(int examId, bool participantMustBeCurrentUser = true)
        {
            var exam = _context.Exams
                               .WithQuestions()
                               .WithAnswers()
                               .FirstOrDefault(e => e.Id == examId);
            if (exam == null)
            {
                throw new EntityNotFoundException(string.Format("Exam with ID {0} not found", examId));
            }

            var currentUser = _authorizationProvider.GetCurrentUser();
            Participant participant;
            if (participantMustBeCurrentUser)
            {
                participant = _context.Participants.FirstOrDefault(p => p.Id == exam.ParticipantId && p.UserId == currentUser.Id && !p.IsDeleted);
                if (participant == null)
                {
                    throw new ValidationException("Current user is not a participant of exam with ID " + examId, ValidationErrorCodes.ParticipantNotFound);
                }
            }
            else
            {
                participant = _context.Participants.FirstOrDefault(p => p.Id == exam.ParticipantId && !p.IsDeleted);
                if (participant == null)
                {
                    throw new EntityNotFoundException(string.Format("Participant with ID {0} not found", exam.ParticipantId));
                }
            }
            
            var activity = _context.Activities.Include(a => a.Participants)
                                              .Include(a => a.ActivityPeriods)
                                              .Include(a => a.ActivityExamCourses)
                                              .FirstOrDefault(a => a.Id == participant.ActivityId && !a.IsDeleted);
            if (activity == null)
            {
                throw new EntityNotFoundException(string.Format("Activity with ID {0} not found", participant.ActivityId));
            }

            var customerArticle = _context.CustomerArticles.FirstOrDefault(a => a.Id == activity.CustomerArticleId && !a.IsDeleted);
            if (customerArticle == null)
            {
                throw new EntityNotFoundException(string.Format("Customer Article with ID {0} not found", activity.CustomerArticleId));
            }
            var article = _context.Articles.WithLanguage().FirstOrDefault(a => a.Id == customerArticle.ArticleCopyId && !a.IsDeleted) as ExamArticle;
            if (article == null)
            {
                throw new EntityNotFoundException(string.Format("Exam Article with ID {0} not found", customerArticle.ArticleCopyId));
            }

            return new ExamInfoData
            {
                Exam = exam,
                Activity = activity,
                Participant = participant,
                Article = article,
                CustomerArticle = customerArticle,
                CurrentUser = currentUser
            };
        }

        private class ExamInfoData
        {
            public Core.Data.Entities.Exam Exam { get; set; }
            public Activity Activity { get; set; }
            public Participant Participant { get; set; }
            public ExamArticle Article { get; set; }
            public CustomerArticle CustomerArticle { get; set; }
            public User CurrentUser { get; set; }
        }
    }
}