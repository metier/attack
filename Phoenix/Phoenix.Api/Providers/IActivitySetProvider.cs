﻿using System;
using System.Collections.Generic;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Facade.Enrollment;
using Metier.Phoenix.Api.Facade.Participant;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Providers
{
    public interface IActivitySetProvider
    {
        ActivitySet AddActivity(int id, int activityId);
        ActivitySet RemoveActivity(int id, int activityId);
        Tuple<ActivitySet, List<int>> Enroll(int id, int userId, EnrollmentDetails enrollmentDetails, string paymentReference = null, int? customerId = null);
        IEnumerable<ActivitySet> Enroll(int[] activitySetIds, int userId, EnrollmentDetails enrollmentDetails, string paymentReference = null, int? customerId = null);
        ActivitySet Unenroll(int id, int userId);
        ActivitySet Share(int id, int customerId);
        ActivitySet Unshare(int id, int customerId);
    }
}