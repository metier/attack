﻿using Metier.Phoenix.Api.Reporting.Reports;

namespace Metier.Phoenix.Api.Reporting
{
    public interface IReportFileProvider
    {
        byte[] GetReportFile(ReportDefinition definition);
    }
}