﻿using System;

namespace Metier.Phoenix.Api.Reporting.Utilities
{
    public static class ProgressHelper
    {
        public static string GetProgress(int numberOfLessons, int numberOfCompletedLessons)
        {
            return string.Format("{0}/{1} ({2}%)",
                numberOfCompletedLessons,
                numberOfLessons,
                GetPercentage(numberOfCompletedLessons, numberOfLessons));
        }

        private static int GetPercentage(int complete, int total)
        {
            return complete == 0 ? 0 : (int) Math.Round((double) (100*complete)/total);
        }
    }
}