﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Reporting.Reports;
using Metier.Phoenix.Api.Reporting.Reports.ActivityParticipants;
using Metier.Phoenix.Api.Reporting.Reports.AllParticipations;
using Metier.Phoenix.Api.Reporting.Reports.HubspotParticipants;
using Metier.Phoenix.Api.Reporting.Reports.CustomerSpecific.Jernbaneverket;
using Metier.Phoenix.Api.Reporting.Reports.Exams;
using Metier.Phoenix.Api.Reporting.Reports.OrdersForInvoicing;
using Metier.Phoenix.Api.Reporting.Reports.ParticipationsPrActivity;
using Metier.Phoenix.Api.Reporting.Reports.ParticipationsStatusPrActivity;
using Metier.Phoenix.Api.Reporting.Reports.PotentialParticipations;
using Metier.Phoenix.Api.Reporting.Reports.PrepaidClassrooms;
using Metier.Phoenix.Api.Reporting.Reports.ProductCourseStatuses;
using Metier.Phoenix.Api.Reporting.Reports.ProgressStatuses;
using Metier.Phoenix.Api.Reporting.Reports.Questions;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats.Excel;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats.Csv;
using Metier.Phoenix.Core.Data.ViewEntities.Reports;

namespace Metier.Phoenix.Api.Reporting
{
    public class ReportFileProvider : IReportFileProvider
    {
        private readonly IExcelFactory _excelFactory;
        private readonly ICsvFactory _csvFactory;
        private readonly IReportProvider<ProgressStatusesReportItem> _progressStatusesReportProvider;
        private readonly IReportProvider<ActivityParticipantsReportItem> _activityParticipantsReportProvider;
        private readonly IReportProvider<ProductCourseStatusReportItem> _productCourseStatusReportProvider;
        private readonly IReportProvider<ParticipationStatusPrActivityReportItem> _participationStatusPrActivityReportProvider;
        private readonly IReportProvider<PotentialParticipationReportItem> _potentialParticipationsReportProvider;
        private readonly IReportProvider<AllParticipationReportItem> _allParticipationsReportProvider;
        private readonly IReportProvider<HubspotParticipantReportItem> _hubspotParticipantReportProvider;
        private readonly IReportProvider<ParticipationsPrActivityReportItem> _allParticipationsPrActivityReportProvider;
        private readonly IReportProvider<PrepaidClassroomsReportItem> _prepaidClassroomsReportProvider;
        private readonly IReportProvider<OrdersForInvoicingReportItem> _ordersForInvoicingReportProvider;
        private readonly IReportProvider<QuestionsReportItem> _questionsReportProvider;
        private readonly IReportProvider<ExamsReportItem> _examsReportProvider;
        private readonly IReportProvider<SafetyCourseParticipantsReportItem> _safetyCourseParticipantsReportProvider;

        public ReportFileProvider(IExcelFactory excelFactory,
                              ICsvFactory csvFactory,
                              IReportProvider<ProgressStatusesReportItem> progressStatusesReportProvider,
                              IReportProvider<ActivityParticipantsReportItem> activityParticipantsReportProvider,
                              IReportProvider<ProductCourseStatusReportItem> productCourseStatusReportProvider,
                              IReportProvider<ParticipationStatusPrActivityReportItem> participationStatusPrActivityReportProvider,
                              IReportProvider<PotentialParticipationReportItem> potentialParticipationsReportProvider,
                              IReportProvider<AllParticipationReportItem> allParticipationsReportProvider,
                              IReportProvider<HubspotParticipantReportItem> hubspotParticipantReportProvider,
                              IReportProvider<ParticipationsPrActivityReportItem> allParticipationsPrActivityReportProvider,
                              IReportProvider<OrdersForInvoicingReportItem> ordersForInvoicingReportProvider,
                              IReportProvider<QuestionsReportItem> questionsReportProvider,
                              IReportProvider<ExamsReportItem> examsReportProvider,
                              IReportProvider<PrepaidClassroomsReportItem> prepaidClassroomsReportProvider,
                              IReportProvider<SafetyCourseParticipantsReportItem> safetyCourseParticipantsReportProvider
            )
        {
            _excelFactory = excelFactory;
            _csvFactory = csvFactory;
            _progressStatusesReportProvider = progressStatusesReportProvider;
            _activityParticipantsReportProvider = activityParticipantsReportProvider;
            _productCourseStatusReportProvider = productCourseStatusReportProvider;
            _participationStatusPrActivityReportProvider = participationStatusPrActivityReportProvider;
            _potentialParticipationsReportProvider = potentialParticipationsReportProvider;
            _allParticipationsReportProvider = allParticipationsReportProvider;
            _hubspotParticipantReportProvider = hubspotParticipantReportProvider;
            _allParticipationsPrActivityReportProvider = allParticipationsPrActivityReportProvider;
            _prepaidClassroomsReportProvider = prepaidClassroomsReportProvider;
            _ordersForInvoicingReportProvider = ordersForInvoicingReportProvider;
            _questionsReportProvider = questionsReportProvider;
            _examsReportProvider = examsReportProvider;
            _safetyCourseParticipantsReportProvider = safetyCourseParticipantsReportProvider;
        }

        public byte[] GetReportFile(ReportDefinition definition)
        {
            switch (definition.Type)
            {
                case ReportTypes.ProgressStatuses:
                    return GetFile(_progressStatusesReportProvider.GetReport(definition), definition);
                case ReportTypes.ActivityParticipants:
                    return GetFile(_activityParticipantsReportProvider.GetReport(definition), definition);
                case ReportTypes.ProductCourseStatuses:
                    return GetFile(_productCourseStatusReportProvider.GetReport(definition), definition);
                case ReportTypes.ParticipationStatusPrActivity:
                    return GetFile(_participationStatusPrActivityReportProvider.GetReport(definition), definition);
                case ReportTypes.PotentialParticipations:
                    return GetFile(_potentialParticipationsReportProvider.GetReport(definition), definition);
                case ReportTypes.AllParticipations:
                    return GetFile(_allParticipationsReportProvider.GetReport(definition), definition);
                case ReportTypes.AllParticipationsPrActivity:
                    return GetFile(_allParticipationsPrActivityReportProvider.GetReport(definition), definition);
                case ReportTypes.HubspotParticipants:
                    return GetFile(_hubspotParticipantReportProvider.GetReport(definition), definition);
                case ReportTypes.PrepaidClassrooms:
                    return GetFile(_prepaidClassroomsReportProvider.GetReport(definition), definition);
                case ReportTypes.OrdersForInvoicing:
                    return GetFile(_ordersForInvoicingReportProvider.GetReport(definition), definition);
                case ReportTypes.Questions:
                    return GetFile(_questionsReportProvider.GetReport(definition), definition);
                case ReportTypes.Exams:
                    return GetFile(_examsReportProvider.GetReport(definition), definition);
                case ReportTypes.CustomerSpecific_JBV_SafetyCourseParticipants:
                    return GetFile(_safetyCourseParticipantsReportProvider.GetReport(definition), definition);
                default:
                    return null;
            }
        }
        
        private byte[] GetFile<T>(IEnumerable<T> report, ReportDefinition definition)
        {
            switch (definition.FileFormat)
            {
                case FileFormats.Excel:
                    return _excelFactory.CreateDocument(report);
                case FileFormats.Csv:
                    return _csvFactory.CreateDocument(report, definition.EncloseCsvValuesInApostrophes);
                default:
                    return _excelFactory.CreateDocument(report);
            }
        }
    }
}