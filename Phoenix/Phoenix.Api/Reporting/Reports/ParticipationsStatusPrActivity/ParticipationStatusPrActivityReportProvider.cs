﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Reporting.Utilities;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.ViewEntities.Reports;

namespace Metier.Phoenix.Api.Reporting.Reports.ParticipationsStatusPrActivity
{
    public class ParticipationStatusPrActivityReportProvider : IReportProvider<ParticipationStatusPrActivityReportItem>
    {
        private readonly MetierLmsContext _context;
        private readonly ICustomersProvider _customersProvider;

        public ParticipationStatusPrActivityReportProvider(MetierLmsContext context, ICustomersProvider customersProvider)
        {
            _context = context;
            _customersProvider = customersProvider;
            ConfigureMapping();
        }

        public IEnumerable<ParticipationStatusPrActivityReportItem> GetReport(ReportDefinition reportDefinition)
        {
            var participationStatusPrActivityReportDefinition = reportDefinition as ParticipationStatusPrActivityReportDefinition;
            if (participationStatusPrActivityReportDefinition == null)
            {
                throw new ArgumentNullException("reportDefinition");
            }
            var report = _context.ParticipationsStatusPrActivity.AsNoTracking();
            if (participationStatusPrActivityReportDefinition.CustomerIds.Any())
            {
                var customerIds = participationStatusPrActivityReportDefinition.CustomerIds.SelectMany(id => _customersProvider.GetChildrenIds(id, true));
                report = report.Where(p => customerIds.Contains(p.CustomerId));
            }

            if (participationStatusPrActivityReportDefinition.ProductIds.Any())
            {
                report = report.Where(p => participationStatusPrActivityReportDefinition.ProductIds.Contains(p.ProductId));
            }

            if (participationStatusPrActivityReportDefinition.FromDate.HasValue)
            {
                var fromDate = participationStatusPrActivityReportDefinition.FromDate.Value;

                if (participationStatusPrActivityReportDefinition.ToDate.HasValue)
                {
                    var toDate = participationStatusPrActivityReportDefinition.ToDate.Value;
                    report = report.Where(p =>
                         ( 
                            DbFunctions.CreateDateTime(p.EnrollmentDate.Year, p.EnrollmentDate.Month, p.EnrollmentDate.Day, 0, 0, 0) >= fromDate
                         &&
                            DbFunctions.CreateDateTime(p.EnrollmentDate.Year, p.EnrollmentDate.Month, p.EnrollmentDate.Day, 0, 0, 0) <= toDate
                         ));
                }
                else
                {
                    report = report.Where(p => DbFunctions.CreateDateTime(p.EnrollmentDate.Year, p.EnrollmentDate.Month, p.EnrollmentDate.Day, 0, 0, 0) >= fromDate);    
                }
                
            }
            if (participationStatusPrActivityReportDefinition.ToDate.HasValue && !participationStatusPrActivityReportDefinition.FromDate.HasValue)
            {
                var toDate = participationStatusPrActivityReportDefinition.ToDate.Value;
                report = report.Where(p => DbFunctions.CreateDateTime(p.EnrollmentDate.Year, p.EnrollmentDate.Month, p.EnrollmentDate.Day, 0, 0, 0) <= toDate);
            }

            if (participationStatusPrActivityReportDefinition.IsArbitrary.HasValue)
            {
                report = report.Where(r => r.IsArbitrary == participationStatusPrActivityReportDefinition.IsArbitrary.Value);
            }
            
            return report.Select(Map).ToList();
        }

        private void ConfigureMapping()
        {
            var map = Mapper.CreateMap<ParticipationStatusPrActivity, ParticipationStatusPrActivityReportItem>();
            map.ForMember(a => a.CourseStatus, m => m.MapFrom(s => ScormLessonStatus.GetDescription(s.CourseStatus)));
            map.ForMember(a => a.CourseProgress, m => m.MapFrom(s => GetProgress(s.NumberOfLessons, s.NumberOfCompletedLessons)));
            map.ForMember(pi => pi.CustomerType, opt => opt.MapFrom(p => ToCustomerType(p.IsArbitrary)));
        }

        private string GetProgress(int? numberOfLessons, int? numberOfCompletedLessons)
        {
            return !numberOfLessons.HasValue ? "" : ProgressHelper.GetProgress(numberOfLessons.Value, numberOfCompletedLessons.GetValueOrDefault());
        }

        private string ToCustomerType(bool source)
        {
            return source ? "Arbitrary" : "Corporate";
        }

        private ParticipationStatusPrActivityReportItem Map(ParticipationStatusPrActivity participationStatusPrActivity)
        {
            var mapped =  Mapper.Map<ParticipationStatusPrActivity, ParticipationStatusPrActivityReportItem>(participationStatusPrActivity);
            return mapped;
        }
    }
}