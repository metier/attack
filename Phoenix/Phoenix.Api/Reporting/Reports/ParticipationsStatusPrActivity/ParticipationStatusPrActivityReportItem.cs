﻿using System;
using Metier.Phoenix.Core.Business.DocumentProduction.Attributes;

namespace Metier.Phoenix.Api.Reporting.Reports.ParticipationsStatusPrActivity
{
    public class ParticipationStatusPrActivityReportItem
    {
        public string Distributor { get; set; }
        public string Customer { get; set; }
        public string ProductNumber { get; set; }
        public string ArticleNumber { get; set; }
        public string ContentType { get; set; }
        public string ProductTitle { get; set; }
        public string ActivitySetTitle { get; set; }
        public string ActivityTitle { get; set; }

        // User
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? ExpiryDate { get; set; }

        // Participant info elements
        public string CustomField1 { get; set; }
        public string CustomField2 { get; set; }
        public string EmployeeId { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string Supervisor { get; set; }

        public string ConfirmationEmail { get; set; }
        public DateTime EnrollmentDate { get; set; }
        public DateTimeOffset? StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public string Status { get; set; }
        [DisplayName("Course performance")]
        public string CourseStatus { get; set; }
        public string NumberOfLessons { get; set; }

        [DisplayName("Course Progress")]
        public string CourseProgress { get; set; }

        [DisplayName("Customer type")]
        public string CustomerType { get; set; }
    }
}