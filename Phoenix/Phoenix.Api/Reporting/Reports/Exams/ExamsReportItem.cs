﻿using System;
using Metier.Phoenix.Core.Business.DocumentProduction.Attributes;

namespace Metier.Phoenix.Api.Reporting.Reports.Exams
{
    public class ExamsReportItem
    {
        [DisplayName("Activity Title")]
        public string ActivityTitle { get; set; }

        [DisplayName("Activity Set Title")]
        public string ActivitySetTitle { get; set; }
        
        public string ECTS { get; set; }
        [DisplayName("User ID")]
        public int UserId { get; set; }
        
        [DisplayName("User Name")]
        public string UserName { get; set; }
        
        [DisplayName("E-Mail")]
        public string Email { get; set; }
        
        [DisplayName("First Name")]
        public string FirstName { get; set; }
        
        [DisplayName("Last Name")]
        public string LastName { get; set; }
        
        [DisplayName("Customer")]
        public string CustomerName { get; set; }
        
        [DisplayName("Exam Date")]
        public DateTime ExamDate { get; set; }

        [DisplayName("Educational Partner")]
        public string EducationalPartner { get; set; }

        [DisplayName("E Questions")]
        public int ExamNumberOfQuestions { get; set; }
        
        [DisplayName("E Answers")]
        public int ExamNumberOfAnswers { get; set; }
        
        [DisplayName("E Correct")]
        public int ExamNumberOfCorrect { get; set; }
        
        [DisplayName("E Points")]
        public int ExamNumberOfPoints { get; set; }
        
        [DisplayName("E Score")]
        public string ExamScorePercentage { get; set; }

        [DisplayName("E Grade")]
        public string ExamGrade { get; set; }
        
        [DisplayName("Question #")]
        public int QuestionNumber { get; set; }

        [DisplayName("Q Difficulty")]
        public string Difficulty { get; set; }

        [DisplayName("Q Versions")]
        public string QuestionVersions { get; set; }
        
        [DisplayName("Question")]
        public string Question { get; set; }
        
        [DisplayName("Answer")]
        public string Answer { get; set; }
        
        [DisplayName("Q Points")]
        public int QuestionPoints { get; set; }
        
        [DisplayName("Question ID")]
        public int QuestionId { get; set; }
        
        [DisplayName("Answer ID")]
        public long AnswerId { get; set; }
    }
}