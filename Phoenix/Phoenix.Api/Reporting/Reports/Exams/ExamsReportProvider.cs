﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Facade.Account;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Api.Reporting.Reports.Exams
{
    public class ExamsReportProvider : IReportProvider<ExamsReportItem>
    {
        private readonly MetierLmsContext _context;
        private readonly IAccountsProvider _accountsProvider;

        public ExamsReportProvider(MetierLmsContext context, 
                                   IAccountsProvider accountsProvider)
        {
            _context = context;
            _accountsProvider = accountsProvider;
        }

        public IEnumerable<ExamsReportItem> GetReport(ReportDefinition reportDefinition)
        {
            var examsReportDefinition = reportDefinition as ExamsReportDefinition;
            if (examsReportDefinition == null)
            {
                throw new ArgumentNullException("reportDefinition");
            }
            var query = _context.Participants.Where(p => !p.IsDeleted)
                .Join(_context.Activities, p => p.ActivityId, a => a.Id, (p, a) => new { Participant = p, Activity = a, })
                .Join(_context.ActivitySets, x => x.Participant.ActivitySetId, aset => aset.Id, (x, aset) => new { x.Participant, x.Activity, ActivitySet = aset })
                .Join(_context.CustomerArticles, x => x.Activity.CustomerArticleId, ca => ca.Id, (x, ca) => new { x.Participant, x.Activity, x.ActivitySet, CustomerArticle = ca })
                .Join(_context.Articles.OfType<ExamArticle>(), x => x.CustomerArticle.ArticleCopyId, a => a.Id, (x, a) => new { x.Participant, x.Activity, x.ActivitySet, x.CustomerArticle, Article = a })
                .Where(x => x.Article.ArticleTypeId == (int)ArticleTypes.MultipleChoice || x.Article.ArticleTypeId == (int)ArticleTypes.InternalCertification)
                .Join(_context.Products, x => x.Article.ProductId, p => p.Id, (x, p) => new { x.Participant, x.Activity, x.ActivitySet, x.CustomerArticle, x.Article, Product = p })
                .Join(_context.Customers, x => x.CustomerArticle.CustomerId, c => c.Id, (x, c) => new { x.Participant, x.Activity, x.ActivitySet, x.Article, x.Product, Customer = c })
                .Join(_context.Exams, x => x.Participant.Id, e => e.ParticipantId, (x, e) => new { x.Participant, x.Activity, x.ActivitySet, x.Article, x.Product, x.Customer, Exam = e, e.Questions, e.Answers })
                .Where(x => x.Exam.IsSubmitted);

            if (examsReportDefinition.CustomerIds.Any())
            {
                query = query.Where(x => examsReportDefinition.CustomerIds.Contains(x.Customer.Id));
            }
            if (examsReportDefinition.IsArbitrary.HasValue)
            {
                query = query.Where(x => x.Customer.IsArbitrary == examsReportDefinition.IsArbitrary.Value);
            }
            if (examsReportDefinition.ProductIds.Any())
            {
                query = query.Where(x => examsReportDefinition.ProductIds.Contains(x.Product.Id));
            }
            if (examsReportDefinition.FromDate.HasValue)
            {
                query = query.Where(x => x.Exam.Created >= examsReportDefinition.FromDate.Value);
            }
            if (examsReportDefinition.ToDate.HasValue)
            {
                query = query.Where(x => x.Exam.Created <= examsReportDefinition.ToDate.Value);
            }

            // Execute query
            var participantExamData = query.ToList();
            
            // Load additional data
            var userAccountCache = new Dictionary<int, UserAccount>();
            var educationalPartnerResourceCache = new Dictionary<int, Resource>();
            foreach (var data in participantExamData)
            {
                if (!userAccountCache.ContainsKey(data.Participant.UserId))
                {
                    var userAccount = _accountsProvider.GetByUserId(data.Participant.UserId);
                    userAccountCache.Add(data.Participant.UserId, userAccount);
                }
                if (data.Activity.EducationalPartnerResourceId.HasValue && 
                    !educationalPartnerResourceCache.ContainsKey(data.Activity.EducationalPartnerResourceId.Value))
                {
                    var resource = _context.Resources.First(r => r.Id == data.Activity.EducationalPartnerResourceId.Value);
                    educationalPartnerResourceCache.Add(data.Activity.EducationalPartnerResourceId.Value, resource);
                }

                foreach (var question in data.Questions)
                {
                    _context.Entry(question).Collection(q => q.QuestionOptions).Load();
                    _context.Entry(question).Collection(q => q.Rcos).Load();
                }
                foreach (var answer in data.Answers)
                {
                    _context.Entry(answer).Collection(q => q.OptionsAnswers).Load();
                }
            }

            var result = new List<ExamsReportItem>();
            foreach (var row in participantExamData)
            {
                var questionAnswers = row.Questions.Select(q => new { Question = q, Answer = row.Answers.First(a => a.QuestionId == q.Id) }).ToList();
                var questionNumber = 1;
                foreach (var questionAnswer in questionAnswers)
                {
                    var reportItem = new ExamsReportItem();
                    reportItem.ActivitySetTitle = row.ActivitySet.Name;
                    reportItem.ActivityTitle = row.Activity.Name;
                    reportItem.ECTS = row.Article.ECTS;
                    reportItem.UserId = row.Participant.UserId;
                    reportItem.UserName = userAccountCache[row.Participant.UserId].Username;
                    reportItem.Email = userAccountCache[row.Participant.UserId].Email;
                    reportItem.FirstName = userAccountCache[row.Participant.UserId].User.FirstName;
                    reportItem.LastName = userAccountCache[row.Participant.UserId].User.LastName;
                    reportItem.CustomerName = row.Customer.Name;
                    reportItem.ExamDate = row.Exam.Created;

                    reportItem.ExamNumberOfQuestions = row.Questions.Count;
                    reportItem.ExamNumberOfAnswers = questionAnswers.Count(x => IsAnswered(x.Question, x.Answer));
                    reportItem.ExamNumberOfCorrect = questionAnswers.Count(x => IsCorrect(x.Question, x.Answer));
                    reportItem.ExamNumberOfPoints = row.Exam.TotalScore.GetValueOrDefault();

                    var examScorePercentage = Decimal.Divide(row.Exam.TotalScore.GetValueOrDefault(), row.Questions.Sum(q => q.Points));
                    reportItem.ExamScorePercentage = string.Format("{0:P0}", examScorePercentage);
                    reportItem.ExamGrade = row.Participant.ExamGrade;
                    if (row.Activity.EducationalPartnerResourceId.HasValue)
                    {
                        reportItem.EducationalPartner = educationalPartnerResourceCache[row.Activity.EducationalPartnerResourceId.Value].Name;    
                    }
                    
                    reportItem.QuestionNumber = questionNumber;
                    reportItem.Difficulty = questionAnswer.Question.Difficulty.ToString();
                    reportItem.QuestionVersions = string.Join(",", questionAnswer.Question.Rcos.Select(r => r.Version));
                    reportItem.Question = questionAnswer.Question.Text;
                    reportItem.Answer = GetAnswer(questionAnswer.Question, questionAnswer.Answer);
                    reportItem.QuestionPoints = IsCorrect(questionAnswer.Question, questionAnswer.Answer) ? questionAnswer.Question.Points : 0;
                    reportItem.QuestionId = questionAnswer.Question.Id;
                    reportItem.AnswerId = questionAnswer.Answer.Id;

                    questionNumber++;

                    result.Add(reportItem);
                }
            }
            return result;
        }

        private string GetAnswer(Question question, Answer answer)
        {
            if (question.Type == QuestionTypes.TrueFalse)
            {
                if (answer.BoolAnswer.HasValue)
                {
                    return answer.BoolAnswer.Value ? "True" : "False";
                }
                return "";
            }
            return string.Join(";",answer.OptionsAnswers.Select(o => o.Text));
        }

        private bool IsAnswered(Question question, Answer answer)
        {
            if (question.Type == QuestionTypes.TrueFalse)
            {
                return answer.BoolAnswer.HasValue;
            }
            return answer.OptionsAnswers.Any();
        }

        private bool IsCorrect(Question question, Answer answer)
        {
            if (question.Type == QuestionTypes.TrueFalse)
            {
                return answer.BoolAnswer.HasValue && question.CorrectBoolAnswer.HasValue && answer.BoolAnswer.Value == question.CorrectBoolAnswer.Value;
            }
            return answer.OptionsAnswers.Any() && answer.OptionsAnswers.All(o => o.IsCorrect);
        }
    }
}