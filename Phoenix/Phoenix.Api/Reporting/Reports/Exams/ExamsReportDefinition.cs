﻿using System;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats;

namespace Metier.Phoenix.Api.Reporting.Reports.Exams
{
    public class ExamsReportDefinition : ReportDefinition
    {
        private readonly bool? _isArbitrary;
        private readonly DateTime? _fromDate;
        private readonly DateTime? _toDate;
        private readonly int[] _customerIds;
        private readonly int[] _productIds;

        public ExamsReportDefinition(int[] customerIds, int[] productIds, bool? isArbitrary, DateTime? fromDate, DateTime? toDate, FileFormats fileFormat)
            : base(ReportTypes.Exams, fileFormat)
        {
            _isArbitrary = isArbitrary;
            if (fromDate.HasValue)
            {
                _fromDate = fromDate.Value.Date;    
            }
            if (toDate.HasValue)
            {
                _toDate = toDate.Value.Date.AddDays(1);    
            }
            _customerIds = customerIds ?? new int[] { };
            _productIds = productIds ?? new int[] { };
        }

        public bool? IsArbitrary
        {
            get { return _isArbitrary; }
        }
        
        public int[] CustomerIds
        {
            get { return _customerIds; }
        }

        public int[] ProductIds
        {
            get { return _productIds; }
        }

        public DateTime? FromDate
        {
            get { return _fromDate; }
        }

        public DateTime? ToDate
        {
            get { return _toDate; }
        }
    }
}