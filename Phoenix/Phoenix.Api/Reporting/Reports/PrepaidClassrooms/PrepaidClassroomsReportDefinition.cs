﻿using System;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats;

namespace Metier.Phoenix.Api.Reporting.Reports.PrepaidClassrooms
{
    public class PrepaidClassroomsReportDefinition : ReportDefinition
    {
        private readonly int[] _distributorIds;
        private readonly DateTime? _fromDate;
        private readonly DateTime? _toDate;

        public PrepaidClassroomsReportDefinition(int[] distributorIds, DateTime? fromDate, DateTime? toDate, FileFormats fileFormat) 
            : base(ReportTypes.PrepaidClassrooms, fileFormat)
        {
            _distributorIds = distributorIds ?? new int[] { };
            _fromDate = fromDate.HasValue ? fromDate : DateTime.MinValue;
            _toDate = toDate;
        }

        public int[] DistributorIds
        {
            get { return _distributorIds; }
        }

        public DateTime? FromDate
        {
            get { return _fromDate; }
        }

        public DateTime? ToDate
        {
            get { return _toDate; }
        }
    }
}