﻿using System;

namespace Metier.Phoenix.Api.Reporting.Reports.PrepaidClassrooms
{
    public class PrepaidClassroomsReportItem
    {
        public string CustomerName { get; set; }
        public string AccountNumber { get; set; }
        public string Article { get; set; }
        public string WorkOrder { get; set; }
        public string Course { get; set; }
        public string OrderTitle { get; set; }
        public string Distributor { get; set; }

        public DateTime? ClassroomDate { get; set; }
        public DateTime? OrderDate { get; set; }

        public Decimal? Amount { get; set; }

        public int? InvoiceNumber { get; set; }
        public string Currency { get; set; }
    
    }
}