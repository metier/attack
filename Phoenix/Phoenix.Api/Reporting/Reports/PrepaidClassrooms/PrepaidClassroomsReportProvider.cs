﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.ViewEntities.Reports;

namespace Metier.Phoenix.Api.Reporting.Reports.PrepaidClassrooms
{
    public class PrepaidClassroomsReportProvider : IReportProvider<PrepaidClassroomsReportItem>
    {
        private readonly MetierLmsContext _context;

        public PrepaidClassroomsReportProvider(MetierLmsContext context)
        {
            _context = context;
            ConfigureMapping();
        }

        public IEnumerable<PrepaidClassroomsReportItem> GetReport(ReportDefinition reportDefinition)
        {
            var prepaidClassroomsReportDefinition = reportDefinition as PrepaidClassroomsReportDefinition;
            if (prepaidClassroomsReportDefinition == null)
            {
                throw new ArgumentNullException("reportDefinition");
            }

            var report = _context.PrepaidClassrooms.AsNoTracking();

            if (prepaidClassroomsReportDefinition.DistributorIds.Any())
            {
                report = report.Where(pc => prepaidClassroomsReportDefinition.DistributorIds.Contains(pc.DistributorId));
            }

            if (prepaidClassroomsReportDefinition.FromDate.HasValue)
            {
                var filterDate = prepaidClassroomsReportDefinition.FromDate.Value.Date;
                report = report.Where(r => r.OrderDate >= filterDate);
            }
            if (prepaidClassroomsReportDefinition.ToDate.HasValue)
            {
                var filterDate = prepaidClassroomsReportDefinition.ToDate.Value.Date + TimeSpan.FromDays(1);
                report = report.Where(r => r.OrderDate < filterDate);
            }
            
            
            return report.Select(Map).ToList();
        }

        private void ConfigureMapping()
        {
            Mapper.CreateMap<PrepaidClassroom, PrepaidClassroomsReportItem>();
        }

        private PrepaidClassroomsReportItem Map(PrepaidClassroom prepaidClassroom)
        {
            return Mapper.Map<PrepaidClassroom, PrepaidClassroomsReportItem>(prepaidClassroom);
        }
    }    
}