﻿using System;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats;

namespace Metier.Phoenix.Api.Reporting.Reports.HubspotParticipants
{
    public class HubspotParticipantReportDefinition : ReportDefinition
    {
        private readonly bool? _isArbitrary;
        private readonly int[] _distributorIds;
        private readonly int[] _customerIds;
        private readonly string[] _productIds;
        private readonly DateTime? _fromDate;
        private readonly DateTime? _toDate;

        public HubspotParticipantReportDefinition(int[] customerIds, int[] distributorIds, string[] productIds, DateTime? fromDate, DateTime? toDate, bool? isArbitrary, FileFormats fileFormat)
            : base(ReportTypes.HubspotParticipants, fileFormat)
        {
            _isArbitrary = isArbitrary;
            _customerIds = customerIds ?? new int[] { };
            _distributorIds = distributorIds ?? new int[] { };
            _productIds = productIds ?? new string[] { };
            _fromDate = fromDate.HasValue ? fromDate : DateTime.MinValue;
            _toDate = toDate;
			this.EncloseCsvValuesInApostrophes = false;
        }

        public int[] DistributorIds
        {
            get { return _distributorIds; }
        }

        public int[] CustomerIds
        {
            get { return _customerIds; }
        }

        public string[] ProductIds
        {
            get { return _productIds; }
        }

        public bool? IsArbitrary
        {
            get { return _isArbitrary; }
        }

        public DateTime? FromDate
        {
            get { return _fromDate; }
        }

        public DateTime? ToDate
        {
            get { return _toDate; }
        }
    }
}