﻿using System;
using Metier.Phoenix.Core.Business.DocumentProduction.Attributes;

namespace Metier.Phoenix.Api.Reporting.Reports.HubspotParticipants
{
    public class HubspotParticipantReportItem
    {
        //public int UserId { get; set; }
        //public int CustomerId { get; set; }
        public string CoursesEnrolled { get; set; }
        public string CoursesCompleted { get; set; }
        //public string Distributor { get; set; }
        //public string ProductPath { get; set; }
        //public string CourseLink { get; set; }

        //public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
		public string LifeCycleStage { get; set; } = "Customer";

		//public string Customer { get; set; }

		//[DisplayName("Customer type")]
		//public string CustomerType { get; set; }        

		//public bool IsArbitrary { get; set; }
		//public DateTime? FirstEnrollmentDate { get; set; }
		//public DateTime? LastEnrollmentDate { get; set; }
	}
}