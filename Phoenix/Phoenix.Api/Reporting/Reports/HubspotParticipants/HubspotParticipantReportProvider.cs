﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.ViewEntities.Reports;

namespace Metier.Phoenix.Api.Reporting.Reports.HubspotParticipants
{
    public class HubspotParticipantReportProvider : IReportProvider<HubspotParticipantReportItem>
    {
        private readonly MetierLmsContext _context;
        private readonly ICustomersProvider _customersProvider;
        private HubspotParticipantReportDefinition _reportDefinition;

        public HubspotParticipantReportProvider(MetierLmsContext context, ICustomersProvider customersProvider)
        {
            context.Database.CommandTimeout = 600;
            _context = context;
            _customersProvider = customersProvider;
            ConfigureMapping();
        }

        public IEnumerable<HubspotParticipantReportItem> GetReport(ReportDefinition reportDefinition)
        {
            var HubspotParticipantReportDefinition = reportDefinition as HubspotParticipantReportDefinition;
            if (HubspotParticipantReportDefinition == null)
            {
                throw new ArgumentNullException("reportDefinition");
            }
            _reportDefinition = HubspotParticipantReportDefinition;
            var customerIds = HubspotParticipantReportDefinition.CustomerIds.SelectMany(id => _customersProvider.GetChildrenIds(id, true)).ToArray();

            var report = _context.HubspotParticipants.AsNoTracking();
            if (customerIds.Any())
            {
                report = report.Where(r => customerIds.Contains(r.CustomerId));
            }
            if (HubspotParticipantReportDefinition.DistributorIds.Any())
            {
                report = report.Where(r => HubspotParticipantReportDefinition.DistributorIds.Contains(r.DistributorId));
            }
            if (HubspotParticipantReportDefinition.ProductIds.Any())
            {
                var ids = HubspotParticipantReportDefinition.ProductIds.ToList();
                //if (ids.Contains("PM24"))
                //{
                //    if (!ids.Contains("PM22")) ids.Add("PM22");
                //    if (!ids.Contains("PM23")) ids.Add("PM23");
                //}
                //else if (ids.Contains("PM22") || ids.Contains("PM23"))
                //    ids.Add("PM24");

                report = report.Where(r => r.CoursesEnrolled != null && ids.Any(s => r.CoursesEnrolled.Contains(s) || (s == "PM24" && r.CoursesEnrolled.Contains("PM22") && r.CoursesEnrolled.Contains("PM23"))));                

            }
            if (HubspotParticipantReportDefinition.IsArbitrary.HasValue)
            {
                report = report.Where(r => r.IsArbitrary == HubspotParticipantReportDefinition.IsArbitrary.Value);
            }
            if (HubspotParticipantReportDefinition.FromDate.HasValue)
            {
                var filterDate = HubspotParticipantReportDefinition.FromDate.Value.Date;
                report = report.Where(r => r.LastEnrollmentDate >= filterDate);
            }
            if (HubspotParticipantReportDefinition.ToDate.HasValue)
            {
                var filterDate = HubspotParticipantReportDefinition.ToDate.Value.Date + TimeSpan.FromDays(1);
                report = report.Where(r => r.LastEnrollmentDate < filterDate);
            }

            return report.Select(Map).ToList();
        }

        
        private void ConfigureMapping()
        {
            Mapper.CreateMap<HubspotParticipant, HubspotParticipantReportItem>()
                //.ForMember(pi => pi.CoursesEnrolled, opt => opt.MapFrom(p => PrepareCoursesEnrolled(p.CoursesEnrolled)))
                //.ForMember(pi => pi.CustomerType, opt => opt.MapFrom(p => ToCustomerType(p.IsArbitrary)))
                ;
        }

        private string ToCustomerType(bool source)
        {
            return source ? "Arbitrary" : "Corporate";
        }        
        private string PrepareCourses(string source)
        {
            if (source == null) return null;
            if (_reportDefinition != null && _reportDefinition.ProductIds != null && _reportDefinition.ProductIds.Length > 0)
            {
                var originalSource = source.ToUpper().Split(';').ToList();
                var coursesEnrolled = new List<string>();
                source = ";";
                _reportDefinition.ProductIds.ToList().ForEach(x =>
                {
                    if (originalSource.Contains(x.ToUpper()))
                    {
                        if (!coursesEnrolled.Contains(x)) coursesEnrolled.Add(x);
                    }
                    else if (x == "PM24")
                    {
                        if ((originalSource.Contains("PM24") || originalSource.Contains("PM22")) && !coursesEnrolled.Contains("PM22")) coursesEnrolled.Add("PM22");
                        if ((originalSource.Contains("PM24") || originalSource.Contains("PM23")) && !coursesEnrolled.Contains("PM23")) coursesEnrolled.Add("PM23");                        
                    }

                });
                source = ";" + string.Join(";", coursesEnrolled) + (coursesEnrolled.Count > 0 ? ";":"");
            }
            else
            {
                if (source == null) return null;
                if (source.Length > 0) source = ";" + source;
            }

            source = source.Replace(" ", "");
            //if (source.Contains(";PM24"))
            //{
            //    source = source.Replace(";PM24", "");
            //    if (!source.Contains(";PM23")) source = ";PM23" + source;
            //    if (!source.Contains(";PM22")) source = ";PM22" + source;                
            //}
            return source;
        }

        private HubspotParticipantReportItem Map(HubspotParticipant HubspotParticipant)
        {
            var item = Mapper.Map<HubspotParticipant, HubspotParticipantReportItem>(HubspotParticipant);
            item.CoursesEnrolled = PrepareCourses(item.CoursesEnrolled);
            item.CoursesCompleted = PrepareCourses(item.CoursesCompleted);
            return item;
        }
    }
}