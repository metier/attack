﻿using System;
using Metier.Phoenix.Core.Business.DocumentProduction.Attributes;

namespace Metier.Phoenix.Api.Reporting.Reports.ParticipationsPrActivity
{
    public class ParticipationsPrActivityReportItem
    {

        public string Distributor { get; set; }
        public string Customer { get; set; }
        public string ParentOrg { get; set; }
        public string ProductPath { get; set; }
        public string ArticleNumber { get; set; }
        public string ContentType { get; set; }
        public string CourseLink { get; set; }
        public string ArticleName { get; set; }
        public string CustomerArticleName { get; set; }
        
        [DisplayName("CustArtLanguage")]
        public string CustomerArticleLanguage { get; set; }
        public string ActivityName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime? LastLoginDate { get; set; }

        [DisplayName("Custom field 1")]
        public string CustomField1 { get; set; }
        [DisplayName("Custom field 2")]
        public string CustomField2 { get; set; }
        [DisplayName("Employee ID")]
        public string EmployeeId { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string Supervisor { get; set; }
        [DisplayName("Confirmation e-mail")]
        public string ConfirmationEmail { get; set; }

        public DateTime? EnrollmentDate { get; set; }
        public DateTimeOffset? StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public string Status { get; set; }
        public string NumberOfLessons { get; set; }
        public string Points { get; set; }

        [DisplayName("Course Progress")]
        public string CourseProgress { get; set; }

        public string AccountManager { get; set; }
        public string Coordinator { get; set; }
        public string SelfEnrollmentExams { get; set; }
        public string SelfEnrollmentCourses { get; set; }
        public string UserRole { get; set; }

        [DisplayName("Customer type")]
        public string CustomerType { get; set; }
    }
}