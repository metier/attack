﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Reporting.Utilities;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.ViewEntities.Reports;

namespace Metier.Phoenix.Api.Reporting.Reports.ParticipationsPrActivity
{
    public class ParticipationsPrActivityReportProvider : IReportProvider<ParticipationsPrActivityReportItem>
    {
        private readonly MetierLmsContext _context;
        private readonly ICustomersProvider _customersProvider;

        public ParticipationsPrActivityReportProvider(MetierLmsContext context, ICustomersProvider customersProvider)
        {
            context.Database.CommandTimeout = 600;
            _context = context;
            _customersProvider = customersProvider;
            ConfigureMapping();
        }

        public IEnumerable<ParticipationsPrActivityReportItem> GetReport(ReportDefinition reportDefinition)
        {
            var participationsPrActivityReportDefinition = reportDefinition as ParticipationsPrActivityReportDefinition;
            if (participationsPrActivityReportDefinition == null)
            {
                throw new ArgumentNullException("reportDefinition");
            }

            var customerIds = participationsPrActivityReportDefinition.CustomerIds.SelectMany(id => _customersProvider.GetChildrenIds(id, true)).ToArray();

            var report = _context.ParticipationsPrActivity.AsNoTracking();
            if (customerIds.Any())
            {
                report = report.Where(r => customerIds.Contains(r.CustomerId));
            }
            if (participationsPrActivityReportDefinition.DistributorIds.Any())
            {
                report = report.Where(r => participationsPrActivityReportDefinition.DistributorIds.Contains(r.DistributorId));
            }
            if (participationsPrActivityReportDefinition.ProductIds.Any())
            {
                report = report.Where(r => participationsPrActivityReportDefinition.ProductIds.Contains(r.ProductId));
            }
            if (participationsPrActivityReportDefinition.IsArbitrary.HasValue)
            {
                report = report.Where(r => r.IsArbitrary == participationsPrActivityReportDefinition.IsArbitrary.Value);
            }
            if (participationsPrActivityReportDefinition.FromDate.HasValue)
            {
                var filterDate = participationsPrActivityReportDefinition.FromDate.Value.Date;
                report = report.Where(r => r.EnrollmentDate >= filterDate);
            }
            if (participationsPrActivityReportDefinition.ToDate.HasValue)
            {
                var filterDate = participationsPrActivityReportDefinition.ToDate.Value.Date + TimeSpan.FromDays(1);
                report = report.Where(r => r.EnrollmentDate < filterDate);
            }

            return report.Select(Map).ToList();
        }

        private string ToYesNoString(bool? source)
        {
            return source.GetValueOrDefault() ? "Yes" : "No";
        }

        private void ConfigureMapping()
        {
            Mapper.CreateMap<ParticipationPrActivity, ParticipationsPrActivityReportItem>()
                .ForMember(a => a.SelfEnrollmentExams, opt => opt.MapFrom(p => ToYesNoString(p.SelfEnrollmentExams)))
                .ForMember(a => a.SelfEnrollmentCourses, opt => opt.MapFrom(p => ToYesNoString(p.SelfEnrollmentCourses)))
                .ForMember(a => a.CourseProgress, m => m.MapFrom(s => GetProgress(s.NumberOfLessons, s.NumberOfCompletedLessons)))
                .ForMember(pi => pi.CustomerType, opt => opt.MapFrom(p => ToCustomerType(p.IsArbitrary)));
        }

        private string ToCustomerType(bool source)
        {
            return source ? "Arbitrary" : "Corporate";
        }
        private string GetProgress(int? numberOfLessons, int? numberOfCompletedLessons)
        {
            return !numberOfLessons.HasValue ? "" : ProgressHelper.GetProgress(numberOfLessons.Value, numberOfCompletedLessons.GetValueOrDefault());
        }


        private ParticipationsPrActivityReportItem Map(ParticipationPrActivity participationPrActivity)
        {
            return Mapper.Map<ParticipationPrActivity, ParticipationsPrActivityReportItem>(participationPrActivity);
        }
    }
}