﻿using System;

namespace Metier.Phoenix.Api.Reporting.Reports.OrdersForInvoicing
{
    public class OrdersForInvoicingReportItem
    {
        public string DistributorName { get; set; }
        public string CustomerName { get; set; }
        public string ArticleCode { get; set; }
        public string ActivityTitle { get; set; }
        public DateTime EarliestInvoiceDate { get; set; }
        public string CurrencyCode { get; set; }
        public decimal Amount { get; set; }
        public string OrderStatus { get; set; }
        public bool IsArbitrary { get; set; }
    }
}