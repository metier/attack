﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Api.Reporting.Reports.OrdersForInvoicing
{
    public class OrdersForInvoicingReportProvider : IReportProvider<OrdersForInvoicingReportItem>
    {
        private readonly MetierLmsContext _context;
        private readonly ICustomersProvider _customersProvider;

        public OrdersForInvoicingReportProvider(MetierLmsContext context, ICustomersProvider customersProvider)
        {
            _context = context;
            _customersProvider = customersProvider;
            ConfigureMapping();
        }

        public IEnumerable<OrdersForInvoicingReportItem> GetReport(ReportDefinition reportDefinition)
        {
            var ordersForInvoicingReportDefinition = reportDefinition as OrdersForInvoicingReportDefinition;
            if (ordersForInvoicingReportDefinition == null)
            {
                throw new ArgumentNullException("reportDefinition");
            }

            var customerIds = ordersForInvoicingReportDefinition.CustomerIds.SelectMany(id => _customersProvider.GetChildrenIds(id, true)).ToArray();

            var report = _context.OrdersForInvoicings.AsNoTracking();
            if (customerIds.Any())
            {
                report = report.Where(r => customerIds.Contains(r.CustomerId));
            }

            if (ordersForInvoicingReportDefinition.DistributorIds.Any())
            {
                report = report.Where(r => ordersForInvoicingReportDefinition.DistributorIds.Contains(r.DistributorId));
            }

            if (ordersForInvoicingReportDefinition.ProductIds.Any())
            {
                report = report.Where(r => ordersForInvoicingReportDefinition.ProductIds.Contains(r.ProductId));
            }

            if (ordersForInvoicingReportDefinition.OrderStatusId.Any())
            {
                report = report.Where(r => ordersForInvoicingReportDefinition.OrderStatusId.Contains(r.OrderStatus));
            }

            if (ordersForInvoicingReportDefinition.EarliestInvoiceDateStart.HasValue)
            {
                report = report.Where(r => r.EarliestInvoiceDate >= ordersForInvoicingReportDefinition.EarliestInvoiceDateStart.Value);
            }

            if (ordersForInvoicingReportDefinition.EarliestInvoiceDateEnd.HasValue)
            {
                report = report.Where(r => r.EarliestInvoiceDate <= ordersForInvoicingReportDefinition.EarliestInvoiceDateEnd.Value);
            }
            if (ordersForInvoicingReportDefinition.IsArbitrary.HasValue)
            {
                report = report.Where(r => r.IsArbitrary == ordersForInvoicingReportDefinition.IsArbitrary.Value);
            }


            return report.Select(Map).ToList();
        }

        private OrdersForInvoicingReportItem Map(Core.Data.ViewEntities.Reports.OrdersForInvoicing ordersForInvoicing)
        {
            return Mapper.Map<Core.Data.ViewEntities.Reports.OrdersForInvoicing, OrdersForInvoicingReportItem>(ordersForInvoicing);
        }

        private void ConfigureMapping()
        {
            Mapper.CreateMap<Core.Data.ViewEntities.Reports.OrdersForInvoicing, OrdersForInvoicingReportItem>()
                .ForMember(o => o.OrderStatus, e => e.MapFrom(o => Enum.GetName(typeof(OrderStatus), o.OrderStatus)));
        }
    }
}