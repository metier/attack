using System;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats;

namespace Metier.Phoenix.Api.Reporting.Reports.OrdersForInvoicing
{
    public class OrdersForInvoicingReportDefinition : ReportDefinition
    {
        private readonly int[] _distributorId;
        private readonly int[] _customerId;
        private readonly int[] _productId;
        private readonly int[] _orderStatusId;
        private readonly DateTime? _earliestInvoiceDateStart;
        private readonly DateTime? _earliestInvoiceDateEnd;
        private readonly bool? _isArbitrary;

        public OrdersForInvoicingReportDefinition(int[] distributorId, int[] customerId, int[] productId, int[] orderStatusId, DateTime? earliestInvoiceDateStart, DateTime? earliestInvoiceDateEnd, bool? isArbitrary, FileFormats fileFormat)
            :base(ReportTypes.OrdersForInvoicing, fileFormat)
        {
            _distributorId = distributorId ?? new int[] { };
            _customerId = customerId ?? new int[] { };
            _productId = productId ?? new int[] { };
            _orderStatusId = orderStatusId ?? new int[] { };
            if (earliestInvoiceDateStart.HasValue)
            {
                _earliestInvoiceDateStart = earliestInvoiceDateStart.Value.Date;    
            }
            if (earliestInvoiceDateEnd.HasValue)
            {
                _earliestInvoiceDateEnd = earliestInvoiceDateEnd.Value.Date.AddDays(1);    
            }
            _isArbitrary = isArbitrary;
        }

        public int[] DistributorIds
        {
            get { return _distributorId; }
        }

        public int[] CustomerIds
        {
            get { return _customerId; }
        }

        public int[] ProductIds
        {
            get { return _productId; }
        }

        public DateTime? EarliestInvoiceDateStart
        {
            get { return _earliestInvoiceDateStart; }
        }

        public DateTime? EarliestInvoiceDateEnd
        {
            get { return _earliestInvoiceDateEnd; }
        }

        public int[] OrderStatusId
        {
            get { return _orderStatusId; }
        }

        public bool? IsArbitrary
        {
            get { return _isArbitrary; }
        }
    }
}