﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Business.DocumentProduction.Attributes;

namespace Metier.Phoenix.Api.Reporting.Reports.Questions
{
    public class QuestionReportAlternative
    {
        [DisplayName("Alternative")]
        [CountListColumns]
        public string Alternative { get; set; }
    }

    public class QuestionsReportItem
    {
        public string Question { get; set; }

        public string Type { get; set; }
        
        [DisplayName("Correct TF")]
        public string CorrectBool { get; set; }
        
        [DisplayName("Correct alternative(s)")]
        public string CorrectAlternatives { get; set; }

        public IList<QuestionReportAlternative> Alternatives { get; set; }

        public string Status { get; set; }

        public string Language { get; set; }
        
        public string Products { get; set; }

        public string Versions { get; set; }

        [DisplayName("Level of difficulty")]
        public int Difficulty { get; set; }

        public string Tags { get; set; }

        public string Author { get; set; }

        [DisplayName("Customer Id")]
        public int? CustomerId { get; set; }
    }
}