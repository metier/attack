﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Api.Reporting.Reports.Questions
{
    public class QuestionsReportProvider : IReportProvider<QuestionsReportItem>
    {
        private readonly IQuestionsProvider _questionsProvider;
        private readonly MetierLmsContext _context;

        public QuestionsReportProvider(IQuestionsProvider questionsProvider, MetierLmsContext context)
        {
            _questionsProvider = questionsProvider;
            _context = context;
        }

        public IEnumerable<QuestionsReportItem> GetReport(ReportDefinition reportDefinition)
        {
            var questionsReportDefinition = reportDefinition as QuestionsReportDefinition;
            if (questionsReportDefinition == null)
            {
                throw new ArgumentNullException("reportDefinition");
            }

            var questions  =_questionsProvider.Search(questionsReportDefinition.Query,
                                                        questionsReportDefinition.TagIds,
                                                        questionsReportDefinition.ProductIds,
                                                        questionsReportDefinition.RcoIds,
                                                        questionsReportDefinition.Difficulties,
                                                        questionsReportDefinition.Statuses,
                                                        questionsReportDefinition.LanguageIds,
                                                        0,
                                                        questionsReportDefinition.CustomerIds,
                                                        questionsReportDefinition.LastUsedFromDate,
                                                        questionsReportDefinition.LastUsedToDate,
                                                        0, -1, null, null);
            var questionIds = questions.Items.Select(q => q.Id);
            var questionEntities = _context.Questions.Include(q => q.QuestionOptions)
                                                     .Include(q => q.Language)
                                                     .Where(q => questionIds.Contains(q.Id)).ToList();
            var reportItems = new List<QuestionsReportItem>();
            
            foreach (var question in questions.Items)
            {
                var questionEntity = questionEntities.First(q => q.Id == question.Id);
                var reportItem = new QuestionsReportItem
                {
                    Question = question.Text,
                    Type = question.Type,
                    CorrectBool = questionEntity.CorrectBoolAnswer.HasValue ? (questionEntity.CorrectBoolAnswer.Value ? "T" : "F") : "",
                    CorrectAlternatives = string.Join(",", questionEntity.QuestionOptions.Select((o, idx) => new { AlternativeNumber = idx + 1, o.IsCorrect }).Where(o => o.IsCorrect).Select(o => o.AlternativeNumber)),
                    Language = questionEntity.Language.Identifier,
                    Status = question.Status.ToExcelString(),
                    Products = string.Join(",", question.Products.Select(p => p.ProductPath)),
                    Versions = string.Join(",", question.Rcos.Select(r => r.Version)),
                    Difficulty = (int)question.Difficulty,
                    Tags = string.Join(",", question.Tags.Select(t => t.Text)),
                    Author = question.Author,
                    CustomerId = question.CustomerId,
                    Alternatives = questionEntity.QuestionOptions.Select(o => new QuestionReportAlternative { Alternative = o.Text}).ToList()
                };

                reportItems.Add(reportItem);
            }

            return reportItems;
        }
    }
}