﻿using System;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Api.Reporting.Reports.Questions
{
    public class QuestionsReportDefinition : ReportDefinition
    {
        private readonly string _query;
        private readonly int[] _tagIds;
        private readonly int[] _productIds;
        private readonly int[] _rcoIds;
        private readonly QuestionDifficulties[] _difficulties;
        private readonly QuestionStatuses[] _statuses;
        private readonly int[] _languageIds;
        private readonly int[] _customerIds;
        private readonly DateTime? _lastUsedFromDate;
        private readonly DateTime? _lastUsedToDate;

        public QuestionsReportDefinition(
            string query, 
            int[] tagIds, 
            int[] productIds, 
            int[] rcoIds, 
            QuestionDifficulties[] difficulties, 
            QuestionStatuses[] statuses, 
            int[] languageIds, 
            int[] customerIds, 
            DateTime? lastUsedFromDate, 
            DateTime? lastUsedToDate, 
            FileFormats fileFormat)
            : base(ReportTypes.Questions, fileFormat)
        {
            _query = query;
            _tagIds = tagIds ?? new int[] {};
            _productIds = productIds ?? new int[]{};
            _rcoIds = rcoIds  ?? new int[] { };
            _difficulties = difficulties ?? new QuestionDifficulties[] { };
            _statuses = statuses ?? new QuestionStatuses[] { };
            _languageIds = languageIds ?? new int[] { };
            _customerIds = customerIds ?? new int[] { };
            _lastUsedFromDate = lastUsedFromDate;
            _lastUsedToDate = lastUsedToDate;
        }

        public string Query
        {
            get { return _query; }
        }

        public int[] TagIds
        {
            get { return _tagIds; }
        }

        public int[] ProductIds
        {
            get { return _productIds; }
        }

        public int[] RcoIds
        {
            get { return _rcoIds; }
        }

        public QuestionDifficulties[] Difficulties
        {
            get { return _difficulties; }
        }

        public QuestionStatuses[] Statuses
        {
            get { return _statuses; }
        }

        public int[] LanguageIds
        {
            get { return _languageIds; }
        }

        public int[] CustomerIds
        {
            get { return _customerIds; }
        }

        public DateTime? LastUsedFromDate
        {
            get { return _lastUsedFromDate; }
        }

        public DateTime? LastUsedToDate
        {
            get { return _lastUsedToDate; }
        }
    }
}