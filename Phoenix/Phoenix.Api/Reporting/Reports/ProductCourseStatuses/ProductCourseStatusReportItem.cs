﻿using System;
using Metier.Phoenix.Core.Business.DocumentProduction.Attributes;

namespace Metier.Phoenix.Api.Reporting.Reports.ProductCourseStatuses
{
    public class ProductCourseStatusReportItem
    {
        public string Customer { get; set; }
        public string Distributor { get; set; }
        public string ProductNumber { get; set; }
        public string ProductTitle { get; set; }
        public string ActivitySetTitle { get; set; }
        
        // User
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? ExpiryDate { get; set; }
        
        // User info elements
        public string CustomField1 { get; set; }
        public string CustomField2 { get; set; }
        public string EmployeeId { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string Supervisor { get; set; }
        public string ConfirmationEmail { get; set; }


        // E-learning
        public string ActivityTitleE { get; set; }
        public DateTime? EnrollmentDateE { get; set; }
        public DateTimeOffset? StartDateE { get; set; }
        public DateTimeOffset? EndDateE { get; set; }
        public string NumberOfCompletedLessons { get; set; }
        [DisplayName("ECourse performance")]
        public string CourseStatus { get; set; }
        public string StatusE { get; set; }

        // Classroom
        public string ActivityTitleC { get; set; }
        public DateTime? EnrollmentDateC { get; set; }
        public DateTimeOffset? StartDateC { get; set; }
        public DateTimeOffset? EndDateC { get; set; }
        public string StatusC { get; set; }

        // Exam
        public string ActivityTitleEX { get; set; }
        public DateTime? EnrollmentDateEX { get; set; }
        public DateTimeOffset? StartDateEX { get; set; }
        public DateTimeOffset? EndDateEX { get; set; }
        public string StatusEX { get; set; }
        public string ResultEX { get; set; }
        public bool IsArbitrary { get; set; }        
    }
}