﻿using System;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats;

namespace Metier.Phoenix.Api.Reporting.Reports.ProductCourseStatuses
{
    public class ProductCourseStatusReportDefinition : ReportDefinition
    {
        private readonly DateTime? _fromDate;
        private readonly DateTime? _toDate;
        private readonly bool? _isArbitrary;
        private readonly int[] _customerIds;
        private readonly int[] _productIds;

        public ProductCourseStatusReportDefinition(int[] customerIds, int[] productIds, DateTime? fromDate, DateTime? toDate, bool? isArbitrary, FileFormats fileFormat)
            : base(ReportTypes.ProductCourseStatuses, fileFormat)
        {
            _fromDate = fromDate.HasValue ? fromDate : DateTime.MinValue;
            _toDate = toDate;
            _isArbitrary = isArbitrary;
            _customerIds = customerIds ?? new int[] {};
            _productIds = productIds ?? new int[] { };
        }

        public DateTime? FromDate
        {
            get { return _fromDate; }
        }

        public DateTime? ToDate
        {
            get { return _toDate; }
        }

        public int[] CustomerIds
        {
            get { return _customerIds; }
        }

        public int[] ProductIds
        {
            get { return _productIds; }
        }

        public bool? IsArbitrary
        {
            get { return _isArbitrary; }
        }
    }
}