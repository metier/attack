﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Reporting.Utilities;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.ViewEntities.Reports;

namespace Metier.Phoenix.Api.Reporting.Reports.ProductCourseStatuses
{
    public class ProductCourseStatusReportProvider : IReportProvider<ProductCourseStatusReportItem>
    {
        private readonly MetierLmsContext _context;
        private readonly ICustomersProvider _customersProvider;

        public ProductCourseStatusReportProvider(MetierLmsContext context, ICustomersProvider customersProvider)
        {
            _context = context;
            _customersProvider = customersProvider;
            ConfigureMapping();
        }

        public IEnumerable<ProductCourseStatusReportItem> GetReport(ReportDefinition reportDefinition)
        {
            var productCourseStatusReportDefinition = reportDefinition as ProductCourseStatusReportDefinition;
            if (productCourseStatusReportDefinition == null)
            {
                throw new ArgumentNullException("reportDefinition");
            }
            var report = _context.ProductCourseStatuses.AsNoTracking();
            if (productCourseStatusReportDefinition.CustomerIds.Any())
            {
                var customerIds = productCourseStatusReportDefinition.CustomerIds.SelectMany(id => _customersProvider.GetChildrenIds(id, true));
                report = report.Where(p => customerIds.Contains(p.CustomerId));
            }

            if (productCourseStatusReportDefinition.ProductIds.Any())
            {
                report = report.Where(p => productCourseStatusReportDefinition.ProductIds.Contains(p.ProductId));
            }

            if (productCourseStatusReportDefinition.FromDate.HasValue)
            {
                var fromDate = productCourseStatusReportDefinition.FromDate.Value;

                if (productCourseStatusReportDefinition.ToDate.HasValue)
                {
                    var toDate = productCourseStatusReportDefinition.ToDate.Value;
                    report = report.Where(p =>
                         ( 
                            DbFunctions.CreateDateTime(p.EnrollmentDate.Year, p.EnrollmentDate.Month, p.EnrollmentDate.Day, 0, 0, 0) >= fromDate
                         &&
                            DbFunctions.CreateDateTime(p.EnrollmentDate.Year, p.EnrollmentDate.Month, p.EnrollmentDate.Day, 0, 0, 0) <= toDate
                         ));
                }
                else
                {
                    report = report.Where(p => DbFunctions.CreateDateTime(p.EnrollmentDate.Year, p.EnrollmentDate.Month, p.EnrollmentDate.Day, 0, 0, 0) >= fromDate);    
                }
                
            }
            if (productCourseStatusReportDefinition.ToDate.HasValue && !productCourseStatusReportDefinition.FromDate.HasValue)
            {
                var toDate = productCourseStatusReportDefinition.ToDate.Value;
                report = report.Where(p => DbFunctions.CreateDateTime(p.EnrollmentDate.Year, p.EnrollmentDate.Month, p.EnrollmentDate.Day, 0, 0, 0) <= toDate);
            }

            if (productCourseStatusReportDefinition.IsArbitrary.HasValue)
            {
                report = report.Where(r => r.IsArbitrary == productCourseStatusReportDefinition.IsArbitrary.Value);
            }
            
            return report.Select(Map).ToList();
        }

        private void ConfigureMapping()
        {
            var map = Mapper.CreateMap<ProductCourseStatus, ProductCourseStatusReportItem>();
            map.ForMember(a => a.CourseStatus, m => m.MapFrom(s => ScormLessonStatus.GetDescription(s.CourseStatus)));
            map.ForMember(a => a.NumberOfCompletedLessons, m => m.MapFrom(s => GetProgress(s.NumberOfLessons, s.NumberOfCompletedLessons)));
        }

        private string GetProgress(int? numberOfLessons, int? numberOfCompletedLessons)
        {
            return !numberOfLessons.HasValue ? "" : ProgressHelper.GetProgress(numberOfLessons.Value, numberOfCompletedLessons.GetValueOrDefault());
        }

        private ProductCourseStatusReportItem Map(ProductCourseStatus productCourseStatus)
        {
            var mapped =  Mapper.Map<ProductCourseStatus, ProductCourseStatusReportItem>(productCourseStatus);
            return mapped;
        }
    }
}