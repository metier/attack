﻿using System;
using Metier.Phoenix.Core.Business.DocumentProduction.Attributes;

namespace Metier.Phoenix.Api.Reporting.Reports.CustomerSpecific.Jernbaneverket
{
    public class SafetyCourseParticipantsReportItem
    {
        [DisplayName("Customer Id")]
        public int CustomerId { get; set; }

        [DisplayName("User Id")]
        public int UserId { get; set; }

        [DisplayName("Customer")]
        public string CustomerName { get; set; }

        [DisplayName("First name")]
        public string FirstName { get; set; }

        [DisplayName("Last name")]
        public string LastName { get; set; }

        [DisplayName("Username")]
        public string UserName { get; set; }

        [DisplayName("Country of Citizenship")]
        public string CountryOfCitizenship { get; set; }

        [DisplayName("Date of birth")]
        public string DateOfBirth { get; set; }

        public string Position { get; set; }

        //[DisplayName("Helmet no.")]
        //public string HelmetNumber { get; set; }

        [DisplayName("ID-number")]
        public string IdNumber { get; set; }

        [DisplayName("User Expiry date")]
        public DateTime? UserExpiryDate { get; set; }


        public string Employer { get; set; }

        [DisplayName("Subcontractor")]
        public string SubContractorFor { get; set; }

        [DisplayName("Confirmation email")]
        public string ConfirmationEmail { get; set; }

        public string ActivitySet { get; set; }

        [DisplayName("Status part 1")]
        public string StatusPart1 { get; set; }

        [DisplayName("Status part 2")]
        public string StatusPart2 { get; set; }

        [DisplayName("Status part 3")]
        public string StatusPart3 { get; set; }

        [DisplayName("Course date")]
        public DateTime? CourseDate { get; set; }

        [DisplayName("Part 1: Completed at")]
        public DateTime? LatestCompletedStatusPart1 { get; set; }

        [DisplayName("Part 2: Completed at")]
        public DateTime? LatestCompletedStatusPart2 { get; set; }

        [DisplayName("Part 3: Completed at")]
        public DateTime? LatestCompletedStatusPart3 { get; set; }

        [DisplayName("Comment")]
        public string Comment { get; set; }
    }
}