﻿using System;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats;

namespace Metier.Phoenix.Api.Reporting.Reports.CustomerSpecific.Jernbaneverket
{
    public class SafetyCourseParticipantsReportDefinition : ReportDefinition
    {
        private readonly int[] _customerIds;
        private readonly DateTime? _fromDate;
        private readonly DateTime? _toDate;

        public SafetyCourseParticipantsReportDefinition(int[] customerIds, DateTime? fromDate, DateTime? toDate, FileFormats fileFormat)
            : base(ReportTypes.CustomerSpecific_JBV_SafetyCourseParticipants, fileFormat)
        {
            _customerIds = customerIds ?? new int[] { };

            //_fromDate = fromDate.HasValue ? fromDate : DateTime.MinValue;
            _fromDate = fromDate;
            _toDate = toDate;
        }

        public int[] CustomerIds
        {
            get { return _customerIds; }
        }

        public DateTime? FromDate
        {
            get { return _fromDate; }
        }

        public DateTime? ToDate
        {
            get { return _toDate; }
        }
    }
}