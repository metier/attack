﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.ViewEntities.Reports.CustomerSpecific.Jernbaneverket;

namespace Metier.Phoenix.Api.Reporting.Reports.CustomerSpecific.Jernbaneverket
{
    public class SafetyCourseParticipantsReportProvider : IReportProvider<SafetyCourseParticipantsReportItem>
    {
        private readonly MetierLmsContext _context;

        public SafetyCourseParticipantsReportProvider(MetierLmsContext context)
        {
            _context = context;
            ConfigureMapping();
        }

        public IEnumerable<SafetyCourseParticipantsReportItem> GetReport(ReportDefinition reportDefinition)
        {
            var safetyCourseParticipantsReportDefinition = reportDefinition as SafetyCourseParticipantsReportDefinition;
            if (safetyCourseParticipantsReportDefinition == null)
            {
                throw new ArgumentNullException("reportDefinition");
            }

            var report = _context.SafetyCourseParticipants.AsNoTracking();

            if (safetyCourseParticipantsReportDefinition.CustomerIds.Any())
            {
                report = report.Where(x => safetyCourseParticipantsReportDefinition.CustomerIds.Contains(x.CustomerId));
            }

            if (safetyCourseParticipantsReportDefinition.FromDate.HasValue)
            {
                var filterDate = safetyCourseParticipantsReportDefinition.FromDate.Value.Date;
                report = report.Where(r => r.CourseDate.Value >= filterDate);
            }
            if (safetyCourseParticipantsReportDefinition.ToDate.HasValue)
            {
                var filterDate = safetyCourseParticipantsReportDefinition.ToDate.Value.Date + TimeSpan.FromDays(1);
                report = report.Where(r => r.CourseDate.Value < filterDate);
            }
            
            return report.Select(Map).ToList();
        }

        private void ConfigureMapping()
        {
            Mapper.CreateMap<SafetyCourseParticipant, SafetyCourseParticipantsReportItem>();
        }

        private SafetyCourseParticipantsReportItem Map(SafetyCourseParticipant safetyCourseParticipant)
        {
            return Mapper.Map<SafetyCourseParticipant, SafetyCourseParticipantsReportItem>(safetyCourseParticipant);
        }
    }    
}