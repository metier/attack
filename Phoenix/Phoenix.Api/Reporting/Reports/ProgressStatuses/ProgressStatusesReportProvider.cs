using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Reporting.Utilities;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.ViewEntities.Reports;

namespace Metier.Phoenix.Api.Reporting.Reports.ProgressStatuses
{
    public class ProgressStatusesReportProvider : IReportProvider<ProgressStatusesReportItem>
    {
        private readonly MetierLmsContext _context;
        private readonly IAuthorizationProvider _authorizationProvider;
        private readonly ICustomersProvider _customersProvider;

        public ProgressStatusesReportProvider(MetierLmsContext context, IAuthorizationProvider authorizationProvider, ICustomersProvider customersProvider)
        {
            _context = context;
            _authorizationProvider = authorizationProvider;
            _customersProvider = customersProvider;
            ConfigureMapping();
        }

        public IEnumerable<ProgressStatusesReportItem> GetReport(ReportDefinition reportDefinition)
        {
            var progressStatusesReportDefinition = reportDefinition as ProgressStatusesReportDefinition;
            if (progressStatusesReportDefinition == null)
            {
                throw new ArgumentNullException("reportDefinition");
            }

            if (!IsAuthorized(progressStatusesReportDefinition.ActivityIds, progressStatusesReportDefinition.CustomerIds))
            {
                throw new UnauthorizedException("User is not authorized for this report");
            }

            var report = _context.ProgressStatuses.AsNoTracking();
            var customerIds = progressStatusesReportDefinition.CustomerIds.SelectMany(id => _customersProvider.GetChildrenIds(id, true)).ToArray();

            if (customerIds.Any())
            {
                report = report.Where(cep => customerIds.Contains(cep.CustomerId));
            }
            if (progressStatusesReportDefinition.LessonStatuses.Any())
            {
                report = report.Where(cep => progressStatusesReportDefinition.LessonStatuses.Contains(cep.Status));
            }
            
            if (progressStatusesReportDefinition.ProductIds.Any())
            {
                report = report.Where(cep => progressStatusesReportDefinition.ProductIds.Contains(cep.ProductId));
            }

            if (progressStatusesReportDefinition.ActivityIds.Any())
            {
                report = report.Where(cep => progressStatusesReportDefinition.ActivityIds.Contains(cep.ActivityId));
            }

            if (progressStatusesReportDefinition.IsArbitrary.HasValue)
            {
                report = report.Where(r => r.IsArbitrary == progressStatusesReportDefinition.IsArbitrary.Value);
            }

            if (progressStatusesReportDefinition.FromDate.HasValue)
            {
                var filterDate = progressStatusesReportDefinition.FromDate.Value.Date;
                report = report.Where(r => r.EnrollmentDate >= filterDate);
            }

            if (progressStatusesReportDefinition.ToDate.HasValue)
            {
                var filterDate = progressStatusesReportDefinition.ToDate.Value.Date + TimeSpan.FromDays(1);
                report = report.Where(r => r.EnrollmentDate < filterDate);
            }

            return report.Select(Map).ToList();
        }

        private void ConfigureMapping()
        {
            var map = Mapper.CreateMap<ProgressStatus, ProgressStatusesReportItem>();
            map.ForMember(psi => psi.CustomerType, opt => opt.MapFrom(p => ToCustomerType(p.IsArbitrary)));
            map.ForMember(a => a.Status, m => m.MapFrom(s => ScormLessonStatus.GetDescription(s.Status)));
            map.ForMember(a => a.NumberOfCompletedLessons, m => m.MapFrom(s => ProgressHelper.GetProgress(s.NumberOfLessons, s.NumberOfCompletedLessons)));
        }

        private string ToCustomerType(bool source)
        {
            return source ? "Arbitrary" : "Corporate";
        }

        private ProgressStatusesReportItem Map(ProgressStatus progressStatus)
        {
            return Mapper.Map<ProgressStatus, ProgressStatusesReportItem>(progressStatus);
        }

        private bool IsAuthorized(int[] activityIds, int[] customerIds)
        {
            if (_authorizationProvider.IsAdmin())
            {
                return true;
            }

            if (activityIds.Length == 0 && customerIds.Length == 0)
            {
                // When not admin, report must be filtered on either customerIds or activityIds
                return false;
            }

            if (customerIds.Any(id => !_authorizationProvider.HasCustomer(id, _customersProvider)))
            {
                return false;
            }
            if (activityIds.Any(id => !_authorizationProvider.HasCustomerOwningActivity(id)))
            {
                return false;
            }

            return true;
        }
    }
}