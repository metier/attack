﻿using System;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats;

namespace Metier.Phoenix.Api.Reporting.Reports.ProgressStatuses
{
    public class ProgressStatusesReportDefinition : ReportDefinition
    {
        private readonly bool? _isArbitrary;
        private readonly int[] _activityIds;
        private readonly string[] _lessonStatuses;
        private readonly int[] _productIds;
        private readonly int[] _customerIds;
        private DateTime? _fromDate;
        private DateTime? _toDate;

        public ProgressStatusesReportDefinition(int[] customerIds, string[] lessonStatuses, int[] productIds, int[] activityIds, DateTime? fromDate, DateTime? toDate, bool? isArbitrary, FileFormats fileFormat)  
            : base(ReportTypes.ProgressStatuses, fileFormat)
        {
            _isArbitrary = isArbitrary;
            _activityIds = activityIds ?? new int[]{};
            _lessonStatuses = lessonStatuses ?? new string[]{};
            _productIds = productIds ?? new int[]{};
            _customerIds = customerIds ?? new int[]{};
            FromDate = fromDate;
            ToDate = toDate;
        }

        public int[] CustomerIds
        {
            get { return _customerIds; }
        }

        public string[] LessonStatuses
        {
            get { return _lessonStatuses; }
        }

        public int[] ProductIds
        {
            get { return _productIds; }
        }

        public int[] ActivityIds
        {
            get { return _activityIds; }
        }

        public bool? IsArbitrary
        {
            get { return _isArbitrary; }
        }

        public DateTime? FromDate
        {
            get { return _fromDate; }
            set { _fromDate = value; }
        }

        public DateTime? ToDate
        {
            get { return _toDate; }
            set { _toDate = value; }
        }
    }
}