﻿using System;
using Metier.Phoenix.Core.Business.DocumentProduction.Attributes;

namespace Metier.Phoenix.Api.Reporting.Reports.ProgressStatuses
{
    public class ProgressStatusesReportItem
    {
        public string Customer { get; set; }
        public string Distributor { get; set; }
        public string ArticleNumber { get; set; }
        public string ActivityTitle { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        [DisplayName("Custom field 1")]
        public string CustomField1 { get; set; }
        [DisplayName("Custom field 2")]
        public string CustomField2 { get; set; }
        [DisplayName("Employee ID")]
        public string EmployeeId { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string Supervisor { get; set; }
        [DisplayName("Confirmation e-mail")]
        public string ConfirmationEmail { get; set; }
        public DateTime EnrollmentDate { get; set; }
        public DateTime? CompletedDate { get; set; }
        public DateTimeOffset? StartDate { get; set; }
        public string NumberOfCompletedLessons { get; set; }
        public string Status { get; set; }
        [DisplayName("Customer type")]
        public string CustomerType { get; set; }
    }
}