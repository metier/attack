﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats;

namespace Metier.Phoenix.Api.Reporting.Reports
{
    public abstract class ReportDefinition
    {
        protected ReportDefinition(ReportTypes type, FileFormats fileFormat)
        {
            Type = type;
            FileFormat = fileFormat;
        }

        public ReportTypes Type { get; private set; }
        public FileFormats FileFormat { get; private set; }
		public bool EncloseCsvValuesInApostrophes { get; set; } = true;
    }
}