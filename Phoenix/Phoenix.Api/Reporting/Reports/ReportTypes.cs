﻿namespace Metier.Phoenix.Api.Reporting.Reports
{
    public enum ReportTypes
    {
        ProgressStatuses,
        ActivityParticipants,
        ProductCourseStatuses,
        ParticipationStatusPrActivity,
        PotentialParticipations,
        AllParticipations,        
        AllParticipationsPrActivity,
        HubspotParticipants,
        OrdersForInvoicing,
        Questions,
        Exams,
        PrepaidClassrooms,
        CustomerSpecific_JBV_SafetyCourseParticipants
    }
}