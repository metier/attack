﻿using System;
using Metier.Phoenix.Core.Business.DocumentProduction.Attributes;

namespace Metier.Phoenix.Api.Reporting.Reports.PotentialParticipations
{
    public class PotentialParticipationReportItem
    {
        public string Customer { get; set; }
        [DisplayName("First name")]
        public string FirstName { get; set; }
        [DisplayName("Last name")]
        public string LastName { get; set; }
        [DisplayName("E-mail")]
        public string Email { get; set; }
        [DisplayName("Phone number")]
        public string PhoneNumber { get; set; }
        [DisplayName("Product path")]
        public string ProductPath { get; set; }
        [DisplayName("Custom field 1")]
        public string CustomField1 { get; set; }
        [DisplayName("Custom field 2")]
        public string CustomField2 { get; set; }
        [DisplayName("Employee ID")]
        public string EmployeeId { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string Supervisor { get; set; }
        [DisplayName("Confirmation e-mail")]
        public string ConfirmationEmail { get; set; }
        [DisplayName("Customer type")]
        public string CustomerType { get; set; }
		//[DisplayName("Expiry date")]
		//public DateTime ExpiryDate { get; set; }
	}
}