﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.ViewEntities.Reports;
using Metier.Phoenix.Core.Exceptions;

namespace Metier.Phoenix.Api.Reporting.Reports.PotentialParticipations
{
    public class PotentialParticipationsReportProvider : IReportProvider<PotentialParticipationReportItem>
    {
        private readonly MetierLmsContext _context;
        private readonly ICustomersProvider _customersProvider;

        public PotentialParticipationsReportProvider(MetierLmsContext context, ICustomersProvider customersProvider)
        {
            _context = context;
            _customersProvider = customersProvider;
            ConfigureMapping();
        }

        public IEnumerable<PotentialParticipationReportItem> GetReport(ReportDefinition reportDefinition)
        {
            var potentialParticipationReportDefinition = reportDefinition as PotentialParticipationReportDefinition;
            if (potentialParticipationReportDefinition == null)
            {
                throw new ArgumentNullException("reportDefinition");
            }

            var customerIds = potentialParticipationReportDefinition.CustomerIds.SelectMany(id => _customersProvider.GetChildrenIds(id, true)).ToArray();

            _context.Database.ExecuteSqlCommand("Phoenix_UserMissingProducts");
            _context.SaveChanges();
            var report = _context.PotentialParticipations.AsNoTracking();
            if (customerIds.Any())
            {
                report = report.Where(p => customerIds.Contains(p.CustomerId));
            }
            else
            {
                throw new ValidationException("CustomerId is required");
            }
            
            if (potentialParticipationReportDefinition.ProductIds.Any())
            {
                report = report.Where(p => potentialParticipationReportDefinition.ProductIds.Contains(p.ProductId));
            }
            if (potentialParticipationReportDefinition.IsArbitrary.HasValue)
            {
                report = report.Where(r => r.IsArbitrary == potentialParticipationReportDefinition.IsArbitrary.Value);
            }

			//Filter to users that have taken TakenProductIds
			if (potentialParticipationReportDefinition.TakenProductIds.Any())
			{
				report = report.Where(r => r.ProductIdsEnrolled != null);				
				foreach (var id in  potentialParticipationReportDefinition.TakenProductIds) 
				{
					var q = ";" + id + ";";
					report = report.Where(r => r.ProductIdsEnrolled.Contains(q));
				}				
			}

			var results = report.Select(Map).ToList();

			//Make sure all participants meet all criteria in ProductIds
			if (potentialParticipationReportDefinition.ProductIdsIsAndQuery && potentialParticipationReportDefinition.ProductIds.Length > 1)
			{

				var distinctProducts = potentialParticipationReportDefinition.ProductIds.Select(id => _context.Products.Where(p => p.Id == id).FirstOrDefault()).Select(p=>p.ProductPath).ToList();
				var tempResults = results.GroupBy(i => i.Email + i.Customer).ToList();
				tempResults.RemoveAll(g => { return g.Count() < distinctProducts.Count || distinctProducts.Any(p => !g.Any(gg => gg.ProductPath == p)); });

				//Expand list again
				results = tempResults.SelectMany(x => x.ToList()).ToList();
			}
									

			return results;
		}

        private void ConfigureMapping()
        {
            Mapper.CreateMap<PotentialParticipation, PotentialParticipationReportItem>()
                .ForMember(pi => pi.CustomerType, opt => opt.MapFrom(p => ToCustomerType(p.IsArbitrary)));
        }

        private string ToCustomerType(bool source)
        {
            return source ? "Arbitrary" : "Corporate";
        }

        private PotentialParticipationReportItem Map(PotentialParticipation potentialParticipation)
        {
            return Mapper.Map<PotentialParticipation, PotentialParticipationReportItem>(potentialParticipation);
        }
    }    
}