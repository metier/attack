﻿using Metier.Phoenix.Core.Business.DocumentProduction.Formats;

namespace Metier.Phoenix.Api.Reporting.Reports.PotentialParticipations
{
    public class PotentialParticipationReportDefinition : ReportDefinition
    {
        private readonly bool? _isArbitrary;
        private readonly int[] _productIds;
		private readonly bool _productIdsIsAndQuery; //If true, run AND between all ProductIds. Otherwise, it's an or
		private readonly int[] _takenProductIds;
		private readonly int[] _customerIds;

        public PotentialParticipationReportDefinition(int[] customerIds, int[] productIds, bool productIdsIsAndQuery, int[] takenProductIds, bool? isArbitrary, FileFormats fileFormat) 
            : base(ReportTypes.PotentialParticipations, fileFormat)
        {
            _isArbitrary = isArbitrary;
            _customerIds = customerIds ?? new int[] { };
            _productIds = productIds ?? new int[] { };
			_takenProductIds = takenProductIds ?? new int[] { };
			_productIdsIsAndQuery = productIdsIsAndQuery;

		}

        public int[] ProductIds
        {
            get { return _productIds; }
        }
		public bool ProductIdsIsAndQuery
		{
			get { return _productIdsIsAndQuery; }
		}
		public int[] TakenProductIds
		{
			get { return _takenProductIds; }
		}
		public int[] CustomerIds
        {
            get { return _customerIds; }
        }

        public bool? IsArbitrary
        {
            get { return _isArbitrary; }
        }
    }
}