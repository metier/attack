﻿using System;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats;

namespace Metier.Phoenix.Api.Reporting.Reports.AllParticipations
{
    public class AllParticipationReportDefinition : ReportDefinition
    {
        private readonly bool? _isArbitrary;
        private readonly int[] _distributorIds;
        private readonly int[] _customerIds;
        private readonly int[] _productIds;
        private readonly DateTime? _fromDate;
        private readonly DateTime? _toDate;

        public AllParticipationReportDefinition(int[] customerIds, int[] distributorIds, int[] productIds, DateTime? fromDate, DateTime? toDate, bool? isArbitrary, FileFormats fileFormat)
            : base(ReportTypes.AllParticipations, fileFormat)
        {
            _isArbitrary = isArbitrary;
            _customerIds = customerIds ?? new int[] { };
            _distributorIds = distributorIds ?? new int[] { };
            _productIds = productIds ?? new int[] { };
            _fromDate = fromDate.HasValue ? fromDate : DateTime.MinValue;
            _toDate = toDate;
        }

        public int[] DistributorIds
        {
            get { return _distributorIds; }
        }

        public int[] CustomerIds
        {
            get { return _customerIds; }
        }

        public int[] ProductIds
        {
            get { return _productIds; }
        }

        public bool? IsArbitrary
        {
            get { return _isArbitrary; }
        }

        public DateTime? FromDate
        {
            get { return _fromDate; }
        }

        public DateTime? ToDate
        {
            get { return _toDate; }
        }
    }
}