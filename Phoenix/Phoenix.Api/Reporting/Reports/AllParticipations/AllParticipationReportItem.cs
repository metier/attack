﻿using System;
using Metier.Phoenix.Core.Business.DocumentProduction.Attributes;

namespace Metier.Phoenix.Api.Reporting.Reports.AllParticipations
{
    public class AllParticipationReportItem
    {
        public string Distributor { get; set; }
        public string ProductPath { get; set; }
        public string CourseLink { get; set; }
        public string ActivityNameE { get; set; }
        public string ActivityNameC { get; set; }
        public string ActivityNameEx { get; set; }
        public string ArticleNameE { get; set; }
        public string ArticleNameC { get; set; }
        public string ArticleNameEx { get; set; }
        public string StatusE { get; set; }
        public string StatusC { get; set; }
        public string StatusEx { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public string Customer { get; set; }
        public string ParentOrg { get; set; }
        public string ELessons { get; set; }
        public int? Points { get; set; }
        public DateTimeOffset? EStartDate { get; set; }
        public DateTimeOffset? CStartDate { get; set; }
        public DateTimeOffset? ExStartDate { get; set; }
        public DateTimeOffset? EEndDate { get; set; }
        public DateTimeOffset? CEndDate { get; set; }
        public DateTimeOffset? ExEndDate { get; set; }
        public DateTime? ECompleteDate { get; set; }
        public DateTime? EnrollmentDateE { get; set; }
        public DateTime? EnrollmentDateC { get; set; }
        public DateTime? EnrollmentDateEx { get; set; }
        public string AM { get; set; }
        public string Coordinator { get; set; }
        public string CustArtLanguageE { get; set; }
        public string CustArtLanguageC { get; set; }
        public string CustArtLanguageEx { get; set; }
        public string SelfEnrollmentExams { get; set; }
        public string SelfEnrollmentCourses { get; set; }
        public string UserRole { get; set; }
        [DisplayName("Custom field 1")]
        public string CustomField1 { get; set; }
        [DisplayName("Custom field 2")]
        public string CustomField2 { get; set; }
        [DisplayName("Employee ID")]
        public string EmployeeId { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string Supervisor { get; set; }
        [DisplayName("Confirmation e-mail")]
        public string ConfirmationEmail { get; set; }
        [DisplayName("Customer type")]
        public string CustomerType { get; set; }
    }
}