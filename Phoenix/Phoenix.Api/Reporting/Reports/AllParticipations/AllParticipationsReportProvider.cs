﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.ViewEntities.Reports;

namespace Metier.Phoenix.Api.Reporting.Reports.AllParticipations
{
    public class AllParticipationsReportProvider : IReportProvider<AllParticipationReportItem>
    {
        private readonly MetierLmsContext _context;
        private readonly ICustomersProvider _customersProvider;

        public AllParticipationsReportProvider(MetierLmsContext context, ICustomersProvider customersProvider)
        {
            context.Database.CommandTimeout = 600;
            _context = context;
            _customersProvider = customersProvider;
            ConfigureMapping();
        }

        public IEnumerable<AllParticipationReportItem> GetReport(ReportDefinition reportDefinition)
        {
            var allParticipationReportDefinition = reportDefinition as AllParticipationReportDefinition;
            if (allParticipationReportDefinition == null)
            {
                throw new ArgumentNullException("reportDefinition");
            }

            var customerIds = allParticipationReportDefinition.CustomerIds.SelectMany(id => _customersProvider.GetChildrenIds(id, true)).ToArray();

            var report = _context.AllParticipations.AsNoTracking();
            if (customerIds.Any())
            {
                report = report.Where(r => customerIds.Contains(r.CustomerId));
            }
            if (allParticipationReportDefinition.DistributorIds.Any())
            {
                report = report.Where(r => allParticipationReportDefinition.DistributorIds.Contains(r.DistributorId));
            }
            if (allParticipationReportDefinition.ProductIds.Any())
            {
                report = report.Where(r => allParticipationReportDefinition.ProductIds.Contains(r.ProductId));
            }
            if (allParticipationReportDefinition.IsArbitrary.HasValue)
            {
                report = report.Where(r => r.IsArbitrary == allParticipationReportDefinition.IsArbitrary.Value);
            }
            if (allParticipationReportDefinition.FromDate.HasValue)
            {
                var filterDate = allParticipationReportDefinition.FromDate.Value.Date;
                report = report.Where(r => r.EnrollmentDate >= filterDate);
            }
            if (allParticipationReportDefinition.ToDate.HasValue)
            {
                var filterDate = allParticipationReportDefinition.ToDate.Value.Date + TimeSpan.FromDays(1);
                report = report.Where(r => r.EnrollmentDate < filterDate);
            }

            return report.Select(Map).ToList();
        }

        private string ToYesNoString(bool? source)
        {
            return source.GetValueOrDefault() ? "Yes" : "No";
        }

        private void ConfigureMapping()
        {
            Mapper.CreateMap<AllParticipation, AllParticipationReportItem>()
                .ForMember(a => a.SelfEnrollmentExams, opt => opt.MapFrom(p => ToYesNoString(p.SelfEnrollmentExams)))
                .ForMember(a => a.SelfEnrollmentCourses, opt => opt.MapFrom(p => ToYesNoString(p.SelfEnrollmentCourses)))
                .ForMember(pi => pi.CustomerType, opt => opt.MapFrom(p => ToCustomerType(p.IsArbitrary)));
        }

        private string ToCustomerType(bool source)
        {
            return source ? "Arbitrary" : "Corporate";
        }

        private AllParticipationReportItem Map(AllParticipation allParticipation)
        {
            return Mapper.Map<AllParticipation, AllParticipationReportItem>(allParticipation);
        }
    }
}