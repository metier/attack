﻿using System.Collections.Generic;

namespace Metier.Phoenix.Api.Reporting.Reports
{
    public interface IReportProvider<T>
    {
        IEnumerable<T> GetReport(ReportDefinition reportDefinition);
    }
}