﻿using Metier.Phoenix.Core.Business.DocumentProduction.Formats;

namespace Metier.Phoenix.Api.Reporting.Reports.ActivityParticipants
{
    public class ActivityParticipantsReportDefinition : ReportDefinition
    {
        private readonly int[] _activityIds;

        public ActivityParticipantsReportDefinition(int[] activityIds, 
                                                    FileFormats fileFormat) 
            : base(ReportTypes.ActivityParticipants, fileFormat)
        {
            _activityIds = activityIds ?? new int[]{};
        }

        public int[] ActivityIds
        {
            get { return _activityIds; }
        }
    }
}