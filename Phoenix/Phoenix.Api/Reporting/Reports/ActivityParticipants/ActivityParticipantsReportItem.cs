﻿using System;

namespace Metier.Phoenix.Api.Reporting.Reports.ActivityParticipants
{
    public class ActivityParticipantsReportItem
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTimeOffset? StartDate { get; set; }
        public string Status { get; set; } 
        public string Result { get; set; } 
    }
}