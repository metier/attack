﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.ViewEntities.Reports;

namespace Metier.Phoenix.Api.Reporting.Reports.ActivityParticipants
{
    public class ActivityParticipantsReportProvider : IReportProvider<ActivityParticipantsReportItem>
    {
        private readonly MetierLmsContext _context;
        
        public ActivityParticipantsReportProvider(MetierLmsContext context)
        {
            _context = context;
            ConfigureMapping();
        }

        public IEnumerable<ActivityParticipantsReportItem> GetReport(ReportDefinition reportDefinition)
        {
            var activityParticipantsReportDefinition = reportDefinition as ActivityParticipantsReportDefinition;
            if (activityParticipantsReportDefinition == null)
            {
                throw new ArgumentNullException("reportDefinition");
            }

            var report = _context.ActivityParticipants.AsNoTracking();
            if (activityParticipantsReportDefinition.ActivityIds.Any())
            {
                report = report.Where(ap => activityParticipantsReportDefinition.ActivityIds.Contains(ap.ActivityId));
            }
            return report.Select(Map).ToList();
        }

        private void ConfigureMapping()
        {
            Mapper.CreateMap<ActivityParticipant, ActivityParticipantsReportItem>();
        }

        private ActivityParticipantsReportItem Map(ActivityParticipant activityParticipant)
        {
            return Mapper.Map<ActivityParticipant, ActivityParticipantsReportItem>(activityParticipant);
        }
    }    
}