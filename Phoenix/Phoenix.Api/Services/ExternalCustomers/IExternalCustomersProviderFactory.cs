﻿
namespace Metier.Phoenix.Api.Services.ExternalCustomers
{
    public interface IExternalCustomersProviderFactory
    {
            /// <summary>
            /// Gets the external customer provider based on the distributorId
            /// </summary>
            /// <param name="distributorId">DistributorId of the customer</param>
            /// <returns>Returns the external customer provider for the given distributor id, or the default provider.</returns>
        IExternalCustomerProvider GetExternalCustomerProvider(int distributorId);

        bool HasExternalCustomersProviders(int distributorId);
    }
}