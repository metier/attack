﻿namespace Metier.Phoenix.Api.Services.ExternalCustomers
{
    public class ExternalCustomer
    {
        public string Name { get; set; }
        public string Id { get; set; }

        public string AccountNumber { get; set; }
        public string CompanyRegistrationNumber { get; set; }
        public string AddressStreet1 { get; set; }
        public string AddressStreet2 { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}