﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Services.Agresso.Customers;

namespace Metier.Phoenix.Api.Services.ExternalCustomers
{
    public class ExternalCustomersSearch : ISearchExternalCustomers
    {
        private readonly ISearchAgressoCustomers _agressoCustomerSearch;

        public ExternalCustomersSearch(ISearchAgressoCustomers agressoCustomerSearch)
        {
            _agressoCustomerSearch = agressoCustomerSearch;
        }

        public List<ExternalCustomer> Search(ExternalCustomersSearchQuery query)
        {
            switch (query.Provider)
            {
                case ExternalCustomersProviders.MetierAcademyNorwayAgresso:
                case ExternalCustomersProviders.MetierAcademySwedenAgresso:
                    return _agressoCustomerSearch.Search(query);
                default:
                    return Default();
            }
        }

        private List<ExternalCustomer> Default()
        {
            return new List<ExternalCustomer>();
        } 
    }
}