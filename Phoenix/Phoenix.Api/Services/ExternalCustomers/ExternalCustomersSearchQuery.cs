﻿namespace Metier.Phoenix.Api.Services.ExternalCustomers
{
    public class ExternalCustomersSearchQuery
    {
        public int DistributorId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerId { get; set; }
    }
}