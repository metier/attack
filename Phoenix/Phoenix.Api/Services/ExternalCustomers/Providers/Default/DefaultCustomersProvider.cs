﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Services.Agresso.Configuration;
using Metier.Phoenix.Api.Services.Agresso.Configuration.Norway;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Services.ExternalCustomers.Providers.Default
{
    public class DefaultCustomersProvider : IExternalCustomerProvider
    {
        public int SupportedDistributorId { get; } = 0;

        public IAgressoConfiguration GetConfiguration()
        {
            return new AgressoNorwayConfiguration();    //TODO: Really not sure why this returns the norwegian configuration...??
        }

        public bool Validate(Customer customer)
        {
            return false;   //No need to do any further processing, so returning false
        }

        public string Create(ExternalCustomerCreateRequest customerCreateRequest)
        {
            return string.Empty;
        }

        public List<ExternalCustomer> Search(ExternalCustomersSearchQuery query)
        {
            return new List<ExternalCustomer>();
        }
    }
}