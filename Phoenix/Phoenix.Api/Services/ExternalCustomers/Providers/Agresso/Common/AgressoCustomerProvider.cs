﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Agresso.CustomerService;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Services.Agresso.Configuration;
using Metier.Phoenix.Api.Services.Agresso.Customers;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Core.Utilities.Logging;
using NLog;

namespace Metier.Phoenix.Api.Services.ExternalCustomers.Providers.Agresso.Common
{
    public abstract class AgressoCustomerProvider : IExternalCustomerProvider
    {
        private readonly CustomerSoap _agressoCustomerService;
        private readonly IExternalCustomerFactory _externalCustomerFactory;
        private readonly Logger _logger;

        protected AgressoCustomerProvider(CustomerSoap agressoCustomerService, IExternalCustomerFactory externalCustomerFactory)
        {
            _agressoCustomerService = agressoCustomerService;
            _externalCustomerFactory = externalCustomerFactory;
            _logger = LogManager.GetCurrentClassLogger();
        }

        public abstract int SupportedDistributorId { get; }

        public abstract IAgressoConfiguration GetConfiguration();

        public bool Validate(Customer customer)
        {
            if (!string.IsNullOrWhiteSpace(customer.ExternalCustomerId))
                return false;

            if (!customer.IsArbitrary)
                throw new ValidationException("This is a corporate customer and only arbitrary customers can be created in Agresso");

            if(customer.DistributorId != SupportedDistributorId)
                throw new ValidationException("Invalid distributor for customer");

            return true;
        }

        public virtual string Create(ExternalCustomerCreateRequest customerCreateRequest)
        {
            var customerObject = CreateCustomerObject(customerCreateRequest);

            CreateCustomerResponse result;
            using (new TimingLogScope(_logger, $"Query to complete customer (Name: {customerCreateRequest.Name}) in Agresso client {GetConfiguration().Client} completed"))
            {
                result = _agressoCustomerService.CreateCustomer(new CreateCustomerRequest(new CreateCustomerRequestBody(customerObject, true, GetConfiguration().GetCustomerServiceCredentials())));
            }
            
            if (result.Body.CreateCustomerResult.ReturnCode == -1)
            {
                var errorMsg = result.Body.CreateCustomerResult.ChildErrorInfoList.Select(e => e.ErrorMessage).FirstOrDefault();
                if (errorMsg != null)
                {
                    throw new ValidationException($"Failed to create customer in Agresso: {errorMsg}");
                }
                throw new CreateExternalCustomerException("Failed to create customer in Agresso");    
                
            }
            var externalCustomer = _externalCustomerFactory.Create(result.Body.CreateCustomerResult.CustomerType);
            return externalCustomer.Id;
        }

        protected virtual CustomerObject CreateCustomerObject(ExternalCustomerCreateRequest customerCreateRequest)
        {
            var customerObject = new CustomerObject
            {
                CustomerName = customerCreateRequest.Name,
                ShortName = GetInitials(customerCreateRequest.Name),
                CompanyRegistrationNumber = customerCreateRequest.CompanyRegistrationNumber,
                RelationValueList = new[]
                {
                    new RelationValueUnitType
                    {
                        Relation = "Y02", // Key Account
                        RelationValue = "999", // Other
                        DateFrom = new DateTime(1900, 1, 1),
                        DateTo = new DateTime(2099, 1, 1)
                    },
                    new RelationValueUnitType
                    {
                        Relation = "Y03", // Industry
                        RelationValue = "OTH", // Other
                        DateFrom = new DateTime(1900, 1, 1),
                        DateTo = new DateTime(2099, 1, 1)
                    },
                    new RelationValueUnitType
                    {
                        Relation = "NB", // Intercompany Company
                        RelationValue = "EX", // External
                        DateFrom = new DateTime(1900, 1, 1),
                        DateTo = new DateTime(2099, 1, 1)
                    },
                    AssignWorkOrderRelation()
                },
                PaymentTerms = "30",
                //Currency = "NOK",
                //Language = "NO",
                PayMethod = "UG",
                BankAccount = "00000000019",
                PostalAccount = "00000000019",
                Status = "N",
                CustomerGroupID = "01",
                CustomerID = "[NEW]",
				HeadOffice = "[NEW]",
				Company = GetConfiguration().Client,
                CountryCode = customerCreateRequest.Country,
                ExternalReference = "Not specified",
                CollectionCode = "IK1",
                ExpiryDate = new DateTime(2099, 1, 1),
                AddressList = new[]
                {
                    new AddressUnitType
                    {
                        Address = customerCreateRequest.AddressStreet1,
                        AddressType = "1", // Generell, hardkodes
                        ZipCode = customerCreateRequest.ZipCode,
                        Place = customerCreateRequest.City,
                        CountryCode = customerCreateRequest.Country,
                        ContactPerson = customerCreateRequest.Name
                    }
                }
            };
            return customerObject;
        }

        public List<ExternalCustomer> Search(ExternalCustomersSearchQuery query)
        {
            var configuration = GetConfiguration();

            var customerObject = new CustomerObject { CustomerName = "%", Company = configuration.Client };

            if (!string.IsNullOrEmpty(query.CustomerId))
            {
                customerObject.CustomerID = query.CustomerId;
            }

            if (!string.IsNullOrEmpty(query.CustomerName))
            {
                customerObject.CustomerName = "%" + query.CustomerName + "%";
            }
            var credentials = configuration.GetCustomerServiceCredentials();
            GetCustomersResponse result;
            using (new TimingLogScope(_logger, $"Query for customers in Agresso client {configuration.Client} completed"))
            {
                result = _agressoCustomerService.GetCustomers(new GetCustomersRequest(new GetCustomersRequestBody(customerObject, false, credentials)));
            }
            var externalCustomers = result.Body.GetCustomersResult.CustomerTypeList.Select(co => _externalCustomerFactory.Create(co)).ToList();
            return externalCustomers.ToList();
        }

        #region private methods

        protected string GetInitials(string name)
        {
            var initials = string.Empty;
            var nameParts = name.Split(' ');

            return nameParts.Where(part => !String.IsNullOrWhiteSpace(part)).Aggregate(initials, (current, part) => current + part.Substring(0, 1));
        }

        protected RelationValueUnitType AssignWorkOrderRelation()
        {
            return new RelationValueUnitType
            {
                Relation = "BF",
                RelationValue = GetConfiguration().IndividualCustomerWorkOrder

			};
        }
        
        #endregion

    }
}