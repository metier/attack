﻿using Metier.Phoenix.Api.Agresso.CustomerService;
using Metier.Phoenix.Api.Services.Agresso.Configuration;
using Metier.Phoenix.Api.Services.Agresso.Configuration.Norway;
using Metier.Phoenix.Api.Services.Agresso.Customers;
using Metier.Phoenix.Api.Services.ExternalCustomers.Providers.Agresso.Common;

namespace Metier.Phoenix.Api.Services.ExternalCustomers.Providers.Agresso.Norway
{
    public class AgressoNorwayCustomerProvider : AgressoCustomerProvider, IAgressoNorwayCustomerProvider
    {
        private readonly IAgressoNorwayConfiguration _configuration;

        public AgressoNorwayCustomerProvider(IAgressoNorwayConfiguration configuration, CustomerSoap agressoCustomerService, IExternalCustomerFactory externalCustomerFactory) : base(agressoCustomerService, externalCustomerFactory)
        {
            _configuration = configuration;
        }

        public override int SupportedDistributorId { get; } = 1194;

        public override IAgressoConfiguration GetConfiguration()
        {
            return _configuration;
        }

        protected override CustomerObject CreateCustomerObject(ExternalCustomerCreateRequest customerCreateRequest)
        {
            var customer = base.CreateCustomerObject(customerCreateRequest);

            customer.Currency = "NOK";
            customer.Language = "NO";

            return customer;
        }
    }
}