﻿using System;
using Metier.Phoenix.Api.Agresso.CustomerService;
using Metier.Phoenix.Api.Services.Agresso.Configuration;
using Metier.Phoenix.Api.Services.Agresso.Configuration.Sweden;
using Metier.Phoenix.Api.Services.Agresso.Customers;
using Metier.Phoenix.Api.Services.ExternalCustomers.Providers.Agresso.Common;
using Metier.Phoenix.Core.Exceptions;

namespace Metier.Phoenix.Api.Services.ExternalCustomers.Providers.Agresso.Sweden
{
    public class AgressoSwedenCustomerProvider : AgressoCustomerProvider, IAgressoSwedenCustomerProvider
    {
        private readonly IAgressoSwedenConfiguration _configuration;

        public AgressoSwedenCustomerProvider(IAgressoSwedenConfiguration configuration, CustomerSoap agressoCustomerService, IExternalCustomerFactory externalCustomerFactory) : base(agressoCustomerService, externalCustomerFactory)
        {
            _configuration = configuration;
        }

        public override int SupportedDistributorId { get; } = 1195;

        public override IAgressoConfiguration GetConfiguration()
        {
            return _configuration;
            //return new AgressoSwedenConfiguration();
        }

        protected override CustomerObject CreateCustomerObject(ExternalCustomerCreateRequest customerCreateRequest)
        {
            FormatAgressoSwedenRequest(customerCreateRequest);
            var customer = base.CreateCustomerObject(customerCreateRequest);

            customer.Currency = "SEK";
            customer.Language = "SE";

            return customer;
        }

        #region Private methods

        private void FormatAgressoSwedenRequest(ExternalCustomerCreateRequest customerCreateRequest)
        {
            if (customerCreateRequest.Country == "SE")
            {
                int parsedValue;
                var parsed = Int32.TryParse(customerCreateRequest.ZipCode.Replace(" ", ""), out parsedValue);

                if (!parsed)
                {
                    throw new ValidationException(string.Format("Zip code must contain numbers only (Was: {0})", customerCreateRequest.ZipCode));
                }

                customerCreateRequest.ZipCode = parsedValue.ToString("000 00");
            }
        }

        #endregion
    }
}