﻿namespace Metier.Phoenix.Api.Services.ExternalCustomers
{
    public class ExternalCustomerCreateRequest
    {
        public string Name { get; set; }
        public string Country { get; set; }
        public string AddressStreet1 { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string CompanyRegistrationNumber { get; set; }		
	}
}