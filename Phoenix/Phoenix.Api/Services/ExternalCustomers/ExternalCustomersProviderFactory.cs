﻿using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Services.ExternalCustomers.Providers.Agresso.Norway;
using Metier.Phoenix.Api.Services.ExternalCustomers.Providers.Agresso.Sweden;
using Metier.Phoenix.Api.Services.ExternalCustomers.Providers.Default;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Services.ExternalCustomers
{
    public class ExternalCustomersProviderFactory : IExternalCustomersProviderFactory
    {
        private readonly HashSet<IExternalCustomerProvider> _externalCustomerProviders;

        public ExternalCustomersProviderFactory(IAgressoNorwayCustomerProvider agressoNorwayCustomerProvider, 
                                        IAgressoSwedenCustomerProvider agressoSwedenCustomerProvider,
                                        IExternalCustomerProvider defaultCustomerCreator)
        {
            _externalCustomerProviders = new HashSet<IExternalCustomerProvider> {agressoNorwayCustomerProvider, agressoSwedenCustomerProvider, defaultCustomerCreator};
        }

        public IExternalCustomerProvider GetExternalCustomerProvider(int distributorId)
        {
            var provider = _externalCustomerProviders.FirstOrDefault(p => p.SupportedDistributorId == distributorId);

            if(provider == null)
                provider = _externalCustomerProviders.FirstOrDefault(p => p.SupportedDistributorId == 0);

            return provider;
        }

        public bool HasExternalCustomersProviders(int distributorId)
        {
            return GetExternalCustomerProvider(distributorId).SupportedDistributorId != 0;
        }


    }
}