﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Services.Agresso.Configuration;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Services.ExternalCustomers
{
    public interface IExternalCustomerProvider
    {

        /// <summary>
        /// The Id of the distributor that this provider supports
        /// </summary>
        int SupportedDistributorId { get; }

        /// <summary>
        /// The configuration for connecting to the specific external system.
        /// If other external systems than Agresso is supported in the future, the 'IAgressoConfiguration' interface must be changed.
        /// </summary>
        /// <returns></returns>
        IAgressoConfiguration GetConfiguration();

        /// <summary>
        /// Validates the 'internal' customer before creation
        /// </summary>
        /// <param name="customer">The 'local' (Phoenix-) customer to be created in the external system.</param>
        bool Validate(Customer customer);
        
        /// <summary>
        /// Creates an external customer
        /// </summary>
        /// <param name="createRequest"></param>
        /// <returns>An identifier for that customer in the external system</returns>
        string Create(ExternalCustomerCreateRequest createRequest);

        List<ExternalCustomer> Search(ExternalCustomersSearchQuery query);
    }
}