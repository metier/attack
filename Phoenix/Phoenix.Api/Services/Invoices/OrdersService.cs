﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Services.Agresso.Invoicing;
using Metier.Phoenix.Api.Services.Invoices.DefaultOrderService;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Services.Invoices
{
    public class OrdersService : IOrdersService
    {
        private readonly IAgressoOrderService _agressoOrderService;
        private readonly IDefaultOrderService _defaultOrderService;

        //Using 'Decorator pattern'
        public OrdersService(IAgressoOrderService agressoOrderService, IDefaultOrderService defaultOrderService)
        {
            _agressoOrderService = agressoOrderService;
            _defaultOrderService = defaultOrderService;
        }

        public OrderRequest PrepareOrderRequest(OrderRequest orderRequest)
        {
            switch (orderRequest.Provider)
            {
                case OrderProviders.MetierAcademyNorwayAgresso:
                case OrderProviders.MetierAcademySwedenAgresso:
                    return _agressoOrderService.PrepareOrderRequest(orderRequest);
                default:
                    return _defaultOrderService.PrepareOrderRequest(orderRequest);
            }
        }
 
        public Order Create(OrderRequest orderRequest)
        {
            switch (orderRequest.Provider)
            {
                case OrderProviders.MetierAcademyNorwayAgresso:                    
                case OrderProviders.MetierAcademySwedenAgresso:
                    return _agressoOrderService.Create(orderRequest);
                default:
                    return _defaultOrderService.Create(orderRequest);
            }
        }

        public IEnumerable<string> GetWorkOrders(WorkOrderRequest workOrderRequest)
        {
            switch (workOrderRequest.Provider)
            {
                case OrderProviders.MetierAcademyNorwayAgresso:
                case OrderProviders.MetierAcademySwedenAgresso:
                    return _agressoOrderService.GetWorkOrders(workOrderRequest);
                default:
                    return _defaultOrderService.GetWorkOrders(workOrderRequest);
            }
        }

    }
}