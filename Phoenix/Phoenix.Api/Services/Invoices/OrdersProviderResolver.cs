﻿namespace Metier.Phoenix.Api.Services.Invoices
{
    public class OrdersProviderResolver : IOrdersProviderResolver
    {
        private readonly int[] _invoiceProviderDistributorIds = { 1194, 1195 };

        public int[] InvoiceProviderDistributorIds
        {
            get { return _invoiceProviderDistributorIds; }
        }

        public OrderProviders GetInvoicesProviders(int distributorId)
        {
            switch (distributorId)
            {
                case 1194:
                    return OrderProviders.MetierAcademyNorwayAgresso;
                case 1195:
                    return OrderProviders.MetierAcademySwedenAgresso;
            }
            return OrderProviders.Default;
        }

        public bool HasInvoiceProviders(int distributorId)
        {
            return true;
        }
    }
}