﻿using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Services.Invoices
{
    public class InvoiceSearchQuery
    {
        public Customer Customer { get; set; }
        public OrderProviders Provider { get; set; }
    }
}