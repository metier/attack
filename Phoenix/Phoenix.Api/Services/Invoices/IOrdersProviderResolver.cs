﻿namespace Metier.Phoenix.Api.Services.Invoices
{
    public interface IOrdersProviderResolver
    {
        int[] InvoiceProviderDistributorIds { get; }
        OrderProviders GetInvoicesProviders(int distributorId);
        bool HasInvoiceProviders(int distributorId);
    }
}