﻿namespace Metier.Phoenix.Api.Services.Invoices
{
    public enum OrderProviders
    {
        Default = 0,
        MetierAcademyNorwayAgresso = 1,
        MetierAcademySwedenAgresso = 2
    }
}