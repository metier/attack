﻿using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Services.Invoices
{
    public class OrderRequest
    {
        public Order Order { get; set; }
        public Order CreditedOrder { get; set; }
        public OrderProviders Provider { get; set; }
        public string ExternalCustomerId { get; set; }
    }
}