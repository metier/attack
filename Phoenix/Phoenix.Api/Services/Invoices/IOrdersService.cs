﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Services.Invoices
{
    public interface IOrdersService
    {
        OrderRequest PrepareOrderRequest(OrderRequest orderRequest);
        Order Create(OrderRequest orderRequest);
        IEnumerable<string> GetWorkOrders(WorkOrderRequest workOrderRequest);
    }

    public class WorkOrderRequest
    {
        public string ExternalCustomerId { get; set; }
        public OrderProviders Provider { get; set; }
    }
}