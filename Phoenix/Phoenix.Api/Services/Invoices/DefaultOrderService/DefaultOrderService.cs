﻿using System;
using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Api.Services.Invoices.DefaultOrderService
{
    public class DefaultOrderService : IDefaultOrderService
    {

        public OrderRequest PrepareOrderRequest(OrderRequest orderRequest)
        {
            orderRequest.Order.OrderNumber = orderRequest.Order.Id;
            orderRequest.Order.InvoiceNumber = orderRequest.Order.Id;
            orderRequest.Order.Status = OrderStatus.Invoiced;
            orderRequest.Order.OrderDate = DateTime.UtcNow;

            return orderRequest;
        }
 
        public Order Create(OrderRequest orderRequest)
        {
            return orderRequest.Order;
        }

        public IEnumerable<string> GetWorkOrders(WorkOrderRequest workOrderRequest)
        {
            return new List<string> {""};       //No given work order
        }

    }
}