﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Transactions;
using Metier.Phoenix.Api.Agresso.ImportService;
using Metier.Phoenix.Api.Agresso.QueryEngine;
using Metier.Phoenix.Api.Configuration;
using Metier.Phoenix.Api.Services.Agresso.Configuration;
using Metier.Phoenix.Api.Services.Agresso.Invoicing.Factories;
using Metier.Phoenix.Api.Services.Agresso.Invoicing.Xml;
using Metier.Phoenix.Api.Services.Invoices;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Core.Utilities.Logging;
using NLog;

namespace Metier.Phoenix.Api.Services.Agresso.Invoicing
{
    public class AgressoOrderService : IAgressoOrderService
    {
        private const string ServerProcessId = "LG04";
        private const string MenuId = "SO103";

        private readonly IAgressoConfigurationProvider _configurationProvider;
        private readonly IAgressoOrderFactory _agressoOrderFactory;
        private readonly IAgressoQueryXmlParser _queryXmlParser;
        private readonly IAgressoOrderSerializer _agressoOrderSerializer;
        private readonly QueryEngineV201101Soap _agressoQueryService;
        private readonly ImportV200606Soap _agressoImportService;
        private readonly Logger _logger;

        public AgressoOrderService(
            IAgressoConfigurationProvider configurationProvider, 
            QueryEngineV201101Soap agressoQueryService, 
            ImportV200606Soap agressoImportService,
            IAgressoOrderFactory agressoOrderFactory, 
            IAgressoQueryXmlParser queryXmlParser, 
            IAgressoOrderSerializer agressoOrderSerializer)
        {
            _configurationProvider = configurationProvider;
            _agressoOrderFactory = agressoOrderFactory;
            _queryXmlParser = queryXmlParser;
            _agressoOrderSerializer = agressoOrderSerializer;
            _agressoQueryService = agressoQueryService;
            _agressoImportService = agressoImportService;

            _logger = LogManager.GetCurrentClassLogger();
        }

        public OrderRequest PrepareOrderRequest(OrderRequest orderRequest)
        {
            var workOrder = orderRequest.Order.WorkOrder ??
                            GetWorkOrders(new WorkOrderRequest
                            {
                                ExternalCustomerId = orderRequest.ExternalCustomerId,
                                Provider = orderRequest.Provider
                            }).FirstOrDefault();

            if (string.IsNullOrEmpty(workOrder))
            {
                throw new ValidationException(
                    string.Format(
                        "Customer {0} does not have any work orders in Agresso. Not possible to invoice order",
                        orderRequest.Order.Customer.Name), ValidationErrorCodes.InvoiceMissingWorkOrder);
            }

            orderRequest.Order.WorkOrder = workOrder;

            var externalOrderReference = GetNextOrderNumber(orderRequest.Provider);
            orderRequest.Order.OrderNumber = Convert.ToInt32(externalOrderReference);

            orderRequest.Order.Status = OrderStatus.Transferred;
            orderRequest.Order.OrderDate = DateTime.UtcNow;

            return orderRequest;
        }


        public Order Create(OrderRequest orderRequest)
        {
            var configuration = _configurationProvider.GetConfiguration(orderRequest.Provider);
            var order = _agressoOrderFactory.CreateOrder(orderRequest.Order, orderRequest.ExternalCustomerId, orderRequest.Order.WorkOrder,
                orderRequest.Order.OrderNumber.ToString(),
                orderRequest.CreditedOrder == null ? null : orderRequest.CreditedOrder.InvoiceNumber);
            var serializedOrder = _agressoOrderSerializer.SerializeOrder(order);

            using (
                new TimingLogScope(_logger,
                    string.Format("Query to create order for Agresso client {0} completed", configuration.Client)))
            {
                try
                {
                    _agressoImportService.ExecuteServerProcessAsynchronously(
                        new ExecuteServerProcessAsynchronouslyRequest(
                            new ExecuteServerProcessAsynchronouslyRequestBody(new ServerProcessInput
                            {
                                ServerProcessId = ServerProcessId,
                                MenuId = MenuId,
                                Variant = 3,
                                XmlString = serializedOrder
                            }, configuration.GetImportServiceCredentials())));
                }
                catch (TimeoutException)
                {
                    throw new ValidationException("The Agresso webservice did not respond within the time limit when creating the order in Agresso.");
                }
            }

            return orderRequest.Order;
        }

        public IEnumerable<string> GetWorkOrders(WorkOrderRequest workOrderRequest)
        {
            if (string.IsNullOrEmpty(workOrderRequest.ExternalCustomerId))
            {
                throw new ValidationException("No ID for external invoice system was found for customer");
            }

            var configuration = _configurationProvider.GetConfiguration(workOrderRequest.Provider);

            TemplateResultAsXML result;
            using (new TimingLogScope(_logger, string.Format("Query for work orders from Agresso client {0} completed", configuration.Client)))
            {
                try
                {
                    result = _agressoQueryService.GetTemplateResultAsXML(new InputForTemplateResult
                    {
                        TemplateId = configuration.CustomerWorkOrderQueryTemplateId,
                        SearchCriteriaPropertiesList = new[]
                        {
                            new SearchCriteriaProperties
                            {
                                ColumnName = "apar_id",
                                FromValue = workOrderRequest.ExternalCustomerId,
                                RestrictionType = "=",
                                DataType = 10
                            }
                        }
                    }, configuration.GetQueryEngineServiceCredentials());
                }
                catch (TimeoutException)
                {
                    throw new ValidationException(string.Format("The Agresso webservice did not respond within the time limit when requesting work orders for Agresso Id {0}.", workOrderRequest.ExternalCustomerId));
                }
            }
			var workOrders = _queryXmlParser.GetWorkOrdersFromQuery(result).ToList();
			//if (workOrders.Count == 1 && workOrders.First() == configuration.IndividualCustomerWorkOrder && configuration.Client == "MO")
			//	workOrders.Add("PAS2227-10");

			return workOrders;
        }
        
        private string GetNextOrderNumber(OrderProviders provider)
        {
            string command = string.Empty;
            string orderNumber;
            int numberAsInt;

            if (provider == OrderProviders.MetierAcademyNorwayAgresso)
            {
                command = "EXECUTE [dbo].[Phoenix_GetNextOrderNo]";
            }
            else if (provider == OrderProviders.MetierAcademySwedenAgresso)
            {
                command = "EXECUTE [dbo].[Phoenix_GetNextOrderNo]";
            }
            
            var configuration = new PhoenixConfiguration();
            using (var conn = new SqlConnection(configuration.ConnectionString))
            {
                var sqlCommand = new SqlCommand(command, conn);
                conn.Open();
                orderNumber = Convert.ToString(sqlCommand.ExecuteScalar());
            }


            if(string.IsNullOrWhiteSpace(orderNumber) || !int.TryParse(orderNumber, out numberAsInt))
                throw new ValidationException("Invalid ordernumber generated!");

            return orderNumber;
        }
    }
}