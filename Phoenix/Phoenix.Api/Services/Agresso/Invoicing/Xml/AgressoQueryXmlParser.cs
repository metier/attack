﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Metier.Phoenix.Api.Agresso.QueryEngine;
using Metier.Phoenix.Api.Exceptions;

namespace Metier.Phoenix.Api.Services.Agresso.Invoicing.Xml
{
    public class AgressoQueryXmlParser : IAgressoQueryXmlParser
    {
        public IEnumerable<string> GetWorkOrdersFromQuery(TemplateResultAsXML result)
        {
            var workOrderQueryResult = XDocument.Parse(result.TemplateResult);
            try
            {
                return workOrderQueryResult.Descendants("AgressoQE").Select(x => x.Descendants("work_order").First().Value);
            }
            catch (ArgumentNullException)
            {
                throw CreateMissingWorkorderException();
            }
            catch (InvalidOperationException)
            {
                throw CreateMissingWorkorderException();
            }
        }

        private static InvoicePreconditionException CreateMissingWorkorderException()
        {
            return new InvoicePreconditionException("No usable work order could be found for customer in Agresso");
        }
    }
}