﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Agresso.QueryEngine;

namespace Metier.Phoenix.Api.Services.Agresso.Invoicing.Xml
{
    public interface IAgressoQueryXmlParser
    {
        IEnumerable<string> GetWorkOrdersFromQuery(TemplateResultAsXML result);
    }
}