﻿using System.Xml;
using System.Xml.Serialization;
using Metier.Phoenix.Api.Services.Agresso.Invoicing.References;
using Metier.Phoenix.Api.Utilities;

namespace Metier.Phoenix.Api.Services.Agresso.Invoicing.Xml
{
    public class AgressoOrderSerializer : IAgressoOrderSerializer
    {
        public string SerializeOrder(ABWOrder order)
        {
            using (var textWriter = new Utf8StringWriter())
            {
                using (var xmlTextWriter = XmlWriter.Create(textWriter, new XmlWriterSettings { OmitXmlDeclaration = true }))
                {
                    var serializer = new XmlSerializer(typeof(ABWOrder));

                    var ns = new XmlSerializerNamespaces();
                    ns.Add("agr", "http://services.agresso.com/schema/ABWOrder/2011/11/14");
                    ns.Add("agrlib", "http://services.agresso.com/schema/ABWSchemaLib/2011/11/14");

                    serializer.Serialize(xmlTextWriter, order, ns);
                    return textWriter.ToString();
                }
            }
        }
    }
}