﻿using Metier.Phoenix.Api.Services.Agresso.Invoicing.References;

namespace Metier.Phoenix.Api.Services.Agresso.Invoicing.Xml
{
    public interface IAgressoOrderSerializer
    {
        string SerializeOrder(ABWOrder order);
    }
}