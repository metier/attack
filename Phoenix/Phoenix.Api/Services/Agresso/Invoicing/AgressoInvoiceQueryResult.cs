﻿using System;

namespace Metier.Phoenix.Api.Services.Agresso.Invoicing
{
    public class AgressoInvoiceQueryResult
    {
        public string WorkOrder { get; set; }
        public decimal Amount { get; set; }
        public decimal VatAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public string OrderStatus { get; set; }
        public DateTime? OrderDate { get; set; }
        public int InvoiceNumber { get; set; }
        public int OrderNumber { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
    }
}