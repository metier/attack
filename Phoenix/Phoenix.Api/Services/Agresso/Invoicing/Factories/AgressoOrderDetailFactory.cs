﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Factories;
using Metier.Phoenix.Api.Services.Agresso.Invoicing.References;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Queryables;

namespace Metier.Phoenix.Api.Services.Agresso.Invoicing.Factories
{
    public class AgressoOrderDetailFactory : IAgressoOrderDetailFactory
    {
        private readonly MetierLmsContext _context;
        private readonly IOrderFactory _orderFactory;

        public AgressoOrderDetailFactory(MetierLmsContext context, IOrderFactory orderFactory)
        {
            _context = context;
            _orderFactory = orderFactory;
        }

        public ABWOrderOrderDetail GetDetailForActivity(Order order, Activity activity)
        {
            CustomerArticle customerArticle = _context.CustomerArticles.FindById(activity.CustomerArticleId, _context.Articles);
            Article articleCopy = _context.Articles.FindByIdWithArticlePath(customerArticle.ArticleCopyId);

            return new ABWOrderOrderDetail
            {
                BuyerProductCode = articleCopy.ArticleCode,
                BuyerProductDescr = activity.Name + " (fixed price)",
                LineTotal = activity.FixedPrice.GetValueOrDefault() * (order.IsCreditNote ? -1 : 1),
                LineTotalSpecified = true,
                Price = activity.FixedPrice.GetValueOrDefault(),
                PriceSpecified = true,
                Quantity = order.IsCreditNote ? -1 : 1,
                DetailInfo = new ABWOrderOrderDetailDetailInfo
                {
                    UseLineTotal = true,
                    UseLineTotalSpecified = true
                }
            };
        }

        public IEnumerable<ABWOrderOrderDetail> GetDetailsForParticipantOrderLines(Order order, List<ParticipantOrderLine> participantOrderLines)
        {
            var participants = participantOrderLines != null
            ? order.ParticipantOrderLines.Select(orderline => orderline.Participant).ToList()
            : new List<Participant>();

            //order.ParticipantOrderLines[0].Price

            var participantActivities = participantOrderLines.Select(p => p.ActivityId).Distinct().ToList();
            var participantsWithInfoElements = participants.Select(orderP => _context.Participants.WithParticipantInfoElements().WithAttachments().FirstOrDefault(p => p.Id == orderP.Id)).ToList();

            foreach (int activityId in participantActivities)
            {
                Activity activity = _context.Activities.FindById(activityId);
                CustomerArticle customerArticle = _context.CustomerArticles.FindById(activity.CustomerArticleId, _context.Articles);
                Article articleCopy = _context.Articles.FindByIdWithArticlePath(customerArticle.ArticleCopyId);
                
                
                var participantsGroupedByUnitPrice =  participantOrderLines.Where(x=>x.Price.HasValue && x.Price!=0 && x.ActivityId == activityId).GroupBy(x => x.Price).ToDictionary(p => p.Key.GetValueOrDefault(), p => p.ToList());

                //var participantsGroupedByUnitPrice = GroupByUnitPrice(activityParticipants);

                foreach (var participantGroup in participantsGroupedByUnitPrice)
                {
                    
                    yield return new ABWOrderOrderDetail
                    {
                        BuyerProductCode = articleCopy.ArticleCode,
                        BuyerProductDescr = activity.Name,
                        ProductSpecification = GetProductSpecifications(participantGroup.Value.Select(x => participantsWithInfoElements.First(y=>y.Id == x.Participant.Id)).ToList()),
                        LineTotal = participantGroup.Key * participantGroup.Value.Count * (order.IsCreditNote ? -1 : 1),
                        LineTotalSpecified = true,
                        Price = participantGroup.Key,
                        PriceSpecified = true,
                        Quantity = participantGroup.Value.Count * (order.IsCreditNote ? -1 : 1),
                        DetailInfo = new ABWOrderOrderDetailDetailInfo
                        {
                            UseLineTotal = true,
                            UseLineTotalSpecified = true
                        }
                    };
                }
            }
        }

        private Dictionary<decimal, List<Participant>> GroupByUnitPrice(IEnumerable<Participant> participants)
        {
            return participants.GroupBy(p => p.UnitPrice).ToDictionary(p => p.Key.GetValueOrDefault(), p => p.ToList());
        }
        
        private tProductSpecification[] GetProductSpecifications(List<Participant> participants)
        {
            var productSpecifications = new List<tProductSpecification>();
            var sequenceNumber = 0;

            foreach (var participant in participants)
            {
                var user = _context.Users.FindById(participant.UserId);

                var orderlineText = user != null ? _orderFactory.GetOrderLineText(user.FirstName, user.LastName, participant) : "";
                var orderlines = AdjustOrderLineToAgressoProductSpecifications(orderlineText);
                productSpecifications.AddRange(orderlines.Select(participantOrderLine =>
                {
                    sequenceNumber = sequenceNumber + 1;
                    return new tProductSpecification
                    {
                        Info = participantOrderLine,
                        SeqNo = sequenceNumber,
                        SeqNoSpecified = true
                    };
                }));
            }
            return productSpecifications.ToArray();
        }

        internal IEnumerable<string> AdjustOrderLineToAgressoProductSpecifications(string orderlineText)
        {
            if (orderlineText.Length <= 60)
            {
                return new List<string> {orderlineText};
            }

            var substrings = orderlineText.Split(new[] {", "}, 2, StringSplitOptions.None);

            var result = new List<string>();
            foreach (var substring in substrings)
            {
                result.AddRange(SplitByLength(substring, 60));
            }

            return result;
        }

        internal static IEnumerable<string> SplitByLength(string str, int maxLength)
        {
            for (int index = 0; index < str.Length; index += maxLength)
            {
                yield return str.Substring(index, Math.Min(maxLength, str.Length - index));
            }
        }
    }
}