﻿using Metier.Phoenix.Api.Services.Agresso.Invoicing.References;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Services.Agresso.Invoicing.Factories
{
    public interface IAgressoOrderFactory
    {
        ABWOrder CreateOrder(Order order, string externalCustomerId, string workOrder, string orderNumber, int? creditedOrderId);
    }
}