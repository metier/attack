﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Services.Agresso.Invoicing.References;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Services.Agresso.Invoicing.Factories
{
    public class AgressoOrderFactory : IAgressoOrderFactory
    {
        private const string SalesOrder = "42";
        private const string VoucherType = "LM";
        private const string WorkOrderCode = "BF";

        private readonly ICodeRepository _codeRepository;
        private readonly IAgressoOrderDetailFactory _agressoOrderDetailFactory;
        
        public AgressoOrderFactory(ICodeRepository codeRepository, IAgressoOrderDetailFactory agressoOrderDetailFactory)
        {
            _codeRepository = codeRepository;
            _agressoOrderDetailFactory = agressoOrderDetailFactory;
        }

        public ABWOrder CreateOrder(Order order, string externalCustomerId, string workOrder, string orderNumber, int? creditedOrderId)
        {
            var abwOrder = new ABWOrder
            {
                Order = new[]
                {   
                    new ABWOrderOrder
                    {
                        TransType = SalesOrder,
                        Period = DateTime.Today.ToString("yyyyMM"),
                        VoucherType = VoucherType,
                        OrderNo = orderNumber,
                        Header = GetHeader(order, externalCustomerId, workOrder, creditedOrderId),
                        Details = GetOrderDetails(order)
                    }
                }
            };

            return abwOrder;
        }

        private ABWOrderOrderHeader GetHeader(Order order, string externalCustomerId, string workOrder, int? creditedOrderId)
        {
            var header = new ABWOrderOrderHeader
            {
                OrderDate = DateTime.Now,
                HeaderText = order.Title,
                OrderDateSpecified = true,
                Currency = GetCurrencyCode(order),
                ExtOrderRef = order.YourRef,
                ExchRate = 1.00F,
                ExchRateSpecified = true,
                Buyer = new ABWOrderOrderHeaderBuyer
                {
                    BuyerNo = externalCustomerId,
                    MainBuyerNo = externalCustomerId,
                    BuyerReferences = new ABWOrderOrderHeaderBuyerBuyerReferences { Accountable = order.YourOrder}
                },
                Seller = new ABWOrderOrderHeaderSeller
                {
                    SellerReferences = new ABWOrderOrderHeaderSellerSellerReferences
                    {
                        Responsible = order.OurRef
                    }
                },
                ReferenceCode = new[]
                {
                    new tReferenceCode
                    {
                        Code = WorkOrderCode,
                        Value = workOrder
                    }
                }
            };

            if (order.IsFullCreditNote && creditedOrderId.HasValue)
            {
                header.ExtOrderId = creditedOrderId.Value.ToString(CultureInfo.InvariantCulture);
            }
            if (order.ParticipantOrderLines != null && order.ParticipantOrderLines.Any(p => !string.IsNullOrEmpty(p.OnlinePaymentReference)))
            {
                header.PayMethod = "CR";
                header.FooterText = "Betalt med kredittkort / Paid by credit card";
            }

            return header;
        }

        private string GetCurrencyCode(Order order)
        {
            if (order.ParticipantOrderLines != null)
            {
                foreach (var participantOrderLine in order.ParticipantOrderLines)
                {
                    if (participantOrderLine.CurrencyCodeId.HasValue)
                    {
                        return _codeRepository.GetCodeValue(participantOrderLine.CurrencyCodeId.Value);
                    }
                }
            }

            if (order.ActivityOrderLines != null)
            {
                foreach (var activityOrderLine in order.ActivityOrderLines)
                {
                    if (activityOrderLine.CurrencyCodeId.HasValue)
                    {
                        return _codeRepository.GetCodeValue(activityOrderLine.CurrencyCodeId.Value);
                    }
                }
            }

            return "";
        }

        private ABWOrderOrderDetail[] GetOrderDetails(Order invoice)
        {
            var orderDetails = new List<ABWOrderOrderDetail>();

            orderDetails.AddRange(CreateOrderDetailsForActivities(invoice));
            orderDetails.AddRange(CreateOrderDetailsForParticipants(invoice));

            SetLineNo(orderDetails);

            return orderDetails.ToArray();
        }

        private IEnumerable<ABWOrderOrderDetail> CreateOrderDetailsForActivities(Order order)
        {
            if(order.ActivityOrderLines == null) 
                order.ActivityOrderLines = new List<ActivityOrderLine>();

            foreach (var activityOrderLine in order.ActivityOrderLines.Where(a => a.Price.HasValue))
            {
                yield return _agressoOrderDetailFactory.GetDetailForActivity(order, activityOrderLine.Activity);
            }
        }

        private IEnumerable<ABWOrderOrderDetail> CreateOrderDetailsForParticipants(Order order)
        {
            return _agressoOrderDetailFactory.GetDetailsForParticipantOrderLines(order, order.ParticipantOrderLines);   
        }

        private static void SetLineNo(IEnumerable<ABWOrderOrderDetail> orderDetails)
        {
            int lineNo = 1;
            foreach (var orderDetail in orderDetails)
            {
                orderDetail.LineNo = lineNo;
                lineNo++;
            }
        }
    }
}