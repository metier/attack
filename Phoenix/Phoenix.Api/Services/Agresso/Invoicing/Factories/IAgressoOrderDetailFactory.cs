﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Services.Agresso.Invoicing.References;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Services.Agresso.Invoicing.Factories
{
    public interface IAgressoOrderDetailFactory
    {
        ABWOrderOrderDetail GetDetailForActivity(Order order, Activity activity);
        IEnumerable<ABWOrderOrderDetail> GetDetailsForParticipantOrderLines(Order order, List<ParticipantOrderLine> participantOrderLines);
    }
}