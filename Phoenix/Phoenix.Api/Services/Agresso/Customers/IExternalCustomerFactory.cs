﻿using Metier.Phoenix.Api.Agresso.CustomerService;
using Metier.Phoenix.Api.Services.ExternalCustomers;

namespace Metier.Phoenix.Api.Services.Agresso.Customers
{
    public interface IExternalCustomerFactory
    {
        ExternalCustomer Create(CustomerObject co);
    }
}