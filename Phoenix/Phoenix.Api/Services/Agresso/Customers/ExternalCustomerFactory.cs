﻿using System.Linq;
using Metier.Phoenix.Api.Agresso.CustomerService;
using Metier.Phoenix.Api.Services.ExternalCustomers;

namespace Metier.Phoenix.Api.Services.Agresso.Customers
{
    public class ExternalCustomerFactory : IExternalCustomerFactory
    {
        public ExternalCustomer Create(CustomerObject co)
        {
            var address = co.AddressList.FirstOrDefault() ?? new AddressUnitType();

            return new ExternalCustomer
            {
                Id = co.CustomerID,
                Name = co.CustomerName,
                CompanyRegistrationNumber = TrimCompanyRegistrationNumber(co.CompanyRegistrationNumber),
                AccountNumber = string.Format("{0}-{1}", co.CustomerGroupID, co.CustomerID),
                AddressStreet1 = address.Address,
                ZipCode = address.ZipCode,
                City = address.Place,
                Country = address.CountryCode
            };
        }

        private string TrimCompanyRegistrationNumber(string companyRegistrationNumber)
        {
            if (companyRegistrationNumber == null || companyRegistrationNumber.Replace(" ", "").Length == 0)
                return null;

            return companyRegistrationNumber.Replace(" ", "");
        }

    }
}