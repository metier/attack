﻿using Metier.Phoenix.Api.Services.ExternalCustomers;

namespace Metier.Phoenix.Api.Services.Agresso.Customers
{
    public interface ISearchAgressoCustomers : ISearchExternalCustomers
    {
    }
}