﻿using System;
using System.Configuration;
using Metier.Phoenix.Api.Agresso.CustomerService;

namespace Metier.Phoenix.Api.Services.Agresso.Configuration.Norway
{
    public class AgressoNorwayConfiguration : IAgressoNorwayConfiguration
    {
        public string Username
        {
            get { return ConfigurationManager.AppSettings["Norway.Agresso.Username"]; }
        }

        public string Password
        {
            get { return ConfigurationManager.AppSettings["Norway.Agresso.Password"]; }
        }

        public string Client
        {
            get { return ConfigurationManager.AppSettings["Norway.Agresso.Client"]; }
        }

        public int InvoiceQueryTemplateId
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["Norway.Agresso.InvoiceQueryTemplateId"]); }
        }

        public int CustomerWorkOrderQueryTemplateId
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["Norway.Agresso.CustomerWorkOrderQueryTemplateId"]); }
        }

        public string IndividualCustomerWorkOrder
        {
            get { return ConfigurationManager.AppSettings["Norway.Agresso.IndividualCustomerWorkOrder"]; }
        }

        public WSCredentials GetCustomerServiceCredentials()
        {
            return new WSCredentials
            {
                Username = Username,
                Password = Password,
                Client = Client
            };
        }

        public Api.Agresso.ImportService.WSCredentials GetImportServiceCredentials()
        {
            return new Api.Agresso.ImportService.WSCredentials
            {
                Username = Username,
                Password = Password,
                Client = Client
            };
        }

        public Api.Agresso.QueryEngine.WSCredentials GetQueryEngineServiceCredentials()
        {
            return new Api.Agresso.QueryEngine.WSCredentials
            {
                Username = Username,
                Password = Password,
                Client = Client
            };
        }
    }
}