﻿using System;
using System.Configuration;
using Metier.Phoenix.Api.Agresso.CustomerService;

namespace Metier.Phoenix.Api.Services.Agresso.Configuration.Sweden
{
    public class AgressoSwedenConfiguration : IAgressoSwedenConfiguration
    {
        public string Username
        {
            get { return ConfigurationManager.AppSettings["Sweden.Agresso.Username"]; }
        }

        public string Password
        {
            get { return ConfigurationManager.AppSettings["Sweden.Agresso.Password"]; }
        }

        public string Client
        {
            get { return ConfigurationManager.AppSettings["Sweden.Agresso.Client"]; }
        }

        public int InvoiceQueryTemplateId
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["Sweden.Agresso.InvoiceQueryTemplateId"]); }
        }

        public int CustomerWorkOrderQueryTemplateId
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["Sweden.Agresso.CustomerWorkOrderQueryTemplateId"]); }
        }

        public string IndividualCustomerWorkOrder
        {
            get { return ConfigurationManager.AppSettings["Sweden.Agresso.IndividualCustomerWorkOrder"]; }
        }

        public WSCredentials GetCustomerServiceCredentials()
        {
            return new WSCredentials
            {
                Username = Username,
                Password = Password,
                Client = Client
            };
        }

        public Api.Agresso.ImportService.WSCredentials GetImportServiceCredentials()
        {
            return new Api.Agresso.ImportService.WSCredentials
            {
                Username = Username,
                Password = Password,
                Client = Client
            };
        }

        public Api.Agresso.QueryEngine.WSCredentials GetQueryEngineServiceCredentials()
        {
            return new Api.Agresso.QueryEngine.WSCredentials
            {
                Username = Username,
                Password = Password,
                Client = Client
            };
        }
    }
}