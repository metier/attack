﻿using Metier.Phoenix.Api.Services.ExternalCustomers;
using Metier.Phoenix.Api.Services.Invoices;

namespace Metier.Phoenix.Api.Services.Agresso.Configuration
{
    public interface IAgressoConfigurationProvider
    {
        IAgressoConfiguration GetConfiguration(OrderProviders providers);
    }
}