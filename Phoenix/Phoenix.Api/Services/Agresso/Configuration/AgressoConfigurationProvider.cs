﻿using Metier.Phoenix.Api.Services.Agresso.Configuration.Norway;
using Metier.Phoenix.Api.Services.Agresso.Configuration.Sweden;
using Metier.Phoenix.Api.Services.ExternalCustomers;
using Metier.Phoenix.Api.Services.Invoices;

namespace Metier.Phoenix.Api.Services.Agresso.Configuration
{
    public class AgressoConfigurationProvider : IAgressoConfigurationProvider
    {
        public IAgressoConfiguration GetConfiguration(OrderProviders providers)
        {
            switch (providers)
            {
                case OrderProviders.MetierAcademyNorwayAgresso:
                    return new AgressoNorwayConfiguration();
                case OrderProviders.MetierAcademySwedenAgresso:
                    return new AgressoSwedenConfiguration();
                default:
                    return new AgressoNorwayConfiguration();
            }
        }
    }
}