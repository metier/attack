﻿namespace Metier.Phoenix.Api.Services.Agresso.Configuration
{
    public interface IAgressoConfiguration
    {
        string Username { get; }
        string Password { get; }
        string Client { get; }

        int InvoiceQueryTemplateId { get; }
        int CustomerWorkOrderQueryTemplateId { get; }
        string IndividualCustomerWorkOrder { get; }

        Api.Agresso.CustomerService.WSCredentials GetCustomerServiceCredentials();
        Api.Agresso.ImportService.WSCredentials GetImportServiceCredentials();
        Api.Agresso.QueryEngine.WSCredentials GetQueryEngineServiceCredentials();
    }
}