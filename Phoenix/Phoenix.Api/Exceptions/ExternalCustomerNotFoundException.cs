﻿using System;

namespace Metier.Phoenix.Api.Exceptions
{
    public class ExternalCustomerNotFoundException : BadRequestException
    {
        public ExternalCustomerNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public ExternalCustomerNotFoundException(string message) : base(message)
        {}
    }
}