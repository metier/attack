﻿using System;

namespace Metier.Phoenix.Api.Exceptions
{
    public class AmbiguousExternalCustomerException: BadRequestException
    {
        public AmbiguousExternalCustomerException(string message) : base(message)
        {
        }

        public AmbiguousExternalCustomerException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}