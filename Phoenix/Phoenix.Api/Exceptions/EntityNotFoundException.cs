﻿using System;

namespace Metier.Phoenix.Api.Exceptions
{
    public class EntityNotFoundException : ApplicationException
    {
        public EntityNotFoundException(string message) : base(message)
        {}
    }
}