﻿using System;

namespace Metier.Phoenix.Api.Exceptions
{
    public class UsernameChangeException : ApplicationException
    {
        public UsernameChangeException(string message) : base(message)
        {}
    }
}