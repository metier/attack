﻿using System;

namespace Metier.Phoenix.Api.Exceptions
{
    public class RoleUpdateException : ApplicationException
    {
        public RoleUpdateException(string message) : base(message)
        {}
    }
}