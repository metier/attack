﻿using System;

namespace Metier.Phoenix.Api.Exceptions
{
    public class InvoicePreconditionException : ApplicationException
    {
        public InvoicePreconditionException(string message) : base(message)
        {}
    }
}