﻿using System;

namespace Metier.Phoenix.Api.Exceptions
{
    public class CreateExternalCustomerException : ApplicationException
    {
        public CreateExternalCustomerException(string message) : base(message)
        {}
    }
}