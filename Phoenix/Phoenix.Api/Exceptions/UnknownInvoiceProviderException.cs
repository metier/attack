﻿using System;

namespace Metier.Phoenix.Api.Exceptions
{
    public class UnknownInvoiceProviderException : Exception
    {
        public UnknownInvoiceProviderException(string message) : base(message)
        {}
    }
}