﻿using System;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Metier.Phoenix.Api.App_Start;
using Metier.Phoenix.Api.Configuration;
using Metier.Phoenix.Api.Filters;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Mail;
using NLog;

namespace Metier.Phoenix.Api
{
    public class MvcApplication : HttpApplication
    {
        private static IWindsorContainer _container;

        public MvcApplication() : this(new ComponentsInstaller())
        {
        }

        public MvcApplication(ComponentsInstaller componentsInstaller)
        {
            _container = new WindsorContainer().Install(
                FromAssembly.Containing<IAutoMailer>(),
                componentsInstaller);
        }

        protected void Application_Start()
        {
            // Can be used if you need to see Web Service requests in Fiddler invoked by the API 
            //GlobalProxySelection.Select = new WebProxy("127.0.0.1", 8888);
            // Only allow JSON:
            GlobalConfiguration.Configuration.Formatters.Clear();
            GlobalConfiguration.Configuration.Formatters.Add(new JsonMediaTypeFormatter());

            var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            json.SerializerSettings.DateFormatHandling= Newtonsoft.Json.DateFormatHandling.IsoDateFormat;
            
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerActivator), new WindsorCompositionRoot(_container));

            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configuration.Filters.Add(new PhoenixAuthorizeAttribute(_container));
            GlobalConfiguration.Configuration.Filters.Add(new ExceptionHandlerFilter());
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            GlobalConfiguration.Configuration.EnsureInitialized();

            Logger logger = LogManager.GetLogger("Metier.Phoenix.Api");
            logger.Info("Application Started!");
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            AddCrossOriginSharingHeaders();
        }

        private static void AddCrossOriginSharingHeaders()
        {
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");

            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            {
                //These headers are handling the "pre-flight" OPTIONS call sent by the browser
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept, PhoenixAuthToken");
                HttpContext.Current.Response.AddHeader("Access-Control-Max-Age", "1728000");
                HttpContext.Current.Response.End();
            }
        }

        public override void Dispose()
        {
            _container.Dispose();
            base.Dispose();
        }
    }
}