using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using Metier.Phoenix.Api.Areas.HelpPage.Models;
using Metier.Phoenix.Api.Security;

namespace Metier.Phoenix.Api.Areas.HelpPage.Controllers
{
    /// <summary>
    /// The controller that will handle requests for the help page.
    /// </summary>
    public class HelpController : Controller
    {
        public HelpController()
            : this(GlobalConfiguration.Configuration)
        {
        }

        public HelpController(HttpConfiguration config)
        {
            Configuration = config;
        }

        public HttpConfiguration Configuration { get; private set; }

        public ActionResult Index()
        {
            ViewBag.DocumentationProvider = Configuration.Services.GetDocumentationProvider();
            var helpItems = Configuration.Services.GetApiExplorer().ApiDescriptions.Select(d => new HelpItemApiModel
            {
                ApiDescription = d,
                AuthorizedRoles = GetAuthroizedRoles(d)
            }).OrderBy(d => d.ApiDescription.ActionDescriptor.ControllerDescriptor.ControllerName);
            return View(helpItems);
        }

        public ActionResult Api(string apiId)
        {
            if (!String.IsNullOrEmpty(apiId))
            {
                HelpPageApiModel apiModel = Configuration.GetHelpPageApiModel(apiId);
                if (apiModel != null)
                {
                    apiModel.AuthorizedRoles = GetAuthroizedRoles(apiModel.ApiDescription);
                    return View(apiModel);
                }
            }

            return View("Error");
        }

        private string GetAuthroizedRoles(ApiDescription description)
        {
            var anonymous = description.ActionDescriptor.GetCustomAttributes<System.Web.Http.AllowAnonymousAttribute>().Any() || 
                            description.ActionDescriptor.ControllerDescriptor.GetCustomAttributes<System.Web.Http.AllowAnonymousAttribute>().Any();
            if (anonymous)
            {
                return "Anonymous";
            }

            var actionAllowRolesAttributes = description.ActionDescriptor.GetCustomAttributes<AllowRolesAttribute>();
            var controllerAllowRolesAttributes = description.ActionDescriptor.ControllerDescriptor.GetCustomAttributes<AllowRolesAttribute>();
            var roles = new[] { PhoenixRoles.PhoenixUser }.Concat(actionAllowRolesAttributes.Concat(controllerAllowRolesAttributes).SelectMany(r => r.AuthorizeRoles)).Distinct().ToList();
            
            if (PhoenixRoles.All.Split(',').All(roles.Contains))
            {
                return "All";
            }
            
            var rolesText = string.Join(", ", roles);
            return rolesText;
        }
    }
}