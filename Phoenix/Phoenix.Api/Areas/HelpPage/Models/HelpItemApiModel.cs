﻿using System.Web.Http.Description;

namespace Metier.Phoenix.Api.Areas.HelpPage.Models
{
    public class HelpItemApiModel
    {
        public string AuthorizedRoles { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="ApiDescription"/> that describes the API.
        /// </summary>
        public ApiDescription ApiDescription { get; set; }
    }
}