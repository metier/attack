﻿using System.IO;
using System.Text;

namespace Metier.Phoenix.Api.Utilities
{
    public sealed class Utf8StringWriter : StringWriter
    {
        public override Encoding Encoding { get { return Encoding.UTF8; } }
    }
}