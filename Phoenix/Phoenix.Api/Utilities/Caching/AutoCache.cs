﻿using System;
using System.Threading;
using NLog;


namespace Metier.Phoenix.Api.Utilities.Caching
{
    public class AutoCache<T> where T : class
    {
        private T _cachedData;
        public Func<T> DataProvider { get; set; }
        public Timer Timer { get; set; }
        public string Name { get; set; }

        public bool HasData
        {
            get { return _cachedData != null; }
        }

        public AutoCache()
        {
        }

        public AutoCache(string cacheName, Func<T> dataProvider, int refreshEveryMilliseconds)
        {
            Init(cacheName, dataProvider, refreshEveryMilliseconds);
        }

        public void Init(string cacheName, Func<T> dataProvider, int refreshEveryMilliseconds)
        {
            Name = cacheName;
            DataProvider = dataProvider;

            Timer = new Timer(o => ReloadData(), null, 0, refreshEveryMilliseconds); 
        }

        private void ReloadData()
        {
            lock (DataProvider)
            {
                try
                {
                    _cachedData = DataProvider();
                }
                catch (Exception e)
                {
                    Logger logger = LogManager.GetLogger(GetType().ToString());
                    logger.ErrorException(string.Format("{0} failed to load data", Name), e);
                }
            }
        }

        public void SetData(T cacheData)
        {
            lock (DataProvider)
            {
                _cachedData = cacheData;
            }
        }

        public T GetData()
        {
            if (_cachedData != null)
            {
                return _cachedData;    
            }
            lock (DataProvider)
            {
                if (_cachedData == null)
                {
                    ReloadData();
                }
            }
            return _cachedData;
        }
    }
}