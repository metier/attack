﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Metier.Phoenix.Api.Utilities.Caching
{
    public class AutoCacheCollection<T> where T : class
    {
        private readonly List<AutoCache<T>> _caches = new List<AutoCache<T>>();

        public void AddCache(string cacheName, Func<T> dataProvider, int refreshEveryMilliseconds)
        {
            _caches.Add(new AutoCache<T>(cacheName, dataProvider, refreshEveryMilliseconds));
        }

        public void Set(string cacheName, T cacheData)
        {
            var autoCache = _caches.FirstOrDefault(c => c.Name == cacheName);
            if (autoCache != null)
            {
                autoCache.SetData(cacheData);
            }
        }
        
        public T Get(string cacheName)
        {
            var autoCache = _caches.FirstOrDefault(c => c.Name == cacheName);
            if (autoCache != null)
            {
                return autoCache.GetData();
            }
            return null;
        }
    }
}