﻿using System.Collections.Generic;
using System.Web.Http;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// Handles product category related operations
    /// </summary>
    public class ProductCategoriesController : ApiController
    {
        private readonly IProductCategoryRepository _productCategoryRepository;

        public ProductCategoriesController(IProductCategoryRepository productCategoryRepository)
        {
            _productCategoryRepository = productCategoryRepository;
        }

        /// <summary>
        /// Returns a list of all the product categories registered in the Phoenix API
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("productcategories")]
        public IEnumerable<ProductCategory> ProductCategories()
        {
            return _productCategoryRepository.GetProductCategories();
        } 
    }
}