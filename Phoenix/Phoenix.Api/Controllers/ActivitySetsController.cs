﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Castle.Core.Logging;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Facade.Enrollment;
using Metier.Phoenix.Api.Facade.Participant;
using Metier.Phoenix.Api.Messaging;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Exceptions;
using NLog;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// Activity set related operations.<br/><br/>
    /// 
    /// An activity set consists of a collection of activities to which users can enroll to in a particular time frame. It is being used as part of a customer's course program.
    /// </summary>
    public class ActivitySetsController : ControllerBase
    {
        private readonly IActivitySetRepository _activitySetRepository;
        private readonly IActivitySetProvider _activitySetProvider;
        private readonly IEventAggregator _eventAggregator;
        private readonly IAuthorizationProvider _authorizationProvider;
        private readonly ICustomersProvider _customersProvider;
        private readonly IUsersRepository _usersRepository;
        private readonly Logger _logger;

        public ActivitySetsController(IActivitySetRepository activitySetRepository, 
                                      IActivitySetProvider activitySetProvider,
                                      IEventAggregator eventAggregator,
                                      IAuthorizationProvider authorizationProvider,
                                      ICustomersProvider customersProvider,
                                      IUsersRepository usersRepository)
        {
            _activitySetRepository = activitySetRepository;
            _activitySetProvider = activitySetProvider;
            _eventAggregator = eventAggregator;
            _authorizationProvider = authorizationProvider;
            _customersProvider = customersProvider;
            _usersRepository = usersRepository;
            _logger = LogManager.GetCurrentClassLogger();

        }

        /// <summary>
        /// Returns a representation of the ActivitySet entity. Can be used for CRUD operations
        /// </summary>
        /// <param name="id">ActivitySet id</param>
        [HttpGet]
        [Route("activitysets/{id}")]
        [AllowRoles(PhoenixRoles.LearningPortal, PhoenixRoles.LearningPortalTokenUser)]
        public ActivitySet GetActivitySet(int id)
        {
            var activitySet = _activitySetRepository.FindById(id);
            if (activitySet == null)
            {
                throw CreateFaultException(HttpStatusCode.NotFound, string.Format("Could not find any activitysets with ID {0}", id));
            }
            return activitySet;
        }

        /// <summary>
        /// Returns a search result of activity sets qualifying the specified search parameters
        /// </summary>
        /// <param name="customerId">Customer id</param>
        /// <param name="query">Free text search on the "Name" column  (optional)</param>
        /// <param name="orderBy">Column to order result by ("name", "publishfrom", "publishto", "enrollmentfrom", "enrollmentto", "ispublished") (optional, default = ActivitySet id)</param>
        /// <param name="orderDirection">Order of the result ("asc", "desc") (optional)</param>
        /// <param name="instructorUserId">The instructor of the activitySet (optional)</param>
        /// <param name="ecoachUserId">The ecoach of the activitySet (optional)</param>
        /// <param name="skip">Index of first item in result (optional)</param>
        /// <param name="limit">Max number of items in result (optional, default = 20)</param>
        /// <param name="courseCalendar">Returns an ordered list of activity set as configured in the customer's course calendar (optional, default = false)</param>
        /// <param name="calculateAvailableSpots">Calculates number of enrollments and number of remaining spots for each activity and the activitySet (optional, default = false)</param>
        /// <param name="includeResources">Should resources be included in result (optional, default = false)</param>
        [HttpGet]
        [Route("activitysets")]
        [AllowRoles(PhoenixRoles.LearningPortal, PhoenixRoles.EzTokenUser)]
        [ResponseType(typeof(IEnumerable<ActivitySet>))]
        public HttpResponseMessage GetActivitySets([FromUri] int? customerId = null, 
                                                   [FromUri] string query = null,
                                                   [FromUri] string orderBy = null,
                                                   [FromUri] string orderDirection = null,
												   [FromUri] int? instructorUserId = null,
												   [FromUri] int? ecoachUserId = null,
                                                   [FromUri] int? skip = null,
                                                   [FromUri] int? limit = 20,
                                                   [FromUri] bool courseCalendar = false,
                                                   [FromUri] bool calculateAvailableSpots = false,
                                                   [FromUri] bool includeResources = false)
        {
            if (courseCalendar)
            {
                if (customerId == null) throw new System.Exception("Calling activitysets search function with the courseCalendar flag requires a valid customerId.");
				//Todo: This should be extracted as a separate method call. It is confusing to allow all these parameters when they're not checked
                return Request.CreateResponse(HttpStatusCode.OK, _activitySetRepository.GetCourseCalendar(customerId.Value, calculateAvailableSpots, includeResources));
            }
            return Request.CreateResponse(HttpStatusCode.OK, _activitySetRepository.Find(customerId, query, orderBy, orderDirection, instructorUserId, ecoachUserId, calculateAvailableSpots, skip, limit));
        }

        /// <summary>
        /// Creates a new activity set. Part of the CRUD operations for this resource.
        /// </summary>
        /// <param name="activitySet">The activity set to create</param>
        /// <returns>The representation of the activity set resource after persisting it to the API</returns>
        [HttpPost]
        [Route("activitysets")]
        [ResponseType(typeof(ActivitySet))]
        public HttpResponseMessage CreateActivitySet(ActivitySet activitySet)
        {
            try
            {
                var created = _activitySetRepository.Create(activitySet);
                return Request.CreateResponse(HttpStatusCode.Created, created);
            }
            catch (ValidationException e)
            {
                return GetInvalidActivitySetResult(e);
            }
        }

        /// <summary>
        /// Adds an activity to the activity sets and enrolls participants of the activity set to the activity
        /// </summary>
        /// <param name="id">ActivitySet id</param>
        /// <param name="activityId">Activity id</param>
        [HttpPut]
        [Route("activitysets/{id}/addactivity")]
        [ResponseType(typeof(ActivitySet))]
        public HttpResponseMessage AddActivity(int id, [FromUri] int activityId)
        {
            try
            {
                var updated = _activitySetProvider.AddActivity(id, activityId);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
            catch (ValidationException e)
            {
                return GetInvalidActivitySetResult(e);
            }
        }
        
        /// <summary>
        /// Adds an activity to the activity sets and removes participants of the activity set from the activity
        /// </summary>
        /// <param name="id">ActivitySet id</param>
        /// <param name="activityId">Activity id</param>
        [HttpPut]
        [Route("activitysets/{id}/removeactivity")]
        [ResponseType(typeof(ActivitySet))]
        public HttpResponseMessage RemoveActivity(int id, [FromUri] int activityId)
        {
            try
            {
                var updated = _activitySetProvider.RemoveActivity(id, activityId);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
            catch (ValidationException e)
            {
                return GetInvalidActivitySetResult(e);
            }
        }

        private void ValidateEnrollmentPermission(int userId, int []activitySetIds)
        {
            if (!_authorizationProvider.HasAnyRole(PhoenixRoles.Phoenix, PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.EzTokenUser))
            {
                if (!_authorizationProvider.Is(userId))
                    throw new Exceptions.UnauthorizedException("You can't enroll another than yourself when you are a LearningPortal user");
                
                //Validate that activityset is in published course calendar
                var user = _authorizationProvider.GetCurrentUser();
                var customer = _customersProvider.GetCustomer(user.CustomerId);
                var calendar = _activitySetRepository.GetCourseCalendar(user.CustomerId, false, false);

                activitySetIds.ToList().ForEach(id => {

                    var activitySet = calendar.FirstOrDefault(a => a.Id == id);

                    if (activitySet == null)
                        throw new Exceptions.EntityNotFoundException($"ActivitySet {id} was not found in the published course calendar for customer {customer.Id}");

                    var isExam = false;
                    if (activitySet.Activities != null) isExam = activitySet.Activities.Any(a => a.CustomerArticle != null && a.CustomerArticle.ExamId.HasValue);

                    //Validate that this user can self enroll
                    if (isExam && (!customer.ParticipantInfoDefinition.IsAllowUsersToSelfEnrollToExams.HasValue || !customer.ParticipantInfoDefinition.IsAllowUsersToSelfEnrollToExams.Value))
                        throw new Exceptions.UnauthorizedException("You are not allowed to self enroll in an exam");

                    if (!isExam && (!customer.ParticipantInfoDefinition.IsAllowUsersToSelfEnrollToCourses.HasValue || !customer.ParticipantInfoDefinition.IsAllowUsersToSelfEnrollToCourses.Value))
                        throw new Exceptions.UnauthorizedException("You are not allowed to self enroll in an course");
                    

                });
                
            }
        }

        /// <summary>
        /// Enrolls a user to an activity set. Creates participations to each activity this activity set consist of
        /// </summary>
        /// <param name="id">ActivitySet id</param>
        /// <param name="userId">User id</param>
        /// <param name="enrollmentDetails">May include a list of infoelements to use for the participants, and/or overridden prices for each activity (optional)</param>
        /// <param name="paymentReference">Payment reference for online payments. If this is specified the participation will be invoiced immediately and marked as paid by credit card (optional)</param>
        /// <param name="customerId">Customer id. Specified if different from customer user belongs to (optional)</param>
        [HttpPut]
        [Route("activitysets/{id}/enroll")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.EzTokenUser, PhoenixRoles.LearningPortal)]
        [ResponseType(typeof(ActivitySet))]
        public HttpResponseMessage Enroll(int id, [FromUri] int userId, EnrollmentDetails enrollmentDetails, [FromUri] string paymentReference = null, [FromUri] int? customerId = null)
        {
            try
            {
                _logger.Info($"Enrolling activitySet ID: {id}, UserId: {userId}");
                ValidateEnrollmentPermission(userId, new int[] { id });                                    

                var updated = _activitySetProvider.Enroll(id, userId, enrollmentDetails, paymentReference, customerId);
                _eventAggregator.Publish(new UserEnrolledEvent { ParticipantIds = updated.Item2 });
                return Request.CreateResponse(HttpStatusCode.OK, updated.Item1);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
            catch (ValidationException e)
            {
                return GetInvalidActivitySetResult(e);
            }
            catch (UnauthorizedException e)
            {
                throw CreateFaultException(HttpStatusCode.Unauthorized, e.Message);
            }
        }

        /// <summary>
        /// Enrolls a user to a number of activity sets. Creates participations to each activity these activity sets consist of
        /// </summary>
        /// <param name="activitySetId">A set of ActivitySet ids</param>
        /// <param name="userId">User id</param>
        /// <param name="enrollmentDetails">May include a list of infoelements to use for the participants, and/or overridden prices for each activity (optional)</param>
        /// <param name="paymentReference">Payment reference for online payments. If this is specified the participation will be invoiced immediately and marked as paid by credit card (optional)</param>
        /// <param name="customerId">Customer id. Specified if different from customer user belongs to (optional)</param>
        [HttpPut]
        [Route("activitysets/enroll")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.EzTokenUser)]
        [ResponseType(typeof(IEnumerable<ActivitySet>))]
        public HttpResponseMessage Enroll([FromUri] int[] activitySetId, [FromUri] int userId, EnrollmentDetails enrollmentDetails, [FromUri] string paymentReference = null, [FromUri] int? customerId = null)
        {
            try
            {
                
                ValidateEnrollmentPermission(userId, activitySetId);
                                
                var updated = _activitySetProvider.Enroll(activitySetId, userId, enrollmentDetails, paymentReference, customerId);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
            catch (ValidationException e)
            {
                return GetInvalidActivitySetResult(e);
            }
            catch (UnauthorizedException e)
            {
                throw CreateFaultException(HttpStatusCode.Unauthorized, e.Message);
            }
        }

        /// <summary>
        /// Unenroll a user from an activity set. Also removes the participations to the activities in the activity set unless the user is enrolled to another activity 
        /// set containing this activity
        /// </summary>
        /// <param name="id">ActivitySet id</param>
        /// <param name="userId">User id</param>
        [HttpPut]
        [Route("activitysets/{id}/unenroll")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
        [ResponseType(typeof(ActivitySet))]
        public HttpResponseMessage Unenroll(int id, [FromUri] int userId)
        {
            try
            {
                var updated = _activitySetProvider.Unenroll(id, userId);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
            catch (ValidationException e)
            {
                return GetInvalidActivitySetResult(e);
            }
            catch (UnauthorizedException e)
            {
                throw CreateFaultException(HttpStatusCode.Unauthorized, e.Message);
            }
        }

        /// <summary>
        /// <p>Updates an existing activity set. Part of the CRUD operations for this resource.</p>
        /// <p>The activity set returned from the resource's GET operation can be used for this update process.</p>
        /// <p><span class="caution">CAUTION: Unspecified properties and list items will be deleted.</span></p>
        /// </summary>
        /// <param name="id">ActivitySet id</param>
        /// <param name="activitySet">The activity set to update</param>
        [HttpPut]
        [Route("activitysets/{id}")]
        [ResponseType(typeof(ActivitySet))]
        public HttpResponseMessage UpdateActivitySet(int id, ActivitySet activitySet)
        {
            try
            {
                var updated = _activitySetRepository.Update(id, activitySet);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
            catch (ValidationException e)
            {
                return GetInvalidActivitySetResult(e);
            }
        }

        /// <summary>
        /// Shares an activity set to another customer. This activity set becomes part of this customer's course program, enabling users of this customer to enroll to the activity set.
        /// </summary>
        /// <param name="id">ActivitySet id</param>
        /// <param name="customerId">Customer id</param>
        [HttpPut]
        [Route("activitysets/{id}/share/{customerId}")]
        [ResponseType(typeof(ActivitySet))]
        public HttpResponseMessage Share(int id, int customerId)
        {
            try
            {
                var updated = _activitySetProvider.Share(id, customerId);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            catch (EntityNotFoundException e)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
        }

        /// <summary>
        /// Unshares an activity set previously shared with another customer
        /// </summary>
        /// <param name="id">ActivitySet id</param>
        /// <param name="customerId">Customer id</param>
        [HttpPut]
        [Route("activitysets/{id}/unshare/{customerId}")]
        [ResponseType(typeof(ActivitySet))]
        public HttpResponseMessage Unshare(int id, int customerId)
        {
            try
            {
                var updated = _activitySetProvider.Unshare(id, customerId);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            catch (EntityNotFoundException e)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
        }
        
        private HttpResponseMessage GetInvalidActivitySetResult(ValidationException ex)
        {
            var invalidActivitySetResult = Request.CreateResponse(HttpStatusCode.BadRequest, new Fault("Not a valid activityset")
            {
                ValidationErrors = ex.ValidationResults.Select(e => e.ErrorMessage).ToList()
            });
            return invalidActivitySetResult;
        }

        
    }
}