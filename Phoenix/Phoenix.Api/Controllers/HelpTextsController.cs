﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Handles help text operations in the API</p>
    /// <p>A help text is a general piece of information which can be associated to GUI elements in Phoenix</p>
    /// </summary>
    public class HelpTextsController : ControllerBase
    {
        private readonly IHelpTextRepository _helpTextRepository;

        public HelpTextsController(IHelpTextRepository helpTextRepository)
        {
            _helpTextRepository = helpTextRepository;
        }

        /// <summary>
        /// <p>Retrieves a specific help text. A help text is identified by a pre-configured known section and name</p>
        /// </summary>
        /// <param name="section">Section is a string for grouping help messages</param>
        /// <param name="name">Name is a string for a specific help text withing a group</param>
        [HttpGet]
        [Route("helptexts")]
        [ResponseType(typeof(HelpText))]
        public HttpResponseMessage Get([FromUri] string section, [FromUri] string name)
        {
            var helpText = _helpTextRepository.Get(section, name);
            if (helpText == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            return Request.CreateResponse(HttpStatusCode.OK, helpText);
        }
        
        /// <summary>
        /// Creates a new help text. Part of the CRUD operations for this resource.
        /// </summary>
        /// <param name="helpText">The help text to create</param>
        /// <returns>The help text data after persisting it to the API</returns>
        [HttpPost]
        [Route("helptexts")]
        [ResponseType(typeof(HelpText))]
        public HttpResponseMessage Create(HelpText helpText)
        {
            var created = _helpTextRepository.Create(helpText);
            return Request.CreateResponse(HttpStatusCode.Created, created);
        }

        /// <summary>
        /// <p>Updates an existing help text. Part of the CRUD operations for this resource.</p>
        /// <p>The help text returned from the resource's GET operation can be used for this update process.</p>
        /// <p><span class="caution">CAUTION: Unspecified properties and list items will be deleted.</span></p>
        /// </summary>
        /// <param name="id">Help text id</param>
        /// <param name="helpText">Updated helpt text object</param>
        /// <returns>The help text data after persisting it to the API</returns>
        [HttpPut]
        [Route("helptexts/{id}")]
        public HelpText Update(int id, HelpText helpText)
        {
            return _helpTextRepository.Update(id, helpText);            
        }
    }
}