﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Api.Facade;


namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Customer article related operations</p>
    /// <p>A customer article is a customer tailored article. The customer article is derived from an original article, can be associated to modules and exams in a course program 
    /// and contains activities for these. The properties of the original article can be overridden in the customer article.</p>
    /// </summary>
    public class CustomerArticlesController : ControllerBase
    {
        private readonly ICustomerArticlesProvider _customerArticlesProvider;

        public CustomerArticlesController(ICustomerArticlesProvider customerArticlesProvider)
        {
            _customerArticlesProvider = customerArticlesProvider;
        }

        /// <summary>
        /// Returns a representation of the customer article entity. Can be used for CRUD operations.
        /// </summary>
        /// <param name="id">Customer article id.</param>
        [HttpGet]
        [Route("customerarticles/{id}")]
        [AllowRoles(PhoenixRoles.LearningPortal, PhoenixRoles.LearningPortalTokenUser)]
        public CustomerArticle Get(int id)
        {
            var customerArticle = _customerArticlesProvider.Get(id);
            if (customerArticle == null)
            {
                throw CreateFaultException(HttpStatusCode.NotFound, string.Format("Could not find any customer articles with ID {0}", id));
            }
            return customerArticle;
        }

        /// <summary>
        /// Returns all customer articles associated with the given exam module in a customer's course program.
        /// </summary>
        /// <param name="examId">The exam module id.</param>
        [HttpGet]
        [Route("customerarticles")]
        [AllowRoles(PhoenixRoles.LearningPortal)]
        public IEnumerable<CustomerArticle> GetByExamId([FromUri] int examId)
        {
            return _customerArticlesProvider.FindByExamId(examId);
        }

        /// <summary>
        /// Returns all customer articles associated with the given course module in a customer's course program.
        /// </summary>
        /// <param name="moduleId">The course module id.</param>
        [HttpGet]
        [Route("customerarticles")]
        [AllowRoles(PhoenixRoles.LearningPortal)]
        public IEnumerable<CustomerArticle> GetByModuleId([FromUri] int moduleId)
        {
            return _customerArticlesProvider.FindByModuleId(moduleId);
        }

        /// <summary>
        /// Returns a list of customer articles (simplified data structure) derivced from a given original article.
        /// </summary>
        /// <param name="articleId">(Original) Article id.</param>
        /// <param name="distributorId">Distributor id. If provided customer article will be filtered to given distributor (optional).</param>
        [HttpGet]
        [Route("customerarticles")]
        [AllowRoles(PhoenixRoles.LearningPortal)]
        public IEnumerable<SearchableCustomerArticle> GetByArticleId([FromUri] int articleId, [FromUri] int? distributorId = null)
        {
            return _customerArticlesProvider.FindBasedOnArticle(articleId, distributorId);
        }

        /// <summary>
        /// <p>Creates a new customer article. Part of the CRUD operations for this resource.</p>
        /// </summary>
        /// <param name="customerArticle">The customer article to create.</param>
        /// <returns>The customer article after persisting it to the API.</returns>
        [HttpPost]
        [Route("customerarticles")]
        [ResponseType(typeof (CustomerArticle))]
        public HttpResponseMessage Create(CustomerArticle customerArticle)
        {
            CustomerArticle created;
            try
            {
                created = _customerArticlesProvider.Create(customerArticle);
            }
            catch (BadRequestException e)
            {
                throw CreateFaultException(HttpStatusCode.BadRequest, e.Message);
            }
            catch (DbUpdateException dbx)
            {
                throw CreateFaultException(HttpStatusCode.BadRequest, "Customer article is not valid");
            }

            var response = Request.CreateResponse(HttpStatusCode.Created, created);
            return response;
        }

        /// <summary>
        /// <p>Updates an existing customer article. Part of the CRUD operations for this resource.</p>
        /// <p>The customer article returned from the resource's GET operation can be used for this update process.</p>
        /// <p><span class="caution">CAUTION: Unspecified properties and list items will be deleted.</span></p>
        /// </summary>
        /// <param name="id">Customer article id.</param>
        /// <param name="customerArticle">The customer article to update.</param>
        [HttpPut]
        [Route("customerarticles/{id}")]
        [ResponseType(typeof (CustomerArticle))]
        public HttpResponseMessage Update(int id, CustomerArticle customerArticle)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, _customerArticlesProvider.Update(customerArticle));
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Not a valid order", e.ValidationResults.ToArray());
            }
            catch (DbUpdateConcurrencyException)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, "A newer record of the customer article exists");
            }
            catch (DbUpdateException)
            {
                throw CreateFaultException(HttpStatusCode.BadRequest, "Customer article is not valid");
            }
        }

        #region Enrollment conditions

        /// <summary>
        /// Retrieves all enrollmentconditions for the given customer article
        /// </summary>
        /// <param name="id">Customer article id</param>
        [HttpGet]
        [Route("customerarticles/{id}/enrollmentconditions")]
        [ResponseType(typeof (List<CustomerArticleEnrollmentCondition>))]
        public List<CustomerArticleEnrollmentCondition> EnrollmentConditions(int id)
        {
            return _customerArticlesProvider.GetEnrollmentConditions(id).ToList();
        }

        /// <summary>
        /// Saves enrollment conditions for a given customer article.
        /// To delete items, remove them from the collection and the save-logic will identify them as 'missing' and delete them.
        /// </summary>
        /// <param name="enrollmentConditions"></param>
        /// <param name="id">The id of the customer article</param>
        [HttpPut]
        [Route("customerarticles/{id}/enrollmentconditions")]
        [AllowRoles(PhoenixRoles.PhoenixUser)]
        public HttpResponseMessage SaveEnrollmentConditions(IEnumerable<CustomerArticleEnrollmentCondition> enrollmentConditions, int id)
        {
            try
            {
                _customerArticlesProvider.SaveEnrollmentConditions(enrollmentConditions, id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (ArgumentException exception)
            {
                var responseMessage = exception.Message;
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadRequest, new Fault(responseMessage)));
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e.ValidationResults.ToArray());
            }
        }


        #endregion

    }
}