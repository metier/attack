﻿using System.Net;
using System.Web.Http;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// Handles operations related to SCORM performance objects in Phoenix
    /// </summary>
    public class PerformancesController : ControllerBase
    {
        private readonly IPerformanceProvider _performanceProvider;

        public PerformancesController(IPerformanceProvider performanceProvider)
        {
            _performanceProvider = performanceProvider;
        }

        /// <summary>
        /// Gets a performance
        /// </summary>
        /// <param name="userId">The user id</param>
        /// <param name="rcoId">The id of the RCO course or lesson</param>
        /// <returns></returns>
        [HttpGet]
        [Route("performances/{userId}/{rcoId}")]
        public ScormPerformance Get(int userId, int rcoId)
        {
            var performance = _performanceProvider.GetPerformance(userId, rcoId);
            if (performance == null)
            {
                throw CreateFaultException(HttpStatusCode.NotFound, "Performance not found");
            }
            return performance;
        }

        /// <summary>
        /// Registers info about a SCORM Performance
        /// </summary>
        /// <param name="userId">The user id</param>
        /// <param name="rcoId">The id of the RCO course or lesson</param>
        /// <param name="request">The request object with values to be updated</param>
        /// <returns>The updated SCORM performance</returns>
        [HttpPut]
        [Route("performances/register/{userId}/{rcoId}")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
        public ScormPerformance Register(int userId, int rcoId, ScormPerformanceRegisterRequest request)
        {
            try
            {
                return _performanceProvider.RegisterPerformance(userId, rcoId, request);
            }
            catch (EntityNotFoundException e)
            {
                throw CreateFaultException(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}