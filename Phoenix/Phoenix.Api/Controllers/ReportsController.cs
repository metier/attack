﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Reporting;
using Metier.Phoenix.Api.Reporting.Reports.ActivityParticipants;
using Metier.Phoenix.Api.Reporting.Reports.AllParticipations;
using Metier.Phoenix.Api.Reporting.Reports.HubspotParticipants;
using Metier.Phoenix.Api.Reporting.Reports.CustomerSpecific.Jernbaneverket;
using Metier.Phoenix.Api.Reporting.Reports.Exams;
using Metier.Phoenix.Api.Reporting.Reports.OrdersForInvoicing;
using Metier.Phoenix.Api.Reporting.Reports.ParticipationsPrActivity;
using Metier.Phoenix.Api.Reporting.Reports.ParticipationsStatusPrActivity;
using Metier.Phoenix.Api.Reporting.Reports.PotentialParticipations;
using Metier.Phoenix.Api.Reporting.Reports.PrepaidClassrooms;
using Metier.Phoenix.Api.Reporting.Reports.ProductCourseStatuses;
using Metier.Phoenix.Api.Reporting.Reports.ProgressStatuses;
using Metier.Phoenix.Api.Reporting.Reports.Questions;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Business.DocumentProduction.Formats;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Exceptions;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Reporting operations</p>
    /// <p>Generates report files</p>
    /// </summary>
    public class ReportsController : ControllerBase
    {
        private readonly IReportFileProvider _reportFileProvider;

        public ReportsController(IReportFileProvider reportFileProvider)
        {
            _reportFileProvider = reportFileProvider;
        }
        
        /// <summary>
        /// <p>Generates a report of e-learning progress statuses based on the given criteria</p>
        /// <p>Returns an Excel (xlsx) file as an application/octet-stream</p>
        /// </summary>
        /// <param name="customerId">Customer ids array (optional)</param>
        /// <param name="lessonStatus">Lesson statuses array ("N", "I", "C", "P", "F") (optional)</param>
        /// <param name="productId">Product ids array (optional)</param>
        /// <param name="activityId">Activity ids array (optional)</param>                 
        /// <param name="fromDate">From date of enrollment (optional)</param>
        /// <param name="toDate">To date of enrollment (optional)</param>                  
        /// <param name="isArbitrary">If true, will only show arbitrary customers. If false, will only show corporate customers. (optional)</param>
        [AllowRoles(PhoenixRoles.LearningPortalSuperUser)]    
        [HttpGet]
        [Route("reports/progressstatus/xlsx")]
        public HttpResponseMessage GetProgressStatus([FromUri] int[] customerId, [FromUri] string[] lessonStatus, [FromUri] int[] productId, [FromUri] int[] activityId, [FromUri] DateTime? fromDate = null, [FromUri] DateTime? toDate = null, [FromUri] bool? isArbitrary = null)
        {
            try
            {
                var file = _reportFileProvider.GetReportFile(new ProgressStatusesReportDefinition(customerId, lessonStatus, productId, activityId, fromDate, toDate, isArbitrary, FileFormats.Excel));
                return GetStreamResponse(file, "progress-status-e-learning.xlsx");
            }
            catch (UnauthorizedException e)
            {
                throw CreateFaultException(HttpStatusCode.Unauthorized, e.Message);
            }
        }

        /// <summary>
        /// <p>Generates a report of participants for a specified set of activities</p>
        /// <p>Returns an Excel (xlsx) file as an application/octet-stream</p>
        /// </summary>
        /// <param name="activityId">Activity ids array (optional)</param>
        [HttpGet]
        [Route("reports/activityparticipants/xlsx")]
        public HttpResponseMessage GetActivityParticipants([FromUri] int[] activityId)
        {
            var file = _reportFileProvider.GetReportFile(new ActivityParticipantsReportDefinition(activityId, FileFormats.Excel));
            return GetStreamResponse(file, "activity-participants.xlsx");
        }

        /// <summary>
        /// <p>Generates a report of products with corresponding activities and participants for the given criteria</p>
        /// <p>Returns an Excel (xlsx) file as an application/octet-stream</p>
        /// </summary>
        /// <param name="customerId">Customer ids array (optional)</param>
        /// <param name="productId">Product ids array (optional)</param>
        /// <param name="fromDate">From date of enrollment (optional)</param>
        /// <param name="toDate">To date of enrollment (optional)</param>
        /// <param name="isArbitrary">If true, will only show arbitrary customers. If false, will only show corporate customers. (optional)</param>
        [HttpGet]
        [Route("reports/productcoursestatus/xlsx")]
        public HttpResponseMessage GetProductCourseStatus([FromUri] int[] customerId, [FromUri] int[] productId, [FromUri] DateTime? fromDate = null, [FromUri] DateTime? toDate = null, [FromUri] bool? isArbitrary = null)
        {
            var file = _reportFileProvider.GetReportFile(new ProductCourseStatusReportDefinition(customerId, productId, fromDate, toDate, isArbitrary, FileFormats.Excel));
            return GetStreamResponse(file, "participations-status.xlsx");
        }

        /// <summary>
        /// <p>Generates a report of participation statuses pr activity for the given criteria</p>
        /// <p>Returns an Excel (xlsx) file as an application/octet-stream</p>
        /// </summary>
        /// <param name="customerId">Customer ids array (optional)</param>
        /// <param name="productId">Product ids array (optional)</param>
        /// <param name="fromDate">From date of enrollment (optional)</param>
        /// <param name="toDate">To date of enrollment (optional)</param>
        /// <param name="isArbitrary">If true, will only show arbitrary customers. If false, will only show corporate customers. (optional)</param>
        [HttpGet]
        [Route("reports/participationsstatuspractivity/xlsx")]
        public HttpResponseMessage GetParticipationStatusPrActivity([FromUri] int[] customerId, [FromUri] int[] productId, [FromUri] DateTime? fromDate = null, [FromUri] DateTime? toDate = null, [FromUri] bool? isArbitrary = null)
        {
            var file = _reportFileProvider.GetReportFile(new ParticipationStatusPrActivityReportDefinition(customerId, productId, fromDate, toDate, isArbitrary, FileFormats.Excel));
            return GetStreamResponse(file, "participations-status-pr-activity.xlsx");
        }

		/// <summary>
		/// <p>Generates a report of potential participations, i.e. users with all the articles they have not participated in.</p>
		/// <p>Returns an Excel (xlsx) file as an application/octet-stream</p>
		/// </summary>
		/// <param name="customerId">Customer ids array (required)</param>
		/// <param name="productId">Product ids array that user is missing (optional)</param>
		/// <param name="takenProductId">Product ids array that user has taken (optional)</param>
		/// <param name="productIdsIsAndQuery">If true, run AND between all ProductIds. Otherwise, it's an or. (optional)</param>
		/// <param name="isArbitrary">If true, will only show arbitrary customers. If false, will only show corporate customers. (optional)</param>
		[HttpGet]
        [Route("reports/potentialparticipations/xlsx")]
        public HttpResponseMessage GetPotentialParticipations([FromUri] int[] customerId, [FromUri] int[] productId, [FromUri] int[] takenProductId, [FromUri] bool productIdsIsAndQuery = false, [FromUri] bool? isArbitrary = null)
        {
            try
            {
                var file = _reportFileProvider.GetReportFile(new PotentialParticipationReportDefinition(customerId, productId, productIdsIsAndQuery, takenProductId, isArbitrary, FileFormats.Excel));
                return GetStreamResponse(file, "potential-participations.xlsx");
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// <p>Generates a report for all participantions based on the given criteria</p>
        /// <p>Returns an Excel (xlsx) file as an application/octet-stream</p>
        /// </summary>
        /// <param name="customerId">Customer ids array (optional)</param>
        /// <param name="distributorId">Distributor ids array (optional)</param>
        /// <param name="productId">Product ids array (optional)</param>
        /// <param name="fromDate">From date of enrollment (optional)</param>
        /// <param name="toDate">To date of enrollment (optional)</param>
        /// <param name="isArbitrary">If true, will only show arbitrary customers. If false, will only show corporate customers. (optional)</param>
        [HttpGet]
        [Route("reports/allparticipations/xlsx")]
        public HttpResponseMessage GetAllParticipations([FromUri] int[] customerId, [FromUri] int[] distributorId, [FromUri] int[] productId, [FromUri] DateTime? fromDate = null, [FromUri] DateTime? toDate = null, [FromUri] bool? isArbitrary = null)
        {
            try
            {
                var file = _reportFileProvider.GetReportFile(new AllParticipationReportDefinition(customerId, distributorId, productId, fromDate, toDate, isArbitrary, FileFormats.Excel));
                return GetStreamResponse(file, "all-participations.xlsx");
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// <p>Generates a report for all participantion for Hubspot based on the given criteria</p>
        /// <p>Returns a Csv file</p>
        /// </summary>
        /// <param name="customerId">Customer ids array (optional)</param>
        /// <param name="distributorId">Distributor ids array (optional)</param>
        /// <param name="productId">Product ids array (optional)</param>
        /// <param name="fromDate">From date of enrollment (optional)</param>
        /// <param name="toDate">To date of enrollment (optional)</param>
        /// <param name="isArbitrary">If true, will only show arbitrary customers. If false, will only show corporate customers. (optional)</param>
        [HttpGet]
        [Route("reports/hubspotparticipants/csv")]
        public HttpResponseMessage GetAllHubspotParticipantsCsv([FromUri] int[] customerId, [FromUri] int[] distributorId, [FromUri] string[] productId, [FromUri] DateTime? fromDate = null, [FromUri] DateTime? toDate = null, [FromUri] bool? isArbitrary = null)
        {
            try
            {
                var file = _reportFileProvider.GetReportFile(new  HubspotParticipantReportDefinition(customerId, distributorId, productId, fromDate, toDate, isArbitrary, FileFormats.Csv));
                return GetStreamResponse(file, "hubspot-participants.csv");
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// <p>Generates a report for all participantion for Hubspot based on the given criteria</p>
        /// <p>Returns a Excel (xlsx) file as an application/octet-stream</p>
        /// </summary>
        /// <param name="customerId">Customer ids array (optional)</param>
        /// <param name="distributorId">Distributor ids array (optional)</param>
        /// <param name="productId">Product ids array (optional)</param>
        /// <param name="fromDate">From date of enrollment (optional)</param>
        /// <param name="toDate">To date of enrollment (optional)</param>
        /// <param name="isArbitrary">If true, will only show arbitrary customers. If false, will only show corporate customers. (optional)</param>
        [HttpGet]
        [Route("reports/hubspotparticipants/xlsx")]
        public HttpResponseMessage GetAllHubspotParticipantsXlsx([FromUri] int[] customerId, [FromUri] int[] distributorId, [FromUri] string[] productId, [FromUri] DateTime? fromDate = null, [FromUri] DateTime? toDate = null, [FromUri] bool? isArbitrary = null)
        {
            try
            {
                var file = _reportFileProvider.GetReportFile(new HubspotParticipantReportDefinition(customerId, distributorId, productId, fromDate, toDate, isArbitrary, FileFormats.Excel));
                return GetStreamResponse(file, "hubspot-participants.xlsx");
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// <p>Generates a report for all participantions based on the given criteria.</p>
        /// <p>The report displays each participation on a separate row.</p>
        /// <p>Returns an Excel (xlsx) file as an application/octet-stream</p>
        /// </summary>
        /// <param name="customerId">Customer ids array (optional)</param>
        /// <param name="distributorId">Distributor ids array (optional)</param>
        /// <param name="productId">Product ids array (optional)</param>
        /// <param name="fromDate">From date of enrollment (optional)</param>
        /// <param name="toDate">To date of enrollment (optional)</param>
        /// <param name="isArbitrary">If true, will only show arbitrary customers. If false, will only show corporate customers. (optional)</param>
        [HttpGet]
        [Route("reports/allparticipationspractivity/xlsx")]
        public HttpResponseMessage GetAllParticipationsPrActivity([FromUri] int[] customerId, [FromUri] int[] distributorId, [FromUri] int[] productId, [FromUri] DateTime? fromDate = null, [FromUri] DateTime? toDate = null, [FromUri] bool? isArbitrary = null)
        {
            try
            {
                var file = _reportFileProvider.GetReportFile(new ParticipationsPrActivityReportDefinition(customerId, distributorId, productId, fromDate, toDate, isArbitrary, FileFormats.Excel));
                return GetStreamResponse(file, "all-participations-pr-activity.xlsx");
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// <p>Generates a invoice forcast report based on the given criteria</p>
        /// <p>Returns an Excel (xlsx) file as an application/octet-stream</p>
        /// </summary>
        /// <param name="distributorId">Distributor ids array (optional)</param>
        /// <param name="customerId">Customer ids array (optional)</param>
        /// <param name="productId">Product ids array (optional)</param>
        /// <param name="orderStatusId">Order status array (Proposed=0, Draft=1, Transferred=2, NotInvoiced=3, Invoiced=4, Rejected=5) (optional)</param>
        /// <param name="fromDate">From earliest invoice date (optional)</param>
        /// <param name="toDate">To earliest invoice date (optional)</param>
        /// <param name="isArbitrary">If true, will only show arbitrary customers. If false, will only show corporate customers. (optional)</param>
        [HttpGet]
        [Route("reports/ordersforinvoicing/xlsx")]
        public HttpResponseMessage GetOrdersForInvoicing(
            [FromUri] int[] distributorId, 
            [FromUri] int[] customerId,
            [FromUri] int[] productId,
            [FromUri] int[] orderStatusId,
            [FromUri] DateTime? fromDate = null, 
            [FromUri] DateTime? toDate = null,
            [FromUri] bool? isArbitrary = null)
        {
            try
            {
                var file = _reportFileProvider.GetReportFile(new OrdersForInvoicingReportDefinition(
                                                                    distributorId, 
                                                                    customerId,
                                                                    productId,
                                                                    orderStatusId,
                                                                    fromDate, 
                                                                    toDate,
                                                                    isArbitrary,
                                                                    FileFormats.Excel));
                return GetStreamResponse(file, "orders-for-invoicing.xlsx");
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// <p>Generates a report showing prepaid classrooms for the selected period.</p>
        /// <p>Returns an Excel (xlsx) file as an application/octet-stream</p>
        /// </summary>
        /// <param name="distributorId">Distributor ids array (optional)</param>
        /// <param name="fromDate">From order date (optional)</param>
        /// <param name="toDate">To order date (optional)</param>
        [HttpGet]
        [Route("reports/prepaidclassrooms/xlsx")]
        public HttpResponseMessage GetPrepaidClassrooms(
            [FromUri] int[] distributorId, 
            [FromUri] DateTime? fromDate = null, 
            [FromUri] DateTime? toDate = null)
        {
            try
            {
                var file = _reportFileProvider.GetReportFile(new PrepaidClassroomsReportDefinition(distributorId, fromDate, toDate, FileFormats.Excel));
                return GetStreamResponse(file, "prepaid-classrooms.xlsx");
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// <p>Generates a report of questions based on the given criteria</p>
        /// <p>Returns an Excel (xlsx) file as an application/octet-stream</p>
        /// </summary>
        /// <param name="query">Question text query (optional)</param>
        /// <param name="tagId">Array of tag ids (optional)</param>
        /// <param name="productId">Array of product ids (optional)</param>
        /// <param name="rcoId">Array of rco ids (optional)</param>
        /// <param name="difficulty">Array of difficulties (Easy=1, Medium=2, Hard=3) (optional)</param>
        /// <param name="status">Array of statuses (Draft=1, Published=2, Obsolete=9) (optional)</param>
        /// <param name="languageId">Array of language ids (optional)</param>
        /// <param name="customerId">Array of customer ids (optional)</param>
        /// <param name="lastUsedFromDate">From when the question was last used (optional)</param>
        /// <param name="lastUsedToDate">To when the question was last used (optional)</param>
        [HttpGet]
        [Route("reports/questions/xlsx")]
        [AllowRoles(PhoenixRoles.BehindContentAdmin)]
        public HttpResponseMessage GetQuestions(
            [FromUri] string query = null,
            [FromUri] int[] tagId = null,
            [FromUri] int[] productId = null,
            [FromUri] int[] rcoId = null,
            [FromUri] QuestionDifficulties[] difficulty = null,
            [FromUri] QuestionStatuses[] status = null,
            [FromUri] int[] languageId = null,
            [FromUri] int[] customerId = null,
            [FromUri] DateTime? lastUsedFromDate = null,
            [FromUri] DateTime? lastUsedToDate = null)
        {
            try
            {
                var file = _reportFileProvider.GetReportFile(new QuestionsReportDefinition(
                                                                    query,
                                                                    tagId,
                                                                    productId,
                                                                    rcoId,
                                                                    difficulty,
                                                                    status,
                                                                    languageId,
                                                                    customerId,
                                                                    lastUsedFromDate,
                                                                    lastUsedToDate,
                                                                    FileFormats.Excel));
                return GetStreamResponse(file, "questions.xlsx");
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// <p>Generates a report of exam answers based on the given criteria</p>
        /// <p>Returns an Excel (xlsx) file as an application/octet-stream</p>
        /// </summary>
        /// <param name="customerId">Array of customer ids (optional)</param>
        /// <param name="productId">Array of product ids (optional)</param>
        /// <param name="isArbitrary">If true, will only show arbitrary customers. If false, will only show corporate customers. (optional)</param>
        /// <param name="fromDate">From when the exam was taken (optional)</param>
        /// <param name="toDate">To when the exam was taken (optional)</param>
        [HttpGet]
        [Route("reports/exams/xlsx")]
        public HttpResponseMessage GetExamsReport([FromUri] int[] customerId, [FromUri] int[] productId, [FromUri] bool? isArbitrary = null, [FromUri] DateTime? fromDate = null, [FromUri] DateTime? toDate = null)
        {
            try
            {
                var file = _reportFileProvider.GetReportFile(new ExamsReportDefinition(customerId, productId, isArbitrary, fromDate, toDate, FileFormats.Excel));
                return GetStreamResponse(file, "exams.xlsx");
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e.ValidationResults.ToArray());
            }
        }


        /// <summary>
        /// <p>Generates a report of participants who has undertaken safetycourses in the selected period for a given customer</p>
        /// <p>Returns an Excel (xlsx) file as an application/octet-stream</p>
        /// </summary>
        /// <param name="customerId">Array of customer ids</param>
        /// <param name="fromDate">Activities held after this date</param>
        /// <param name="toDate">Activities held before this date</param>
        [HttpGet]
        [Route("reports/customerspecific/jernbaneverket/safetycourseparticipants/xlsx")]
        public HttpResponseMessage GetSafetyCourseParticipantsReport([FromUri] int[] customerId, [FromUri] DateTime? fromDate = null, [FromUri] DateTime? toDate = null)
        {
            try
            {
                var file = _reportFileProvider.GetReportFile(new SafetyCourseParticipantsReportDefinition(customerId, fromDate, toDate, FileFormats.Excel));
                return GetStreamResponse(file, "SafetyCourseParticipants_Follobanen.xlsx");
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e.ValidationResults.ToArray());
            }
        }

        
        
        private static HttpResponseMessage GetStreamResponse(byte[] file, string fileName)
        {
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StreamContent(new MemoryStream(file));
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = fileName;
            return result;
        }
    }
}