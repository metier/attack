﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Exceptions;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// Handles product related operations
    /// </summary>
    public class ProductsController : ControllerBase
    {
        private readonly IProductRepository _productRepository;

        public ProductsController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        /// <summary>
        /// Returns all products (both active and inactive)
        /// </summary>
        [HttpGet]
        [Route("products")]
        [AllowRoles(PhoenixRoles.All)]
        public IEnumerable<Product> GetProducts()
        {
            return _productRepository.GetAll(); 
        }

        /// <summary>
        /// Returns all products (both active and inactive) matching current filter
        /// </summary>
        [HttpGet]
        [Route("products")]
        [AllowRoles(PhoenixRoles.All)]
        public IEnumerable<Product> GetProducts([FromUri] string query)
        {
            return _productRepository.Search(query); 
        }

        /// <summary>
        /// Returns the product with the given id. Can be used for CRUD operations
        /// </summary>
        /// <param name="id">Product id</param>
        [HttpGet]
        [Route("products/{id}")]
        public Product GetProducts(int id)
        {
            return _productRepository.Get(id);
        }

        /// <summary>
        /// Creates a new product. Part of the CRUD operations for this resource.
        /// </summary>
        /// <param name="product">The product to create</param>
        /// <returns>The product after persisting it to the API</returns>
        [HttpPost]
        [Route("products")]
        [ResponseType(typeof(Product))]
        public HttpResponseMessage CreateProduct(Product product)
        {
            try
            {
                product = _productRepository.Create(product);
                return Request.CreateResponse(HttpStatusCode.Created, product);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid product", e);
            }
        }
        
        /// <summary>
        /// <p>Updates an existing product. Part of the CRUD operations for this resource.</p>
        /// <p>The product returned from the resource's GET operation can be used for this update process.</p>
        /// <p><span class="caution">CAUTION: Unspecified properties and list items will be deleted.</span></p>
        /// </summary>
        /// <param name="id">Product id</param>
        /// <param name="product">The product for update</param>
        /// <returns>The product after update</returns>
        [HttpPut]
        [Route("products/{id}")]
        public Product UpdateProduct(int id, Product product)
        {
            try
            {
                product = _productRepository.Update(id, product);
            }
            catch (ValidationException e)
            {
                throw CreateValidationErrorFaultException("Invalid product", e);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
            catch (DbUpdateException e)
            {
                if (IsUniqueConstraintException(e))
                    throw CreateFaultException(HttpStatusCode.BadRequest, "Cannot insert duplicate entries");
                if (IsForeignKeyConstraintException(e))
                    throw CreateFaultException(HttpStatusCode.BadRequest, "Cannot update product. Foreign reference missing.");
                throw;
            }

            return product;
        }

        /// <summary>
        /// Returns the product type of a given product
        /// </summary>
        /// <param name="id">Product id</param>
        [HttpGet]
        [Route("products/{id}/producttype")]
        public ProductType ProductType(int id)
        {
            return _productRepository.GetProductTypeByProductId(id);
        }
    }
}