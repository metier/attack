﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Metier.Phoenix.Core.Data;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// This resource provides operations for diagnosing the API.
    /// </summary>
    public class DiagnosticsController : ControllerBase
    {
        private readonly MetierLmsContext _context;

        public DiagnosticsController(MetierLmsContext context)
        {
            _context = context;
        }

        /// <summary>
        /// If no errors are returned, the API is up and running with a database connection
        /// </summary>
        /// <returns>HTTP/200 OK if up and running - otherwise, the server is down</returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("diagnostics/isalive")]
        public HttpResponseMessage IsAlive()
        {
            _context.Users.First();
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}