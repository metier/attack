﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Http;
using System.Web.Http.Description;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Business.FileStorage;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Handles operations related to posting and reading of files</p>
    /// </summary>
    public class FilesController : ControllerBase
    {
        private readonly MetierLmsContext _context;
        private readonly IFileStorage _fileStorage;

        public FilesController(MetierLmsContext context, IFileStorage fileStorage)
        {
            _context = context;
            _fileStorage = fileStorage;
        }

        /// <summary>
        /// Retrieves a file by the given file id.
        /// </summary>
        /// <param name="id">File id</param>
        /// <returns>The file is returned as a stream where the content type is set to application/octet-stream (binary file).</returns>
        [HttpGet]
        [Route("files/{id}")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.Examiner)]
        public HttpResponseMessage GetFile(int id)
        {
            try
            {
                return GetFileById(id);
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            
        }


		/// <summary>
		/// Retrieves a file by the given file id and shareId.
		/// </summary>
		/// <param name="id">File id</param>
		/// <param name="shareId">Guid secret</param>
		/// <returns>The file is returned as a stream where the content type is set to application/octet-stream (binary file).</returns>
		[HttpGet]
		[Route("files/{id}/{shareId:guid}")]
		[AllowAnonymous]
		public HttpResponseMessage GetFileByIdAndShareId(int id, Guid shareId)
		{
			try
			{
				var attachment = _context.Attachments.Where(a => a.FileId == id && a.ShareGuid == shareId)
					.FirstOrDefault();

				if (attachment == null) throw new Exception();

				return GetFileById(id);
			}
			catch (Exception)
			{
				return new HttpResponseMessage(HttpStatusCode.NotFound);
			}

		}

		/// <summary>
		/// Retrieves a file by the given file id and version.
		/// </summary>
		/// <param name="id">File id</param>
		/// <param name="version">File version</param>
		/// <returns>The file is returned as a stream where the content type is set to application/octet-stream (binary file).</returns>
		[HttpGet]
        [Route("files/{id}/{version}")]
        [AllowRoles(PhoenixRoles.LearningPortal, PhoenixRoles.Examiner)]
        public HttpResponseMessage GetFileByIdAndVersion(int id,string version)
        {
            try
            {
                var attachment = _context.Attachments.Where(a => a.FileId == id)
                    .ToList()
                    .FirstOrDefault(a => Convert.ToBase64String(a.RowVersion).Replace('/', '_') == version);
                if (attachment == null) throw new Exception();
                                
                return GetFileById(id);
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

        }



		/// <summary>
		/// Used to retrieve files which has been shared by a file id. These files can be collected without authenticating by knowing the id (i.e. received via e-mail)
		/// </summary>
		/// <param name="fileId">A share file id</param>
		/// <returns>The file is returned as a stream where the content type is set to application/octet-stream (binary file).</returns>
		[HttpGet]
        [Route("files/shared")]
        [AllowAnonymous]
        public HttpResponseMessage GetFile([FromUri] Guid fileId)
        {
            var sharedFile = _context.SharedFiles.FirstOrDefault(sf => sf.ShareGuid == fileId);

            if (sharedFile == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            try
            {
                return GetFileById(sharedFile.FileId);
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        /// Files can be posted to the API with a multipart MIME-type. The files are posted as a multipart stream to the server.
        /// </summary>
        [HttpPost]
        [Route("files")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.LearningPortal)]
        [ResponseType(typeof(Attachment))]
        public async Task<HttpResponseMessage> Post()
        {
            if (Request.Content.IsMimeMultipartContent())
            {
                var memoryStreamProvider = new MultipartMemoryStreamProvider();
                await Request.Content.ReadAsMultipartAsync(memoryStreamProvider);

                var attachment = new Attachment();
                string title = string.Empty;
                foreach (var httpContent in memoryStreamProvider.Contents)
                {
                    var name = RemoveEscapeCharacters(httpContent.Headers.ContentDisposition.Name);
                    if (name == "file")
                    {
                        using (var transaction = new TransactionScope())
                        {
                            using (var fileStream = await httpContent.ReadAsStreamAsync())
                            {
                                var file = _fileStorage.CreateFile(
                                    RemoveEscapeCharacters(httpContent.Headers.ContentDisposition.FileName),
                                    httpContent.Headers.ContentType.MediaType,
                                    fileStream);
                                attachment.FileId = file.Id;

                                if (String.IsNullOrEmpty(title))
                                {
                                    title = file.Filename;
                                }
                            }
                            transaction.Complete();
                        }
                    }
                    else
                    {
                        var streamReader = new StreamReader(await httpContent.ReadAsStreamAsync());
                        var content = streamReader.ReadToEnd();
                        if (name == "title")
                        {
                            title = content;
                        }
                        if (name == "description")
                        {
                            attachment.Description = content;
                        }
                    }
                }
                attachment.Title = title;

                var response = Request.CreateResponse(HttpStatusCode.OK, attachment);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                return response;
            }
            else
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotAcceptable,
                    "This request is not properly formatted"));
            }
        }
        private HttpResponseMessage GetFileById(int id)
        {
            var file = _context.Files.FirstOrDefault(f => f.Id == id);
            
            if (file == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            var fileStream = _fileStorage.GetFileStream(file);

            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StreamContent(fileStream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = file.Filename;
            return result;
        }

        private static string RemoveEscapeCharacters(string value)
        {
            return value.Replace("\"", "");
        }
    }
}