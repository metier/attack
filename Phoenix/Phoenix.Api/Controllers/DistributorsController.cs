﻿using System.Collections.Generic;
using System.Web.Http;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;
using NLog;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Distributor related operations.</p>
    /// <p>A distributor represents the business units which customers are associated and invoiced by.</p>
    /// </summary>
    public class DistributorsController : ControllerBase
    {
        private readonly IDistributorRepository _distributorRepository;
        private readonly Logger _logger;

        public DistributorsController(IDistributorRepository distributorRepository)
        {
            _distributorRepository = distributorRepository;
            _logger = LogManager.GetCurrentClassLogger();

        }

        /// <summary>
        /// Returns all the current distributors in the system.
        /// </summary>
        [HttpGet]
        [Route("distributors")]
        [AllowRoles(PhoenixRoles.All)]
        public IEnumerable<Distributor> GetDistributors()
        {
            _logger.Info($"Getting distributors");
            return _distributorRepository.GetDistributors();
        }
    }
}