﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Handles rco related operations</p>
    /// <p>An rco describes a course or an exam from the MCS system</p>
    /// </summary>
    public class RcosController : ControllerBase
    {
        private readonly IRcoProvider _rcoProvider;

        public RcosController(IRcoProvider rcoProvider)
        {
            _rcoProvider = rcoProvider;
        }

        /// <summary>
        /// Searches for courses
        /// </summary>
        /// <param name="query">Free text search on columns ("Name", "Language", "Version") (optional)</param>
        /// <param name="orderBy">Column to order result by ("name", "language", "version") (optional, default = id)</param>
        /// <param name="orderDirection">Order of the result ("asc", "desc") (optional)</param>
        /// <param name="skip">Index of first item in result (optional)</param>
        /// <param name="limit">Max number of items in result (optional, default = 20)</param>
        [HttpGet]
        [Route("rcos/courses")]
        [ResponseType(typeof(PartialList<SearchableRco>))]
        [AllowRoles(PhoenixRoles.LearningPortal, PhoenixRoles.LearningPortalTokenUser)]
        public HttpResponseMessage SearchCourses([FromUri] string query = null,
                                                 [FromUri] string orderBy = null,
                                                 [FromUri] string orderDirection = null,
                                                 [FromUri] int? skip = null,
                                                 [FromUri] int? limit = 20)
        {

            var courses = _rcoProvider.GetCourses(query, orderBy, orderDirection, skip, limit);
            return Request.CreateResponse(HttpStatusCode.OK, courses);
        }

        /// <summary>
        /// Searches for exams
        /// </summary>
        /// <param name="query">Free text search on columns ("Name", "Language", "Version") (optional)</param>
        /// <param name="orderBy">Column to order result by ("name", "language", "version") (optional, default = id)</param>
        /// <param name="orderDirection">Order of the result ("asc", "desc") (optional)</param>
        /// <param name="skip">Index of first item in result (optional)</param>
        /// <param name="limit">Max number of items in result (optional, default = 20)</param>
        [HttpGet]
        [Route("rcos/exams")]
        [ResponseType(typeof(PartialList<SearchableRco>))]
        [AllowRoles(PhoenixRoles.LearningPortal, PhoenixRoles.LearningPortalTokenUser)]
        public HttpResponseMessage SearchExams([FromUri] string query = null,
                                                 [FromUri] string orderBy = null,
                                                 [FromUri] string orderDirection = null,
                                                 [FromUri] int? skip = null,
                                                 [FromUri] int? limit = 20)
        {

            var courses = _rcoProvider.GetExamContainers(query, orderBy, orderDirection, skip, limit);
            return Request.CreateResponse(HttpStatusCode.OK, courses);
        }

        /// <summary>
        /// Searches for exams and courses
        /// </summary>
        /// <param name="query">Free text search on columns ("Name", "Language", "Version") (optional)</param>
        /// <param name="rcoIds">An array of rco Ids</param>
        /// <param name="orderBy">Column to order result by ("name", "language", "version") (optional, default = id)</param>
        /// <param name="orderDirection">Order of the result ("asc", "desc") (optional)</param>
        /// <param name="skip">Index of first item in result (optional)</param>
        /// <param name="limit">Max number of items in result (optional, default = 20)</param>
        [HttpGet]
        [Route("rcos")]
        [AllowRoles(PhoenixRoles.BehindContentAdmin)]
        [ResponseType(typeof(PartialList<SearchableRco>))]
        public HttpResponseMessage Search([FromUri] string query = null,
                                            [FromUri] int[] rcoIds = null,
                                            [FromUri] string orderBy = null,
                                            [FromUri] string orderDirection = null,
                                            [FromUri] int? skip = null,
                                            [FromUri] int? limit = 20)
        {
            var versions = _rcoProvider.SearchRcos(query, rcoIds, orderBy, orderDirection, skip, limit);
            return Request.CreateResponse(HttpStatusCode.OK, versions);
        }

        /// <summary>
        /// Gets a course rco with the given id
        /// </summary>
        /// <param name="id">Rco id</param>
        [HttpGet]
        [Route("rcos/courses/{id}")]
        [ResponseType(typeof(RcoCourse))]
        public HttpResponseMessage GetCourse(int id)
        {
            try
            {
                var course = _rcoProvider.GetCourse(id);
                return Request.CreateResponse(HttpStatusCode.OK, course);
            }
            catch (EntityNotFoundException e)
            {
                return CreateFaultResponse(HttpStatusCode.NotFound, e.Message);
            }
        }

        /// <summary>
        /// Gets an exam rco with the given id
        /// </summary>
        /// <param name="id">Rco id</param>
        [HttpGet]
        [Route("rcos/exams/{id}")]
        [ResponseType(typeof(RcoExamContainer))]
        public HttpResponseMessage GetExam(int id)
        {
            try
            {
                var exam = _rcoProvider.GetExamContainer(id);
                return Request.CreateResponse(HttpStatusCode.OK, exam);
            }
            catch (EntityNotFoundException e)
            {
                return CreateFaultResponse(HttpStatusCode.NotFound, e.Message);
            }
        }

    }
}