﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Metier.Phoenix.Api.Services.ExternalCustomers;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>External customer related operations</p>
    /// <p>An external customer is data registered about Phoenix customers in a distributor's external invoicing system</p>
    /// </summary>
    public class ExternalCustomersController : ControllerBase
    {
        private readonly IExternalCustomersProviderFactory _externalCustomersProviderFactory;

        public ExternalCustomersController(IExternalCustomersProviderFactory externalCustomersProviderFactory)
        {
            _externalCustomersProviderFactory = externalCustomersProviderFactory;
        }

        /// <summary>
        /// Returns true if the given distributor has an external customer provider configured in the API
        /// </summary>
        /// <param name="distributorId">Distributor id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("externalcustomers/hasexternalcustomersprovider")]
        public bool HasExternalCustomersProvider([FromUri] int distributorId)
        {
            return _externalCustomersProviderFactory.HasExternalCustomersProviders(distributorId);
        }

        /// <summary>
        /// Searches for customer's in a distributor's external customer provider by name.
        /// </summary>
        /// <param name="name">Customer name</param>
        /// <param name="distributorId">Distributor id</param>
        [HttpGet]
        [Route("externalcustomers")]
        public List<ExternalCustomer> Get([FromUri] string name, [FromUri] int distributorId)
        {
            var provider = _externalCustomersProviderFactory.GetExternalCustomerProvider(distributorId);
            var result = provider.Search(new ExternalCustomersSearchQuery
            {
                CustomerName = name,
                DistributorId = distributorId
            });

            return result.Take(20).ToList();
        }
    }
}