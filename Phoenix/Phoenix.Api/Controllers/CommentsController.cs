﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// Supports operations enabling commenting of entities in Phoenix.
    /// </summary>
    [AllowRoles(PhoenixRoles.BehindContentAdmin, PhoenixRoles.Examiner)]
    public class CommentsController : ControllerBase
    {
        private readonly ICommentsProvider _commentsProvider;

        public CommentsController(ICommentsProvider commentsProvider)
        {
            _commentsProvider = commentsProvider;
        }

        /// <summary>
        /// Gets all comments for a given entity
        /// </summary>
        /// <param name="entity">The name of the entity. By convention the entity name is the same name as the API resource (not in plural form).</param>
        /// <param name="entityId">The entity id.</param>
        [HttpGet]
        [Route("comments")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
        public List<Comment> GetComments([FromUri] string entity, [FromUri] int entityId)
        {
            return _commentsProvider.FindByEntityId(entity, entityId);
        }

        /// <summary>
        /// Gets all comments on participants for a given activity
        /// </summary>
        /// <param name="activityId">The id of the activity.</param>
        /// <param name="includeUserComments">Include comments on the users.</param>
        /// <param name="includeEnrollmentComments">Include comments on the users enrollments.</param>
        [HttpGet]
        [Route("comments")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
        public List<Comment> GetParticipantComments([FromUri] int activityId, [FromUri] bool includeUserComments = false, [FromUri] bool includeEnrollmentComments = false)
        {
            return _commentsProvider.GetParticipantComments(activityId, includeUserComments, includeEnrollmentComments);
        }
        
        /// <summary>
        /// Gets all comments on the user, including the users enrollments and participations if specified.
        /// </summary>
        /// <param name="userId">The id of the user.</param>
        /// <param name="includeEnrollmentComments">Include comments on the users enrollments.</param>
        /// <param name="includeParticipantComments">Include comments on the users participations.</param>
        [HttpGet]
        [Route("usercomments")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
        public List<Comment> GetUserComments([FromUri] int userId, [FromUri] bool includeEnrollmentComments = true, [FromUri] bool includeParticipantComments = true)
        {
            return _commentsProvider.GetUserComments(userId, includeEnrollmentComments, includeParticipantComments);
        }

        /// <summary>
        /// Gets all comments on the participation, including comments on the user and the related enrollment if specified.
        /// </summary>
        /// <param name="participantId">The id of the participant.</param>
        /// <param name="includeUserComments">Include comments on the users.</param>
        /// <param name="includeEnrollmentComments">Include comments on the users enrollments.</param>
        [HttpGet]
        [Route("comments")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
        public List<Comment> GetParticipationComments([FromUri] int participantId, [FromUri] bool includeUserComments = true, [FromUri] bool includeEnrollmentComments = true)
        {
            return _commentsProvider.GetParticipationComments(participantId, includeUserComments, includeEnrollmentComments);
        }
        
        /// <summary>
        /// Gets all comments on the enrollment, including comments on the user and the users participations if specified.
        /// </summary>
        /// <param name="enrollmentId">The id of the enrollment.</param>
        /// <param name="includeUserComments">Include comments on the user.</param>
        /// <param name="includeParticipantComments">Include comments on the users participations.</param>
        [HttpGet]
        [Route("enrollmentcomments")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
        public List<Comment> GetEnrollmentComments([FromUri] int enrollmentId, [FromUri] bool includeUserComments = true, [FromUri] bool includeParticipantComments = true)
        {
            return _commentsProvider.GetEnrollmentComments(enrollmentId, includeUserComments, includeParticipantComments);
        }

        /// <summary>
        /// Gets all comments on the enrollment, including comments on the user and the users participations if specified.
        /// </summary>
        /// <param name="activitySetId">The id of the enrollment.</param>
        /// <param name="includeUserComments">Include comments on the user.</param>
        /// <param name="includeParticipantComments">Include comments on the users participations.</param>
        [HttpGet]
        [Route("activitysetcomments")]
        public List<Comment> GetAllEnrollmentComments([FromUri] int activitySetId, [FromUri] bool includeUserComments = true, [FromUri] bool includeParticipantComments = true)
        {
            return _commentsProvider.GetAllEnrollmentComments(activitySetId, includeUserComments, includeParticipantComments);
        }

        /// <summary>
        /// <p>Creates a new comment on an entity. The entity name and id constitutes a unique comment. There are no restrictions to the naming of entities, but 
        /// by convention the single form name of the resource are used</p>
        /// </summary>
        /// <param name="comment">Comment data</param>
        /// <returns>All the comments registered for the given entity</returns>
        [HttpPost]
        [Route("comments")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
        [ResponseType(typeof(List<Comment>))]
        public HttpResponseMessage  CreateComment(Comment comment)
        {
            var entityComments = _commentsProvider.Create(comment);
            var response = Request.CreateResponse(HttpStatusCode.Created, entityComments);
            return response;
        }

        /// <summary>
        /// <p>Updates an existing comment identified by entity name and id. User in CRUD operation for the entity</p>
        /// <p>Use comment data retrieved from the resource's GET operation as basis for the update.</p>
        /// <p><span class="caution">CAUTION: Unspecified properties and list items will be deleted.</span></p>
        /// </summary>
        /// <param name="comment">The comment for updating</param>
        /// <returns>All the comments registered for the given entity</returns>
        [HttpPut]
        [Route("comments")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
        public List<Comment> UpdateComment(Comment comment)
        {
            return _commentsProvider.Update(comment);
        }

        /// <summary>
        /// Deletes the comment with the given id
        /// </summary>
        /// <param name="id">Comment id</param>
        /// <returns>All the comments registered for the given entity</returns>
        [HttpDelete]
        [Route("comments/{id}")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
        public List<Comment> DeleteComment(int id)
        {
            return _commentsProvider.Delete(id);
        }
    }
}