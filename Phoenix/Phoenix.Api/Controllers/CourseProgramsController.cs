﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Api.Providers;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Course program related operations</p>
    /// <p>A course program represents a structure of exams and courses associated to a customer. One customer can have several customer programs. This consists of a number of course steps containing course modules and exams.</p>
    /// </summary>
    public class CourseProgramsController : ControllerBase
    {
        private readonly ICourseProgramRepository _courseProgramRepository;        
        private readonly IAuthorizationProvider _authorizationProvider;
        private readonly ICustomersProvider _customersProvider;

        public CourseProgramsController(ICourseProgramRepository courseProgramRepository, IAuthorizationProvider authorizationProvider, ICustomersProvider customersProvider)
        {
            _courseProgramRepository = courseProgramRepository;
            _authorizationProvider = authorizationProvider;
            _customersProvider = customersProvider;
        }

        /// <summary>
        /// Returns a single course program entity. Can be used for CRUD operations
        /// </summary>
        /// <param name="id">CourseProgram id.</param>
        [HttpGet]
        [Route("courseprograms/{id}")]
        public CourseProgram GetCourseProgram(int id)
        {
            var courseProgram = _courseProgramRepository.Get(id);            
            if (courseProgram == null)
            {
                throw CreateFaultException(HttpStatusCode.NotFound, string.Format("Could not find course program with ID: {0}", id));
            }
            return courseProgram;
        }

        /// <summary>
        /// Query for all course programs of a given customer.
        /// </summary>
        /// <param name="customerId">Customer id.</param>
        [HttpGet]
        [Route("courseprograms")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.LearningPortal)]
        public IEnumerable<CourseProgram> GetCoursePrograms([FromUri] int customerId)
       {            
            if (!_authorizationProvider.HasAnyRole(PhoenixRoles.Phoenix, PhoenixRoles.LearningPortalTokenUser) && !_authorizationProvider.HasCustomer(customerId, _customersProvider))
                throw new Exceptions.UnauthorizedException("You do not have access to read the competency form for this user");

            return _courseProgramRepository.Find(customerId);
        }
        
        /// <summary>
        /// Query to retrieve all products in the given course program.
        /// </summary>
        /// <param name="courseprogramId">Courseprogram id.</param>
        [HttpGet]
        [Route("productsincourseprogram")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.LearningPortal)]
        public IEnumerable<Product> GetProductsincourseprogram([FromUri] int courseprogramId)
        {
            var courseProgram = GetCourseProgram(courseprogramId);

            if (!_authorizationProvider.HasAnyRole(PhoenixRoles.Phoenix, PhoenixRoles.LearningPortalTokenUser) && !_authorizationProvider.HasCustomer(courseProgram.CustomerId, _customersProvider))
                throw new Exceptions.UnauthorizedException("You do not have access to read the competency form for this user");

            return _courseProgramRepository.GetProductsInCourseProgram(courseprogramId);
        }

        /// <summary>
        /// Creates a new course program. Part of the CRUD operations for this resource.
        /// </summary>
        /// <param name="courseProgram">The course program to create</param>
        [HttpPost]
        [Route("courseprograms")]
        [ResponseType(typeof(CourseProgram))]
        public HttpResponseMessage Create(CourseProgram courseProgram)
        {
            var created = _courseProgramRepository.Create(courseProgram);
            return Request.CreateResponse(HttpStatusCode.Created, created);
        }
        
        /// <summary>
        /// <p>Updates an existing course program. Part of the CRUD operations for this resource.</p>
        /// <p>The activity returned from the resource's GET operation can be used for this update process.</p>
        /// <p><span class="caution">CAUTION: Unspecified properties and list items will be deleted.</span></p>
        /// </summary>
        /// <param name="id">Course program id.</param>
        /// <param name="courseProgram">The cours program to update.</param>
        [HttpPut]
        [Route("courseprograms/{id}")]
        public CourseProgram Update(int id, CourseProgram courseProgram)
        {
            try
            {
                return _courseProgramRepository.Update(id, courseProgram);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
            catch (DbUpdateException e)
            {
                if (IsUniqueConstraintException(e))
                    throw CreateFaultException(HttpStatusCode.BadRequest, "Cannot insert duplicate entries");
                throw;
            }
            catch (ValidationException e)
            {
                throw CreateValidationErrorFaultException("Invalid course program", e.ValidationResults.ToArray());
            }
        }
    }
}