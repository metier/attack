﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;
using ValidationException = Metier.Phoenix.Core.Exceptions.ValidationException;
using System.Linq;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// Operations related to product descriptions
    /// </summary>
    public class ProductDescriptionsController : ControllerBase
    {
        private readonly IProductDescriptionRepository _productDescriptionRepository;
        
        public ProductDescriptionsController(IProductDescriptionRepository productDescriptionRepository)
        {
            _productDescriptionRepository = productDescriptionRepository;
        }

        /// <summary>
        /// Returns a product description by its id. Part of the CRUD operations for the resource
        /// </summary>
        /// <param name="id">Product description id</param>
        /// <param name="withoutReferences">If set to true also customer and language information will be included (optional, default = false)</param>
        /// <param name="includeDeleted">If set to true, deleted product descriptions will be included in the result (optional, default = false)</param>
        [HttpGet]
        [Route("productdescriptions/{id}")]
        [AllowRoles(PhoenixRoles.LearningPortal, PhoenixRoles.LearningPortalTokenUser)]
        public ProductDescription GetProductDescription(int id, bool withoutReferences = false, bool includeDeleted = false)
        {
            var productDescription = _productDescriptionRepository.FindById(id, withoutReferences, includeDeleted);

            if (productDescription == null)
            {
                throw CreateFaultException(HttpStatusCode.NotFound, $"Could not find any product descriptions with ID {id}");
            }

            return productDescription;
        }

        /// <summary>
        /// Gets a list of product based on associated ids
        /// </summary>
        /// <param name="productId">Product id</param>
        /// <param name="customerId">Customer id (optional)</param>
        /// <param name="genericOnly">If true customer specific product descriptions will be excluded (optional, default = false)</param>
        /// <param name="simple">If true a simpler object containing less data will be returned (optional, default = false)</param>
        /// <returns></returns>
        [HttpGet]
        [Route("productdescriptions")]
        [ResponseType(typeof(List<ProductDescription>))]
        public HttpResponseMessage GetProductDescriptions([FromUri] int productId, 
                                                          [FromUri] int? customerId = null, 
                                                          [FromUri] bool genericOnly = false, 
                                                          [FromUri] bool simple = false)
        {
            if (simple)
            {
                return Request.CreateResponse(HttpStatusCode.OK, _productDescriptionRepository.FindSimple(productId, customerId, genericOnly));
            }
            return Request.CreateResponse(HttpStatusCode.OK, _productDescriptionRepository.Find(productId, customerId, genericOnly));
        }

        /// <summary>
        /// Creates a new product description. Part of the CRUD operations for the resource
        /// </summary>
        /// <param name="productDescription">The product description being created</param>
        /// <returns>The product description after persisting it to the API</returns>
        [HttpPost]
        [Route("productdescriptions")]
        [ResponseType(typeof(ProductDescription))]
        public HttpResponseMessage CreateProductDescription(ProductDescription productDescription)
        {
            try
            {
                productDescription = _productDescriptionRepository.Create(productDescription);
                return Request.CreateResponse(HttpStatusCode.Created, productDescription);
            }
            catch (LanguageIsAlreadyDefinedException)
            {
                throw CreateValidationErrorFaultException("Invalid product description", new ValidationResult("Language is already defined. Please select a different language."));
            }
            catch (DbUpdateException e)
            {
                throw CreateFaultException(HttpStatusCode.BadRequest, e.Message);
            }
        }

        /// <summary>
        /// <p>Updates an existing product description. Part of the CRUD operations for the resource.</p>
        /// <p>The product description returned from the resource's GET operation can be used for this update process.</p>
        /// <p><span class="caution">CAUTION: Unspecified properties and list items will be deleted.</span></p>
        /// </summary>
        /// <param name="id">Product description id</param>
        /// <param name="productDescription">The updated product description</param>
        [HttpPut]
        [Route("productdescriptions/{id}")]
        public ProductDescription UpdateProductDescription(int id, ProductDescription productDescription)
        {
            try
            {
                productDescription = _productDescriptionRepository.Update(id, productDescription);
            }
            catch (LanguageIsAlreadyDefinedException)
            {
                throw CreateValidationErrorFaultException("Invalid product description", new ValidationResult("Language is already defined. Please select a different language."));
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
            catch (ValidationException e)
            {
                throw CreateValidationErrorFaultException(e.Message, e.ValidationResults.ToArray());
            }
            catch (DbUpdateException e)
            {
                if (IsUniqueConstraintException(e))
                    throw CreateFaultException(HttpStatusCode.BadRequest, "Cannot insert duplicate entries");
                if (IsForeignKeyConstraintException(e))
                    throw CreateFaultException(HttpStatusCode.BadRequest, "Cannot update product description. Foreign reference missing.");
                throw CreateFaultException(HttpStatusCode.BadRequest, e.Message);
            }

            return productDescription;
        }
    }
}