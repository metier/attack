﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Customer related operations.</p>
    /// </summary>
    public class CustomersController : ControllerBase
    {
        private readonly ICustomersProvider _customersProvider;
        private readonly IPerformanceProvider _performanceProvider;
        private readonly IAuthorizationProvider _authorizationProvider;

        public CustomersController(ICustomersProvider customersProvider, IPerformanceProvider performanceProvider, IAuthorizationProvider authorizationProvider)
        {
            _customersProvider = customersProvider;
            _performanceProvider = performanceProvider;
            _authorizationProvider = authorizationProvider;
        }

        /// <summary>
        /// <p>Returns a customer entity. Can be used for CRUD operations.</p>
        /// </summary>
        /// <param name="id">Customer id.</param>
        [HttpGet]
        [Route("customers/{id}")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.EzTokenUser, PhoenixRoles.BehindContentAdmin)]
        public Customer Get(int id)
        {
            var customer = _customersProvider.GetCustomer(id);
            if (customer == null)
            {
                var responseMessage = CreateFaultResponse(HttpStatusCode.NotFound, string.Format("Could not find any customers with ID {0}", id));
                throw new HttpResponseException(responseMessage);
            }
            return customer;
        }

        /// <summary>
        /// <p>Returns a customer entity identified by its external customer id (id from invoicing system)</p>
        /// <p>The external customer id should be unique per distributor (only one invoicing system per distributor)</p>
        /// </summary>
        /// <param name="distributorId">Distributor id</param>
        /// <param name="externalCustomerId">External customer id</param>
        [HttpGet]
        [Route("customers")]
        public Customer Get([FromUri] int distributorId, [FromUri] string externalCustomerId)
        {
            var customer = _customersProvider.GetByExternalCustomer(distributorId, externalCustomerId);
            if (customer == null)
            {
                var responseMessage = CreateFaultResponse(HttpStatusCode.NotFound, string.Format("Could not find any customers with external customer ID {0} for distributor with ID {1}", externalCustomerId, distributorId));
                throw new HttpResponseException(responseMessage);
            }
            return customer;
        }

        /// <summary>
        /// <p>Returns a customer entity identified by its external customer id (id from invoicing system)</p>
        /// <p>The external customer id should be unique per distributor (only one invoicing system per distributor)</p>
        /// </summary>
        /// <param name="distributorId">Distributor id</param>
        /// <param name="isArbitrary">Is this an arbitrary or corporate customer</param>
        /// <param name="companyRegistrationNumber">Company registration number / organization number</param>
        [HttpGet]
        [Route("customers")]
        public Customer Get([FromUri] int distributorId, [FromUri] bool isArbitrary, [FromUri] string companyRegistrationNumber)
        {
            var customer = _customersProvider.GetByCompanyRegistrationNumber(distributorId, isArbitrary, companyRegistrationNumber);
            if (customer == null)
            {
                var responseMessage = CreateFaultResponse(HttpStatusCode.NotFound, string.Format("Could not find any customers of given type (arbitrary / corporate) with company registration number {0} for distributor with ID {1}", companyRegistrationNumber, distributorId));
                throw new HttpResponseException(responseMessage);
            }
            return customer;
        }

        /// <summary>
        /// Creates a new customer. Part of the CRUD operations for this resource.
        /// </summary>
        /// <param name="customer">The customer to create</param>
        [HttpPost]
        [Route("customers")]
        [ResponseType(typeof(Customer))]
        public HttpResponseMessage PostCustomer(Customer customer)
        {
            if (customer == null)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, "Request is not a customer");
            }
            if (customer.Id != 0)
            {
                return CreateFaultResponse(HttpStatusCode.Conflict, string.Format("Can not create new customer from object with existing id {0}", customer.Id));
            }

            try
            {
                customer = _customersProvider.Create(customer);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid customer", e.ValidationResults.ToArray());
            }
            catch (DbUpdateException e)
            {
                if (IsUniqueConstraintException(e))
                    return CreateFaultResponse(HttpStatusCode.BadRequest, "Cannot insert duplicate entries");
            }

            var response = Request.CreateResponse(HttpStatusCode.Created, customer);
            return response;
        }



        /// <summary>
        /// <p>Updates an existing customer. Part of the CRUD operations for this resource.</p>
        /// <p>The customer returned from the resource's GET operation can be used for this update process.</p>
        /// <p><span class="caution">CAUTION: Unspecified properties and list items will be deleted.</span></p>
        /// </summary>
        /// <param name="id">Customer id.</param>
        /// <param name="customer">The customer to update.</param>
        [HttpPut]
        [Route("customers/{id}")]
        [ResponseType(typeof(Customer))]
        public HttpResponseMessage PutCustomer(int id, Customer customer)
        {
            if (customer == null)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, "Request is not a customer");
            }
            if (id != customer.Id)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, "Id in customer data must correspond to customer being updated");
            }

            try
            {
                customer = _customersProvider.Update(id, customer);
            }
            catch (DbUpdateConcurrencyException e)
            {
                return CreateFaultResponse(HttpStatusCode.Conflict, e.Message);
            }
            catch (ValidationException ex)
            {
                return GetValidationErrorFaultResponse("Invalid customer", ex.ValidationResults.ToArray());
            }
            catch (DbUpdateException e)
            {
                if (IsUniqueConstraintException(e))
                    return CreateFaultResponse(HttpStatusCode.BadRequest, "Cannot insert duplicate entries");
                throw;
            }

            var response = Request.CreateResponse(HttpStatusCode.OK, customer);
            return response;
        }

        /// <summary>
        /// <p>Updates an existing customer's invoicing details..</p>
        /// <p>The customer returned from the resource's GET operation can be used for this update process.</p>
        /// <p><span class="caution">CAUTION: Unspecified properties and list items will be deleted.</span></p>
        /// </summary>
        /// <param name="id">Customer id.</param>
        /// <param name="customerInvoicing">The customer invoicing to update.</param>
        [HttpPut]
        [Route("customers/{id}/invoicing")]
        [ResponseType(typeof(Customer))]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.EzTokenUser, PhoenixRoles.BehindContentAdmin)]
        public HttpResponseMessage PutCustomerInvoice(int id, CustomerInvoicing customerInvoicing)
        {
            if (customerInvoicing == null)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, "Request is not a customerInvoicing");
            }
            var customer = _customersProvider.GetCustomer(id);
            if (customer == null)
            {
                var responseMessage = CreateFaultResponse(HttpStatusCode.NotFound, string.Format("Could not find any customers with ID {0}", id));
                throw new HttpResponseException(responseMessage);
            }

            try
            {
                customer.CustomerInvoicing = customerInvoicing;
                customer = _customersProvider.Update(id, customer);
            }
            catch (DbUpdateConcurrencyException e)
            {
                return CreateFaultResponse(HttpStatusCode.Conflict, e.Message);
            }
            catch (ValidationException ex)
            {
                return GetValidationErrorFaultResponse("Invalid customer", ex.ValidationResults.ToArray());
            }
            catch (DbUpdateException e)
            {
                if (IsUniqueConstraintException(e))
                    return CreateFaultResponse(HttpStatusCode.BadRequest, "Cannot insert duplicate entries");
                throw;
            }

            var response = Request.CreateResponse(HttpStatusCode.OK, customer);
            return response;
        }

        /// <summary>
        /// Deletes the customer from the database (hard delete)
        /// </summary>
        /// <param name="id">The id of the customer to delete.</param>
        /// <returns></returns>
        [HttpPut]
        [Route("customers/{id}/delete")]
        public HttpResponseMessage DeleteCustomer(int id)
        {
            try
            {
                _customersProvider.Delete(id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Could not delete customer", e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// Returns a search result with a simple representation of customers qualifying the specified search parameters
        /// </summary>
        /// <param name="distributorId">Distributor id (optional)</param>
        /// <param name="query">Free text search on columns ("Name", "ParentName", "Language") (optional)</param>
        /// <param name="customerIds">A specific list of customer ids (optional)</param>
        /// <param name="childrenOfCustomerId">Gets the children of given customer id (optional)</param>
        /// <param name="orderBy">Column to order result by ("name", "parentname", "language", "isarbitrary") (optional, default = Customer id)</param>
        /// <param name="orderDirection">Order of the result ("asc", "desc") (optional)</param>
        /// <param name="skip">Index of first item in result (optional)</param>
        /// <param name="limit">Max number of items in result (optional, default = 20)</param>
        [HttpGet]
        [Route("customers")]
        [AllowRoles(PhoenixRoles.BehindContentAdmin, PhoenixRoles.LearningPortalTokenUser)]
        [ResponseType(typeof(PartialList<SearchableCustomer>))]
        public HttpResponseMessage Search([FromUri] int? distributorId = null,
                                          [FromUri] string query = null,
                                          [FromUri] int[] customerIds = null,
                                          [FromUri] int? childrenOfCustomerId = null,
                                          [FromUri] string orderBy = null,
                                          [FromUri] string orderDirection = null,
                                          [FromUri] int? skip = null,
                                          [FromUri] int? limit = 20)
        {
            try
            {
                if (customerIds != null && customerIds.Length == 0)
                {
                    customerIds = null;
                }
                var customers = _customersProvider.GetCustomers(distributorId, query, customerIds, childrenOfCustomerId, orderBy, orderDirection, skip, limit);
                return Request.CreateResponse(HttpStatusCode.OK, customers);
            }
            catch (BadRequestException e)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        /// <summary>
        /// Gets the custom leading texts for this customer
        /// </summary>
        /// <param name="id">Customer id</param>
        [HttpGet]
        [AllowRoles(PhoenixRoles.LearningPortal, PhoenixRoles.LearningPortalTokenUser)]
        [Route("customers/{id}/leadingtexts")]
        public List<CustomLeadingText> LeadingTexts(int id)
        {
            if(!_authorizationProvider.HasAnyRole(PhoenixRoles.Phoenix, PhoenixRoles.LearningPortalTokenUser))
            {
                //A user is only allowed to get CustomLeadingTexts from company inside same company tree
                if(!_authorizationProvider.HasCustomer(id, _customersProvider))
                {
                    throw new Exceptions.UnauthorizedException("You do not have access to read the custom leading texts for this company");
                }
            }

            return _customersProvider.GetLeadingTexts(id);
        }

        /// <summary>
        /// Retrieves a summary of the performance user's of the specified customer has achieved on courses they have participated on
        /// </summary>
        /// <param name="id">The ID of a customer</param>
        /// <returns>A list of <see cref="PerformanceSummary"/> objects.</returns>
        [HttpGet]
        [Route("customers/{id}/performances")]
        [AllowRoles(PhoenixRoles.LearningPortal, PhoenixRoles.LearningPortalTokenUser)]
        public List<PerformanceSummary> CustomerPerformances(int id)
        {
            return _performanceProvider.GetPerformanceSummaryByCustomer(id);
        }
    }
}