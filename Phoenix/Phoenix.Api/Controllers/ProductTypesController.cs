﻿using System.Collections.Generic;
using System.Web.Http;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// Handles product type related operations
    /// </summary>
    public class ProductTypesController : ControllerBase
    {
        private readonly IProductTypeRepository _productTypeRepository;

        public ProductTypesController(IProductTypeRepository productTypeRepository)
        {
            _productTypeRepository = productTypeRepository;
        }

        /// <summary>
        /// Returns all the product types in the API
        /// </summary>
        [HttpGet]
        [Route("producttypes")]
        public IEnumerable<ProductType> ProductTypes()
        {
            return _productTypeRepository.GetProductTypes();
        }
    }
}