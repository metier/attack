﻿using System.Data.Entity.Infrastructure;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Collections.Generic;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Core.Utilities.Collections;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Article related operations.</p>
    /// <p>An article is a content description for the components of a product. Depending on the product type different article types can be created.</p>
    /// </summary>
    public class ArticlesController : ControllerBase
    {
        private readonly IArticleRepository _articleRepository;

        public ArticlesController(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        /// <summary>
        /// Returns a representation of the Article entity. Can be used for CRUD operations
        /// </summary>
        /// <param name="id">Article id</param>
        [HttpGet]
        [Route("articles/{id}")]
        [AllowRoles(PhoenixRoles.LearningPortal, PhoenixRoles.LearningPortalTokenUser)]
        public Article GetArtice(int id)
        {
            var article = _articleRepository.FindById(id);
            if (article == null)
            {
                throw CreateFaultException(HttpStatusCode.NotFound, string.Format("Could not find any articles with ID {0}", id));
            }
            return article;
        }

        /// <summary>
        /// Returns a search result of articles qualifying the specified search parameters
        /// </summary>
        /// <param name="productId">Product id (optional)</param>
        /// <param name="status">Article statuses to include in search. All statuses included if not specified (Codes: "Draft", "Published", "Obsolete") (optional)</param>
        /// <param name="query">Free text search on columns ("Title", "Path", "ArticleType", "Language") (optional)</param>
        /// <param name="orderBy">Column to order result by ("path", "title", "status", "language", "type") (optional, default = Article id)</param>
        /// <param name="orderDirection">Order of the result ("asc", "desc") (optional)</param>
        /// <param name="skip">Index of first item in result (optional)</param>
        /// <param name="limit">Max number of items in result (optional, default = 15)</param>
        [HttpGet]
        [Route("articles")]
        [ResponseType(typeof(PartialList<SearchableArticle>))]
        public HttpResponseMessage GetArticles([FromUri] int? productId = null,
                                               [FromUri] string[] status = null,
                                               [FromUri] string query = null,
                                               [FromUri] string orderBy = null,
                                               [FromUri] string orderDirection = null,
                                               [FromUri] int? skip = null,
                                               [FromUri] int? limit = 15)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _articleRepository.Find(productId, status, query, orderBy, orderDirection, skip, limit));
        }

        /// <summary>
        /// <p>Creates a new article. Part of the CRUD operations for this resource.</p>
        /// <p>NOTE: Request object is equal to response object</p>
        /// </summary>
        [HttpPost]
        [Route("articles")]
        [ResponseType(typeof(Article))]
        public async Task<HttpResponseMessage> CreateArticle()
        {
            Article article = await GetDeserializeArticleByType(Request.Content);
            try
            {
                var created = _articleRepository.Create(article);
                return Request.CreateResponse(HttpStatusCode.Created, created);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid article", e);
            }
        }

        /// <summary>
        /// <p>Updates an existing article. Part of the CRUD operations for this resource.</p>
        /// <p>The article returned from the resource's GET operation can be used for this update process.</p>
        /// <p>NOTE: Request body object is equal to response object</p>
        /// <p><span class="caution">CAUTION: Unspecified properties and list items will be deleted.</span></p>
        /// </summary>
        /// <param name="id">Article id</param>
        [HttpPut]
        [Route("articles/{id}")]
        [ResponseType(typeof(OtherArticle))]
        public async Task<Article> UpdateArticle(int id)
        {
            Article article;
            try
            {
                article = await GetDeserializeArticleByType(Request.Content);
                article = _articleRepository.Update(id, article);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
            catch (DbUpdateException e)
            {
                if (IsUniqueConstraintException(e))
                    throw CreateFaultException(HttpStatusCode.BadRequest, "Cannot insert duplicate entries");
                if (IsForeignKeyConstraintException(e))
                    throw CreateFaultException(HttpStatusCode.BadRequest,
                        "Cannot update article. Foreign reference missing.");
                throw;
            }
            catch (ValidationException e)
            {
                throw CreateValidationErrorFaultException("Invalid article", e);
            }

            return article;
        }

        private async Task<Article> GetDeserializeArticleByType(HttpContent content)
        {
            var json = await content.ReadAsStringAsync();
            var jsonObject = JObject.Parse(json);
            var articleTypeId = jsonObject["ArticleTypeId"].Value<int>();

            Article article;
            if (articleTypeId == (int)ArticleTypes.ELearning)
            {
                article = await JsonConvert.DeserializeObjectAsync<ElearningArticle>(json);
            }
            else if (articleTypeId == (int)ArticleTypes.Classroom)
            {
                article = await JsonConvert.DeserializeObjectAsync<ClassroomArticle>(json);
            }
            else if (articleTypeId == (int)ArticleTypes.MultipleChoice || articleTypeId == (int)ArticleTypes.InternalCertification)
            {
                article = await JsonConvert.DeserializeObjectAsync<ExamArticle>(json);
            }
            else if (articleTypeId == (int)ArticleTypes.CaseExam || articleTypeId == (int)ArticleTypes.ProjectAssignment || articleTypeId == (int)ArticleTypes.ExternalCertification)
            {
                article = await JsonConvert.DeserializeObjectAsync<CaseExamArticle>(json);
            }
            else if (articleTypeId == (int)ArticleTypes.Other)
            {
                article = await JsonConvert.DeserializeObjectAsync<OtherArticle>(json);
            }
            else if (articleTypeId == (int)ArticleTypes.Mockexam)
            {
                article = await JsonConvert.DeserializeObjectAsync<MockexamArticle>(json);
            }
            else if (articleTypeId == (int)ArticleTypes.Seminar)
            {
                article = await JsonConvert.DeserializeObjectAsync<SeminarArticle>(json);
            }
            else if (articleTypeId == (int)ArticleTypes.Webinar)
            {
                article = await JsonConvert.DeserializeObjectAsync<WebinarArticle>(json);
            }
            else if (articleTypeId == (int)ArticleTypes.Subscription)
            {
                article = await JsonConvert.DeserializeObjectAsync<SubscriptionArticle>(json);
            }
            else
            {
                throw CreateFaultException(HttpStatusCode.BadRequest, "Article type could not be recognized");
            }
            return article;
        }

        /// <summary>
        /// Returns the articletype for the given id
        /// </summary>
        /// <param name="id">Articletype id</param>
        [HttpGet]
        [Route("articletypes/{id}")]
        [AllowRoles(PhoenixRoles.All)]
        public ArticleType GetArticleType(int id)
        {
            return _articleRepository.GetArticleType(id);
        }

        /// <summary>
        /// Returns all products (both active and inactive) matching current filter
        /// </summary>
        [HttpGet]
        [Route("articletypes")]
        [AllowRoles(PhoenixRoles.All)]
        public IEnumerable<ArticleType> GetArticleTypes([FromUri] string query)
        {
            return _articleRepository.SearchArticleTypes(query);
        }

    }
}