﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Facade.ExamPlayer;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;
using ValidationException = Metier.Phoenix.Core.Exceptions.ValidationException;
using System;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Activity related operations.</p>
    /// <p>An activity represents a time-delimited event associated to a customer article.</p>
    /// </summary>
    public class ActivitiesController : ControllerBase
    {
        private readonly IActivityRepository _activityRepository;
        private readonly IActivityProvider _activityProvider;
        private readonly IPerformanceProvider _performanceProvider;

        public ActivitiesController(IActivityRepository activityRepository, IActivityProvider activityProvider, IPerformanceProvider performanceProvider)
        {
            _activityRepository = activityRepository;
            _activityProvider = activityProvider;
            _performanceProvider = performanceProvider;
        }

        /// <summary>
        /// Returns a representation of the Activity entity. Can be used for CRUD operations
        /// </summary>
        /// <param name="id">Activity id</param>
        /// <param name="searchable">Whether to return a simpler representation of the activity (optional, default = false)</param>
        /// <param name="includeParticipantsWithOrderInformation">Whether to return participants with order information (optional, default = true)</param>
        [HttpGet]
        [Route("activities/{id}")]
        [AllowRoles(PhoenixRoles.LearningPortal, PhoenixRoles.LearningPortalTokenUser)]
        [ResponseType(typeof(Activity))]
        public HttpResponseMessage GetActivity(int id, [FromUri] bool searchable = false, [FromUri] bool includeParticipantsWithOrderInformation = true)
        {
            object activity;
            if (searchable)
            {
                activity = _activityRepository.FindSearchableById(id);
            }
            else
            {
                activity = _activityProvider.Get(id, includeParticipantsWithOrderInformation: includeParticipantsWithOrderInformation);
            }
            if (activity == null)
            {
                throw CreateFaultException(HttpStatusCode.NotFound, string.Format("Could not find any activities with ID {0}", id));
            }
            return Request.CreateResponse(HttpStatusCode.OK, activity);
        }

        /// <summary>
        /// Returns a search result with a simple representation of activities qualifying the specified search parameters
        /// </summary>
        /// <param name="distributorId">Distributor id (optional)</param>
        /// <param name="customerArticleId">CustomerArticle id (optional)</param>
        /// <param name="customerId">Customer id (optional)</param>
        /// <param name="activitySetId">ActivitySet id (optional)</param>
        /// <param name="articleOriginalId">Article id (optional)</param>
        /// <param name="includeActivitySets">ActivitySet id (optional)</param>
        /// <param name="query">Free text search on columns ("Name", "ArticlePath", "ArticleType") (optional)</param>
        /// <param name="orderBy">Column to order result by ("activityend", "activitystart", "articlepath", "articletype", "activitysetname", "rcoexamname", "rcocourseversion", "name") (optional, default = Activity id (desc))</param>
        /// <param name="orderDirection">Order of the result ("asc", "desc") (optional)</param>
        /// <param name="isProposed">Filters activities to be proposed (optional)</param>
        /// <param name="hasFixedPrice">Includes only activities which has a fixed price cost higher than 0 (optional)</param>
        /// <param name="skip">Index of first item in result (optional)</param>
        /// <param name="limit">Max number of items in result (optional, default = 20)</param>
        [HttpGet]
        [Route("activities")]
        [ResponseType(typeof(PartialList<SearchableActivityWithActivitySet>))]
        public HttpResponseMessage GetActivities([FromUri] int? distributorId = null,
                                                [FromUri] int? customerArticleId = null,
                                                [FromUri] int? customerId = null,
                                                [FromUri] int? activitySetId = null,
                                                [FromUri] int? articleOriginalId = null,
                                                [FromUri] bool? includeActivitySets = null,
                                                [FromUri] string query = null,
                                                [FromUri] string orderBy = null,
                                                [FromUri] string orderDirection = null,
                                                [FromUri] bool? isProposed = null,
                                                [FromUri] bool? hasFixedPrice = null,
                                                [FromUri] int? skip = null,
                                                [FromUri] int? limit = 20)
        {
            
            if (includeActivitySets.HasValue && includeActivitySets.Value)
            {
                var activitiesWithActivitySets = _activityRepository.FindWithActivitySets(distributorId, customerArticleId, customerId, activitySetId, articleOriginalId, isProposed, hasFixedPrice, query, orderBy, orderDirection, skip, limit);
                return Request.CreateResponse(HttpStatusCode.OK, activitiesWithActivitySets);
            }
            var activities = _activityRepository.Find(distributorId, customerArticleId, customerId, activitySetId, articleOriginalId, isProposed, hasFixedPrice, query, orderBy, orderDirection, skip, limit);    
            return Request.CreateResponse(HttpStatusCode.OK, activities);
        }

        /// <summary>
        /// Returns the current order for this activity or an empty result if the activity is not in an order
        /// </summary>
        /// <param name="id">Activity id</param>
        [HttpGet]
        [Route("activities/{id}/currentorder")]
        [ResponseType(typeof(Order))]
        public HttpResponseMessage CurrentOrder(int id)
        {
            var order = _activityRepository.GetCurrentOrderForActivity(id);
            if (order == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            return Request.CreateResponse(HttpStatusCode.OK, order);
        }

        /// <summary>
        /// Returns the exam status for all participants enrolled to the activity
        /// </summary>
        /// <param name="id">Activity id</param>
        [HttpGet]
        [Route("activities/{id}/examstatuses")]
        [ResponseType(typeof(IEnumerable<ExamStatus>))]
        public HttpResponseMessage GetExamStatus(int id)
        {
            var examStatuses = _activityProvider.GetExamStatuses(id);
            return Request.CreateResponse(HttpStatusCode.OK, examStatuses);
        }

        /// <summary>
        /// Returns all activity sets which the activity is a part of
        /// </summary>
        /// <param name="id">Activity id</param>
        [HttpGet]
        [Route("activities/{id}/activitysets")]
        [ResponseType(typeof(IEnumerable<ActivitySet>))]
        public IEnumerable<ActivitySet> GetActivitySets(int id)
        {
            return _activityProvider.GetActivitySets(id);
        }

        /// <summary>
        /// Returns the product description of the product which this activity is based on
        /// </summary>
        /// <param name="id">Activity id</param>
        [HttpGet]
        [AllowRoles(PhoenixRoles.EzTokenUser)]
        [Route("activities/{id}/productdescription")]
        [ResponseType(typeof(ProductDescription))]
        public ProductDescription GetProductDescription(int id)
        {
            return _activityProvider.GetProductDescription(id);
        }

        /// <summary>
        /// Creates a new activity. Part of the CRUD operations for this resource.
        /// </summary>
        /// <param name="activity">The activity to create</param>
        /// <returns>The representation of the activity resource after persisting it to the API</returns>
        [HttpPost]
        [Route("activities")]
        [ResponseType(typeof(Activity))]
        public HttpResponseMessage CreateActivity(Activity activity)
        {
            try
            {
                var created = _activityRepository.Create(activity);
                return Request.CreateResponse(HttpStatusCode.Created, created);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid activity", e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// Creates a new activity based on an existing activity. Part of the CRUD operations for this resource.
        /// </summary>
        /// <param name="activity">The activity to create</param>
        /// <param name="copyFromActivityId">The id of the activity to copy information from</param>
        /// <returns>The representation of the activity resource after persisting it to the API</returns>
        [HttpPost]
        [Route("activities/copy/{copyFromActivityId}")]
        [ResponseType(typeof(Activity))]
        public HttpResponseMessage CopyActivity(int copyFromActivityId, Activity activity)
        {
            try
            {
                var created = _activityRepository.Create(activity, copyFromActivityId);
                return Request.CreateResponse(HttpStatusCode.Created, created);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid activity", e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// <p>Updates an existing activity. Part of the CRUD operations for this resource.</p>
        /// <p>The activity returned from the resource's GET operation can be used for this update process.</p>
        /// <p><span class="caution">CAUTION: Unspecified properties and list items will be deleted.</span></p>
        /// </summary>
        /// <param name="id">Activity id</param>
        /// <param name="activity">The activity to update</param>
        [HttpPut]
        [Route("activities/{id}")]
        [ResponseType(typeof(Activity))]
        public HttpResponseMessage UpdateActivity(int id, Activity activity)
        {
            try
            {
                var updated = _activityRepository.Update(id, activity);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid activity", e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// Returns the order line representation of this activity in an order
        /// </summary>
        /// <param name="id">Activity id</param>
        [HttpGet]
        [Route("activities/{id}/orderline")]
        public OrderLine OrderLine(int id)
        {
            var orderLine = _activityProvider.GetOrderLine(id);
            if (orderLine == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return orderLine;
        }

        /// <summary>
        /// Retrieves a summary of the performance the participants of a specified activity has achieved on the course associated to the activity
        /// </summary>
        /// <param name="id">Activity id</param>
        [HttpGet]
        [Route("activities/{id}/performances")]
        [AllowRoles(PhoenixRoles.LearningPortal)]
        [ResponseType(typeof(List<PerformanceSummary>))]
        public List<PerformanceSummary> ActivityPerformances(int id)
        {
            return _performanceProvider.GetPerformanceSummaryByActivity(id);
        }

        /// <summary>
        /// Retrieves all enrollmentconditions for the given activity
        /// </summary>
        /// <param name="id">Activity id</param>
        [HttpGet]
        [Route("activities/{id}/enrollmentconditions")]
        [ResponseType(typeof(List<EnrollmentCondition>))]
        public List<EnrollmentCondition> EnrollmentConditions(int id)
        {
            return _activityProvider.GetEnrollmentConditions(id).ToList();
        }

        /// <summary>
        /// Saves enrollment conditions for a given activity.
        /// To delete items, remove them from the collection and the save-logic will identify them as 'missing' and delete them.
        /// </summary>
        /// <param name="enrollmentConditions"></param>
        /// <param name="id">The id of the activity</param>
        [HttpPut]
        [Route("activities/{id}/enrollmentconditions")]
        [AllowRoles(PhoenixRoles.PhoenixUser)]
        public HttpResponseMessage SaveEnrollmentConditions(IEnumerable<EnrollmentCondition> enrollmentConditions, int id)
        {
            try
            {
                _activityProvider.SaveEnrollmentConditions(enrollmentConditions, id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (ArgumentException exception)
            {
                var responseMessage = exception.Message;
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadRequest, new Fault(responseMessage)));
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e.ValidationResults.ToArray());
            }


        }
    }
}