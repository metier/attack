﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Castle.Core.Logging;
using Metier.Phoenix.Api.Facade.Mail;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;
using NLog;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Handles mail history related operations</p>
    /// <p>When automails are sent from Phoenix the content, recipient, time and status for these are stored. The mail resource provides operations for 
    /// this mail history</p>
    /// </summary>
    public class MailsController : ControllerBase
    {
        private readonly IMailsProvider _mailsProvider;
        //private readonly Logger _logger;
        public MailsController(IMailsProvider mailsProvider)
        {
            _mailsProvider = mailsProvider;
            //_logger = logger;
        }

        /// <summary>
        /// Searches for mail registrations
        /// </summary>
        /// <param name="query">Free text search on columns ("RecipientEmail", "FirstName", "LastName", "Subject") (optional)</param>
        /// <param name="messageType">Integer representation for the message types to be included in the search (optional)</param>
        /// <param name="mailStatus">Integer representation for the mail statuses to be included in the search (optional)</param>
        /// <param name="fromDate">Lower bound e-mail send dates (optional)</param>
        /// <param name="toDate">Upper bound e-mail send dates (optional)</param>
        /// <param name="orderBy">Column to order result by ("recipientemail", "created", "statustime", "mailstatus", "messagetype", "firstname", "lastname", "subject") (optional, default = status time (desc))</param>
        /// <param name="orderDirection">Order of the result ("asc", "desc") (optional)</param>
        /// <param name="skip">Index of first item in result (optional)</param>
        /// <param name="limit">Max number of items in result (optional, default = 20)</param>
        [HttpGet]
        [Route("mails")]
        [ResponseType(typeof(PartialList<SearchableMail>))]
        public HttpResponseMessage Search(
                [FromUri] string query = null,
                [FromUri] int[] messageType = null,
                [FromUri] int[] mailStatus = null,
                [FromUri] DateTime? fromDate = null,
                [FromUri] DateTime? toDate = null,
                [FromUri] string orderBy = null,
                [FromUri] string orderDirection = null,
                [FromUri] int skip = 0,
                [FromUri] int limit = 20)
        {
            var mails = _mailsProvider.Search(query, messageType, mailStatus, fromDate, toDate, orderBy, orderDirection, skip, limit);
            return Request.CreateResponse(HttpStatusCode.OK, mails);
        }

        /// <summary>
        /// Resends an e-mail from the mail history
        /// </summary>
        /// <param name="mailId">Mail id</param>
        [HttpPut]
        [Route("mails/{mailId}/send")]
        [ResponseType(typeof(MailSentResponse))]
        public HttpResponseMessage Send(int mailId)
        {
            var response = _mailsProvider.Send(mailId);
            if (response.Status == MailStatusType.Sent)
            {
                return Request.CreateResponse(HttpStatusCode.OK, response);    
            }
            return Request.CreateResponse(HttpStatusCode.ServiceUnavailable, response);
        }

        /// <summary>
        /// Gets content for a given e-mail. Can be used to provide a preview of the e-mail as it was sent from the server.
        /// </summary>
        /// <param name="mailId">Mail id</param>
        [HttpGet]
        [Route("mails/{mailId}/preview")]
        public MailPreview GetPreview(int mailId)
        {
            return _mailsProvider.GetPreview(mailId);
        }


       
    }
}