﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Metier.Phoenix.Api.Facade;
using NLog;
using ValidationException = Metier.Phoenix.Core.Exceptions.ValidationException;

namespace Metier.Phoenix.Api.Controllers
{
    public abstract class ControllerBase : ApiController
    {
        private Logger _logger;

        protected ControllerBase()
        {
            _logger = LogManager.GetCurrentClassLogger();
        }

        protected HttpResponseMessage CreateFaultResponse(HttpStatusCode httpStatusCode, string message)
        {
            return Request.CreateResponse(httpStatusCode, new Fault(message));
        }
        
        protected HttpResponseException CreateFaultException(HttpStatusCode httpStatusCode, string message)
        {
            var faultResponse = CreateFaultResponse(httpStatusCode, message);
            return new HttpResponseException(faultResponse);
        }

        protected bool IsCannotBeNullConstraintException(DbUpdateException e)
        {
            if (e.InnerException == null || e.InnerException.InnerException == null) return false;
            var sqlException = e.InnerException.InnerException as SqlException;
            return sqlException != null && sqlException.Number == 515;
        }

        protected bool IsForeignKeyConstraintException(DbUpdateException e)
        {
            if (e.InnerException == null || e.InnerException.InnerException == null) return false;
            var sqlException = e.InnerException.InnerException as SqlException;
            return sqlException != null && sqlException.Number == 547;
        }

        protected bool IsUniqueConstraintException(DbUpdateException e)
        {
            if (e.InnerException == null || e.InnerException.InnerException == null) return false;
            var sqlException = e.InnerException.InnerException as SqlException;
            return sqlException != null && sqlException.Number == 2627;
        }
        
        protected HttpResponseException CreateValidationErrorFaultException(string msg, params ValidationResult[] validationResults)
        {
            var httpResponseMessage = GetValidationErrorFaultResponse(msg, validationResults);
            return new HttpResponseException(httpResponseMessage);
        }

        protected HttpResponseMessage GetValidationErrorFaultResponse(string msg, params ValidationResult[] validationResults)
        {
            _logger.Info(msg + Environment.NewLine + string.Join(Environment.NewLine, validationResults.Select(r => r.ErrorMessage)));
            var invalidCustomerResult = Request.CreateResponse(HttpStatusCode.BadRequest, new Fault(msg)
            {
                ValidationErrors = validationResults.Select(e => e.ErrorMessage).ToList()
            });
            return invalidCustomerResult;
        }

        protected HttpResponseException CreateValidationErrorFaultException(string msg, ValidationException validationException)
        {
            var httpResponseMessage = GetValidationErrorFaultResponse(msg, validationException);
            return new HttpResponseException(httpResponseMessage);
        }

        protected HttpResponseMessage GetValidationErrorFaultResponse(string msg, ValidationException validationException)
        {
            var faultResponse = Request.CreateResponse(HttpStatusCode.BadRequest, new Fault(msg)
            {
                ValidationErrors = validationException.ValidationResults.Select(e => e.ErrorMessage).ToList(),
                ErrorCode = validationException.ErrorCode
            });
            _logger.InfoException(msg, validationException);
            return faultResponse;
        }
    }
}