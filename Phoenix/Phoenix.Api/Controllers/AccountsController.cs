﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration.Provider;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Security;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade.Account;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Api.Data.Repositories;
using NLog;
using ChangePassword = Metier.Phoenix.Api.Facade.Account.ChangePassword;
using ValidationException = Metier.Phoenix.Core.Exceptions.ValidationException;
using System.ComponentModel.Design;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Account related operations.</p>
    /// <p>An account represents an end-user in a login/authentication context.</p>
    /// </summary>
    public class AccountsController : ControllerBase
    {
        private readonly IAccountsProvider _accountsProvider;
        private readonly IParticipantRepository _participantRepository;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public AccountsController(IAccountsProvider accountsProvider,
                                  IParticipantRepository participantRepository)
        {
            _accountsProvider = accountsProvider;
            _participantRepository = participantRepository;
            _logger = LogManager.GetCurrentClassLogger();

        }

        /// <summary>
        /// Returns a representation of the UserAccount entity.
        /// </summary>
        /// <param name="userId">Phoenix User id</param>
        [HttpGet]
        [Route("accounts")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.EzTokenUser, PhoenixRoles.BehindContentAdmin)]
        [ResponseType(typeof(UserAccount))]
        public HttpResponseMessage GetAccountByUserId([FromUri] int userId)
        {
            var account = _accountsProvider.GetByUserId(userId);

            if (account == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, account);
        }

        /// <summary>
        /// Returns a representation of the UserAccount entity.
        /// </summary>
        /// <param name="email">User e-mail address</param>
        [HttpGet]
        [Route("accounts")]
        [ResponseType(typeof(UserAccount))]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.EzTokenUser)]
        public HttpResponseMessage GetAccountByEmail([FromUri] string email)
        {
            try
            {
                var account = _accountsProvider.GetByEmail(email);

                if (account == null)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
                return Request.CreateResponse(HttpStatusCode.OK, account);
            }
            catch (EntityNotFoundException e)
            {
                return CreateFaultResponse(HttpStatusCode.NotFound, e.Message);
            }
        }
        
        /// <summary>
        /// Uses the authentication cookie from logged in user to return the UserAccount entity for this user.
        /// </summary>
        [HttpGet]
        [Route("accounts/current")]
        [AllowRoles(PhoenixRoles.All)]
        [ResponseType(typeof(UserAccount))]
        public HttpResponseMessage GetAccountForCurrentUser()
        {
            var account = _accountsProvider.GetCurrentUser();
            if (account == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, account);
        }

        /// <summary>
        /// Returns a representation of the UserAccount entity.
        /// </summary>
        /// <param name="username">Phoenix username</param>
        [HttpGet]
        [Route("accounts")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.BehindContentAdmin, PhoenixRoles.Examiner)]
        [ResponseType(typeof(UserAccount))]
        public HttpResponseMessage GetAccountByUsername([FromUri] string username)
        {
            var account = string.IsNullOrEmpty(username) ? 
                          _accountsProvider.GetCurrentUser() : 
                          _accountsProvider.GetByUsername(username);

            if (account == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, account);
        }

        /// <summary>
        /// <p>Creates an arbitrary user.</p>
        /// <p>Based on provided data, will search for an existing arbitrary customer to map to the user.
        /// If no such arbitrary customer exists, a new one will be created.</p>
        /// </summary>
        /// <param name="request">User data of the arbitrary user/company</param>
        /// <param name="source">Optional string value specifying which system the password reset request is initiated for (Values: Phoenix, LearningPortal, PSOTrainingPortal). 
        /// API determines password reset URL for e-mail sent to user based on this parameter, but ONLY if no passwordResetFormUrl is defined for the related distributor. 
        /// Defaults to 'LearningPortal' if no url defined on the distributor, and parameter is null og empty.</param>
        [HttpPost]
        [Route("accounts/arbitrary")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.EzTokenUser)]
        [ResponseType(typeof(ArbitraryAccountResponse))]
        public HttpResponseMessage CreateArbitraryUser(ArbitraryAccountRequest request, string source = "LearningPortal")
        {
            try
            {
                var createResponse = _accountsProvider.CreateArbitraryAccount(request, source);
                return Request.CreateResponse(HttpStatusCode.Created, createResponse);
            }
            catch (MembershipCreateUserException e)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }
            catch (CreateExternalCustomerException e)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e.ValidationResults.ToArray());
            }            
        }

        /// <summary>
        /// <p>Creates a user</p>
        /// 
        /// <p>Arbitrary customer users:</p>
        /// <p>Users not associated with a customer (i.e. CustomerId = 0) will be regarded as 
        /// "Arbitrary Customer Users". For these users a Customer (internal and external) will be created and mapped to
        /// the user.</p>
        /// <p>For arbitrary customer users, the User's UserInfoElements' CustomLeadingTextId should point to the generic LeadingTextId as provided by:
        /// GET leadingtexts</p>
        /// </summary>
        /// <param name="account">The user account object</param>
        /// <param name="autoGeneratePassword">Defines if the system should auto generate a password for the user (optional, default = false)</param>
        /// <param name="source">Optional string value specifying which system the password reset request is initiated for (Values: Phoenix, LearningPortal, PSOTrainingPortal). 
        /// API determines password reset URL for e-mail sent to user based on this parameter, but ONLY if no passwordResetFormUrl is defined for the related distributor. 
        /// Defaults to 'LearningPortal' if no url defined on the distributor, and parameter is null og empty.</param>
        [HttpPost]
        [Route("accounts")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.EzTokenUser)]
        [ResponseType(typeof(User))]
        public HttpResponseMessage CreateAccount(UserAccount account, [FromUri] bool autoGeneratePassword = false, [FromUri] string source = "LearningPortal")
        {
            User user = null;
            try
            {
                user = _accountsProvider.CreateUser(account, autoGeneratePassword, source);
            }
            catch (MembershipCreateUserException e)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }
            catch (DbUpdateException e)
            {
                if (IsForeignKeyConstraintException(e))
                    return CreateFaultResponse(HttpStatusCode.BadRequest, "User has missing foreign key reference");
            }
            catch (CreateExternalCustomerException e)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e.ValidationResults.ToArray());
            }
            catch (MailSendException e)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }

            var response = Request.CreateResponse(HttpStatusCode.Created, user);
            return response;
        }

        /// <summary>
        /// <p>Updates data for the user account of the posted object.</p>
        /// <ul>
        /// <li>If the "Email" property is different from the data registered in the data store it will be updated</li>
        /// <li>If the "NewUsername" is specified it will update the username accordingly</li>
        /// <li>if the "Roles" list is specified it will replace existing roles of the user with the provided list</li>
        /// <li>The "User" property is set to update an existing User entity. This is a CRUD operation for this entity</li>
        /// </ul>
        /// <p><span class="caution">CAUTION: Unspecified properties and list items in the User entity will be deleted.</span></p>
        /// </summary>
        /// <param name="account">UserAccount object</param>
        [HttpPut]
        [Route("accounts")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.LearningPortal)]
        [ResponseType(typeof(UserAccount))]
        public UserAccount UpdateAccount(UserAccount account)
        {
            try
            {
                account = _accountsProvider.Update(account);
            }
            catch (EntityNotFoundException e)
            {
                throw CreateFaultException(HttpStatusCode.NotFound, e.Message);
            }
            catch (RoleUpdateException e)
            {
                throw CreateValidationErrorFaultException("Invalid user account", new ValidationResult(e.Message));
            }
            catch (UsernameChangeException e)
            {
                throw CreateValidationErrorFaultException("Invalid user account", new ValidationResult(e.Message));
            }
            catch(ProviderException e)
            {
                if (e.Message == "The E-mail supplied is invalid.")
                {
                    throw CreateValidationErrorFaultException("Invalid user account", new ValidationResult("The e-mail is already in use"));
                }
                throw CreateFaultException(HttpStatusCode.BadRequest, e.Message);
            }
            return account;
        }

        /// <summary>
        /// <p>Authenticates the user and creates a user session</p>
        /// <p>Upon a successful login an authentication cookie will be added to the HTTP response. This encrypted cookie can be used to authenticate with the API for 
        /// requests from the same domain as the API reside in.</p>
        /// </summary>
        /// <param name="id">Phoenix userid</param>
        /// <param name="logOn">Correct password should be provided in this object</param>
        [HttpPost]
        [AllowAnonymous]
        [Route("accounts/{id}/logon")]
        [ResponseType(typeof(UserContext))]
        public HttpResponseMessage LogOn(string id, LogOn logOn)
        {
            try
            {
                var userAccount = _accountsProvider.GetByUsername(id);
                if (userAccount == null)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
                else
                {
                    if (_accountsProvider.Validate(id, logOn.Password))
                    {
                        var userContext = _accountsProvider.GetUserContext(id);

                        //JVT: If request contains a Phoenix cookie, remove it (it might be old, added before we added the domain property to the forms authentication in Web.config
                        if (Request.Headers.Any(h => h.Key == "Cookie") && Request.Headers.GetValues("Cookie").FirstOrDefault()?.IndexOf("PhoenixAuth=") > -1)
                        {
                            System.Web.HttpContext.Current.Response.Cookies.Add(new System.Web.HttpCookie("PhoenixAuth")
                            {
                                Domain = null,
                                Expires = DateTime.UtcNow.AddDays(-1),
                            });
                        }
                        FormsAuthentication.SetAuthCookie(id, false);
                        return Request.CreateResponse(HttpStatusCode.OK, userContext);
                    }
                    return GetValidationErrorFaultResponse("Could not authenticate", new[] { new ValidationResult("User does not exist or password is wrong.") });

                }
               
            }
            catch (ValidationException e)
            {
                _logger.Error(e);
                return GetValidationErrorFaultResponse("Authentication failed", e);
            }
            catch(Exception e)
            {
                _logger.Error(e);
                return CreateFaultResponse(HttpStatusCode.BadRequest, "Authentication failed");
            }
        }

        /// <summary>
        /// <p>Impersonate a non-Phoenix user</p>
        /// <p>This encrypted cookie can be used to authenticate with the API for requests to the API.</p>
        /// </summary>
        /// <param name="userId">Non-Phoenix userid to impersonate</param>        
        [HttpGet]        
        [Route("accounts/impersonate/{userId:int}")]
        [ResponseType(typeof(UserContext))]
        public HttpResponseMessage GenerateTokenForAccount(int userId)
        {            
            var user = _accountsProvider.GetByUserId(userId);
            if (user.Roles != null && user.Roles.Contains(PhoenixRoles.Phoenix))
                return CreateFaultResponse(HttpStatusCode.BadRequest, "Can't issue a token for a PhoenixUser");

            var userContext = _accountsProvider.GetUserContext(user.Username);
            FormsAuthentication.SetAuthCookie(user.Username, false);
            return Request.CreateResponse(HttpStatusCode.OK, userContext);            
        }

        /// <summary>
        /// Terminates the user session by removing the authentication cookie
        /// </summary>
        /// <returns>HTTP status code 204 - No Content</returns>
        [HttpPost]
        [Route("accounts/logout")]
        [AllowAnonymous]
        public HttpResponseMessage LogOut()
        {
            FormsAuthentication.SignOut();
            return Request.CreateResponse(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Sets a new password for the user
        /// </summary>
        /// <param name="id">Phoenix username</param>
        /// <param name="changePassword">Object containing the new password to be used</param>
        /// <returns>Empty response</returns>
        [HttpPost]
        [Route("accounts/{id}/changepassword")]
        [AllowRoles(PhoenixRoles.All)]
        public HttpResponseMessage ChangePassword(string id, ChangePassword changePassword)
        {
            try
            {
                if (_accountsProvider.ChangePassword(id, changePassword.NewPassword))
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                return GetValidationErrorFaultResponse("Could not change password", new[] { new ValidationResult("Could not change password. Either user does not exist or old password is wrong.") });
            }
            catch (UnauthorizedException e)
            {
                throw CreateFaultException(HttpStatusCode.Unauthorized, e.Message);
            }
            catch (Exception)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, "Password change failed");
            }
        }

        /// <summary>
        /// Accepts anonymous requests to request a password reset given an e-mail address to an existing user in Phoenix. Will generate a 
        /// unique link for resetting the password and e-mail it to the given user. The link will be customized based on the specified source.
        /// </summary>
        /// <param name="email">E-mail for user resetting password. This e-mail address must match the e-mail address the user is registered with in Phoenix</param>
        /// <param name="source">String value specifying which system the password reset request is initiated for (Values: Phoenix, LearningPortal, PSOTrainingPortal). 
        /// API determines password reset URL for e-mail sent to user based on this parameter, but ONLY if no passwordResetFormUrl is defined for the related distributor. 
        /// Defaults to 'LearningPortal' if no url defined on the distributor, and parameter is null og empty.</param>
        /// <returns>HTTP status code 204 - No Content</returns>
        [HttpPut]
        [Route("accounts/requestpasswordreset")]
        [AllowAnonymous]
        public HttpResponseMessage RequestPasswordReset([FromUri] string email, [FromUri] string source)
        {
            try
            {
                _accountsProvider.RequestPasswordReset(email, source);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message + "  be ok .... " + e.StackTrace, e.ValidationResults.ToArray());
            }
            catch (EntityNotFoundException e)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }
            catch (MailSendException e)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        [HttpGet]
        [Route("accounts/version")]
        [AllowAnonymous]
        public string GetVersioning()
        {
            return "Hi, :)";
        }

            [HttpGet]
        [Route("accounts/requestpasswordresetG")]
        [AllowAnonymous]
        public HttpResponseMessage RequestPasswordResetG([FromUri] string email, [FromUri] string source)
        {
            try
            {
                _accountsProvider.RequestPasswordReset(email, source);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message + "  be ok .... " + e.StackTrace, e.ValidationResults.ToArray());
            }
            catch (EntityNotFoundException e)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }
            catch (MailSendException e)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
        /// <summary>
        /// Allows anonymous requests to set a new password to an existing user. The user must present a unique token received by e-mail from the 
        /// RequestPasswordReset action. If the specified e-mail address matches the e-mail address associated with the token this operation will 
        /// change the password of the corresponding user with the provided password
        /// </summary>
        /// <param name="email">E-mail address for user. Must match e-mail address associated to token</param>
        /// <param name="token">Unique token previously generated in Phoenix for the purpose of resetting user's password</param>
        /// <param name="newPassword">A non-empty string specifying the new password of the user</param>
        /// <returns>HTTP status code 204 - No Content</returns>
        [HttpPut]
        [Route("accounts/resetpassword")]
        [AllowAnonymous]
        public HttpResponseMessage ResetPassword([FromUri] string email, [FromUri] Guid token, string newPassword)
        {
            try
            {
                var passwordReset = _accountsProvider.ResetPassword(token, email, newPassword);
                if (passwordReset)
                {
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                }
                return GetValidationErrorFaultResponse("Could not reset password", new[] { new ValidationResult("Could not reset password. Either provided email address or token is invalid.") });
                
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e.ValidationResults.ToArray());
            }
            catch (EntityNotFoundException e)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }


        /// <summary>
        /// Merges the user with the secondary username into the user with the primary user id.
        /// </summary>
        /// <param name="userId">The id of the user to merge into.</param>
        /// <param name="userIdToMerge">The id of the user to merge. This user will be discontinued (deleted) after merge.</param>
        /// <returns>HttpStatusCode.OK if merge was successful, HttpStatusCode.Conflict if merge could not be fulfilled.</returns>
        [HttpPut]
        [Route("accounts/{userId}/merge/{userIdToMerge}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage MergeUsers(int userId, int userIdToMerge)
        {
            try
            {
                _accountsProvider.MergeAccounts(userId, userIdToMerge);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e.ValidationResults.ToArray());
            }
            catch (Exception e)
            {
                _logger.Error("An error occured when trying to merge users. ", e);
                return GetValidationErrorFaultResponse("Could not merge Users", new[] {new ValidationResult("An unexpected error occured when merging users.") });
            }
        }

        /// <summary>
        /// Uses the authentication cookie from logged in user to return the subscriptions for this user.
        /// </summary>
        [HttpGet]
        [Route("accounts/subscriptions")]
        [AllowRoles(PhoenixRoles.All)]
        [ResponseType(typeof(Subscriptions))]
        public HttpResponseMessage GetSubscriptionsForCurrentUser()
        {
            var account = _accountsProvider.GetCurrentUser();
            var isMetierPlusCustomer = account?.User?.Customer?.IsMetierPlusCustomer == true;
            if (!isMetierPlusCustomer && account?.User?.Id > 0)
            {
                var subscriptions = _participantRepository.Find(null, account.User.Id, null, null, null, false, null, null, 0, 1, null, articleTypeId: (int)ArticleTypes.Subscription, participantIsAvailable: true );
                isMetierPlusCustomer = (subscriptions.TotalCount > 0);
            }
            if (isMetierPlusCustomer)// || new List<int> { 11536, 1450082 , 371 , 11652036 }.IndexOf(account?.User?.Id ?? 0) > -1)
            {
                var subs = new Subscriptions
                {
                    User = account,
                    Items = new List<Subscriptions.Subscription> { new Subscriptions.Subscription { StartDate = DateTime.Parse("2020-01-01"), EndDate = DateTime.Parse("2099-12-31"), SubscriptionId = 1 } }
                };
                return Request.CreateResponse(HttpStatusCode.OK, subs);                                
            }

            return Request.CreateResponse(HttpStatusCode.OK, new Subscriptions { });
        }

    }
}