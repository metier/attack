﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade.ExamPlayer;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Security;
using ValidationException = Metier.Phoenix.Core.Exceptions.ValidationException;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Exam related operations.</p>
    /// </summary>
    public class ExamsController : ControllerBase
    {
        private readonly IExamsProvider _examsProvider;
        
        public ExamsController(IExamsProvider examsProvider)
        {
            _examsProvider = examsProvider;
        }

        /// <summary>
        /// <p>Returns registered data for the given exam.</p>
        /// </summary>
        /// <param name="id">Exam id</param>
        [Route("exams/{id}")]
        [HttpGet]
        [AllowRoles(PhoenixRoles.LearningPortal)]
        [ResponseType(typeof(Exam))]
        public HttpResponseMessage GetExam(int id)
        {
            return ExecuteAndHandleExceptions(() =>
            {
                var exam = _examsProvider.GetExam(id);
                return Request.CreateResponse(HttpStatusCode.OK, exam);
            });
        }

        /// <summary>
        /// Returns information about the exam related to the given activity id. Contains information useful before the exam has been started.
        /// </summary>
        /// <param name="activityId">Activity id</param>
        /// <returns></returns>
        [Route("exams/info")]
        [HttpGet]
        [AllowRoles(PhoenixRoles.LearningPortal)]
        [ResponseType(typeof(ExamInfo))]
        public HttpResponseMessage GetExamInfo([FromUri] int activityId)
        {
            return ExecuteAndHandleExceptions(() =>
            {
                var examInfo = _examsProvider.GetExamInfo(activityId);
                return Request.CreateResponse(HttpStatusCode.OK, examInfo);
            });
        }
        
        /// <summary>
        /// Starts and creates an exam for the specified exam activity for the logged in user.
        /// </summary>
        /// <param name="activityId">Activity id</param>
        [Route("exams/start")]
        [HttpPost]
        [AllowRoles(PhoenixRoles.LearningPortal)]
        [ResponseType(typeof(Exam))]
        public HttpResponseMessage StartExam([FromUri] int activityId)
        {
            return ExecuteAndHandleExceptions(() =>
            {
                var exam = _examsProvider.StartExam(activityId);
                return Request.CreateResponse(HttpStatusCode.OK, exam);
            });
        }

        /// <summary>
        /// Unsubmits a submitted exam so the participant can continue the exam.
        /// </summary>
        /// <param name="id">Exam id</param>
        /// <returns>Returns an exam object, including answers.</returns>
        [Route("exams/{id}/unsubmit")]
        [HttpPut]
        [ResponseType(typeof(Exam))]
        public HttpResponseMessage UnsubmitExam(int id)
        {
            return ExecuteAndHandleExceptions(() =>
            {
                var examResult = _examsProvider.UnsubmitExam(id);
                return Request.CreateResponse(HttpStatusCode.OK, examResult);
            });
        }

        /// <summary>
        /// Gets an exam object, including the answers for the submitted exam.
        /// </summary>
        /// <param name="participantId">Participant id</param>
        [Route("exams/result")]
        [HttpGet]
        [ResponseType(typeof(Exam))]
        public HttpResponseMessage GetExamResult([FromUri] int participantId)
        {
            return ExecuteAndHandleExceptions(() =>
            {
                var examResult = _examsProvider.GetExamResult(participantId);
                return Request.CreateResponse(HttpStatusCode.OK, examResult);
            });
        }

        /// <summary>
        /// Sets the total score for the exam
        /// </summary>
        /// <param name="id">Exam id</param>
        /// <param name="isOverridden">Boolean parameter to specify on the exam whether the total score has been overridden (optional, default = true)</param>
        /// <param name="totalScore">The new total score (optional if overridden = false)</param>
        /// <returns>Empty response</returns>
        [Route("exams/{id}/score")]
        [HttpPut]
        public HttpResponseMessage OverrideTotalScore(int id, [FromUri] bool isOverridden, [FromUri] int totalScore)
        {
            return ExecuteAndHandleExceptions(() =>
            {
                _examsProvider.OverrideTotalScore(id, isOverridden, totalScore);
                return Request.CreateResponse(HttpStatusCode.OK);
            });
        }

        /// <summary>
        /// Gets the current status for the given exam
        /// </summary>
        /// <param name="id">Exam id</param>
        [Route("exams/{id}/status")]
        [HttpGet]
        [AllowRoles(PhoenixRoles.LearningPortal)]
        [ResponseType(typeof(ExamStatus))]
        public HttpResponseMessage GetExamStatus(int id)
        {
            return ExecuteAndHandleExceptions(() =>
            {
                var exam = _examsProvider.GetExamStatus(id);
                return Request.CreateResponse(HttpStatusCode.OK, exam);
            });
        }

        /// <summary>
        /// Submits an answer to an exam.
        /// </summary>
        /// <param name="id">Exam id</param>
        /// <param name="answer">The submitted answer</param>
        /// <returns>No content</returns>
        [Route("exams/{id}/answer")]
        [HttpPut]
        [AllowRoles(PhoenixRoles.LearningPortal)]
        public HttpResponseMessage SubmitAnswer(int id, Answer answer)
        {
            return ExecuteAndHandleExceptions(() =>
            {
                _examsProvider.SubmitAnswer(id, answer);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            });
        }

        /// <summary>
        /// Registered the exam as submitted.
        /// </summary>
        /// <param name="id">Exam id</param>
        [Route("exams/{id}/submit")]
        [HttpPut]
        [AllowRoles(PhoenixRoles.LearningPortal)]
        [ResponseType(typeof(ExamResult))]
        public HttpResponseMessage SubmitExam(int id)
        {
            return ExecuteAndHandleExceptions(() =>
            {
                var result =_examsProvider.SubmitExam(id);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }

        private HttpResponseMessage ExecuteAndHandleExceptions(Func<HttpResponseMessage> response)
        {
            try
            {
                return response();
            }
            catch (EntityNotFoundException e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e);
            }
        }
    }
}