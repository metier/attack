﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Metier.Phoenix.Api.Facade.ExamContent;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// Handles operations related to exam questions
    /// </summary>
    [AllowRoles(PhoenixRoles.BehindContentAdmin)]
    public class QuestionsController : ControllerBase
    {
        private readonly IQuestionsProvider _questionsProvider;

        public QuestionsController(IQuestionsProvider questionsProvider)
        {
            _questionsProvider = questionsProvider;
        }

        /// <summary>
        /// Returns the question with the given id. Can be used for CRUD operations
        /// </summary>
        /// <param name="id">Question id</param>
        [Route("questions/{id}")]
        [HttpGet]
        public Question Get(int id)
        {
            return _questionsProvider.Get(id);
        }

        /// <summary>
        /// Returns all previous versions of the questiong with the given id.
        /// </summary>
        /// <param name="id">Question id</param>
        [Route("questions/{id}/earlierversions")]
        [HttpGet]
        public IEnumerable<Question> GetEarlierVersions(int id)
        {
            return _questionsProvider.GetPreviousVersions(id);
        }

        /// <summary>
        /// Get the question with the newest version related to the question with the given id
        /// </summary>
        /// <param name="id">Question id</param>
        [Route("questions/{id}/newestversion")]
        [HttpGet]
        public Question GetNewestVersion(int id)
        {
            return _questionsProvider.GetNewestVersion(id);
        }

        /// <summary>
        /// Searches for questions
        /// </summary>
        /// <param name="query">Question text (optional)</param>
        /// <param name="tagId">Array of tagIds (optional)</param>
        /// <param name="productId">Array of productIds (optional)</param>
        /// <param name="rcoId">Array of rcoIds (optional)</param>
        /// <param name="difficulty">Array of difficulties (int) (optional)</param>
        /// <param name="status">Array of statuses (int) (optional)</param>
        /// <param name="languageId">Array of languageIds (optional)</param>
        /// <param name="customerSpecificFilterValue">A value indicating wether to include customer specific questions or not (optional).
        /// The various options are 
        ///     0 = 'Show All', 
        ///     1 = 'Show only generic questions'
        ///     2 = 'Show only customer specific questions' 
        /// </param>
        /// <param name="customerId">Array of customerIds (optional)</param>
        /// <param name="lastUsedFromDate">Lower bound date for when the question was last used on an exam</param>
        /// <param name="lastUsedToDate">Upper bound date for when the question was last used on an exam</param>
        /// <param name="orderBy">Column to order result by ("id", "difficulty", "status") (optional, default = id)</param>
        /// <param name="orderDirection">Order of the result ("asc", "desc") (optional)</param>
        /// <param name="skip">Index of first item in result (optional)</param>
        /// <param name="limit">Max number of items in result (optional, default = 20)</param>
        [Route("questions")]
        [HttpGet]
        public PartialList<SearchableQuestion> Search(
            [FromUri] string query = null,
            [FromUri] int[] tagId = null,
            [FromUri] int[] productId = null,
            [FromUri] int[] rcoId = null,
            [FromUri] QuestionDifficulties[] difficulty = null,
            [FromUri] QuestionStatuses[] status = null,
            [FromUri] int[] languageId = null,
            [FromUri] int customerSpecificFilterValue = 0,
            [FromUri] int[] customerId = null,
            [FromUri] DateTime? lastUsedFromDate = null,
            [FromUri] DateTime? lastUsedToDate = null,
            [FromUri] string orderBy = null,
            [FromUri] string orderDirection = null,
            [FromUri] int skip = 0,
            [FromUri] int limit = 20)
        {
            return _questionsProvider.Search(query, tagId, productId, rcoId, difficulty, status, languageId, customerSpecificFilterValue, customerId, lastUsedFromDate, lastUsedToDate, skip, limit, orderBy, orderDirection);
        }

        /// <summary>
        /// Creates a new question. Part of the CRUD operations for this resource.
        /// </summary>
        /// <param name="question">The question to create</param>
        /// <returns>The question after it has been persisted to the API</returns>
        [Route("questions")]
        [HttpPost]
        [ResponseType(typeof(Question))]
        public HttpResponseMessage Create(Question question)
        {
            try
            {
                var created = _questionsProvider.Create(question);
                return Request.CreateResponse(HttpStatusCode.Created, created);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e);
            }
        }

        /// <summary>
        /// Creates a set of questions
        /// </summary>
        /// <param name="questions">Array of questions to create</param>
        /// <returns>List of alle the questions after persisting them to the API</returns>
        [Route("questions/batch")]
        [HttpPost]
        [ResponseType(typeof(List<Question>))]
        public HttpResponseMessage BatchCreate(List<Question> questions)
        {
            try
            {
                var created = _questionsProvider.BatchCreate(questions);
                return Request.CreateResponse(HttpStatusCode.Created, created);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e);
            }
        }

   
        /// <summary>
        /// Updates a set of questions with the supplied changeset
        /// </summary>
        /// <param name="questionsChangeSet">A set of values that should be applied to the referenced questions. 
        /// Must include the set of questionIds to update.</param>
        /// <returns>List of alle the questions after persisting them to the API</returns>
        [Route("questions/batch")]
        [HttpPut]
        [ResponseType(typeof(List<Question>))]
        public HttpResponseMessage BatchUpdate(QuestionsChangeSet questionsChangeSet)
        {
            try
            {
                var updated = _questionsProvider.BatchUpdate(questionsChangeSet);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e);
            }
        }        


        /// <summary>
        /// <p>Updates an existing questions. Part of the CRUD operations for this resource.</p>
        /// <p>The question returned from the resource's GET operation can be used for this update process.</p>
        /// <p><span class="caution">CAUTION: Unspecified properties and list items will be deleted.</span></p>
        /// </summary>
        /// <param name="id">Question id</param>
        /// <param name="question">The updated question</param>
        /// <returns>The question after saving the update to the API</returns>
        [Route("questions/{id}")]
        [HttpPut]
        [ResponseType(typeof(Question))]
        public HttpResponseMessage Update(int id, Question question)
        {
            try
            {
                var updated = _questionsProvider.Update(id, question);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
        }
    }
}