﻿using System.Data.Entity.Infrastructure;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Resource related operations.</p>
    /// <p>A resource represents a location or a person with a given role.</p>
    /// </summary>
    public class ResourcesController : ControllerBase
    {
        private readonly IResourceRepository _resourceRepository;

        public ResourcesController(IResourceRepository resourceRepository)
        {
            _resourceRepository = resourceRepository;
        }

        /// <summary>
        /// Returns a resource entity
        /// </summary>
        /// <param name="id">Resource id</param>
        [HttpGet]
        [Route("resources/{id}")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
        public Resource GetResource(int id)
        {
            var resource = _resourceRepository.FindById(id);
            if (resource == null)
            {
                throw CreateFaultException(HttpStatusCode.NotFound, string.Format("Could not find any resources with ID {0}", id));
            }
            return resource;
        }

        /// <summary>
        /// Returns a partial list of resources matching the criteria
        /// </summary>
        /// <param name="searchColumn">Array of column names ("name", "contact", "resourcetype") to match the query parameter (optional) (empty array means all)</param>
        /// <param name="query">Free text search on columns as defined by the searchColumns parameter (optional)</param>
        /// <param name="type">Resource type id (optional)</param>
        /// <param name="orderBy">Column to order result by ("name", "resourcetypename", "contact", "currency","amount") (optional)</param>
        /// <param name="orderDirection">Order of the result ("asc", "desc") (optional)</param>
        /// <param name="skip">Index of first item in result (optional)</param>
        /// <param name="limit">Max number of items in result (optional)</param>
        [HttpGet]
        [Route("resources")]
        public HttpResponseMessage Search([FromUri] string[] searchColumn,
                                            [FromUri] string query = null, 
                                            [FromUri] int? type = null, 
                                            [FromUri] string orderBy = null, 
                                            [FromUri] string orderDirection = null, 
                                            [FromUri] int skip = 0, 
                                            [FromUri] int limit = 20)
        {
            var resources = _resourceRepository.GetResources(query, searchColumn, type, skip, limit, orderBy, orderDirection);
            return Request.CreateResponse(HttpStatusCode.OK, resources);
        }

        /// <summary>
        /// <p>Creates a new resource entity</p>
        /// <p>NOTE: Request object is equal to response object</p>
        /// </summary>
        [HttpPost]
        [Route("resources")]
        [ResponseType(typeof(Resource))]
        public async Task<HttpResponseMessage> CreateResource()
        {
            var resource = await GetDeserializedResourceByType(Request.Content);
            var createdResource = _resourceRepository.Create(resource);

            var response = Request.CreateResponse(HttpStatusCode.Created, createdResource);
            
            return response;
        }

        /// <summary>
        /// <p>Updates a resource entity</p>
        /// <p>NOTE: Request object is equal to response object</p>
        /// <p class="caution">CAUTION: Unspecified properties and list items will be deleted.s</p>
        /// </summary>
        [HttpPut]
        [Route("resources")]
        [ResponseType(typeof(Resource))]
        public async Task<HttpResponseMessage> UpdateResource()
        {
            var resource = await GetDeserializedResourceByType(Request.Content);
            try
            {
                var updateResource = _resourceRepository.Update(resource);
                return Request.CreateResponse(HttpStatusCode.OK, updateResource);
            }
            catch (DbUpdateException e)
            {
                if (IsUniqueConstraintException(e))
                    throw CreateFaultException(HttpStatusCode.BadRequest, "Cannot insert duplicate entries");
                if (IsForeignKeyConstraintException(e))
                    throw CreateFaultException(HttpStatusCode.BadRequest, "Cannot update resource. Foreign reference missing.");
                if (IsCannotBeNullConstraintException(e))
                    throw CreateFaultException(HttpStatusCode.BadRequest, "Cannot update resource. A required field in the resource was missing a value.");
                throw;
            }
        }

        private async Task<Resource> GetDeserializedResourceByType(HttpContent content)
        {
            var json = await content.ReadAsStringAsync();
            var jsonObject = JObject.Parse(json);
            var resourceTypeId = jsonObject["ResourceTypeId"].Value<int>();

            Resource resource;
            if (resourceTypeId == 1 ||
                resourceTypeId == 3 ||
                resourceTypeId == 5 ||
                resourceTypeId == 6 ||
                resourceTypeId == 7)
            {
                resource = await JsonConvert.DeserializeObjectAsync<UserResource>(json);
            }
            else if (resourceTypeId == 2 ||
                     resourceTypeId == 4)
            {
                resource = await JsonConvert.DeserializeObjectAsync<Resource>(json);
            }
            else
            {
                throw CreateFaultException(HttpStatusCode.BadRequest, "Resource type could not be recognized");
            }
            return resource;
        }
    }
}