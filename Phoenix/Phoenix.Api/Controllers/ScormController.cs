﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Exceptions;
using NLog;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// SCORM related operations
    /// </summary>    
    public class ScormController : ControllerBase
    {
        private readonly IScormProvider _scormProvider;
        private readonly IAccountsProvider _accountsProvider;
        private readonly Logger _logger;

        public ScormController(IScormProvider scormProvider, IAccountsProvider accountsProvider)
        {
            _scormProvider = scormProvider;
            _accountsProvider = accountsProvider;

            _logger = LogManager.GetCurrentClassLogger();
        }

        /// <summary>
        /// Get SCORM session
        /// </summary>
        /// <param name="sessionId">A unique token for the session id</param>
        /// <returns>An object representing a SCORM session</returns>
        [HttpGet]
        [AllowRoles(PhoenixRoles.LearningPortal)]
        [Route("scorm/{sessionId}")]
        public HttpResponseMessage Get(Guid sessionId)
        {
            try
            {
                _logger.Info($"Getting SCORM session {sessionId}");
                var attempt = _scormProvider.GetAttempt(sessionId, activeOnly: false);
                return Request.CreateResponse(HttpStatusCode.OK, attempt);
            }
            catch (UnauthorizedException e)
            {
                throw CreateFaultException(HttpStatusCode.Unauthorized, e.Message);
            }
            catch (EntityNotFoundException e)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
        
        /// <summary>
        /// Creates a new SCORM session for the current user. Old active sessions will be auto terminated.
        /// </summary>
        /// <param name="rcoId">The id (number) of an RCO object in the CMS</param>
        /// <returns>A unique token for the session id</returns>
        [HttpPost]
        [AllowRoles(PhoenixRoles.LearningPortal)]
        [Route("scorm/create")]
        public HttpResponseMessage Create([FromUri] int rcoId)
        {
            try
            {
                _logger.Info($"Creating SCORM session {rcoId}");
                var currentUser = _accountsProvider.GetCurrentUser();
                if (currentUser == null || currentUser.User == null)
                {
                    throw new EntityNotFoundException("Could not find current user");
                }
                var attempt = _scormProvider.CreateSession(currentUser.User.Id, rcoId);
                _logger.Info($"SCORM session created {attempt.SessionId} {attempt.UserId} {attempt.Id}");
                return Request.CreateResponse(HttpStatusCode.Created, attempt.SessionId.ToString());
            }
            catch (UnauthorizedException e)
            {
                _logger.Error(e);
                throw CreateFaultException(HttpStatusCode.Unauthorized, e.Message);
            }
            catch (EntityNotFoundException e)
            {
                _logger.Error(e);
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return CreateFaultResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        /// <summary>
        /// Updates the score of the SCORM session
        /// </summary>
        /// <param name="sessionId">A unique token for the session id</param>
        /// <param name="score">The updated score (number)</param>
        /// <returns></returns>
        [HttpPut]
        [AllowRoles(PhoenixRoles.LearningPortal)]
        [Route("scorm/{sessionId}/score/{score}")]
        public HttpResponseMessage UpdateScore(Guid sessionId, int score)
        {
            try
            {
                _scormProvider.UpdateScore(sessionId, score);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (UnauthorizedException e)
            {
                throw CreateFaultException(HttpStatusCode.Unauthorized, e.Message);
            }
            catch (EntityNotFoundException e)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        /// <summary>
        /// Updates the status of the SCORM session
        /// </summary>
        /// <param name="sessionId">A unique token for the session id</param>
        /// <param name="status">The updated status ('N', 'I', 'C', 'P', 'F')</param>
        /// <returns></returns>
        [HttpPut]
        [AllowRoles(PhoenixRoles.LearningPortal)]
        [Route("scorm/{sessionId}/status/{status}")]
        public HttpResponseMessage UpdateStatus(Guid sessionId, string status)
        {
            try
            {
                _scormProvider.UpdateStatus(sessionId, status);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (UnauthorizedException e)
            {
                throw CreateFaultException(HttpStatusCode.Unauthorized, e.Message);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e.ValidationResults.ToArray());
            }
            catch (EntityNotFoundException e)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
        
        /// <summary>
        /// Updates the suspend data of the SCORM session
        /// </summary>
        /// <param name="sessionId">A unique token for the session id</param>
        /// <param name="suspendData">A JSON object representing the state of the GUI</param>
        /// <returns></returns>
        [HttpPut]
        [AllowRoles(PhoenixRoles.LearningPortal)]
        [Route("scorm/{sessionId}/suspenddata")]
        public HttpResponseMessage UpdateSuspendData(Guid sessionId, object suspendData)
        {
            try
            {
                if (suspendData != null && suspendData.GetType() != typeof(System.String))
                    suspendData = Newtonsoft.Json.JsonConvert.SerializeObject(suspendData);

                _scormProvider.UpdateSuspendData(sessionId, (string)suspendData);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (UnauthorizedException e)
            {
                throw CreateFaultException(HttpStatusCode.Unauthorized, e.Message);
            }
            catch (EntityNotFoundException e)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
        
        /// <summary>
        /// <p>Terminates the SCORM session and copies the state of the session over to the associated performance.</p>
        /// <p>If the session is a final test and status is "Completed", all other related performances will be completed.</p>
        /// </summary>
        /// <param name="sessionId">A unique token for the session id</param>
        /// <returns></returns>
        [HttpPut]
        [AllowRoles(PhoenixRoles.LearningPortal)]
        [Route("scorm/{sessionId}/terminate")]
        public HttpResponseMessage Terminate([FromUri] Guid sessionId)
        {
            try
            {
                _scormProvider.TerminateSession(sessionId);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (UnauthorizedException e)
            {
                throw CreateFaultException(HttpStatusCode.Unauthorized, e.Message);
            }
            catch (EntityNotFoundException e)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        /// <summary>
        /// Reset the SCORM Performance and SCORM attempt for
        /// </summary>
        /// <param name="rcoId">A unique token for the session id</param>
        /// <param name="userId">The Id of the user</param>
        /// <param name="includeChildren">Also reset the children of the Rco</param>
        /// <returns></returns>
        [HttpPut]        
        [AllowRoles(PhoenixRoles.EzTokenUser, PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.PhoenixUser)]
        [Route("scorm/reset")]
        public HttpResponseMessage ResetRcoStatus([FromUri] int rcoId, [FromUri] int userId,[FromUri] bool includeChildren = false)
        {            
            try
            {
                _scormProvider.ResetRcoStatus(rcoId,userId, includeChildren);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (UnauthorizedException e)
            {
                throw CreateFaultException(HttpStatusCode.Unauthorized, e.Message);
            }
            catch (EntityNotFoundException e)
            {
                return CreateFaultResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}