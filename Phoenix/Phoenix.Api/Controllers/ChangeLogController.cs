﻿using System.Web.Http;
using Metier.Phoenix.Core.Business.ChangeLog;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// The change log resource is a general log of defined events happening to entities in Phoenix. A change log entry defines entity types  and change log types 
    /// for entities in the system. These changes are logged by the API and can be retrieved via this resouce
    /// </summary>
    public class ChangeLogController : ControllerBase
    {
        private readonly IChangeLogProvider _changeLogProvider;

        public ChangeLogController(IChangeLogProvider changeLogProvider)
        {
            _changeLogProvider = changeLogProvider;
        }

        /// <summary>
        /// Returns change log entries for a specific entity
        /// </summary>
        /// <param name="entityId">Entity id</param>
        /// <param name="entityType">The entity type which the entity id is given for (Supported entities: 1 (Participant))</param>
        /// <param name="skip">Index of first item in result (optional, default = 0)</param>
        /// <param name="limit">Max number of items in result (optional, default = 10)</param>
        /// <param name="type">Type of change log entry (1 (Created), 2 (Modified), 3 (StatusChanged), 4 (ExamSubmitted), 5 (GradeSet), 6 (CandidateSurveySent)</param>
        [HttpGet]
        [Route("changelog")]
        public PartialList<ChangeLogEntryDto> GetEntries([FromUri] int entityId, [FromUri] ChangeLogEntityTypes entityType, [FromUri] int skip = 0, [FromUri] int limit = 10, [FromUri] ChangeLogTypes? type = null)
        {
            return _changeLogProvider.GetEntries(entityId, entityType, skip, limit, type);
        }
    }
}