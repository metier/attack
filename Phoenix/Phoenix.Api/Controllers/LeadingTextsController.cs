﻿using System.Collections.Generic;
using System.Web.Http;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Handles leading text related operations</p>
    /// <p>A leading text is a predefined information element with a default label. These labels can be overridden on customer and user level, and 
    /// visibility/requirement of them can be changed. These configurations determine grouping and layout of invoices from participations in Phoenix 
    /// courses</p>
    /// </summary>
    public class LeadingTextsController : ApiController
    {
        private readonly ILeadingTextRepository _leadingTextRepository;

        public LeadingTextsController(ILeadingTextRepository leadingTextRepository)
        {
            _leadingTextRepository = leadingTextRepository;
        }

        /// <summary>
        /// Gets the list of preconfigured, default leading texts in Phoenix
        /// </summary>
        [HttpGet]
        [Route("leadingtexts")]
        [AllowRoles(PhoenixRoles.All)]
        public List<LeadingText> Get()
        {
            return _leadingTextRepository.GetLeadingTexts();
        }
    }
}