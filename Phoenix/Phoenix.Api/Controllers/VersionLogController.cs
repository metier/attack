﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Version log related operations</p>
    /// </summary>
    public class VersionLogController : ControllerBase
    {
        private readonly IVersionLogProvider _versionLogProvider;

        public VersionLogController(IVersionLogProvider versionLogProvider)
        {
            _versionLogProvider = versionLogProvider;
        }

        /// <summary>
        /// Returns a partial list of Version log items ordered from newest to oldest
        /// </summary>
        /// <param name="skip">Index of first item in result (optional)</param>
        /// <param name="limit">Max number of items in result (optional)</param>
        [HttpGet]
        [Route("versionlog")]
        [AllowRoles(PhoenixRoles.Phoenix)]
        public PartialList<VersionLogItem> GetAll([FromUri] int? skip = null, [FromUri] int? limit = 20)
        {
            return _versionLogProvider.GetVersionLogItems(skip, limit);
        }

    }
}