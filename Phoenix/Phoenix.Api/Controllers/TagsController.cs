﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Tag related operations</p>
    /// <p>A tag represents textual meta data</p>
    /// </summary>
    public class TagsController : ControllerBase
    {
        private readonly ITagsProvider _tagsProvider;

        public TagsController(ITagsProvider tagsProvider)
        {
            _tagsProvider = tagsProvider;
        }

        /// <summary>
        /// Returns a partial list of tags matching the search criteria
        /// </summary>
        /// <param name="query">Free text search on columns ("Text") (optional)</param>
        /// <param name="tagIds">Array of specific Tag ids to include (optional)</param>
        /// <param name="orderBy">Column to order result by ("text") (optional)</param>
        /// <param name="orderDirection">Order of the result ("asc", "desc") (optional)</param>
        /// <param name="skip">Index of first item in result (optional)</param>
        /// <param name="limit">Max number of items in result (optional)</param>
        [HttpGet]
        [Route("tags")]
        [AllowRoles(PhoenixRoles.BehindContentAdmin)]
        public PartialList<Tag> Search([FromUri] string query = null,
                                        [FromUri] int[] tagIds = null,
                                        [FromUri] string orderBy = null,
                                        [FromUri] string orderDirection = null,
                                        [FromUri] int? skip = null,
                                        [FromUri] int? limit = 20)
        {
            return _tagsProvider.SearchTags(query, tagIds, orderBy, orderDirection, skip, limit);
        }

        /// <summary>
        /// Creates a new Tag
        /// </summary>
        /// <param name="tag">The tag object</param>
        [HttpPost]
        [Route("tags")]
        [AllowRoles(PhoenixRoles.BehindContentAdmin)]
        [ResponseType(typeof(Tag))]
        public HttpResponseMessage Create(Tag tag)
        {
            try
            {
                var created = _tagsProvider.Create(tag);
                return Request.CreateResponse(HttpStatusCode.Created, created);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse(e.Message, e);
            }
        }
    }
}