﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Http.Headers;

using System.Web.Http.Description;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Business.FileStorage;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Core.Utilities.Collections;
using System.Web.Security;
using NLog;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>User related operations.</p>
    /// <p>A user represent an end-user connected to a Customer.</p>
    /// </summary>
    public class UsersController : ControllerBase
    {
        private readonly MetierLmsContext _context;
        private readonly IFileStorage _fileStorage;
        private readonly IUsersRepository _usersRepository;
        private readonly IPerformanceProvider _performanceProvider;
        private readonly IUserCompetenceProvider _userCompetenceProvider;
        private readonly IUserDiplomaProvider _userDiplomaProvider;
        private readonly IIdentityProvider _identityProvider;
        private readonly Logger _logger;

        private readonly IAuthorizationProvider _authorizationProvider;

        public UsersController(MetierLmsContext context,
                                IFileStorage fileStorage,
                                IUsersRepository usersRepository, 
                               IPerformanceProvider performanceProvider, 
                               IUserCompetenceProvider userCompetenceProvider,
                               IAuthorizationProvider authorizationProvider,
                               IUserDiplomaProvider userDiplomaProvider,
                               IIdentityProvider identityProvider)
        {
            _context = context;
            _fileStorage = fileStorage;
            _usersRepository = usersRepository;
            _performanceProvider = performanceProvider;
            _userCompetenceProvider = userCompetenceProvider;
            _authorizationProvider = authorizationProvider;
            _userDiplomaProvider = userDiplomaProvider;
            _identityProvider = identityProvider;
            _logger = LogManager.GetCurrentClassLogger();

        }

        /// <summary>
        /// Returns a partial list of users matching the search criteria
        /// </summary>
        /// <param name="distributorId">Distributor id (optional)</param>
        /// <param name="customerId">Customer id (optional)</param>
        /// <param name="userId">Id of user (optional)</param>
        /// <param name="activityId">Activity id (optional)</param>
        /// <param name="activitySetId">Activity Set id (optional)</param>
        /// <param name="phoenixUsersOnly">Whether to limit the results to Phoenix users only (i.e. admin users) (optional, default = false)</param>
        /// <param name="instructorUsersOnly">Whether to limit the results to instructor resource users (optional, default = false)</param>
        /// <param name="ecoachUsersOnly">Whether to limit the results to eCoach resource users (optional, default = false)</param>
        /// <param name="includeParticipations">Whether to include performances for the returned users (optional, default = false)</param>
        /// <param name="query">Free text search on columns ("FirstName", "LastName", "CustomerName", "Email", "UserName") (optional)</param>
        /// <param name="alsoSearchCustomFields">Whether query should also be performed on custom fields (optional)</param>
        /// <param name="advancedQuery">Pipe separated list of complex query expressions. For each set, perform query against the provided columns (ie. FirstName,LeadingText15:foo|LastName:bar) (optional)</param>
        /// <param name="isActive">Flag to only show active/inactive users (optional)</param>		
        /// <param name="orderBy">Column to order result by ("customername", "email", "firstname", "lastname","username","isphoenixuser") (optional)</param>
        /// <param name="orderDirection">Order of the result ("asc", "desc") (optional)</param>
        /// <param name="skip">Index of first item in result (optional)</param>
        /// <param name="limit">Max number of items in result (optional)</param>
        /// <returns></returns>
        [HttpGet]
        [Route("users")]
        [AllowRoles(PhoenixRoles.LearningPortal)]
        [ResponseType(typeof(PartialList<SearchableUser>))]
        public HttpResponseMessage Get([FromUri] int? distributorId = null,
                                        [FromUri] int? customerId = null,
                                        [FromUri] int? userId = null,
                                        [FromUri] int? activityId = null,
                                        [FromUri] int? activitySetId = null,
                                        [FromUri] bool? phoenixUsersOnly = false,
										[FromUri] bool? instructorUsersOnly = false,
										[FromUri] bool? ecoachUsersOnly = false,                                        
                                        [FromUri] bool? includeParticipations = false,
                                        [FromUri] string query = null,
                                        [FromUri] bool? alsoSearchCustomFields = false,
										[FromUri] string advancedQuery = null,
										[FromUri] bool? isActive = null,										
										[FromUri] string orderBy = null, 
                                        [FromUri] string orderDirection = null,										
										[FromUri] int? skip = null, 
                                        [FromUri] int? limit = 20)
        {
            PartialList<SearchableUser> result = _usersRepository.Search(distributorId, customerId, userId, activityId, activitySetId, phoenixUsersOnly.Value, instructorUsersOnly.Value, ecoachUsersOnly.Value, includeParticipations.Value, query, alsoSearchCustomFields.Value, advancedQuery, isActive, orderBy, orderDirection, skip, limit);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// Returns all the performances (course, lessons, exams) for a user for a given article or activity
        /// </summary>
        /// <param name="userId">The user id</param>
        /// <param name="articleId">The id of an e-learning article (optional)</param>
        /// <param name="activityId">The id of an e-learning activity (optional)</param>
        [HttpGet]
        [Route("users/{userId}/performances")]
        [AllowRoles(PhoenixRoles.LearningPortal, PhoenixRoles.LearningPortalTokenUser)]
        public IEnumerable<PerformanceInfo> GetPerformances(int userId, [FromUri] int? articleId = null, [FromUri] int? activityId = null)
        {
            try
            {
                if (articleId.HasValue)
                {
                    return new List<PerformanceInfo> { _performanceProvider.GetPerformancesByArticle(userId, articleId.Value) };
                }
                if (activityId.HasValue)
                {
                    return new List<PerformanceInfo> {_performanceProvider.GetPerformancesByActivity(userId, activityId.Value) };
                }
                return _performanceProvider.GetPerformancesByUserId(userId);
            }
            catch (ArgumentException e)
            {
                throw CreateFaultException(HttpStatusCode.BadRequest, e.Message);
            }
        }

        /// <summary>
        /// Returns the user competence form
        /// </summary>
        /// <param name="userId"></param>
        [HttpGet]
        [Route("users/{userId}/competence")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.LearningPortal)]
        [ResponseType(typeof(UserCompetence))]
        public HttpResponseMessage GetUserCompetence(int userId)
        {
            if (!_authorizationProvider.Is(userId) && !_authorizationProvider.HasAnyRole(PhoenixRoles.Phoenix, PhoenixRoles.LearningPortalTokenUser))
                throw new Exceptions.UnauthorizedException("You do not have access to read the competency form for this user");

            var competence = _userCompetenceProvider.GetByUserId(userId);
            if (competence == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            return Request.CreateResponse(HttpStatusCode.OK, competence);
        }


        /// <summary>
        /// Retrieves a attachment by the given file id and creation timestamp.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="id">File id</param>        
        /// <returns>The file is returned as a stream where the content type is set to application/octet-stream (binary file).</returns>
        [HttpGet]
        [Route("users/{userId}/competence/file/{id}")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.LearningPortal)]
        public HttpResponseMessage GetCompetencyFile (int userId, int id)
        {
            try
            {
                var competence = _userCompetenceProvider.GetByUserId(userId);
                if (competence.Attachments.Any(file => file.FileId == id))
                    return GetFileById(id);

                throw new Exception();                
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

        }

        /// <summary>
        /// <p>Updates the user competence form.</p>
        /// <p class="caution">CAUTION: Unspecified properties will be deleted</p>
        /// </summary>
        /// <param name="userId">User id</param>
        /// <param name="userCompetence">User competence form</param>
        [HttpPut]
        [Route("users/{userId}/competence")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.LearningPortal)]
        [ResponseType(typeof(UserCompetence))]
        public HttpResponseMessage UpdateUserCompetence(int userId, UserCompetence userCompetence)
        {
            if (!_authorizationProvider.Is(userId) && !_authorizationProvider.HasAnyRole(PhoenixRoles.Phoenix, PhoenixRoles.LearningPortalTokenUser))
                throw new Exceptions.UnauthorizedException("You do not have access to read the competency form for this user");

            try
            {
                var updated = _userCompetenceProvider.Update(userId, userCompetence);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid competence", e);
            }
            catch (DbUpdateConcurrencyException e)
            {
                return CreateFaultResponse(HttpStatusCode.Conflict, e.Message);
            }
        }

        /// <summary>
        /// Creates a new user competence form
        /// </summary>
        /// <param name="userId">User id</param>
        /// <param name="userCompetence">User competence form</param>
        [HttpPost]
        [Route("users/{userId}/competence")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.LearningPortal)]
        [ResponseType(typeof(UserCompetence))]
        public HttpResponseMessage CreateUserCompetence(int userId, UserCompetence userCompetence)
        {
            if (!_authorizationProvider.Is(userId) && !_authorizationProvider.HasAnyRole(PhoenixRoles.Phoenix, PhoenixRoles.LearningPortalTokenUser))
                throw new Exceptions.UnauthorizedException("You do not have access to read the competency form for this user");

            try
            {
                var created = _userCompetenceProvider.Create(userId, userCompetence);
                return Request.CreateResponse(HttpStatusCode.Created, created);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid competence", e); 
            }
        }



        /// <summary>
        /// Returns the user diploma form
        /// </summary>
        /// <param name="userId"></param>
        [HttpGet]
        [Route("users/{userId}/diplomas")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.LearningPortal)]
        [ResponseType(typeof(UserCompetence))]
        public HttpResponseMessage GetUserDiplomas(int userId)
        {
            if (!_authorizationProvider.Is(userId) && !_authorizationProvider.HasAnyRole(PhoenixRoles.Phoenix, PhoenixRoles.LearningPortalTokenUser))
                throw new Exceptions.UnauthorizedException("You do not have access to read the competency form for this user");

            var competence = _userDiplomaProvider.GetByUserId(userId);
            if (competence == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            return Request.CreateResponse(HttpStatusCode.OK, competence);
        }

        /// <summary>
        /// Creates a new user diploma form
        /// </summary>
        /// <param name="userId">User id</param>
        /// <param name="userDiploma">User diploma form</param>
        [HttpPost]
        [Route("users/{userId}/diplomas")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.LearningPortal)]
        [ResponseType(typeof(UserCompetence))]
        public HttpResponseMessage CreateUserDiploma(int userId, UserDiploma userDiploma)
        {
            if (!_authorizationProvider.Is(userId) && !_authorizationProvider.HasAnyRole(PhoenixRoles.Phoenix, PhoenixRoles.LearningPortalTokenUser))
                throw new Exceptions.UnauthorizedException("You do not have access to read the competency form for this user");

            try
            {
                var created = _userDiplomaProvider.Create(userId, userDiploma);

                return Request.CreateResponse(HttpStatusCode.Created, created);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid diploma", e);
            }
        }

        /// <summary>
        /// Edit a existing user diploma 
        /// </summary>
        /// <param name="userId">User id</param>
        /// <param name="userDiploma">User diploma form</param>
        [HttpPut]
        [Route("users/{userId}/diplomas")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.LearningPortal)]
        [ResponseType(typeof(UserCompetence))]
        public HttpResponseMessage EditUserDiploma(int userId, UserDiploma userDiploma)
        {
            if (!_authorizationProvider.Is(userId) && !_authorizationProvider.HasAnyRole(PhoenixRoles.Phoenix, PhoenixRoles.LearningPortalTokenUser))
                throw new Exceptions.UnauthorizedException("You do not have access to read the competency form for this user");

            try
            {
                var updated = _userDiplomaProvider.Update(userId, userDiploma);

                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid diploma", e);
            }
        }

        /// <summary>
        /// Edit a existing user diploma 
        /// </summary>
        /// <param name="userId">User id</param>
        /// <param name="userDiploma">User diploma form</param>
        [HttpDelete]
        [Route("users/{userId}/diplomas/{diplomaId}")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.LearningPortal)]
        [ResponseType(typeof(UserCompetence))]
        public HttpResponseMessage DeleteUserDiploma(int userId, int diplomaId)
        {
            if (!_authorizationProvider.Is(userId) && !_authorizationProvider.HasAnyRole(PhoenixRoles.Phoenix, PhoenixRoles.LearningPortalTokenUser))
                throw new Exceptions.UnauthorizedException("You do not have access to read the competency form for this user");

            try
            {
                var removed = _userDiplomaProvider.Remove(diplomaId);

                return Request.CreateResponse(HttpStatusCode.OK, removed);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid diploma", e);
            }
        }

        [HttpGet]
        [Route("users/{userId}/transcriptfiles/{id}")]
        [AllowRoles(PhoenixRoles.LearningPortal)]
        public HttpResponseMessage GetFile(int userId, int id)
        {
            try
            {
                var currentUserName = _identityProvider.GetCurrentUser().Identity.Name;
                var user = _usersRepository.FindByUsername(currentUserName);
                var diplomas = _userDiplomaProvider.GetByUserId(user.Id);
                if (user != null && user.Id == userId && diplomas.Any(d => d.FileId == id))
                {
                    return GetFileById(id);
                } else
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

        }


        private HttpResponseMessage GetFileById(int id)
        {
            var file = _context.Files.FirstOrDefault(f => f.Id == id);
            if (file == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            var fileStream = _fileStorage.GetFileStream(file);

            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StreamContent(fileStream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = file.Filename;
            return result;
        }
    }
}