﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Security;
using Code = Metier.Phoenix.Api.Facade.Code;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// Static code related operations
    /// </summary>
    public class CodesController : ApiController
    {
        private readonly ICodeRepository _codeRepository;

        public CodesController(ICodeRepository codeRepository)
        {
            _codeRepository = codeRepository;
        }

        /// <summary>
        /// Returns the static codes for a given code type
        /// </summary>
        /// <param name="type">Code type name</param>
        [HttpGet]
        [Route("codes")]
        [AllowRoles(PhoenixRoles.All)]
        public IEnumerable<Code> Get([FromUri] string type)
        {
            try
            {
                return _codeRepository.GetCodes(type);
            }
            catch (ArgumentException)
            {
                var responseMessage = Request.CreateResponse(HttpStatusCode.BadRequest, new Fault("No codes with type: " + type));
                throw new HttpResponseException(responseMessage);
            }
        }
    }
}