﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Facade.Participant;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Business;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Handles operations related to the Participant resource</p>
    /// <p>A participant is a user enrolled to an activity/activity set.</p>
    /// </summary>
    public class ParticipantsController : ControllerBase
    {
        private readonly IParticipantRepository _participantRepository;
        private readonly IParticipantsProvider _participantsProvider;
        private readonly IArticleRepository _articleRepository;
        private readonly IParticipantBusiness _participantBusiness;
        private readonly IAuthorizationProvider _authorizationProvider;

        public ParticipantsController(IParticipantRepository participantRepository, 
                                      IParticipantsProvider participantsProvider, 
                                      IArticleRepository articleRepository,
                                      IParticipantBusiness participantBusiness,
                                      IAuthorizationProvider authorizationProvider)
        {
            _participantRepository = participantRepository;
            _participantsProvider = participantsProvider;
            _articleRepository = articleRepository;
            _participantBusiness = participantBusiness;
            _authorizationProvider = authorizationProvider;
        }

        /// <summary>
        /// Gets the data for a single participant. Can be used for CRUD operations
        /// </summary>
        /// <param name="id">Participant id</param>
        [HttpGet]
        [Route("participants/{id}")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser, PhoenixRoles.LearningPortal)]
        public Participant GetParticipant(int id)
        {
            var participant = _participantRepository.FindById(id);
            if (participant == null)
            {
                throw CreateFaultException(HttpStatusCode.NotFound, string.Format("Could not find any participants with ID {0}", id));
            }
            if (!_authorizationProvider.Is(participant.UserId) && !_authorizationProvider.HasAnyRole(PhoenixRoles.Phoenix, PhoenixRoles.LearningPortalTokenUser))
                throw new Exceptions.UnauthorizedException("You do not have access to read the participation for this user");

            return participant;
        }

        /// <summary>
        /// Gets the data for a single participant.
        /// </summary>
        /// <param name="id">Participant id</param>
        [HttpGet]
        [Route("searchableparticipants/{id}")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
        public SearchableParticipant GetSearchableParticipant(int id)
        {
            var participant = _participantRepository.FindSearchableParticipantById(id);
            if (participant == null)
            {
                throw CreateFaultException(HttpStatusCode.NotFound, string.Format("Could not find any searchable participants with ID {0}", id));
            }
            return participant;
        }

        /// <summary>
        /// Retrieve all participants related to the given enrollmentId.
        /// </summary>
        /// <param name="enrollmentId">Enrollment id</param>
        [HttpGet]
        [Route("searchableparticipants/enrollments/{enrollmentId}")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
        public HttpResponseMessage GetSearchableParticipantsByEnrollmentId(int enrollmentId)
        {
            var participants = _participantsProvider.GetParticipationsForEnrollment(enrollmentId);
            if (participants == null)
            {
                throw CreateFaultException(HttpStatusCode.NotFound, string.Format("Could not find any searchable participants related to enrollmentId: {0}", enrollmentId));
            }

            return Request.CreateResponse(HttpStatusCode.OK, participants);
        }

        /// <summary>
        /// Gets the data for a single participant. 
        /// Include orderlines and order information in the response object.
        /// </summary>
        /// <param name="id">Participant id</param>
        [HttpGet]
        [Route("participants/{id}/includeOrderInformation")]
        public Participant GetParticipantWithOrderInformation(int id)
        {
            var participant = _participantRepository.FindById(id, true);
            if (participant == null)
            {
                throw CreateFaultException(HttpStatusCode.NotFound, string.Format("Could not find any participants with ID {0}", id));
            }
            return participant;
        }

        /// <summary>
        /// Gets information about the course associated to the activity this participation belongs to
        /// </summary>
        /// <param name="id">Participant id</param>
        [HttpGet]
        [Route("participants/{id}/course")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
        [ResponseType(typeof(RcoCourse))]
        public HttpResponseMessage GetRcoCourse(int id)
        {
            var course = _participantsProvider.GetRcoCourse(id);
            if (course == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            return Request.CreateResponse(HttpStatusCode.OK, course);
        }

        /// <summary>
        /// Gets information about the exam container associated to the activity this participation belongs to
        /// </summary>
        /// <param name="id">Participant id</param>
        [HttpGet]
        [Route("participants/{id}/examcontainer")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
        [ResponseType(typeof(RcoExamContainer))]
        public HttpResponseMessage GetRcoExamContainer(int id)
        {
            var examContainer = _participantsProvider.GetRcoExamContainer(id);
            if (examContainer == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            return Request.CreateResponse(HttpStatusCode.OK, examContainer);
        }

        /// <summary>
        /// Search for participants
        /// </summary>
        /// <param name="query">Free text search on columns ("ActivityName", "ActivitySetName", "FirstName", "LastName") (optional)</param>
        /// <param name="userId">User id (optional)</param>
        /// <param name="customerId">Customer id (optional)</param>
        /// <param name="activitySetId">ActivitySet id (optional)</param>
        /// <param name="activityId">Activity id (optional)</param>
        /// <param name="activityIds">Activity ids (optional)</param>
        /// <param name="includeParticipantInfoElements">Flag to indicate whether ParticipantInfoElements should be returned (optional)</param>
        /// <param name="isInvoiced">Boolean value for filtering participant's invoicing status (optional)</param>
        /// <param name="includeParticipantsEnrolledInActivitySetButThroughOtherActivitySet">Boolean value for including participants that are under activity set, but where participant is created through a different activity set (optional)</param>        
        /// <param name="orderBy">Column to order result by ("firstname", "lastname", "unitprice", "activitysetname", "activityname") (optional, default = Activity set name)</param>
        /// <param name="orderDirection">Order of the result ("asc", "desc") (optional)</param>
        /// <param name="skip">Index of first item in result (optional)</param>
        /// <param name="limit">Max number of items in result. Specify a negative number to query without any limit. (optional, default = 20)</param>
        /// <returns></returns>
        [HttpGet]
        [Route("participants")]
        [AllowRoles(PhoenixRoles.LearningPortal, PhoenixRoles.LearningPortalTokenUser)]
        public HttpResponseMessage Search(
            [FromUri] string query = null,
            [FromUri] int? userId = null,
            [FromUri] int? customerId = null,
			[FromUri] int? activitySetId = null, 
			[FromUri] int? activityId = null,
            [FromUri] List<int> activityIds = null,
            [FromUri] bool? includeParticipantInfoElements = false,			
            [FromUri] bool? isInvoiced = null,
            [FromUri] bool includeParticipantsEnrolledInActivitySetButThroughOtherActivitySet = false,
            [FromUri] string orderBy = null,
            [FromUri] string orderDirection = null,
            [FromUri] int skip = 0,
            [FromUri] int limit = 20)
        {
            if (activityIds == null) activityIds = new List<int>();
            if (activityId.HasValue) activityIds.Add(activityId.Value);            
            var participants = _participantRepository.Find(query, userId, customerId, activitySetId, activityIds, includeParticipantInfoElements.Value, orderBy, orderDirection, skip, limit, isInvoiced, includeParticipantsEnrolledInActivitySetButThroughOtherActivitySet: includeParticipantsEnrolledInActivitySetButThroughOtherActivitySet);
            return Request.CreateResponse(HttpStatusCode.OK, participants);
        }

        /// <summary>
        /// Retrieves the current order/invoice this participation belongs to
        /// </summary>
        /// <param name="id">Participant id</param>
        [HttpGet]
        [Route("participants/{id}/currentorder")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
        [ResponseType(typeof(Order))]
        public HttpResponseMessage CurrentOrder(int id)
        {
            var order = _participantRepository.GetCurrentOrderForParticipant(id);
            if (order == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);    
            }
            return Request.CreateResponse(HttpStatusCode.OK, order);
        }

        /// <summary>
        /// Creates a new participant. Part of the CRUD operations for this resource.
        /// </summary>
        /// <param name="participant">The participant to create</param>
        /// <returns>The data stored for the participant after persisting it to the API</returns>
        [HttpPost]
        [Route("participants")]
        [ResponseType(typeof(Participant))]
        public HttpResponseMessage CreateParticipant(Participant participant)
        {
            try
            {
                var created = _participantRepository.Create(participant);
                return Request.CreateResponse(HttpStatusCode.Created, created);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid participant", e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// <p>Updates an existing participant. Part of the CRUD operations for this resource.</p>
        /// <p>The participant returned from the resource's GET operation can be used for this update process.</p>
        /// <p><span class="caution">CAUTION: Unspecified properties and list items will be deleted.</span></p>
        /// </summary>
        /// <param name="id">Participant id</param>
        /// <param name="participant">The updated participant object</param>
        /// <returns>The data stored for the participant after persisting it to the API</returns>
        [HttpPut]
        [Route("participants/{id}")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
        [ResponseType(typeof(Participant))]
        public HttpResponseMessage UpdateParticipant(int id, Participant participant)
        {
            try
            {
                var updated = _participantRepository.Update(id, participant);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid participant", e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// Updates the status of the participant.
        /// </summary>
        /// <param name="id">Participant ID</param>
        /// <param name="status">The status code string (Valid values: "CANCELLED", "ENROLLED", "COMPLETED", "NOSHOW", "DISPENSATION", "ONHOLD", "INPROGRESS")</param>
        /// <returns></returns>
        [HttpPut]
        [Route("participants/{id}/status/{status}")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
        public HttpResponseMessage ChangeStatus(int id, string status)
        {
            try
            {
                var changed = _participantsProvider.ChangeStatus(id, status);
                return Request.CreateResponse(HttpStatusCode.OK, changed);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid request", e.ValidationResults.ToArray());
            }
        }

		/// <summary>
		/// Updates the participant.
		/// </summary>
		/// <param name="id">Participant ID</param>
		/// <param name="updateRequest">Update request</param>		
		/// <returns></returns>
		[HttpPut]
		[Route("participants/{id}/status")]
		[AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
		public HttpResponseMessage UpdateParticipant(int id, ParticipantUpdateRequest updateRequest)
		{
			try
			{
				var changed = _participantsProvider.UpdateParticipant(id, updateRequest);
				return Request.CreateResponse(HttpStatusCode.OK, changed);
			}
			catch (ValidationException e)
			{
				return GetValidationErrorFaultResponse("Invalid request", e.ValidationResults.ToArray());
			}
		}

		/// <summary>
		/// Updates the grade of the participant of a Case Exam or Project Assignment
		/// </summary>
		/// <param name="id">Participant ID</param>
		/// <param name="grade">The exam grade (Valid values: A, B, C, D, E, F). Empty value will remove grade and results.</param>
		/// <returns></returns>
		[HttpPut]
        [Route("participants/{id}/grade")]
        [AllowRoles(PhoenixRoles.Examiner)]
        public HttpResponseMessage SetExamGrade(int id, [FromUri] string grade)
        {
            try
            {
                var changed = _participantsProvider.SetExamGrade(id, grade);
                return Request.CreateResponse(HttpStatusCode.OK, changed);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid request", e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// Moves the participant to another activity / activity set.
        /// </summary>
        /// <param name="id">Participant id</param>
        /// <param name="moveRequest">Specification of which activity and activity set the participant is being moved to</param>
        [HttpPut]
        [Route("participants/{id}/move")]
        [ResponseType(typeof(Participant))]
        public HttpResponseMessage Move(int id, ParticipantMoveRequest moveRequest)
        {
            try
            {
                var moved = _participantsProvider.Move(id, moveRequest);
                return Request.CreateResponse(HttpStatusCode.OK, moved);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid request", e.ValidationResults.ToArray());
            }
        }


        ///// <summary>
        ///// Gets order line information for this participation.
        ///// </summary>
        ///// <param name="id">Participant id</param>
        //[HttpGet]
        //[Route("participants/{id}/orderline")]
        //public OrderLine OrderLine(int id)
        //{
        //    var orderLine = _participantsProvider.GetOrderLine(_participantRepository.FindById(id));
        //    if (orderLine == null)
        //    {
        //        throw new HttpResponseException(HttpStatusCode.NotFound);
        //    }
        //    return orderLine;
        //}

        /// <summary>
        /// Returns availability information for a specific participant. Contains a boolean value specifying whether the given activity 
        /// should be made available for the participant, and the current availability period
        /// </summary>
        /// <param name="id">Participant ID</param>
        /// <returns>Object specifying whether given activity is available and availability period</returns>
        [HttpGet]
        [Route("participants/{id}/availability")]
        [AllowRoles(PhoenixRoles.LearningPortal, PhoenixRoles.LearningPortalTokenUser)]
        public HttpResponseMessage Availability(int id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, _participantBusiness.GetAvailability(id));
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid request", e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// Returns a specific time when the participant should have completed its associated activity
        /// </summary>
        /// <param name="id">Participant ID</param>
        /// <returns>An ISO8601 formatted date and time string representing the due date</returns>
        [HttpGet]
        [Route("participants/{id}/duedate")]
        [AllowRoles(PhoenixRoles.LearningPortal, PhoenixRoles.LearningPortalTokenUser)]
        public HttpResponseMessage DueDate(int id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, _participantBusiness.GetDueDate(id));
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid request", e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// Submits the project assigment or case exam
        /// </summary>
        /// <param name="id">Participant ID</param>
        /// <param name="attachment">A file that has been uploaded through the files-API</param>
        /// <returns></returns>
        [HttpPut]
        [Route("participants/{id}/submitexam")]
        [AllowRoles(PhoenixRoles.LearningPortal)]
        public HttpResponseMessage SubmitExam(int id, Attachment attachment)
        {
            try
            {
                _participantsProvider.SubmitExam(id, attachment);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid request", e.ValidationResults.ToArray());
            }
            catch (ArgumentException)
            {
                return GetValidationErrorFaultResponse("Invalid request");
            }
        }

        /// <summary>
        /// Gets the article of the customer article associated with this participant (via activity)
        /// </summary>
        /// <param name="id">Participant id</param>
        [HttpGet]
        [Route("participants/{id}/article")]
        [AllowRoles(PhoenixRoles.LearningPortalTokenUser)]
        public Article GetArticle(int id)
        {
            var article = _articleRepository.FindByParticpantId(id);
            if (article == null)
            {
                throw CreateFaultException(HttpStatusCode.NotFound, string.Format("Could not find any articles with ID {0}", id));
            }
            return article;
        }

        /// <summary>
        /// Gets a list of exam submissions assigned to the current user
        /// </summary>
        /// <param name="includeGraded">Whether to include already graded exam submissions</param>
        /// <param name="skip"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("participants/examsubmissions")]
        [AllowRoles(PhoenixRoles.Examiner)]
        public PartialList<ExamSubmission> GetExamSubmissionsForCurrentUser([FromUri] bool includeGraded = false, [FromUri] int skip = 0, [FromUri] int limit = 20)
        {
            return _participantsProvider.GetExamSubmissionsForCurrentUser(includeGraded, skip, limit);
        }
    }
}