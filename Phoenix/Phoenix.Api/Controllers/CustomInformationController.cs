﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// Static code related operations
    /// </summary>
    public class CustomInformationController : ApiController
    {
        private readonly ICustomInformationRepository _customInformationRepository;

        public CustomInformationController(ICustomInformationRepository customInformationRepository)
        {
            _customInformationRepository = customInformationRepository;
        }

        /// <summary>
        /// Returns the custom information for a given entity, identified by entityId and entitytype.
        /// </summary>
        /// <param name="entitytype">Entity type name, either 'Activity' or 'CustomerArticle'</param>
        /// <param name="entityid">Entity id</param>
        [HttpGet]
        [Route("custominformation")]
        [AllowRoles(PhoenixRoles.PhoenixUser)]
        public IEnumerable<CustomInformation> Get([FromUri] string entitytype, [FromUri] int entityid)
        {
            try
            {
                return _customInformationRepository.GetCustomInformation(entitytype, entityid);
            }
            catch (ArgumentException exception)
            {
                var responseMessage = exception.Message;
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadRequest, new Fault(responseMessage)));
            }
        }

        /// <summary>
        /// Saves the custom information for a given entity, identified by entityId and entitytype.
        /// </summary>
        /// <param name="customInformationSet"></param>
        /// <param name="entitytype">Entity type name, either 'Activity' or 'CustomerArticle'</param>
        /// <param name="entityid">Entity id</param>
        [HttpPut]
        [Route("custominformation/{entitytype}/{entityid}")]
        [AllowRoles(PhoenixRoles.PhoenixUser)]
        public IEnumerable<CustomInformation> Save(IEnumerable<CustomInformation> customInformationSet, string entitytype, int entityid)
        {
            try
            {
                return _customInformationRepository.Save(customInformationSet, entitytype, entityid);
            }
            catch (ArgumentException exception)
            {
                var responseMessage = exception.Message;

                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadRequest, new Fault(responseMessage)));
            }
        }

        /// <summary>
        /// Deletes the custom information item with the given id
        /// </summary>
        /// <param name="id">Id of the custom information item</param>
        [HttpDelete]
        [Route("custominformation/{id}")]
        [AllowRoles(PhoenixRoles.PhoenixUser)]
        public HttpResponseMessage DeleteCustomInformation(int id)
        {
            try
            {
                _customInformationRepository.Delete(id);

                var response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            catch (ArgumentException exception)
            {
                var responseMessage = exception.Message;

                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadRequest,
                    new Fault(responseMessage)));
            }
        }

        /// <summary>
        /// Deletes the custom information item with the given id
        /// </summary>
        /// <param name="entitytype">EntityType of the related entity ('CustomerArticle' or 'Activity')</param>
        /// <param name="entityid">EntityId of the related entity</param>
        [HttpDelete]
        [Route("custominformation")]
        [AllowRoles(PhoenixRoles.PhoenixUser)]
        public HttpResponseMessage DeleteAllCustomInformation([FromUri] string entitytype, [FromUri] int entityid)
        {
            try
            {
                _customInformationRepository.DeleteAll(entitytype, entityid);

                var response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            catch (ArgumentException exception)
            {
                var responseMessage = exception.Message;

                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadRequest,
                    new Fault(responseMessage)));
            }

        }

    }
}