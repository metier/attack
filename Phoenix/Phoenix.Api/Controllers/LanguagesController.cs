﻿using System.Collections.Generic;
using System.Web.Http;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Api.Security;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Handles language support related operations</p>
    /// </summary>
    public class LanguagesController : ControllerBase
    {
        private readonly ILanguageRepository _languageRepository;

        public LanguagesController(ILanguageRepository languageRepository)
        {
            _languageRepository = languageRepository;
        }

        /// <summary>
        /// Retrieves a list of all the supported languages in Phoenix
        /// </summary>
        [HttpGet]
        [Route("languages")]
        [AllowRoles(PhoenixRoles.All)]
        public IEnumerable<Language> Get()
        {
            return _languageRepository.GetAll();            
        }
    }
}