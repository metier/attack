﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Facade.Participant;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;
using ValidationException = Metier.Phoenix.Core.Exceptions.ValidationException;

namespace Metier.Phoenix.Api.Controllers
{
    /// <summary>
    /// <p>Handles order/invoice related operations</p>
    /// <p>The order resource describes an order/invoice in Phoenix.</p>
    /// </summary>
    public class OrdersController : ControllerBase
    {
        private readonly IOrdersProvider _ordersProvider;

        public OrdersController(IOrdersProvider ordersProvider)
        {
            _ordersProvider = ordersProvider;
        }

        /// <summary>
        /// Gets an overview of every order line in a stored order in Phoenix
        /// </summary>
        /// <param name="id">Order id</param>
        [HttpGet]
        [Route("orders/orderlines")]
        public IEnumerable<OrderLine> OrderLines([FromUri] int id)
        {
            IEnumerable<OrderLine> orderLines = _ordersProvider.GetOrderInfoLinesByOrderId(id);
            if (orderLines == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return orderLines;
        }


        /// <summary>
        /// Gets information abot the content of an existing order
        /// </summary>
        /// <param name="id">Order id</param>
        [HttpGet]
        [Route("orders/{id}")]
        public OrderInfo GetOrderInfoById(int id)
        {
            var order = _ordersProvider.GetOrderInfo(id);
            if (order == null)
            {
                throw CreateFaultException(HttpStatusCode.NotFound, string.Format("Could not find any orders with ID {0}", id));
            }
            return order;
        }

        /// <summary>
        /// Gets information abot the content which would be the result of creating a proposed order
        /// </summary>
        /// <param name="groupByKey">Groupbykey is a "key" containing a filtered down order based on grouping from various parts of the order. Valid groupbykey's 
        /// can be found by searching for proposed orders</param>
        [HttpGet]
        [Route("orders")]
        public OrderInfo GetOrderInfoByGroupByKey([FromUri] string groupByKey)
        {
            var order = _ordersProvider.GetProposedOrderInfo(groupByKey);
            if (order == null)
            {
                throw CreateFaultException(HttpStatusCode.NotFound, string.Format("Could not create an order matching groupbykey {0}", groupByKey));
            }
            return order;
        }

        /// <summary>
        /// Returns a search result for proposed and existing orders
        /// </summary>
        /// <param name="query">Free text search on columns ("OrderNumber", "Title", "CoordinatorFullName", "YourOrder", "YourRef", "OurRef", "InvoiceNumber") (optional)</param>
        /// <param name="distributorId">Distributor id (optional)</param>
        /// <param name="coordinatorId">User id of coordinator (optional)</param>
        /// <param name="statusFilter">Integer representation of order statuses to filter by (optional)</param>
        /// <param name="customerTypeFilter">Integer representation of customer types to filter by (optional)</param>
        /// <param name="fromDate">Lower bound of order creation dates (Earliest invoice date for proposed orders) (optional)</param>
        /// <param name="toDate">Upper bound of order creation dates (Earliest invoice date for proposed orders) (optional)</param>
        /// <param name="orderBy">Column to order result by ("ordernumber", "title", "customername", "programcoordinator", "yourorder", "yourref", "ourref", "amount", "lastmodified", "status", "invoicenumber") (optional, default = Last modified)</param>
        /// <param name="orderDirection">Order of the result ("asc", "desc") (optional)</param>
        /// <param name="skip">Index of first item in result (optional)</param>
        /// <param name="limit">Max number of items in result (optional, default = 20)</param>
        [HttpGet]
        [Route("orders")]
        [ResponseType(typeof(PartialList<SearchableOrder>))]
        public HttpResponseMessage Search(
            [FromUri] string query = null, 
            [FromUri] int? distributorId = null, 
            [FromUri] int? coordinatorId = null, 
            [FromUri] int[] statusFilter = null, 
            [FromUri] int[] customerTypeFilter = null,
            [FromUri] DateTime? fromDate = null,
            [FromUri] DateTime? toDate = null,
            [FromUri] string orderBy = null,
            [FromUri] string orderDirection = null,
            [FromUri] int skip = 0,
            [FromUri] int limit = 20)
        {
            var invoices = _ordersProvider.Search(query, distributorId, coordinatorId, statusFilter, customerTypeFilter, fromDate, toDate, skip, limit, orderBy, orderDirection);
            return Request.CreateResponse(HttpStatusCode.OK, invoices);
        }

        /// <summary>
        /// Creates a new order. Part of the CRUD operations for this resource.
        /// </summary>
        /// <param name="order">The order to be created</param>
        /// <returns>The created order as it was persisted to the API</returns>
        [HttpPost]
        [Route("orders")]
        [ResponseType(typeof(OrderInfo))]
        public HttpResponseMessage Create(OrderInfo order)
        {
            try
            {
                var createdOrderInfo = _ordersProvider.Create(order);
                return Request.CreateResponse(HttpStatusCode.OK, createdOrderInfo);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Not a valid order", e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// <p>Updates an existing order. Part of the CRUD operations for this resource.</p>
        /// <p>The order returned from the resource's GET operation can be used for this update process.</p>
        /// <p><span class="caution">CAUTION: Unspecified properties and list items will be deleted.</span></p>
        /// </summary>
        /// <param name="id">Order id</param>
        /// <param name="order">The updated order</param>
        /// <returns>The updated order as it was persisted to the API</returns>
        [HttpPut]
        [Route("orders/{id}")]
        [ResponseType(typeof(OrderInfo))]
        public HttpResponseMessage Update(int id, OrderInfo order)
        {
            try
            {
                var updated = _ordersProvider.Update(id, order);
                return Request.CreateResponse(HttpStatusCode.OK, updated);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid order", e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// <p>Adds selected participant(s) to given order, unless they're already added.</p>
        /// </summary>
        /// <param name="id">Order id</param>
        /// <param name="participantIds">Participant ids</param>
        /// <returns>The updated order as it was persisted to the API</returns>
        [HttpPut]
        [Route("orders/{id}/addParticipants")]
        [ResponseType(typeof(OrderInfo))]
        public HttpResponseMessage AddParticipants(int id, int[] participantIds)
        {
            try
            {
                var orderInfo = _ordersProvider.AddParticipantsToOrder(id, participantIds);
                return Request.CreateResponse(HttpStatusCode.OK, orderInfo);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("One or more invalid participantIds", e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// <p>Adds selected fixed price activities to given order, unless they're already added.</p>
        /// </summary>
        /// <param name="id">Order id</param>
        /// <param name="activityIds">Participant ids</param>
        /// <returns>The updated order as it was persisted to the API</returns>
        [HttpPut]
        [Route("orders/{id}/addActivities")]
        [ResponseType(typeof(OrderInfo))]
        public HttpResponseMessage AddActitivies(int id, int[] activityIds)
        {
            try
            {
                var orderInfo = _ordersProvider.AddActivitiesToOrder(id, activityIds);
                return Request.CreateResponse(HttpStatusCode.OK, orderInfo);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("One or more invalid activityIds", e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// <p>Removes selected orderline(s) from given order.</p>
        /// </summary>
        /// <param name="id">Order id</param>
        /// <param name="orderLineIds">Orderline ids</param>
        /// <returns>The updated order as it was persisted to the API</returns>
        [HttpPut]
        [Route("orders/{id}/removeOrderLines")]
        [ResponseType(typeof(OrderInfo))]
        public HttpResponseMessage RemoveOrderLines(int id, int[] orderLineIds)
        {
            try
            {
                var orderInfo = _ordersProvider.RemoveOrderlinesFromOrder(id, orderLineIds);
                return Request.CreateResponse(HttpStatusCode.OK, orderInfo);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("One or more invalid orderLineIds", e.ValidationResults.ToArray());
            }
        }


        /// <summary>
        /// Deletes the order
        /// </summary>
        /// <param name="id">Order id</param>
        /// <returns>202 Accepted</returns>
        [HttpDelete]
        [Route("orders/{id}")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                _ordersProvider.Delete(id);
                return Request.CreateResponse(HttpStatusCode.Accepted);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid order", e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// Invoices an order (via the distributor's configured invoice provider)
        /// </summary>
        /// <param name="id">Order id</param>
        /// <param name="workOrder">Work order for the order (optional)</param>
        [HttpPut]
        [Route("orders/invoice")]
        [ResponseType(typeof(OrderInfo))]
        public HttpResponseMessage Invoice([FromUri] int id, [FromUri] string workOrder = null)
        {
            try
            {
                var invoicedOrder = _ordersProvider.InvoiceByOrderId(id, workOrder);
                return Request.CreateResponse(HttpStatusCode.OK, invoicedOrder);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid order", e.ValidationResults.ToArray());
            }
            catch (CreateExternalCustomerException e)
            {
                return GetValidationErrorFaultResponse("invalid customer data", new ValidationResult(e.Message));
            }
        }

        /// <summary>
        /// Invoices a proposed order by its groupbykey (via the distributor's configured invoice provider)
        /// </summary>
        /// <param name="groupByKey">Groupbykey is a "key" containing a filtered down order based on grouping from various parts of the order. Valid groupbykey's 
        /// can be found by searching for proposed orders</param>
        /// <param name="workOrder">Work order for the proposed order (optional)</param>
        [HttpPut]   
        [Route("orders/invoice")]
        [ResponseType(typeof(OrderInfo))]
        public HttpResponseMessage Invoice([FromUri] string groupByKey, [FromUri] string workOrder = null)
        {
            try
            {
                var invoicedOrder = _ordersProvider.InvoiceByGroupByKey(groupByKey, workOrder);
                return Request.CreateResponse(HttpStatusCode.OK, invoicedOrder);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid order", e);
            }
            catch (CreateExternalCustomerException e)
            {
                return GetValidationErrorFaultResponse("invalid customer data", new ValidationResult(e.Message));
            }
        }        
        
        /// <summary>
        /// If an order has been rejected in the distributor's invoice provider it can be reset in Phoenix for correction. This means that the record of the 
        /// invoiced order is deleted and the order is changed to a draft order.
        /// </summary>
        /// <param name="id">Order id</param>
        [HttpPut]   
        [Route("orders/{id}/reset")]
        [ResponseType(typeof(OrderInfo))]
        public HttpResponseMessage Reset([FromUri] int id)
        {
            try
            {
                var resetOrder = _ordersProvider.ResetOrder(id);
                return Request.CreateResponse(HttpStatusCode.OK, resetOrder);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw CreateFaultException(HttpStatusCode.Conflict, e.Message);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid order", e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// Credits an invoice
        /// </summary>
        /// <param name="id">Order id</param>
        [HttpPut]
        [Route("orders/{id}/credit")]
        [ResponseType(typeof(OrderInfo))]
        public HttpResponseMessage Credit([FromUri] int id)
        {
            try
            {
                var creditNoteOrder = _ordersProvider.CreditOrder(id);
                return Request.CreateResponse(HttpStatusCode.OK, creditNoteOrder);
            }
            catch (EntityNotFoundException e)
            {
                throw CreateFaultException(HttpStatusCode.NotFound, e.Message);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid credit note", e.ValidationResults.ToArray());
            }
            catch (CreateExternalCustomerException e)
            {
                return GetValidationErrorFaultResponse("invalid customer data", new ValidationResult(e.Message));
            }
        }

        /// <summary>
        /// Creates a credit note draft based on a transferred/invoiced order
        /// </summary>
        /// <param name="id">Order id</param>
        [HttpPut]
        [Route("orders/{id}/creditnote")]
        [ResponseType(typeof(OrderInfo))]
        public HttpResponseMessage CreditNote([FromUri] int id)
        {
            try
            {
                var creditNoteDraft = _ordersProvider.CreateCreditOrderDraft(id);
                return Request.CreateResponse(HttpStatusCode.OK, creditNoteDraft);
            }
            catch (EntityNotFoundException e)
            {
                throw CreateFaultException(HttpStatusCode.NotFound, e.Message);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid credit note", e.ValidationResults.ToArray());
            }
        }

        /// <summary>
        /// Retrieves a list of work orders (strings) for the customer. These work orders are collected from 
        /// the invoice system of the distributor which the customer is associated with.
        /// </summary>
        /// <param name="customerId">Customer ID in Phoenix</param>
        /// <returns>List of strings representing the work orders</returns>
        [HttpGet]
        [Route("orders/workorders")]
        public HttpResponseMessage GetWorkOrders([FromUri] int customerId)
        {
            try
            {
                var workOrders = _ordersProvider.GetWorkOrders(customerId);
                return Request.CreateResponse(HttpStatusCode.OK, workOrders);
            }
            catch (EntityNotFoundException e)
            {
                throw CreateFaultException(HttpStatusCode.NotFound, e.Message);
            }
            catch (ValidationException e)
            {
                return GetValidationErrorFaultResponse("Invalid work order request", e.ValidationResults.ToArray());
            }
        }
    }
}