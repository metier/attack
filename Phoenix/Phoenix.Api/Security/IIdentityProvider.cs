﻿using System.Security.Principal;

namespace Metier.Phoenix.Api.Security
{
    public interface IIdentityProvider
    {
        IPrincipal GetCurrentUser();
    }
}