﻿using System.Security.Principal;
using System.Web;

namespace Metier.Phoenix.Api.Security
{
    public class IdentityProvider : IIdentityProvider
    {
        public IPrincipal GetCurrentUser()
        {
            return HttpContext.Current.User;
        }
    }
}