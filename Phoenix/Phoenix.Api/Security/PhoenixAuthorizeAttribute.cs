﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Security;
using Castle.Windsor;
using Metier.Phoenix.Api.Data.Repositories;

namespace Metier.Phoenix.Api.Security
{
    public class PhoenixAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly IWindsorContainer _container;

        public PhoenixAuthorizeAttribute(IWindsorContainer container)
        {
            _container = container;
        }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var isAuthorized = base.IsAuthorized(actionContext);
            if (!isAuthorized)
            {
                var authenticationToken = GetAuthenticationToken(actionContext);
                if (authenticationToken != null)
                {
                    isAuthorized = AuthenticateFromToken(actionContext, authenticationToken);
                }

                if (!isAuthorized)
                {
                    return false;
                }
            }

            if (actionContext.RequestContext.Principal.IsInRole(PhoenixRoles.PhoenixUser))
            {
                return true;
            }

            var authorizedRoles = GetAllowedRoles(actionContext);
            isAuthorized = authorizedRoles.Any(r => actionContext.RequestContext.Principal.IsInRole(r));
            return isAuthorized;
        }


        private string GetAuthenticationToken(HttpActionContext actionContext)
        {
            IEnumerable<string> header;
            actionContext.Request.Headers.TryGetValues("PhoenixAuthToken", out header);
            if (header == null)
            {
                return null;
            }
            return header.FirstOrDefault();
        }

        private bool AuthenticateFromToken(HttpActionContext actionContext, string token)
        {
            var authenticationTokenRepository = _container.Resolve<IAuthenticationTokenRepository>();
            var authentication = authenticationTokenRepository.FindByToken(token);
            if (authentication != null)
            {
                actionContext.RequestContext.Principal = new RolePrincipal(new FormsIdentity(new FormsAuthenticationTicket(authentication.UserName, false, 60)));
                return true;
            }
            return false;
        }

        private static IEnumerable<string> GetAllowedRoles(HttpActionContext actionContext)
        {
            var actionAllowRolesAttributes = actionContext.ActionDescriptor.GetCustomAttributes<AllowRolesAttribute>();
            var controllerAllowRolesAttributes = actionContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes<AllowRolesAttribute>();
            return actionAllowRolesAttributes.Concat(controllerAllowRolesAttributes).SelectMany(r => r.AuthorizeRoles).Distinct();
        }
    }
}