﻿namespace Metier.Phoenix.Api.Security
{
    public interface IPhoenixMembershipRoles
    {
        string[] GetRolesForUser(string username);
    }
}