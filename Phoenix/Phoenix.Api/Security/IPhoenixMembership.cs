﻿using System.Web.Security;
using Metier.Phoenix.Core.Data;

namespace Metier.Phoenix.Api.Security
{
    public interface IPhoenixMembership
    {
        IPhoenixMemberShipProvider GetProvider(MetierLmsContext context);

        bool ChangeUsername(string oldUsername, string newUsername);
        MembershipUser CreateUser(string username, string password, string email);
        string GeneratePassword(int length, int numberOfAlphanumericCharacters);
        MembershipUser GetUser(object providerUserKey, bool userIsOnline);
        MembershipUser GetUser(string username, bool userIsOnline);
        void UpdateUser(MembershipUser membershipUser);
        string GetUserNameByEmail(string emailToMatch);
    }
}