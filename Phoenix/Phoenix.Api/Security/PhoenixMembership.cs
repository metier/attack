﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using Metier.Phoenix.Api.Configuration;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Core.Data;

namespace Metier.Phoenix.Api.Security
{
    public class PhoenixMembership : IPhoenixMembership
    {
        private readonly IConfiguration _configuration;

        public PhoenixMembership(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IPhoenixMemberShipProvider GetProvider(MetierLmsContext context)
        {
            var provider = Membership.Provider as PhoenixSqlMembershipProvider;
            provider.Context = context;

            return provider;
        }

        public bool ChangeUsername(string oldUsername, string newUsername)
        {
            if (string.IsNullOrWhiteSpace(oldUsername))
                throw new ArgumentNullException("oldUsername");

            if (string.IsNullOrWhiteSpace(newUsername))
                throw new ArgumentNullException("newUsername");

            if (oldUsername == newUsername)
                return true;

            using (var connection = new SqlConnection(_configuration.ConnectionString))
            {
                connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "UPDATE aspnet_Users SET UserName=@NewUsername,LoweredUserName=@LoweredNewUsername WHERE UserName=@OldUsername";

                    var parameter = new SqlParameter("@OldUsername", SqlDbType.VarChar)
                    {
                        Value = oldUsername
                    };
                    command.Parameters.Add(parameter);

                    parameter = new SqlParameter("@NewUsername", SqlDbType.VarChar)
                    {
                        Value = newUsername
                    };
                    command.Parameters.Add(parameter);

                    parameter = new SqlParameter("@LoweredNewUsername", SqlDbType.VarChar)
                    {
                        Value = newUsername.ToLower()
                    };
                    command.Parameters.Add(parameter);

                    bool result = false;
                    try
                    {
                        result = (command.ExecuteNonQuery() > 0);
                    }
                    catch (SqlException e)
                    {
                        if (e.Number == 2601)
                        {
                            throw new UsernameChangeException(string.Format("{0} is already in use.", newUsername));
                        }
                        throw;
                    }
                    return result;
                }
            }
        }

        public MembershipUser CreateUser(string username, string password, string email)
        {
            return Membership.CreateUser(username, password, email);
        }

        public string GeneratePassword(int length, int numberOfAlphanumericCharacters)
        {
            return Membership.GeneratePassword(length, numberOfAlphanumericCharacters);
        }

        public MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            return Membership.GetUser(providerUserKey, userIsOnline);
        }

        public MembershipUser GetUser(string username, bool userIsOnline)
        {
            return Membership.GetUser(username, userIsOnline);
        }

        public string GetUserNameByEmail(string emailToMatch)
        {
            return  Membership.GetUserNameByEmail(emailToMatch);
        }

        public void UpdateUser(MembershipUser membershipUser)
        {
            Membership.UpdateUser(membershipUser);
        }
    }
}