﻿namespace Metier.Phoenix.Api.Security
{
    public class ImportUserPasswordChangeData
    {
        public string KnownPassword { get; set; }
        public string OriginalPasswordHash { get; set; }
        public string OriginalPasswordSalt { get; set; }
    }
}