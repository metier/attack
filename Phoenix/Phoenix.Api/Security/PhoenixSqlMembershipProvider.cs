﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;
using Metier.Phoenix.Api.Configuration;
using Metier.Phoenix.Api.Data;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.ViewEntities;

namespace Metier.Phoenix.Api.Security
{
    public class PhoenixSqlMembershipProvider : SqlMembershipProvider, IPhoenixMemberShipProvider
    {
        public MetierLmsContext Context { get; set; }

        public override bool ValidateUser(string username, string password)
        {
            var importedUser = GetImportedUser(username);
            if (importedUser != null)
            {
                var validationSuccessful = ValidateImportedUser(importedUser, password);
                if(validationSuccessful)
                {
                    UpdateLastLoginDate(importedUser);
                }
                return validationSuccessful;
            }
            
            return base.ValidateUser(username, password);
        }

        public override string ResetPassword(string username, string passwordAnswer)
        {
            string newPassword;
            var importedUser = GetImportedUser(username);

            base.UnlockUser(username);
            if (importedUser != null)
            {
                try
                {
                    // Temporarly change the user's password to a known password that uses the same crypto functions as Membership.
                    // This little hack allows for a regular Membership "Change password"
                    ChangePasswordToKnownPassword(importedUser);
                    newPassword = base.ResetPassword(username, null);
                    MarkUserAsNotImported(importedUser);
                }
                catch
                {
                    RestoreUserCredentials(importedUser);
                    throw;
                }
            }
            else
            {
                newPassword = base.ResetPassword(username, passwordAnswer);
            }

            return newPassword;
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            var passwordChanged = false;
            var importedUser = GetImportedUser(username);
            if (importedUser != null)
            {
                try
                {
                    if (ValidateImportedUser(importedUser, oldPassword))
                    {
                        // Temporarly change the user's password to a known password that uses the same crypto functions as Membership.
                        // This little hack allows for a regular Membership "Change password"
                        var tempPassword = ChangePasswordToKnownPassword(importedUser);
                        passwordChanged = base.ChangePassword(username, tempPassword, newPassword);
                    }

                    if (passwordChanged)
                    {
                        MarkUserAsNotImported(importedUser);
                    }
                    else
                    {
                        RestoreUserCredentials(importedUser);
                    }
                }
                catch
                {
                    RestoreUserCredentials(importedUser);
                    throw;
                }
                
            }
            else
            {
                passwordChanged = base.ChangePassword(username, oldPassword, newPassword);
            }

            return passwordChanged;
        }

        private void RestoreUserCredentials(ImportedMembershipUser importedUser)
        {
            var query = string.Format("UPDATE [dbo].[aspnet_Membership] SET Password = '{0}', PasswordSalt= '{1}' " +
                                      "WHERE UserId = (SELECT UserId FROM [dbo].[aspnet_Users] WHERE UserName = '{2}')", 
                                     importedUser.Password, importedUser.PasswordSalt, importedUser.UserName);
            ExecuteScalarNoReturn(query);
        }

        private void MarkUserAsNotImported(ImportedMembershipUser importedUser)
        {
            var query = string.Format("UPDATE [dbo].[aspnet_Membership] SET Comment = NULL " +
                                      "WHERE UserId = (SELECT UserId FROM [dbo].[aspnet_Users] WHERE UserName = '{0}')", importedUser.UserName);
            ExecuteScalarNoReturn(query);
        }

        private string ChangePasswordToKnownPassword(ImportedMembershipUser importedUser)
        {
            const string knownPassword = "p@ssword1";
            const string knownPasswordHash = "EuX2nhh6PshQF9yLnCu2OEwy4BhwfJHJ+6lhWLstjHk=";
            const string knownPasswordSalt = @"mALephywsLRUIVC+mYoVfA==";

            var query = string.Format("UPDATE [dbo].[aspnet_Membership] SET Password = '{0}', PasswordSalt = '{1}' " +
                                      "WHERE UserId = (SELECT UserId FROM [dbo].[aspnet_Users] WHERE UserName = '{2}')",
                                     knownPasswordHash, knownPasswordSalt, importedUser.UserName);
            ExecuteScalarNoReturn(query);
            return knownPassword;
        }

        private static bool ValidateImportedUser(ImportedMembershipUser importedUser, string password)
        {
            byte[] plaintext = ASCIIEncoding.Default.GetBytes(password + importedUser.PasswordSalt);
            var hashed = new MD5CryptoServiceProvider().ComputeHash(plaintext);
            return importedUser.Password == Convert.ToBase64String(hashed);
        }

        private ImportedMembershipUser GetImportedUser(string username)
        {
            if (Context == null)
            {
                throw new ApplicationException("Property Context must be specified before validating user");
            }

            return Context.ImportedUsers.FirstOrDefault(m => m.UserName == username);
        }

        private void UpdateLastLoginDate(ImportedMembershipUser importedUser)
        {
            var query = string.Format("UPDATE [dbo].[aspnet_Membership] SET LastLoginDate = SYSUTCDATETIME ( ) " +
                                      "WHERE UserId = (SELECT UserId FROM [dbo].[aspnet_Users] WHERE UserName = '{0}')",
                                     importedUser.UserName);
            ExecuteScalarNoReturn(query);
        }


        private void ExecuteScalarNoReturn(string query)
        {
            var configuration = new PhoenixConfiguration();
            using (var conn = new SqlConnection(configuration.ConnectionString))
            {
                var sqlCommand = new SqlCommand(query, conn);
                conn.Open();
                sqlCommand.ExecuteScalar();
            }
        }
    }
}