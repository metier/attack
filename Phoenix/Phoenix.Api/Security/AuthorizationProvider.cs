using System.Linq;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Security
{
    public class AuthorizationProvider : IAuthorizationProvider
    {
        private readonly MetierLmsContext _context;
        private readonly IIdentityProvider _identityProvider;

        public AuthorizationProvider(MetierLmsContext context, IIdentityProvider identityProvider)
        {
            _context = context;
            _identityProvider = identityProvider;
        }

        public bool IsAdminOr(int userId)
        {
            return IsAdmin() || Is(userId);
        }

        public bool IsAdminOr(string username)
        {
            return IsAdmin() || Is(username);
        }

        public bool IsAdmin()
        {
            return _identityProvider.GetCurrentUser().IsInRole(PhoenixRoles.PhoenixUser);
        }

        public bool Is(string username)
        {
            var currentUsername = _identityProvider.GetCurrentUser().Identity.Name;
            return currentUsername.ToLower() == username.ToLower();
        }

        public bool Is(int userId)
        {
            var currentUsername = _identityProvider.GetCurrentUser().Identity.Name;
            return currentUsername.ToLower() == GetUsername(userId).ToLower();
        }

        public bool HasCustomer(int customerId)
        {
            var user = GetCurrentUser();
            if (user == null)
            {
                return false;
            }
            
            return user.CustomerId == customerId;
        }

        public bool HasCustomer(int customerId, ICustomersProvider customersProvider)
        {
            var user = GetCurrentUser();
            if (user == null)
            {
                return false;
            }
            
            var customerIds = customersProvider.GetChildrenIds(customerId, true).ToArray();
            return customerIds.Contains(user.CustomerId);
        }


        public bool HasCustomerOwningActivity(int activityId)
        {
            var user = GetCurrentUser();
            if (user == null)
            {
                return false;
            }
            var activity = _context.Activities.FirstOrDefault(a => a.Id == activityId && !a.IsDeleted);
            if (activity == null)
            {
                return false;
            }
            var customerArticle = _context.CustomerArticles.FirstOrDefault(c => c.Id == activity.CustomerArticleId && !c.IsDeleted);
            if (customerArticle == null)
            {
                return false;
            }
            return customerArticle.CustomerId == user.CustomerId;
        }

        public User GetCurrentUser()
        {
            var currentUsername = _identityProvider.GetCurrentUser().Identity.Name;
            return _context.Users.FirstOrDefault(u => u.MembershipUserId.ToLower() == currentUsername.ToLower());
        }

        public bool HasRole(string role)
        {
            var user = _identityProvider.GetCurrentUser();
            return user.IsInRole(role);
        }
        public bool HasAnyRole(params string[] roles)
        {
            if (roles == null || roles.Length == 0) return false;
            var user = _identityProvider.GetCurrentUser();

            for(var x=0;x< roles.Length;x++)
                if(user.IsInRole(roles[x]))
                    return true;

            return false;
        }

        private string GetUsername(int userId)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == userId);
            return user == null ? string.Empty : user.MembershipUserId;
        }
    }
}