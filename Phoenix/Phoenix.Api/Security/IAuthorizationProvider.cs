﻿using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Security
{
    /// <summary>
    /// Authorization operations on the authenticated user
    /// </summary>
    public interface IAuthorizationProvider
    {
        /// <summary>
        /// Checks if the authenticated user is an admin user or has the given <see cref="userId"/>
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        bool IsAdminOr(int userId);

        /// <summary>
        /// Checks if the authenticated user is an admin user or has the given <see cref="username"/>
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        bool IsAdminOr(string username);

        /// <summary>
        /// Checks if the authenticated user is and admin user
        /// </summary>
        /// <returns></returns>
        bool IsAdmin();

        /// <summary>
        /// Checks if the authenticated user has the given <see cref="username"/>
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        bool Is(string username);

        /// <summary>
        /// Checks if the authenticated user has the given <see cref="userId"/>
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        bool Is(int userId);

        /// <summary>
        /// Checks if the authenticated user is connected to the given <see cref="customerId"/>
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="customersProvider"></param>
        /// <returns></returns>
        bool HasCustomer(int customerId, ICustomersProvider customersProvider);

        /// <summary>
        /// Checks if the authenticated user is connected to the customer that owns the given <see cref="activityId"/>
        /// </summary>
        /// <param name="activityId"></param>
        /// <returns></returns>
        bool HasCustomerOwningActivity(int activityId);

        User GetCurrentUser();

        /// <summary>
        /// Checks if the user has the required role
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        bool HasRole(string role);

        /// <summary>
        /// Checks if the user has any of the required roles
        /// </summary>
        /// <param name="roles"></param>
        /// <returns></returns>
        bool HasAnyRole(params string[] roles);
        
    }
}