namespace Metier.Phoenix.Api.Security
{
    public interface IPhoenixMemberShipProvider
    {
        bool ValidateUser(string username, string password);
        string ResetPassword(string username, string passwordAnswer);
        bool ChangePassword(string username, string oldPassword, string newPassword);
    }
}