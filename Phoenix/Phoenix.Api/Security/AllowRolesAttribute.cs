﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Metier.Phoenix.Api.Security
{
    public class AllowRolesAttribute : Attribute
    {
        private readonly string[] _authorizeRoles;

        public AllowRolesAttribute(params string[] roles)
        {
            _authorizeRoles = GetRoles(roles).ToArray();
        }

        private IEnumerable<string> GetRoles(IEnumerable<string> roles)
        {
            return roles.SelectMany(role => role.Split(','));
        }

        public string[] AuthorizeRoles
        {
            get { return _authorizeRoles; }
        }
    }
}