﻿using System.Web.Security;

namespace Metier.Phoenix.Api.Security
{
    public class PhoenixMembershipRoles : IPhoenixMembershipRoles
    {
        public string[] GetRolesForUser(string username)
        {
            return Roles.GetRolesForUser(username);
        }
    }
}