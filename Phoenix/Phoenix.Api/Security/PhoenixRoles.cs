﻿namespace Metier.Phoenix.Api.Security
{
    public static class PhoenixRoles
    {
        // Phoenix roles
        public const string PhoenixUser = "PhoenixUser";

        // Learning portal roles
        public const string LearningPortalUser = "LearningPortalUser";
        public const string LearningPortalSuperUser = "LearningPortalSuperUser";

        // Behind roles
        public const string BehindAdmin = "BehindAdmin";
        public const string BehindContentAdmin = "BehindContentAdmin";
        public const string BehindSuperAdmin = "BehindSuperAdmin";

        // Examiner roles
        public const string Examiner = "Examiner";
        
        // Token roles
        public const string LearningPortalTokenUser = "LearningPortalTokenUser";
        public const string EzTokenUser = "EzTokenUser";

        // Aggregated roles
        public const string Phoenix = PhoenixUser;
        public const string Behind = BehindAdmin + "," + BehindContentAdmin + "," + BehindSuperAdmin;
        public const string LearningPortal = LearningPortalUser + "," + LearningPortalSuperUser;
        public const string All = Phoenix + "," + LearningPortal + "," + Behind + "," + Examiner + "," + LearningPortalTokenUser + "," + EzTokenUser;
    }
}