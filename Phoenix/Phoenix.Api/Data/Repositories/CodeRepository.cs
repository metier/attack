﻿using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Facade;
using System.Data.Entity;
using Metier.Phoenix.Core.Data;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class CodeRepository : ICodeRepository
    {
        private readonly MetierLmsContext _context;

        public CodeRepository(MetierLmsContext context)
        {
            _context = context;
        }

        public IEnumerable<Code> GetCodes(string type)
        {
            return GetCodesFromCodesTable(type);
        }

        public string GetCodeValue(int codeId)
        {
            return _context.Codes.Where(c => c.Id == codeId).Select(c => c.Value).FirstOrDefault();
        }

        public bool IsValidCodeValue(string value, string type)
        {
            return _context.CodeTypes
                           .Where(t => t.Name == type)
                           .Join(_context.Codes, t => t.Id, c => c.CodeTypeId, (t, c) => c)
                           .Any(c => c.Value == value);
        }

        private IEnumerable<Code> GetCodesFromCodesTable(string name)
        {
            var codeType = _context.CodeTypes.Include(ct => ct.Codes).Single(c => c.Name.ToUpper() == name.ToUpper());
            return codeType.Codes.Select(c => new Code(c.Id, c.Value, c.Description));
        }
    }
}