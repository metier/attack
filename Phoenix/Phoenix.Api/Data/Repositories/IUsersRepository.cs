﻿using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface IUsersRepository
    {
        User Create(User user);
        User FindByUsername(string username);
        User Update(User user);
        void MergeUsers(int userId, int userIdToMerge);
        void MergeUsers(User user, User userToMerge);
        PartialList<SearchableUser> Search(int? distributorId, int? customerId, int? userId, int? activityId, int? activitySetId, bool phoenixUsersOnly, bool instructorUsersOnly, bool ecoachUsersOnly, bool includeParticipations, string query, bool alsoSearchCustomFields, string advancedQuery, bool? isActive, string orderBy, string orderDirection, int? skip, int? limit);
    }
}