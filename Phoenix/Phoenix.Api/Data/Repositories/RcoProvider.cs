﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;
using Metier.Phoenix.Core.Utilities.Sorting;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class RcoProvider : IRcoProvider
    {
        private readonly IRcoRepository _rcoRepository;
        private readonly MetierLmsContext _context;
        private readonly ISortNumberedStrings _sortNumberedStrings;

        public RcoProvider(IRcoRepository rcoRepository, MetierLmsContext context, ISortNumberedStrings sortNumberedStrings)
        {
            _rcoRepository = rcoRepository;
            _context = context;
            _sortNumberedStrings = sortNumberedStrings;
        }

        public PartialList<SearchableRco> GetCourses(string query, string orderBy, string orderDirection, int? skip, int? limit)
        {
            return _rcoRepository.GetRcos(query, null, orderBy, orderDirection, new [] {"course"}, skip, limit);
        }

        public PartialList<SearchableRco> GetExamContainers(string query, string orderBy, string orderDirection, int? skip, int? limit)
        {
            return _rcoRepository.GetRcos(query, null, orderBy, orderDirection, new[] { "examcontainer" }, skip, limit);
        }

        public PartialList<SearchableRco> SearchRcos(string query, int[] rcoIds, string orderBy, string orderDirection, int? skip, int? limit)
        {
            return _rcoRepository.GetRcos(query, rcoIds, orderBy, orderDirection, null, skip, limit);
        }

        public RcoCourse GetCourse(int courseRcoId)
        {
            var rco = _context.Rcos.FirstOrDefault(r => r.Id == courseRcoId && r.Context == "course");
            if (rco == null)
            {
                throw new EntityNotFoundException("Course does not exist");
            }
            var course = CreateCourseFacade(rco);
            if (rco.ChildCount > 0)
            {
                PopulateChildren(course);
            }
            return course;
        }

        public RcoExamContainer GetExamContainer(int id)
        {
            var rco = _context.Rcos.FirstOrDefault(r => r.Id == id && r.Context == "examcontainer");
            if (rco == null)
            {
                throw new EntityNotFoundException("Exam container does not exist");
            }
            var exam = CreateExamContainerFacade(rco);
            if (rco.ChildCount > 0)
            {
                PopulateChildren(exam);
            }
            return exam;
        }
        
        private RcoExamContainer CreateExamContainerFacade(Rco rco)
        {
            var exam =  new RcoExamContainer
            {
                Id = rco.Id,
                Name = rco.Name,
                Created = rco.Created
            };
            return exam;
        }

        private RcoExam CreateExamFacade(Rco rco)
        {
            var exam = new RcoExam
            {
                Id = rco.Id,
                Name = rco.Name,
                Created = rco.Created
            };
            return exam;
        }

        public IEnumerable<Rco> GetCourseRcos(int rcoId, bool includeCourse = false)
        {
            return GetRcoWithChildren(rcoId, "lesson", includeCourse);
        }

        public IEnumerable<Rco> GetExamContainerRcos(int rcoId, bool includeContainer = false)
        {
            return GetRcoWithChildren(rcoId, "exam", includeContainer);
        }

        private IEnumerable<Rco> GetRcoWithChildren(int rcoId, string context, bool includeParent = false)
        {
            var parent = GetTopParent(rcoId);
            var children = new List<Rco>();
            if (parent.ChildCount > 0)
            {
                children = GetAllChildren(parent.Id, context).ToList();
            }
            if (includeParent || parent.Context == context)
            {
                return new[] { parent }.Concat(children);
            }
            return children;
        }

        private RcoCourse CreateCourseFacade(Rco rco)
        {
            var course = new RcoCourse
            {
                Id = rco.Id,
                Name = rco.Name,
                Version = rco.Version,
                Created = rco.Created,
                SilverPoints = rco.SilverPoints,
                GoldPoints = rco.GoldPoints,
                UnlockFinalPoints = rco.UnlockTestPoints
            };
            if (!string.IsNullOrEmpty(rco.Language))
            {
                course.LanguageIdentifier = rco.Language.ToUpper();
                course.Language = GetLanguage(rco.Language.ToUpper());
            }
            return course;
        }

        private IEnumerable<Rco> GetChildren(int id)
        {
            return _context.Rcos.Where(r => r.ParentId == id).ToList();
        }

        private void PopulateChildren(RcoCourse course)
        {
            var children = GetChildren(course.Id);
            foreach (var child in children)
            {
                var lesson = CreateLessonFacade(child);
                if (child.ChildCount > 0)
                {
                    PopulateChildren(child, lesson);    
                }
                course.Lessons.Add(lesson);
            }
            course.Lessons = course.Lessons.OrderBy(l => _sortNumberedStrings.PadInitialNumbersForSorting(l.Name, l.IsFinalTest)).ToList();
        }

        private void PopulateChildren(RcoExamContainer examContainer)
        {
            var children = GetChildren(examContainer.Id);
            foreach (var child in children)
            {
                var exam = CreateExamFacade(child);
                examContainer.Children.Add(exam);
            }
        }

        private IEnumerable<Rco> GetAllChildren(int rcoId, string context)
        {
            var result = new List<Rco>();

            var children = _context.Rcos.Where(r => r.ParentId == rcoId && r.Context == context).ToList();
			children = children.OrderBy(l => _sortNumberedStrings.PadInitialNumbersForSorting(l.Name, l.IsFinalTest == true)).ToList();

			foreach (var child in children)
            {
                result.Add(child);
                if (child.ChildCount > 0)
                {
                    result.AddRange(GetAllChildren(child.Id, context));
                }
            }
            return result;
        }

        public Rco GetTopParent(int rcoId)
        {
            var current = _context.Rcos.FirstOrDefault(r => r.Id == rcoId);
            if (current == null)
            {
                return null;
            }
            if (current.ParentId == null)
            {
                return current;
            }
            var parent = _context.Rcos.FirstOrDefault(r => r.Id == current.ParentId);
            if (parent == null)
            {
                return current;
            }
            return GetTopParent(parent.Id);
        }

        private void PopulateChildren(Rco lessonRco, RcoLesson lesson)
        {
            if (lessonRco.ChildCount == 0) return;
            
            var children = GetChildren(lesson.Id);
            foreach (var child in children)
            {
                var childLesson = CreateLessonFacade(child);
                PopulateChildren(child, childLesson);
                lesson.Lessons.Add(childLesson);
            }
        }

        private RcoLesson CreateLessonFacade(Rco rco)
        {
            return new RcoLesson
            {
                Id = rco.Id,
                Name =  rco.Name,
                Weight =  rco.Weight,
                IsFinalTest = rco.IsFinalTest.GetValueOrDefault(),
                Created = rco.Created
            };
        }

        private string GetLanguage(string languageIdentifier)
        {
            return _context.Languages.Where(lan => lan.Identifier == languageIdentifier)
                                     .Select(lan => lan.Name)
                                     .FirstOrDefault();
        }
    }
}