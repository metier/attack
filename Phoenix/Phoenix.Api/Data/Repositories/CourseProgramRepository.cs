﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Exceptions;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class CourseProgramRepository : RepositoryBase, ICourseProgramRepository
    {
        public CourseProgramRepository(MetierLmsContext context) : base(context) {}

        public IEnumerable<CourseProgram> Find(int customerId)
        {
            var coursePrograms = Context.CoursePrograms.WithAll().Where(cp => !cp.IsDeleted && cp.CustomerId == customerId).ToList();
            foreach (var courseProgram in coursePrograms)
            {
                OrderLists(courseProgram);
            }
            return coursePrograms;
        }

        public CourseProgram Get(int id)
        {
            var courseProgram = Context.CoursePrograms.WithAll().SingleOrDefault(cp => !cp.IsDeleted && cp.Id == id);
            if (courseProgram != null)
            {
                OrderLists(courseProgram);    
            }
            return courseProgram;
        }

        public IEnumerable<Product> GetProductsInCourseProgram(int courseProgramId)
        {
            var products = new List<Product>();
            var courseProgram = Get(courseProgramId);

            if (courseProgram != null)
            {
                var moduleIds = (from step in courseProgram.Steps from module in step.Modules select module.Id).ToList();
                var examModuleIds = (from step in courseProgram.Steps from examModule in step.Exams select examModule.Id).ToList();

                products = (from p in Context.Products
                    join a in Context.Articles on p.Id equals a.ProductId
                    join ca in Context.CustomerArticles on a.Id equals ca.ArticleCopyId
                    where (ca.ModuleId.HasValue && moduleIds.Contains(ca.ModuleId.Value))
                    || (ca.ExamId.HasValue && examModuleIds.Contains(ca.ExamId.Value))
                    select p).Distinct().ToList();
            }

            return products;
        }

        private static void OrderLists(CourseProgram courseProgram)
        {
            courseProgram.Steps = courseProgram.Steps.OrderBy(s => s.Order).ToList();
            foreach (var step in courseProgram.Steps)
            {
                step.Modules = step.Modules.OrderBy(m => m.Order).ToList();
                step.Exams = step.Exams.OrderBy(e => e.Order).ToList();
            }
        }

        public CourseProgram Create(CourseProgram courseProgram)
        {
            Context.CoursePrograms.Add(courseProgram);
            Context.SaveChanges();
            return courseProgram;
        }

        public CourseProgram Update(int id, CourseProgram courseProgram)
        {
            var dbCourseProgram = Get(id);
            if (!courseProgram.RowVersion.SequenceEqual(dbCourseProgram.RowVersion))
            {
                throw new DbUpdateConcurrencyException("There is a newer record of course program in the database.");
            }
            if (courseProgram.IsDeleted && CourseProgramHasContent(dbCourseProgram))
            {
                throw new ValidationException("Can not delete course program containing customer article(s)");
            }
            if (HasRemovedStepsContainingExamsOrModulesWithCustomerArticles(courseProgram, dbCourseProgram))
            {
                throw new ValidationException("Can not delete step containing exams or modules");
            }
            if (HasRemovedExamsContainingCustomerArticles(courseProgram, dbCourseProgram))
            {
                throw new ValidationException("Can not delete exams containing customer article(s)");
            }
            if (HasRemovedCourseModulesContainingCustomerArticles(courseProgram, dbCourseProgram))
            {
                throw new ValidationException("Can not delete course modules containing customer article(s)");
            }

            Context.Entry(dbCourseProgram).CurrentValues.SetValues(courseProgram);

            UpdateSteps(courseProgram, dbCourseProgram);
            Context.SaveChanges();
            OrderLists(dbCourseProgram);
            return dbCourseProgram;
        }

        private bool HasRemovedCourseModulesContainingCustomerArticles(CourseProgram courseProgram, CourseProgram dbCourseProgram)
        {
            foreach (var courseStep in courseProgram.Steps)
            {
                var correspondingDbStep = dbCourseProgram.Steps.FirstOrDefault(s => s.Id == courseStep.Id);
                if (correspondingDbStep != null)
                {
                    var removedModules = correspondingDbStep.Modules.Where(cm => !courseStep.Modules.Exists(cm2 => cm2.Id == cm.Id)).ToList();
                    foreach (var removedModule in removedModules)
                    {
                        // Course module containing customer articles && not in any other posted steps
                        if (Context.CustomerArticles.Any(ca => ca.ModuleId == removedModule.Id && !ca.IsDeleted) &&
                            !courseProgram.Steps.Any(s => s.Modules.Any(cm => cm.Id == removedModule.Id)))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool HasRemovedExamsContainingCustomerArticles(CourseProgram courseProgram, CourseProgram dbCourseProgram)
        {
            foreach (var courseStep in courseProgram.Steps)
            {
                var correspondingDbStep = dbCourseProgram.Steps.FirstOrDefault(s => s.Id == courseStep.Id);
                if (correspondingDbStep != null)
                {
                    var removedExams = correspondingDbStep.Exams.Where(ce => !courseStep.Exams.Exists(ce2 => ce2.Id == ce.Id)).ToList();
                    foreach (var removedExam in removedExams)
                    {
                        // Exam module containing customer articles && not in any other posted steps
                        if (Context.CustomerArticles.Any(ca => ca.ExamId == removedExam.Id && !ca.IsDeleted) && 
                            !courseProgram.Steps.Any(s => s.Exams.Any(ce => ce.Id == removedExam.Id)))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool HasRemovedStepsContainingExamsOrModulesWithCustomerArticles(CourseProgram courseProgram, CourseProgram dbCourseProgram)
        {
            var removedSteps = dbCourseProgram.Steps.Where(s => !courseProgram.Steps.Exists(s2 => s2.Id == s.Id)).ToList();
            return removedSteps.Any(
                removedStep => removedStep.Exams.Any(ce => Context.CustomerArticles.Any(ca => ca.ExamId == ce.Id && !ca.IsDeleted)) ||
                    removedStep.Modules.Any(cm => Context.CustomerArticles.Any(ca => ca.ModuleId == cm.Id && !ca.IsDeleted)));
        }

        private bool CourseProgramHasContent(CourseProgram dbCourseProgram)
        {
            return dbCourseProgram.Steps.Any(
                s => s.Exams.Any(e => Context.CustomerArticles.Count(c => c.ExamId == e.Id && !c.IsDeleted) > 0) ||
                     s.Modules.Any(m => Context.CustomerArticles.Count(c => c.ModuleId == m.Id && !c.IsDeleted) > 0));
        }

        private void UpdateSteps(CourseProgram postedCourseProgram, CourseProgram dbCourseProgram)
        {
            var postedSteps = postedCourseProgram.Steps.ToList();
            var dbSteps = dbCourseProgram.Steps.ToList();

            var addedSteps = postedSteps.Where(a => !dbSteps.Select(d => d.Id).Contains(a.Id));
            var deletedSteps = dbSteps.Where(a => !postedSteps.Select(n => n.Id).Contains(a.Id));

            foreach (var step in addedSteps)
            {
                var movedModules = step.Modules.Where(m => m.Id != 0).ToList();
                step.Modules.RemoveAll(m => m.Id != 0);
                foreach (var movedModule in movedModules)
                {
                    movedModule.CourseStepId = 0;
                    movedModule.CourseStep = step;
                    UpdateValuesOnMovedCourseModule(movedModule);
                }

                var movedExams = step.Exams.Where(e => e.Id != 0).ToList();
                step.Exams.RemoveAll(e => e.Id != 0);
                foreach (var movedExam in movedExams)
                {
                    movedExam.CourseStepId = 0;
                    movedExam.CourseStep = step;
                    UpdateValuesOnMovedExam(movedExam);
                }

                dbCourseProgram.Steps.Add(step);
            }
            foreach (var step in deletedSteps)
            {
                var exams = step.Exams.ToList();
                foreach (var courseExam in exams)
                {
                    Context.Entry(courseExam).State = EntityState.Deleted;
                }
                var modules = step.Modules.ToList();
                foreach (var courseModule in modules)
                {
                    Context.Entry(courseModule).State = EntityState.Deleted;
                }
                Context.Entry(step).State = EntityState.Deleted;
                dbCourseProgram.Steps.Remove(step);
            }

            foreach (var dbStep in dbCourseProgram.Steps)
            {
                var postedStep = postedCourseProgram.Steps.SingleOrDefault(s => s.Id != 0 && s.Id == dbStep.Id);
                if (postedStep != null)
                {
                    Context.Entry(dbStep).CurrentValues.SetValues(postedStep);

                    UpdateExams(postedStep, dbStep, postedCourseProgram.Steps);
                    UpdateModules(postedStep, dbStep, postedCourseProgram.Steps);
                }
            }
        }

        private void UpdateExams(CourseStep postedStep, CourseStep dbStep, List<CourseStep> postedSteps)
        {
            var postedExams = postedStep.Exams.ToList();
            var dbExams = dbStep.Exams.ToList();

            var addedExams = postedExams.Where(p => !dbExams.Select(d => d.Id).Contains(p.Id));
            var deletedExams = dbExams.Where(d => !postedExams.Select(p => p.Id).Contains(d.Id));

            foreach (var exam in addedExams)
            {
                if (exam.Id != 0)
                {
                    UpdateValuesOnMovedExam(exam);
                }
                else
                {
                    dbStep.Exams.Add(exam);
                }
            }
            foreach (var exam in deletedExams)
            {
                if (!postedSteps.Any(s => s.Exams.Any(e => e.Id == exam.Id)))
                {
                    Context.Entry(exam).State = EntityState.Deleted;
                }
                dbStep.Exams.Remove(exam);
            }
            
            foreach (var dbExam in dbExams)
            {
                var postedExam = postedExams.SingleOrDefault(e => e.Id != 0 && e.Id == dbExam.Id);
                if (postedExam != null)
                {
                    Context.Entry(dbExam).CurrentValues.SetValues(postedExam);
                }
            }
        }

        private void UpdateValuesOnMovedExam(CourseExam exam)
        {
            var dbExam = Context.Set<CourseExam>().First(e => e.Id == exam.Id);
            Context.Entry(dbExam).CurrentValues.SetValues(exam);
        }

        private void UpdateValuesOnMovedCourseModule(CourseModule module)
        {
            var dbModule = Context.Set<CourseModule>().First(m => m.Id == module.Id);
            Context.Entry(dbModule).CurrentValues.SetValues(module);
        }

        private void UpdateModules(CourseStep postedStep, CourseStep dbStep, List<CourseStep> postedSteps)
        {
            var postedModules = postedStep.Modules.ToList();
            var dbModules = dbStep.Modules.ToList();

            var addedModules = postedModules.Where(p => !dbModules.Select(d => d.Id).Contains(p.Id));
            var deletedModules = dbModules.Where(d => !postedModules.Select(p => p.Id).Contains(d.Id));

            foreach (var module in addedModules)
            {
                if (module.Id != 0)
                {
                    UpdateValuesOnMovedCourseModule(module);
                }
                else
                {
                    dbStep.Modules.Add(module);
                }
            }
            foreach (var module in deletedModules)
            {
                if (!postedSteps.Any(s => s.Modules.Any(e => e.Id == module.Id)))
                {
                    Context.Entry(module).State = EntityState.Deleted;
                }
                dbStep.Modules.Remove(module);
            }

            foreach (var dbModule in dbModules)
            {
                var postedModule = postedModules.SingleOrDefault(m => m.Id != 0 && m.Id == dbModule.Id);
                if (postedModule != null)
                {
                    Context.Entry(dbModule).CurrentValues.SetValues(postedModule);
                }
            }
        }
    }
}