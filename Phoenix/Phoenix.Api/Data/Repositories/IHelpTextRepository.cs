﻿using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface IHelpTextRepository
    {
        HelpText Get(string section, string name);
        HelpText Create(HelpText helpText);
        HelpText Update(int id, HelpText helpText);
    }
}