﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface ILeadingTextRepository
    {
        List<LeadingText> GetLeadingTexts();
    }
}