﻿using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface IRcoRepository
    {
        PartialList<SearchableRco> GetRcos(string query, int[] rcoIds, string orderBy, string orderDirection, string[] contexts, int? skip, int? limit);
    }
}