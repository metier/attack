﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Facade;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface ICodeRepository
    {
        IEnumerable<Code> GetCodes(string type);
        string GetCodeValue(int codeId);
        bool IsValidCodeValue(string value, string type);
    }
}