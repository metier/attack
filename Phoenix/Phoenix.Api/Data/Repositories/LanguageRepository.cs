﻿using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class LanguageRepository : RepositoryBase, ILanguageRepository
    {
        public LanguageRepository(MetierLmsContext context) : base(context)
        {
        }

        public IEnumerable<Language> GetAll()
        {
            return Context.Languages.ToList();
        }
    }
}