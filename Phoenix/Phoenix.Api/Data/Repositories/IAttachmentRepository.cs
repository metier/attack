﻿using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface IAttachmentRepository
    {
        Attachment Create(Attachment attachment);
    }
}