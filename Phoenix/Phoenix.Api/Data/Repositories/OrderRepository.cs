using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.Data.Entity.Validation;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using Castle.MicroKernel.ModelBuilder.Descriptors;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.Extensions;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Core.Utilities.Collections;
using Newtonsoft.Json;
using NLog;
using ValidationException = Metier.Phoenix.Core.Exceptions.ValidationException;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class OrderRepository : RepositoryBase, IOrderRepository
    {
        private readonly IValidateOrder _orderValidator;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public OrderRepository(MetierLmsContext context, IValidateOrder orderValidator) : base(context)
        {
            _orderValidator = orderValidator;
        }
        
        public Order FindOrder(int orderId)
        {
            var order = Context.Orders.WithAll().FirstOrDefault(o => o.Id == orderId);

            if(order == null)
                return null;

            foreach (var participantLine in order.ParticipantOrderLines)
            {
                LoadParticipantRelations(participantLine.Participant);
            }

            return order;
        }

        /// <summary>
        /// Finds/constructs a proposed order based on the key to group by.
        /// </summary>
        /// <param name="groupByKey">The key to group/filter orderlines by.</param>
        /// <returns>A proposed order</returns>
        public Order FindProposedOrder(string groupByKey)
        {
            GroupByKey groupByKeyInstance;
            try
            {
                groupByKeyInstance = JsonConvert.DeserializeObject<GroupByKey>(groupByKey);
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("Could not deserialize groupbykey {0}", groupByKey), e);
                throw new ValidationException("Could not resolve groupByKey");
            }
            
            var proposedOrderLines = Context.ProposedOrderLines.Where(pol => pol.DistributorId == groupByKeyInstance.DistributorId &&
                                                                             pol.ActivitySetId.HasValue && pol.ActivitySetId.Value == groupByKeyInstance.ActivitySetId &&
                                                                             pol.CustomerId == groupByKeyInstance.CustomerId &&
                                                                             pol.GroupByKey == groupByKey).ToList();

            var participantIds = proposedOrderLines.Where(pol => pol.ParticipantId > 0)
                                                   .Select(pol => pol.ParticipantId)
                                                   .Distinct();

            var activityIds = proposedOrderLines.Where(pol => pol.ParticipantId == -1)
                                                .Select(pol => pol.ActivityId)
                                                .Distinct();

            var proposedOrderLine = proposedOrderLines.FirstOrDefault(pol => pol.GroupByKey == groupByKey);
            if (proposedOrderLine != null)
            {
                var participants = Context.Participants.Include(p => p.ParticipantInfoElements).Where(p => participantIds.Contains(p.Id)).ToList();
                var onlinePaymentReference = participants.Where(p => !string.IsNullOrEmpty(p.OnlinePaymentReference)).Select(p => p.OnlinePaymentReference).FirstOrDefault();
                var participantOrderLines = participants.Select(CreateProposedOrderLine).ToList();

                var activities = Context.Activities.Where(a => activityIds.Contains(a.Id)).ToList();
                var activityOrderLines = activities.Select(CreateProposedOrderLine).ToList();

                var order = new Order
                {
                    GroupByKey = groupByKey,
                    CoordinatorId = proposedOrderLine.CoordinatorId,
                    YourOrder = proposedOrderLine.YourOrder,
                    YourRef = onlinePaymentReference ?? proposedOrderLine.YourRef,
                    OurRef = proposedOrderLine.OurRef,
                    Status = OrderStatus.Proposed,
                    Title = proposedOrderLine.Title,
                    LastModified = DateTime.UtcNow,
                    CustomerId = groupByKeyInstance.CustomerId,
                    Customer = Context.Customers.FirstOrDefault(c => c.Id == groupByKeyInstance.CustomerId),
                    ParticipantOrderLines = participantOrderLines,
                    ActivityOrderLines = activityOrderLines
                };

                return order;
                
            }
            return null;
        }

        public List<Order> FindCreditedOrders(Order order)
        {
            return
                Context.Orders.Where(o => o.CreditedOrderId.HasValue && o.CreditedOrderId == order.Id)
                    .Include(o => o.ParticipantOrderLines)
                    .Include(o => o.ActivityOrderLines)
                    .ToList();
        }

        public Order AddParticipantsToOrder(Order order, List<Participant> participants)
        {
            var orderLines = participants.Select(p => CreateParticipantOrderLine(order, p)).ToList();
            AddOrderLines(order, orderLines);

            return order;
        }

        public Order AddActivitiesToOrder(Order order, List<Activity> activities)
        {
            var orderLines = activities.Select(a => CreateActivityOrderLine(order, a)).ToList();
            AddOrderLines(order, orderLines);

            return order;
        }


        /// <summary>
        /// Creates an ParticipantOrderLine for an order with status = Draft
        /// </summary>
        /// <param name="order">A draft order</param>
        /// <param name="participant"></param>
        /// <returns></returns>
        public ParticipantOrderLine CreateParticipantOrderLine(Order order, Participant participant)
        {

            //If the orderline already exists on the order...
            var line = Context.ParticipantOrderLines?.FirstOrDefault(l => l.Participant.Id == participant.Id && l.ActivityId == participant.ActivityId && l.Order.Id == order.Id);

            if (line != null && line.Participant == null)
            {
                Context.Entry(line).Reference(l => l.Participant).Load();
            }

            //If it doesnt exist, it means the order is a draft, and the orderline should only include the relational mapping...
            return line ?? new ParticipantOrderLine
            {
                ActivityId = participant.ActivityId,
                Participant = participant,
            };
        }

        public ActivityOrderLine CreateActivityOrderLine(Order order, Activity activity)
        {
            //If the orderline already exists on the order...
            var line = Context.ActivityOrderLines.FirstOrDefault(l => l.Activity.Id == activity.Id && l.Order.Id == order.Id && (activity.FixedPrice == null || activity.FixedPrice == 0));
            var customerArticle = Context.CustomerArticles.FindById(activity.CustomerArticleId, Context.Articles);

            //If it doesnt exist, it means the order is a draft or it is a "headerorderline", and the orderline should only include the relational mapping...
            if (line == null)
            {
                return new ActivityOrderLine
                {
                    Activity = activity,
                    ArticlePath = customerArticle.ArticlePath
                };
            }

            line.ArticlePath = customerArticle.ArticlePath;

            if (line.Activity == null)
                Context.Entry(line).Reference(l => l.Activity).Load();

            return line;
        }

        public ParticipantOrderLine CreateProposedOrderLine(Participant participant)
        {
            var activity = 
                Context.SearchableActivities.Where(a => a.ActivityId == participant.ActivityId)
                    .Select(act => act)
                    .FirstOrDefault();

            LoadParticipantRelations(participant);

            return new ParticipantOrderLine
            {
                Id = 0,
                Participant = participant,
                Price = participant.UnitPrice,
                CurrencyCodeId = participant.CurrencyCodeId,
                OnlinePaymentReference = participant.OnlinePaymentReference,
                ActivityId = participant.ActivityId,
                InfoText = participant.GetInfoTexts(),
                ActivityName = activity != null ? activity.Name : ""
            };
        }

        private ActivityOrderLine CreateProposedOrderLine(Activity activity)
        {
            return new ActivityOrderLine
            {
                Id = 0,
                Activity = activity,
                Price = activity.FixedPrice,
                CurrencyCodeId = activity.CurrencyCodeId,
                ActivityName = activity.Name
            };
        }

        private void LoadParticipantRelations(Participant participant)
        {
            if (participant.User == null)
                Context.Entry(participant).Reference(p => p.User).Load();

            if (participant.ParticipantInfoElements == null || participant.ParticipantInfoElements.Count == 0)
                Context.Entry(participant).Collection(p => p.ParticipantInfoElements).Load();

            foreach (ParticipantInfoElement element in participant.ParticipantInfoElements)
            {
                if (element.LeadingText == null)
                    Context.Entry(element).Reference(e => e.LeadingText).Load();

                if (element.LeadingText.CustomLeadingTexts == null)
                    element.LeadingText.CustomLeadingTexts = Context.CustomLeadingTexts.Where(clt => clt.CustomerId == participant.CustomerId && clt.LeadingTextId == element.LeadingTextId).ToList();
            }
        }

 
        public IEnumerable<Order> FindOrdersWithTemporaryStatus()
        {
            return Context.Orders.Where(o => o.Status == OrderStatus.Transferred || o.Status == OrderStatus.NotInvoiced).ToList();
        }

        public PartialList<SearchableOrder> Find(string query, int? distributorId, int? coordinatorId, int[] statusFilter, int[] customerTypeFilter, DateTime? fromDate, DateTime? toDate, int skip, int limit, string orderBy, string orderDirection)
        {
            IQueryable<SearchableOrder> orders = Context.SearchableOrders;

            if (!string.IsNullOrEmpty(query))
            {
                foreach (var subQuery in query.Split(' '))
                {
                    var q = subQuery;
                    orders = orders.Where(o => (o.OrderNumber.HasValue && SqlFunctions.StringConvert((decimal?)o.OrderNumber.Value).Trim() == q) ||
                                               o.Title.Contains(q) || o.Name.Contains(q) || o.CoordinatorFullName.Contains(q) ||
                                               o.YourOrder.Contains(q) || o.YourRef.Contains(q) || o.OurRef.Contains(q) ||
                                               (o.InvoiceNumber.HasValue && SqlFunctions.StringConvert((decimal?)o.InvoiceNumber.Value).Trim() == q));
                }
            }
            
            if (distributorId.HasValue)
            {
                orders = orders.Where(o => o.DistributorId == distributorId.Value);
            }

            if (coordinatorId.HasValue)
            {
                orders = orders.Where(o => o.CoordinatorId == coordinatorId.Value);
            }

            if (statusFilter != null && statusFilter.Any())
            {
                orders = orders.Where(o => statusFilter.Contains((int)o.OrderStatus));
            }

            if (customerTypeFilter != null && customerTypeFilter.Any())
            {
                orders = orders.Where(o => customerTypeFilter.Contains((int)o.CustomerType));
            }

            if (fromDate.HasValue)
            {
                orders = orders.Where(o => DbFunctions.CreateDateTime(o.LastModified.Year, o.LastModified.Month, o.LastModified.Day, 0, 0, 0) >= fromDate.Value);
            }
            if (toDate.HasValue)
            {
                orders = orders.Where(o => DbFunctions.CreateDateTime(o.LastModified.Year, o.LastModified.Month, o.LastModified.Day, 0, 0, 0) <= toDate.Value);
            }

            orders = Order(orders, orderBy, orderDirection);
            var orderList = orders.ToList();
            var totalCount = orderList.Count;

            if (skip + limit > totalCount)
            {
                if (skip > totalCount)
                {
                    skip = 0;
                }
                else
                {
                    limit = totalCount - skip;
                }
            }

            orderList = orderList.Count >= limit ? orderList.GetRange(skip, limit) : orderList;

            return orderList.ToPartialList(totalCount, orderBy);
        }

        public Order Create(Order order)
        {
            ValidateOrder(order);

            order.Status = OrderStatus.Draft;
            order.LastModified = DateTime.UtcNow;

            order.Customer = null;
            
            Context.Orders.Add(order);
            Context.SaveChanges();
            return FindOrder(order.Id);
        }

        public Order Update(int id, Order order, bool forceUpdate = false)
        {
            ValidateOrder(order);
            
            var dbOrder = FindOrder(id);
            if (!forceUpdate)
            {
                ValidateOrderStatus(dbOrder);    
            }

            if (!order.RowVersion.SequenceEqual(dbOrder.RowVersion))
            {
                throw new DbUpdateConcurrencyException("There is a newer record of the order in the database.");
            }

            order.LastModified = DateTime.UtcNow;

            Context.Entry(dbOrder).CurrentValues.SetValues(order);

            Context.SaveChanges();

            return FindOrder(id);
        }


        public Order AddOrderLines(Order order, List<ParticipantOrderLine> participantOrderLines)
        {
            var existingParticipations = order.ParticipantOrderLines.Select(pol => pol.Participant.Id);
            order.ParticipantOrderLines.AddRange(participantOrderLines.Where(pol => !existingParticipations.Contains(pol.Participant.Id)));
            Context.SaveChanges();

            return FindOrder(order.Id);
        }

        public Order AddOrderLines(Order order, List<ActivityOrderLine> activityOrderLines)
        {
            order.ActivityOrderLines.AddRange(activityOrderLines);
            Context.SaveChanges();

            return FindOrder(order.Id);
        }

        public void RemoveOrderLines(int[] orderlineIds)
        {
            var participantOrderlines = Context.ParticipantOrderLines.Where(l => orderlineIds.Contains(l.Id));
            var activityOrderlines = Context.ActivityOrderLines.Where(l => orderlineIds.Contains(l.Id));

            foreach (var orderline in participantOrderlines)
            {
                Context.ParticipantOrderLines.Remove(orderline);
            }

            foreach (var orderline in activityOrderlines)
            {
                Context.ActivityOrderLines.Remove(orderline);
            }

            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            var order = Context.Orders.FirstOrDefault(o => o.Id == id);

            if (order != null)
            {
                if (order.Status != OrderStatus.Draft)
                {
                    throw new ValidationException("Can only delete orders with status Draft");
                }

                Context.Orders.Remove(order);
                Context.SaveChanges();    
            }
        }

        public Dictionary<int, List<Order>> GetDictionaryOfOrdersForParticipantsInOrder(Order order)
        {
            return order.ParticipantOrderLines.ToDictionary(l => l.Participant.Id, p => Context.Orders.WithParticipants().Where(o => o.ParticipantOrderLines.Any(ol => ol.Participant.Id == p.Participant.Id)).ToList());  
        }

        public Dictionary<int, List<Order>> GetDictionaryOfOrdersForActivtiesInOrder(Order order)
        {
            return order.ActivityOrderLines.ToDictionary(a => a.Activity.Id, a => Context.Orders.WithActivities().Where(o => o.ActivityOrderLines.Any(ol => ol.Activity.Id == a.Activity.Id)).ToList());
        }

        private IQueryable<SearchableOrder> Order(IQueryable<SearchableOrder> orders, string orderBy, string orderDirection)
        {
            orderBy = orderBy == null ? "" : orderBy.ToLower();
            orderDirection = orderDirection == null ? "" : orderDirection.ToLower();

            switch (orderBy)
            {
                case "ordernumber":
                    return orderDirection == "desc" ? orders.OrderByDescending(o => o.OrderNumber) : orders.OrderBy(o => o.OrderNumber);
                case "title":
                    return orderDirection == "desc" ? orders.OrderByDescending(o => o.Title) : orders.OrderBy(o => o.Title);
                case "customername":
                    return orderDirection == "desc" ? orders.OrderByDescending(o => o.Name) : orders.OrderBy(o => o.Name);
                case "programcoordinator":
                    return orderDirection == "desc" ? orders.OrderByDescending(o => o.CoordinatorFullName) : orders.OrderBy(o => o.CoordinatorFullName);
                case "yourorder":
                    return orderDirection == "desc" ? orders.OrderByDescending(o => o.YourOrder) : orders.OrderBy(o => o.YourOrder);
                case "yourref":
                    return orderDirection == "desc" ? orders.OrderByDescending(o => o.YourRef) : orders.OrderBy(o => o.YourRef);
                case "ourref":
                    return orderDirection == "desc" ? orders.OrderByDescending(o => o.OurRef) : orders.OrderBy(o => o.OurRef);
                case "amount":
                    return orderDirection == "desc" ? orders.OrderByDescending(o => o.TotalAmount) : orders.OrderBy(o => o.TotalAmount);
                case "lastmodified":
                    return orderDirection == "desc" ? orders.OrderByDescending(o => o.LastModified) : orders.OrderBy(o => o.LastModified);
                case "status":
                    return orderDirection == "desc" ? orders.OrderByDescending(o => o.OrderStatus) : orders.OrderBy(o => o.OrderStatus);
                case "invoicenumber":
                    return orderDirection == "desc" ? orders.OrderByDescending(o => o.InvoiceNumber) : orders.OrderBy(o => o.InvoiceNumber);
                default:
                    return orderDirection == "desc" ? orders.OrderByDescending(o => o.LastModified) : orders.OrderBy(o => o.LastModified);
            }
        }

        private void ValidateOrderStatus(Order dbOrder)
        {
            var loadedOrder = Context.Orders.AsNoTracking().FirstOrDefault(o => o.Id == dbOrder.Id);
            var dbOrderStatus = loadedOrder == null ? OrderStatus.Proposed : loadedOrder.Status;

            if (dbOrderStatus != OrderStatus.Draft && dbOrderStatus != OrderStatus.Proposed)
            {
                throw new ValidationException("Invalid order", new List<ValidationResult> { new ValidationResult("Order has been invoiced and cannot be changed") });
            }
        }

        private void ValidateOrder(Order order)
        {
            var participantsOrders = GetDictionaryOfOrdersForParticipantsInOrder(order);
            var activitiesOrders = GetDictionaryOfOrdersForActivtiesInOrder(order);
            
            var validationResults = _orderValidator.Validate(order, participantsOrders, activitiesOrders).ToList();
            if (validationResults.Count > 0)
            {
                throw new ValidationException("Invalid order", validationResults);
            }
        }
    }
}