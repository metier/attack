﻿using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface IResourceRepository
    {
        Resource FindById(int id);
        Resource Create(Resource resource);
        Resource Update(Resource resource);
        PartialList<Resource> GetResources(string query, string[] searchColumns, int? type, int? skip, int? limit, string orderBy, string orderDirection);
    }
}