using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class RcoRepository : RepositoryBase, IRcoRepository
    {
        public RcoRepository(MetierLmsContext context) : base(context) {}

        public PartialList<SearchableRco> GetRcos(string query, int[] rcoIds, string orderBy, string orderDirection, string[] contexts, int? skip, int? limit)
        {
            IQueryable<SearchableRco> courses = Context.SearchableCourseRcos;
            if (!string.IsNullOrEmpty(query))
            {
                foreach (string subQuery in query.Split(' '))
                {
                    var q = subQuery;
                    courses = courses.Where(c => c.Name.Contains(q) || c.Language.Contains(q) || c.Version.Contains(q));
                }
            }

            if (rcoIds != null && rcoIds.Length > 0)
            {
                courses = courses.Where(c => rcoIds.ToList().Contains(c.Id));
            }

            if (contexts != null && contexts.Length > 0)
            {
                courses = courses.Where(c => contexts.Contains(c.Context));
            }

            var totalCount = courses.Count();
            courses = Order(courses, orderBy, orderDirection);

            if (skip.HasValue)
            {
                courses = courses.Skip(skip.Value);
            }
            if (limit.HasValue && limit.Value > 0)
            {
                courses = courses.Take(limit.Value);
            }

            return courses.ToPartialList(totalCount, orderBy);
        }

        private IQueryable<SearchableRco> Order(IQueryable<SearchableRco> customers, string orderBy, string orderDirection)
        {
            orderBy = orderBy == null ? "" : orderBy.ToLower();
            orderDirection = orderDirection == null ? "" : orderDirection.ToLower();

            switch (orderBy)
            {
                case "name":
                    return orderDirection == "desc" ? customers.OrderByDescending(c => c.Name) : customers.OrderBy(c => c.Name);
                case "language":
                    return orderDirection == "desc" ? customers.OrderByDescending(c => c.Language) : customers.OrderBy(c => c.Language);
                case "version":
                    return orderDirection == "desc" ? customers.OrderByDescending(c => c.Version) : customers.OrderBy(c => c.Version);
                default:
                    return orderDirection == "desc" ? customers.OrderByDescending(c => c.Id) : customers.OrderBy(c => c.Id);
            }
        }
    }
}