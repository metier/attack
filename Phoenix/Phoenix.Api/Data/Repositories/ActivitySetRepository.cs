﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Data.Interfaces;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Core.Utilities.Collections;
using ValidationException = Metier.Phoenix.Core.Exceptions.ValidationException;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class ActivitySetRepository : RepositoryBase, IActivitySetRepository
    {
        private readonly IValidate<ActivitySet> _activitySetValidator;

        public ActivitySetRepository(MetierLmsContext context, IValidate<ActivitySet> activitySetValidator) : base(context)
        {
            _activitySetValidator = activitySetValidator;
        }

        public ActivitySet FindById(int id)
        {
            var activitySet = Context.ActivitySets.WithAllExceptShares().FirstOrDefault(a => a.Id == id && !a.IsDeleted);
            return activitySet.IncludeActivitySetShares(Context) as ActivitySet;
        }

        public PartialList<ActivitySet> Find(int? customerId, string query, string orderBy, string orderDirection, int? instructorUserId, int? ecoachUserId, bool calculateAvailableSpots, int? skip, int? limit)
        {
            IQueryable<ActivitySet> activitySets = Context.ActivitySets;
            
            if (customerId.HasValue)
                activitySets = activitySets.Include(c => c.ActivitySetShares)
                         .Where(a => (a.CustomerId == customerId || (a.ActivitySetShares.Any(cs => cs.CustomerId == customerId))) && !a.IsDeleted);

            if (!string.IsNullOrEmpty(query))
            {
                var subQueries = query.Split(' ');
                foreach (var subQuery in subQueries)
                {
                    var q = subQuery;
                    activitySets = activitySets.Where(a => a.Name.Contains(q));
                }
            }
            if (instructorUserId.HasValue)
            {
                activitySets = activitySets.Where(set => set.Activities.Any(a => a.Resources.Any(r => r.ResourceTypeId == 1 && (r as UserResource).UserId == instructorUserId)));
            }
            if (ecoachUserId.HasValue)
            {
                activitySets = activitySets.Where(set => set.Activities.Any(a => a.Resources.Any(r => r.ResourceTypeId == 3 && (r as UserResource).UserId == ecoachUserId)));
            }

            var totalCount = activitySets.Count();
            activitySets = Order(activitySets, orderBy, orderDirection);

            if (skip.HasValue)
            {
                activitySets = activitySets.Skip(skip.Value);
            }
            if (limit.HasValue && limit.Value > 0)
            {
                activitySets = activitySets.Take(limit.Value);
            }

            if (calculateAvailableSpots)
                return Context.LoadActivitySetAvailableSpots(activitySets).ToPartialList(totalCount, orderBy);
            //    return activitySets.Select(a => new { ActivitySet = a, AvailableSpots = Context.GetActivitySetExtraData(a.Id).Select(b => b.AvailableSpots).FirstOrDefault() }).ToList().Select(a => { a.ActivitySet.AvailableSpots = a.AvailableSpots; return a.ActivitySet; }).ToPartialList(totalCount, orderBy);

            return activitySets.ToList().ToPartialList(totalCount, orderBy);
        }

        private IQueryable<ActivitySet> Order(IQueryable<ActivitySet> activitySets, string orderBy, string orderDirection)
        {
            orderBy = orderBy == null ? "" : orderBy.ToLower();
            orderDirection = orderDirection == null ? "" : orderDirection.ToLower();

            switch (orderBy)
            {
                case "name":
                    return orderDirection == "desc" ? activitySets.OrderByDescending(a => a.Name) : activitySets.OrderBy(a => a.Name);
                case "publishfrom":
                    return orderDirection == "desc" ? activitySets.OrderByDescending(a => a.PublishFrom) : activitySets.OrderBy(a => a.PublishFrom);
                case "publishto":
                    return orderDirection == "desc" ? activitySets.OrderByDescending(a => a.PublishTo) : activitySets.OrderBy(a => a.PublishTo);
                case "enrollmentfrom":
                    return orderDirection == "desc" ? activitySets.OrderByDescending(a => a.EnrollmentFrom) : activitySets.OrderBy(a => a.EnrollmentFrom);
                case "enrollmentto":
                    return orderDirection == "desc" ? activitySets.OrderByDescending(a => a.EnrollmentTo) : activitySets.OrderBy(a => a.EnrollmentTo);
                case "ispublished":
                    return orderDirection == "desc" ? activitySets.OrderByDescending(a => a.IsPublished) : activitySets.OrderBy(a => a.IsPublished);
                default:
                    return orderDirection == "desc" ? activitySets.OrderByDescending(a => a.Id) : activitySets.OrderBy(a => a.Id);
            }
        }




        public IEnumerable<ActivitySet> GetCourseCalendar(int customerId, bool calculateAvailableSpots, bool includeResources)
        {
            IQueryable<ActivitySet> activitySets = Context.ActivitySets;
            
            activitySets = activitySets.Include(c => c.ActivitySetShares)
                                    .Where(a => (a.CustomerId == customerId || (a.ActivitySetShares.Any(cs => cs.CustomerId == customerId))) && a.IsPublished && !a.IsDeleted)
                                    .Where(a => a.PublishFrom == null || a.PublishFrom.Value <= DateTime.UtcNow)
                                    .Where(a => a.PublishTo == null || a.PublishTo.Value >= DateTime.UtcNow)
                                    .WithActivitiesAndActivityPeriods();

            if (includeResources)
                activitySets = activitySets.WithActivitiesAndResources();

            if (calculateAvailableSpots)
                return Context.LoadActivitySetAvailableSpots(activitySets);
                
            //    activitySets.Join()
            //    return activitySets.Select(a => new { ActivitySet = a, AvailableSpots = Context.GetActivitySetExtraData(a.Id).Select(b => b.AvailableSpots).FirstOrDefault() }).ToList().Select(a => { a.ActivitySet.AvailableSpots = a.AvailableSpots; return a.ActivitySet; }).ToList();
            
            return activitySets;
        }


        public ActivitySet Create(ActivitySet activitySet)
        {
            Validate(activitySet);

            activitySet.Activities = new List<Activity>();
            activitySet.Enrollments = new List<Enrollment>();
            activitySet.ActivitySetShares = new List<ActivitySetShare>();
            
            activitySet.Created = activitySet.LastModified = DateTime.UtcNow;

            Context.ActivitySets.Add(activitySet);
            Context.SaveChanges();
            //(Context as IObjectContextAdapter).ObjectContext.Refresh(System.Data.Entity.Core.Objects.RefreshMode.StoreWins, Context.ChangeTracker.Entries().Select(c => c.Entity).ToList());
            return FindById(activitySet.Id);
        }

        public ActivitySet Update(int id, ActivitySet activitySet)
        {
            activitySet.LastModified = DateTime.UtcNow;

            Validate(activitySet);

            var dbActivitySet = FindById(id);

            if (!activitySet.RowVersion.SequenceEqual(dbActivitySet.RowVersion))
            {
                throw new DbUpdateConcurrencyException("There is a newer record of the activityset in the database.");
            }
            
            Context.Entry(dbActivitySet).CurrentValues.SetValues(activitySet);
            
            Context.SaveChanges();

            var updated = FindById(id);
            return updated;
        }
        
        private void Validate(ActivitySet activitySet)
        {
            var validationResults = _activitySetValidator.Validate(activitySet).ToList();
            if (validationResults.Count > 0)
            {
                throw new ValidationException("Invalid activityset", validationResults);
            }
        }
    }
}