﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface IArticleRepository
    {
        Article FindByActivityId(int activityId);
        Article FindById(int id);
        Article Create(Article article);
        Article Update(int id, Article article);
        Article Copy(int originalArticleId, string copyTitle, string educationalPartner);
        PartialList<SearchableArticle> Find(int? productId, string[] status, string query, string orderBy, string orderDirection, int? skip, int? limit);
        Article FindByParticpantId(int participantId);
        ArticleType GetArticleType(int id);
        List<ArticleType> SearchArticleTypes(string query);
    }
}