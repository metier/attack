﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Transactions;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Api.Messaging;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.Extensions;
using Metier.Phoenix.Core.Data.Interfaces;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Core.Utilities.Collections;
using Microsoft.Ajax.Utilities;
using ValidationException = Metier.Phoenix.Core.Exceptions.ValidationException;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class ActivityRepository : RepositoryBase, IActivityRepository
    {
        private readonly IValidateActivity _activityValidator;
        private readonly IEventAggregator _eventAggregator;

        public ActivityRepository(MetierLmsContext context, 
                                  IValidateActivity activityValidator,
                                  IEventAggregator eventAggregator)
            : base(context)
        {
            _activityValidator = activityValidator;
            _eventAggregator = eventAggregator;
        }

        public SearchableActivity FindSearchableById(int id)
        {
            return Context.SearchableActivities.FirstOrDefault(a => a.ActivityId == id);
        }

        public PartialList<SearchableActivityWithActivitySet> FindWithActivitySets(int? distributorId, int? customerArticleId, int? customerId, int? activitySetId, int? articleOriginalId, bool? isProposed, bool? hasFixedPrice, string query, string orderBy, string orderDirection, int? skip, int? limit)
        {
            var activities = Context.SearchableActivitiesWithActivitySets.AsQueryable();
            if (distributorId.HasValue)
            {
                activities = activities.Where(a => a.DistributorId == distributorId.Value);
            }
            if (customerArticleId.HasValue)
            {
                activities = activities.Where(a => a.CustomerArticleId == customerArticleId.Value);
            }
            if (customerId.HasValue)
            {
                activities = activities.Where(a => a.CustomerId == customerId.Value);
            }
            if (articleOriginalId.HasValue)
            {
                activities = activities.Where(a => a.ArticleOriginalId == articleOriginalId.Value);
            }
            if (activitySetId.HasValue)
            {
                var activitySetActivityIds = Context.ActivitySets.Where(aSet => !aSet.IsDeleted && aSet.Id == activitySetId.Value)
                                                                 .SelectMany(aSet => aSet.Activities.Select(a => a.Id)).ToList();
                activities = activities.Where(a => activitySetActivityIds.Contains(a.ActivityId));
            }
            if (isProposed.HasValue)
            {
                activities = activities.Where(a => a.IsProposed == isProposed.Value);
            }
            if (hasFixedPrice.HasValue)
            {
                activities = activities.Where(a => a.FixedPrice > 0);
            }
            if (!string.IsNullOrEmpty(query))
            {
                var subQueries = query.Split(' ');
                foreach (var subQuery in subQueries)
                {
                    var q = subQuery;
                    activities = activities.Where(a => a.Name.Contains(q) || a.ArticlePath.Contains(q) || a.ArticleType.Contains(q) || a.CustomerName.Contains(q));
                }
            }

            activities = activities.Where(a => a.ActivitySetId != null);
            var totalCount = activities.Count();
            activities = Order(activities, orderBy, orderDirection);

            if (skip.HasValue)
            {
                activities = activities.Skip(skip.Value);
            }
            if (limit.HasValue && limit.Value > 0)
            {
                activities = activities.Take(limit.Value);
            }
            return activities.ToList().ToPartialList(totalCount, orderBy);
        }

        public PartialList<SearchableActivity> Find(int? distributorId, int? customerArticleId, int? customerId, int? activitySetId, int? articleOriginalId, bool? isProposed, bool? hasFixedPrice, string query, string orderBy, string orderDirection, int? skip, int? limit)
        {
            IQueryable<SearchableActivity> activities = Context.SearchableActivities.AsQueryable();
            if (distributorId.HasValue)
            {
                activities = activities.Where(a => a.DistributorId == distributorId.Value);
            }
            if (customerArticleId.HasValue)
            {
                activities = activities.Where(a => a.CustomerArticleId == customerArticleId.Value);
            }
            if (customerId.HasValue)
            {
                activities = activities.Where(a => a.CustomerId == customerId.Value);
            }
            if (articleOriginalId.HasValue)
            {
                activities = activities.Where(a => a.ArticleOriginalId == articleOriginalId.Value);
            }
            if (activitySetId.HasValue)
            {
                var activitySetActivityIds = Context.ActivitySets.Where(aSet => !aSet.IsDeleted && aSet.Id == activitySetId.Value)
                                                                 .SelectMany(aSet => aSet.Activities.Select(a => a.Id)).ToList();
                activities = activities.Where(a => activitySetActivityIds.Contains(a.ActivityId));
            }
            if (isProposed.HasValue)
            {
                activities = activities.Where(a => a.IsProposed == isProposed.Value);
            }
            if (hasFixedPrice.HasValue)
            {
                activities = activities.Where(a => a.FixedPrice > 0);
            }
            if (!string.IsNullOrEmpty(query))
            {
                var subQueries = query.Split(' ');
                foreach (var subQuery in subQueries)
                {
                    var q = subQuery;
                    activities = activities.Where(a => a.Name.Contains(q) || a.ArticlePath.Contains(q) || a.ArticleType.Contains(q) || a.CustomerName.Contains(q));
                }
            }

            var totalCount = activities.GroupBy(a => a.ActivityId).Count();
            activities = Order(activities, orderBy, orderDirection);

            if (skip.HasValue)
            {
                activities = activities.Skip(skip.Value);
            }
            if (limit.HasValue && limit.Value > 0)
            {
                activities = activities.Take(limit.Value);
            }
            return activities.DistinctBy(a => a.ActivityId).ToList().ToPartialList(totalCount, orderBy);
        }

        private IQueryable<SearchableActivity> Order(IQueryable<SearchableActivity> activities, string orderBy, string orderDirection)
        {
            orderBy = orderBy == null ? "" : orderBy.ToLower();
            orderDirection = orderDirection == null ? "" : orderDirection.ToLower();

            switch (orderBy)
            {
                case "activityend":
                    return orderDirection == "desc" ? activities.OrderByDescending(a => a.ActivityEnd) : activities.OrderBy(a => a.ActivityEnd);
                case "activitystart":
                    return orderDirection == "desc" ? activities.OrderByDescending(a => a.ActivityStart) : activities.OrderBy(a => a.ActivityStart);
                case "articlepath":
                    return orderDirection == "desc" ? activities.OrderByDescending(a => a.ArticlePath) : activities.OrderBy(a => a.ArticlePath);
                case "articletype":
                    return orderDirection == "desc" ? activities.OrderByDescending(a => a.ArticleType) : activities.OrderBy(a => a.ArticleType);
                case "rcoexamname":
                    return orderDirection == "desc" ? activities.OrderByDescending(a => a.RcoExamContainerName) : activities.OrderBy(a => a.RcoExamContainerName);
                case "rcocourseversion":
                    return orderDirection == "desc" ? activities.OrderByDescending(a => a.RcoCourseVersion) : activities.OrderBy(a => a.RcoCourseVersion);
                case "name":
                    return orderDirection == "desc" ? activities.OrderByDescending(a => a.Name) : activities.OrderBy(a => a.Name);
                default:
                    return activities.OrderByDescending(a => a.ActivityId);
            }
        }

        private IQueryable<SearchableActivityWithActivitySet> Order(IQueryable<SearchableActivityWithActivitySet> activities, string orderBy, string orderDirection)
        {
            orderBy = orderBy == null ? "" : orderBy.ToLower();
            orderDirection = orderDirection == null ? "" : orderDirection.ToLower();
            switch (orderBy)
            {
                case "activityend":
                    return orderDirection == "desc" ? activities.OrderByDescending(a => a.ActivityEnd) : activities.OrderBy(a => a.ActivityEnd);
                case "activitystart":
                    return orderDirection == "desc" ? activities.OrderByDescending(a => a.ActivityStart) : activities.OrderBy(a => a.ActivityStart);
                case "articlepath":
                    return orderDirection == "desc" ? activities.OrderByDescending(a => a.ArticlePath) : activities.OrderBy(a => a.ArticlePath);
                case "articletype":
                    return orderDirection == "desc" ? activities.OrderByDescending(a => a.ArticleType) : activities.OrderBy(a => a.ArticleType);
                case "activitysetname":
                    return orderDirection == "desc" ? activities.OrderByDescending(a => a.ActivitySetName) : activities.OrderBy(a => a.ActivitySetName);
                case "rcoexamname":
                    return orderDirection == "desc" ? activities.OrderByDescending(a => a.RcoExamContainerName) : activities.OrderBy(a => a.RcoExamContainerName);
                case "rcocourseversion":
                    return orderDirection == "desc" ? activities.OrderByDescending(a => a.RcoCourseVersion) : activities.OrderBy(a => a.RcoCourseVersion);
                case "name":
                    return orderDirection == "desc" ? activities.OrderByDescending(a => a.Name) : activities.OrderBy(a => a.Name);
                default:
                    return activities.OrderByDescending(a => a.ActivityId);
            }
        }

        public Activity Create(Activity activity, int copyFromActivityId = 0)
        {
            using (var transaction = new TransactionScope())
            {
                RemoveDuplicateRcoConnections(activity);
                Validate(activity);

                activity.ReplaceEntitiesFromContext(a => a.Resources, Context.Resources);
                activity.EnrollmentConditions = null;

                activity.Created = activity.LastModified = DateTime.UtcNow;
                Context.Activities.Add(activity);

                Context.SaveChanges();

                if (copyFromActivityId == 0)
                {
                    CopyCustomInformationToActivity(activity, "CustomerArticle", activity.CustomerArticleId);
                    CopyCustomerArticlesEnrollmentConditionsToActivity(activity);
                }
                else
                {
                    CopyCustomInformationToActivity(activity, "Activity", copyFromActivityId);
                    CopyEnrollmentConditionsToActivity(activity, copyFromActivityId);
                }                    

                transaction.Complete();
            }

            return activity;
        }

        private void CopyCustomInformationToActivity(Activity activity, string sourceEntityType, int sourceEntityId)
        {

            var customInformationSet =
                Context.CustomInformationSet.Where(
                    c =>
                        c.EntityId == sourceEntityId &&
                        c.EntityType.Equals(sourceEntityType, StringComparison.OrdinalIgnoreCase)).ToList();

            foreach (var item in customInformationSet)
            {
                var newItem = new CustomInformation
                {
                    EntityType = "Activity",
                    EntityId = activity.Id,
                    KeyField = item.KeyField,
                    Field1 = item.Field1,
                    Field2 = item.Field2,
                    Field3 = item.Field3,
                    Field4 = item.Field4,
                    Field5 = item.Field5
                };
                Context.CustomInformationSet.Add(newItem);
            }
            Context.SaveChanges();
        }

        private void CopyEnrollmentConditionsToActivity(Activity activity, int copyFromActivityId)
        {
            if (activity.EnrollmentConditions == null)
                activity.EnrollmentConditions = new List<EnrollmentCondition>();

            var sourceConditions = Context.EnrollmentConditions.Where(ec => ec.ActivityId == copyFromActivityId).ToList();

            foreach (var condition in sourceConditions)
                activity.EnrollmentConditions.Add(ClonEnrollmentCondition(activity.Id, condition));

            Context.SaveChanges();
        }

        private void CopyCustomerArticlesEnrollmentConditionsToActivity(Activity activity)
        {
            if (activity.EnrollmentConditions == null)
                activity.EnrollmentConditions = new List<EnrollmentCondition>();

            var sourceConditions = Context.CustomerArticleEnrollmentConditions.Where(ec => ec.CustomerArticleId == activity.CustomerArticleId);

            foreach (var condition in sourceConditions)
                activity.EnrollmentConditions.Add(ClonEnrollmentCondition(activity.Id, condition));

            Context.SaveChanges();
        }

        private EnrollmentCondition ClonEnrollmentCondition(int activityId, IEnrollmentCondition enrollmentCondition)
        {
            return
                new EnrollmentCondition
                {
                    ActivityId = activityId,
                    IsStrictRequirement = enrollmentCondition.IsStrictRequirement,
                    Title = enrollmentCondition.Title,
                    ProductId = enrollmentCondition.ProductId,
                    ArticleTypeId = enrollmentCondition.ArticleTypeId,
                    CustomerId = enrollmentCondition.CustomerId,
                    LanguageId = enrollmentCondition.LanguageId,
                };
        }

        internal void RemoveDuplicateRcoConnections(Activity activity)
        {
            activity.ActivityExamCourses = activity.ActivityExamCourses.DistinctBy(a => a.CourseRcoId).ToList();
        }

        public void AddParticipant(Activity activity, Participant participant)
        {

            if (!activity.Participants.Any(p => p.UserId == participant.UserId && !p.IsDeleted))
            {
                if (HasAvailableSeats(activity))
                {
                    activity.Participants.Add(participant);
                    Context.SaveChanges();          //Needs to save here in order to retrieve the participantId
                    _eventAggregator.Publish(new ParticipantCreatedEvent { ParticipantId = participant.Id });

                }
                else
                    throw new ValidationException($"Activity '{activity.Name}' is already full. Max number of participants are {activity.MaxParticipants}.");
            }

            
        }

        public Activity Update(int id, Activity activity)
        {
            using (var transaction = new TransactionScope())
            {
                activity.LastModified = DateTime.UtcNow;

                var dbActivity = Context.Activities.WithAllExceptParticipants().FindById(id).IncludeParticipants(Context);

                Validate(activity, dbActivity);

                if (!activity.RowVersion.SequenceEqual(dbActivity.RowVersion))
                {
                    throw new DbUpdateConcurrencyException("There is a newer record of the activity in the database.");
                }

                Context.Entry(dbActivity).CurrentValues.SetValues(activity);

                if (activity.ActivityAutomailText != null)
                {
                    if (dbActivity.ActivityAutomailText == null)
                    {
                        dbActivity.ActivityAutomailText = new ActivityAutomailText();
                    }
                    Context.Entry(dbActivity.ActivityAutomailText).CurrentValues.SetValues(activity.ActivityAutomailText);
                }
                
                activity.UpdateRelatedEntities(dbActivity, a => a.ActivityPeriods, Context);
                activity.UpdateRelatedEntities(dbActivity, a => a.Attachments, Context);
                activity.UpdateRelatedEntities(dbActivity, a => a.ActivityExamCourses, Context);

                var hasAddedResources = activity.Resources.Any(a => !dbActivity.Resources.Select(d => d.Id).Contains(a.Id));
                var hasDeletedResources = dbActivity.Resources.Any(a => !activity.Resources.Select(p => p.Id).Contains(a.Id));
                
                activity.UpdateExistingRelatedEntities(dbActivity, a => a.Resources, Context.Resources);

                Context.SaveChanges();
                if (hasAddedResources || hasDeletedResources)
                {
                    _eventAggregator.Publish(new ActivityResourcesChangedEvent
                    {
                        ActivityId = id
                    });
                }
                transaction.Complete();
            }
            return Context.Activities.WithAllExceptParticipants().FindById(id).IncludeParticipants(Context);
        }

        public Order GetCurrentOrderForActivity(int activityId)
        {
            var activity = Context.Activities.FindById(activityId);
            return GetCurrentOrderForActivity(activity);
        }

        public Order GetCurrentOrderForActivity(Activity activity)
        {
            if (activity != null)
            {
                return Context.Orders.Where(o => o.ActivityOrderLines.Any(a => a.Activity.Id == activity.Id)).OrderByDescending(o => o.Id).FirstOrDefault();
            }
            return null;
        }

        public bool ActivityLockedInOrder(Activity activity)
        {
            var currentOrder = GetCurrentOrderForActivity(activity);

            if (currentOrder != null)
            {
                if (!currentOrder.IsCreditNote)
                {
                    return true;
                }

                if (!OrderHasBeenSentToInvoicing(currentOrder.Status))
                {
                    return true;
                }
            }
            return false;
        }

        private void Validate(Activity activity, Activity currentActivity = null)
        {
            //TODO: All validation logic should be placed in the ActivityValidator class...
            if ((activity.MinParticipants.HasValue && activity.MaxParticipants.HasValue) &&
                (activity.MinParticipants.Value > activity.MaxParticipants.Value))
            {
                throw new ValidationException("Invalid activity", new List<ValidationResult> {new ValidationResult("Minimum number of participants must not be larger than maximum number of participants")});
            }

            var customerArticle = Context.CustomerArticles.FindById(activity.CustomerArticleId, Context.Articles);
            if (customerArticle == null)
            {
                throw new ValidationException("Invalid activity", new List<ValidationResult> { new ValidationResult("Activity does not have any customer article") });
            }
            var articleCopy = Context.Articles.FindByIdWithArticlePath(customerArticle.ArticleCopyId);
            if (articleCopy == null)
            {
                throw new ValidationException("Invalid activity", new List<ValidationResult> { new ValidationResult("Customer article does not have any article") });
            }
            var currentOrder = GetCurrentOrderForActivity(currentActivity);

            var validationResults = _activityValidator.Validate(activity, articleCopy, currentActivity, currentOrder).ToList();
            if (articleCopy.ArticleTypeId == (int)ArticleTypes.CaseExam || articleCopy.ArticleTypeId == (int)ArticleTypes.ProjectAssignment || articleCopy.ArticleTypeId == (int)ArticleTypes.ExternalCertification)
            {
                if (activity.ActivityPeriods.Count > 1)
                {
                    validationResults.Add(new ValidationResult("Case exams, project assignments and external certifications cannot have multiple activity periods"));    
                }
            }

            //Validations not applicable to activities that's about to be created.
            if (activity.Id > 0)
            {
                if (activity.MaxParticipants.HasValue && activity.MaxParticipants < activity.Participants.Count(p => !p.IsDeleted))
                {
                    throw new ValidationException("Invalid activity", new List<ValidationResult> { new ValidationResult("Max number of participants cannot be less than current number of participants")});
                }

                if (ActivityLockedInOrder(activity) && activity.IsDeleted)
                {
                    validationResults.Add(new ValidationResult("Activity cannot be deleted because it has been added to an order"));
                }

                if (ActivityHasParticipants(activity) && activity.IsDeleted)
                {
                    validationResults.Add(new ValidationResult("Activity cannot be deleted because it has participants"));
                }
            }

            if (validationResults.Count > 0)
            {
                throw new ValidationException("Invalid activity", validationResults);
            }
        }

        private bool ActivityHasParticipants(Activity activity)
        {
            return  (activity.Participants != null && activity.Participants.Count(p => !p.IsDeleted) > 0) || Context.Activities.Any(a => a.Id == activity.Id && a.Participants.Count(p => !p.IsDeleted) > 0);
        }

        private bool HasAvailableSeats(Activity activity)
        {
            if (!activity.MaxParticipants.HasValue)
            {
                return true;
            }
            return activity.Participants.Count(p => !p.IsDeleted) < activity.MaxParticipants;
        }


        private bool OrderHasBeenSentToInvoicing(OrderStatus status)
        {
            return status == OrderStatus.Transferred ||
                   status == OrderStatus.NotInvoiced ||
                   status == OrderStatus.Invoiced ||
                   status == OrderStatus.Rejected;
        }
    }
}