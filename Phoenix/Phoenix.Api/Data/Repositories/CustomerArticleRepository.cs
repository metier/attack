using System;
using System.Linq;
using System.Transactions;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Extensions;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Exceptions;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class CustomerArticleRepository : RepositoryBase, ICustomerArticleRepository
    {
        private readonly IArticleRepository _articleRepository;

        public CustomerArticleRepository(MetierLmsContext context, IArticleRepository articleRepository) : base(context)
        {
            _articleRepository = articleRepository;
        }

        public CustomerArticle Create(CustomerArticle customerArticle)
        {
            if (customerArticle.ArticleOriginalId == 0)
            {
                throw new BadRequestException("Original article is not provided!");
            }

            CustomerArticle created;
            using (var transaction = new TransactionScope())
            {
                var educationalPartnerResource = GetEducationalPartnerResource(customerArticle);
                var educationalPartner = educationalPartnerResource == null ? "" : educationalPartnerResource.Name;
                var articleCopy = _articleRepository.Copy(customerArticle.ArticleOriginalId, customerArticle.Title, educationalPartner);

                customerArticle.ArticleCopyId = articleCopy.Id;

                if (educationalPartnerResource == null)
                    customerArticle.EducationalPartnerId = null;
                else
                    customerArticle.EducationalPartnerId = educationalPartnerResource.Id;

                customerArticle.Created = DateTime.UtcNow;
                customerArticle.LastModified = customerArticle.Created;
                Context.CustomerArticles.Add(customerArticle);
                
                Context.SaveChanges();
                
                transaction.Complete();
                created = customerArticle;  
            }
            created.WithArticle(Context.Articles);
            return created;
        }

        private Resource GetEducationalPartnerResource(CustomerArticle customerArticle)
        {
            if (customerArticle.ExamId.HasValue || customerArticle.ModuleId.HasValue)
            {
                var customerCoursePrograms = Context.CoursePrograms.WithAll().Where(cp => !cp.IsDeleted && cp.CustomerId == customerArticle.CustomerId);
                var connectedCourseProgram = customerCoursePrograms.FirstOrDefault(cp => cp.Steps.Any(s => s.Modules.Any(m => m.Id == customerArticle.ModuleId) || 
                                                                                                           s.Exams.Any(e => e.Id == customerArticle.ExamId)));
                if (connectedCourseProgram != null && connectedCourseProgram.EducationalPartnerResourceId.HasValue)
                {
                    return Context.Resources.FirstOrDefault(r => r.Id == connectedCourseProgram.EducationalPartnerResourceId.Value);
                }
            }
            return null;
        }

        public CustomerArticle Update(CustomerArticle customerArticle)
        {
            ValidateForUpdate(customerArticle);

            customerArticle.LastModified = DateTime.UtcNow;
            
            customerArticle.ConfigureDbContextForEntry(Context);
            Context.SaveChanges();
            customerArticle.WithArticle(Context.Articles);
            return customerArticle;
        }
        
        private void ValidateForUpdate(CustomerArticle customerArticle)
        {
            if (Context.Activities.Any(a => a.CustomerArticleId == customerArticle.Id && !a.IsDeleted) && customerArticle.IsDeleted)
            {
                throw new ValidationException("Customer Article contains activity(ies) and cannot be deleted");
            }
        }
    }
}