﻿using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface IAuthenticationTokenRepository
    {
        AuthenticationToken FindByToken(string token); 
    }
}