﻿using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class ProductCategoryRepository : RepositoryBase, IProductCategoryRepository
    {
        public ProductCategoryRepository(MetierLmsContext context) : base(context)
        {
        }

        public List<ProductCategory> GetProductCategories()
        {
            return Context.ProductCategories.ToList();
        }
    }
}