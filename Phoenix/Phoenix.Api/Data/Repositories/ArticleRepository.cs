using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Extensions;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Core.Utilities.Collections;
using ValidationException = Metier.Phoenix.Core.Exceptions.ValidationException;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class ArticleRepository : RepositoryBase, IArticleRepository
    {
        private readonly IAccountsProvider _accountsProvider;

        public ArticleRepository(MetierLmsContext context, IAccountsProvider accountsProvider) : base(context)
        {
            _accountsProvider = accountsProvider;
        }

        public Article FindByActivityId(int activityId)
        {
            return Context.Activities.Where(a => a.Id == activityId)
                                             .Join(Context.CustomerArticles, a => a.CustomerArticleId, ca => ca.Id, (a, ca) => ca)
                                             .Join(Context.Articles, ca => ca.ArticleCopyId, a => a.Id, (ca, a) => a)
                                             .First();
        }

        public Article FindById(int id)
        {
            var article = Context.Articles.Where(a => !a.IsDeleted).WithAll().SingleOrDefault(a => a.Id == id);
            return LoadNavigationPropertiesOfDerivedTypes(article);
        }

        public Article FindByParticpantId(int participantId)
        {
            var article = Context.Participants.Where(p => p.Id == participantId).Join(Context.Activities, p => p.ActivityId, a => a.Id, (p, a) => a)
			        .Join(Context.CustomerArticles, a => a.CustomerArticleId, ca => ca.Id, (a, ca) => ca)
			        .Join(Context.Articles, ca => ca.ArticleCopyId, a => a.Id, (ca, a) => a)
                    .Where(a => !a.IsDeleted)
                    .WithAll()
                    .FirstOrDefault();
            return LoadNavigationPropertiesOfDerivedTypes(article);
        }

        public Article Create(Article article)
        {
            article.Created = article.LastModified = DateTime.UtcNow;
            var userAccount = _accountsProvider.GetCurrentUser();
            article.CreatedById = article.ModifiedById = userAccount.User.Id;

            Context.Articles.Add(article);
            Context.SaveChanges();

            return FindById(article.Id);
        }

        public Article Update(int id, Article article)
        {
            var dbArticle = FindById(id);
            if (!article.RowVersion.SequenceEqual(dbArticle.RowVersion))
            {
                throw new DbUpdateConcurrencyException("There is a newer record of article in the database.");
            }

            article.LastModified = DateTime.UtcNow;
            var userAccount = _accountsProvider.GetCurrentUser();
            article.ModifiedById = userAccount.User.Id;
            
            Context.Entry(dbArticle).CurrentValues.SetValues(article);

            var caseExamArticle = article as CaseExamArticle;
            var dbCaseExamArticle = dbArticle as CaseExamArticle;
            if (caseExamArticle != null && dbCaseExamArticle != null)
            {
                dbCaseExamArticle.CaseAttachment = caseExamArticle.CaseAttachment;
            }

            article.UpdateRelatedEntities(dbArticle, a => a.ArticleExamCourses, Context);
            article.UpdateRelatedEntities(dbArticle, a => a.Attachments, Context);

            Context.SaveChanges();
            return FindById(id);
        }

        public Article Copy(int originalArticleId, string copyTitle, string educationalPartner)
        {
            var originalArticle = Context.Articles.WithAttachments().WithArticleExamCourses().Where(a => !a.IsDeleted).SingleOrDefault(a => a.Id == originalArticleId);
            
            if (originalArticle != null)
            {
                var attachments = new List<Attachment> (originalArticle.Attachments);
                var articleExamCourses = new List<ArticleExamCourse>(originalArticle.ArticleExamCourses);
                Context.Entry(originalArticle).State = EntityState.Detached;
                
                originalArticle.Id=  0;
                originalArticle.RowVersion = null;
                if (!string.IsNullOrEmpty(copyTitle))
                {
                    originalArticle.Title = copyTitle; 
                }

                var examArticle = originalArticle as ExamArticle;
                if (examArticle != null)
                {
                    if (!string.IsNullOrEmpty(educationalPartner) && !string.IsNullOrEmpty(examArticle.NoResultResponse))
                    {
                        examArticle.NoResultResponse = examArticle.NoResultResponse.Replace("{educationalpartner}", educationalPartner);
                    }
                }

                originalArticle.Attachments = attachments;
                foreach (var attachment in attachments)
                {
                    Context.Entry(attachment).State = EntityState.Detached;
                    attachment.Id = 0;
                    attachment.RowVersion = null;
                }

                originalArticle.ArticleExamCourses = articleExamCourses;
                foreach (var articleExamCourse in articleExamCourses)
                {
                    Context.Entry(articleExamCourse).State = EntityState.Detached;
                    articleExamCourse.Id = 0;
                    articleExamCourse.RowVersion = null;
                }

                Context.Articles.Add(originalArticle);
                Context.SaveChanges();
                return originalArticle;    
            }
            throw new ApplicationException("Failed to make copy article with ID " + originalArticleId);
        }

        public PartialList<SearchableArticle> Find(int? productId, string[] status, string query, string orderBy, string orderDirection, int? skip, int? limit)
        {
            IQueryable<SearchableArticle> articles = Context.SearchableArticles;
            if (productId.HasValue)
            {
                articles = articles.Where(a => a.ProductId == productId.Value);
            }
            if (!string.IsNullOrEmpty(query))
            {
                var subQueries = query.Split(' ');
                foreach (var subQuery in subQueries)
                {
                    var q = subQuery;
                    articles = articles.Where(a => a.Title.Contains(q) || a.Path.Contains(q) || a.ArticleType.Contains(q) || a.Language.Contains(q));    
                }
            }
            if (status != null && status.Length > 0)
            {
                var lowercaseArray = status.Select(i => i.ToLowerInvariant()).ToArray();
                articles = articles.Where(a => lowercaseArray.Contains(a.StatusCode.ToLower()));
            }
            
            var totalCount = articles.Count();
            articles = Order(articles, orderBy, orderDirection);
            
            if (skip.HasValue)
            {
                articles = articles.Skip(skip.Value);
            }
            if (limit.HasValue && limit.Value > 0)
            {
                articles = articles.Take(limit.Value);
            }
            

            return articles.ToList().ToPartialList(totalCount, orderBy);
        }
        
        private Article LoadNavigationPropertiesOfDerivedTypes(Article article)
        {
            var caseExam = article as CaseExamArticle;
            if (caseExam != null)
            {
                return Context.Articles.Where(a => a.Id == caseExam.Id).OfType<CaseExamArticle>().Include(e => e.CaseAttachment).WithAll().FirstOrDefault();
            }
            return article;
        }

        private IQueryable<SearchableArticle> Order(IQueryable<SearchableArticle> articles, string orderBy, string orderDirection)
        {
            orderBy = orderBy == null ? "" : orderBy.ToLower();
            orderDirection = orderDirection == null ? "" : orderDirection.ToLower();

            switch (orderBy)
            {
                case "path":
                    return orderDirection == "desc" ? articles.OrderByDescending(a => a.Path) : articles.OrderBy(a => a.Path);
                case "title":
                    return orderDirection == "desc" ? articles.OrderByDescending(a => a.Title) : articles.OrderBy(a => a.Title);
                case "status":
                    return orderDirection == "desc" ? articles.OrderByDescending(a => a.StatusCode) : articles.OrderBy(a => a.StatusCode);
                case "language":
                    return orderDirection == "desc" ? articles.OrderByDescending(a => a.Language) : articles.OrderBy(a => a.Language);
                case "type":
                    return orderDirection == "desc" ? articles.OrderByDescending(a => a.ArticleType) : articles.OrderBy(a => a.ArticleType);
                default:
                    return orderDirection == "desc" ? articles.OrderByDescending(a => a.Id) : articles.OrderBy(a => a.Id);
            }
        }

        public ArticleType GetArticleType(int id)
        {
            return Context.ArticleTypes.FirstOrDefault(at => at.Id == id);
        }

        public List<ArticleType> SearchArticleTypes(string query)
        {
            var articletypes = Context.ArticleTypes.AsQueryable();

            if (!string.IsNullOrEmpty(query))
            {
                foreach (string subQuery in query.Split(' '))
                {
                    var q = subQuery;
                    articletypes = articletypes.Where(at => at.Name.Contains(q) || at.CharCode.Contains(q));
                }
            }

            var sorted = articletypes.OrderBy(at => at.Name).ToList();
            return sorted;
        }

    }
}