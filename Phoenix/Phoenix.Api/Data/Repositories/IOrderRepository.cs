﻿using System;
using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface IOrderRepository
    {
        Order Create(Order order);
        Order FindOrder(int id);
        Order FindProposedOrder(string groupByKey);
        List<Order> FindCreditedOrders(Order order);
        IEnumerable<Order> FindOrdersWithTemporaryStatus();
        PartialList<SearchableOrder> Find(string query, int? distributorId, int? coordinatorId, int[] statusFilter, int[] customerTypeFilter, DateTime? fromDate, DateTime? toDate, int skip, int limit, string orderBy, string orderDirection);
        Order Update(int id, Order order, bool forceUpdate = false);
        void Delete(int id);

        Dictionary<int, List<Order>> GetDictionaryOfOrdersForParticipantsInOrder(Order order);
        Dictionary<int, List<Order>> GetDictionaryOfOrdersForActivtiesInOrder(Order order);

        Order AddParticipantsToOrder(Order order, List<Participant> participants);
        Order AddActivitiesToOrder(Order order, List<Activity> activities);

        ParticipantOrderLine CreateParticipantOrderLine(Order order, Participant participant);
        ActivityOrderLine CreateActivityOrderLine(Order order, Activity activity);

        ParticipantOrderLine CreateProposedOrderLine(Participant participant);

        Order AddOrderLines(Order order, List<ParticipantOrderLine> participantOrderLines);
        Order AddOrderLines(Order order, List<ActivityOrderLine> activityOrderLines);

        void RemoveOrderLines(int[] orderlineIds);
    }
}