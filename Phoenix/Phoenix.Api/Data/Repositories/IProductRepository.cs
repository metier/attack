﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface IProductRepository
    {
        List<Product> GetAll();
        List<Product> Search(string query);
        Product Create(Product product);
        Product Update(int id, Product product);
        ProductType GetProductTypeByProductId(int id);
        Product Get(int id);
    }
}