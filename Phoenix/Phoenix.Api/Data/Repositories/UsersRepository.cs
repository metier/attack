using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Transactions;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Extensions;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Core.Utilities.Collections;
using Metier.Phoenix.Core.Exceptions;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class UsersRepository : RepositoryBase, IUsersRepository
    {

        public UsersRepository(MetierLmsContext context) : base(context){}

        public User Create(User user)
        {
            if (user.Customer != null)
            {
                user.Customer.ConfigureDbContextForEntry(Context);
            }

            Context.Users.Add(user);
            CreateUserInfoElements(user);
            
            Context.SaveChanges();

            return Context.Users.WithAll().FindById(user.Id);
        }

        private void CreateUserInfoElements(User user)
        {
            if (user.UserInfoElements == null)
                user.UserInfoElements = new List<UserInfoElement>();

            if (user.Customer == null)
                Context.Entry(user).Reference(u => u.Customer).Load();

            if(user.Customer.CustomLeadingTexts == null || user.Customer.CustomLeadingTexts.Count == 0)
                Context.Entry(user.Customer).Collection(c => c.CustomLeadingTexts).Load();


            foreach (var customLeadingText in user.Customer.CustomLeadingTexts)
            {
                if (!user.UserInfoElements.Exists(e => e.LeadingTextId == customLeadingText.LeadingTextId))
                    user.UserInfoElements.Add(new UserInfoElement { UserId = user.Id, LeadingTextId = customLeadingText.LeadingTextId }); 
            }
        }

        public User FindByUsername(string username)
        {
            return Context.Users.WithAll().FirstOrDefault(u => u.MembershipUserId == username);
        }

        public User Update(User user)
        {
            //TODO: Consider replacing this with a call to 'CreateUserInfoElements'
            foreach (var userInfoElement in user.UserInfoElements)
            {
                //userInfoElement.CustomLeadingText = null;
                userInfoElement.ConfigureDbContextForEntry(Context);
            }

            if (user.Attachment != null)
            {
                user.Attachment.ConfigureDbContextForEntry(Context);
            }

            user.ConfigureDbContextForEntry(Context);

            Context.SaveChanges();
            return Context.Users.WithAll().FindById(user.Id);
        }

        public void MergeUsers(int userId, int userIdToMerge)
        {
            var primaryUser = Context.Users.FirstOrDefault(u => u.Id == userId);
            var secondaryUser = Context.Users.FirstOrDefault(u => u.Id == userIdToMerge);

            MergeUsers(primaryUser, secondaryUser);
        }

        public void MergeUsers(User primaryUser, User secondaryUser)
        {
            using (var transaction = new TransactionScope())
            {
                ValidateMerge(primaryUser, secondaryUser);

                MergeResources(primaryUser, secondaryUser);
                MergeEnrollments(primaryUser, secondaryUser);
                MergeParticipations(primaryUser, secondaryUser);
                MergeElearningProgress(primaryUser, secondaryUser);
                MergeMetierContactPersons(primaryUser, secondaryUser);
                MergeMails(primaryUser, secondaryUser);
                MergeComments(primaryUser, secondaryUser);
                MergeChangeLogItems(primaryUser, secondaryUser);
                MergeUserCompetences(primaryUser, secondaryUser);

                //NOTE: No need to update table 'usermissingproducts' as this will be recreated everytime the report is run.

                DeleteUser(secondaryUser);

                Context.SaveChanges();
                transaction.Complete();
            }
        }

        #region Methods for merging

        private void ValidateMerge(User primaryUser, User secondaryUser)
        {

            if(primaryUser.Id == secondaryUser.Id)
                throw new ValidationException("Both users are the same. Could not merge users.");

            var primaryParticipations = Context.Participants.Where(p => p.UserId == primaryUser.Id && !p.IsDeleted);
            var secondaryParticipations = Context.Participants.Where(p => p.UserId == secondaryUser.Id && !p.IsDeleted);

            var duplicateParticipations = primaryParticipations.Where(pp => secondaryParticipations.Select(se => se.ActivityId).Contains(pp.ActivityId));

            if (duplicateParticipations.Count() > 0)
                throw new ValidationException("Duplicate participations found. Could not merge users. ActivityIds: " + string.Join(", ", duplicateParticipations.Select(r => r.ActivityId)));

            //Checking enrollments/activitysets as well in case a participant was moved, which could result in no hit in the validation above.
            var duplicateEnrollments = Context.Enrollments
                .Where(e => e.UserId == primaryUser.Id && !e.IsCancelled)
                .Where(pe =>
                    Context.Enrollments.Where(se => se.UserId == secondaryUser.Id && !se.IsCancelled)
                        .Select(se => se.ActivitySetId)
                        .Contains(pe.ActivitySetId)
                );

            if (duplicateEnrollments.Count() > 0)
                throw new ValidationException("Duplicate enrollments found. Could not merge users. ActivitySetIds: " + string.Join(", ", duplicateEnrollments.Select(r => r.ActivitySetId)));

            var primaryActivities = Context.Activities.Where(a => primaryParticipations.Select(p => p.ActivityId).Contains(a.Id));
            var secondaryActivities = Context.Activities.Where(a => secondaryParticipations.Select(p => p.ActivityId).Contains(a.Id));

            var duplicateRcos = primaryActivities.Where(pa => pa.RcoCourseId != null)
                .Where(pa => secondaryActivities.Where(sa => sa.RcoCourseId != null).Select(sa => sa.RcoCourseId).Contains(pa.RcoCourseId));

            if (duplicateRcos.Count() > 0)
                throw new ValidationException("Both users registered on the same e-learning course. Could not merge users. RcoCourseIds: " + string.Join(", ",  duplicateRcos.Select(r=>r.RcoCourseId)));

            var duplicatePerformances = Context.ScormPerformances
                .Where(sp => sp.UserId == primaryUser.Id)
                .Where(psp =>
                    Context.ScormPerformances.Where(ssp => ssp.UserId == secondaryUser.Id)
                        .Select(ssp => ssp.RcoId).Contains(psp.RcoId)
                );

            if (duplicatePerformances.Count() > 0)
                throw new ValidationException("Both users have progress on the same e-learning course. Could not merge users. RcoIds: " + string.Join(", ", duplicatePerformances.Select(r => r.RcoId)));

        }

        private void MergeResources(User primaryUser, User secondaryUser)
        {
            var userResources = Context.Resources.Select(r=> r as UserResource).Where(r => r.UserId == secondaryUser.Id);
            userResources.ForEach(ur => ur.UserId = primaryUser.Id);
        }


        private void MergeEnrollments(User primaryUser, User secondaryUser)
        {
            var deletedEnrollments = Context.Enrollments.Where(p => p.UserId == secondaryUser.Id && p.IsCancelled).ToList();
            deletedEnrollments.ForEach(DeleteEnrollment);

            var enrollments = Context.Enrollments.Where(e => e.UserId == secondaryUser.Id).ToList();
            enrollments.ForEach(e => e.UserId = primaryUser.Id);
        }

        private void DeleteEnrollment(Enrollment enrollment)
        {
            var participations = Context.Participants.Where(p => p.EnrollmentId == enrollment.Id).ToList();
            if (participations.Any(p => !p.IsDeleted))
            {
                //This should not happen, but it might... ;-)
                throw new ValidationException("One or more cancelled enrollments have active participations. Could not merge users.");
            }

            participations.ForEach(DeleteParticipant);
            Context.SaveChanges();

            Context.Enrollments.Remove(enrollment);
        }

        private void DeleteParticipant(Participant participant)
        {
            var orderLines = Context.ParticipantOrderLines.Where(pol => pol.Participant.Id == participant.Id);
            if (orderLines.Any())
                throw new ValidationException("One or more participants marked for deletion has already been added to an order. Could not merge users.");
            
            var mails = Context.Mails.Where(m => m.ParticipantId == participant.Id);
            var serializedMailMessages = Context.SerializedMailMessages.Join(mails, sm => sm.Mail.Id, m => m.Id, (sm, m) => sm);

            serializedMailMessages.ForEach(sm => Context.SerializedMailMessages.Remove(sm));
            mails.ForEach(m => Context.Mails.Remove(m));

            Context.SaveChanges();
            Context.Participants.Remove(participant);
        }

        private void MergeParticipations(User primaryUser, User secondaryUser)
        {
            var activeParticipations = Context.Participants.Where(p => p.UserId == primaryUser.Id);
            var deletedParticipations = Context.Participants.Where(p => p.UserId == secondaryUser.Id && p.IsDeleted)
                .Where(sp => activeParticipations.Select(ap => ap.ActivityId).Contains(sp.ActivityId)); //Only delete 'duplicate' participations

            deletedParticipations.ForEach(DeleteParticipant);

            var participations = Context.Participants.Where(p => p.UserId == secondaryUser.Id);
            participations.ForEach(p => p.UserId = primaryUser.Id);

            var examgraders = Context.Participants.Where(p => p.ExamGraderUserId.Value == secondaryUser.Id);
            examgraders.ForEach(eg => eg.UserId = primaryUser.Id);
        }

        private void MergeComments(User primaryUser, User secondaryUser)
        {
            var comments = Context.Comments.Where(c => c.Entity == "user" && c.EntityId == secondaryUser.Id);
            comments.ForEach(c=> c.EntityId = primaryUser.Id);
        }

        private void MergeMails(User primaryUser, User secondaryUser)
        {
            var mails = Context.Mails.Where(m => m.UserId == secondaryUser.Id);
            mails.ForEach(m => m.UserId = primaryUser.Id);
        }

        private void MergeElearningProgress(User primaryUser, User secondaryUser)
        {
            var attempts = Context.ScormAttempts.Where(sa => sa.UserId == secondaryUser.Id);
            attempts.ForEach(a => a.UserId = primaryUser.Id);

            var performances = Context.ScormPerformances.Where(sp => sp.UserId == secondaryUser.Id);
            performances.ForEach(p => p.UserId = primaryUser.Id);
        }

        private void MergeMetierContactPersons(User primaryUser, User secondaryUser)
        {
            var persons = Context.MetierContactPersons.Where(cp => cp.UserId == secondaryUser.Id);
            persons.ForEach(p => p.UserId = primaryUser.Id);
        }

        private void MergeChangeLogItems(User primaryUser, User secondaryUser)
        {
            var items = Context.ChangeLog.Where(cp => cp.UserId == secondaryUser.Id);
            items.ForEach(i => i.UserId = primaryUser.Id);
        }

        private void MergeUserCompetences(User primaryUser, User secondaryUser)
        {

            var userIds = new List<int> {primaryUser.Id, secondaryUser.Id};
            var selectedItem = Context.UserCompetences.Where(uc => userIds.Contains(uc.UserId)).OrderByDescending(uc => uc.IsTermsAccepted).ThenByDescending(uc => uc.TermsAcceptedDate).ThenByDescending(uc => uc.Created).FirstOrDefault();

            if (selectedItem == null)
                return;

            if (selectedItem.UserId == primaryUser.Id)
            {
                var secondaryUserItems = Context.UserCompetences.Where(uc => uc.UserId == secondaryUser.Id);
                secondaryUserItems.ForEach(i=> Context.UserCompetences.Remove(i));
                return;
            }

            if (selectedItem.UserId == secondaryUser.Id)
            {
                var primaryUserItems = Context.UserCompetences.Where(uc => uc.UserId == primaryUser.Id);
                primaryUserItems.ForEach(i => Context.UserCompetences.Remove(i));
                selectedItem.UserId = primaryUser.Id;   //Intentionally not overwriting firstname, lastname, e-mail etc, in order to keep the historical values until the user eventually changes them.
            }
        }

        #endregion

        private void DeleteUser(User user)
        {
            DeletePasswordRequests(user);
            Context.Users.Remove(user);
        }

        private void DeletePasswordRequests(User secondaryUser)
        {
            var requests = Context.PasswordResetRequests.Where(m => m.UserId == secondaryUser.Id);
            requests.ForEach(r => Context.PasswordResetRequests.Remove(r));
        }


        public PartialList<SearchableUser> Search(int? distributorId, int? customerId, int? userId, int? activityId, int? activitySetId, bool phoenixUsersOnly, bool instructorUsersOnly, bool ecoachUsersOnly, bool includeParticipations, string query, bool alsoSearchCustomFields, string advancedQuery, bool? isActive, string orderBy, string orderDirection, int? skip, int? limit)
        {
            var users = Context.SearchableUsers.AsQueryable();

            if (userId.HasValue)
            {
                users = users.Where(u => u.Id == userId.Value);
            }
            if (phoenixUsersOnly)
            {
                users = users.Where(u => u.IsPhoenixUser);
            }
            if (distributorId.HasValue)
            {
                users = users.Where(u => u.DistributorId == distributorId.Value);
            }
            if (customerId.HasValue)
            {
                var customerIds = GetCustomerIdHierarchy(customerId.Value).ToList();    // Consider rewriting this logic so a .Join() is used 
                users = users.Where(u => customerIds.Contains(u.CustomerId));           // instead of a .Contains() wich is no good performancewise.
            }
            if (activityId.HasValue)
            {
                var participants = Context.Participants.Where(p => p.ActivityId == activityId.Value && !p.IsDeleted);
                users = users.Join(participants, u => u.Id, p => p.UserId, (u, p) => u);
            }
            if (activitySetId.HasValue)
            {
                var enrollments = Context.Enrollments.Where(e => !e.IsCancelled);
                var activitySets = Context.ActivitySets.Where(aSet => aSet.Id == activitySetId.Value && !aSet.IsDeleted);

                users = users.Join(enrollments, u => u.Id, e => e.UserId, (u, e) => new {u, e})
                    .Join(activitySets, ue => ue.e.ActivitySetId, aSet => aSet.Id, (ue, aSet) => ue.u);
            }

			if (instructorUsersOnly)
			{
			    var instructors = Context.Resources.Where(r => r.ResourceTypeId == 1).Select(r => r as UserResource);
			    users = users.Join(instructors, u => u.Id, i => i.UserId, (u, i) => u);
			}
			if (ecoachUsersOnly) 
			{
                var eCoaches = Context.Resources.Where(r => r.ResourceTypeId == 3).Select(r => r as UserResource);
                users = users.Join(eCoaches, u => u.Id, e => e.UserId, (u, e) => u);
			}
            if (!string.IsNullOrEmpty(query))
            {
                var subQueries = query.Split(' ');
                foreach (var subQuery in subQueries)
                {
                    var q = subQuery;

                    if(alsoSearchCustomFields)
                        users = users.Where(u => System.Data.Entity.SqlServer.SqlFunctions.StringConvert((double)u.Id).Contains(q) || u.FirstName.Contains(q) || u.LastName.Contains(q) || u.PhoneNumber.Contains(q) || u.CustomerName.Contains(q) || u.Email.Contains(q) || u.UserName.Contains(q) || u.LeadingText11.Contains(q) || u.LeadingText12.Contains(q) || u.LeadingText13.Contains(q) || u.LeadingText14.Contains(q) || u.LeadingText15.Contains(q) || u.LeadingText16.Contains(q) || u.LeadingText17.Contains(q));
                    else
                        users = users.Where(u => System.Data.Entity.SqlServer.SqlFunctions.StringConvert((double)u.Id).Contains(q) || u.FirstName.Contains(q) || u.LastName.Contains(q) || u.PhoneNumber.Contains(q) || u.CustomerName.Contains(q) || u.Email.Contains(q) || u.UserName.Contains(q));
                }
            }

			if (!string.IsNullOrWhiteSpace(advancedQuery))
			{
				var queries = advancedQuery.Split('|');
				foreach(var q in queries)
				{
					if (!string.IsNullOrWhiteSpace(q))
					{
						var col = q.Split(':')[0];
						var val = q.Substring(col.Length + 1);
						var cols = col.Split(',');

						var subQueries = val.Split(' ');
						foreach (var subQuery in subQueries)
						{
							users = users.Where(u => 
							(!cols.Contains("FirstName") || u.FirstName.Contains(subQuery)) 
							&& (!cols.Contains("LastName") || u.LastName.Contains(subQuery))
							&& (!cols.Contains("PhoneNumber") || u.PhoneNumber.Contains(subQuery))
							&& (!cols.Contains("CustomerName") || u.CustomerName.Contains(subQuery))
							&& (!cols.Contains("Email") || u.Email.Contains(subQuery))
							&& (!cols.Contains("UserName") || u.UserName.Contains(subQuery))
							&& (!cols.Contains("LeadingText11") || u.LeadingText11.Contains(subQuery))
							&& (!cols.Contains("LeadingText12") || u.LeadingText12.Contains(subQuery))
							&& (!cols.Contains("LeadingText13") || u.LeadingText13.Contains(subQuery))
							&& (!cols.Contains("LeadingText14") || u.LeadingText14.Contains(subQuery))
							&& (!cols.Contains("LeadingText15") || u.LeadingText15.Contains(subQuery))
							&& (!cols.Contains("LeadingText16") || u.LeadingText16.Contains(subQuery))
							&& (!cols.Contains("LeadingText17") || u.LeadingText17.Contains(subQuery)));
						}
					}
				}
			}

			if (isActive.HasValue)
			{
				if (isActive.Value)
					users = users.Where(u => u.ExpiryDate == null);
				else
					users = users.Where(u => u.ExpiryDate != null);
			}

            var totalCount = users.Count();
            users = Order(users, orderBy, orderDirection);

            if (skip.HasValue)
            {
                users = users.Skip(skip.Value);
            }
            if (limit.HasValue && limit.Value > 0)
            {
                users = users.Take(limit.Value);
            }
            var partialUsers = users.ToPartialList(totalCount, orderBy);
            if (includeParticipations)
            {
                partialUsers.Items.ForEach(u => {
                    u.Participations = Context.SearchableParticipants.Where(p => p.UserId == u.Id).ToList();                    
                });                
            }
            return partialUsers;
        }

        private IEnumerable<int> GetCustomerIdHierarchy(int customerId)
        {
            yield return customerId;

            var childIds = Context.Customers.Where(c => c.ParentId == customerId).Select(c => c.Id).ToList();
            foreach (var childId in childIds)
            {
                var children = GetCustomerIdHierarchy(childId);
                foreach (var child in children)
                {
                    yield return child;
                }    
            }
        }

        private IQueryable<SearchableUser> Order(IQueryable<SearchableUser> users, string orderBy, string orderDirection)
        {
            orderBy = orderBy == null ? "" : orderBy.ToLower();
            orderDirection = orderDirection == null ? "" : orderDirection.ToLower();

            switch (orderBy)
            {
                case "customername":
                    return orderDirection == "desc" ? users.OrderByDescending(c => c.CustomerName) : users.OrderBy(c => c.CustomerName);
                case "email":
                    return orderDirection == "desc" ? users.OrderByDescending(c => c.Email) : users.OrderBy(c => c.Email);
                case "firstname":
                    return orderDirection == "desc" ? users.OrderByDescending(c => c.FirstName) : users.OrderBy(c => c.FirstName);
                case "lastname":
                    return orderDirection == "desc" ? users.OrderByDescending(c => c.LastName) : users.OrderBy(c => c.LastName);
                case "username":
                    return orderDirection == "desc" ? users.OrderByDescending(c => c.UserName) : users.OrderBy(c => c.UserName);
                case "isphoenixuser":
                    return orderDirection == "desc" ? users.OrderByDescending(c => c.IsPhoenixUser) : users.OrderBy(c => c.IsPhoenixUser);
                case "leadingtext11":
                    return orderDirection == "desc" ? users.OrderByDescending(c => c.LeadingText11) : users.OrderBy(c => c.LeadingText11);
                case "leadingtext12":
                    return orderDirection == "desc" ? users.OrderByDescending(c => c.LeadingText12) : users.OrderBy(c => c.LeadingText12);
                case "leadingtext13":
                    return orderDirection == "desc" ? users.OrderByDescending(c => c.LeadingText13) : users.OrderBy(c => c.LeadingText13);
                case "leadingtext14":
                    return orderDirection == "desc" ? users.OrderByDescending(c => c.LeadingText14) : users.OrderBy(c => c.LeadingText14);
                case "leadingtext15":
                    return orderDirection == "desc" ? users.OrderByDescending(c => c.LeadingText15) : users.OrderBy(c => c.LeadingText15);
                case "leadingtext16":
                    return orderDirection == "desc" ? users.OrderByDescending(c => c.LeadingText16) : users.OrderBy(c => c.LeadingText16);
                case "leadingtext17":
                    return orderDirection == "desc" ? users.OrderByDescending(c => c.LeadingText17) : users.OrderBy(c => c.LeadingText17);
                case "numberofparticipations":
                    return orderDirection == "desc" ? users.OrderByDescending(c => c.NumberOfParticipations) : users.OrderBy(c => c.NumberOfParticipations);
                case "numberofenrollments":
                    return orderDirection == "desc" ? users.OrderByDescending(c => c.NumberOfEnrollments) : users.OrderBy(c => c.NumberOfEnrollments);
                case "latestcoursedate":
                    return orderDirection == "desc" ? users.OrderByDescending(c => c.LatestCourseDate) : users.OrderBy(c => c.LatestCourseDate);
                case "latestenrollmentdate":
                    return orderDirection == "desc" ? users.OrderByDescending(c => c.LatestEnrollmentDate) : users.OrderBy(c => c.LatestEnrollmentDate);
                default:
                    return orderDirection == "desc" ? users.OrderByDescending(c => c.Id) : users.OrderBy(c => c.Id);
            }
        }
    }
}