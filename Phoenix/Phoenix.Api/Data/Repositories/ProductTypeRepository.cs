﻿using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class ProductTypeRepository : RepositoryBase, IProductTypeRepository
    {
        public ProductTypeRepository(MetierLmsContext context) : base(context)
        {
        }

        public List<ProductType> GetProductTypes()
        {
            return Context.ProductTypes.ToList();
        }
    }
}