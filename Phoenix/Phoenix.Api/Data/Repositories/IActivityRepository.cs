﻿using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface IActivityRepository
    {
        SearchableActivity FindSearchableById(int id);
        
        PartialList<SearchableActivity> Find(int? distributorId, int? customerArticleId, int? customerId, int? activitySetId, int? articleOriginalId, bool? isProposed, bool? hasFixedPrice, string query, string orderBy, string orderDirection, int? skip, int? limit);
        PartialList<SearchableActivityWithActivitySet> FindWithActivitySets(int? distributorId, int? customerArticleId, int? customerId, int? activitySetId, int? articleOriginalId, bool? isProposed, bool? hasFixedPrice, string query, string orderBy, string orderDirection, int? skip, int? limit);

        Activity Create(Activity activity, int copyFromActivityId = 0);
        Activity Update(int id, Activity activity);
        void AddParticipant(Activity activity, Participant participant);
        bool ActivityLockedInOrder(Activity activity);
        Order GetCurrentOrderForActivity(Activity activity);
        Order GetCurrentOrderForActivity(int activityId);
    }
}