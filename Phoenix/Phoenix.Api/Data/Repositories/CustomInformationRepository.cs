﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Extensions;
using Microsoft.Ajax.Utilities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class CustomInformationRepository : RepositoryBase, ICustomInformationRepository
    {
        public CustomInformationRepository(MetierLmsContext context) : base(context)
        {
        }

        public IEnumerable<CustomInformation> GetCustomInformation(string entityType, int entityId)
        {
            return
                Context.CustomInformationSet.Where(c => c.EntityId == entityId && c.EntityType.Equals(entityType))
                    .OrderBy(c => c.Id)
                    .ToList();
        }

        public IEnumerable<CustomInformation> Save(IEnumerable<CustomInformation> customInformationSet, string entityType, int entityId)
        {
            if(customInformationSet == null)
                return null;

            //To avoid possible multiple iterations of IEnumerable
            var customInformationList = customInformationSet as IList<CustomInformation> ?? customInformationSet.ToList(); 

            if (string.IsNullOrWhiteSpace(entityType) || !ValidEntityTypes().Contains(entityType.ToLower()))
                throw new ValidationException("Invalid entitytype");

            if (entityId <= 0)
                throw new ValidationException("Invalid entityId");

            foreach (var customInfo in customInformationList)
            {
                if (customInfo.EntityId != entityId || !entityType.Equals(customInfo.EntityType, StringComparison.OrdinalIgnoreCase))
                    throw new ValidationException("Invalid entityId or type provided for custom information object.");

                customInfo.ConfigureDbContextForEntry(Context);
            }

            //Remove deleted rows
            var dbCustomerInfoSet = GetCustomInformation(entityType, entityId);
            foreach (var item in dbCustomerInfoSet)
            {
                if (customInformationList.All(c => c.Id != item.Id))
                    Context.CustomInformationSet.Remove(item);
            }

            Context.SaveChanges();

            return customInformationList;
        }


        public void Delete(int id)
        {
            var customInformation = Context.CustomInformationSet.FirstOrDefault(c => c.Id == id);
            if(customInformation != null)
            {
                Context.CustomInformationSet.Remove(customInformation);
                Context.SaveChanges();
            }
        }
        public void DeleteAll(string entityType, int entityId)
        {
            var customInformationSet = Context.CustomInformationSet.Where(c => c.EntityId == entityId && c.EntityType.Equals(entityType));

            foreach (var item in customInformationSet)
            {
                Context.CustomInformationSet.Remove(item);
            }

            Context.SaveChanges();
        }

        private List<string> ValidEntityTypes()
        {
            return new List<string> {"customerarticle", "activity"};
        }

    }
}