﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Extensions;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class ResourceRepository : RepositoryBase, IResourceRepository
    {
        public ResourceRepository(MetierLmsContext context) : base(context)
        {}

        public Resource FindById(int id)
        {
            var resource = Context.Resources.WithAll().SingleOrDefault(r => r.Id == id);
            return LoadNavigationPropertiesOfDerivedTypes(resource);
        }

        public Resource Create(Resource resource)
        {
            resource.Created = resource.LastModified = DateTime.UtcNow;

            resource.Currency = null;

            var userResource = resource as UserResource;
            if (userResource != null)
            {
                userResource.User = null;
            }

            Context.Resources.Add(resource);
            Context.SaveChanges();

            return FindById(resource.Id);
        }

        public Resource Update(Resource resource)
        {
            var dbResource = FindById(resource.Id);

            if (!resource.RowVersion.SequenceEqual(dbResource.RowVersion))
            {
                throw new DbUpdateConcurrencyException("There is a newer record of resource in the database.");
            }

            Context.Entry(dbResource).CurrentValues.SetValues(resource);

            resource.UpdateRelatedEntities(dbResource, r => r.Attachments, Context);

            foreach (var resourceAddress in resource.Addresses)
            {
                var dbAddress = dbResource.Addresses.FirstOrDefault(a => a.Id == resourceAddress.Id);
                if (dbAddress == null)
                {
                    dbResource.Addresses.Add(resourceAddress);
                }
                else
                {
                    Context.Entry(dbAddress).CurrentValues.SetValues(resourceAddress);
                }
            }
            
            Context.SaveChanges();
            return FindById(resource.Id);
        }

        public PartialList<Resource> GetResources(string query, string[] searchColumns, int? type, int? skip, int? limit, string orderBy, string orderDirection)
        {
            var resources = Context.Resources.WithResourceType().WithCurrency();

            if (type.HasValue)
            {
                resources = resources.Where(r => r.ResourceTypeId == type.Value);
            }

            if (!string.IsNullOrEmpty(query))
            {
                foreach (var subQuery in query.Split(' '))
                {
                    var q = subQuery;
                    if (searchColumns.Length == 0)
                    {
                        // Search all columns
                        resources = resources.Where(r => (r.Name.Contains(q) || r.Contact.Contains(q) || r.ResourceType.Name.Contains(q)));
                    }
                    else
                    {
                        // Search only columns included in the searchColumns array
                        resources = resources.Where(r => (
                            (searchColumns.Contains("name") && r.Name.Contains(q)) ||
                            (searchColumns.Contains("contact") && r.Contact.Contains(q)) ||
                            (searchColumns.Contains("resourcetype") && r.ResourceType.Name.Contains(q))
                            ));
                    }   
                }
            }
            var totalCount = resources.Count();

            resources = Order(resources, orderBy, orderDirection);
            if (skip.HasValue)
            {
                resources = resources.Skip(skip.Value);
            }
            if (limit.HasValue && limit.Value > 0)
            {
                resources = resources.Take(limit.Value);
            }
            return resources.ToList().ToPartialList(totalCount);
        }

        private Resource LoadNavigationPropertiesOfDerivedTypes(Resource resource)
        {
            var userResource = resource as UserResource;
            if (userResource != null)
            {
                return Context.Resources.Where(r => r.Id == userResource.Id).OfType<UserResource>().Include(r => r.User).WithAll().FirstOrDefault();
            }
            return resource;
        }

        private IQueryable<Resource> Order(IQueryable<Resource> resources, string orderBy, string orderDirection)
        {
            orderBy = orderBy == null ? "" : orderBy.ToLower();
            orderDirection = orderDirection == null ? "" : orderDirection.ToLower();

            switch (orderBy)
            {
                case "name":
                    return orderDirection == "desc" ? resources.OrderByDescending(r => r.Name) : resources.OrderBy(r => r.Name);
                case "resourcetypename":
                    return orderDirection == "desc" ? resources.OrderByDescending(r => r.ResourceType.Name) : resources.OrderBy(r => r.ResourceType.Name);
                case "contact":
                    return orderDirection == "desc" ? resources.OrderByDescending(r => r.Contact) : resources.OrderBy(r => r.Contact);
                case "currency":
                    return orderDirection == "desc" ? resources.OrderByDescending(r => r.Currency.Value) : resources.OrderBy(r => r.Currency.Value);
                case "amount":
                    return orderDirection == "desc" ? resources.OrderByDescending(r => r.Amount) : resources.OrderBy(r => r.Amount);
                default:
                    return orderDirection == "desc" ? resources.OrderByDescending(r => r.Id) : resources.OrderBy(r => r.Id);
            }
        }
    }
}