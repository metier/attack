﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface ICustomerRepository
    {
        Customer FindById(int id);
        Customer Create(Customer customer);
        List<CustomLeadingText> Create(List<CustomLeadingText> entities);
        Customer Update(int id, Customer update);
        bool Delete(Customer customer);
        List<CustomLeadingText> GetLeadingTexts(int customerId);
        Customer FindbyExternalCustomerId(int distributorId, string externalCustomerId);
        Customer FindbyCompanyRegistrationNumber(int distributorId, bool isArbitrary, string companyRegistrationNumber);

        PartialList<SearchableCustomer> GetCustomers(int? distributorId = null, string query = null, int[] customerIdFilter = null, string orderBy = null, string orderDirection = null, int? skip = null, int? limit = null);
    }
}