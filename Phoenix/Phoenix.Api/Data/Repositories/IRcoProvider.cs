﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface IRcoProvider
    {
        PartialList<SearchableRco> GetCourses(string query, string orderBy, string orderDirection, int? skip, int? limit);
        PartialList<SearchableRco> GetExamContainers(string query, string orderBy, string orderDirection, int? skip, int? limit);
        RcoCourse GetCourse(int courseRcoId);
        RcoExamContainer GetExamContainer(int id);

        IEnumerable<Rco> GetCourseRcos(int rcoId, bool includeCourse = false);
        IEnumerable<Rco> GetExamContainerRcos(int rcoId, bool includeContainer = false);
        Rco GetTopParent(int rcoId);
        PartialList<SearchableRco> SearchRcos(string query, int[] rcoIds, string orderBy, string orderDirection, int? skip, int? limit);
    }
}