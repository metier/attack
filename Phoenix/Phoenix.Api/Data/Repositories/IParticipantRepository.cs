﻿using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;
using System.Collections.Generic;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface IParticipantRepository
    {
        Participant Create(Participant participant);
        Participant Update(int id, Participant participant);
        Participant FindById(int id, bool includeOrderInformation = false);
        SearchableParticipant FindSearchableParticipantById(int participantId);
        PartialList<SearchableParticipant> Find(string query, int? userId, int? customerId, int? activitySetId, List<int> activityIds, bool includeParticipantInfoElements, string orderBy, string orderDirection, int skip, int limit, bool? isInvoiced, int? articleTypeId = null, bool? participantIsAvailable = null, bool includeParticipantsEnrolledInActivitySetButThroughOtherActivitySet = false);
        void Delete(Participant participant);
        Order GetCurrentOrderForParticipant(int id);
    }
}