﻿using System;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class AuthenticationTokenRepository : RepositoryBase, IAuthenticationTokenRepository
    {
        public AuthenticationTokenRepository(MetierLmsContext context) : base(context)
        {
        }

        public AuthenticationToken FindByToken(string token)
        {
            Guid tokenGuid;
            var isGuid = Guid.TryParse(token, out tokenGuid);
            if (!isGuid)
            {
                return null;
            }

            return Context.AuthenticationTokens.Single(a => a.Token == tokenGuid);
        }
    }
}