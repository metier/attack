﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface IProductTypeRepository
    {
        List<ProductType> GetProductTypes();
    }
}