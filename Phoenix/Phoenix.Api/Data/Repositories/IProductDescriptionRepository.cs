﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface IProductDescriptionRepository
    {
        ProductDescription FindById(int id, bool withoutReferences, bool includeDeleted = false);
        ProductDescription Create(ProductDescription productDescription);
        ProductDescription Update(int id, ProductDescription productDescription);
        List<ProductDescription> Find(int productId, int? customerId, bool genericOnly);
        List<ProductDescriptionSimple> FindSimple(int productId, int? customerId, bool genericOnly);
    }
}