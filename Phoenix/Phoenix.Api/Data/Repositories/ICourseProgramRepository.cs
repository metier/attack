﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface ICourseProgramRepository
    {
        IEnumerable<CourseProgram> Find(int customerId);
        IEnumerable<Product> GetProductsInCourseProgram(int courseprogramId);

        CourseProgram Get(int id);
        CourseProgram Create(CourseProgram courseProgram);
        CourseProgram Update(int id, CourseProgram courseProgram);
    }
}