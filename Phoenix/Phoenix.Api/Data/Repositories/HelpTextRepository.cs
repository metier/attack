﻿using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Extensions;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class HelpTextRepository : RepositoryBase, IHelpTextRepository
    {
        public HelpTextRepository(MetierLmsContext context) : base(context)
        {
        }

        public HelpText Get(string section, string name)
        {
            return Context.HelpTexts.SingleOrDefault(ht => ht.Name.ToLower() == name.ToLower() && 
                                                           ht.Section.ToLower() == section.ToLower());            
        }

        public HelpText Create(HelpText helpText)
        {
            Context.HelpTexts.Add(helpText);
            Context.SaveChanges();
            return helpText;
        }

        public HelpText Update(int id, HelpText helpText)
        {
            helpText.ConfigureDbContextForEntry(Context);
            Context.SaveChanges();
            return helpText;
        }
    }
}