﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class DistributorRepository :IDistributorRepository
    {
        private readonly MetierLmsContext _context;

        public DistributorRepository(MetierLmsContext context)
        {
            _context = context;
        }

        public IEnumerable<Distributor> GetDistributors()
        {
            return _context.Distributors;
        }
    }
}