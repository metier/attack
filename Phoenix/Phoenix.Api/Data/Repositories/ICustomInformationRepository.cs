﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface ICustomInformationRepository
    {
        IEnumerable<CustomInformation> GetCustomInformation(string entityType, int entityId);
        IEnumerable<CustomInformation> Save(IEnumerable<CustomInformation> customInformation, string entityType, int entityId);
        void Delete(int id);
        void DeleteAll(string entityType, int entityId);
    }
}