﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Extensions;
using Metier.Phoenix.Core.Data.Queryables;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class CommentsRepository : RepositoryBase, ICommentsRepository
    {
        public CommentsRepository(MetierLmsContext context) : base(context)
        {}

        public Comment FindById(int commentId)
        {
            return Context.Comments.First(c => c.Id == commentId);
        }

        public List<Comment> FindByEntityId(string entity, int entityId)
        {
            return Context.Comments.Where(c => c.Entity.ToLower() == entity.ToLower() && c.EntityId == entityId).WithAll().ToList();
        }

        public List<Comment> FindByEntityIds(string entity, int[] entityIds)
        {
            return Context.Comments.Where(c => c.Entity.ToLower() == entity.ToLower() && entityIds.Contains(c.EntityId)).WithAll().ToList();
        }

        public List<Comment> Delete(int id)
        {
            var comment = Context.Comments.FirstOrDefault(c => c.Id == id);
            if (comment != null)
            {
                var entityId = comment.EntityId;
                var entityString = comment.Entity;

                Context.Comments.Remove(comment);
                Context.SaveChanges();

                return FindByEntityId(entityString, entityId);
            }
            return null;
        }

        public List<Comment> Create(Comment comment)
        {
            comment.Created = comment.LastModified = DateTime.UtcNow;

            Context.Comments.Add(comment);
            Context.SaveChanges();

            return FindByEntityId(comment.Entity, comment.EntityId);
        }

        public List<Comment> Update(Comment comment)
        {
            comment.ConfigureDbContextForEntry(Context);
            Context.SaveChanges();

            return FindByEntityId(comment.Entity, comment.EntityId);
        }
    }
}