﻿using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface ICustomerArticleRepository
    {
        CustomerArticle Create(CustomerArticle customerArticle);
        CustomerArticle Update(CustomerArticle customerArticle);
    }
}