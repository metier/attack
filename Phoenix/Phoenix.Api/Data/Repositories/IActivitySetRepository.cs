﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Interfaces;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Utilities.Collections;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface IActivitySetRepository
    {
        ActivitySet FindById(int id);
        IEnumerable<ActivitySet> GetCourseCalendar(int customerId, bool calculateAvailableSpots, bool includeResources);
        ActivitySet Create(ActivitySet activitySet);
        ActivitySet Update(int id, ActivitySet activitySet);
        PartialList<ActivitySet> Find(int? customerId, string query, string orderBy, string orderDirection, int? instructorUserId, int? ecoachUserId, bool calculateAvailableSpots, int? skip, int? limit);
    }
}