﻿using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Extensions;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Exceptions;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class ProductRepository : RepositoryBase, IProductRepository
    {
        public ProductRepository(MetierLmsContext context) : base(context)
        {
        }

        public Product Get(int id)
        {
            var product = Context.Products.Where(p => p.Id == id).WithProductCategory().WithProductType().FirstOrDefault();
            WriteProductDescriptionCount(product);
            return product;
        }

        private void WriteProductDescriptionCount(Product product)
        {
            product.NumberOfProductDescriptions = Context.ProductDescriptions.Count(p => !p.IsDeleted && p.ProductId == product.Id);
            product.NumberOfArticles = Context.SearchableArticles.Count(a => a.ProductId == product.Id);
        }

        public List<Product> GetAll()
        {
            var allProducts = Context.Products.WithProductCategory().WithProductType().ToList();
            var sorted = allProducts.OrderBy(p => p.ProductPath).ToList();
            foreach (var product in sorted)
            {
                WriteProductDescriptionCount(product);
            }
            return sorted;
        }
        public List<Product> Search(string query)
        {
            var allProducts = Context.Products.WithProductCategory().WithProductType();

            if (!string.IsNullOrEmpty(query))
            {
                foreach (string subQuery in query.Split(' '))
                {
                    var q = subQuery;
                    allProducts = allProducts.Where(p => string.Concat(p.ProductCategory.ProductCodePrefix, p.ProductNumber).Contains(q));
                }
            }

            var sorted = allProducts.OrderBy(p => p.ProductCategory.ProductCodePrefix).ThenBy(p=> p.ProductNumber).ToList();
            return sorted;
        }

        public Product Create(Product product)
        {
            ValidateNoOtherActiveProducts(product);
            Context.Products.Add(product);
            Context.SaveChanges();
            return product;
        }

        public Product Update(int id, Product product)
        {
            ValidateNoOtherActiveProducts(product);
            product.ConfigureDbContextForEntry(Context);
            Context.SaveChanges();
            return product;
        }

        private void ValidateNoOtherActiveProducts(Product product)
        {
            if (product.IsActive)
            {
                var similarActiveProducts = Context.Products.Where(p => p.ProductNumber == product.ProductNumber &&
                                                                        p.ProductCategoryId == product.ProductCategoryId && 
                                                                        p.Id != product.Id &&
                                                                        p.IsActive);
                if (similarActiveProducts.Any())
                {
                    if (product.ProductCategory == null)
                    {
                        // Load product category in order to show correct product path in error message
                        product.ProductCategory = Context.ProductCategories.FirstOrDefault(p => p.Id == product.ProductCategoryId);
                    }
                    throw new ValidationException("There can only be one active product with product number " + product.ProductPath);
                }
            }
        }

        public ProductType GetProductTypeByProductId(int id)
        {
            return Context.Products.Where(p => p.Id == id).Select(p => p.ProductType).WithProductTemplateParts().FirstOrDefault();
        }
    }
}