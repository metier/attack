using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.Extensions;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Core.Utilities.Collections;
using System.Data.Entity.Infrastructure;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class CustomerRepository : RepositoryBase, ICustomerRepository
    {
        private readonly IValidate<Customer> _customerValidator;

        public CustomerRepository(MetierLmsContext context, IValidate<Customer> customerValidator) : base(context)
        {
            _customerValidator = customerValidator;
        }

        public PartialList<SearchableCustomer> GetCustomers(int? distributorId = null, string query = null, int[] customerIdFilter = null, string orderBy = null, string orderDirection = null, int? skip = null, int? limit = null)
        {
            IQueryable<SearchableCustomer> customers = Context.SearchableCustomers;
            if (customerIdFilter != null)
            {
                customers = customers.Where(c => customerIdFilter.Contains(c.Id));
            }
            if (distributorId.HasValue)
            {
                customers = customers.Where(c => c.DistributorId == distributorId.Value);
            }
            
            if (!string.IsNullOrEmpty(query))
            {
                foreach (string subQuery in query.Split(' '))
                {
                    var q = subQuery;
                    customers = customers.Where(c => c.Name.Contains(q) || c.ParentName.Contains(q) || c.Language.Contains(q));
                }
            }

            var totalCount = customers.Count();
            customers = Order(customers, orderBy, orderDirection);

            if (skip.HasValue)
            {
                customers = customers.Skip(skip.Value);
            }
            if (limit.HasValue && limit.Value > 0)
            {
                customers = customers.Take(limit.Value);
            }
            
            return customers.ToPartialList(totalCount, orderBy);
        }

		private IQueryable<SearchableCustomer> Order(IQueryable<SearchableCustomer> customers, string orderBy, string orderDirection)
        {
            orderBy = orderBy == null ? "" : orderBy.ToLower();
            orderDirection = orderDirection == null ? "" : orderDirection.ToLower();

            switch (orderBy)
            {
                case "name":
                    return orderDirection == "desc" ? customers.OrderByDescending(c => c.Name) : customers.OrderBy(c => c.Name);
                case "parentname":
                    return orderDirection == "desc" ? customers.OrderByDescending(c => c.ParentName) : customers.OrderBy(c => c.ParentName);
                case "language":
                    return orderDirection == "desc" ? customers.OrderByDescending(c => c.Language) : customers.OrderBy(c => c.Language);
                case "isarbitrary":
                    return orderDirection == "desc" ? customers.OrderByDescending(c => c.IsArbitrary) : customers.OrderBy(c => c.IsArbitrary);
                default:
                    return orderDirection == "desc" ? customers.OrderByDescending(c => c.Id) : customers.OrderBy(c => c.Id);
            }
        }

        public Customer FindById(int id)
        {
            return Context.Customers.Where(c => c.Id == id).WithAll().FirstOrDefault();
        }

        public Customer FindbyExternalCustomerId(int distributorId, string externalCustomerId)
        {
            return Context.Customers.Where(c => c.ExternalCustomerId == externalCustomerId && c.DistributorId == distributorId).WithAll().FirstOrDefault();
        }

        public Customer FindbyCompanyRegistrationNumber(int distributorId, bool isArbitrary, string companyRegistrationNumber)
        {
            return Context.Customers.Where(c => c.CompanyRegistrationNumber == companyRegistrationNumber && c.IsArbitrary == isArbitrary && c.DistributorId == distributorId).WithAll().FirstOrDefault();
        }

        public Customer Create(Customer entity)
        {
            RemoveEmptyEntries(entity);
            Context.Customers.Add(entity);

            if (entity.ParticipantInfoDefinition != null && !entity.ParticipantInfoDefinition.CompetitionMode.HasValue)
            {
                entity.ParticipantInfoDefinition.CompetitionMode = CompetitionModeTypes.None;
            }

            var validationResults = _customerValidator.Validate(entity).ToList();
            if (validationResults.Count > 0)
            {
                throw new ValidationException("Invalid customer", validationResults);
            }

            Context.SaveChanges();
            return entity;
        }

        public bool Delete(Customer customer)
        {
            Context.Customers.Remove(customer);
            Context.SaveChanges();

            return true;
        }

        public List<CustomLeadingText> Create(List<CustomLeadingText> entities)
        {
            foreach (var customLeadingText in entities)
            {
                Context.CustomLeadingTexts.Add(customLeadingText);
            }

            Context.SaveChanges();
            return entities;
        }

		public Customer Update(int id, Customer update)
		{

			RemoveEmptyEntries(update);

			if (update.DiplomaLogoAttachment != null)
			{
				if (!update.DiplomaLogoAttachment.ShareGuid.HasValue)
					update.DiplomaLogoAttachment.ShareGuid = Guid.NewGuid();

				Context.Attachments.Add(update.DiplomaLogoAttachment);
				Context.SaveChanges();
				update.DiplomaLogoAttachmentId = update.DiplomaLogoAttachment.Id;
				update.DiplomaLogoAttachment = null;
			}

			if (update.DiplomaSignatureAttachment != null)
			{
				if (!update.DiplomaSignatureAttachment.ShareGuid.HasValue)
					update.DiplomaSignatureAttachment.ShareGuid = Guid.NewGuid();

				Context.Attachments.Add(update.DiplomaSignatureAttachment);
				Context.SaveChanges();
				update.DiplomaSignatureAttachmentId = update.DiplomaSignatureAttachment.Id;
				update.DiplomaSignatureAttachment = null;
			}


			foreach (var entity in update.Addresses)
			{
				entity.ConfigureDbContextForEntry(Context);
			}
			foreach (var entity in update.MetierContactPersons)
			{
				entity.ConfigureDbContextForEntry(Context);
			}
			foreach (var entity in update.CustomerContactPersons)
			{
				entity.ConfigureDbContextForEntry(Context);
			}

			if (update.CustomerInvoicing != null)
			{
				update.CustomerInvoicing.ConfigureDbContextForEntry(Context);
			}
			if (update.ParticipantInfoDefinition != null)
			{
				if (update.ParticipantInfoDefinition.AcceptedEmailAddresses != null)
				{
					foreach (var acceptedEmailAddress in update.ParticipantInfoDefinition.AcceptedEmailAddresses)
					{
						acceptedEmailAddress.ConfigureDbContextForEntry(Context);
					}
				}
				update.ParticipantInfoDefinition.ConfigureDbContextForEntry(Context);

				if (!update.ParticipantInfoDefinition.CompetitionMode.HasValue)
				{
					update.ParticipantInfoDefinition.CompetitionMode = CompetitionModeTypes.None;
				}
			}

			if (update.AutomailDefinition != null)
			{
				update.AutomailDefinition.ConfigureDbContextForEntry(Context);
			}

			foreach (var entity in update.CustomLeadingTexts)
			{
				entity.CustomerId = id;
				entity.ConfigureDbContextForEntry(Context);
			}


			

			update.ConfigureDbContextForEntry(Context);

			var validationResults = _customerValidator.Validate(update).ToList();
			if (validationResults.Count > 0)
			{
				throw new ValidationException("Invalid customer", validationResults);
			}

			Context.SaveChanges();
            return update;
        }

        private void RemoveEmptyEntries(Customer customer)
        {
            customer.CustomerContactPersons?.RemoveAll(cp => cp.IsEmptyNew());
            customer.ParticipantInfoDefinition?.AcceptedEmailAddresses?.RemoveAll(cp => cp.IsEmptyNew());
        }

        public List<CustomLeadingText> GetLeadingTexts(int customerId)
        {
            var leadingTexts = Context.CustomLeadingTexts
                                 .Include(clt => clt.LeadingText)
                                 .Where(clt => clt.Customer.Id == customerId)
                                 .ToList();
            
            return leadingTexts;
        }
    }
}