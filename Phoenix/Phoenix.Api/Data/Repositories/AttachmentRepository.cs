﻿using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class AttachmentRepository : RepositoryBase, IAttachmentRepository
    {
        public AttachmentRepository(MetierLmsContext context) : base(context)
        {
        }

        public Attachment Create(Attachment attachment)
        {
            Context.Attachments.Add(attachment);
            Context.SaveChanges();

            return attachment;
        }
    }
}