﻿using System;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class ScormAttemptRepository : RepositoryBase, IScormAttemptRepository
    {
        public ScormAttemptRepository(MetierLmsContext context) : base(context){}

        public ScormAttempt Create(ScormAttempt attempt)
        {
            attempt.StartTime = DateTime.UtcNow;
            Context.ScormAttempts.Add(attempt);
            Context.SaveChanges();
            return attempt;
        }
    }
}