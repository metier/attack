﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface ICommentsRepository
    {
        Comment FindById(int commentId);
        List<Comment> FindByEntityId(string entity, int entityId);
        List<Comment> FindByEntityIds(string entity, int[] entityIds);
        List<Comment> Delete(int id);
        List<Comment> Create(Comment comment);
        List<Comment> Update(Comment comment);
    }
}