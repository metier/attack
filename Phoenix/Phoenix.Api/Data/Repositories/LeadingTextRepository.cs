using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class LeadingTextRepository : ILeadingTextRepository
    {
        private readonly MetierLmsContext _context;

        public LeadingTextRepository(MetierLmsContext context)
        {
            _context = context;
        }

        public List<LeadingText> GetLeadingTexts()
        {
            return _context.LeadingTextes.ToList();
        }
    }
}