using Metier.Phoenix.Core.Data;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public abstract class RepositoryBase
    {
        protected  MetierLmsContext Context;

        protected RepositoryBase(MetierLmsContext context)
        {
            Context = context;
        }
    }
}