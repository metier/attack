﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Api.Messaging;
using Metier.Phoenix.Api.Messaging.Events;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.Extensions;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Core.Utilities;
using Metier.Phoenix.Core.Utilities.Collections;
using Metier.Phoenix.Core.Business;
using System.Security.Cryptography;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class ParticipantRepository : RepositoryBase, IParticipantRepository
    {
        private readonly IValidateParticipant _participantValidator;
        private readonly IEventAggregator _eventAggregator;
        private readonly IParticipantBusiness _participantBusiness;

        public ParticipantRepository(MetierLmsContext context, IValidateParticipant participantValidator, IParticipantBusiness participantBusiness, IEventAggregator eventAggregator) : base(context)
        {
            _participantValidator = participantValidator;
            _participantBusiness = participantBusiness;
            _eventAggregator = eventAggregator;
        }

        public Participant Create(Participant participant)
        {
            var validationResults = _participantValidator.Validate(participant).ToList();
            if (validationResults.Count > 0)
            {
                throw new ValidationException("Invalid participant", validationResults);
            }

            participant.Attachments = new List<Attachment>();
            Context.Participants.Add(participant);
            Context.SaveChanges();
            _eventAggregator.Publish(new ParticipantStatusChangedEvent(participant.Id, string.Empty, participant.StatusCodeValue));
            return FindById(participant.Id);
        }

        public Participant Update(int id, Participant participant)
        {
            var dbParticipant = FindById(id);

            var oldStatus = dbParticipant.StatusCodeValue;

            // Do not allow manual change of exam grade
            var examGrade = dbParticipant.ExamGrade;
            participant.ExamGrade = examGrade;

            var currentOrder = GetCurrentOrderForParticipant(id);
            var validationResults = _participantValidator.Validate(participant, dbParticipant, currentOrder).ToList();
            if (validationResults.Count > 0)
            {
                throw new ValidationException("Invalid participant", validationResults);
            }

            if (!participant.RowVersion.SequenceEqual(dbParticipant.RowVersion))
            {
                throw new DbUpdateConcurrencyException("There is a newer record of the participant in the database.");
            }

            Context.Entry(dbParticipant).CurrentValues.SetValues(participant);
            participant.UpdateRelatedEntities(dbParticipant, a => a.ParticipantInfoElements, Context);
            Context.SaveChanges();

            _eventAggregator.Publish(new ParticipantStatusChangedEvent(participant.Id, oldStatus, dbParticipant.StatusCodeValue));

            return FindById(id, true);
        }

        public Participant FindById(int id, bool includeOrderInformation = false)
        {
            if(includeOrderInformation)
                return Context.Participants.WithParticipantInfoElements().WithAttachments().WithOrderInformation().FirstOrDefault(p => p.Id == id && !p.IsDeleted).IncludeQualificationStatus(Context);

            return Context.Participants.WithParticipantInfoElements().WithAttachments().FirstOrDefault(p => p.Id == id && !p.IsDeleted).IncludeQualificationStatus(Context);
        }

        public SearchableParticipant FindSearchableParticipantById(int participantId)
        {
            return Context.SearchableParticipants.FirstOrDefault(p => p.Id == participantId);
        }

        public PartialList<SearchableParticipant> Find(string query, int? userId, int? customerId, int? activitySetId, List<int> activityIds, bool includeParticipantInfoElements, string orderBy, string orderDirection, int skip, int limit, bool? isInvoiced, int? articleTypeId = null, bool? participantIsAvailable = null, bool includeParticipantsEnrolledInActivitySetButThroughOtherActivitySet = false)
        {
            IQueryable<SearchableParticipant> participants = Context.SearchableParticipants;
			
            if (userId.HasValue)
            {
                participants = participants.Where(p => p.UserId == userId.Value);
            }

            if (customerId.HasValue)
            {
                participants = participants.Where(p => p.CustomerId == customerId.Value);
            }
			if (activitySetId.HasValue) 
			{
                if (includeParticipantsEnrolledInActivitySetButThroughOtherActivitySet)
                {
                    //Get all UserIds
                    var enrolledUserIds = Context.Enrollments.Where(e => e.ActivitySetId == activitySetId.Value && e.IsCancelled == false).Select(e => e.UserId).ToList();                    
                    var activitySetActivityIds = Context.ActivitySets.WithActivities().First(a=> a.Id == activitySetId.Value).Activities.Select(a => a.Id).ToList();
                    participants = participants.Where(p => enrolledUserIds.Contains(p.UserId) && activitySetActivityIds.Contains(p.ActivityId));
                }
                else
                {
                    participants = participants.Where(p => p.ActivitySetId == activitySetId.Value);
                }
			}
			if (activityIds?.Any() == true) 
			{
				participants = participants.Where(p => activityIds.Contains(p.ActivityId));
			}
            if (isInvoiced.HasValue)
            {
                participants = participants.Where(p => p.IsInvoiced == isInvoiced.Value);
            }

            if (articleTypeId.HasValue)
                participants = participants.Where(p => p.ArticleTypeId == articleTypeId.Value);

            if (!string.IsNullOrEmpty(query))
            {
                foreach (var subQuery in query.Split(' '))
                {
                    var q = subQuery;
                    participants =
                        participants.Where(p => p.ActivityName.Contains(q) || 
                                                p.ActivitySetName.Contains(q) || 
                                                p.FirstName.Contains(q) ||
                                                p.LastName.Contains(q));
                }
            }

            participants = Order(participants, orderBy, orderDirection);

            var participantList = participants.ToList();
            if (participantIsAvailable.HasValue)
            {
                participantList = participantList.Where(p =>
                {
                    return _participantBusiness.GetAvailability(p.Id).IsAvailable == participantIsAvailable.Value;
                }).ToList();
            }

            var totalCount = participantList.Count();
            participantList = participantList.Skip(skip).ToList();

            if (limit > 0)            
                participantList.Take(limit);            

            if (includeParticipantInfoElements)
            {
                participantList.ForEach(p => {
                    p.ParticipantInfoElements = FindById(p.Id).ParticipantInfoElements;
                });
            }

            return participantList.ToPartialList(totalCount, orderBy);			            
        }

        public void Delete(Participant participant)
        {
            var currentOrder = GetCurrentOrderForParticipant(participant.Id);

            if (currentOrder != null)
            {
                if (!currentOrder.IsCreditNote)
                {
                    throw new ValidationException(string.Format("Participant with ID {0} has already been added to an order and cannot be deleted.", participant.Id));
                }

                if (!OrderHasBeenSentToInvoicing(currentOrder.Status))
                {
                    throw new ValidationException(
                        string.Format(
                            "Participant with ID {0} has been added to a credit note not yet transferred to accounting system. The participant cannot be deleted.",
                            participant.Id));
                }
            }

            participant.IsDeleted = true;
        }

        public Order GetCurrentOrderForParticipant(int participantId)
        {
            return Context.Orders.Where(o => o.ParticipantOrderLines.Any(ol => ol.Participant.Id == participantId)).OrderByDescending(o => o.Id).FirstOrDefault();
        }

        private bool OrderHasBeenSentToInvoicing(OrderStatus status)
        {
            return status == OrderStatus.Transferred ||
                   status == OrderStatus.NotInvoiced ||
                   status == OrderStatus.Invoiced ||
                   status == OrderStatus.Rejected;
        }

        private IQueryable<SearchableParticipant> Order(IQueryable<SearchableParticipant> participants, string orderBy, string orderDirection)
        {
            orderBy = orderBy == null ? "" : orderBy.ToLower();
            orderDirection = orderDirection == null ? "" : orderDirection.ToLower();

            switch (orderBy)
            {
                case "participantid":
                    return orderDirection == "desc" ? participants.OrderByDescending(p => p.Id) : participants.OrderBy(p => p.Id);
                case "firstname":
                    return orderDirection == "desc" ? participants.OrderByDescending(p => p.FirstName) : participants.OrderBy(p => p.FirstName);
                case "lastname":
                    return orderDirection == "desc" ? participants.OrderByDescending(p => p.LastName) : participants.OrderBy(p => p.LastName);
                case "unitprice":
                    return orderDirection == "desc" ? participants.OrderByDescending(p => p.UnitPrice) : participants.OrderBy(p => p.UnitPrice);
                case "activitysetname":
                    return orderDirection == "desc" ? participants.OrderByDescending(p => p.ActivitySetName) : participants.OrderBy(p => p.ActivitySetName);
                case "activityname":
                    return orderDirection == "desc" ? participants.OrderByDescending(p => p.ActivityName) : participants.OrderBy(p => p.ActivityName);
                default:
                    return orderDirection == "desc" ? participants.OrderByDescending(p => p.ActivitySetName) : participants.OrderBy(p => p.ActivitySetName);
            }
        }
    }
}