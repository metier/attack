﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Metier.Phoenix.Api.Exceptions;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Extensions;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Exceptions;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public class ProductDescriptionRepository : RepositoryBase, IProductDescriptionRepository
    {
        private readonly IAccountsProvider _accountsProvider;

        public ProductDescriptionRepository(MetierLmsContext context, IAccountsProvider accountsProvider) : base(context)
        {
            _accountsProvider = accountsProvider;
        }

        public ProductDescription FindById(int id, bool withoutReferences, bool includeDeleted = false)
        {
            if (withoutReferences)
            {
                return Context.ProductDescriptions
                    .Where(p => includeDeleted || !p.IsDeleted)
                    .WithProductDescriptionTexts()
                    .WithAttachment()
                    .WithTemplateParts()
                    .WithModifiedBy()
                    .WithCreatedBy()
                    .SingleOrDefault(p => p.Id == id);
            }
            return Context.ProductDescriptions
                .Where(p => includeDeleted || !p.IsDeleted)
                .WithAll()
                .SingleOrDefault(p => p.Id == id);
        }

        public List<ProductDescription> Find(int productId, int? customerId, bool genericOnly)
        {
            return GetProductDescriptions(productId, customerId, genericOnly).WithAll().ToList();
        }

        public List<ProductDescriptionSimple> FindSimple(int productId, int? customerId, bool genericOnly)
        {
            var productDescriptions = GetProductDescriptions(productId, customerId, genericOnly).WithCustomer();
            return productDescriptions.ToList().Select(CreateProductDescriptionSimple).ToList();
        }

        public ProductDescription Create(ProductDescription productDescription)
        {
            if (GetProductDescriptionIdsWithSameLanguage(productDescription).Count > 0)
            {
                throw new LanguageIsAlreadyDefinedException();
            }

            foreach (var entity in productDescription.ProductDescriptionTexts)
            {   
                entity.ProductTemplatePart = null;
            }
            
            productDescription.Created = productDescription.LastModified = DateTime.UtcNow;
            var userAccount = _accountsProvider.GetCurrentUser();
            productDescription.CreatedById = productDescription.ModifiedById = userAccount.User.Id;

            Context.ProductDescriptions.Add(productDescription);
            Context.SaveChanges();
            return FindById(productDescription.Id, true);
        }

        public ProductDescription Update(int id, ProductDescription productDescription)
        {
            var productDescriptionIdsWithSameLanguage = GetProductDescriptionIdsWithSameLanguage(productDescription);
            if (productDescriptionIdsWithSameLanguage.Count >= 1 && !productDescriptionIdsWithSameLanguage.Contains(productDescription.Id))
            {
                throw new LanguageIsAlreadyDefinedException();
            }

            var dbProductDescription = FindById(id, true);

            if (!productDescription.RowVersion.SequenceEqual(dbProductDescription.RowVersion))
            {
                throw new DbUpdateConcurrencyException("There is a newer record of the product description in the database.");
            }

            //Check for "!dbProductDescription.IsDeleted!" is strictly not necessary, but adding it for the code to be "future proof".
            if (productDescription.IsDeleted && !dbProductDescription.IsDeleted)
            {
                if (Context.Articles.Any(a => a.ProductDescriptionId == productDescription.Id && !a.IsDeleted))
                    throw new ValidationException("The product description is in use. Remove it from relevant articles/customer articles before deleting it.");
            }

            productDescription.LastModified = DateTime.UtcNow;
            var userAccount = _accountsProvider.GetCurrentUser();
            productDescription.ModifiedById = userAccount.User.Id;

            if (productDescription.Attachment != null && productDescription.AttachmentId != dbProductDescription.AttachmentId)
            {
                productDescription.Attachment.ConfigureDbContextForEntry(Context);
            }

            productDescription.UpdateRelatedEntities(dbProductDescription, pd => pd.ProductDescriptionTexts, Context);

            Context.Entry(dbProductDescription).CurrentValues.SetValues(productDescription);

            Context.SaveChanges();
            return FindById(id, true);
        }

        private List<int> GetProductDescriptionIdsWithSameLanguage(ProductDescription productDescription)
        {
            var query = Context.ProductDescriptions.Where(pd => !pd.IsDeleted && 
                productDescription.LanguageId == pd.LanguageId &&
                productDescription.ProductId == pd.ProductId &&
                (productDescription.CustomerId.HasValue ? pd.CustomerId == productDescription.CustomerId.Value : !pd.CustomerId.HasValue));
            return query.Select(pd => pd.Id).ToList();
        }

        private static ProductDescriptionSimple CreateProductDescriptionSimple(ProductDescription product)
        {
            var productDescriptionSimple = new ProductDescriptionSimple
            {
                CustomerId = product.CustomerId,
                Id = product.Id,
                LanguageId = product.LanguageId,
                ProductId = product.ProductId,
                Title = product.Title
            };

            if (product.Customer != null)
            {
                productDescriptionSimple.CustomerName = product.Customer.Name;
            }

            return productDescriptionSimple;
        }

        private IQueryable<ProductDescription> GetProductDescriptions(int productId, int? customerId, bool genericOnly)
        {
            var productDescriptions = Context.ProductDescriptions.Where(pd => !pd.IsDeleted && pd.ProductId == productId);
            if (genericOnly)
            {
                productDescriptions = productDescriptions.Where(pd => pd.CustomerId == null);
            }
            else if (customerId.HasValue)
            {
                productDescriptions = productDescriptions.Where(pd => pd.CustomerId == customerId);
            }
            return productDescriptions;
        }
    }
}