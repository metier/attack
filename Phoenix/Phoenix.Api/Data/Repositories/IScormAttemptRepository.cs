﻿using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Repositories
{
    public interface IScormAttemptRepository
    {
        ScormAttempt Create(ScormAttempt attempt);
    }
}