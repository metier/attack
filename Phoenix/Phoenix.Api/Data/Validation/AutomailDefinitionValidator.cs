﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Validation
{
    public class AutomailDefinitionValidator : IValidate<AutomailDefinition>
    {
        public IEnumerable<ValidationResult> Validate(AutomailDefinition item)
        {
            return new List<ValidationResult>();
        }
    }
}