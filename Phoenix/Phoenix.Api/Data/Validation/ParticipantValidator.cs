﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Api.Data.Validation
{
    public class ParticipantValidator : IValidateParticipant
    {
        private readonly MetierLmsContext _context;

        public ParticipantValidator(MetierLmsContext context)
        {
            _context = context;
        }

        public IEnumerable<ValidationResult> Validate(Participant postedParticipant)
        {
            var validationResults = new List<ValidationResult>();
            CheckForUserConsistancy(postedParticipant, validationResults);
            return validationResults;
        }

        public IEnumerable<ValidationResult> Validate(Participant postedParticipant, Participant currentParticipant, Order currentOrder)
        {
            var validationResults = new List<ValidationResult>();

            if (postedParticipant.EnrollmentId != currentParticipant.EnrollmentId)
            {
                validationResults.Add(new ValidationResult("Related enrollment cannot be changed for a participant."));
            }

            CheckForUserConsistancy(postedParticipant, validationResults);

            if (string.IsNullOrEmpty(postedParticipant.Result) && postedParticipant.StatusCodeValue == ParticipantStatusCodes.Completed)
            {
                var customerArticleArticle = GetCustomerArticleArticle(postedParticipant);
                if (customerArticleArticle != null && customerArticleArticle.ArticleTypeId == (int) ArticleTypes.ExternalCertification)
                {
                    validationResults.Add(new ValidationResult("Result must be set on participant before setting status to completed"));
                }
            }

            if (!postedParticipant.IsNotBillable && (postedParticipant.CurrencyCodeId == null || postedParticipant.CurrencyCodeId == 0 || postedParticipant.UnitPrice == null) )
            {
                validationResults.Add(new ValidationResult("Price and currency must be specified for billable participants"));
            }

            if (currentOrder != null && OrderHasBeenCreated(currentOrder))
            {
                if (postedParticipant.IsNotBillable != currentParticipant.IsNotBillable)
                {
                    validationResults.Add(new ValidationResult("Billable status cannot be changed for participation because it already exists on an order"));
                }

                if (postedParticipant.CurrencyCodeId != currentParticipant.CurrencyCodeId)
                {
                    validationResults.Add(new ValidationResult("Currency cannot be changed for participation because it already exists on an order"));
                }

                if (postedParticipant.EarliestInvoiceDate != currentParticipant.EarliestInvoiceDate)
                {
                    validationResults.Add(new ValidationResult("Earliest invoice date cannot be changed for participation because it already exists on an order"));
                }

                if (postedParticipant.OnlinePaymentReference != currentParticipant.OnlinePaymentReference)
                {
                    validationResults.Add(new ValidationResult("Online payment reference cannot be changed for participation because it already exists on an order"));
                }
            }

            return validationResults;
        }

        private void CheckForUserConsistancy(Participant postedParticipant, List<ValidationResult> validationResults)
        {
            var enrollment = _context.Enrollments.FirstOrDefault(e => e.Id == postedParticipant.EnrollmentId);
            if (enrollment != null && enrollment.UserId != postedParticipant.UserId)
                validationResults.Add(
                    new ValidationResult("The user related to the enrollment must be the same as the user on the participant."));
        }

        private Article GetCustomerArticleArticle(Participant participant)
        {
            return _context.Activities.Where(a => a.Id == participant.ActivityId && !a.IsDeleted)
                           .Join(_context.CustomerArticles, a => a.CustomerArticleId, ca => ca.Id, (a, ca) => ca)
                           .Where(ca => !ca.IsDeleted)
                           .Join(_context.Articles, ca => ca.ArticleCopyId, a => a.Id, (ca, a) => a)
                           .FirstOrDefault(a => !a.IsDeleted);
        }

        private bool OrderHasBeenCreated(Order currentOrder)
        {
            if(currentOrder.IsCreditNote || currentOrder.IsFullCreditNote)
                return !(OrderIsTransferred(currentOrder.Status));  //If a creditnote is sent to invoicing, open up the participation for editing.

            return OrderIsTransferred(currentOrder.Status) || currentOrder.Status == OrderStatus.Draft;
        }

        private bool OrderIsTransferred(OrderStatus currentOrderStatus)
        {
            return currentOrderStatus == OrderStatus.Transferred ||
                   currentOrderStatus == OrderStatus.NotInvoiced ||
                   currentOrderStatus == OrderStatus.Invoiced ||
                   currentOrderStatus == OrderStatus.Rejected;
        }
    }
}