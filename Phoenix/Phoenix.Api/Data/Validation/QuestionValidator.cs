﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Validation
{
    public class QuestionValidator : IValidate<Question>
    {
        public IEnumerable<ValidationResult> Validate(Question question)
        {
            if (question.Products.Count == 0)
            {
                yield return new ValidationResult("Products are required");
            }
            if (question.Rcos.Count == 0)
            {
                yield return new ValidationResult("Versions are required");
            }
            if (string.IsNullOrEmpty(question.Text))
            {
                yield return new ValidationResult("Text is required");
            }

            if (string.IsNullOrEmpty(question.Type))
            {
                yield return new ValidationResult("Type is required");
            }
            else if(!QuestionTypes.IsValidType(question.Type))
            {
                yield return new ValidationResult("Invalid type");
            }

            if (question.LanguageId <= 0)
            {
                yield return new ValidationResult("LanguageId is required");
            }

            if (question.Type == QuestionTypes.TrueFalse)
            {
                if (!question.CorrectBoolAnswer.HasValue)
                {
                    yield return new ValidationResult("CorrectBoolAnswer is required");    
                }
            }

            if (question.Type == QuestionTypes.MultipleChoiceSingle || question.Type == QuestionTypes.MultipleChoiceMultiple)
            {
                if (!question.QuestionOptions.Any(o => o.IsCorrect))
                {
                    yield return new ValidationResult("Multiple choice questions must have at least one correct option");
                }
            }

            if (question.Type == QuestionTypes.MultipleChoiceSingle)
            {
                if (question.QuestionOptions.Count(o => o.IsCorrect) > 1)
                {
                    yield return new ValidationResult("The question type does not allow multiple correct options");
                }
            }
        }
    }
}