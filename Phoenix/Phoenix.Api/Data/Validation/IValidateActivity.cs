﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Validation
{
    public interface IValidateActivity
    {
        IEnumerable<ValidationResult> Validate(Activity activity, Article article, Activity currentActivity = null, Order currentOrder = null);
    }
}