﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Validation
{
    public class TagsValidator : IValidate<Tag>
    {
        public IEnumerable<ValidationResult> Validate(Tag tag)
        {
            if (string.IsNullOrEmpty(tag.Text))
            {
                yield return new ValidationResult("Text is required");
            }
        }
    }
}