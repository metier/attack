﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Utilities.Validation;

namespace Metier.Phoenix.Api.Data.Validation
{
    public class ParticipantInfoDefinitionValidator : IValidate<ParticipantInfoDefinition>
    {
        private readonly IValidate<AcceptedEmailAddress> _acceptedEmailAddressValidator;

        public ParticipantInfoDefinitionValidator(IValidate<AcceptedEmailAddress> acceptedEmailAddressValidator)
        {
            _acceptedEmailAddressValidator = acceptedEmailAddressValidator;
        }

        public IEnumerable<ValidationResult> Validate(ParticipantInfoDefinition item)
        {
            var validationResults = new List<ValidationResult>();

            if (item.AcceptedEmailAddresses.Count  > 0)
            {
                foreach (var acceptedEmailAddress in item.AcceptedEmailAddresses)
                {
                    validationResults.AddRange(_acceptedEmailAddressValidator.Validate(acceptedEmailAddress));
                }
            }

            if (!string.IsNullOrEmpty(item.EmailConfirmationAddress) && !EmailValidation.IsValidEmail(item.EmailConfirmationAddress))
            {
                validationResults.Add(new ValidationResult("Invalid e-mail address", new[] { "EmailConfirmationAddress" }));
            }

            return validationResults;
        }
    }
}