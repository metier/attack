﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Api.Data.Validation
{
    public class ActivityValidator : IValidateActivity
    {
        public IEnumerable<ValidationResult> Validate(Activity activity, Article article, Activity currentActivity = null, Order currentOrder = null)
        {
            if (article.ArticleTypeId == (int)ArticleTypes.CaseExam || 
                article.ArticleTypeId == (int)ArticleTypes.ExternalCertification || 
                article.ArticleTypeId == (int)ArticleTypes.ProjectAssignment)
            {
                if (activity.Duration == null && activity.EndDate == null)
                {
                    yield return new ValidationResult("Duration or end date is required");
                }
                if (activity.Duration != null && activity.EndDate != null)
                {
                    yield return new ValidationResult("Duration or end date should be set - not both");
                }
            }
            if (article.ArticleTypeId == (int)ArticleTypes.InternalCertification || article.ArticleTypeId == (int)ArticleTypes.MultipleChoice)
            {
                if (activity.Duration == null)
                {
                    yield return new ValidationResult("Duration is required");
                }
                if (activity.EndDate != null)
                {
                    yield return new ValidationResult("End date is not a valid option for this article type");
                }
            }

            if (article.ArticleTypeId == (int) ArticleTypes.ELearning)
            {
                if (activity.SendProgressAutomail.HasValue && activity.SendProgressAutomail.Value)
                {
                    if (activity.CompletionTargetDuration == null && activity.CompletionTargetDate == null)
                    {
                        yield return new ValidationResult("Duration or due date is required for sending progress automails");
                    }
                    if (activity.CompletionTargetDuration != null && activity.CompletionTargetDate != null)
                    {
                        yield return new ValidationResult("When activating progress automails, duration or due date should be set - not both");
                    }
                }

                if (activity.EndDate != null)
                {
                    yield return new ValidationResult("End date is not a valid option for this article type");
                }

                if (activity.ActivityPeriods == null || activity.ActivityPeriods.Count != 1)
                {
                    yield return new ValidationResult("E-learning activities must have one, and only one activity period");
                }

                if (activity.Duration != null && activity.ActivityPeriods?.Count == 1 && activity.ActivityPeriods?[0]?.End != null)
                {
                    yield return new ValidationResult("Duration or end date should be set - not both");
                }
            }

            if (activity.ActivityPeriods != null)
            {
                foreach (var activityPeriod in activity.ActivityPeriods)
                {
                    if (activityPeriod.Start == null)
                    {
                        yield return new ValidationResult("Period start is required");
                    }

                    if (article.ArticleTypeId == (int)ArticleTypes.Classroom)
                    {
                        if (activityPeriod.End == null)
                        {
                            yield return new ValidationResult("Period end is required");
                        }
                    }
                    
                    if (activityPeriod.Start != null && activityPeriod.End != null)
                    {
                        if (activityPeriod.End < activityPeriod.Start)
                        {
                            yield return new ValidationResult("End cannot be before start");
                        }    
                    }
                }
            }

            if (currentOrder != null && OrderHasBeenCreated(currentOrder.Status))
            {
                if (currentActivity == null)
                {
                    throw new ArgumentNullException("currentActivity");
                }

                if (activity.FixedPrice != currentActivity.FixedPrice)
                {
                    yield return new ValidationResult("Fixed price cannot be changed for activity because it already exists on an order");
                }

                if (activity.CurrencyCodeId != currentActivity.CurrencyCodeId)
                {
                    yield return new ValidationResult("Currency cannot be changed for activity because it already exists on an order");
                }

                if (activity.EarliestInvoiceDate != currentActivity.EarliestInvoiceDate)
                {
                    yield return new ValidationResult("Earliest invoice date cannot be changed for activity because it already exists on an order");
                }
            }

            if (activity.EasyCustomerSpecificCount > activity.EasyQuestionCount)
            {
                yield return new ValidationResult("Cannot have more customer specific easy questions than total number of easy questions");
            }
            if (activity.MediumCustomerSpecificCount > activity.MediumQuestionCount)
            {
                yield return new ValidationResult("Cannot have more customer specific medium questions than total number of medium questions");
            }
            if (activity.HardCustomerSpecificCount > activity.HardQuestionCount)
            {
                yield return new ValidationResult("Cannot have more customer specific hard questions than total number of hard questions");
            }

            if (!activity.FixedPriceNotBillable && activity.FixedPrice == null)
            {
                yield return new ValidationResult("A fixed price must be set for billable activities");
            }
            if (!activity.UnitPriceNotBillable && activity.UnitPrice == null)
            {
                yield return new ValidationResult("A default unit price must be set for billable participants");
            }
            if ((!activity.FixedPriceNotBillable || !activity.UnitPriceNotBillable) && (activity.CurrencyCodeId == null || activity.CurrencyCodeId == 0))
            {
                yield return new ValidationResult("A currencycode must be set for activities where fixed price or unit price is specified");
            }
        }

        private bool OrderHasBeenCreated(OrderStatus currentOrderStatus)
        {
            return OrderIsTransferred(currentOrderStatus) || currentOrderStatus == OrderStatus.Draft;
        }

        private bool OrderIsTransferred(OrderStatus currentOrderStatus)
        {
            return currentOrderStatus == OrderStatus.Transferred ||
                   currentOrderStatus == OrderStatus.NotInvoiced ||
                   currentOrderStatus == OrderStatus.Invoiced ||
                   currentOrderStatus == OrderStatus.Rejected;
        }
    }
}