﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using ValidationException = Metier.Phoenix.Core.Exceptions.ValidationException;

namespace Metier.Phoenix.Api.Data.Validation
{
    public class OrderValidator : IValidateOrder
    {
        private readonly MetierLmsContext _context;

        public OrderValidator(MetierLmsContext context)
        {
            _context = context;
        }

        public IEnumerable<ValidationResult> Validate(Order order, Dictionary<int, List<Order>> participantsOrders, Dictionary<int, List<Order>> activitiesOrders)
        {
            var validationResults = new List<ValidationResult>();

            if (order.CustomerId <= 0)
            {
                validationResults.Add(new ValidationResult("Customer ID missing on order being created/updated."));
            }

            ValidateParticipants(order, participantsOrders, validationResults);
            ValidateActivities(order, activitiesOrders, validationResults);

            return validationResults;
        }

        private void ValidateParticipants(Order order, Dictionary<int, List<Order>> participantsOrders, List<ValidationResult> validationResults)
        {
            foreach (var participantOrderLine in order.ParticipantOrderLines)
            {
                var participant = participantOrderLine.Participant;

                if (participant == null)
                {
                    _context.Entry(participantOrderLine).Reference(l => l.Participant).Load();
                    participant = participantOrderLine.Participant;
                }

                var participantOrders = participantsOrders[participant.Id];

                if (participantOrders.Any())
                {
                    var currentParticipantOrder = participantOrders.OrderByDescending(o => o.Id).First();

                    if (currentParticipantOrder.Id != order.Id && currentParticipantOrder.IsCreditNote == order.IsCreditNote)
                    {
                        validationResults.Add(
                            new ValidationResult(
                                string.Format("Participant with ID {0} has already been added to another existing order.",
                                    participant.Id)));
                    }
                }

                var populatedParticipant = participant;
                if (populatedParticipant.CustomerId == 0)
                {
                    populatedParticipant = _context.Participants.First(p => p.Id == participant.Id);
                }
                if (populatedParticipant.CustomerId != order.CustomerId)
                {
                    validationResults.Add(
                        new ValidationResult(
                            "Participant is associated with a different customer than the one associated with the order."));
                }
            }
        }

        private void ValidateActivities(Order order, Dictionary<int, List<Order>> activitiesOrders, List<ValidationResult> validationResults)
        {
            foreach (var activityOrderLine in order.ActivityOrderLines)
            {
                var activity = activityOrderLine.Activity;
                var activityOrders = activitiesOrders[activity.Id];

                if (activityOrders.Any())
                {
                    var currentActivityOrder = activityOrders.OrderByDescending(o => o.Id).First();

                    if ((currentActivityOrder.Id != order.Id & currentActivityOrder.IsCreditNote == order.IsCreditNote))
                    {
                        validationResults.Add(
                            new ValidationResult(
                                string.Format("Activity with ID {0} has already been added to another existing order.",
                                    activity.Id)));
                    }
                }

                var populatedActivity = activity;
                if (populatedActivity.CustomerArticleId == 0)
                {
                    populatedActivity = _context.Activities.First(a => a.Id == activity.Id);
                }
                var customerArticle = _context.CustomerArticles.First(ca => ca.Id == populatedActivity.CustomerArticleId);
                if (customerArticle.CustomerId != order.CustomerId)
                {
                    validationResults.Add(
                        new ValidationResult("Activity is associated with a different customer than the current order."));
                }
            }
        }

        public void ValidateForFullCreditNote(Order order, Dictionary<int, List<Order>> participantOrders, Dictionary<int, List<Order>> activityOrders)
        {
            if (order.Status != OrderStatus.Invoiced)
            {
                throw new ValidationException("Order is not yet invoiced, so crediting is not allowed.");
            }

            foreach (var orderLines in order.ParticipantOrderLines)
            {
                var currentParticipantOrder = participantOrders[orderLines.Participant.Id].Where(o => o.ParticipantOrderLines.Any(p => p.Participant.Id == orderLines.Participant.Id)).OrderByDescending(o => o.Id).First();

                if (currentParticipantOrder.Id != order.Id)
                {
                    throw new ValidationException("Order is already partially credited");
                }
            }

            foreach (var orderLine in order.ActivityOrderLines)
            {
                var currentActivityOrder = activityOrders[orderLine.Activity.Id].Where(o => o.ActivityOrderLines.Any(a => a.Activity.Id == orderLine.Activity.Id)).OrderByDescending(o => o.Id).First();

                if (currentActivityOrder.Id != order.Id)
                {
                    throw new ValidationException("Order is already partially credited");
                }
            }
        }

        public void ValidateForPartialCreditNote(Order order, Dictionary<int, List<Order>> participantOrders, Dictionary<int, List<Order>> activityOrders)
        {
            if (order.Status != OrderStatus.Draft)
            {
                throw new ValidationException("Credit note has already been sent");
            }
            if (!order.CreditedOrderId.HasValue || order.CreditedOrderId == 0)
            {
                throw new ValidationException("Order being credited must be specified on credit note order");
            }
            foreach (var orderLine in order.ParticipantOrderLines)
            {
                var participant = orderLine.Participant;
                var currentParticipantOrder = participantOrders[participant.Id].Where(o => o.ParticipantOrderLines.Any(l => l.Participant.Id == participant.Id)).OrderByDescending(o => o.Id).First();

                if (currentParticipantOrder.Id != order.Id)
                {
                    throw new ValidationException("Order has already been partially credited");
                }
                var previousParticipantOrder = participantOrders[participant.Id].Where(o => o.ParticipantOrderLines.Any(l => l.Participant.Id == participant.Id)).OrderByDescending(o => o.Id).ElementAtOrDefault(1);

                if (previousParticipantOrder == null)
                {
                    throw new ValidationException(string.Format("Credit note contains participant (ID: {0}) not yet invoiced", participant.Id));
                }
                if (previousParticipantOrder.IsCreditNote)
                {
                    throw new ValidationException(string.Format("Credit note contains participant (ID: {0}) already credited", participant.Id));
                }
                if (previousParticipantOrder.Id != order.CreditedOrderId)
                {
                    throw new ValidationException(string.Format("Participant (ID: {0}) is currently invoiced in an order different from the order being credited", participant.Id));
                }
                if (previousParticipantOrder.Status != OrderStatus.Invoiced)
                {
                    throw new ValidationException(string.Format("Credit note contains participant (ID: {0}) not yet invoiced", participant.Id));
                }
            }

            foreach (var orderLine in order.ActivityOrderLines)
            {
                var activityId = orderLine.Activity.Id;
                var currentActivityOrder = activityOrders[activityId].Where(o => o.ActivityOrderLines.Any(a => a.Activity.Id == activityId)).OrderByDescending(o => o.Id).First();

                if (currentActivityOrder.Id != order.Id)
                {
                    throw new ValidationException("Order has already been partially credited");
                }
                var previousActivityOrder = activityOrders[activityId].Where(o => o.ActivityOrderLines.Any(a => a.Activity.Id == activityId)).OrderByDescending(o => o.Id).ElementAtOrDefault(1);

                if (previousActivityOrder == null)
                {
                    throw new ValidationException(string.Format("Credit note contains activity (ID: {0}) not yet invoiced", activityId));
                }
                if (previousActivityOrder.IsCreditNote)
                {
                    throw new ValidationException(string.Format("Credit note contains activity (ID: {0}) already credited", activityId));
                }
                if (previousActivityOrder.Id != order.CreditedOrderId)
                {
                    throw new ValidationException(string.Format("Activity (ID: {0}) is currently invoiced in an order different from the order being credited", activityId));
                }
                if (previousActivityOrder.Status != OrderStatus.Invoiced)
                {
                    throw new ValidationException(string.Format("Credit note contains activity (ID: {0}) not yet invoiced", activityId));
                }
            }
        }
    }
}