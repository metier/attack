﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Utilities.Validation;

namespace Metier.Phoenix.Api.Data.Validation
{
    public class CustomerContactPersonValidator : IValidate<CustomerContactPerson>
    {
        public IEnumerable<ValidationResult> Validate(CustomerContactPerson item)
        {
            if (!string.IsNullOrEmpty(item.Email) && !EmailValidation.IsValidEmail(item.Email))
            {
                yield return new ValidationResult("Invalid e-mail address", new[] { "Email" });
            }
        }
    }
}