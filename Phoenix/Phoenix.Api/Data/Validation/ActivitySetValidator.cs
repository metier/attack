﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Queryables;

namespace Metier.Phoenix.Api.Data.Validation
{
    public class ActivitySetValidator : IValidate<ActivitySet>
    {
        private readonly MetierLmsContext _context;

        public ActivitySetValidator(MetierLmsContext context)
        {
            _context = context;
        }

        public IEnumerable<ValidationResult> Validate(ActivitySet activitySet)
        {
            if (activitySet.EnrollmentFrom == null)
            {
                yield return new ValidationResult("Enrollment from-date is required");
            }
            if (activitySet.IsPublished)
            {
                if (activitySet.PublishFrom == null)
                {
                    yield return new ValidationResult("Publish from-date is required when IsPublished property is set");
                }

                if (activitySet.PublishTo.HasValue && activitySet.PublishFrom.HasValue &&
                    activitySet.PublishTo.Value.Date < activitySet.PublishFrom.Value.Date)
                {
                    yield return new ValidationResult("Publish end date cannot be earlier than start date");
                }
            }

            if (activitySet.EnrollmentTo.HasValue && activitySet.EnrollmentFrom.HasValue &&
                activitySet.EnrollmentTo.Value.Date < activitySet.EnrollmentFrom.Value.Date)
            {
                yield return new ValidationResult("Enrollment end date cannot be earlier than start date");
            }

            if (activitySet.IsDeleted)
            {
                var dbActivitySet = _context.ActivitySets.WithActivitiesAndActivityPeriods().First(a => a.Id == activitySet.Id);
                if (dbActivitySet.Activities.Count(a => !a.IsDeleted) > 0)
                {
                    yield return new ValidationResult("Cannot delete activity set containing activities");
                }
            }
        }
    }
}