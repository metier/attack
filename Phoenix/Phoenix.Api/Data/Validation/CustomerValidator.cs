﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Api.Services.ExternalCustomers;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Validation
{
    public class CustomerValidator : IValidate<Customer>
    {
        private readonly IValidate<ParticipantInfoDefinition> _participantInfoDefinitionValidator;
        private readonly IValidate<CustomerContactPerson> _customerContactPersonValidator;
        private readonly MetierLmsContext _context;

        public CustomerValidator(IValidate<ParticipantInfoDefinition> participantInfoDefinitionValidator,
                                 IValidate<CustomerContactPerson> customerContactPersonValidator,
                                 MetierLmsContext context)
        {
            _participantInfoDefinitionValidator = participantInfoDefinitionValidator;
            _customerContactPersonValidator = customerContactPersonValidator;
            _context = context;
        }

        public IEnumerable<ValidationResult> Validate(Customer customer)
        {
            var validationResults = new List<ValidationResult>();

            validationResults.AddRange(ValidateCustomer(customer));

            if (customer.ParticipantInfoDefinition != null)
            {
                validationResults.AddRange(_participantInfoDefinitionValidator.Validate(customer.ParticipantInfoDefinition));
            }

            if (customer.CustomerContactPersons.Count > 0)
            {
                foreach (var customerContactPerson in customer.CustomerContactPersons)
                {
                    validationResults.AddRange(_customerContactPersonValidator.Validate(customerContactPerson));
                }
            }
            
            return validationResults;
        }

        public IEnumerable<ValidationResult> ValidateCustomer(Customer customer)
        {
            if (customer.Name == null)
            {
                yield return new ValidationResult("Name is required", new[] { "Name" });
            }

            if (customer.DistributorId == 0)
            {
                yield return new ValidationResult("Distributor is required", new[] { "DistributorId" });
            }

            if (customer.LanguageId == null)
            {
                yield return new ValidationResult("Language is required", new[] { "LanguageId" });
            }

            if (customer.Id > 0) //Existing customer
            {
                var dbCustomer = _context.Customers.AsNoTracking().FirstOrDefault(c => c.Id == customer.Id);
                if (!string.IsNullOrEmpty(dbCustomer.ExternalCustomerId) && dbCustomer.ExternalCustomerId != customer.ExternalCustomerId)
                {
                    yield return new ValidationResult("External Customer ID can not be changed", new[] { "ExternalCustomerId" });
                }
            }

            if (customer.ParentId.HasValue && HasParentReferenceToSelf(customer, customer.Id))
            {
                yield return new ValidationResult("Assigning selected parent organization would result in a circular organization structure");
            }
        }

        internal bool HasParentReferenceToSelf(Customer customer, int id)
        {
            if (!customer.ParentId.HasValue)
            {
                return false;
            }
            if (customer.ParentId.Value == id)
            {
                return true;
            }
            var parentCustomer = _context.Customers.First(c => c.Id == customer.ParentId.Value);
            return HasParentReferenceToSelf(parentCustomer, id);
        }
    }
}