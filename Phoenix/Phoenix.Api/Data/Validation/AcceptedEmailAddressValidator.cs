﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Utilities.Validation;

namespace Metier.Phoenix.Api.Data.Validation
{
    public class AcceptedEmailAddressValidator : IValidate<AcceptedEmailAddress>
    {
        public IEnumerable<ValidationResult> Validate(AcceptedEmailAddress item)
        {
            if (!string.IsNullOrEmpty(item.Domain) && !DomainValidation.IsValidDomain(item.Domain))
            {
                yield return new ValidationResult("Invalid domain address", new[] { "Domain" });
            }
        }
    }
}