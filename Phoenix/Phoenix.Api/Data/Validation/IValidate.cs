﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Metier.Phoenix.Api.Data.Validation
{
    public interface IValidate<in T> where T : class
    {
        IEnumerable<ValidationResult> Validate(T item);
    }
}