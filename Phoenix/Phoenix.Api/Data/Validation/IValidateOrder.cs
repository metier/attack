using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Validation
{
    public interface IValidateOrder
    {
        IEnumerable<ValidationResult> Validate(Order order, Dictionary<int, List<Order>> participantOrders, Dictionary<int, List<Order>> activityOrders);
        void ValidateForFullCreditNote(Order order, Dictionary<int, List<Order>> participantOrders,Dictionary<int, List<Order>> activityOrders);
        void ValidateForPartialCreditNote(Order order, Dictionary<int, List<Order>> participantOrders,Dictionary<int, List<Order>> activityOrders);
    }
}