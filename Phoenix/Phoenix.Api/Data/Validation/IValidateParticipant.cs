﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Data.Validation
{
    public interface IValidateParticipant
    {
        IEnumerable<ValidationResult> Validate(Participant postedParticipant);
        IEnumerable<ValidationResult> Validate(Participant postedParticipant, Participant currentParticipant, Order currentOrder);
    }
}