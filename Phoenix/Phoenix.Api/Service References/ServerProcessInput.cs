﻿namespace Metier.Phoenix.Api.Agresso.ImportService
{
    public partial class ServerProcessInput
    {
        [System.Runtime.Serialization.DataMemberAttribute(Name = "Xml", EmitDefaultValue = false, Order = 3)]
        public CDataWrapper Xml
        {
            get
            {
                return XmlString;
            }
            set
            {
                XmlString = value;
            }
        }
    }
}