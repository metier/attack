﻿namespace Metier.Phoenix.Api.Facade.Participant
{
    public class ParticipantMoveRequest
    {
        public int ActivityId { get; set; } 
        public int ActivitySetId { get; set; } 
    }
}