﻿using System;
using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Facade.Participant
{
    public class ExamSubmission
    {
        public string ActivityTitle { get; set; }
        public int ParticipantId { get; set; }
        public string ExamGrade { get; set; }
        public List<Attachment> Attachments { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? ExamGradeDate { get; set; }
    }
}