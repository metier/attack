﻿namespace Metier.Phoenix.Api.Facade.Participant
{
    public class ParticipantPayment
    {
        public int ActivityId { get; set; }
        public decimal Price { get; set; }
        public int CurrencyCodeId { get; set; }
    }
}