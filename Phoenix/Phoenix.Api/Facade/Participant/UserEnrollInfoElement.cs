﻿namespace Metier.Phoenix.Api.Facade.Participant
{
    public class UserEnrollInfoElement
    {
        public string InfoText { get; set; }
        public int LeadingTextId { get; set; }
    }
}