﻿using System;
namespace Metier.Phoenix.Api.Facade.Participant
{
    public class ParticipantUpdateRequest
	{
        //public int Id { get; set; }
		public bool SetStatus { get; set; }
		public bool SetCompletedDate { get; set; }
		public string Status { get; set; }
		public DateTime? CompletedDate { get; set; }
    }
}