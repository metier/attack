﻿using System;

namespace Metier.Phoenix.Api.Facade.Participant
{
    public class ParticipantGradeResponse
    {
        public byte[] ParticipantRowVersion { get; set; }
        public string ExamGrade { get; set; }
        public DateTime? ExamGradeDate { get; set; }
        public string StatusCodeValue { get; set; }
        public string Result { get; set; }
    }
}