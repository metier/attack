﻿using System.Web.Security;

namespace Metier.Phoenix.Api.Facade
{
    public class AccountSearchResult
    {
        public AccountSearchResult(Core.Data.Entities.User user, MembershipUser membershipUser)
        {
            UserId = user.Id;
            Username = membershipUser.UserName;
            Email = membershipUser.Email;
            FirstName = user.FirstName;
            LastName = user.LastName;
            CustomerId = user.CustomerId;
            CustomerName = user.Customer.Name;
        }

        public int UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
    }
}