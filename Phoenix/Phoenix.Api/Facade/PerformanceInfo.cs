﻿using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Facade
{
    public class PerformanceInfo
    {
        public PerformanceInfo()
        {
            Performances = new List<ScormPerformance>();
        }

        public int UserId { get; set; } 
        public RcoCourse Course { get; set; } 
        public RcoExamContainer ExamContainer { get; set; } 
        public List<ScormPerformance> Performances { get; set; } 
    }
}