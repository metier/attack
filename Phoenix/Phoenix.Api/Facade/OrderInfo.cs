﻿using System;
using System.Collections.Generic;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Api.Facade
{
    public class OrderInfo
    {
        public OrderInfo()
        {
            Participants = new List<int>();
            FixedPriceActivities = new List<int>();
            OrderLines = new List<OrderLine>();
        }
        
        public int Id { get; set; }
        public byte[] RowVersion { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int? CoordinatorId { get; set; }
        public string YourOrder { get; set; }
        public string YourRef { get; set; }
        public string OurRef { get; set; }
        public int? OrderNumber { get; set; }
        public int? InvoiceNumber { get; set; }
        public OrderStatus Status { get; set; }
        public string Title { get; set; }
        public DateTime LastModified { get; set; }
        public DateTime? OrderDate { get; set; }
        public bool IsCreditNote { get; set; }
        public int? CreditedOrderId { get; set; }
        public string WorkOrder { get; set; }

        public List<int> Participants { get; set; }
        public List<int> FixedPriceActivities { get; set; }

        public IEnumerable<OrderLine> OrderLines { get; set; }
    }
}