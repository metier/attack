﻿using System;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Api.Facade.Mail
{
    public class MailSentResponse
    {
        public int Id { get; set; }
        public MailStatusType Status { get; set; } 
        public DateTime StatusTime { get; set; } 
    }
}