﻿namespace Metier.Phoenix.Api.Facade.Mail
{
    public class MailPreview
    {
        public string RecipientEmail { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}