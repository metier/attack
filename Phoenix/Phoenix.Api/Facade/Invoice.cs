﻿using System;
using System.Collections.Generic;

namespace Metier.Phoenix.Api.Facade
{
    public class Invoice
    {
        public string ExternalId { get; set; }
        public int CustomerId { get; set; }
        public int InvoiceNumber { get; set; }
        public int OrderNumber { get; set; }

        public List<InvoiceLine> InvoiceLines { get; set; }

        public DateTime? InvoiceDate { get; set; }
        public DateTime? SentDate { get; set; }
        public DateTime? PaidDate { get; set; }
        public string Status { get; set; }
        public string Project { get; set; }
        public string WorkOrder { get; set; }
    }

    public class InvoiceLine
    {
        public int LineNumber { get; set; }
        public decimal Amount { get; set; }
        public string ArticleCode { get; set; }
        public List<string> ArticleDescriptionLines { get; set; }
        public float Quantity { get; set; }
    }
}