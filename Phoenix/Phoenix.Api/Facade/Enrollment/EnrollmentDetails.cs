﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Metier.Phoenix.Api.Facade.Participant;

namespace Metier.Phoenix.Api.Facade.Enrollment
{
    public class EnrollmentDetails
    {
        public List<UserEnrollInfoElement> InfoElements { get; set; }

        public List<ParticipantPayment> ParticipantPayments { get; set; }
    }
}