﻿using System;
using System.Collections.Generic;

namespace Metier.Phoenix.Api.Facade
{
    public class RcoExamContainer
    {
        public RcoExamContainer()
        {
            Children = new List<RcoExam>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public List<RcoExam> Children { get; set; }

        public DateTime Created { get; set; } 
    }

    public class RcoExam
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; } 
    }
}