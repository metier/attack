namespace Metier.Phoenix.Api.Facade
{
    public class PerformanceSummary
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? UserProfileImageFileId { get; set; }
        public int TotalScore { get; set; }
    }
}