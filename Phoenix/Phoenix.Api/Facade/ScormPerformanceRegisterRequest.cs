﻿using System;

namespace Metier.Phoenix.Api.Facade
{
    public class ScormPerformanceRegisterRequest
    {
        public int? Score { get; set; }
        public int? TimeUsedInSeconds { get; set; }
        public DateTime? CompletedDate { get; set; }
        public string Status { get; set; }
    }
}