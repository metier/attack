﻿using System.Collections.Generic;

namespace Metier.Phoenix.Api.Facade.ExamContent
{
    public class QuestionsChangeSet
    {
        public QuestionsChangeSet()
        {
            QuestionIds = new List<int>();

            ProductIdsToAdd = new List<int>();
            ProductIdsToRemove = new List<int>();

            VersionIdsToAdd = new List<int>();
            VersionIdsToRemove = new List<int>();

            TagIdsToAdd = new List<int>();
            TagIdsToRemove = new List<int>();
        }

        public List<int> QuestionIds { get; set; } 
        public int DifficultyId { get; set; }
        public int StatusId { get; set; }
        public int LanguageId { get; set; }
        public bool IsChangeCustomerTailoredSetting { get; set; }
        public int? CustomerId { get; set; }
        public int? Points { get; set; }
        public string Author { get; set; }

        public List<int> ProductIdsToAdd { get; set; }
        public List<int> ProductIdsToRemove { get; set; }

        public List<int> VersionIdsToAdd { get; set; }
        public List<int> VersionIdsToRemove { get; set; }

        public List<int> TagIdsToAdd { get; set; }
        public List<int> TagIdsToRemove { get; set; }
         
    }
}