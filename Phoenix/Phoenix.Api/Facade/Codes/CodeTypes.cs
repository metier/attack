﻿namespace Metier.Phoenix.Api.Facade.Codes
{
    public static class CodeTypes
    {
        public const string ArticleStatuses = "articlestatuses";
        public const string CountryCodes = "countrycodes";
        public const string CurrencyCodes = "currencycodes";
        public const string ParticipantStatuses = "participantstatuses";
    }
}