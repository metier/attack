﻿using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.ViewEntities;

namespace Metier.Phoenix.Api.Facade
{
    public class CourseProductMapping
    {
        public Rco Rco { get; set; }
        public Product Product { get; set; }
    }
}