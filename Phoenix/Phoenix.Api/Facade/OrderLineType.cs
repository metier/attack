﻿namespace Metier.Phoenix.Api.Facade
{
    public enum OrderLineType
    {
        Custom = 0,
        FixedPriceActivity = 1, // Stand-alone fixed-priced activities
        UnitPriceActivity = 2,  // Activities with participants with same price
        Participant = 3
    }
}