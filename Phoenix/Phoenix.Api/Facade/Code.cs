﻿namespace Metier.Phoenix.Api.Facade
{
    public class Code
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Display { get; set; }

        public Code()
        {
        }
        
        public Code(int id, string value, string display)
        {
            Id = id;
            Value = value;
            Display = display;
        }
    }
}