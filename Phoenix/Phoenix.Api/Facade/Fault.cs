﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Data.Validation;
using Metier.Phoenix.Core.Data.Enums;

namespace Metier.Phoenix.Api.Facade
{
    public class Fault
    {
        public Fault(string message)
        {
            Message = message;
        }

        public string Message { get; set; }
        public ValidationErrorCodes? ErrorCode { get; set; }
        public List<string> ValidationErrors { get; set; }
    }
}