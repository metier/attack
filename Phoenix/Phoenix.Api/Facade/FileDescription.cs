﻿namespace Metier.Phoenix.Api.Facade
{
    public class FileDescription
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public long Size { get; set; }

        public FileDescription()
        {
        }

        public FileDescription(string name, string path, long size)
        {
            Name = name;
            Path = path;
            Size = size;
        }
    }
}