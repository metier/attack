﻿using System;
using System.Collections.Generic;

namespace Metier.Phoenix.Api.Facade
{
    public class RcoCourse
    {
        public RcoCourse()
        {
            Lessons = new List<RcoLesson>();
        }

        public int Id { get; set; }
        public string Version { get; set; }
        public string Name { get; set; }
        public string Language { get; set; }
        public string LanguageIdentifier { get; set; }

        public DateTime Created { get; set; }
        
        public List<RcoLesson> Lessons { get; set; }
        public int? GoldPoints { get; set; }
        public int? SilverPoints { get; set; }
        public int? UnlockFinalPoints { get; set; }
    }
}