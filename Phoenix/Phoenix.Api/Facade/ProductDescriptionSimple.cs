﻿namespace Metier.Phoenix.Api.Facade
{
    public class ProductDescriptionSimple
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int LanguageId { get; set; }
        public int ProductId { get; set; }
    }
}