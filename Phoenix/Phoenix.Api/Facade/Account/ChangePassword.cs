﻿namespace Metier.Phoenix.Api.Facade.Account
{
    public class ChangePassword
    {
        public string NewPassword { get; set; }
    }
}