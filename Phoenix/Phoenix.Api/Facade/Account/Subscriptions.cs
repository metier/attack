﻿using System;
using System.Collections.Generic;

namespace Metier.Phoenix.Api.Facade.Account
{
    public class Subscriptions
    {
        public List<Subscription> Items { get; set; }
        public UserAccount User { get; set; }

        public class Subscription
        {
            public int SubscriptionId { get; set; }//1 == Flow
            public DateTime StartDate { get; set; }
            public DateTime? EndDate { get; set; }
        }
    }
}