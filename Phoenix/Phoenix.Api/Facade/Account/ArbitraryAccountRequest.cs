﻿namespace Metier.Phoenix.Api.Facade.Account
{
    public class ArbitraryAccountRequest
    {
        public int DistributorId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyRegistrationNumber { get; set; }
        public string OurRef { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public int LanguageId { get; set; }
        public int? PreferredLanguageId { get; set; }
    }
}