﻿namespace Metier.Phoenix.Api.Facade.Account
{
    public class ArbitraryAccountResponse
    {
        public int UserId { get; set; }
        public int ArbitraryCustomerId { get; set; }
    }
}