﻿using System.Collections.Generic;

namespace Metier.Phoenix.Api.Facade.Account
{
    public class UserAccount
    {
        public string CompanyName { get; set; }
        public string CompanyRegistrationNumber { get; set; }
        public string OurRef { get; set; }
        public string Username { get; set; }
        public string NewUsername { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int CustomerId { get; set; } // For organization users
        public int? DistributorId { get; set; } // For arbitrarty customer users
        public List<string> Roles { get; set; }
        public Core.Data.Entities.User User { get; set; }
        public UserContext UserContext { get; set; }
    }
}