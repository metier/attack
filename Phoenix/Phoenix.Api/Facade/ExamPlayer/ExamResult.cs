﻿namespace Metier.Phoenix.Api.Facade.ExamPlayer
{
    public class ExamResult
    {
        public decimal? ScorePercentage { get; set; }
        public string Feedback { get; set; }
    }
}