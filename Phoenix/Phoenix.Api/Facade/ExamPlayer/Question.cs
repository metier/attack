﻿using System.Collections.Generic;

namespace Metier.Phoenix.Api.Facade.ExamPlayer
{
    public class Question
    {
        public int Id { get; set; }
        public int Order { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }

        public List<Option> Options { get; set; }
    }
}