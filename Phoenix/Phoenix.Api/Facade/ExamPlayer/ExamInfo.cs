﻿using System;

namespace Metier.Phoenix.Api.Facade.ExamPlayer
{
    public class ExamInfo
    {
        public string Name { get; set; }
        public DateTimeOffset? StartStartTime { get; set; }
        public DateTimeOffset? StartEndTime { get; set; }
        public long? TimeBeforeAvailableInSeconds { get; set; }
        public long DurationInSeconds { get; set; }
        public int NumberOfQuestions { get; set; }
        public int NumberOfQuestionsPerPage { get; set; }
        public string Language { get; set; }
        public Exam Exam { get; set; }
        public ExamResult Result { get; set; }
    }
}