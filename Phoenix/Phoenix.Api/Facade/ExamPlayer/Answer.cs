﻿using System.Collections.Generic;

namespace Metier.Phoenix.Api.Facade.ExamPlayer
{
    public class Answer
    {
        public Answer()
        {
            MultipleChoiceAnswers = new List<int>();
        }

        public long Id { get; set; }
        public int QuestionId { get; set; }
        public bool? BoolAnswer { get; set; }
        public List<int> MultipleChoiceAnswers { get; set; }
        public int? SingleChoiceAnswer { get; set; }
        public bool IsMarkedForReview { get; set; }
    }
}