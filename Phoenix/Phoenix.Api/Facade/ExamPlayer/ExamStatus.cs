﻿namespace Metier.Phoenix.Api.Facade.ExamPlayer
{
    public class ExamStatus
    {
        public int TimeRemainingInSeconds { get; set; } 
        public bool IsStarted { get; set; }
        public bool IsSubmitted { get; set; }
        public int ParticipantId { get; set; }
    }
}