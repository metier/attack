﻿using System.Collections.Generic;

namespace Metier.Phoenix.Api.Facade.ExamPlayer
{
    public class Exam
    {
        public long Id { get; set; }
        public ExamStatus Status { get; set; }
        public List<Question> Questions { get; set; }
        public List<Answer> Answers { get; set; }
    }
}