﻿namespace Metier.Phoenix.Api.Facade.ExamPlayer
{
    public class Option
    {
        public int Id { get; set; }
        public string Text { get; set; }
    }
}