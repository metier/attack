﻿using System;
using System.Collections.Generic;

namespace Metier.Phoenix.Api.Facade
{
    public class RcoLesson
    {
        public RcoLesson()
        {
            Lessons = new List<RcoLesson>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? Weight { get; set; }
        public bool IsFinalTest { get; set; }
        public DateTime Created { get; set; }

        public List<RcoLesson> Lessons { get; set; }
    }
}