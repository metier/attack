﻿namespace Metier.Phoenix.Api.Facade
{
    public class OrderLine
    {
        public int Id { get; set; }
        public int? ParticipantId { get; set; }
        public int? ParentActivityId { get; set; }
        public string Article { get; set; }
        public string Text { get; set; }
        public decimal ValueAddedTax { get; set; }
        public decimal Price { get; set; }
        public int CurrencyCodeId { get; set; }
        public OrderLineType Type { get; set; }
    }
}