﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using NLog;

namespace Metier.Phoenix.Api.Filters
{
    public class ExceptionHandlerFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception != null)
            {
                var logger = LogManager.GetLogger("Metier.Phoenix.Api");
                logger.Error("An unexpected error occured", actionExecutedContext.Exception);
                actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "An unexpected error occured");
            }
        }
    }
}