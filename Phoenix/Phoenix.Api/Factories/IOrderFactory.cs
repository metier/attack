﻿using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Factories
{
    public interface IOrderFactory
    {
        Order CreateCreditNoteFromOrder(Order order);
        Order CreateCreditNoteDraftFromOrder(Order order);
        string GetOrderLineText(string participantFirstName, string participantLastName, Participant participant);
    }
}