﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Factories
{
    public interface IPerformanceSummaryFactory
    {
        PerformanceSummary Create(User user, IEnumerable<PerformanceInfo> coursePerformance);
    }
}