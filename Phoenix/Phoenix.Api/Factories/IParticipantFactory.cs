﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Facade.Participant;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Factories
{
    public interface IParticipantFactory    
    {
        Participant Create(User user, Enrollment enrollment, Activity activity, int activitySetId, IEnumerable<UserEnrollInfoElement> infoElements, string paymentReference = null, Customer customer = null, decimal? overridePrice = null, int? overrideCurrencyCodeId = null);
    }
}