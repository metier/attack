﻿using Metier.Phoenix.Api.Facade.ExamPlayer;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Factories.ExamPlayer
{
    public interface IExamPlayerFactory
    {
        Facade.ExamPlayer.Exam CreateExam(Core.Data.Entities.Exam examEntity, Participant participant, Activity activity, ExamArticle article);
        ExamStatus CreateExamStatus(Core.Data.Entities.Exam examEntity, Participant participant, Activity activity, ExamArticle article);
        ExamResult CreateResult(Core.Data.Entities.Exam exam, ExamArticle article);
    }
}