﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Facade.ExamPlayer;
using Metier.Phoenix.Core.Business;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Utilities;
using Answer = Metier.Phoenix.Api.Facade.ExamPlayer.Answer;
using Question = Metier.Phoenix.Core.Data.Entities.Question;

namespace Metier.Phoenix.Api.Factories.ExamPlayer
{
    public class ExamPlayerFactory : IExamPlayerFactory
    {
        private readonly IParticipantBusiness _participantBusiness;

        public ExamPlayerFactory(IParticipantBusiness participantBusiness)
        {
            _participantBusiness = participantBusiness;
        }

        public Facade.ExamPlayer.Exam CreateExam(Core.Data.Entities.Exam examEntity, Participant participant, Activity activity, ExamArticle article)
        {
            return new Facade.ExamPlayer.Exam
            {
                Id = examEntity.Id,
                Status = CreateExamStatus(examEntity, participant, activity, article),
                Questions = examEntity.Questions.Select(CreateQuestion).ToList(),
                Answers = examEntity.Answers.Select(a => CreateAnswer(a, examEntity.Questions)).ToList()
            };
        }

        public ExamStatus CreateExamStatus(Core.Data.Entities.Exam examEntity, Participant participant, Activity activity, ExamArticle article)
        {
            var status = new ExamStatus { ParticipantId = participant.Id };
            status.IsStarted = false;

            if (examEntity != null)
            {
                status.IsStarted = true;
                status.IsSubmitted = examEntity.IsSubmitted;
                status.TimeRemainingInSeconds = GetTimeRemainingInSeconds(examEntity, participant, activity, article);
            }
            return status;
        }

        public ExamResult CreateResult(Core.Data.Entities.Exam exam, ExamArticle article)
        {
            var result = new ExamResult { Feedback = "" };
            switch (article.ExamResultResponseType)
            {
                case ExamResultResponseTypes.NoScore:
                    result.Feedback = article.NoResultResponse;
                    break;
                case ExamResultResponseTypes.MasteryScore:
                    var passMark = article.MasteryScore.HasValue ? article.MasteryScore.Value : 0;
                    var achievedScore = exam.TotalScore.HasValue ? exam.TotalScore.Value : 0;
                    result.ScorePercentage = Decimal.Divide(achievedScore, exam.Questions.Sum(q => q.Points));
                    result.Feedback = result.ScorePercentage >= passMark ? article.AboveRequiredScoreResponse : article.BelowRequiredScoreResponse;
                    break;
            }
            return result;
        }

        private Answer CreateAnswer(Core.Data.Entities.Answer answerEntity, IEnumerable<Question> examQuestions)
        {
            var question = examQuestions.FirstOrDefault(q => q.Id == answerEntity.QuestionId);
            var answer = new Answer
            {
                Id = answerEntity.Id,
                QuestionId = answerEntity.QuestionId,
                IsMarkedForReview = answerEntity.IsMarkedForReview
            };

            if (question != null)
            {
                if (question.Type == QuestionTypes.TrueFalse)
                {
                    answer.BoolAnswer = answerEntity.BoolAnswer;
                }
                else if (question.Type == QuestionTypes.MultipleChoiceSingle)
                {
                    if (!answerEntity.OptionsAnswers.Any())
                    {
                        answer.SingleChoiceAnswer = null;
                    }
                    else
                    {
                        answer.SingleChoiceAnswer = answerEntity.OptionsAnswers.Select(a => a.Id).First();
                    }
                }
                else if (question.Type == QuestionTypes.MultipleChoiceMultiple)
                {
                    answer.MultipleChoiceAnswers = answerEntity.OptionsAnswers.Select(a => a.Id).ToList();
                }
            }
            return answer;
        }

        private Facade.ExamPlayer.Question CreateQuestion(Question questionEntity, int index)
        {
            var options = questionEntity.QuestionOptions.Select(CreateOption).ToList();
            return new Facade.ExamPlayer.Question
            {
                Id = questionEntity.Id,
                Order = index + 1,
                Text = questionEntity.Text,
                Type = questionEntity.Type,
                Options = questionEntity.IsOptionsRandomized ? options.AsRandom().ToList() : options
            };
        }

        private Option CreateOption(QuestionOption optionEntity)
        {
            return new Option
            {
                Id = optionEntity.Id,
                Text = optionEntity.Text
            };
        }

        private int GetTimeRemainingInSeconds(Core.Data.Entities.Exam examEntity, Participant participant, Activity activity, Article article)
        {
            var dueDate = _participantBusiness.GetDueDate(participant, activity, article, examEntity);
            if (!dueDate.HasValue || dueDate.Value < DateTime.UtcNow)
            {
                return 0;
            }
            return (int) (dueDate.Value - DateTime.UtcNow).TotalSeconds;
        }
    }
}