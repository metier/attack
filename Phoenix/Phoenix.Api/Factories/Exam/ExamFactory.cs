﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.Queryables;
using Metier.Phoenix.Core.Data.ViewEntities;
using Metier.Phoenix.Core.Exceptions;
using Metier.Phoenix.Core.Utilities;
using Microsoft.Ajax.Utilities;

namespace Metier.Phoenix.Api.Factories.Exam
{
    public class ExamFactory : IExamFactory
    {
        private readonly MetierLmsContext _context;

        public ExamFactory(MetierLmsContext context)
        {
            _context = context;
        }

        public Core.Data.Entities.Exam Create(ExamArticle examArticle, Activity activity, Participant participant, CustomerArticle customerArticle)
        {
            var qualificationStatus = participant.QualificationStatus ?? participant.IncludeQualificationStatus(_context).QualificationStatus;

            if(qualificationStatus == ParticipantQualificationStatus.NotQualified)
            {
                throw new ValidationException("Participant is not qualified for this exam. Cannot start exam.", ValidationErrorCodes.ParticipantNotQualified);
            }

            var questions = GetQuestions(examArticle, activity, customerArticle);
            foreach (var question in questions)
            {
                question.LastUsed = DateTime.UtcNow;
            }

            return new Core.Data.Entities.Exam
            {
                ParticipantId = participant.Id,
                IsSubmitted = false,
                Created = DateTime.UtcNow,
                Questions = questions,
                Answers = questions.Select(CreateAnswer).ToList()
            };
        }

        private Answer CreateAnswer(Question question)
        {
            return new Answer
            {
                QuestionId = question.Id
            };
        }
        
        private List<Question> GetQuestions(ExamArticle examArticle, Activity activity, CustomerArticle customerArticle)
        {
            if (examArticle.ExamType.HasValue)
            {
                if (examArticle.ExamType.Value == ExamTypes.Random)
                {
                    return GetQuestionsByRandom(examArticle, activity, customerArticle);
                }
                throw new NotImplementedException("Exam type not implemented for exam type set on article");
            }
            throw new ApplicationException("ExamType was not set on article. Unable to determine what type of questions to select.");
        }

        private List<Question> GetQuestionsByRandom(ExamArticle examArticle, Activity activity, CustomerArticle customerArticle)
        {
            var rcosForUseInExam = GetRcosForUseInExam(activity).Select(r => r.Id).ToList();
            
            var questionsQuery = _context.Questions
                                        .Include(q => q.Products)
                                        .Include(q => q.Rcos)
                                        .Include(q => q.QuestionOptions)
                                        // Questions with courses that the user has completed
                                        .Where(q => q.Rcos.Any(r => rcosForUseInExam.Contains(r.Id)))
                                        // Published questions
                                        .Where(q => q.Status == QuestionStatuses.Published)
                                        // Questions with the correct language
                                        .Where(q => q.LanguageId == examArticle.LanguageId).ToList();

            var easy = GetQuestions(questionsQuery, QuestionDifficulties.Easy, customerArticle.CustomerId, activity.EasyQuestionCount, activity.EasyCustomerSpecificCount, rcosForUseInExam);
            if (easy.Count < activity.EasyQuestionCount)
            {
                throw new ExamException("Could not find enough easy questions");
            }
            var medium = GetQuestions(questionsQuery, QuestionDifficulties.Medium, customerArticle.CustomerId, activity.MediumQuestionCount, activity.MediumCustomerSpecificCount, rcosForUseInExam);
            if (medium.Count < activity.MediumQuestionCount)
            {
                throw new ExamException("Could not find enough medium questions");
            }
            var hard = GetQuestions(questionsQuery, QuestionDifficulties.Hard, customerArticle.CustomerId, activity.HardQuestionCount, activity.HardCustomerSpecificCount, rcosForUseInExam);
            if (hard.Count < activity.HardQuestionCount)
            {
                throw new ExamException("Could not find enough hard questions");
            }

            return easy.Concat(medium).Concat(hard).ToList().AsRandom().ToList();
        }

        //TODO: FLMS-187, Rewrite or remove if not used anymore.
        internal IEnumerable<Rco> GetRcosForUseInExam(Activity activity)
        {
            //Select Rcos specified for the activity
            var rcoIds = activity.ActivityExamCourses.Select(aec => aec.CourseRcoId).ToList();
            return _context.Rcos.Where(r => rcoIds.Contains(r.Id));
        }

        private List<Question> GetQuestions(List<Question> questionsQuery, QuestionDifficulties difficulty, int customerId, int? genericCount, int? customerSpecificCount, List<int> rcosForUseInExam )
        {
            var numberOfCustomerSpecific = customerSpecificCount.GetValueOrDefault();
            if (numberOfCustomerSpecific < 0)
            {
                numberOfCustomerSpecific = 0;
            }

            var numberOfGeneric = genericCount.GetValueOrDefault() - numberOfCustomerSpecific;
            if (numberOfGeneric < 0)
            {
                numberOfGeneric = 0;
            }

            List<Question> customerSpecificQuestions = GetQuestions(questionsQuery, difficulty, customerId, numberOfCustomerSpecific, rcosForUseInExam);
            List<Question> genericQuestions = GetQuestions(questionsQuery, difficulty, null, numberOfGeneric, rcosForUseInExam);

            return genericQuestions.Concat(customerSpecificQuestions).ToList();
        } 

        private List<Question> GetQuestions(List<Question> questionsQuery, QuestionDifficulties difficulty, int? customerId, int? numberOfQuestionsToSelect, List<int> rcosForUseInExam )
        {
            var examQuestions = new List<Question>();

            if (rcosForUseInExam.Count == 0)
            {
                throw new ExamException("No courses specified, so not able to select any questions");
            }

            if (!numberOfQuestionsToSelect.HasValue)
            {
                numberOfQuestionsToSelect = 0;
            }

            /* 
             * C# integer division:
             * The division rounds the result towards zero, 
             * and the absolute value of the result is the largest possible integer 
             * that is less than the absolute value of the quotient of the two operands. 
             * The result is zero or positive when the two operands have the same sign and zero 
             * or negative when the two operands have opposite signs.
             * Ref.: https://msdn.microsoft.com/en-us/library/aa691373(v=vs.71).aspx
             */
            int numberOfQuestionsForRco = numberOfQuestionsToSelect.Value / rcosForUseInExam.Count;

            foreach (var rcoId in rcosForUseInExam)
            {
                var questions = GetQuestionsForRco(questionsQuery, difficulty, customerId, rcoId, numberOfQuestionsForRco, examQuestions);
                examQuestions = examQuestions.Concat(questions).ToList();
            }

            int remainder = numberOfQuestionsToSelect.GetValueOrDefault() - examQuestions.Count;
            if (remainder > 0)
            {
                var questions = GetRemainingQuestions(questionsQuery, examQuestions, difficulty, customerId, remainder);
                examQuestions = examQuestions.Concat(questions).ToList();
            }
            
            return examQuestions;
        }

        private static List<Question> GetQuestionsForRco(List<Question> questionsQuery, QuestionDifficulties difficulty, int? customerId, int rcoId, int numberOfQuestions, List<Question> selectedQuestions )
        {
            //There is probably some way of constructing one query which handles both situations, but don't have time to figure out how just now...
            List<Question> questions;

            if (customerId.HasValue)
            {
                questions = questionsQuery.AsRandom()
                    .Where(
                        q =>
                            q.Difficulty == difficulty && (q.CustomerId == customerId) &&
                            q.Rcos.Any(r => r.Id == rcoId) &&
                            !selectedQuestions.Exists(sq => sq.Id == q.Id))
                    .Take(numberOfQuestions)
                    .ToList();

                return questions;
            }

            questions = questionsQuery.AsRandom()
                .Where(
                    q =>
                        q.Difficulty == difficulty &&
                        !q.CustomerId.HasValue &&
                        q.Rcos.Any(r => r.Id == rcoId) &&
                        !selectedQuestions.Exists(sq => sq.Id == q.Id))
                .Take(numberOfQuestions)
                .ToList();

            return questions;
        }

        private static List<Question> GetRemainingQuestions(List<Question> questionsQuery, List<Question> retrievedQuestions, QuestionDifficulties difficulty, int? customerId, int numberOfQuestions)
        {
            //There is probably some way of constructing one query which handles both situations, but don't have time to figure out how just now...
            List<Question> questions;

            if (customerId.HasValue)
            {
                questions = questionsQuery.AsRandom()
                    .Where(
                        q =>
                            q.Difficulty == difficulty && (q.CustomerId == customerId) &&
                            !retrievedQuestions.Exists(rq => rq.Id == q.Id))
                    .Take(numberOfQuestions)
                    .ToList();

                return questions;
            }

            questions = questionsQuery.AsRandom()
                .Where(
                    q =>
                        q.Difficulty == difficulty &&
                        !q.CustomerId.HasValue &&
                        !retrievedQuestions.Exists(rq => rq.Id == q.Id))
                .Take(numberOfQuestions)
                .ToList();

            return questions;


        }
    }
}