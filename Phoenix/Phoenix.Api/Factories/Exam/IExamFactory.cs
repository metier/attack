﻿using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Factories.Exam
{
    public interface IExamFactory
    {
        Core.Data.Entities.Exam Create(ExamArticle examArticle, Activity activity, Participant participant, CustomerArticle customerArticle);
    }
}