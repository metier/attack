using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Factories
{
    public class PerformanceSummaryFactory : IPerformanceSummaryFactory
    {
        public PerformanceSummary Create(User user, IEnumerable<PerformanceInfo> coursePerformances)
        {
            return new PerformanceSummary
            {
                UserId = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                TotalScore = coursePerformances.Sum(cp => cp.Performances == null ? 0 : cp.Performances.Sum(p => p.Score * PerformanceWeight(p.RcoId, cp))),
                UserProfileImageFileId = user.Attachment != null ? user.Attachment.FileId : (int?)null
            };

        }

        private int PerformanceWeight(int id, PerformanceInfo performanceInfo)
        {
            if (performanceInfo.Course != null)
            {
                foreach (var lesson in performanceInfo.Course.Lessons)
                {
                    var weight = FindWeightForLesson(id, lesson);
                    if (weight.HasValue)
                    {
                        return weight.Value;
                    }
                }
            }
            return 1;
        }

        private int? FindWeightForLesson(int rcoId, RcoLesson lesson)
        {
            if (lesson.Id == rcoId)
            {
                return lesson.Weight.HasValue ? lesson.Weight.Value : 1;
            }
            foreach (var childLesson in lesson.Lessons)
            {
                var weight = FindWeightForLesson(rcoId, childLesson);
                if (weight.HasValue)
                {
                    return weight.Value;
                }
            }
            return null;
        }
    }
}