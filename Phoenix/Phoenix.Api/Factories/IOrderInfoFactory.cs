﻿using System.Collections.Generic;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Factories
{
    public interface IOrderInfoFactory
    {
        OrderInfo ConvertOrder(Order order);
        IEnumerable<OrderLine> GetOrderInfoLines(Order order);
    }
}
