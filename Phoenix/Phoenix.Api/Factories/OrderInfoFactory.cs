﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Metier.Phoenix.Api.Facade;
using Metier.Phoenix.Core.Data;
using Metier.Phoenix.Core.Data.Entities;
using Metier.Phoenix.Core.Data.Enums;
using Metier.Phoenix.Core.Data.Queryables;

namespace Metier.Phoenix.Api.Factories
{
    public class OrderInfoFactory : IOrderInfoFactory
    {
        private readonly MetierLmsContext _context;

        //TODO: After having given this some thought, I realize that this 
        //class is almost exclusively used for presentation purposes.
        //In other words, this logic should have been moved to the project Phoenix.Web.Admin
        //and handled in the presentation layer by the use of Javascript/Angular.
        public OrderInfoFactory(MetierLmsContext context)
        {
            _context = context;
        }

        public OrderInfo ConvertOrder(Order order)
        {
            var orderInfo = Mapper.Map<Order, OrderInfo>(order);

            orderInfo.Participants = order.ParticipantOrderLines.Select(l => l.Participant.Id).ToList().ToList();
            orderInfo.FixedPriceActivities = order.ActivityOrderLines.Select(a => a.Activity.Id).ToList();
            orderInfo.OrderLines = GetOrderInfoLines(order);

            orderInfo.CustomerName = order.Customer.Name;

            return orderInfo;
        }

        public IEnumerable<OrderLine> GetOrderInfoLines(Order order)
        {
            var orderLines = new List<OrderLine>();

            var activityOrderLines = order.ActivityOrderLines.Select(a => GetOrderLine(order, a));
            orderLines.AddRange(activityOrderLines.Where(o => o != null));

            var headerOrderLines = GetHeaderOrderLines(order).Select(h => GetOrderLine(order, h, OrderLineType.UnitPriceActivity));
            orderLines.AddRange(headerOrderLines.Where(o => o != null));

            var participantOrderLines = order.ParticipantOrderLines.Select(p => GetOrderLine(order, p));
            orderLines.AddRange(participantOrderLines);

            return orderLines;
        }

        private List<ActivityOrderLine> GetHeaderOrderLines(Order order)
        {
            var orderLines = new List<ActivityOrderLine>();

            var activityIds = order.ParticipantOrderLines.Select(l=> l.ActivityId);
            var activities = GetActivities(activityIds.ToArray());
            var participantActivityOrderLines = activities.Select(CreateHeaderOrderLine);

            orderLines.AddRange(participantActivityOrderLines.Where(line => line != null));

            return orderLines;
        }

        private IEnumerable<Activity> GetActivities(int[] activityIds)
        {
            return _context.Activities.Where(p => activityIds.Contains(p.Id)).Distinct().ToList(); //The 'Distinct' is really not nessesary, but anyways...
        }

        private ActivityOrderLine CreateHeaderOrderLine(Activity activity)
        {
            var customerArticle = _context.CustomerArticles.FindById(activity.CustomerArticleId, _context.Articles);

            return new ActivityOrderLine
            {
                Id = activity.Id,
                ActivityName = activity.Name,
                Activity = activity,
                ArticlePath = customerArticle.ArticlePath,
            };
        }

        private OrderLine GetOrderLine(Order order, ParticipantOrderLine orderLine)
        {
            var infoLine = new OrderLine
            {
                Id = orderLine.Id,
                ParticipantId = orderLine.Participant.Id,
                ValueAddedTax = 0,
                Article = "",
                Type = OrderLineType.Participant
            };

            if (order.Status >= OrderStatus.Transferred || order.IsCreditNote || order.IsFullCreditNote)
            {
                infoLine.ParentActivityId = orderLine.ActivityId;
                infoLine.CurrencyCodeId = orderLine.CurrencyCodeId.GetValueOrDefault();
                infoLine.Text = orderLine.InfoText;
                infoLine.Price = orderLine.Price.GetValueOrDefault();
            }
            else
            {
                infoLine.ParentActivityId = orderLine.Participant.ActivityId;
                infoLine.CurrencyCodeId = orderLine.Participant.CurrencyCodeId.GetValueOrDefault();
                infoLine.Text = orderLine.Participant.GetInfoTexts();
                infoLine.Price = orderLine.Participant.UnitPrice.GetValueOrDefault();
            }

            return infoLine;
        }

        private OrderLine GetOrderLine(Order order, ActivityOrderLine orderLine, OrderLineType orderLineType = OrderLineType.FixedPriceActivity)
        {
            var customerArticle = _context.CustomerArticles.FindById(orderLine.Activity.CustomerArticleId, _context.Articles);

            if (order.Status >= OrderStatus.Transferred || order.IsCreditNote || order.IsFullCreditNote)
            {
                return new OrderLine
                {
                    Id = orderLine.Id,
                    ParticipantId = null,
                    ParentActivityId = orderLine.Activity.Id,
                    CurrencyCodeId = orderLine.CurrencyCodeId.GetValueOrDefault(),
                    Text = orderLine.ActivityName,
                    Price = orderLine.Price.GetValueOrDefault(),
                    ValueAddedTax = 0,
                    Article = customerArticle.ArticlePath,
                    Type = orderLineType
                };
            }

            return new OrderLine
            {
                Id = orderLineType == OrderLineType.UnitPriceActivity ? orderLine.Activity.Id : orderLine.Id,
                ParticipantId = null,
                ParentActivityId = orderLine.Activity.Id,
                CurrencyCodeId = orderLine.Activity.CurrencyCodeId.GetValueOrDefault(),
                Text = orderLine.Activity.Name,
                Price = orderLine.Activity.FixedPrice.GetValueOrDefault(),
                ValueAddedTax = 0,
                Article = customerArticle.ArticlePath,
                Type = orderLineType
            };
        }

    }
}
