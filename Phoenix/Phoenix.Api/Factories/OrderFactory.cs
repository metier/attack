﻿using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Data.Repositories;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Factories
{
    public class OrderFactory : IOrderFactory
    {
        private readonly IParticipantRepository _participantRepository;

        public OrderFactory(IParticipantRepository participantRepository)
        {
            _participantRepository = participantRepository;
        }

        public Order CreateCreditNoteFromOrder(Order order)
        {
            var creditNoteOrder = CopyOrderValues(order);
            creditNoteOrder.IsCreditNote = true;
            creditNoteOrder.IsFullCreditNote = true;
            creditNoteOrder.CreditedOrderId = order.Id;

            return creditNoteOrder;
        }

        public Order CreateCreditNoteDraftFromOrder(Order order)
        {
            var creditNoteOrder = CopyOrderValues(order);
            creditNoteOrder.IsCreditNote = true;
            creditNoteOrder.CreditedOrderId = order.Id;

            return creditNoteOrder;
        }

        private static Order CopyOrderValues(Order order)
        {
            var creditNoteOrder = new Order
            {
                CoordinatorId = order.CoordinatorId,
                YourOrder = order.YourOrder,
                YourRef = order.YourRef,
                OurRef = order.OurRef,
                Title = order.Title,
                CustomerId = order.CustomerId,
                WorkOrder = order.WorkOrder,
                ParticipantOrderLines = new List<ParticipantOrderLine>(),
                ActivityOrderLines = new List<ActivityOrderLine>()
            };

            foreach(var line in order.ParticipantOrderLines)
            {
                creditNoteOrder.ParticipantOrderLines.Add(
                    new ParticipantOrderLine
                    {
                        Participant = line.Participant,
                        Order = creditNoteOrder,
                        ActivityName = line.ActivityName,
                        ActivityId = line.ActivityId,
                        Price = line.Price,
                        CurrencyCodeId = line.CurrencyCodeId,
                        InfoText = line.InfoText,
                        OnlinePaymentReference = line.OnlinePaymentReference
                    }
                    );
            }

            foreach (var line in order.ActivityOrderLines)
            {
                creditNoteOrder.ActivityOrderLines.Add(
                    new ActivityOrderLine
                    {
                        Activity = line.Activity,
                        Order = creditNoteOrder,
                        ActivityName = line.ActivityName,
                        Price = line.Price,
                        CurrencyCodeId = line.CurrencyCodeId,
                        ArticlePath = line.ArticlePath
                    }
                    );
            }

            return creditNoteOrder;
        }

        //Consider refactoring so only the Participants-class is used...
        public string GetOrderLineText(string participantFirstName, string participantLastName, Participant participant)
        {
            if (participant.ParticipantInfoElements == null || (participant.ParticipantInfoElements.Count > 0 && participant.ParticipantInfoElements.First().LeadingText == null))
            {
                participant = _participantRepository.FindById(participant.Id);
            }

            return participant.GetInfoTexts();
        }
    }
}