﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metier.Phoenix.Api.Facade.Participant;
using Metier.Phoenix.Api.Providers;
using Metier.Phoenix.Core.Data.Codes;
using Metier.Phoenix.Core.Data.Entities;

namespace Metier.Phoenix.Api.Factories
{
    public class ParticipantFactory : IParticipantFactory
    {
        private readonly IPerformanceProvider _performanceProvider;

        public ParticipantFactory(IPerformanceProvider performanceProvider)
        {
            _performanceProvider = performanceProvider;
        }

        public Participant Create(User user, Enrollment enrollment, Activity activity, int activitySetId, IEnumerable<UserEnrollInfoElement> infoElements, string paymentReference = null, Customer customer = null, decimal? overridePrice = null, int? overrideCurrencyCodeId = null)
        {
            var customerId = customer?.Id ?? user.CustomerId;

            return new Participant
            {
                UserId = user.Id,
                CustomerId = customerId,
                ActivityId = activity.Id,
                ActivitySetId = activitySetId,
                EarliestInvoiceDate = activity.EarliestInvoiceDate > DateTime.UtcNow ? activity.EarliestInvoiceDate : DateTime.UtcNow,
                UnitPrice = overridePrice ?? activity.UnitPrice,
                CurrencyCodeId = (overrideCurrencyCodeId.HasValue && overrideCurrencyCodeId.Value > 0 ? overrideCurrencyCodeId.Value : activity.CurrencyCodeId),
                StatusCodeValue = GetStatus(user, activity),
                IsNotBillable = user.ParticipantNotBillable || activity.UnitPriceNotBillable,
                ParticipantInfoElements = GetParticipantInfoElements(user, infoElements),
                OnlinePaymentReference = paymentReference,
                Created = DateTime.UtcNow,
                EnrollmentId = enrollment.Id
            };
        }

        private string GetStatus(User user, Activity activity)
        {
            ScormPerformance performance = null;
            if (activity.RcoCourseId.HasValue)
            {
                performance = _performanceProvider.GetParentPerformance(user.Id, activity.RcoCourseId.Value);   
            }
            if (activity.RcoExamContainerId.HasValue)
            {
                performance = _performanceProvider.GetParentPerformance(user.Id, activity.RcoExamContainerId.Value);
            }
            if (performance != null)
            {
                switch (performance.Status)
                {
                    case ScormLessonStatus.Complete:
                        return ParticipantStatusCodes.Completed;
                    case ScormLessonStatus.Incomplete:
                        return ParticipantStatusCodes.InProgress;
                }
            }
            return ParticipantStatusCodes.Enrolled;
        }

        private List<ParticipantInfoElement> GetParticipantInfoElements(User user, IEnumerable<UserEnrollInfoElement> infoElements)
        {
            var participantInfoElements = new List<ParticipantInfoElement>();

            if (user.UserInfoElements == null)
                throw new Exception("UserInfoElements must be loaded before invoking this method.");

            if (infoElements != null)
            {
                participantInfoElements.AddRange(infoElements.Select(infoElement => new ParticipantInfoElement
                {
                    LeadingTextId = infoElement.LeadingTextId,
                    InfoText = infoElement.InfoText
                }).Where(pie=> pie.InfoText != null));
            }

            foreach (var userInfoElement in user.UserInfoElements)
            {
                if(participantInfoElements.All(ie => ie.LeadingTextId != userInfoElement.LeadingTextId))
                    participantInfoElements.Add(new ParticipantInfoElement { LeadingTextId = userInfoElement.LeadingTextId, InfoText = userInfoElement.InfoText });
            }

            return participantInfoElements;
        }
    }
}