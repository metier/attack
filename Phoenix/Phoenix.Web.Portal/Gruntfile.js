﻿module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            development: {
                options: {
                    paths: ['app/styles']
                },
                files: {
                    'app/styles/app-compiled.css': 'app/styles/app.less'
                }
            }
        },
        cssmin: {
            minify: {
                expand: true,
                cwd: 'app/styles',
                src: ['app-compiled.css'],
                dest: 'app/styles',
                ext: '.min.css',
                options: {
                    banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
					        '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
					        '* Copyright (c) <%= grunt.template.today("yyyy") %> */'
                }
            }
        },
        jshint: {
            options: {
                jshintrc: true
            },
            all: ['app/portal/**/*.js', 'app/portal*/**/*.js']
        },
        watch: {
            options: {
                livereload: true
            },
            htmlfiles: {
                files: ['app/portal/**/*.html', 'app/portal*/**/*.html']
            },
            styles: {
                files: ['app/styles/**/*.less', 'app/portal/**/*.less', 'app/portal*/**/*.less'],
                tasks: ['less', 'cssmin']
            },
            scripts: {
                files: ['.jshintrc', 'app/portal/**/*.js', 'app/portal*/**/*.js'],
                tasks: ['jshint']
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'app/assets/img_non_optimized',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'app/assets/img/'
                }]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('server', ['connect:livereload', 'watch']);

    grunt.registerTask('default', ['less', 'cssmin']);
};