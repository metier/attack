﻿angular.module('portal.exam').factory('examService', [
    '$http',
    '$q',
    '$timeout',
    '$translate',
    '$modal',
    'portalConfig',
    'Exam',
    function ($http, $q, $timeout, $translate, $modal, portalConfig, Exam) {
        'use strict';
       
        function getMultipleChoiceAnswersFromBindable(answer) {
            return _.chain(answer.BindableMultipleChoiceAnswers)
                                            .reject(function (a) { return a === undefined || a === null || a === 'null'; })
                                            .map(function (a) { return parseInt(a, 10); })
                                            .value();
        }
        return {
            // GET exams/info?activiyId : ExamInfo
            getInfo: function (activityId) {
                var defer = $q.defer();
                var url = portalConfig.apiPath + 'exams/info?activityId=' + activityId;
                $http.get(url, { interceptErrors: true }).success(function (examInfo) {
                    examInfo.NumberOfPages = Math.ceil(examInfo.NumberOfQuestions / examInfo.NumberOfQuestionsPerPage);
                    if (examInfo.Exam) {
                        examInfo.Exam = new Exam(examInfo.Exam);
                    }
                    defer.resolve(examInfo);
                }).error(function (error) {
                    defer.reject(error);
                });
                
                return defer.promise;
            },
            // POST exams/start?activityId : Exam
            startExam: function (activityId) {
                var defer = $q.defer();
                var url = portalConfig.apiPath + 'exams/start?activityId=' + activityId;
                $http.post(url, null, { interceptErrors: false }).success(function (exam) {
                    defer.resolve(new Exam(exam));
                }).error(function (error) {
                    defer.reject(error);
                });
                return defer.promise;
            },
            // GET exams/{id}/status : ExamStatus
            getExamStatus: function (examId) {
                var url = portalConfig.apiPath + 'exams/' + examId +'/status';
                return $http.get(url, { interceptErrors: true });
            },
            // PUT exams/{id}/answer?questionId : NO CONTENT
            submitAnswer: function (examId, answer) {
                answer.MultipleChoiceAnswers = getMultipleChoiceAnswersFromBindable(answer);
                var answerToSubmit = {
                    Id: answer.Id,
                    QuestionId: answer.QuestionId,
                    Type: answer.Type,
                    BoolAnswer: answer.BoolAnswer,
                    MultipleChoiceAnswers: answer.MultipleChoiceAnswers,
                    SingleChoiceAnswer: answer.SingleChoiceAnswer,
                    IsMarkedForReview: answer.IsMarkedForReview
                };

                var url = portalConfig.apiPath + 'exams/' + examId + '/answer';
                return $http.put(url, answerToSubmit, { interceptErrors: true });
            },
            // PUT exams/{id}/submit : ExamResult
            submitExam: function(examId) {
                var url = portalConfig.apiPath + 'exams/' +examId + '/submit';
                return $http.put(url, null, { interceptErrors: false });
            },
            showSubmitExamModal: function (exam) {
                var modalInstance = $modal.open({
                    templateUrl: portalConfig.basePath + 'app/portal.exam/views/modals/submit-exam.html',
                    controller: 'SubmitExamCtrl',
                    backdrop: 'static',
                    resolve: {
                        exam: function () { return exam; }
                    }
                });

                return modalInstance.result;
            },
            showConnectionErrorModal: function (exam) {
                $modal.open({
                    templateUrl: portalConfig.basePath + 'app/portal.exam/views/modals/connection-error.html',
                    controller: 'ConnectionErrorCtrl',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        exam: function () { return exam; }
                    }
            });
        }
        };
    }
]);