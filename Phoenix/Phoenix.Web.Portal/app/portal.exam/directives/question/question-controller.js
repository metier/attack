﻿angular.module('portal.exam').controller('QuestionCtrl', [
    '$scope',
    '$modal',
    '$window',
    'portalConfig',
    'examService',
    function ($scope, $modal, $window, portalConfig, examService) {
        'use strict';
        function answerSaved(answer) {
            if ($scope.onAnswerSaved) {
                $scope.onAnswerSaved();
            }

            answer.isSaving = false;
            if (answer.hasChanges) {
                answer.hasChanges = false;
                $scope.saveAnswer(answer);
            }
        }

        $scope.saveAnswer = function (answer) {
            if (answer.isSaving) {
                answer.hasChanges = true;
                return;
            }
            answer.isSaving = true;
            examService.submitAnswer($scope.exam.Id, answer).then(function () {
                answerSaved(answer);
            }, function (error) {
                if (error.status === 0 || error.status >= 500) {
                    examService.showConnectionErrorModal($scope.exam);
                }
            });
        };
    }
]);