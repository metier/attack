﻿angular.module('portal.exam').directive('question', [
    'portalConfig',
    function (config) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                question: '=',
                answer: '=',
                exam: '=',
                onAnswerSaved: '&'
            },
            templateUrl: config.basePath + 'app/portal.exam/directives/question/question.html',
            controller: 'QuestionCtrl'
        };
    }
]);