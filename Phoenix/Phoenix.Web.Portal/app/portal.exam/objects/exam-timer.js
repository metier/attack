﻿angular.module('portal.exam').factory('ExamTimer', [
    '$interval',
    'Timer',
    'examService',
    function ($interval, Timer, examService) {
        'use strict';

        function isDanger(secondsRemaining) {
            return secondsRemaining <= 901; // If less than 15 minutes remaining
        }

        function ExamTimer(exam) {
            var self = this;
            self.exam = exam;
            self.isDanger = isDanger(self.exam.Status.TimeRemainingInSeconds);
            Timer.call(self, self.exam.Status.TimeRemainingInSeconds);
            
            // Tick the timer every second
            self.examTick = $interval(function () {
                self.isDanger = isDanger(self.secondsRemaining);
            }, 1000);

            // Update the seconds remaining from the server every X minutes
            self.updateTick = $interval(function() {
                if (self.exam && self.isRunning) {
                    examService.getExamStatus(self.exam.Id).then(function(result) {
                        self.secondsRemaining = result.data.TimeRemainingInSeconds;
                    });
                }
            }, 60000);
        }

        ExamTimer.prototype = _.create(Timer.prototype);

        ExamTimer.prototype.stop = function () {
            $interval.cancel(this.examTick);
            $interval.cancel(this.updateTick);
            Timer.prototype.stop.call(this);
        };

        return ExamTimer;
    }
]);