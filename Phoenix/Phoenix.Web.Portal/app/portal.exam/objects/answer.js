﻿angular.module('portal.exam').factory('Answer', [
    function () {
        'use strict';

        function Answer(answer) {
            _.assign(this, answer);
        }

        Answer.prototype = {
            isAnswered: function () {
                return (_.isNull(this.BoolAnswer) || _.isUndefined(this.BoolAnswer)) &&
                       (_.isNull(this.MultipleChoiceAnswers) || _.isUndefined(this.MultipleChoiceAnswers) || this.MultipleChoiceAnswers.length === 0) &&
                       (_.isNull(this.SingleChoiceAnswer) || _.isUndefined(this.SingleChoiceAnswer));
            }
        };

        return Answer;
    }
]);