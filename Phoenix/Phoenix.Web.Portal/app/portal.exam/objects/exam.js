﻿angular.module('portal.exam').factory('Exam', [
    'Answer',
    function (Answer) {
        'use strict';
        function setBindableMultipleChoiceAnswers(exam) {
            _(exam.Answers).forEach(function (a) {
                var question = _(exam.Questions).find(function (q) { return q.Id === a.QuestionId; });
                if (question && question.Type === 'M') {
                    a.BindableMultipleChoiceAnswers = [];
                    _(question.Options).forEach(function (o) {
                        var isChecked = _(a.MultipleChoiceAnswers).some(function (ma) { return ma === o.Id; });
                        if (isChecked) {
                            a.BindableMultipleChoiceAnswers.push(o.Id.toString());
                        } else {
                            a.BindableMultipleChoiceAnswers.push('null');
                        }
                    });
                }
            });
        }
        
        function Exam(exam) {
            var self = this;
            setBindableMultipleChoiceAnswers(exam);
            _.assign(self, exam);
            _.each(self.Answers, function (answer, index) {
                self.Answers[index] = new Answer(answer);
            });
        }

        Exam.prototype = {
            getAnswersMarkedForReview: function() {
                return _(this.Answers).filter(function (a) { return a.IsMarkedForReview; }).value();
            },
            getAnswersNotAnswered: function () {
                return _(this.Answers).filter(function (a) { return a.isAnswered(); }).value();
            },
            getAnswersMarkedForReviewAndNotAnswered: function () {
                return _(this.Answers).filter(function (a) { return a.IsMarkedForReview && a.isAnswered(); }).value();
            },
            canStart: function() {
                
            },
            secondsBeforeStart: function() {
                
            }
        };

        return Exam;
    }
]);