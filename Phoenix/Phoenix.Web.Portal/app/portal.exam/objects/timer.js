﻿angular.module('portal.exam').factory('Timer', [
    '$interval',
    function ($interval) {
        'use strict';

        function twoDigits(number) {
            return ("0" + number).slice(-2);
        }

        function getTimerText(secondsRemaining) {
            var dur = moment.duration({ seconds: secondsRemaining });
            return twoDigits(dur.hours()) + ':' + twoDigits(dur.minutes()) + ':' + twoDigits(dur.seconds());
        }

        function secondsPassedSinceLastTick(previousTickTimestamp) {
            var currentTimestamp = Date.now();
            return Math.round((currentTimestamp - previousTickTimestamp) / 1000);
        }

        function Timer(initialValue) {
            var self = this;
            self.previousTickTimestamp = Date.now();
            self.secondsRemaining = initialValue;
            self.isRunning = false;
            self.timerText = getTimerText(self.secondsRemaining);

            // Tick the timer every second
            self.tick = $interval(function () {
                if (self.isRunning) {
                    if (self.secondsRemaining > 0) {
                        self.secondsRemaining = self.secondsRemaining - secondsPassedSinceLastTick(self.previousTickTimestamp);
                        self.previousTickTimestamp = Date.now();

                        self.timerText = getTimerText(self.secondsRemaining);
                        self.onTextUpdated();

                        if (self.secondsRemaining === 0) {
                            self.stop();
                            self.onTimerAutoStopped();
                        }
                    }
                }
            }, 1000);
        }

        Timer.prototype = {
            start: function () {
                this.isRunning = true;
            },
            stop: function () {
                this.isRunning = false;
                $interval.cancel(this.tick);
                this.onStopped();
            },
            onTextUpdated: function () { },
            onStopped: function () { },
            onTimerAutoStopped: function (){ }
        };

        return Timer;
    }
]);