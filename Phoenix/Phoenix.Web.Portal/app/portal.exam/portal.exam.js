﻿angular.module('portal.exam', [
    'pascalprecht.translate',
    'portal.shared',
    'portal.config',
    'ngAnimate',
    'ui.router',
    'ui.bootstrap',
    'tmh.dynamicLocale'
]).config([
    '$stateProvider',
    '$urlRouterProvider',
    '$httpProvider',
    'portalConfig',
    function ($stateProvider, $urlRouterProvider, $httpProvider, portalConfig) {
        'use strict';

        var onEnterRedirectToResultsIfSubmitted = ['$state', '$stateParams', 'examInfo', function ($state, $stateParams, examInfo) {
            var exam = examInfo.Exam;
            if (exam) {
                if (exam.Status && (exam.Status.IsSubmitted || exam.Status.TimeRemainingInSeconds === 0)) {
                    $state.go('exam.result', { activityId: $stateParams.activityId }, { location: 'replace' });
                }
            }
        }];

        $stateProvider.state('exam', {
            url: '/exam?activityId',
            abstract: true,
            controller: 'ExamCtrl',
            templateUrl: portalConfig.basePath + 'app/portal.exam/views/exam.html',
            resolve: {
                examInfo: ['examService', '$stateParams', function (examService, $stateParams) {
                    if (!$stateParams.activityId) {
                        throw 'Activity ID is not defined';
                    }
                    return examService.getInfo($stateParams.activityId);
                }]
            }
        }).state('exam.home', {
            url: '',
            templateUrl: portalConfig.basePath + 'app/portal.exam/views/exam-home.html',
            controller: 'ExamHomeCtrl',
            onEnter: onEnterRedirectToResultsIfSubmitted
        }).state('exam.questions', {
            url: '/questions?page',
            templateUrl: portalConfig.basePath + 'app/portal.exam/views/exam-questions.html',
            controller: 'ExamQuestionsCtrl',
            resolve: {
                questions: ['examInfo', '$stateParams', function (examInfo,  $stateParams) {
                    if (!$stateParams.page) {
                        $stateParams.page = 1;
                    }
                    var page = parseInt($stateParams.page, 10);
                    var exam = examInfo.Exam;
                    var startIndex = page <= 1 ? 0 : (page - 1) * examInfo.NumberOfQuestionsPerPage;
                    var endIndex = page * examInfo.NumberOfQuestionsPerPage;
                    if (endIndex > exam.Questions.length) {
                        endIndex = exam.Questions.length;
                    }
                    return exam.Questions.slice(startIndex, endIndex);
                }]
            },
            onEnter: onEnterRedirectToResultsIfSubmitted
        }).state('exam.summary', {
            url: '/summary',
            templateUrl: portalConfig.basePath + 'app/portal.exam/views/exam-summary.html',
            controller: 'ExamSummaryCtrl',
            onEnter: onEnterRedirectToResultsIfSubmitted
        }).state('exam.result', {
            url: '/result',
            templateUrl: portalConfig.basePath + 'app/portal.exam/views/exam-result.html',
            controller: 'ExamResultCtrl'
        }).state('examerror', {
            url: '/error',
            templateUrl: portalConfig.basePath + 'app/portal.exam/views/errors/error.html'
        }).state('examerror.noparticipant', {
            url: '/noparticipant',
            templateUrl: portalConfig.basePath + 'app/portal.exam/views/errors/no-participant.html'
        });

        $httpProvider.interceptors.push('errorHandlingHttpInterceptor');
    }
]).run([
    '$rootScope',
    '$state',
    '$http',
    '$templateCache',
    'portalConfig',
    function($rootScope, $state, $http, $templateCache, portalConfig) {
        'use strict';
        $rootScope.$on('$stateChangeError', function(event, toState, toParams) {
            if (toState.name !== 'exam.home') {
                $state.go('exam.home', { activityId: toParams.activityId }, { location: 'replace' });
            }
        });

        // Load templates into $templateCache
        $http.get(portalConfig.basePath + 'app/portal.exam/views/modals/connection-error.html', { cache: $templateCache });
    }
]);