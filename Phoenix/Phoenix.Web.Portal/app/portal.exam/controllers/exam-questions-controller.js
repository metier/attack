﻿angular.module('portal.exam').controller('ExamQuestionsCtrl', [
    '$scope',
    '$state',
    '$stateParams',
    'examService',
    'questions',
    'examInfo',
    function ($scope, $state, $stateParams, examService, questions, examInfo) {
        'use strict';
        $scope.questions = questions;
        $scope.exam = examInfo.Exam;

        if (!examInfo.Exam) {
            $state.go('exam.home', { activityId: $stateParams.activityId }, { location: 'replace' });
            return;
        }

        $scope.getAnswer = function (question) {
            return _($scope.exam.Answers).find(function (a) { return a.QuestionId === question.Id; });
        };
    }
]);