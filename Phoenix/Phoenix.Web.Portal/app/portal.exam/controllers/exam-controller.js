﻿angular.module('portal.exam').controller('ExamCtrl', [
    '$scope',
    '$state',
    '$stateParams',
    '$translate',
    '$timeout',
    '$modal',
    '$window',
    'tmhDynamicLocale',
    'examInfo',
    'examService',
    'portalConfig',
    'ExamTimer',
    function ($scope, $state, $stateParams, $translate, $timeout, $modal, $window, tmhDynamicLocale, examInfo, examService, portalConfig, ExamTimer) {
        'use strict';
        var timer;
        var exam = examInfo.Exam;
        var dangerModal;
        $scope.isCollapsed = true;
        $scope.isTimerVisible = false;

        // Supported languages with translations in 'app/portal.exam/translations/translation_XX.js'
        $scope.languages = [
            { locale: 'en', display: 'English' },
            { locale: 'no', display: 'Norwegian' },
            { locale: 'pt', display: 'Portuguese' },
            { locale: 'da', display: 'Danish' },
            { locale: 'de', display: 'German' },
            { locale: 'es', display: 'Spanish' },
            { locale: 'fr', display: 'French' },
//            { locale: 'it', display: 'Italian' },
            { locale: 'nl', display: 'Dutch' },
            { locale: 'pl', display: 'Polish' },            
            { locale: 'sv', display: 'Swedish' }
        ];

        function showTimeIsRunningOutModal() {
            dangerModal = $modal.open({
                templateUrl: portalConfig.basePath + 'app/portal.exam/views/modals/time-running-out.html',
                controller: 'TimeRunningOutCtrl',
                backdrop: 'static',
                resolve: {
                    secondsRemaining: function () { return timer.secondsRemaining; }
                }
            });
            dangerModal.isOpen = true;
            dangerModal.result['catch'](function (result) {
                dangerModal.isOpen = false;
                if (result === 'summary') {
                    $state.go('exam.summary');
                }
            });
        }

        function showModalIfTimeIsRunningOut() {
            if (!timer.isDanger) {
                return;
            }
            if (dangerModal) {
                // Only show it one time
                return;
            }
            showTimeIsRunningOutModal();
        }

        function initTimer() {
            if (timer) {
                $scope.isTimerVisible = true;
                return;
            }
            
            timer = new ExamTimer(exam);
            $scope.timerText = timer.timerText;
            $scope.isTimerVisible = true;
            showModalIfTimeIsRunningOut();

            timer.onTextUpdated = function () {
                $scope.timerText = timer.timerText;
                $scope.isTimerDanger = timer.isDanger;
                showModalIfTimeIsRunningOut();
            };

            timer.onTimerAutoStopped = function () {
                if (dangerModal && dangerModal.isOpen) {
                    dangerModal.close();
                }
                
                if (!$state.is('exam.result')) {
                    $state.go('exam.result', { activityId: $stateParams.activityId }, { reload: true });
                }
            };

            timer.start();
        }

        function init() {
            $scope.examInfo = examInfo;
            $scope.setLanguage(examInfo.Language);
        }

        function getLanguage(locale) {
            return _($scope.languages).find(function (lan) { return lan.locale === locale; });
        }

        bindEvent($window, "offline", function() {
            examService.showConnectionErrorModal(exam);
        }, true);

        $scope.setLanguage = function (locale) {
            // Select language, default to english
            $scope.currentLanguage = getLanguage(locale) || getLanguage('en');
            tmhDynamicLocale.set($scope.currentLanguage.locale);
            $translate.use($scope.currentLanguage.locale);
        };
        
        $scope.navigateForward = function () {
            $scope.direction = 'next';
            var currentPage = parseInt($state.params.page, 10);
            $state.go('exam.questions', { page: ++currentPage });
        };

        $scope.navigateBack = function () {
            $scope.direction = 'back';
            var currentPage = parseInt($state.params.page, 10);
            $state.go('exam.questions', { page: --currentPage });
        };
        
        $scope.submit = function () {
            examService.showSubmitExamModal(exam).then(function () {
                $state.go('exam.result', { activityId: $stateParams.activityId }, { reload: true });
            }, function(result) {
                if (result === 'summary') {
                    $state.go('exam.summary');
                }
            });
        };

        $scope.summary = function () {
            $state.go('exam.summary');
        };

        $scope.$on('$stateChangeStart', function (event, toState) {
            if (timer && toState.name === 'exam.result') {
                timer.stop();
            }
        });

        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            // Init navbar from states
            $scope.isHomeLinkVisible = true;
            $scope.isNavigationVisible = false;
            $scope.isTimerVisible = false;
            $scope.isSubmitButtonVisible = false;
            $scope.isSummaryButtonVisible = false;
            $scope.isContinueButtonVisible = false;
            $scope.isLanguagesVisible = false;
            $scope.continueToPage = 1;

            if (toState.name === 'exam.home') {
                $scope.isHomeLinkVisible = false;
                if (exam) {
                    initTimer();
                    $scope.isNavigationVisible = false;
                    $scope.isSubmitButtonVisible = false;
                    $scope.isSummaryButtonVisible = false;
                }
            }
            if (toState.name === 'exam.questions') {
                if (exam) {
                    initTimer();
                    $scope.isNavigationVisible = !!exam && examInfo.NumberOfQuestions > examInfo.NumberOfQuestionsPerPage;
                    $scope.currentPage = parseInt(toParams.page, 10);
                    $scope.numberOfPages = examInfo.NumberOfPages;
                    $scope.isFirstPage = $scope.currentPage <= 1;
                    
                    $scope.isLastPage = $scope.currentPage === examInfo.NumberOfPages;
                    $scope.isSubmitButtonVisible = true;
                    $scope.isSummaryButtonVisible = true;
                }
            }
            if (toState.name === 'exam.summary') {
                $scope.continueToPage = fromParams.page || 1;
                if (exam) {
                    initTimer();
                    $scope.isContinueButtonVisible = true;
                    $scope.isSubmitButtonVisible = true;
                    $scope.isSummaryButtonVisible = true;
                }
            }
            if (toState.name === 'exam.result') {
                $scope.isHomeLinkVisible = false;
            }
        });
            
        init();
    }
]);