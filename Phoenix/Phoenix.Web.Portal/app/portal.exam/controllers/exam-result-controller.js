﻿angular.module('portal.exam').controller('ExamResultCtrl', [
    '$scope',
    '$state',
    '$stateParams',
    'examInfo',
    function ($scope, $state, $stateParams, examInfo) {
        'use strict';
        $scope.examInfo = examInfo;

        if (!examInfo.Exam || !examInfo.Result) {
            $state.go('exam.home', { activityId: $stateParams.activityId }, { location: 'replace' });
            return;
        }

        if (examInfo.Result.ScorePercentage !== null) {
            $scope.percentageVariables = {
                percentage: Math.floor(examInfo.Result.ScorePercentage * 100),
                numberOfQuestions: examInfo.NumberOfQuestions
            };
        }
    }
]);