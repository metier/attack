﻿angular.module('portal.exam').controller('ExamHomeCtrl', [
    '$scope',
    '$state',
    'examInfo',
    'examService',
    'Timer',
    function ($scope, $state, examInfo, examService, Timer) {
        'use strict';

        function initTimer(timeBeforeAvailableInSeconds) {
            var availabilityTimer = new Timer(timeBeforeAvailableInSeconds);

            $scope.showTimer = false;
            $scope.isExamAvailable = false;
            $scope.timerText = availabilityTimer.timerText;
            
            availabilityTimer.onTextUpdated = function () {
                $scope.timerText = availabilityTimer.timerText;
                if (availabilityTimer.secondsRemaining <= 0) {
                    $scope.isExamAvailable = true;
                    $scope.showTimer = false;
                }
            };

            if (_.isNumber(timeBeforeAvailableInSeconds)) {
                if (timeBeforeAvailableInSeconds > 0) {
                    // Exam is not yet available
                    availabilityTimer.start();
                    $scope.showTimer = true;
                    $scope.isExamAvailable = false;
                } else {
                    // Exam is available
                    $scope.showTimer = false;
                    $scope.isExamAvailable = true;
                }
            }
        }

        function init() {
            $scope.examInfo = examInfo;
            $scope.exam = examInfo.Exam;
            $scope.durationInMinutes = examInfo.DurationInSeconds / 60;

            initTimer($scope.examInfo.TimeBeforeAvailableInSeconds);
        }
        
        init();

        $scope.closeErrorMsg = function () {
            $scope.showErrorMsg = false;
        };

        $scope.start = function () {
            $scope.isStartingExam = true;
            $scope.showErrorMsg = false;
            examService.startExam($state.params.activityId).then(function () {
                $state.go('exam.questions', { page: 1 }, { reload: true });
            }, function (error) {
                var knownErrorCodes = [2002, 2003];
                if (_(knownErrorCodes).contains(error.ErrorCode)) {
                    $scope.errorCode = error.ErrorCode;
                } else {
                    $scope.errorCode = null;
                }
                
                $scope.showErrorMsg = true;
            })['finally'](function() {
                $scope.isStartingExam = false;
            });
        };

        $scope.continueExam = function () {
            $state.go('exam.questions', { page: 1 });
        };
    }
]);