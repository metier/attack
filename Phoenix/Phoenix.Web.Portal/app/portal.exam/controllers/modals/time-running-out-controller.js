﻿angular.module('portal.exam').controller('TimeRunningOutCtrl', [
    '$scope',
    '$modalInstance',
    'secondsRemaining',
    function ($scope, $modalInstance, secondsRemaining) {
        'use strict';
        $scope.minutesRemaining = Math.ceil(secondsRemaining / 60);

        $scope.summary = function () {
            $modalInstance.dismiss('summary');
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }
]);