﻿angular.module('portal.exam').controller('UnexpectedErrorCtrl', [
    '$scope',
    '$window',
    function ($scope, $window) {
        'use strict';
        
        $scope.reloadPage = function () {
            $window.location.reload();
        };
    }
]); 