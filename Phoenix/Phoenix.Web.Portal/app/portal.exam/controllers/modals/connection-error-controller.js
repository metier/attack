﻿angular.module('portal.exam').controller('ConnectionErrorCtrl', [
    '$scope',
    '$window',
    '$q',
    '$timeout',
    '$interval',
    'exam',
    'examService',
    function ($scope, $window, $q, $timeout, $interval, exam, examService) {
        'use strict';
        var initialRetryInterval = 4000;
        var maxRetryInterval = 20000;
        
        function tryGetStatus(examId, retryInterval, promise) {
            promise = promise || $q.defer();

            $scope.isRetrying = true;
            examService.getExamStatus(examId).then(function () {
                promise.resolve();
                $scope.isRetrying = false;
                $scope.reloadPage();
            }, function () {
                $scope.isRetrying = false;
                $scope.secondsToNextRetry = Math.ceil(retryInterval / 1000);
                $timeout(function () {
                    tryGetStatus(examId, Math.min(maxRetryInterval, retryInterval * 2), promise);
                }, retryInterval);
            });
        }
        
        function init() {
            $scope.secondsToNextRetry = 1;
            $scope.isRetrying = false;
            $scope.isOnline = $window.navigator.onLine;

            tryGetStatus(exam.Id, initialRetryInterval);
            $interval(function () {
                if ($scope.secondsToNextRetry > 1) {
                    $scope.secondsToNextRetry--;
                }
            }, 1000);
        }
        
        $scope.reloadPage = function () {
            $window.location.reload();
        };
        
        init();
    }
]); 