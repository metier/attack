﻿angular.module('portal.exam').controller('SubmitExamCtrl', [
    '$scope',
    '$modalInstance',
    '$translate',
    'exam',
    'examService',
    function ($scope, $modalInstance, $translate, exam, examService) {
        'use strict';
        $scope.submitMessageTranslateData = {
            numberOfAnsweredQuestions: exam.Answers.length - exam.getAnswersNotAnswered().length,
            numberOfQuestions: exam.Answers.length
        };

        $scope.summary = function () {
            $modalInstance.dismiss('summary');
        };

        $scope.closeErrorMsg = function () {
            $scope.showErrorMsg = false;
        };

        $scope.submit = function () {
            $scope.isSubmittingExam = true;
            $scope.showErrorMsg = false;
            examService.submitExam(exam.Id).then(function () {
                $scope.isSubmittingExam = false;
                $modalInstance.close();
            }, function () {
                $scope.showErrorMsg = true;
                $scope.isSubmittingExam = false;
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }
]);