﻿angular.module('portal.exam').controller('ExamSummaryCtrl', [
    '$scope',
    '$state',
    '$stateParams',
    'examInfo',
    'examService',
    function ($scope, $state, $stateParams, examInfo, examService) {
        'use strict';

        $scope.filterMarkedForReview = false;
        $scope.filterNotAnswered = false;
        $scope.examInfo = examInfo;

        if (!examInfo.Exam) {
            $state.go('exam.home', { activityId: $stateParams.activityId }, { location: 'replace' });
            return;
        }

        $scope.exam = examInfo.Exam;
        $scope.questions = examInfo.Exam.Questions;

        $scope.submit = function () {
            examService.showSubmitExamModal($scope.exam).then(function () {
                $state.go('exam.result', { activityId: $stateParams.activityId }, { reload: true });
            }, function (result) {
                if (result === 'summary') {
                    $state.go('exam.summary');
                }
            });
        };

        $scope.getAnswer = function (question) {
            return _($scope.exam.Answers).find(function (a) { return a.QuestionId === question.Id; });
        };

        $scope.filterQuestions = function () {
            var answersMarkedForReview = $scope.examInfo.Exam.getAnswersMarkedForReview();
            var answersNotAnswered = $scope.examInfo.Exam.getAnswersNotAnswered();

            $scope.numberOfQuestionsMarkedForReview = answersMarkedForReview.length;
            $scope.numberOfQuestionsNotAnswered = answersNotAnswered.length;
            
            var answers = $scope.examInfo.Exam.Answers;
            if ($scope.filterMarkedForReview && $scope.filterNotAnswered) {
                answers = $scope.examInfo.Exam.getAnswersMarkedForReviewAndNotAnswered();
            } else if ($scope.filterMarkedForReview) {
                answers = answersMarkedForReview;
            } else if ($scope.filterNotAnswered) {
                answers = answersNotAnswered;
            }

            $scope.questions = _(examInfo.Exam.Questions).filter(function (q) {
                return !!_(answers).find(function (a) { return a.QuestionId === q.Id; });
            }).value();
        };
        
        $scope.questionsSaved = function () {
            $scope.filterQuestions();
        };

        $scope.filterQuestions();
    }
]);