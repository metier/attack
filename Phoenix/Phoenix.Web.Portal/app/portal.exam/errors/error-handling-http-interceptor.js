﻿angular.module('portal.exam').factory('errorHandlingHttpInterceptor', [
    '$q',
    '$injector',
    '$window',
    '$location',
    'portalConfig',
    function ($q, $injector, $window, $location, config) {
        'use strict';
        var unexpectedErrorModal;
        function showUnexpectedErrorModal() {
            // Only show one at a time. The modal requires reload page as only "close" option
            if (unexpectedErrorModal) {
                return;
            }
            unexpectedErrorModal = $injector.get('$modal');
            unexpectedErrorModal.open({
                templateUrl: config.basePath + 'app/portal.exam/views/modals/unexpected-error.html',
                controller: 'UnexpectedErrorCtrl',
                backdrop: 'static',
                keyboard: false
            });
        }

        function redirectTo(stateName) {
            var $state = $injector.get('$state');
            $state.go(stateName, null, { location: 'replace' });
        }

        return {
            'responseError': function(rejection) {
                // Always intercept "Unauthorized" errors
                if (rejection.status === 401) {
                    $window.location = config.learningPortalLoginPath + '?referrer=' + encodeURI($location.absUrl());
                } else {
                    // Only intercept HTTP errors if $http config interceptErrors-flag is not set or is set to true
                    if (rejection.config.interceptErrors !== undefined && !rejection.config.interceptErrors) {
                        return $q.reject(rejection);
                    }

                    if (rejection.status === 400) {
                        if (rejection.data) {
                            if (rejection.data.ErrorCode) {
                                var errorCode = rejection.data.ErrorCode;
                                if (errorCode === 2001) {
                                    redirectTo('examerror.noparticipant');
                                } else {
                                    showUnexpectedErrorModal();
                                }
                            } else {
                                showUnexpectedErrorModal();
                            }
                        } else {
                            showUnexpectedErrorModal();
                        }
                    }
                }
                
                return $q.reject(rejection);
            }
        };
    }
]);