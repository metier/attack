﻿angular.module('portal.exam').config(['$translateProvider', function ($translateProvider) {
    'use strict';
    $translateProvider.translations('sv', {
        // Generic expressions
        'TRUE': 'Sant',
        'FALSE': 'Falskt',
        'CANCEL': 'Avbryt',
        'CLOSE': 'Stäng',

        // Menu
        'MENU_CONTINUE_EXAM': 'Fortsätt examen',
        'MENU_PREVIOUS': 'Föregående',
        'MENU_NEXT': 'Nästa',
        'MENU_FINISH_EXAM': 'Avsluta examen',
        'MENU_SUMMARY': 'Sammandrag',

        // Home page
        'HOME_START_EXAM': 'Starta examen',
        'HOME_STARTING_EXAM': 'Examen startas',
        'HOME_CONTINUE_EXAM': 'Fortsätt examen',
        'HOME_START_EXAM_ERROR_MSG': 'Ett fel uppstod då examen startades.',
        'HOME_START_EXAM_NOT_QUALIFIED_ERROR_MSG': 'Du är inte kvalificerad för att ta den här examen. Var vänlig och ta kontakt direkt med Metier Academy på telefon +46 733 837540.',
        'HOME_START_EXAM_NOT_ENOUGH_COMPLETED_COURSES_ERROR_MSG': 'Du har inte genomfört tillräckligt mycket kurs för att kunna starta den här examen. Var vänlig och ta kontakt med Metier Academy på telefon +46 733 837540',
        'HOME_MINUTES': 'minuter',
        'HOME_SECONDS': 'sekunder',
        'HOME_DURATION': 'Varaktighet',
        'HOME_AVAILABLE': 'Tillgänglig',
        'HOME_NUMBER_OF_QUESTIONS': 'Antal frågor',

        // Deliver exam modal
        'COMPLETE_EXAM_FINISH_EXAM': 'Avsluta examen',
        'COMPLETE_EXAM_SUBMIT_EXAM': 'Skicka in examen',
        'COMPLETE_EXAM_SUBMIT_EXAM_ERROR_MSG': 'Ett fel uppstod när du skickade in examen. Var snäll och försök igen.',
        'COMPLETE_EXAM_SUBMIT_MSG': 'Du har besvarat {{numberOfAnsweredQuestions}} av {{numberOfQuestions}} frågor. Klicka på "Skicka in examen" när du är säker på att du vill lämna in din examen. Om du vill få en översikt över de frågor du markerat för att gå tillbaka till, så klicka på "Sammandrag.',

        'COMPLETE_EXAM_SUMMARY': 'Sammandrag',

        // Results page
        'RESULT_EXAM_COMPLETED': 'Examen är genomförd',
        'RESULT_PERCENTAGE': 'Du hade {{percentage}}% rätta svar av {{numberOfQuestions}} frågor.',

        // Summary page
        'SUMMARY': 'Sammandrag',
        'SUMMARY_INFO': 'Om du vill ändra dina svar, så klicka på frågorna det gäller. Klicka på "Avsluta examen" när du är nöjd med dina svar.',
        'SUMMARY_MARK_FOR_REVIEW': 'Markera för att gå tillbaka till',
        'SUMMARY_MARKED_FOR_REVIEW': 'Markerad att gå tillbaka till',
        'SUMMARY_NOT_ANSWERED': 'Inte besvarad',
        'SUMMARY_SUBMIT_EXAM': 'Avsluta examen',

        // Questions page
        'QUESTIONS_INFO': 'Svara på examensfrågorna på den här sidan. Klicka på "Sammandrag" om du vill få en översikt över de frågor du markerat att du ska gå tillbaka till. Klicka på ”Avsluta examen” när du är färdig att sända in din examen.',
        'QUESTIONS_S_INFO': 'Kryssa i rätt svarsalternativ',
        'QUESTIONS_M_INFO': 'Kryssa i rätt svarsalternativ',
        'QUESTIONS_TF_INFO': 'Kryssa i rätt svarsalternativ',

        // Time is running out modal
        'TIME_RUNNING_OUT': 'Bara lite tid återstår',
        'TIME_RUNNING_OUT_MSG': 'Det är {{minutesRemaining}} minuter kvar av examenstiden. Kontrollera dina svar i god tid innan tiden är ute.',
        'TIME_RUNNING_OUT_SUMMARY': 'Sammandrag',

        // Connection lost modal
        'CONNECTION_ERROR': 'Anslutningsfel',
        'CONNECTION_ERROR_MSG_OFFLINE': 'Det verkar som om din dator har tappat internetanslutningen.',
        'CONNECTION_ERROR_MSG': 'Det har dessvärre uppstått tekniska problem. Din examen kommer strax att bli tillgänglig..',
        'CONNECTION_ERROR_MSG_CONTACT': 'Var vänlig och ta kontakt direkt med Metier Academy på telefon +46 733 837540.',
        'CONNECTION_ERROR_RETRYING_IN': 'Försöker igen',
        'CONNECTION_ERROR_CONNECTING': 'Ansluter',
        'CONNECTION_ERROR_RELOAD_PAGE': 'Ladda upp sidan igen',

        // Unexpected error modal
        'UNEXPECTED_ERROR': 'Oväntat fel',
        'UNEXPECTED_ERROR_MSG': 'Ett oväntat fel uppstod. Var snäll och ladda upp sidan igjen.',
        'UNEXPECTED_ERROR_RELOAD_PAGE': 'Ladda upp sidan igen'
    });
}]);