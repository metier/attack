﻿angular.module('portal.exam').config(['$translateProvider', function ($translateProvider) {
    'use strict';
    $translateProvider.translations('pt', {
               
        // Generic expressions
        'TRUE': ' Verdade',
        'FALSE': 'Falso',
        'CANCEL': 'Cancelar',
        'CLOSE': 'Fechar',
        
        // Menu
        'MENU_CONTINUE_EXAM': 'Continuar exame',
        'MENU_PREVIOUS': 'Retornar',
        'MENU_NEXT': ' Próxima',
        'MENU_FINISH_EXAM': 'Finalizar exame',
        'MENU_SUMMARY': 'Resumo',
 
        // Home page
        'HOME_START_EXAM': 'Iniciar exame',
        'HOME_STARTING_EXAM': 'Iniciando exame',
        'HOME_CONTINUE_EXAM': 'Continuar exame',
        'HOME_START_EXAM_ERROR_MSG': 'Ocorreu um erro ao tentar iniciar o seu exame.',
        'HOME_START_EXAM_NOT_QUALIFIED_ERROR_MSG': 'Você não está qualificado para este exame. Entre em contato com Metier Academy imediatamente por telefone +47 24 12 45 00.',
        'HOME_START_EXAM_NOT_ENOUGH_COMPLETED_COURSES_ERROR_MSG': 'Você não tenham completado cursos suficientes para iniciar este exame. Entre em contato com Metier Academia por telefone +47 24 12 45 00.',
        'HOME_MINUTES': 'minutos',
        'HOME_SECONDS': 'segundos',
        'HOME_DURATION': 'Duração',
        'HOME_AVAILABLE': 'Disponível',
        'HOME_NUMBER_OF_QUESTIONS': 'Número de perguntas',
        
        // Deliver exam modal
        'COMPLETE_EXAM_FINISH_EXAM': 'Terminar exame',
        'COMPLETE_EXAM_SUBMIT_EXAM': 'Enviar exame',
        'COMPLETE_EXAM_SUBMIT_EXAM_ERROR_MSG': 'Ocorreu um erro ao tentar enviar o seu exame. Por favor, tente novamente',
        'COMPLETE_EXAM_SUBMIT_MSG': 'Você respondeu {{numberOfQuestions}} de {{numberOfAnsweredQuestions}} perguntas  . Clique em "Enviar o exame" se você realmente deseja enviar seu exame. Se você deseja ter um resumo de suas perguntas que estão marcados para revisão, clique em "Resumo"',

        'COMPLETE_EXAM_SUMMARY': 'Resumo',
 
        // Results page
        'RESULT_EXAM_COMPLETED': 'Exame concluído',
        'RESULT_PERCENTAGE': 'Você tem {{percentual}}% correto de {{numberOfQuestions}} perguntas.', 
 
 
        // Summary page
        'SUMMARY': 'Resumo',
        'SUMMARY_INFO': 'Se você gostaria de mudar qualquer uma das suas respostas, clique sobre a resposta. Clique em " exame Finish" quando você estiver satisfeito com as respostas.',
        'SUMMARY_MARK_FOR_REVIEW': 'Marcar para revisão',
        'SUMMARY_MARKED_FOR_REVIEW': 'Marcadas para review',
        'SUMMARY_NOT_ANSWERED': 'Não respondeu',
        'SUMMARY_SUBMIT_EXAM': 'Finalizar exame',
 
        // Questions page
        'QUESTIONS_INFO': 'Responda às suas perguntas do exame nesta página. Se quiser ter obter uma visão geral das questões que estão marcadas para revisão, clique em "Resumo". Quando estver pronto para submeter o seu exame, clique em "Concluir o exame.',
        'QUESTIONS_S_INFO': 'Marque uma opção correta',
        'QUESTIONS_M_INFO': 'Verifique as opções corretas',
        'QUESTIONS_TF_INFO': 'Marque a opção correta',
 
        // Time is running out modal
        'TIME_RUNNING_OUT': 'Seu tempo está se esgotando',
        'TIME_RUNNING_OUT_MSG': ' {{minutos restantes}} minutos restantes para o exame. Certifique-se de olhar suas respostas antes que o tempo se esgote',

        'TIME_RUNNING_OUT_SUMMARY': 'Resumo',
 
        // Connection lost modal
        'CONNECTION_ERROR': 'Lost conexão ao servidor',
        'CONNECTION_ERROR_MSG_OFFLINE': 'Parece que seu computador tenha perdido o acesso à internet.',
        'CONNECTION_ERROR_MSG': 'No momento, estamos passando por dificuldades técnicas. Seu exame será em breve disponível.',
        'CONNECTION_ERROR_MSG_CONTACT': 'Por favor, entre em contato com Metier Academy imediatamente por telefone +47 24 12 45 00.',
        'CONNECTION_ERROR_RETRYING_IN': 'Tentando novamente em',
        'CONNECTION_ERROR_CONNECTING': 'Ligar',
        'CONNECTION_ERROR_RELOAD_PAGE': 'Recarregar página',
 
        // Unexpected error modal
        'UNEXPECTED_ERROR': 'Erro inesperado',
        'UNEXPECTED_ERROR_MSG': 'Ocorreu um erro inesperado. Por favor, recarregue a página e tente novamente.',
        'UNEXPECTED_ERROR_RELOAD_PAGE': 'Recarregar página'

    });
}]);