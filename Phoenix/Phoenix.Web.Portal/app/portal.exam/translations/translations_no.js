﻿angular.module('portal.exam').config(['$translateProvider', function ($translateProvider) {
    'use strict';
    $translateProvider.translations('no', {
        // Generic expressions
        'TRUE': 'Sant',
        'FALSE': 'Usant',
        'CANCEL': 'Avbryt',
        'CLOSE': 'Lukk',
        
        // Menu
        'MENU_CONTINUE_EXAM': 'Fortsett eksamen',
        'MENU_PREVIOUS': 'Forrige',
        'MENU_NEXT': 'Neste',
        'MENU_FINISH_EXAM': 'Fullfør eksamen',
        'MENU_SUMMARY': 'Sammendrag',

        // Home page
        'HOME_START_EXAM': 'Start eksamen',
        'HOME_STARTING_EXAM': 'Starter examen',
        'HOME_CONTINUE_EXAM': 'Fortsett eksamen',
        'HOME_START_EXAM_ERROR_MSG': 'En feil oppstod under oppstart av eksamen.',
        'HOME_START_EXAM_NOT_QUALIFIED_ERROR_MSG': 'Du er ikke kvalifisert for å ta denne eksamen. Vennligst ta kontakt med Metier Academy umiddelbart på telefon +47 24 12 45 00.',
        'HOME_START_EXAM_NOT_ENOUGH_COMPLETED_COURSES_ERROR_MSG': 'Du har ikke fullført nok kurs til å kunne starte denne eksamen. Vennligst ta kontakt med Metier Academy på telefon +47 24 12 45 00.',
        'HOME_MINUTES': 'minutter',
        'HOME_SECONDS': 'sekunder',
        'HOME_DURATION': 'Varighet',
        'HOME_AVAILABLE': 'Tilgjengelig',
        'HOME_NUMBER_OF_QUESTIONS': 'Antall spørsmål',
        
        // Deliver exam modal
        'COMPLETE_EXAM_FINISH_EXAM': 'Fullfør eksamen',
        'COMPLETE_EXAM_SUBMIT_EXAM': 'Send eksamen',
        'COMPLETE_EXAM_SUBMIT_EXAM_ERROR_MSG': 'En feil oppstod under innsending av eksamen. Vennligst prøv igjen.',
        'COMPLETE_EXAM_SUBMIT_MSG': 'Du har besvart {{numberOfAnsweredQuestions}} av {{numberOfQuestions}} spørsmål. Klikk "Send eksamen" hvis du er sikker på at du vil levere din eksamen. Ønsker du å få en oversikt over eksamensspørsmålene som er markert for vurdering klikker du på "Sammendrag".',
        'COMPLETE_EXAM_SUMMARY': 'Sammendrag',

        // Results page
        'RESULT_EXAM_COMPLETED': 'Eksamen er fullført',
        'RESULT_PERCENTAGE': 'Du hadde {{percentage}}% riktige svar av {{numberOfQuestions}} spørsmål.',

        // Summary page
        'SUMMARY': 'Sammendrag',
        'SUMMARY_INFO': 'Ønsker du å gjøre noen endringer på dine svar, klikk på spørsmålet. Klikk på "Fullfør eksamen" når du er fornøyd med svarene dine.',
        'SUMMARY_MARK_FOR_REVIEW': 'Merk for gjennomgang',
        'SUMMARY_MARKED_FOR_REVIEW': 'Markert for gjennomgang',
        'SUMMARY_NOT_ANSWERED': 'Ikke besvart',
        'SUMMARY_SUBMIT_EXAM': 'Fullfør eksamen',

        // Questions page
        'QUESTIONS_INFO': 'Svar på eksamensspørsmålene på denne siden. Klikk på "Sammendrag" dersom du ønsker å få en oversikt over hvilke spørsmål som er markert for vurdering. Klikk på "Fullfør eksamen" når du er klar for å levere din eksamen.',
        'QUESTIONS_S_INFO': 'Huk av for det korrekte svaralternativet',
        'QUESTIONS_M_INFO': 'Huk av for de korrekte svaralternativene',
        'QUESTIONS_TF_INFO': 'Huk av for det korrekte svaralternativet',
        
        // Time is running out modal
        'TIME_RUNNING_OUT': 'Liten tid igjen',
        'TIME_RUNNING_OUT_MSG': 'Det er {{minutesRemaining}} minutter igjen av eksamenstiden. Husk å se over dine eksamenssvar i god tid før tiden er ute.',
        'TIME_RUNNING_OUT_SUMMARY': 'Sammendrag',

        // Connection lost modal
        'CONNECTION_ERROR': 'Mistet tilkobling til tjener',
        'CONNECTION_ERROR_MSG_OFFLINE': 'Det ser ut til at datamaskinen din har mistet tilgang til internett.',
        'CONNECTION_ERROR_MSG': 'Det har dessverre oppstått tekniske problemer, din eksamen vil være tilgjengelig om kort tid.',
        'CONNECTION_ERROR_MSG_CONTACT': 'Vennligst ta kontakt med Metier Academy umiddelbart på telefon +47 24 12 45 00.',
        'CONNECTION_ERROR_RETRYING_IN': 'Prøver på nytt om',
        'CONNECTION_ERROR_CONNECTING': 'Kobler til',
        'CONNECTION_ERROR_RELOAD_PAGE': 'Last siden på nytt',

        // Unexpected error modal
        'UNEXPECTED_ERROR': 'Uventet feil',
        'UNEXPECTED_ERROR_MSG': 'En uventet feil oppstod. Vennligst last siden på nytt og prøv igjen.',
        'UNEXPECTED_ERROR_RELOAD_PAGE': 'Last siden på nytt'
    });
}]);