﻿angular.module('portal.exam').config(['$translateProvider', function ($translateProvider) {
    'use strict';
    $translateProvider.translations('es', {
        // Generic expressions
        'TRUE': 'Verdadero',
        'FALSE': 'Falso',  
        'CANCEL': 'Cancelar',  
        'CLOSE': 'Cerrar',  
        
        // Menu
        'MENU_CONTINUE_EXAM': 'Continuar el examen',  
        'MENU_PREVIOUS': 'Anterior', 
        'MENU_NEXT': 'Siguiente', 
        'MENU_FINISH_EXAM': 'Terminar el examen', 
        'MENU_SUMMARY': 'Resumen', 
 
        // Home page
        'HOME_START_EXAM': 'Iniciar el examen', 
        'HOME_STARTING_EXAM': 'Iniciando el examen',  
        'HOME_CONTINUE_EXAM': 'Continuar el examen', 
        'HOME_START_EXAM_ERROR_MSG': 'Ocurrió un error al tratar de iniciar el examen.', 
        'HOME_START_EXAM_NOT_QUALIFIED_ERROR_MSG': 'No estás calificado para este examen. Por favor contacta con "Metier Academy" inmediatamente por teléfono al número +47 24 12 45 00.',
        'HOME_START_EXAM_NOT_ENOUGH_COMPLETED_COURSES_ERROR_MSG': 'No has completado cursos suficientes para iniciar este examen. Por favor contacta con "Metier Academy" por teléfono al número +47 24 12 45 00.',
        'HOME_MINUTES': 'minutos', 
        'HOME_SECONDS': 'segundos', 
        'HOME_DURATION': 'Duración', 
        'HOME_AVAILABLE': 'Disponible', 
        'HOME_NUMBER_OF_QUESTIONS': 'Número de preguntas', 
        
    // Deliver exam modal
        'COMPLETE_EXAM_FINISH_EXAM': 'Finalizar el examen', 
        'COMPLETE_EXAM_SUBMIT_EXAM': 'Enviar el examen', 
        'COMPLETE_EXAM_SUBMIT_EXAM_ERROR_MSG': 'Un error ocurrió al tratar de enviar tu examen. Por favor intenta nuevamente.', 
        'COMPLETE_EXAM_SUBMIT_MSG': 'Has contestado {{numberOfAnsweredQuestions}} de {{numberOfQuestions}} preguntas. Selecciona la opción "Enviar el examen" si en realidad deseas enviar el examen. Si deseas obtener un resumen de las preguntas del examen que están marcadas para revisión, por favor selecciona "Resumen"', 
        'COMPLETE_EXAM_SUMMARY': 'Resumen', 
 
    // Results page
        'RESULT_EXAM_COMPLETED': 'Examen completado', 
        'RESULT_PERCENTAGE': 'Obtuviste {{percentage}}% correcto de {{numberOfQuestions}} preguntas.', 
 
    // Summary page
        'SUMMARY': 'Resumen', 
        'SUMMARY_INFO': 'Si deseas cambiar cualquiera de las preguntas, selecciona la pregunta. Selecciona "Finalizar el examen" cuando estés satisfecho con tus respuestas.',
        'SUMMARY_MARK_FOR_REVIEW': 'Marcar para revisión', 
        'SUMMARY_MARKED_FOR_REVIEW': 'Marcado para revisión', 
        'SUMMARY_NOT_ANSWERED': 'No contestado', 
        'SUMMARY_SUBMIT_EXAM': 'Finalizar el examen', 
 
    // Questions page
        'QUESTIONS_INFO': 'Responde las preguntas del examen en esta página. Selecciona "Resumen" si quieres tener una vista de las preguntas que están marcadas para revisión. Selecciona "Finalizar el examen" cuando estés listo para enviar el examen.', 
        'QUESTIONS_S_INFO': 'Revisar una opción correcta', 
        'QUESTIONS_M_INFO': 'Revisar las opciones correctas', 
        'QUESTIONS_TF_INFO': 'Revisar la opción correcta', 
 
    // Time is running out modal
        'TIME_RUNNING_OUT': 'El tiempo se está acabando', 
        'TIME_RUNNING_OUT_MSG': 'Quedan {{minutesRemaining}} minutos para finalizar el examen. Asegúrese de revisar bien sus respuestas antes de que termine el tiempo.',
        'TIME_RUNNING_OUT_SUMMARY': 'Resumen', 
 
    // Connection lost modal
        'CONNECTION_ERROR': 'Se perdió la conexión con el servidor', 
        'CONNECTION_ERROR_MSG_OFFLINE': 'Parece que su ordenador ha perdido acceso a internet.', 
        'CONNECTION_ERROR_MSG': 'Actualmente estamos experimentando dificultades técnicas. Su examen estará disponible pronto.', 
        'CONNECTION_ERROR_MSG_CONTACT': 'Por favor contacte con "Metier Academy" inmediatamente por teléfono +47 24 12 45 00.',
        'CONNECTION_ERROR_RETRYING_IN': 'Reintentando en', 
        'CONNECTION_ERROR_CONNECTING': 'Conectando', 
        'CONNECTION_ERROR_RELOAD_PAGE': 'Recargando página', 
 
    // Unexpected error modal
        'UNEXPECTED_ERROR': 'Error inesperado', 
        'UNEXPECTED_ERROR_MSG': 'Ocurrió un error inesperado. Por favor recargue la página y trate nuevamente.', 
        'UNEXPECTED_ERROR_RELOAD_PAGE': 'Recargar página'
    });
}]);