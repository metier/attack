﻿angular.module('portal.exam').config(['$translateProvider', function ($translateProvider) {
    'use strict';
    $translateProvider.translations('de', {
        // Generic expressions
        'TRUE': 'Richtig', 
        'FALSE': 'Falsch', 
        'CANCEL': 'Löschen', 
        'CLOSE': 'Schließen', 
        
        // Menu
        'MENU_CONTINUE_EXAM': 'Test fortsetzen', 
        'MENU_PREVIOUS': 'Zurück', 
        'MENU_NEXT': 'Nächstes', 
        'MENU_FINISH_EXAM': 'Test abschliessen', 
        'MENU_SUMMARY': 'Zusammenfassung', 
 
        // Home page
        'HOME_START_EXAM': 'Test starten', 
        'HOME_STARTING_EXAM': 'Test starten', 
        'HOME_CONTINUE_EXAM': 'Test fortsetzen', 
        'HOME_START_EXAM_ERROR_MSG': 'Ein fehler ist aufgetreten während ihr Test gestartet wurde.',
        'HOME_START_EXAM_NOT_QUALIFIED_ERROR_MSG': 'Sie sind nicht zugelassen zu diesen Test. Bitte wenden sie sich umgehend an Metier Academy.', 
        'HOME_START_EXAM_NOT_ENOUGH_COMPLETED_COURSES_ERROR_MSG': 'Sie haben nicht ausreichend Kursmodule belegt, um den Test abzulegen. Bitte wenden sie sich an Metier Academy.', 
        'HOME_MINUTES': 'Minuten', 
        'HOME_SECONDS': 'Sekunden', 
        'HOME_DURATION': 'Dauer', 
        'HOME_AVAILABLE': 'Verfügbar',  
        'HOME_NUMBER_OF_QUESTIONS': 'Anzahl der Fragen', 
        
        // Deliver exam modal
        'COMPLETE_EXAM_FINISH_EXAM': 'Test abschliessen', 
        'COMPLETE_EXAM_SUBMIT_EXAM': 'Test senden', 
        'COMPLETE_EXAM_SUBMIT_EXAM_ERROR_MSG': 'Ein Fehler ist aufgetreten während sie ihren Test einreichen wollten. Bitte wiederholen sie den vorgang.', 
        'COMPLETE_EXAM_SUBMIT_MSG': 'Sie habe {{numberOfAnsweredQuestions}} Fragen von {{numberOfQuestions}} Fragen beantwortet. Click "Test senden" wenn sie wirklich den Test einreichen wollen. Wenn sie eine Zusammenfassung ihrer Testfragen wünschen; klicken sie bitte "Zusammenfassung" an.', 
        'COMPLETE_EXAM_SUMMARY': 'Zusammenfassung', 
 
        // Results page
        'RESULT_EXAM_COMPLETED': 'Test vollständig', 
        'RESULT_PERCENTAGE': 'Sie haben {{percentage}} % Fragen richtig beantwortet.', 
 
        // Summary page
        'SUMMARY': 'Zusammenfassung',
        'SUMMARY_INFO': 'Wenn sie ihre Antworten ändern möchten; bitte klicken sie auf die Frage: click "Test abschliessen" wenn sie mit ihrer Antwort zufrieden sind.', 
        'SUMMARY_MARK_FOR_REVIEW': 'Für späteren review markieren', 
        'SUMMARY_MARKED_FOR_REVIEW': 'Für späteren review markiert', 
        'SUMMARY_NOT_ANSWERED': 'Nicht beantwortet', 
        'SUMMARY_SUBMIT_EXAM': 'Test abschliessen', 
 
        // Questions page
        'QUESTIONS_INFO': 'Bitte beantworten sie die Fragen auf dieser Seite. Click "Zusammenfassung" wenn sie einen Überblick über die zum review markierten Fragen sehen möchten: click "Test abschliessen" wenn sie bereit sind den Test einzureichen.',
        'QUESTIONS_S_INFO': 'Wähle eine richtige Option', 
        'QUESTIONS_M_INFO': 'Wähle die richtigen Optionen', 
        'QUESTIONS_TF_INFO': 'Wähle die richtige Option',
 
        // Time is running out modal
        'TIME_RUNNING_OUT': 'Ihre Testzeit läuft ab', 
        'TIME_RUNNING_OUT_MSG': 'Sie haben nur noch {{minutesRemaining}} Minuten zeit; um ihren Test zubearbeiten. Bitte prüfen sie nochmals ob sie alle Fragen zu ihrer zufriedenheit beantwortet haben bevor die Zeit abläuft.',
        'TIME_RUNNING_OUT_SUMMARY': 'Zusammenfassung',
 
        // Connection lost modal
        'CONNECTION_ERROR': 'Verbindungsproblem mit dem Server', 
        'CONNECTION_ERROR_MSG_OFFLINE': 'Es scheint; dass ihr Computer die Verbindung zum Internet verloren hast.', 
        'CONNECTION_ERROR_MSG': 'Aktuell gibt es technische Probleme: ihr Test wird gleich zur verfügung stehen.',  
        'CONNECTION_ERROR_MSG_CONTACT': 'Bitte kontaktieren sie sofort die Metier Academy.', 
        'CONNECTION_ERROR_RETRYING_IN': 'Erneut versuchen in', 
        'CONNECTION_ERROR_CONNECTING': 'Verbinden', 
        'CONNECTION_ERROR_RELOAD_PAGE': 'Seite neu laden', 
 
        // Unexpected error modal
        'UNEXPECTED_ERROR': 'Unerwarteter fehler',
        'UNEXPECTED_ERROR_MSG': 'Ein unerwarteter Fehler ist aufgetreten. Bitte laden sie die Seite neu und versuchen sie es erneut.', 
        'UNEXPECTED_ERROR_RELOAD_PAGE': 'Seite neu laden' 
    });
}]);