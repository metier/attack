﻿angular.module('portal.exam').config(['$translateProvider', function ($translateProvider) {
    'use strict';
    $translateProvider.translations('da', {
        // Generic expressions
        'TRUE': 'Sandt',
        'FALSE': 'Falsk',
        'CANCEL': 'Afbryd',
        'CLOSE': 'Luk',

        // Menu
        'MENU_CONTINUE_EXAM': 'Fortsæt eksamen',
        'MENU_PREVIOUS': 'Forrige',
        'MENU_NEXT': 'Næste',
        'MENU_FINISH_EXAM': 'Afslut eksamen',
        'MENU_SUMMARY': 'Overblik',

        // Home page
        'HOME_START_EXAM': 'Start eksamen',
        'HOME_STARTING_EXAM': 'Starter eksamen',
        'HOME_CONTINUE_EXAM': 'Forsæt eksamen',
        'HOME_START_EXAM_ERROR_MSG': 'Der opstod en fejl, da du forsøgte at starte din eksamen.',
        'HOME_START_EXAM_NOT_QUALIFIED_ERROR_MSG': 'Du er ikke kvalificeret til denne eksamen. Kontakt venligst Metier Academy på telefon +45 7230 2035.',
        'HOME_START_EXAM_NOT_ENOUGH_COMPLETED_COURSES_ERROR_MSG': 'Du har ikke gennemført nok kurser til at starte denne eksamen. Kontakt venligst Metier Academy på telefon +45 7230 2035.',
        'HOME_MINUTES': 'minutter',
        'HOME_SECONDS': 'sekunder',
        'HOME_DURATION': 'Varighed',
        'HOME_AVAILABLE': 'Tilgængelig',
        'HOME_NUMBER_OF_QUESTIONS': 'Antal spørgsmål',

        // Deliver exam modal
        'COMPLETE_EXAM_FINISH_EXAM': 'Afslut eksamen',
        'COMPLETE_EXAM_SUBMIT_EXAM': 'Send eksamen',
        'COMPLETE_EXAM_SUBMIT_EXAM_ERROR_MSG': 'Der opstod en fejl under indsendelsen af din eksamen. Prøv venligst igen.',
        'COMPLETE_EXAM_SUBMIT_MSG': 'Du har besvaret {{numberOfAnsweredQuestions}} af {{numberOfQuestions}} spørgsmål. Klik på "Send eksamen" hvis du ønsker at indsende din eksamen. Hvis du ønsker at have en oversigt over dine eksamensspørgsmål, der er markeret til gennemsyn skal du klikke på "Overblik".',
        'COMPLETE_EXAM_SUMMARY': 'Overblik',

        // Results page
        'RESULT_EXAM_COMPLETED': 'Eksamen er fuldført',
        'RESULT_PERCENTAGE': 'Du fik {{percentage}}% rigtige ud af {{numberOfQuestions}} spørgsmål.',

        // Summary page
        'SUMMARY': 'Overblik',
        'SUMMARY_INFO': 'Hvis du ønsker at ændre nogle af dine svar, klik på svaret. Klik ”Afslut eksamen” når du er tilfreds med svarene og klar til at aflevere eksamen.',
        'SUMMARY_MARK_FOR_REVIEW': 'Markér for gennemgang',
        'SUMMARY_MARKED_FOR_REVIEW': 'Markeret for gennemgang',
        'SUMMARY_NOT_ANSWERED': 'Ikke besvaret',
        'SUMMARY_SUBMIT_EXAM': 'Afslut eksamen',

        // Questions page
        'QUESTIONS_INFO': 'Besvar spørgsmålene på denne side. Klik på “Overblik” hvis du ønsker at få en oversigt over spørgsmålene der er markeret til gennemgang.',
        'QUESTIONS_S_INFO': 'Markér det rigtige svar',
        'QUESTIONS_M_INFO': 'Markér det rigtige svar',
        'QUESTIONS_TF_INFO': 'Markér det rigtige svar',

        // Time is running out modal
        'TIME_RUNNING_OUT': 'Tiden er ved at løbe ud',
        'TIME_RUNNING_OUT_MSG': 'Der er {{minutesRemaining}} minutter tilbage af eksamenstiden. Husk at se dine eksamenssvar igennem i god tid inden tiden løber ud.',
        'TIME_RUNNING_OUT_SUMMARY': 'Overblik',

        // Connection lost modal
        'CONNECTION_ERROR': 'Mistede forbindelsen til serveren',
        'CONNECTION_ERROR_MSG_OFFLINE': 'Det ser ud til at din computer har mistet forbindelsen til internettet.',
        'CONNECTION_ERROR_MSG': 'Vi oplever desværre tekniske problemer. Din eksamen vil være tilgængelig igen om et øjeblik.',
        'CONNECTION_ERROR_MSG_CONTACT': 'Kontakt venligst Metier Academy øjeblikkeligt på telefon +45 7230 2035.',
        'CONNECTION_ERROR_RETRYING_IN': 'Prøver igen',
        'CONNECTION_ERROR_CONNECTING': 'Forbinder',
        'CONNECTION_ERROR_RELOAD_PAGE': 'Opdater siden',

        // Unexpected error modal
        'UNEXPECTED_ERROR': 'Uventet fejl',
        'UNEXPECTED_ERROR_MSG': 'Der opstod en uventet fejl. Opdater venligst siden og prøv igen.',
        'UNEXPECTED_ERROR_RELOAD_PAGE': 'Opdater siden'
    });
}]);