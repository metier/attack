﻿angular.module('portal.exam').config(['$translateProvider', function ($translateProvider) {
    'use strict';
    $translateProvider.translations('en', {
        // Generic expressions
        'TRUE': 'True',
        'FALSE': 'False',
        'CANCEL': 'Cancel',
        'CLOSE': 'Close',
        
        // Menu
        'MENU_CONTINUE_EXAM': 'Continue exam',
        'MENU_PREVIOUS': 'Previous',
        'MENU_NEXT': 'Next',
        'MENU_FINISH_EXAM': 'Finish exam',
        'MENU_SUMMARY': 'Summary',

        // Home page
        'HOME_START_EXAM': 'Start exam',
        'HOME_STARTING_EXAM': 'Starting exam',
        'HOME_CONTINUE_EXAM': 'Continue exam',
        'HOME_START_EXAM_ERROR_MSG': 'An error occured while trying to start your exam.',
        'HOME_START_EXAM_NOT_QUALIFIED_ERROR_MSG': 'You are not qualified for this exam. Please contact Metier Academy immediately by phone +47 24 12 45 00.',
        'HOME_START_EXAM_NOT_ENOUGH_COMPLETED_COURSES_ERROR_MSG': 'You have not completed enough courses to start this exam. Please contact Metier Academy by phone +47 24 12 45 00.',
        'HOME_MINUTES': 'minutes',
        'HOME_SECONDS': 'seconds',
        'HOME_DURATION': 'Duration',
        'HOME_AVAILABLE': 'Available',
        'HOME_NUMBER_OF_QUESTIONS': 'Number of questions',
        
        // Complete exam modal
        'COMPLETE_EXAM_FINISH_EXAM': 'Finish exam',
        'COMPLETE_EXAM_SUBMIT_EXAM': 'Submit exam',
        'COMPLETE_EXAM_SUBMIT_EXAM_ERROR_MSG': 'An error occured while trying to submit your exam. Please try again.',
        'COMPLETE_EXAM_SUBMIT_MSG': 'You have answered {{numberOfAnsweredQuestions}} of {{numberOfQuestions}} questions. Click "Submit exam" if you really want to submit your exam. If you wish to have a summary of your exam questions that are marked for review, click "Summary"',
        'COMPLETE_EXAM_SUMMARY': 'Summary',

        // Results page
        'RESULT_EXAM_COMPLETED': 'Exam completed',
        'RESULT_PERCENTAGE': 'You got {{percentage}}% correct of {{numberOfQuestions}} questions.',

        // Summary page
        'SUMMARY': 'Summary',
        'SUMMARY_INFO': 'If you would like to change any of your answers, click on the answer. Click "Finish exam" when you are satisfied with your answers.',
        'SUMMARY_MARK_FOR_REVIEW': 'Mark for review',
        'SUMMARY_MARKED_FOR_REVIEW': 'Marked for review',
        'SUMMARY_NOT_ANSWERED': 'Not answered',
        'SUMMARY_SUBMIT_EXAM': 'Finish exam',

        // Questions page
        'QUESTIONS_INFO': 'Answer your exam questions on this page. Click "Summary" if you would like an overview of the questions that are marked for review. Click "Finish exam" when you are ready to submit your exam.',
        'QUESTIONS_S_INFO': 'Check one correct option',
        'QUESTIONS_M_INFO': 'Check the correct options',
        'QUESTIONS_TF_INFO': 'Check the correct option',

        // Time is running out modal
        'TIME_RUNNING_OUT': 'Time is running out',
        'TIME_RUNNING_OUT_MSG': 'It\'s {{minutesRemaining}} minutes left of the exam time. Be sure to look over your answers well before time runs out.',
        'TIME_RUNNING_OUT_SUMMARY': 'Summary',

        // Connection lost modal
        'CONNECTION_ERROR': 'Lost connection to server',
        'CONNECTION_ERROR_MSG_OFFLINE': 'It appears that your computer has lost access to the internet.',
        'CONNECTION_ERROR_MSG': 'We are currently experiencing technical difficulties. Your exam will shortly be available.',
        'CONNECTION_ERROR_MSG_CONTACT': 'Please contact Metier Academy immediately by phone +47 24 12 45 00.',
        'CONNECTION_ERROR_RETRYING_IN': 'Retrying in',
        'CONNECTION_ERROR_CONNECTING': 'Connecting',
        'CONNECTION_ERROR_RELOAD_PAGE': 'Reload page',

        // Unexpected error modal
        'UNEXPECTED_ERROR': 'Unexpected error',
        'UNEXPECTED_ERROR_MSG': 'An unexpected error occured. Please reload page and try again.',
        'UNEXPECTED_ERROR_RELOAD_PAGE': 'Reload page'
    });
}]);