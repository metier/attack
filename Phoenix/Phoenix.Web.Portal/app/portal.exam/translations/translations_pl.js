﻿angular.module('portal.exam').config(['$translateProvider', function ($translateProvider) {
    'use strict';
    $translateProvider.translations('pl', {
        // Generic expressions
        'TRUE': 'Prawda',
        'FALSE': 'Fałsz',
        'CANCEL': 'Anuluj',
        'CLOSE': 'Zamknij',

        // Menu
        'MENU_CONTINUE_EXAM': 'KONTYNUUJ EGZAMIN',
        'MENU_PREVIOUS': 'Poprzedni',
        'MENU_NEXT': 'Nnastępny',
        'MENU_FINISH_EXAM': 'Zakończ egzamin',
        'MENU_SUMMARY': 'Podsumowanie',

        // Home page
        'HOME_START_EXAM': 'Rozpocznij egzamin',
        'HOME_STARTING_EXAM': 'Rozpoczęcie egzaminu',
        'HOME_CONTINUE_EXAM': 'Kontynuuj egzamin',
        'HOME_START_EXAM_ERROR_MSG': 'Podczas próby uruchomienia egzaminu wystąpił błąd.',
        'HOME_START_EXAM_NOT_QUALIFIED_ERROR_MSG': 'Nie zostałeś(-łaś) zakwalifikowany(-a) do tego egzaminu. Prosimy o bezzwłoczny, telefoniczny kontakt z Metier Academy,  na numer +47 24 12 45 00.',
        'HOME_START_EXAM_NOT_ENOUGH_COMPLETED_COURSES_ERROR_MSG': 'Nie ukończyłeś(-aś) wystarczającej ilości kursów, aby przystąpić do egzaminu. Prosimy o kontakt telefoniczny z Metier Academy na numer +47 24 12 45 00.',
        'HOME_MINUTES': 'minut',
        'HOME_SECONDS': 'sekund',
        'HOME_DURATION': 'Czas trwania',
        'HOME_AVAILABLE': 'Dostępne',
        'HOME_NUMBER_OF_QUESTIONS': 'Ilość pytań',

        // Deliver exam modal
        'COMPLETE_EXAM_FINISH_EXAM': 'Zakończ egzamin',
        'COMPLETE_EXAM_SUBMIT_EXAM': 'Wyślij egzamin',
        'COMPLETE_EXAM_SUBMIT_EXAM_ERROR_MSG': 'Podczas przesyłania egzaminu wystąpił błąd. Prosimy spróbować ponownie.',
        'COMPLETE_EXAM_SUBMIT_MSG': 'Odpowiedziałeś(-łaś) na {{numberOfAnsweredQuestions}} z {{numberOfQuestions}} pytań. Kliknij na "Prześlij egzamin", jeśli jesteś gotowy(-a) złożyć swój egzamin. Jeżeli chciałbyś/chciałabyś otrzymać raport zawierający zestawienie Twoich pytań zaznaczonych do recenzji, to kliknij na "Podsumowanie"',
        'COMPLETE_EXAM_SUMMARY': ' Summary ',

        // Results page
        'RESULT_EXAM_COMPLETED': 'Egzamin został ukończony',
        'RESULT_PERCENTAGE': 'Uzyskałeś(-aś) {{percentage}}% odpowiedzi prawidłowych na {{numberOfQuestions}} pytań.',

        // Summary page
        'SUMMARY': 'Summary',
        'SUMMARY_INFO': 'Jeżeli chciałbyś/chciałabyś zmienić którąkolwiek ze swoich odpowiedzi, to kliknij na daną odpowiedź. Gdy swoją odpowiedź uznasz za prawidłową, kliknij na "Zakończ egzamin".',
        'SUMMARY_MARK_FOR_REVIEW': 'Zaznacz, aby sprawdzić',
        'SUMMARY_MARKED_FOR_REVIEW': 'Zaznaczone do sprawdzenia',
        'SUMMARY_NOT_ANSWERED': 'Brak odpowiedzi',
        'SUMMARY_SUBMIT_EXAM': 'Zakończ egzamin',

        // Questions page
        'QUESTIONS_INFO': 'Odpowiedz na pytania egzaminacyjne na tej stronie. Kliknij na "Podsumowanie", jeśli chciałbyś/chciałabyś uzyskać przegląd pytań zaznaczonych do recenzji. Kliknij na "Zakończ egzamin", gdy będziesz chciał(-a) przesłać swój egzamin.',
        'QUESTIONS_S_INFO': 'Sprawdź jedna prawidłową opcję',
        'QUESTIONS_M_INFO': 'Sprawdź prawidłowe opcje',
        'QUESTIONS_TF_INFO': 'Sprawdź prawidłową opcję',

        // Time is running out modal
        'TIME_RUNNING_OUT': 'Czas się kończy',
        'TIME_RUNNING_OUT_MSG': 'Pozostało {{minutesRemaining}} minut do zakończenia egzaminu. Przed upływem czasu upewnij się, czy sprawdziłeś(-aś) swoje odpowiedzi. ',
        'TIME_RUNNING_OUT_SUMMARY': 'Summary',

        // Connection lost modal
        'CONNECTION_ERROR': 'Połączenie z serwerem zostało przerwane. ',
        'CONNECTION_ERROR_MSG_OFFLINE': 'Wygląda na to, że Twój komputer utracił dostęp do internetu.',
        'CONNECTION_ERROR_MSG': 'Jesteśmy w trakcie sprawdzania problemów technicznych. Twój egzamin będzie wkrótce dostępny.',
        'CONNECTION_ERROR_MSG_CONTACT': ' Prosimy o bezzwłoczny, telefoniczny kontakt z Metier Academy na numer +47 24 12 45 00.',
        'CONNECTION_ERROR_RETRYING_IN': 'Ponowna próba w ciągu',
        'CONNECTION_ERROR_CONNECTING': 'Łączenie',
        'CONNECTION_ERROR_RELOAD_PAGE': 'Przeładuj stronę',

        // Unexpected error modal
        'UNEXPECTED_ERROR': 'Nieoczekiwany błąd',
        'UNEXPECTED_ERROR_MSG': 'Wystąpił nieoczekiwany błąd. Prosimy przeładować stronę i spróbować ponownie.',
        'UNEXPECTED_ERROR_RELOAD_PAGE': 'Przeładuj stronę'

    });
}]);