﻿angular.module('portal.exam').config(['$translateProvider', function ($translateProvider) {
    'use strict';
    $translateProvider.translations('fr', {
        // Generic expressions
        'TRUE': 'Vrai',
        'FALSE': 'Faux',
        'CANCEL': 'Annuler',
        'CLOSE': 'Fermer',

        // Menu
        'MENU_CONTINUE_EXAM': 'Continuer l’examen',
        'MENU_PREVIOUS': 'Précédent',
        'MENU_NEXT': 'Suivant',
        'MENU_FINISH_EXAM': 'Terminer l’examen',
        'MENU_SUMMARY': 'Résumé',

        // Home page
        'HOME_START_EXAM': 'Commencer l’examen',
        'HOME_STARTING_EXAM': 'Examen en cours de chargement',
        'HOME_CONTINUE_EXAM': 'Continuer l’examen',
        'HOME_START_EXAM_ERROR_MSG': 'Une erreur s’est produite lors du lancement de votre examen.',
        'HOME_START_EXAM_NOT_QUALIFIED_ERROR_MSG': 'Vous n’avez pas accès à cet examen, contactez au plus vite Metier Academy par téléphone : +47 24 12 45 00.',
        'HOME_START_EXAM_NOT_ENOUGH_COMPLETED_COURSES_ERROR_MSG': 'Vous n’avez pas terminé suffisamment de leçons pour commencer cet examen. Contactez Metier Academy par téléphone : +47 24 12 45 00',

        'HOME_MINUTES': 'minutes',
        'HOME_SECONDS': 'secondes',
        'HOME_DURATION': 'Durée',
        'HOME_AVAILABLE': 'Disponible',
        'HOME_NUMBER_OF_QUESTIONS': 'Nombre de questions',


        // Deliver exam modal
        'COMPLETE_EXAM_FINISH_EXAM': 'Terminer l’examen',
        'COMPLETE_EXAM_SUBMIT_EXAM': 'Envoyer l’examen',
        'COMPLETE_EXAM_SUBMIT_EXAM_ERROR_MSG': 'Une erreur s’est produite lors de l’envoi de votre examen. Merci de réessayer.',

        'COMPLETE_EXAM_SUBMIT_MSG': 'Vous avez répondu à {{numberOfAnsweredQuestions}} questions sur un total de {{numberOfQuestions}}. Cliquez sur Envoyer l’examen si vous êtes sûr de vouloir envoyer. Si vous souhaitez voir un résumé des questions à relire, cliquez sur Résumé.',

        'COMPLETE_EXAM_SUMMARY': 'Résumé',

        // Results page
        'RESULT_EXAM_COMPLETED': 'Examen terminé',
        'RESULT_PERCENTAGE': 'Vous avez répondu correctement à {{percentage}}% des {{numberOfQuestions}} questions.',


        // Summary page
        'SUMMARY': 'Résumé',
        'SUMMARY_INFO': 'Si vous souhaitez changer certaines de vos réponses, cliquez sur la réponse à changer. Cliquez sur Terminer l’examen quand vous êtes satisfait de vos réponses.',



        'SUMMARY_MARK_FOR_REVIEW': 'Marquer à relire',
        'SUMMARY_MARKED_FOR_REVIEW': 'Marqué à relire',
        'SUMMARY_NOT_ANSWERED': 'Sans réponse',
        'SUMMARY_SUBMIT_EXAM': 'Terminer l’examen',

        // Questions page
        'QUESTIONS_INFO': 'Répondez aux questions sur cette page. Cliquez sur Résumé si vous souhaitez voir un résumé des questions à relire. Cliquez sur Terminer l’examen quand vous souhaitez soumettre vos réponses.',


        'QUESTIONS_S_INFO': 'Choisissez une réponse correcte',
        'QUESTIONS_M_INFO': 'Choisissez les réponses correctes',
        'QUESTIONS_TF_INFO': 'Choisissez la réponse correcte',

        // Time is running out modal
        'TIME_RUNNING_OUT': 'Temps disponible presque écoulé',
        'TIME_RUNNING_OUT_MSG': 'Seulement {{minutesRemaining}} minutes avant la fin de l’examen. Assurez-vous de relire vos réponses avant la fin du temps disponible.',

        'TIME_RUNNING_OUT_SUMMARY': 'Résumé',

        // Connection lost modal	
        'CONNECTION_ERROR': 'Connection au serveur interrompue',

        'CONNECTION_ERROR_MSG_OFFLINE': 'Il semblerait que votre ordinateur ne soit pas connecté à Internet.',
        'CONNECTION_ERROR_MSG': 'Nous rencontrons actuellement des difficultés techniques. Votre examen sera disponible dans quelques instants.',
        'CONNECTION_ERROR_MSG_CONTACT': 'Merci de prendre contact au plus vite avec Metier Academy par téléphone : +47 24 12 45 00.',

        'CONNECTION_ERROR_RETRYING_IN': 'Nouvel essai dans',
        'CONNECTION_ERROR_CONNECTING': 'Connection',
        'CONNECTION_ERROR_RELOAD_PAGE': 'Rafraichir la page',

        // Unexpected error modal
        'UNEXPECTED_ERROR': 'Erreur inattendue',
        'UNEXPECTED_ERROR_MSG': 'Une erreur inattendue s’est produite. Merci de bien vouloir rafraichir la page et de réessayer.',
        'UNEXPECTED_ERROR_RELOAD_PAGE': 'Rafraichir la page'

    });
}]);