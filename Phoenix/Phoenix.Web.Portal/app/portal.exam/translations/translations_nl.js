﻿angular.module('portal.exam').config(['$translateProvider', function ($translateProvider) {
    'use strict';
    $translateProvider.translations('nl', {
        // Generic expressions
        'TRUE': 'Waar',
        'FALSE': 'Niet waar',
        'CANCEL': 'Annuleren',
        'CLOSE': 'Afsluiten',

        // Menu
        'MENU_CONTINUE_EXAM': 'Hervat examen',
        'MENU_PREVIOUS': 'Vorige',
        'MENU_NEXT': 'Volgende',
        'MENU_FINISH_EXAM': 'Beëindig examen',
        'MENU_SUMMARY': 'Overzicht',

        // Home page
        'HOME_START_EXAM': 'Start examen',
        'HOME_STARTING_EXAM': 'Examen aan het laden',
        'HOME_CONTINUE_EXAM': 'Hervat examen',
        'HOME_START_EXAM_ERROR_MSG': 'Er is een fout opgetreden tijdens het starten van het examen',
        'HOME_START_EXAM_NOT_QUALIFIED_ERROR_MSG': 'Je bent niet gekwalificeerd om dit examen af te leggen. Neem a.u.b. contact op met e-coach.nederland@metieracademy.com.',
        'HOME_START_EXAM_NOT_ENOUGH_COMPLETED_COURSES_ERROR_MSG': 'Je hebt niet voldoende lessen afgerond om het examen af te leggen. Neem a.u.b. contact op met e-coach.nederland@metieracademy.com',
        'HOME_MINUTES': 'minuten',
        'HOME_SECONDS': 'seconden',
        'HOME_DURATION': 'Duur',
        'HOME_AVAILABLE': 'Beschikbaar',
        'HOME_NUMBER_OF_QUESTIONS': 'Aantal vragen',

        // Deliver exam modal
        'COMPLETE_EXAM_FINISH_EXAM': 'Beëindig examen',
        'COMPLETE_EXAM_SUBMIT_EXAM': 'Verstuur examen',
        'COMPLETE_EXAM_SUBMIT_EXAM_ERROR_MSG': 'Er is een fout opgetreden tijdens het versturen van het examen. Probeer het a.u.b. opnieuw.',
        'COMPLETE_EXAM_SUBMIT_MSG': 'Je hebt {{numberOfAnsweredQuestions}} van de {{numberOfQuestions}} vragen beantwoord. Klik op “Verstuur examen” als je daadwerkelijk het examen wilt insturen. Als je een overzicht wilt van de vragen die je hebt gemarkeerd ter review, klik dan op “Overzicht”.',
        'COMPLETE_EXAM_SUMMARY': 'Overzicht',

        // Results page
        'RESULT_EXAM_COMPLETED': 'Examen voltooid',
        'RESULT_PERCENTAGE': 'Je hebt {{percentage}}% goed van {{numberOfQuestions}} vragen',

        // Summary page
        'SUMMARY': 'Overzicht',
        'SUMMARY_INFO': 'Als je een antwoord wilt veranderen, klik dan op het betreffende antwoord. Klik “Beëindig examen” wanneer je tevreden bent met jouw antwoorden.',
        'SUMMARY_MARK_FOR_REVIEW': 'Markeer voor review',
        'SUMMARY_MARKED_FOR_REVIEW': 'Gemarkeerd voor review',
        'SUMMARY_NOT_ANSWERED': 'Niet beantwoord',
        'SUMMARY_SUBMIT_EXAM': 'Beëindig examen',

        // Questions page
        'QUESTIONS_INFO': 'Beantwoord de examenvragen op deze pagina. Klik op “Overzicht” als je een overzicht wilt van de vragen die zijn gemarkeerd voor review. Klik op “Beëindig examen” als het examen wilt versturen.',
        'QUESTIONS_S_INFO': 'Kies het juiste antwoord',
        'QUESTIONS_M_INFO': 'Kies de juiste antwoorden',
        'QUESTIONS_TF_INFO': 'Kies het juiste antwoord',

        // Time is running out modal
        'TIME_RUNNING_OUT': 'De tijd is bijna om',
        'TIME_RUNNING_OUT_MSG': 'Er zijn {{minutesRemaining}} minuten over van de examentijd. Zorg ervoor dat je alle vragen hebt beantwoord voordat de tijd voorbij is.',
        'TIME_RUNNING_OUT_SUMMARY': 'Overzicht',

        // Connection lost modal
        'CONNECTION_ERROR': 'Verbinding met de server verloren',
        'CONNECTION_ERROR_MSG_OFFLINE': 'Uw computer heeft geen verbinding meer met het internet',
        'CONNECTION_ERROR_MSG': 'Wij ondervinden momenteel technische problemen. Het examen is binnenkort weer beschikbaar.',
        'CONNECTION_ERROR_MSG_CONTACT': 'Neem a.u.b. contact op met e-coach.nederland@metieracademy.com ',
        'CONNECTION_ERROR_RETRYING_IN': 'Opnieuw proberen in',
        'CONNECTION_ERROR_CONNECTING': 'Verbinding maken',
        'CONNECTION_ERROR_RELOAD_PAGE': 'Herlaad pagina',

        // Unexpected error modal
        'UNEXPECTED_ERROR': 'Een onverwachte fout',
        'UNEXPECTED_ERROR_MSG': 'Er is een onverwachte fout opgetreden. Ververs de pagina a.u.b. en probeer opnieuw.',
        'UNEXPECTED_ERROR_RELOAD_PAGE': 'Herlaad pagina'

    });
}]);