﻿angular.module('portal', [
    'portal.shared',
    'portal.exam',
    'portal.config',
    'ngAnimate',
    'ui.router',
    'ui.bootstrap',
    'pascalprecht.translate',
    'tmh.dynamicLocale'
]).config([
    '$stateProvider',
    '$urlRouterProvider',
    'tmhDynamicLocaleProvider',
    'portalConfig',
    function ($stateProvider, $urlRouterProvider, tmhDynamicLocaleProvider , portalConfig) {
        'use strict';
        $stateProvider.state('404', {
            url: '/404',
            templateUrl: portalConfig.basePath + 'app/portal/views/404.html',
        });
        $urlRouterProvider.otherwise("/404");
        tmhDynamicLocaleProvider.localeLocationPattern(portalConfig.basePath + 'app/bower_components/angular-i18n/angular-locale_{{locale}}.js');
    }
]);

angular.module('portal').config(['$translateProvider', function ($translateProvider) {
    'use strict';
    $translateProvider.preferredLanguage('en');
}]);