﻿using System.Web.Mvc;

namespace Phoenix.Web.Portal.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
    }
}