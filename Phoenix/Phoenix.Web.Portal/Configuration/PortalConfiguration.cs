﻿using System.Configuration;

namespace Metier.Phoenix.Web.Portal.Configuration
{
    public static class PortalConfiguration
    {
        public static string GetPhoenixApiUrl()
        {
            var url = ConfigurationManager.AppSettings["PhoenixApiUrl"];
            return url.EndsWith("/") ? url : url + "/";
        }

        public static string GetLearningPortalLoginUrl()
        {
            return ConfigurationManager.AppSettings["LearningPortalLoginUrl"];
        }
    }
}