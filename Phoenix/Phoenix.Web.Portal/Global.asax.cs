﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Metier.Phoenix.Web.Portal.App_Start;

namespace Phoenix.Web.Portal
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
