﻿using System.Web.Optimization;

namespace Metier.Phoenix.Web.Portal.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/app/portal/js")
                        .IncludeDirectory("~/app/portal.shared", "*.js", true)
                        .IncludeDirectory("~/app/portal", "*.js", true)
                        .IncludeDirectory("~/app/portal.exam", "*.js", true));

            bundles.Add(new StyleBundle("~/app/portalcss").Include(
                "~/app/bower_components/animate-css/animate.css",
                "~/app/styles/app-compiled.css"
                ));

            bundles.Add(new ScriptBundle("~/app/polyfills/lib/js").Include(
               "~/app/polyfills/stringTrimPolyfill.js",
               "~/app/polyfills/addEventListenerPolyfill.js",
               "~/app/polyfills/getComputedStylePolyfill.js",
               "~/app/polyfills/dateNowPolyfill.js",
               "~/app/polyfills/indexOfPolyfill.js"
            ));

            bundles.Add(new ScriptBundle("~/app/bower/ie/js").Include(
               "~/app/bower_components/respond/dest/respond.min.js"
            ));

            bundles.Add(new ScriptBundle("~/app/bower/lib/js").Include(
               "~/app/bower_components/jquery/dist/jquery.js",
               "~/app/bower_components/angular/angular.js",
               "~/app/bower_components/angular-animate/angular-animate.js",
               "~/app/bower_components/angular-ui-router/release/angular-ui-router.js",
               "~/app/bower_components/angular-translate/angular-translate.js",
               "~/app/bower_components/angular-dynamic-locale/src/tmhDynamicLocale.js",
               "~/app/bower_components/lodash/dist/lodash.js",
               "~/app/bower_components/momentjs/moment.js",
               "~/app/bower_components/respond/moment.js",
               "~/app/bower_components/angular-bootstrap/ui-bootstrap.js",
               "~/app/bower_components/angular-bootstrap/ui-bootstrap-tpls.js"
            ));
        }
    }
}