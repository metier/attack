
if exists(
	select * from sys.databases where name = 'mcs'
	)
Begin
	EXEC msdb.dbo.sp_delete_database_backuphistory @database_name = N'mcs'

	USE [master]
	ALTER DATABASE [mcs] SET  SINGLE_USER WITH ROLLBACK IMMEDIATE
	USE [master]
	/****** Object:  Database [mcs]    Script Date: 27.11.2014 10:38:33 ******/
	DROP DATABASE [mcs]
End


if exists(
	select * from sys.databases where name = 'Phoenix'
	)
Begin
	EXEC msdb.dbo.sp_delete_database_backuphistory @database_name = N'Phoenix';
	
	USE [master]
	ALTER DATABASE [Phoenix] SET  SINGLE_USER WITH ROLLBACK IMMEDIATE
	/****** Object:  Database [Phoenix]    Script Date: 27.11.2014 10:39:13 ******/
	DROP DATABASE [Phoenix]
End


USE [master]
--ALTER DATABASE [Phoenix] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
RESTORE DATABASE [Phoenix] FROM  DISK = N'C:\temp\Databases\Phoenix.bak' WITH  FILE = 1,  MOVE N'Phoenix' TO N'E:\Data\Phoenix.mdf',  MOVE N'Phoenix_log' TO N'F:\Log\Phoenix_log.ldf',  NOUNLOAD,  REPLACE,  STATS = 5
ALTER DATABASE [Phoenix] SET MULTI_USER

GO


USE [master]
--ALTER DATABASE [mcs] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
RESTORE DATABASE [mcs] FROM  DISK = N'C:\temp\Databases\mcs.bak' WITH  FILE = 1,  MOVE N'mcs' TO N'E:\Data\mcs.mdf',  MOVE N'mcs_log' TO N'F:\Log\mcs_log.ldf',  NOUNLOAD,  REPLACE,  STATS = 5
ALTER DATABASE [mcs] SET MULTI_USER

GO

print 'Setting permissions for met-PhoenixApi'
USE [Phoenix]
GO
CREATE USER [WINAD\met-phoenixapi] FOR LOGIN [WINAD\met-phoenixapi]
GO
USE [Phoenix]
GO
ALTER ROLE [db_datareader] ADD MEMBER [WINAD\met-phoenixapi]
GO
USE [Phoenix]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [WINAD\met-phoenixapi]
GO

use [Phoenix]
GO
GRANT EXECUTE TO [WINAD\met-phoenixapi]
GO

print 'Setting permissions for met-PhoenixTask'
USE [mcs]
GO
CREATE USER [WINAD\met-phoenixtask] FOR LOGIN [WINAD\met-phoenixtask]
GO
USE [mcs]
GO
ALTER ROLE [db_datareader] ADD MEMBER [WINAD\met-phoenixtask]
GO
USE [Phoenix]
GO
CREATE USER [WINAD\met-phoenixtask] FOR LOGIN [WINAD\met-phoenixtask]
GO
USE [Phoenix]
GO
ALTER ROLE [db_datareader] ADD MEMBER [WINAD\met-phoenixtask]
GO
USE [Phoenix]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [WINAD\met-phoenixtask]
GO

print 'Setting permissions for met-Behind'
USE [mcs]
GO
CREATE USER [WINAD\met-behind] FOR LOGIN [WINAD\met-behind]
GO
USE [mcs]
GO
ALTER ROLE [db_owner] ADD MEMBER [WINAD\met-behind]
GO

USE [Phoenix]
GO
CREATE USER [WINAD\met-behind] FOR LOGIN [WINAD\met-behind]
GO

use [Phoenix]
GO
GRANT SELECT ON [dbo].[Users] TO [WINAD\met-behind]
GO

use [Phoenix]
GO
GRANT SELECT ON [dbo].[Customers] TO [WINAD\met-behind]
GO

use [Phoenix]
GO
GRANT SELECT ON [dbo].[vw_aspnet_MembershipUsers] TO [WINAD\met-behind]
GO


print 'Removing orphaned users from mcs-database'
USE [mcs]
GO

Drop user BehindDev
Drop user mcs_owner

GO

print 'Successfully imported and set permissions on databases!'