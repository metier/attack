param([string]$Destination = "")

& xcopy /Q /Y "$Env:TF_BUILD_BINARIESDIRECTORY" "$Destination"

$username = "Administrator"
$password = "p@ssword1"
$credentials = New-Object System.Management.Automation.PSCredential -ArgumentList @($username,(ConvertTo-SecureString -String $password -AsPlainText -Force))

$taskArguments = "-register"
Start-Process $Destination\Metier.Phoenix.Tasks.exe $taskArguments -Credential ($credentials)