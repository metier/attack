$localDirectory = "$Env:TF_BUILD_SOURCESDIRECTORY\phoenix\database\scripts\RoundHousE\*"
$remoteDirectory = '\\U-LMS-001\websites\PhoenixProd\db-scripts\'

Remove-Item "$remoteDirectory" -recurse -Force
new-item "$remoteDirectory" -itemtype directory
Copy-Item -Path "$localDirectory" -Destination "$remoteDirectory" -recurse -Force