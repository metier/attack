﻿# This script is maintained under "Phoenix/Scripts/" in the git repository. Make sure both are in sync!!

. "$PSScriptRoot\ZipFile_function.ps1"

$DeploymentBaseFolder = 'C:\PhoenixDeploy'

$ErrorActionPreference="SilentlyContinue"
Stop-Transcript | out-null
$ErrorActionPreference = "Stop"
Start-Transcript -path $DeploymentBaseFolder'\Logs\Deployment.txt' -append


$DeploymentInstallFolder = $DeploymentBaseFolder+'\InstallFiles'
$iisAppFolder = 'C:\inetpub\wwwroot\Phoenix'
$BackupFile = "C:\PhoenixDeploy\Backups\$(Get-Date -format ddMMyyyy-HHmm).zip"

$PhoenixTasksFolder = "C:\Phoenix\Tasks"

$apiFolder = $DeploymentInstallFolder+'\api'
$adminFolder = $DeploymentInstallFolder+'\admin'
$portalFolder = $DeploymentInstallFolder+'\portal'
$behindFolder = $DeploymentInstallFolder+'\learningportal'
$customerSpecificFolder = $DeploymentInstallFolder+'\customerspecific'
$tasksFolder = $DeploymentInstallFolder+'\tasks\*'

"Deploying new version of Phoenix"

$DatabaseServerName = "met-db01.prodno.osl.basefarm.net"

"Creating a backup of current site"
#New-ZipFile -InputObject $iisAppFolder -ZipFilePath $BackupFile 
#New-ZipFile -InputObject $PhoenixTasksFolder -ZipFilePath $BackupFile -Append


#"Running Roundhouse scripts, if any..."
#& $DeploymentBaseFolder\rh.exe /silent /c="Server=$DatabaseServerName;Database=Phoenix;Trusted_Connection=True" /ct=90000000 /cta=90000000 /f="$DeploymentBaseFolder\InstallFiles\db-scripts\"


if( Test-Path $apiFolder ){
    "Deploying Api"
    Stop-WebAppPool -Name "PhoenixApi"
    Copy-Item -Path $apiFolder -Destination $iisAppFolder -Force -Recurse 
    Start-WebAppPool -Name "PhoenixApi"
    "Api deployed"
}

if( Test-Path $adminFolder ){
    "Deploying Admin"
    Stop-WebAppPool -Name "PhoenixAdmin"
    Copy-Item -Path $adminFolder -Destination $iisAppFolder -Force -Recurse 
    Start-WebAppPool -Name "PhoenixAdmin"
    "Admin deployed"
}

if( Test-Path $portalFolder ){
    "Deploying Portal"
    Stop-WebAppPool -Name "PhoenixPortal"
    Copy-Item -Path $portalFolder -Destination $iisAppFolder -Force -Recurse 
    Start-WebAppPool -Name "PhoenixPortal"
    "Portal deployed"
}

if( Test-Path $behindFolder ){
    "Deploying Behind"
    Stop-WebAppPool -Name "PhoenixBehind"
    Copy-Item -Path $behindFolder -Destination $iisAppFolder -Force -Recurse 
    Start-WebAppPool -Name "PhoenixBehind"
    "Behind deployed"
}

if( Test-Path $customerSpecificFolder ){
    "Deploying customerSpecific"
    Stop-WebAppPool -Name "PhoenixCustomerSpecific"
    Copy-Item -Path $customerSpecificFolder -Destination $iisAppFolder -Force -Recurse 
    Start-WebAppPool -Name "PhoenixCustomerSpecific"
    "CustomerSpecific deployed"
}


if( Test-Path $tasksFolder ){
    "Deploying tasks"
    Copy-Item -Path $tasksFolder -Destination $PhoenixTasksFolder -Force -Recurse 
    "Tasks deployed"
}

"Deployment complete"

Stop-Transcript